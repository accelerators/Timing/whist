--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:19:18 04/29/2016
-- Design Name:   
-- Module Name:   /mntdirect/_users/broquet/kairos/gw/testbench/buclk/tb_buclk.vhd
-- Project Name:  kairos
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: buclk
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use ieee.std_logic_unsigned.all;

ENTITY tb_i2c_mux IS
END tb_i2c_mux;
 
ARCHITECTURE behavior OF tb_i2c_mux IS 

  component i2c_mux is
    port (
    --MUX signals
    i2c_sel_i : in  std_logic_vector(1 downto 0);
    i2c_lck_o : out std_logic;

    --I2C signals
    sda_mux_i : in  std_logic;
    sda_mux_o : out std_logic;
    scl_mux_i : in  std_logic;
    scl_mux_o : out std_logic;

    wrc_sda_i : out std_logic;
    wrc_sda_o : in  std_logic;
    wrc_scl_i : out std_logic;
    wrc_scl_o : in  std_logic;
    
    wrn_sda_i : out std_logic;
    wrn_sda_o : in  std_logic;
    wrn_scl_i : out std_logic;
    wrn_scl_o : in  std_logic
    );
  end component;
    
  --Inputs
  signal i2c_sel_i : std_logic_vector(1 downto 0) := "00";
  signal sda_mux_i : std_logic := '0';
  signal scl_mux_i : std_logic := '0';
  signal wrc_sda_o : std_logic := '0';
  signal wrc_scl_o : std_logic := '0';
  signal wrn_sda_o : std_logic := '0';
  signal wrn_scl_o : std_logic := '0';

  --Outputs
  signal i2c_lck_o : std_logic;
  signal sda_mux_o : std_logic;
  signal scl_mux_o : std_logic;
  signal wrc_sda_i : std_logic;
  signal wrc_scl_i : std_logic;    
  signal wrn_sda_i : std_logic;
  signal wrn_scl_i : std_logic;

  -- Clock period definitions
  constant clk_i2c_period : time := 5 us;
  
BEGIN
 
  -- Instantiate the Unit Under Test (UUT)
  uut: i2c_mux
    PORT MAP (
      i2c_sel_i => i2c_sel_i,
      i2c_lck_o => i2c_lck_o,
      sda_mux_i => sda_mux_i,
      sda_mux_o => sda_mux_o,
      scl_mux_i => scl_mux_i,
      scl_mux_o => scl_mux_o,
      wrc_sda_i => wrc_sda_i,
      wrc_sda_o => wrc_sda_o,
      wrc_scl_i => wrc_scl_i,
      wrc_scl_o => wrc_scl_o,
      wrn_sda_i => wrn_sda_i,
      wrn_sda_o => wrn_sda_o,
      wrn_scl_i => wrn_scl_i,
      wrn_scl_o => wrn_scl_o
      );


   -- Clock process definitions
   clk_i2c_i_process :process
   begin
     scl_mux_i <= '0';
     wrc_scl_o <= '0';
     wrn_scl_o <= '0';
     wait for clk_i2c_period/2;
     scl_mux_i <= '1';
     wrc_scl_o <= '1';
     wrn_scl_o <= '1';
     wait for clk_i2c_period/2;
   end process;

  wrc_sda_o_process :process
  begin
    wrc_sda_o <= '1';
    wait for clk_i2c_period;
    wrc_sda_o <= '0';
    wait for clk_i2c_period;
    wrc_sda_o <= '1';
    wait for clk_i2c_period;
    wrc_sda_o <= '1';
    wait for clk_i2c_period;
    wrc_sda_o <= '1';
    wait for clk_i2c_period;
    wrc_sda_o <= '0';
    wait for clk_i2c_period;
    wrc_sda_o <= '1';
    wait for clk_i2c_period;
    wrc_sda_o <= '1';
    wait for clk_i2c_period;
  end process;

  wrn_sda_o_process :process
  begin
    wrn_sda_o <= '0';
    wait for clk_i2c_period;
    wrn_sda_o <= '1';
    wait for clk_i2c_period;
    wrn_sda_o <= '1';
    wait for clk_i2c_period;
    wrn_sda_o <= '0';
    wait for clk_i2c_period;
    wrn_sda_o <= '0';
    wait for clk_i2c_period;
    wrn_sda_o <= '1';
    wait for clk_i2c_period;
    wrn_sda_o <= '1';
    wait for clk_i2c_period;
    wrn_sda_o <= '0';
    wait for clk_i2c_period;
  end process;

  sda_mux_i_process :process
  begin
    sda_mux_i <= '1';
    wait for clk_i2c_period;
    sda_mux_i <= '1';
    wait for clk_i2c_period;
    sda_mux_i <= '0';
    wait for clk_i2c_period;
    sda_mux_i <= '1';
    wait for clk_i2c_period;
    sda_mux_i <= '0';
    wait for clk_i2c_period;
    sda_mux_i <= '1';
    wait for clk_i2c_period;
    sda_mux_i <= '0';
    wait for clk_i2c_period;
    sda_mux_i <= '1';
    wait for clk_i2c_period;
  end process;

  
  -- Stimulus process
   stim_proc: process
   begin
     -- hold reset state for 100 ns.
     wait for 100 ns;	

     -- insert stimulus here 
     wait for clk_i2c_period*20;
     i2c_sel_i <= "01";
     wait for clk_i2c_period*20;
     i2c_sel_i <= "10";
     wait for clk_i2c_period*50;
     i2c_sel_i <= "11";
     wait for clk_i2c_period*10;
     i2c_sel_i <= "00";
     
     wait;
   end process;

END;
