-------------------------------------------------------------------------------
-- Title      : WR RF DDS Distribution Node (SVEC)
-- Project    : 
-------------------------------------------------------------------------------
-- File       : svec_top.vhd
-- Author     : Tomasz Włostowski
-- Company    : CERN BE-CO-HT
-- Created    : 2014-04-01
-- Last update: 2020-07-30
-- Platform   : FPGA-generic
-- Standard   : VHDL'93
-------------------------------------------------------------------------------
-- Description: 
--
-- fill me
-------------------------------------------------------------------------------
--
-- Copyright (c) 2014 CERN
--
-- This source file is free software; you can redistribute it   
-- and/or modify it under the terms of the GNU Lesser General   
-- Public License as published by the Free Software Foundation; 
-- either version 2.1 of the License, or (at your option) any   
-- later version.                                               
--
-- This source is distributed in the hope that it will be       
-- useful, but WITHOUT ANY WARRANTY; without even the implied   
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      
-- PURPOSE.  See the GNU Lesser General Public License for more 
-- details.                                                     
--
-- You should have received a copy of the GNU Lesser General    
-- Public License along with this source; if not, download it   
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.wishbone_pkg.all;
use work.spec_node_pkg.all;
use work.wrn_mqueue_pkg.all;
use work.wr_node_pkg.all;
use work.buclk_sdb_pkg.all;
use work.buclk_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity top is
  generic (
    g_simulation : boolean := false;
    g_pulses : integer := 12
    );

  port (
    
      clk_20m_vcxo_i : in std_logic;    -- 20MHz VCXO clock

      clk_125m_pllref_p_i : in std_logic;  -- 125 MHz PLL reference
      clk_125m_pllref_n_i : in std_logic;

      clk_125m_gtp_n_i : in std_logic;  -- 125 MHz GTP reference
      clk_125m_gtp_p_i : in std_logic;

      -- font panel leds
      led_red   : out std_logic;
      led_green : out std_logic;

      -------------------------------------------------------------------------
      -- PLL VCXO DAC Drive
      -------------------------------------------------------------------------

      dac_sclk_o  : out std_logic;
      dac_din_o   : out std_logic;
      dac_cs1_n_o : out std_logic;
      dac_cs2_n_o : out std_logic;

      fmc_scl_b : inout std_logic := '1';
      fmc_sda_b : inout std_logic := '1';

      carrier_onewire_b : inout std_logic := '1';
      fmc_prsnt_m2c_l_i : in    std_logic;

      -------------------------------------------------------------------------
      -- SFP pins
      -------------------------------------------------------------------------

      sfp_txp_o : out std_logic;
      sfp_txn_o : out std_logic;

      sfp_rxp_i : in std_logic := '0';
      sfp_rxn_i : in std_logic := '1';

      sfp_mod_def0_b    : in    std_logic;  -- detect pin
      sfp_mod_def1_b    : inout std_logic;  -- scl
      sfp_mod_def2_b    : inout std_logic;  -- sda
      sfp_rate_select_b : inout std_logic := '0';
      sfp_tx_fault_i    : in    std_logic := '0';
      sfp_tx_disable_o  : out   std_logic;
      sfp_los_i         : in    std_logic := '0';


      -------------------------------------------------------------------------
      -- WR core UART
      -------------------------------------------------------------------------
            
      uart_rxd_i : in  std_logic := '1';
      uart_txd_o : out std_logic;

      button1_i : in std_logic := '1';
      button2_i : in std_logic := '1';

      leds_n_o : out std_logic_vector(3 downto 0);

      -- Flash SPI pins
      spi_cs_n_o : out std_logic;
      spi_sclk_o : out std_logic;
      spi_mosi_o : out std_logic;
      spi_miso_i : in  std_logic;

      
      -------------------------------------------------------------------------
      -- FMC DDS
      -------------------------------------------------------------------------

    -- DDS Dac I/F (Maxim)
    fmc0_dac_n_o : out std_logic_vector(13 downto 0);
    fmc0_dac_p_o : out std_logic_vector(13 downto 0);

    -- SPI bus to both PLL chips
    fmc0_pll_sclk_o : out  std_logic; --originally declared as buffer
    fmc0_pll_sdio_b : inout std_logic;
    fmc0_pll_sdo_i  : in    std_logic;


    -- System/WR PLL dedicated lines
    fmc0_pll_sys_ld_i      : in  std_logic;
    fmc0_pll_sys_reset_n_o : out std_logic; --originally declared as buffer
    fmc0_pll_sys_cs_n_o    : out std_logic; --originally declared as buffer
    fmc0_pll_sys_sync_n_o  : out std_logic; --originally declared as buffer

    -- VCXO PLL dedicated lines
    fmc0_pll_vcxo_cs_n_o   : out std_logic; --originally declared as buffer
    fmc0_pll_vcxo_sync_n_o : out std_logic; --originally declared as buffer
    fmc0_pll_vcxo_status_i : in  std_logic;

    -- Phase Detector & ADC
    fmc0_adc_sdo_i : in  std_logic;
    fmc0_adc_sck_o : out std_logic;
    fmc0_adc_cnv_o : out std_logic;
    fmc0_adc_sdi_o : out std_logic;
    fmc0_pd_lockdet_i : in    std_logic;
    fmc0_pd_clk_o     : out   std_logic;
    fmc0_pd_data_b    : inout std_logic;
    fmc0_pd_le_o      : out   std_logic;
    
    -- WR reference clock from FMC's WR PLL (AD9516)
    fmc0_wr_ref_clk_n_i : in std_logic;
    fmc0_wr_ref_clk_p_i : in std_logic;

    -- WR reference clock from FMC's RF DAC
    fmc0_synth_clk_n_i : in std_logic; -- = clk0_M2C_P/N (fmc connector) = CLK0OUT_P/N (DAC)
    fmc0_synth_clk_p_i : in std_logic;

    -- WR reference clock from FMC's RF PLL (AD9510)
    fmc0_rf_clk_n_i : in std_logic; -- = clk1_M2C_P/N (fmc connector) = CLK2OUT_P/N (AD9510)
    fmc0_rf_clk_p_i : in std_logic;

    -- Front Panel outputs
    fp_out        : out buclk_pulse(g_pulses downto 1);

    -- Trigger inputs
    trig_sma      : in std_logic_vector(1 downto 0);
    trig_lemo     : in std_logic_vector(3 downto 2);

    -- OneWire (ID & temp sensor)
    fmc0_onewire_b : inout std_logic;

    -- WR mezzanine DAC
    fmc0_wr_dac_sclk_o : out std_logic;
    fmc0_wr_dac_din_o : out std_logic;
    fmc0_wr_dac_sync_n_o : out std_logic

    );
end top;

architecture rtl of top is

  function f_int_to_bool (x : integer) return boolean is
  begin
    if (x = 0) then
      return false;
    else
      return true;
    end if;
  end function;


  component wr_d3s_core is
    generic (
      g_simulation     : boolean;
      g_sim_pps_period : integer := 1000);
    port (
      clk_sys_i                 : in    std_logic;
      rst_n_i                   : in    std_logic;
      clk_ref_i                 : in    std_logic;
      clk_wr_o                  : out   std_logic;
      tm_link_up_i              : in    std_logic := '1';
      tm_time_valid_i           : in    std_logic;
      tm_tai_i                  : in    std_logic_vector(39 downto 0);
      tm_cycles_i               : in    std_logic_vector(27 downto 0);
      tm_clk_aux_lock_en_o      : out   std_logic;
      tm_clk_aux_locked_i       : in    std_logic;
      tm_dac_value_i            : in    std_logic_vector(23 downto 0);
      tm_dac_wr_i               : in    std_logic;
      dac_n_o                   : out   std_logic_vector(13 downto 0);
      dac_p_o                   : out   std_logic_vector(13 downto 0);
      wr_ref_clk_n_i            : in    std_logic;
      wr_ref_clk_p_i            : in    std_logic;
      clk_dds_synth             : in    std_logic;
      clk_rf_in                 : in    std_logic;
      pll_sys_cs_n_o            : out   std_logic;
      pll_sys_ld_i              : in    std_logic;
      pll_sys_reset_n_o         : out   std_logic;
      pll_sys_sync_n_o          : out   std_logic;
      pll_vcxo_cs_n_o           : out   std_logic;
      pll_vcxo_sync_n_o         : out   std_logic;
      pll_vcxo_status_i         : in    std_logic;
      pll_sclk_o                : out   std_logic;
      pll_sdio_b                : inout std_logic;
      pll_sdo_i                 : in    std_logic;
      pd_lockdet_i              : in    std_logic;
      pd_clk_o                  : out   std_logic;
      pd_data_b                 : inout std_logic;
      pd_le_o                   : out   std_logic;
      adc_sdo_i                 : in    std_logic;
      adc_sck_o                 : out   std_logic;
      adc_cnv_o                 : out   std_logic;
      adc_sdi_o                 : out   std_logic;
      delay_d_o                 : out   std_logic_vector(9 downto 0);
      delay_fb_i                : in    std_logic;
      delay_len_o               : out   std_logic;
      delay_pulse_o             : out   std_logic;
      onewire_b                 : inout std_logic;
      wr_dac_sclk_o             : out   std_logic;
      wr_dac_din_o              : out   std_logic;
      wr_dac_sync_n_o           : out   std_logic;
      rf_counter_valid_o        : out   std_logic;
      rf_phase_safe_o           : out   std_logic;
      rf_counter_o              : out   std_logic_vector(31 downto 0);
      rf_counter_overflow_p_o   : out   std_logic;
      rt_d3s_running_o          : out   std_logic;
      rt_d3s_locked_o           : out   std_logic;
      slave_i                   : in    t_wishbone_slave_in;
      slave_o                   : out   t_wishbone_slave_out;
      debug_o                   : out std_logic_vector(3 downto 0)
);
  end component wr_d3s_core;


 constant c_D3S_SDB_DEVICE : t_sdb_device := (
    abi_class     => x"0000",              -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"00",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"7",                 -- 8/16/32-bit port granularity
    sdb_component => (
      addr_first  => x"0000000000000000",
      addr_last   => x"0000000000000fff",
      product     => (
        vendor_id => x"000000000000CE42",  -- CERN
        device_id => x"dd334410",          
        version   => x"00000001",
        date      => x"20150427",
        name      => "WR-D3S-Core        ")));
  
  constant c_hmq_config : t_wrn_mqueue_config :=
    (
      out_slot_count  => 3,
      out_slot_config => (
        0             => (width => 32, entries => 8),  -- control CPU 0 (to host)
        1             => (width => 32, entries => 8),  -- control CPU 1 (to host)
        2             => (width => 32, entries => 8),  -- control CPU 2 (to host)
        others        => (0, 0)),

      in_slot_count  => 3,
      in_slot_config => (
        0            => (width => 32, entries => 8),  -- control CPU 0 (from host)
        1            => (width => 32, entries => 8),  -- control CPU 1 (from host)
        2            => (width => 32, entries => 8),  -- control CPU 2 (from host)
        others       => (0, 0)
        )
      );

  
  constant c_rmq_config : t_wrn_mqueue_config :=
    (
      out_slot_count  => 3,
      out_slot_config => (
        0             => (width => 32, entries => 16),  -- RF stream (CPU0)
        1             => (width => 32, entries => 16),  -- Events (CPU1)
        2             => (width => 32, entries => 16),  -- Events (CPU2)
        others        => (0, 0)),

      in_slot_count  => 3,
      in_slot_config => (
        0            => (width => 32, entries => 16),  -- RF stream (CPU0)
        1            => (width => 32, entries => 16),  -- Events (CPU1)
        2            => (width => 32, entries => 16),  -- Events (CPU2)
        others => (0, 0)
        )
      );

  
  constant c_node_config : t_wr_node_config :=
    (
      app_id       => x"dd3f3c01",
      cpu_count    => 3,
      cpu_memsizes => (16384, 32768, 16384, 0, 0, 0, 0, 0),
      hmq_config   => c_hmq_config,
      rmq_config   => c_rmq_config,
		shared_mem_size => 1024
      );

  constant FPGA_VERSION        : std_logic_vector(15 downto 0) := X"0052";

  signal clk_sys    : std_logic;
  signal clk_125m   : std_logic;
  signal clk_oddr   : std_logic;
  signal buclk_rf8  : std_logic;
  signal rst_n      : std_logic;
  signal clk_xo_in  : std_logic;

  signal fmc_host_wb_out, fmc_dp_wb_out : t_wishbone_master_out_array(0 to 2);
  signal fmc_host_wb_in, fmc_dp_wb_in   : t_wishbone_master_in_array(0 to 2);
  signal fmc_host_irq                   : std_logic_vector(1 downto 0);

  constant c_d3s0_sdb_record : t_sdb_record       := f_sdb_embed_device(c_buclk_sdb, x"00010000");
  constant c_d3s1_sdb_record : t_sdb_record       := f_sdb_embed_device(c_D3S_SDB_DEVICE, x"00011000");
  constant c_d3s_vector      : t_wishbone_address := x"ffffffff";

  signal tm_link_up          : std_logic;
  signal tm_dac_value        : std_logic_vector(23 downto 0);
  signal tm_dac_wr           : std_logic_vector(0 downto 0);
  signal tm_clk_aux_lock_en  : std_logic_vector(0 downto 0) := (others => '0');
  signal tm_clk_aux_locked   : std_logic_vector(0 downto 0);
  signal tm_time_valid       : std_logic;
  signal tm_tai              : std_logic_vector(39 downto 0);
  signal tm_cycles           : std_logic_vector(27 downto 0);

  signal fmc0_clk_wr         : std_logic;
  signal t0_trig             : std_logic;
  signal master_bcl_pipe     : std_logic;

  signal debug : std_logic_vector(3 downto 0);

  constant c_slave_addr : t_wishbone_address_array(0 downto 0) :=
    ( 0 =>    x"00000000" );
  constant c_slave_mask : t_wishbone_address_array(0 downto 0) :=
    ( 0 =>    x"00000000" );

  signal fmc_wb_muxed_out : t_wishbone_master_out;
  signal fmc_wb_muxed_in : t_wishbone_master_in;

  signal clk_dds_synth      : std_logic;
  -------------------
  -- GG buclk
  -------------------
  signal  rst352            : std_logic;
  signal  tw_pulse          : std_logic;
  signal  buclk_352_lkd     : std_logic;
  signal  buclk_gun_o       : std_logic;
  signal  buclk_g8          : std_logic;
  signal  buclk_inj_o       : std_logic;
  signal  buclk_ext_o       : std_logic;
  signal  buclk_352         : std_logic;
  signal  buclk_352_o       : std_logic;
  signal  buclk_352_n       : std_logic;
  signal  buclk_fb          : std_logic;
--  signal  fmc0_clk_dds          : std_logic;
  signal  pps_o             : std_logic;
  signal  sync_inj_o        : std_logic;
  signal  rf_mast0_stamp    : std_logic_vector(63 downto 0);
  signal  rf_hot_ok         : std_logic;
  signal  rf_hot_pps        : std_logic_vector(63 downto 0);
  signal  rf_hot_paps       : std_logic_vector(63 downto 0);
  signal  rf_hot_pbps       : std_logic_vector(63 downto 0);
  signal  rf_counter          : std_logic_vector(RFTIMEWID-1 downto 0);
--  signal  rf_counter_stampin  : std_logic_vector(RFTIMEWID-1 downto 0);
  signal  rf_pps_stamp      : std_logic_vector(RFTIMEWID-1 downto 0);
  signal  rf_paps_stamp     : std_logic_vector(RFTIMEWID-1 downto 0);
  signal  rf_pbps_stamp     : std_logic_vector(RFTIMEWID-1 downto 0);
  signal  slave_bit         : std_logic;
  -- GG others
  signal  input_pol         : std_logic_vector(3 downto 0);       
  signal  input_ena         : std_logic_vector(3 downto 0);       
  signal  evt               : std_logic_vector(4 downto 1);       
  signal  gg_pulse          : std_logic;       
  signal  link_oko          : std_logic;       
  signal  phase_safe        : std_logic;       
  signal  pulbou            : buclk_pulse(g_pulses downto 1);
  signal  master_t0         : std_logic;
  signal  master_t0_seq     : std_logic;
  signal  master_start      : std_logic;
  signal  pll_352_rst       : std_logic;
  signal  slok_ack          : std_logic;
  signal  slok_nok          : std_logic;
  signal  slok_adj_0        : std_logic;
  signal  slok_adj_1        : std_logic;
  signal  slok_run          : std_logic;
  signal  slok_trig         : std_logic;
  signal  syncope           : std_logic;
  signal  tinrise           : std_logic_vector(3 downto 0);
  signal  srboo_pps_stamp   : std_logic_vector(31 downto 0);
  signal  master_srboo_pps  : std_logic_vector(31 downto 0);
  signal  out_mux           : std_logic_vector(1 downto 0);
  signal  rf_en             : std_logic;
  signal  rf_pps_rdy_bcc    : std_logic;
  signal  rt_d3s_run        : std_logic;
  signal  rt_d3s_locked     : std_logic;
  signal  inj_error         : std_logic;
  signal  paps              : std_logic;
  signal  papa              : std_logic;
  signal  pbps              : std_logic;
  signal  pbpa              : std_logic;
  signal  bu_pll_vcxo_sync_n_o      : std_logic;
  signal  d3s_pll_vcxo_sync_n_o     : std_logic;

  attribute keep : string;
  attribute keep of master_bcl_pipe   : signal is "true";

begin

-----------------------------------
-- GG put RFoE (clk_dds_synth) to top level
-----------------------------------
  U_Buf_CLK_DDS : IBUFGDS
    generic map (
      DIFF_TERM    => true,
      IBUF_LOW_PWR => false  -- Low power (TRUE) vs. performance (FALSE) setting for referenced
      )
    port map (
--      O  => fmc0_clk_dds,              -- Buffer output
      O  => clk_dds_synth,              -- Buffer output
      I  => fmc0_synth_clk_p_i,  -- Diff_p buffer input (connect directly to top-level port)
      IB => fmc0_synth_clk_n_i  -- Diff_n buffer input (connect directly to top-level port)
      );

-----------------------------------
-- GG put RFoE (clk_dds_synth) to top level
-----------------------------------
  U_Buf_CLK_RF : IBUFGDS
    generic map (
      DIFF_TERM    => true,
      IBUF_LOW_PWR => false  -- Low power (TRUE) vs. performance (FALSE) setting for referenced
      )
    port map (
      O  => clk_xo_in,        -- Buffer output
      I  => fmc0_rf_clk_p_i,  -- Diff_p buffer input (connect directly to top-level port)
      IB => fmc0_rf_clk_n_i   -- Diff_n buffer input (connect directly to top-level port)
      );

-----------------------------------
-- GG make 3 PPS for automatic unsync detection
-----------------------------------
process(clk_125m)        -- WR 125 MHz
  begin
    if clk_125m'event and clk_125m = '1' then
      if rst_n = '0' then
        pbps                 <= '0';
        paps                 <= '0';
        pbpa                 <= '0';
        papa                 <= '0';
      else
        papa                 <= pps_o;
        paps                 <= papa;
        pbpa                 <= paps;
        pbps                 <= pbpa;
      end if;
		end if;
  end process;

-----------------------------------
-- original section adapted
-----------------------------------
  U_Node_Template : spec_node_template
    generic map (
      g_fmc0_sdb                 => c_d3s0_sdb_record,
      g_fmc0_vic_vector          => c_d3s_vector,
      g_simulation               => g_simulation,
      g_with_white_rabbit        => true,
      g_wr_core_dpram_initf      => "",
      g_with_wr_phy              => true,
      g_double_wrnode_core_clock => false,
      g_system_clock_freq        => 62500000,
      g_wr_node_config           => c_node_config
    )
    port map (
      rst_n_sys_o          => rst_n,
      clk_sys_o            => clk_sys,
      clk_125m_o           => clk_125m,
      pps_o                => pps_o,

      clk_20m_vcxo_i       => clk_20m_vcxo_i,
      clk_125m_pllref_p_i  => clk_125m_pllref_p_i,
      clk_125m_pllref_n_i  => clk_125m_pllref_n_i,
      clk_125m_gtp_n_i     => clk_125m_gtp_n_i,
      clk_125m_gtp_p_i     => clk_125m_gtp_p_i,

      l_rst_n              => '1',
      led_red              => led_red,
      led_green            => link_oko,
      
      dac_sclk_o           => dac_sclk_o,
      dac_din_o            => dac_din_o,
      dac_cs1_n_o          => dac_cs1_n_o,
      dac_cs2_n_o          => dac_cs2_n_o,
      fmc_scl_b            => fmc_scl_b,
      fmc_sda_b            => fmc_sda_b,
      carrier_onewire_b    => carrier_onewire_b,
      fmc_prsnt_m2c_l_i    => fmc_prsnt_m2c_l_i,
      sfp_txp_o            => sfp_txp_o,
      sfp_txn_o            => sfp_txn_o,
      sfp_rxp_i            => sfp_rxp_i,
      sfp_rxn_i            => sfp_rxn_i,
      sfp_mod_def0_b       => sfp_mod_def0_b,
      sfp_mod_def1_b       => sfp_mod_def1_b,
      sfp_mod_def2_b       => sfp_mod_def2_b,
      sfp_rate_select_b    => sfp_rate_select_b,
      sfp_tx_fault_i       => sfp_tx_fault_i,
      sfp_tx_disable_o     => sfp_tx_disable_o,
      sfp_los_i            => sfp_los_i,
      uart_rxd_i           => uart_rxd_i,
      uart_txd_o           => uart_txd_o,

      spi_cs_n_o => spi_cs_n_o,
      spi_sclk_o => spi_sclk_o,
      spi_mosi_o => spi_mosi_o,
      spi_miso_i => spi_miso_i,
      
      fmc0_clk_aux_i       => fmc0_clk_wr,
      fmc0_host_irq_i      => '0',
      fmc0_host_wb_o       => fmc_host_wb_out(0),
      fmc0_host_wb_i       => fmc_host_wb_in(0),
      tm_link_up_o         => tm_link_up,

      dp_master_o(0)       => fmc_dp_wb_out(0),
      dp_master_o(1)       => fmc_dp_wb_out(1),
      dp_master_o(2)       => fmc_dp_wb_out(2),
      dp_master_i(0)       => fmc_dp_wb_in(0),
      dp_master_i(1)       => fmc_dp_wb_in(1),
      dp_master_i(2)       => fmc_dp_wb_in(2),

      tm_dac_value_o       => tm_dac_value,
      tm_dac_wr_o          => tm_dac_wr,
      tm_clk_aux_lock_en_i => tm_clk_aux_lock_en,
      tm_clk_aux_locked_o  => tm_clk_aux_locked,
      tm_time_valid_o      => tm_time_valid,
      tm_tai_o             => tm_tai,
      tm_cycles_o          => tm_cycles
    );

  U_DDS_Core0 : wr_d3s_core
    generic map (
      g_simulation       => g_simulation,
      g_sim_pps_period   => 1000)
    port map (
      clk_sys_i            => clk_sys,
      rst_n_i              => rst_n,
      clk_ref_i            => '0',
      clk_wr_o             => fmc0_clk_wr,
      tm_link_up_i         => tm_link_up,
      tm_time_valid_i      => tm_time_valid,
      tm_tai_i             => tm_tai,
      tm_cycles_i          => tm_cycles,
      tm_clk_aux_lock_en_o => tm_clk_aux_lock_en(0),
      tm_clk_aux_locked_i  => tm_clk_aux_locked(0),
      tm_dac_value_i       => tm_dac_value,
      tm_dac_wr_i          => tm_dac_wr(0),
      dac_n_o              => fmc0_dac_n_o,
      dac_p_o              => fmc0_dac_p_o,
      wr_ref_clk_n_i       => fmc0_wr_ref_clk_n_i,
      wr_ref_clk_p_i       => fmc0_wr_ref_clk_p_i,
      clk_dds_synth        => clk_dds_synth,
      clk_rf_in            => clk_xo_in,
      
      pll_sys_cs_n_o       => fmc0_pll_sys_cs_n_o,
      pll_sys_ld_i         => fmc0_pll_sys_ld_i,
      pll_sys_reset_n_o    => fmc0_pll_sys_reset_n_o,
      pll_sys_sync_n_o     => fmc0_pll_sys_sync_n_o,
      pll_vcxo_cs_n_o      => fmc0_pll_vcxo_cs_n_o,
      pll_vcxo_sync_n_o    => d3s_pll_vcxo_sync_n_o,
      pll_vcxo_status_i    => fmc0_pll_vcxo_status_i,
      pll_sclk_o           => fmc0_pll_sclk_o,
      pll_sdio_b           => fmc0_pll_sdio_b,
      pll_sdo_i            => fmc0_pll_sdo_i,
      pd_lockdet_i         => fmc0_pd_lockdet_i,
      pd_clk_o             => fmc0_pd_clk_o,
      pd_data_b            => fmc0_pd_data_b,
      pd_le_o              => fmc0_pd_le_o,
      adc_sdo_i            => fmc0_adc_sdo_i,
      adc_sck_o            => fmc0_adc_sck_o,
      adc_cnv_o            => fmc0_adc_cnv_o,
      adc_sdi_o            => fmc0_adc_sdi_o,
      delay_d_o            => open,
      delay_fb_i           => '0',
      delay_len_o          => open,
      delay_pulse_o        => open,
      onewire_b            => fmc0_onewire_b,
      wr_dac_sync_n_o      => fmc0_wr_dac_sync_n_o,
      wr_dac_din_o         => fmc0_wr_dac_din_o,
      wr_dac_sclk_o        => fmc0_wr_dac_sclk_o,
      rf_counter_valid_o       => open,
      rf_counter_o             => open,
      rf_counter_overflow_p_o  => open,
      rf_phase_safe_o          => phase_safe,
      rt_d3s_running_o         => rt_d3s_run,
      rt_d3s_locked_o          => rt_d3s_locked,
      slave_i              => fmc_dp_wb_out(0),
      slave_o              => fmc_dp_wb_in(0),
      debug_o              => debug
      );
                              
-- vcxo (AD95xx) sync is active low. Set at 1 in wr_d3s_core
syncope    <= d3s_pll_vcxo_sync_n_o and not(bu_pll_vcxo_sync_n_o);
-----------------------------------
-- bcc_core input and instanciation
-----------------------------------

with out_mux select
	t0_trig    <=     trig_lemo(3) when "11",
                    trig_lemo(2) when "10",
                    trig_sma(1) when "01",
                    trig_sma(0) when others;
  
  evt_controller : evt_core
    generic map (
      g_interface_mode        => PIPELINED,
      g_address_granularity   => BYTE)
    port map (
      clk_sys_i            => clk_sys,
      rst_n_i              => rst_n,
      clk_dds_i            => clk_dds_synth,
      slave_bit_i          => slave_bit,
      input_pol_i          => input_pol,
      input_ena_i          => input_ena,
--      rf_counter_i         => FILLRFTIMEWID & rf_counter_stampin,
      rf_counter_i         => FILLRFTIMEWID & rf_counter,
      tx_p_i               => trig_lemo & trig_sma,
      tinrise_o            => tinrise,
      evt_o                => evt,
      slave_i              => fmc_dp_wb_out(2),
      slave_o              => fmc_dp_wb_in(2)
    );

  buclk_controller : bcc_core
    generic map (
      g_interface_mode        => PIPELINED,
      g_address_granularity   => BYTE)
    port map (
      clk_sys_i            => clk_sys,
      rst_n_i              => rst_n,
      clk_dds_i            => clk_dds_synth,
--      clk_dds_i            => buclk_rf8,
      clk_xo_i             => clk_xo_in,
      rf_counter_i         => FILLRFTIMEWID & rf_counter,
      rfpps_counter_i      => FILLRFTIMEWID & rf_pps_stamp,
      rfpaps_i             => FILLRFTIMEWID & rf_paps_stamp,
      rfpbps_i             => FILLRFTIMEWID & rf_pbps_stamp,
      t0_p_i               => t0_trig,
      pps_i                => pps_o,
      pps_p_i              => rf_pps_rdy_bcc,
      slok_run_i           => slok_run,
      srboo_pps_stamp_i    => srboo_pps_stamp,
      t0_led_o             => leds_n_o(0),
      slave_bit_o          => slave_bit,
      master_start_o       => master_start,
      rfpps_recv_o         => rf_hot_ok,
      slok_recv_o          => slok_trig,
      slok_ack_o           => slok_ack,
      slok_nok_o           => slok_nok,
      slok_adj_0_o         => slok_adj_0,
      slok_adj_1_o         => slok_adj_1,
      t0_start_o           => master_t0,
      seq_start_o          => master_t0_seq,
      t0_rf_stamp_o        => rf_mast0_stamp,
      master_bcl_pipe_o    => master_bcl_pipe,
      master_rfpps_recv_o  => rf_hot_pps,
      master_rfpaps_o      => rf_hot_paps,
      master_rfpbps_o      => rf_hot_pbps,
      master_srboo_recv_o  => master_srboo_pps,
      slave_i              => fmc_dp_wb_out(1),
      slave_o              => fmc_dp_wb_in(1)
    );

-----------------------------------
-- GG bunch clock main timer
-----------------------------------
  buclk_sequencer : buclk
    generic map (
      g_pulses                 => g_pulses
    )
    port map (
      buclk_sys_i              => buclk_rf8,
      buclk_352_i              => buclk_352,
      clk_sys_i                => clk_sys,
      clk_125_i                => clk_125m,
      rst_n_i                  => rst_n,

      slave_i                  => fmc_host_wb_out(0),
      slave_o                  => fmc_host_wb_in(0),

      slave_bit_i              => slave_bit,
      evt_i                    => evt,

      fpga_version_i           => FPGA_VERSION,
      link_ok_i                => link_oko,
      pps_i                    => pps_o,
      paps_i                   => paps,
      pbps_i                   => pbps,
      pll_352_lkd_i            => buclk_352_lkd,
      dds_pd_lkdet_i           => fmc0_pd_lockdet_i,

      rf_mast0_stamp_i         => rf_mast0_stamp(RFTIMEWID-1 downto 0),
      rf_hot_pps_i             => rf_hot_pps(RFTIMEWID-1 downto 0),
      rf_hot_paps_i            => rf_hot_paps(RFTIMEWID-1 downto 0),
      rf_hot_pbps_i            => rf_hot_pbps(RFTIMEWID-1 downto 0),
      master_srboo_pps_i       => master_srboo_pps,
      master_t0_i              => master_t0,
      master_t0_seq_i          => master_t0_seq,
      master_start_i           => master_start,
      master_bcl_pipe_i        => master_bcl_pipe,
      rf_hot_ok_i              => rf_hot_ok,
      rf_phase_safe_i          => phase_safe,
      slok_ack_i               => slok_ack,
      slok_nok_i               => slok_nok,
      slok_adj_0_i             => slok_adj_0,
      slok_adj_1_i             => slok_adj_1,
      slok_trig_i              => slok_trig,
      pll_dld_i                => fmc0_pll_vcxo_status_i,

      tinrise_i                => tinrise,

      rt_d3s_run_i             => rt_d3s_run,
      rt_d3s_locked_i          => rt_d3s_locked,

      input_pol_o              => input_pol,
      input_ena_o              => input_ena,
      pulse_o                  => pulbou,
      rf_en_o                  => rf_en,
      out_mux_o                => out_mux,
      rst352_o                 => rst352,
      rf_pps_rdy_bcc_o         => rf_pps_rdy_bcc,
      sync_95xx_o              => bu_pll_vcxo_sync_n_o,
      slok_run_o               => slok_run,
      srboo_pps_stamp_o        => srboo_pps_stamp,

      buclk_injerr_o           => inj_error,

      rf_pps_stamp_o           => rf_pps_stamp,
      rf_paps_stamp_o          => rf_paps_stamp,
      rf_pbps_stamp_o          => rf_pbps_stamp,
--      rf_stampin_o             => rf_counter_stampin,
      rf_maincnt_o             => rf_counter
    );

-- GG buclk : PLL used for RF
  buclk_pll : PLL_BASE
    generic map (
      BANDWIDTH          => "OPTIMIZED",
      CLK_FEEDBACK       => "CLKFBOUT", 
      COMPENSATION       => "INTERNAL",
      DIVCLK_DIVIDE      => 1,
      CLKFBOUT_MULT      => 16,
      CLKFBOUT_PHASE     => 0.000,
      CLKOUT0_DIVIDE     => 2,          -- ((RF/8)*16)/2=RF
      CLKOUT0_PHASE      => 0.000,
      CLKOUT0_DUTY_CYCLE => 0.500,
      CLKOUT1_DIVIDE     => 2,          
      CLKOUT1_PHASE      => 180.0,
      CLKOUT1_DUTY_CYCLE => 0.500,
      CLKOUT2_DIVIDE     => 16,         -- RF/8
      CLKOUT2_PHASE      => 0.000,
      CLKOUT2_DUTY_CYCLE => 0.500,
      CLKIN_PERIOD       => 22.0,
      REF_JITTER         => 0.016)
    port map (
      CLKFBOUT => buclk_fb,
      CLKOUT0  => buclk_352_o,
      CLKOUT1  => buclk_352_n,
      CLKOUT2  => clk_oddr,
      CLKOUT3  => open,
      CLKOUT4  => open,
      CLKOUT5  => open,
      LOCKED   => buclk_352_lkd,
      RST      => pll_352_rst,
      CLKFBIN  => buclk_fb,
      CLKIN    => clk_dds_synth    -- DAC out direct
      );

  pll_352_rst <= not(rf_en) or rst352;
  
  bunch_clk_352 : BUFG
    port map (
    O => buclk_352,
    I => buclk_352_o
    );
  
  clk_rf8_oddr : BUFG
    port map (
    O => buclk_rf8,
    I => clk_oddr
    );
  
-- outputs lookup table, taken care of in .ucf file
-- PULSES_0   -------> FRONT_PANEL_OUT_8    --> LA29-N - BANK#2 - Y18
-- PULSES_1   -------> FRONT_PANEL_OUT_4    --> LA28-N - BANK#2 - W15
-- PULSES_2   -------> FRONT_PANEL_OUT_5    --> LA31-P - BANK#0 - D17
-- PULSES_3   -------> FRONT_PANEL_OUT_6    --> LA30-P - BANK#2 - V17
-- PULSES_4   -------> FRONT_PANEL_OUT_7    --> LA31-N - BANK#0 - C18
-- PULSES_5   -------> FRONT_PANEL_OUT_9    --> LA30-N - BANK#2 - W18
-- PULSES_6   -------> FRONT_PANEL_OUT_10   --> LA33-P - BANK#0 - C19
-- PULSES_7   -------> FRONT_PANEL_OUT_11   --> LA32-P - BANK#0 - B20
-- PULSES_8   -------> FRONT_PANEL_OUT_12   --> LA33-N - BANK#0 - A19
-- PULSES_9   -------> FRONT_PANEL_OUT_1    --> LA26-N - BANK#2 - AB17
-- PULSES_10  -------> FRONT_PANEL_OUT_2    --> LA10-P - BANK#2 - AA8
-- PULSES_11  -------> FRONT_PANEL_OUT_3    --> LA10-N - BANK#2 - AB8

sinkrotroneto : for K in 1 to 3 generate
sinkoddr1 : ODDR2
    generic map (
      DDR_ALIGNMENT      => "C0",
      INIT               => '0',
      SRTYPE             => "ASYNC")
    port map (
      Q               => fp_out(K),
      C0              => buclk_352,
      C1              => '0',
      CE              => '1',
      D0              => pulbou(K),
      D1              => pulbou(K),
      R               => '0',
      S               => '0'
      );
end generate;
--  fp_out(3)           <= pulbou(3);
--  fp_out(2)           <= pulbou(2);
--  fp_out(1)           <= pulbou(1);

sinkrotronego : for K in 4 to 12 generate
sinkoddr4 : ODDR2
    generic map (
      DDR_ALIGNMENT      => "C0",
      INIT               => '0',
      SRTYPE             => "ASYNC")
    port map (
      Q               => fp_out(K),
      C0              => buclk_rf8,
      C1              => '0',
      CE              => '1',
      D0              => pulbou(K),
      D1              => pulbou(K),
      R               => '0',
      S               => '0'
      );
end generate;

sinksync : ODDR2
    generic map (
      DDR_ALIGNMENT      => "C0",
      INIT               => '1',
      SRTYPE             => "ASYNC")
    port map (
      Q               => fmc0_pll_vcxo_sync_n_o,
      C0              => buclk_rf8,
      C1              => '0',
      CE              => '1',
      D0              => syncope,
      D1              => syncope,
      R               => '0',
      S               => not(buclk_352_lkd)
      );
                
-- GG intercepted link_oko to LED_GREEN for WR_OK management over bunch clock logic		
  LED_GREEN        <= link_oko;
  
end rtl;



