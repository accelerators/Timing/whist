action = "synthesis"
target="xilinx"
fetchto = "../ip_cores"

syn_device = "xc6slx100t"
syn_grade = "-3"
syn_package = "fgg484"
syn_top = "top"
syn_project = "kairos.xise"

modules = { "local" : [ "../top" ] }
