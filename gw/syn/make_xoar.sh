#!/bin/bash
#OAR -l {mem_core_mb>4000 and cpu_vendor='INTEL'}/core=4,walltime=3
#OAR --stdout xoar_stdout
#OAR --stderr xoar_stderr

. ~/.profile_cae

LOCAL_DIR=`pwd`

#synthetize, implement and create bitstream
xtclsh $LOCAL_DIR/xrun.tcl

# Fill CPUs program to blockRAM
data2mem -bm cpus_progmem_bd.bmm -bd wrc.elf tag lm32_wrpc_memory -bd rt-d3s.elf tag lm32_wrnc_cpu0_memory -bd rt-bcc.elf tag lm32_wrnc_cpu1_memory -bd rt-evt.elf tag lm32_wrnc_cpu2_memory -bt top.bit -o b 00_whist.bit

# Generate FLASH file
promgen -w -p mcs -c FF -o 00_whist -s 4096 -u 0000 00_whist.bit -spi

# Generate binary file for standalone mode (program flash through network)
# This must be done from the .mcs file due to .bit header size changing
# (with a constant header size, dd could be used by skipping header bytes)
promgen -w -p bin -r 00_whist.mcs -o 00_whist_binary
mv 00_whist_binary.bin 00_whist.bin
rm 00_whist_binary.*
