-- -*- Mode: LUA; tab-width: 2 -*-

peripheral {
  name = "EVenT controller (EVT) WB Slave";
  hdl_entity = "evt_wb_slave";
  prefix = "evt";

  reg {
    prefix = "CSR";
    name = "Control & Status Register";

    field {
      name = "Trigger output 0 following an event received from network";
      prefix = "OUT0_ON_EVT";
      description = "write 1: an event has been broadcast by a node of the network, and this node is registered to generate a trigger from this event on output 0. The stamp delayed registers 0 are updated.";
      clock = "clk_dds_i";
      type = MONOSTABLE;
    };

    field {
      name = "Trigger output 1 following an event received from network";
      prefix = "OUT1_ON_EVT";
      description = "write 1: an event has been broadcast by a node of the network, and this node is registered to generate a trigger from this event on output 1. The stamp delayed registers 1 are updated.";
      clock = "clk_dds_i";
      type = MONOSTABLE;
    };

    field {
      name = "Trigger output 2 following an event received from network";
      prefix = "OUT2_ON_EVT";
      description = "write 1: an event has been broadcast by a node of the network, and this node is registered to generate a trigger from this event on output 2. The stamp delayed registers 2 are updated.";
      clock = "clk_dds_i";
      type = MONOSTABLE;
    };

    field {
      name = "Trigger output 3 following an event received from network";
      prefix = "OUT3_ON_EVT";
      description = "write 1: an event has been broadcast by a node of the network, and this node is registered to generate a trigger from this event on output 3. The stamp delayed registers 3 are updated.";
      clock = "clk_dds_i";
      type = MONOSTABLE;
    };

    field {
      name = "IN0 Arm";
      prefix = "ARM_IN0";
      description = "arm the external trigger on Input 0";
      type = MONOSTABLE;
      clock = "clk_dds_i";
    };

    field {
      name = "IN1 Arm";
      prefix = "ARM_IN1";
      description = "arm the external trigger on Input 1";
      type = MONOSTABLE;
      clock = "clk_dds_i";
    };

    field {
      name = "IN2 Arm";
      prefix = "ARM_IN2";
      description = "arm the external trigger on Input 2";
      type = MONOSTABLE;
      clock = "clk_dds_i";
    };

    field {
      name = "IN3 Arm";
      prefix = "ARM_IN3";
      description = "arm the external trigger on Input 3";
      type = MONOSTABLE;
      clock = "clk_dds_i";
    };

    field {
      name = "IN0 Done";
      prefix = "DONE_IN0";
      description = "set to 1 when external trigger on Input 0 has been latched";
      type = BIT;
      clock = "clk_dds_i";
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };

    field {
      name = "IN1 Done";
      prefix = "DONE_IN1";
      description = "set to 1 when external trigger on Input 1 has been latched";
      type = BIT;
      clock = "clk_dds_i";
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };

    field {
      name = "IN2 Done";
      prefix = "DONE_IN2";
      description = "set to 1 when external trigger on Input 2 has been latched";
      type = BIT;
      clock = "clk_dds_i";
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };

    field {
      name = "IN3 Done";
      prefix = "DONE_IN3";
      description = "set to 1 when external trigger on Input 3 has been latched";
      type = BIT;
      clock = "clk_dds_i";
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };

  reg {
    name = "IN0 Stamped - MSB";
    prefix = "IN0_STAMPED_MSB";
    
    field {
      name = "IN0 external trigger stamped (MSB RF cycles) to be broadcast";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };
  reg {
    name = "IN0 Stamped - LSB";
    prefix = "IN0_STAMPED_LSB";
    
    field {
      name = "IN0 external trigger stamped (LSB RF cycles) to be broadcast";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };

  reg {
    name = "IN1 Stamped - MSB";
    prefix = "IN1_STAMPED_MSB";
    
    field {
      name = "IN1 external trigger stamped (MSB RF cycles) to be broadcast";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };
  reg {
    name = "IN1 Stamped - LSB";
    prefix = "IN1_STAMPED_LSB";
    
    field {
      name = "IN1 external trigger stamped (LSB RF cycles) to be broadcast";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };

  reg {
    name = "IN2 Stamped - MSB";
    prefix = "IN2_STAMPED_MSB";
    
    field {
      name = "IN2 external trigger stamped (MSB RF cycles) to be broadcast";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };
  reg {
    name = "IN2 Stamped - LSB";
    prefix = "IN2_STAMPED_LSB";
    
    field {
      name = "IN2 external trigger stamped (LSB RF cycles) to be broadcast";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };

  reg {
    name = "IN3 Stamped - MSB";
    prefix = "IN3_STAMPED_MSB";
    
    field {
      name = "IN3 external trigger stamped (MSB RF cycles) to be broadcast";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };
  reg {
    name = "IN3 Stamped - LSB";
    prefix = "IN3_STAMPED_LSB";
    
    field {
      name = "IN3 external trigger stamped (LSB RF cycles) to be broadcast";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_ONLY;
      access_dev = WRITE_ONLY;
    };
  };

  reg {
    name = "OUT0 Stamp delayed - MSB";
    prefix = "OUT0_STAMP_DLY_MSB";
    
    field {
      name = "RF stamp (MSB RF cycles) delayed after event received. This value is compared by HDL device before generating OUT0 for BUCLK. The OUT0_ON_EVT monostable must be generated after these registers (MSB & LSB) have been written.";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };
  reg {
    name = "OUT0 Stamp delayed - LSB";
    prefix = "OUT0_STAMP_DLY_LSB";
    
    field {
      name = "RF stamp (LSB RF cycles) delayed after event received. This value is compared by HDL device before generating OUT0 for BUCLK. The OUT0_ON_EVT monostable must be generated after these registers (MSB & LSB) have been written.";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };

  reg {
    name = "OUT1 Stamp delayed - MSB";
    prefix = "OUT1_STAMP_DLY_MSB";
    
    field {
      name = "RF stamp (MSB RF cycles) delayed after event received. This value is compared by HDL device before generating OUT1 for BUCLK. The OUT1_ON_EVT monostable must be generated after these registers (MSB & LSB) have been written.";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };
  reg {
    name = "OUT1 Stamp delayed - LSB";
    prefix = "OUT1_STAMP_DLY_LSB";
    
    field {
      name = "RF stamp (LSB RF cycles) delayed after event received. This value is compared by HDL device before generating OUT1 for BUCLK. The OUT1_ON_EVT monostable must be generated after these registers (MSB & LSB) have been written.";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };

  reg {
    name = "OUT2 Stamp delayed - MSB";
    prefix = "OUT2_STAMP_DLY_MSB";
    
    field {
      name = "RF stamp (MSB RF cycles) delayed after event received. This value is compared by HDL device before generating OUT2 for BUCLK.  The OUT2_ON_EVT monostable must be generated after these registers (MSB & LSB) have been written.";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };
  reg {
    name = "OUT2 Stamp delayed - LSB";
    prefix = "OUT2_STAMP_DLY_LSB";
    
    field {
      name = "RF stamp (LSB RF cycles) delayed after event received. This value is compared by HDL device before generating OUT2 for BUCLK. The OUT2_ON_EVT monostable must be generated after these registers (MSB & LSB) have been written.";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };

  reg {
    name = "OUT3 Stamp delayed - MSB";
    prefix = "OUT3_STAMP_DLY_MSB";
    
    field {
      name = "RF stamp (MSB RF cycles) delayed after event received. This value is compared by HDL device before generating OUT3 for BUCLK. The OUT3_ON_EVT monostable must be generated after these registers (MSB & LSB) have been written.";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };
  reg {
    name = "OUT3 Stamp delayed - LSB";
    prefix = "OUT3_STAMP_DLY_LSB";
    
    field {
      name = "RF stamp (LSB RF cycles) delayed after event received. This value is compared by HDL device before generating OUT3 for BUCLK. The OUT3_ON_EVT monostable must be generated after these registers (MSB & LSB) have been written.";
      clock = "clk_dds_i";
      type = SLV;
      size = 32;
      access_bus = READ_WRITE;
      access_dev = READ_ONLY;
    };
  };
};
