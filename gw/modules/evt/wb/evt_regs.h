/*
  Register definitions for slave core: EVenT controller (EVT) WB Slave

  * File           : evt_regs.h
  * Author         : auto-generated by wbgen2 from evt_wb_slave.wb
  * Created        : Thu Apr 11 17:23:56 2019
  * Standard       : ANSI C

    THIS FILE WAS GENERATED BY wbgen2 FROM SOURCE FILE evt_wb_slave.wb
    DO NOT HAND-EDIT UNLESS IT'S ABSOLUTELY NECESSARY!

*/

#ifndef __WBGEN2_REGDEFS_EVT_WB_SLAVE_WB
#define __WBGEN2_REGDEFS_EVT_WB_SLAVE_WB

#include <inttypes.h>

#if defined( __GNUC__)
#define PACKED __attribute__ ((packed))
#else
#error "Unsupported compiler?"
#endif

#ifndef __WBGEN2_MACROS_DEFINED__
#define __WBGEN2_MACROS_DEFINED__
#define WBGEN2_GEN_MASK(offset, size) (((1<<(size))-1) << (offset))
#define WBGEN2_GEN_WRITE(value, offset, size) (((value) & ((1<<(size))-1)) << (offset))
#define WBGEN2_GEN_READ(reg, offset, size) (((reg) >> (offset)) & ((1<<(size))-1))
#define WBGEN2_SIGN_EXTEND(value, bits) (((value) & (1<<bits) ? ~((1<<(bits))-1): 0 ) | (value))
#endif


/* definitions for register: Control & Status Register */

/* definitions for field: Trigger output 0 following an event received from network in reg: Control & Status Register */
#define EVT_CSR_OUT0_ON_EVT                   WBGEN2_GEN_MASK(0, 1)

/* definitions for field: Trigger output 1 following an event received from network in reg: Control & Status Register */
#define EVT_CSR_OUT1_ON_EVT                   WBGEN2_GEN_MASK(1, 1)

/* definitions for field: Trigger output 2 following an event received from network in reg: Control & Status Register */
#define EVT_CSR_OUT2_ON_EVT                   WBGEN2_GEN_MASK(2, 1)

/* definitions for field: Trigger output 3 following an event received from network in reg: Control & Status Register */
#define EVT_CSR_OUT3_ON_EVT                   WBGEN2_GEN_MASK(3, 1)

/* definitions for field: IN0 Arm in reg: Control & Status Register */
#define EVT_CSR_ARM_IN0                       WBGEN2_GEN_MASK(4, 1)

/* definitions for field: IN1 Arm in reg: Control & Status Register */
#define EVT_CSR_ARM_IN1                       WBGEN2_GEN_MASK(5, 1)

/* definitions for field: IN2 Arm in reg: Control & Status Register */
#define EVT_CSR_ARM_IN2                       WBGEN2_GEN_MASK(6, 1)

/* definitions for field: IN3 Arm in reg: Control & Status Register */
#define EVT_CSR_ARM_IN3                       WBGEN2_GEN_MASK(7, 1)

/* definitions for field: IN0 Done in reg: Control & Status Register */
#define EVT_CSR_DONE_IN0                      WBGEN2_GEN_MASK(8, 1)

/* definitions for field: IN1 Done in reg: Control & Status Register */
#define EVT_CSR_DONE_IN1                      WBGEN2_GEN_MASK(9, 1)

/* definitions for field: IN2 Done in reg: Control & Status Register */
#define EVT_CSR_DONE_IN2                      WBGEN2_GEN_MASK(10, 1)

/* definitions for field: IN3 Done in reg: Control & Status Register */
#define EVT_CSR_DONE_IN3                      WBGEN2_GEN_MASK(11, 1)

/* definitions for register: IN0 Stamped - MSB */

/* definitions for register: IN0 Stamped - LSB */

/* definitions for register: IN1 Stamped - MSB */

/* definitions for register: IN1 Stamped - LSB */

/* definitions for register: IN2 Stamped - MSB */

/* definitions for register: IN2 Stamped - LSB */

/* definitions for register: IN3 Stamped - MSB */

/* definitions for register: IN3 Stamped - LSB */

/* definitions for register: OUT0 Stamp delayed - MSB */

/* definitions for register: OUT0 Stamp delayed - LSB */

/* definitions for register: OUT1 Stamp delayed - MSB */

/* definitions for register: OUT1 Stamp delayed - LSB */

/* definitions for register: OUT2 Stamp delayed - MSB */

/* definitions for register: OUT2 Stamp delayed - LSB */

/* definitions for register: OUT3 Stamp delayed - MSB */

/* definitions for register: OUT3 Stamp delayed - LSB */
/* [0x0]: REG Control & Status Register */
#define EVT_REG_CSR 0x00000000
/* [0x4]: REG IN0 Stamped - MSB */
#define EVT_REG_IN0_STAMPED_MSB 0x00000004
/* [0x8]: REG IN0 Stamped - LSB */
#define EVT_REG_IN0_STAMPED_LSB 0x00000008
/* [0xc]: REG IN1 Stamped - MSB */
#define EVT_REG_IN1_STAMPED_MSB 0x0000000c
/* [0x10]: REG IN1 Stamped - LSB */
#define EVT_REG_IN1_STAMPED_LSB 0x00000010
/* [0x14]: REG IN2 Stamped - MSB */
#define EVT_REG_IN2_STAMPED_MSB 0x00000014
/* [0x18]: REG IN2 Stamped - LSB */
#define EVT_REG_IN2_STAMPED_LSB 0x00000018
/* [0x1c]: REG IN3 Stamped - MSB */
#define EVT_REG_IN3_STAMPED_MSB 0x0000001c
/* [0x20]: REG IN3 Stamped - LSB */
#define EVT_REG_IN3_STAMPED_LSB 0x00000020
/* [0x24]: REG OUT0 Stamp delayed - MSB */
#define EVT_REG_OUT0_STAMP_DLY_MSB 0x00000024
/* [0x28]: REG OUT0 Stamp delayed - LSB */
#define EVT_REG_OUT0_STAMP_DLY_LSB 0x00000028
/* [0x2c]: REG OUT1 Stamp delayed - MSB */
#define EVT_REG_OUT1_STAMP_DLY_MSB 0x0000002c
/* [0x30]: REG OUT1 Stamp delayed - LSB */
#define EVT_REG_OUT1_STAMP_DLY_LSB 0x00000030
/* [0x34]: REG OUT2 Stamp delayed - MSB */
#define EVT_REG_OUT2_STAMP_DLY_MSB 0x00000034
/* [0x38]: REG OUT2 Stamp delayed - LSB */
#define EVT_REG_OUT2_STAMP_DLY_LSB 0x00000038
/* [0x3c]: REG OUT3 Stamp delayed - MSB */
#define EVT_REG_OUT3_STAMP_DLY_MSB 0x0000003c
/* [0x40]: REG OUT3 Stamp delayed - LSB */
#define EVT_REG_OUT3_STAMP_DLY_LSB 0x00000040
#endif
