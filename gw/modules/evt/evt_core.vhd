library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

use work.wishbone_pkg.all;
use work.evt_wbgen2_pkg.all;
use work.gencores_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity evt_core is
  generic (
    g_interface_mode      : t_wishbone_interface_mode      := CLASSIC;
    g_address_granularity : t_wishbone_address_granularity := WORD);
  port (
    -- Clocks & resets
    clk_sys_i           : in std_logic;
    rst_n_i             : in std_logic;

    -- DDS synth clock (RF/8) to synchronize trigger
    clk_dds_i           : in std_logic;

    slave_bit_i         : in std_logic;
    input_pol_i         : in std_logic_vector(3 downto 0);
    input_ena_i         : in std_logic_vector(3 downto 0);
    
    -- cnt_bu44 counter for trigger snapshot
    rf_counter_i        : in std_logic_vector(63 downto 0);
    -- Tx trigger inputs
    tx_p_i              : in std_logic_vector(3 downto 0);

-------------------------------------------------
-- event monostables
-------------------------------------------------
    tinrise_o           : out std_logic_vector(3 downto 0);
    evt_o               : out std_logic_vector(4 downto 1);
-------------------------------------------------
-- Core control
-------------------------------------------------
    slave_i             : in  t_wishbone_slave_in;
    slave_o             : out t_wishbone_slave_out
    );

end evt_core;

architecture behavioral of evt_core is

  component evt_wb_slave is
    port (
      rst_n_i    : in  std_logic;
      clk_sys_i  : in  std_logic;
      wb_adr_i   : in  std_logic_vector(4 downto 0);
      wb_dat_i   : in  std_logic_vector(31 downto 0);
      wb_dat_o   : out std_logic_vector(31 downto 0);
      wb_cyc_i   : in  std_logic;
      wb_sel_i   : in  std_logic_vector(3 downto 0);
      wb_stb_i   : in  std_logic;
      wb_we_i    : in  std_logic;
      wb_ack_o   : out std_logic;
      wb_stall_o : out std_logic;
      clk_dds_i  : in  std_logic;
      regs_i     : in  t_evt_in_registers;
      regs_o     : out t_evt_out_registers);
  end component evt_wb_slave;

  -- REGS
  signal regs_in  : t_evt_in_registers  := c_evt_in_registers_init_value;
  signal regs_out : t_evt_out_registers := c_evt_out_registers_init_value;

  -- Triggers
  signal tx_armed          : std_logic_vector(3 downto 0);
  signal tx_p              : std_logic_vector(3 downto 0);
  signal do_evt            : std_logic_vector(3 downto 0);
  signal evt               : std_logic_vector(3 downto 0);
  signal csr_arm_in        : std_logic_vector(3 downto 0);
  signal csr_done_in       : std_logic_vector(3 downto 0);
  signal csr_out_on_evt    : std_logic_vector(3 downto 0);
  type tampin is array(3 downto 0) of std_logic_vector(31 downto 0); 
  type tampou is array(3 downto 0) of std_logic_vector(63 downto 0); 
  signal stampon_hi        : tampin;
  signal stampon_lo        : tampin;
  signal out_stamp         : tampou;
  
attribute keep : string;
attribute keep of stampon_hi     : signal is "true";
attribute keep of stampon_lo     : signal is "true";

begin  -- behavioral

  U_WB_Slave : evt_wb_slave
    port map (
      rst_n_i    => rst_n_i,
      clk_sys_i  => clk_sys_i,
      wb_adr_i   => slave_i.adr(6 downto 2),
      wb_dat_i   => slave_i.dat,
      wb_dat_o   => slave_o.dat,
      wb_cyc_i   => slave_i.cyc,
      wb_sel_i   => slave_i.sel,
      wb_stb_i   => slave_i.stb,
      wb_we_i    => slave_i.we,
      wb_ack_o   => slave_o.ack,
      wb_stall_o => slave_o.stall,
      clk_dds_i  => clk_dds_i,
      regs_i     => regs_in,
      regs_o     => regs_out
      );

  slave_o.err    <= '0';
  slave_o.rty    <= '0';

  tx_sync : for K in 0 to 3 generate
  U_Sync_Trigger : gc_sync_ffs
    port map (
      clk_i    => clk_dds_i,
      rst_n_i  => '1',
      data_i   => (tx_p_i(K) xor input_pol_i(K)) and input_ena_i(K),
      ppulse_o => tx_p(K)
      );
  end generate;

  tinrise_o        <= tx_p;

  csr_arm_in <= regs_out.csr_arm_in3_o & regs_out.csr_arm_in2_o & regs_out.csr_arm_in1_o & regs_out.csr_arm_in0_o;

  -- stamp 4 inputs
  timbre : for K in 0 to 3 generate
  process(clk_dds_i)
  begin
    if rising_edge(clk_dds_i) then
      if rst_n_i = '0' or slave_bit_i = '0' then
        tx_armed(K)                  <= '0';
        csr_done_in(K)               <= '0';        
      else
        if(csr_arm_in(K) = '1') then
          tx_armed(K)                <= '1';
          csr_done_in(K)             <= '0';
        elsif (tx_p(K) = '1' and tx_armed(K) = '1') then
          tx_armed(K)                <= '0';
          stampon_hi(K)              <= rf_counter_i(63 downto 32);
          stampon_lo(K)              <= rf_counter_i(31 downto 0);
          csr_done_in(K)             <= '1';
        end if;
      end if;
    end if;
  end process;
  end generate;

  regs_in.csr_done_in3_i       <= csr_done_in(3);
  regs_in.csr_done_in2_i       <= csr_done_in(2);
  regs_in.csr_done_in1_i       <= csr_done_in(1);
  regs_in.csr_done_in0_i       <= csr_done_in(0);
  regs_in.in0_stamped_msb_i    <= stampon_hi(0);
  regs_in.in0_stamped_lsb_i    <= stampon_lo(0);
  regs_in.in1_stamped_msb_i    <= stampon_hi(1);
  regs_in.in1_stamped_lsb_i    <= stampon_lo(1);
  regs_in.in2_stamped_msb_i    <= stampon_hi(2);
  regs_in.in2_stamped_lsb_i    <= stampon_lo(2);
  regs_in.in3_stamped_msb_i    <= stampon_hi(3);
  regs_in.in3_stamped_lsb_i    <= stampon_lo(3);

  
  out_stamp(0) <= regs_out.out0_stamp_dly_msb_o & regs_out.out0_stamp_dly_lsb_o;
  out_stamp(1) <= regs_out.out1_stamp_dly_msb_o & regs_out.out1_stamp_dly_lsb_o;
  out_stamp(2) <= regs_out.out2_stamp_dly_msb_o & regs_out.out2_stamp_dly_lsb_o;
  out_stamp(3) <= regs_out.out3_stamp_dly_msb_o & regs_out.out3_stamp_dly_lsb_o;

  csr_out_on_evt <= regs_out.csr_out3_on_evt_o & regs_out.csr_out2_on_evt_o & regs_out.csr_out1_on_evt_o & regs_out.csr_out0_on_evt_o;

  -- receive calculated delay then trig event for 4 outputs
  hit_out : for K in 0 to 3 generate
  process(clk_dds_i)
  begin
    if rising_edge(clk_dds_i) then
      if rst_n_i = '0' then
        evt(K)                     <= '0';
        do_evt(K)                  <= '0';
      else
        if csr_out_on_evt(K) = '1' then   -- latch monostable
          do_evt(K)                <= '1';
        elsif evt(K) = '1' then
          do_evt(K)                <= '0';
        end if;
        if do_evt(K) = '1' and (out_stamp(K) = rf_counter_i) then   -- wait for hit
          evt(K)         <= '1';
        else
          evt(K)         <= '0';         -- monostable
        end if;
      end if;
    end if;
  end process;
  end generate;

  pulsiones : for K in 0 to 3 generate
  evt_o(K+1)             <=  evt(K);
  end generate;

end behavioral;
