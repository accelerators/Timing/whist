library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

use work.wishbone_pkg.all;
use work.bcc_wbgen2_pkg.all;
use work.gencores_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity bcc_core is
  generic (
    g_interface_mode      : t_wishbone_interface_mode      := CLASSIC;
    g_address_granularity : t_wishbone_address_granularity := WORD);
  port (
    -- Clocks & resets
    clk_sys_i           : in std_logic;
    rst_n_i             : in std_logic;

    -- DDS synth clock (RF/8) to synchronize trigger
    clk_dds_i           : in std_logic;
    -- RF vcxo through AD9510
    clk_xo_i            : in std_logic;

    -- RF Counter for trigger snapshot
    rf_counter_i        : in std_logic_vector(63 downto 0);
    -- RF@PPS Counter
    rfpps_counter_i        : in std_logic_vector(63 downto 0);
    rfpaps_i            : in std_logic_vector(63 downto 0);
    rfpbps_i            : in std_logic_vector(63 downto 0);

    -- T0 trigger input
    t0_p_i              : in std_logic;

    -- PPS inputs
    pps_p_i             : in std_logic; -- from buclk RF/8
    pps_i               : in std_logic; -- from spec_node_template

    -- SLOK_RUN input
    slok_run_i          : in std_logic;
    srboo_pps_stamp_i   : in std_logic_vector(31 downto 0);

    -- LED on T0
    t0_led_o            : out std_logic;

    -- MASTER / SLAVE bit
    slave_bit_o         : out std_logic;

    -- Master ENABLE
    master_start_o      : out std_logic;
    rfpps_recv_o        : out std_logic;
    t0_start_o          : out std_logic;
    seq_start_o         : out std_logic;
    master_bcl_pipe_o   : out std_logic;

    -- SLOK management outputs
    slok_recv_o         : out std_logic;
    slok_ack_o          : out std_logic;
    slok_nok_o          : out std_logic;
    slok_adj_0_o        : out std_logic;
    slok_adj_1_o        : out std_logic;

-------------------------------------------------
-- T0 - RF Counter Stamp (received from Master)
-------------------------------------------------
    t0_rf_stamp_o       : out std_logic_vector(63 downto 0);
-------------------------------------------------
-- RF@PPS and SR booster Counters received from Master
-------------------------------------------------
    master_rfpps_recv_o : out std_logic_vector(63 downto 0);
    master_rfpaps_o     : out std_logic_vector(63 downto 0);
    master_rfpbps_o     : out std_logic_vector(63 downto 0);
    master_srboo_recv_o : out std_logic_vector(31 downto 0);

-------------------------------------------------
-- Core control
-------------------------------------------------
    slave_i             : in  t_wishbone_slave_in;
    slave_o             : out t_wishbone_slave_out
    );

end bcc_core;

architecture behavioral of bcc_core is

  component bcc_wb_slave is
    port (
      rst_n_i    : in  std_logic;
      clk_sys_i  : in  std_logic;
      wb_adr_i   : in  std_logic_vector(4 downto 0);
      wb_dat_i   : in  std_logic_vector(31 downto 0);
      wb_dat_o   : out std_logic_vector(31 downto 0);
      wb_cyc_i   : in  std_logic;
      wb_sel_i   : in  std_logic_vector(3 downto 0);
      wb_stb_i   : in  std_logic;
      wb_we_i    : in  std_logic;
      wb_ack_o   : out std_logic;
      wb_stall_o : out std_logic;
      clk_dds_i  : in     std_logic;
      clk_xo_i   : in     std_logic;
      regs_i     : in  t_bcc_in_registers;
      regs_o     : out t_bcc_out_registers);
  end component bcc_wb_slave;

  -- REGS
  signal regs_in  : t_bcc_in_registers  := c_bcc_in_registers_init_value;
  signal regs_out : t_bcc_out_registers := c_bcc_out_registers_init_value;

  -- Triggers
  signal t0_armed   : std_logic;
  signal t0_p       : std_logic;
  signal pps_armed  : std_logic;
  signal pps_p      : std_logic;
  signal ppsxo_p    : std_logic;
  signal xo_cnt          : std_logic_vector(31 downto 0);
  signal xo_cnt_ltch     : std_logic_vector(31 downto 0);

  attribute keep : string;
  attribute keep of xo_cnt_ltch     : signal is "true";

begin  -- behavioral

  U_WB_Slave : bcc_wb_slave
    port map (
      rst_n_i    => rst_n_i,
      clk_sys_i  => clk_sys_i,
      wb_adr_i   => slave_i.adr(6 downto 2),
      wb_dat_i   => slave_i.dat,
      wb_dat_o   => slave_o.dat,
      wb_cyc_i   => slave_i.cyc,
      wb_sel_i   => slave_i.sel,
      wb_stb_i   => slave_i.stb,
      wb_we_i    => slave_i.we,
      wb_ack_o   => slave_o.ack,
      wb_stall_o => slave_o.stall,
      clk_dds_i  => clk_dds_i,
      clk_xo_i   => clk_xo_i,
      regs_i     => regs_in,
      regs_o     => regs_out);

  slave_o.err    <= '0';
  slave_o.rty    <= '0';

  U_Sync_Trigger : gc_sync_ffs
    port map (
      clk_i    => clk_dds_i,
      rst_n_i  => '1',
      data_i   => t0_p_i,
      ppulse_o => t0_p);

  U_Sync_PPS : gc_sync_ffs
    port map (
      clk_i    => clk_dds_i,
      rst_n_i  => '1',
      data_i   => pps_p_i,
      ppulse_o => pps_p);

  U_Sync_PPSXO : gc_sync_ffs
    port map (
      clk_i    => clk_xo_i,
      rst_n_i  => '1',
      data_i   => pps_i,
      ppulse_o => ppsxo_p);

  U_vxco_frequency_meter : process(clk_xo_i)
  begin
    if rising_edge(clk_xo_i) then
      if rst_n_i = '0' then
        xo_cnt                 <= (others => '0');        
      else
        if ppsxo_p = '1' then
          xo_cnt_ltch          <= xo_cnt;
          xo_cnt               <= (others => '0');
        else
          xo_cnt               <= xo_cnt + 1;
        end if;
      end if;
    end if;
  end process;
  regs_in.xo_freq_i    <= xo_cnt_ltch;

  p_trigger_snapshot : process(clk_dds_i)
  begin
    if rising_edge(clk_dds_i) then
      if rst_n_i = '0' then
        t0_armed                <= '0';
        regs_in.csr_done_t0_i   <= '0';        
      else
        if(regs_out.csr_arm_t0_o = '1') then
          t0_armed                <= '1';
          regs_in.csr_done_t0_i   <= '0';
        elsif (t0_p = '1' and t0_armed = '1') then
          t0_armed                 <= '0';
          regs_in.t0_stamp_hi_i <= rf_counter_i(63 downto 32);
          regs_in.t0_stamp_lo_i <= rf_counter_i(31 downto 0);
          regs_in.csr_done_t0_i <= '1';
        end if;
      end if;
    end if;
  end process;

  p_PPS_snapshot : process(clk_dds_i)
  begin
    if rising_edge(clk_dds_i) then
      if rst_n_i = '0' then
        pps_armed                <= '0';
        regs_in.csr_done_pps_i   <= '0';        
      else
        if(regs_out.csr_arm_pps_o = '1') then
          pps_armed                <= '1';
          regs_in.csr_done_pps_i   <= '0';
        elsif (pps_p = '1' and pps_armed = '1') then
          pps_armed                 <= '0';
          regs_in.csr_done_pps_i <= '1';
        end if;
      end if;
    end if;
  end process;

  -- continuous transfer, software knows when value OK
  regs_in.pps_stamp_hi_i <= rfpps_counter_i(63 downto 32);
  regs_in.pps_stamp_lo_i <= rfpps_counter_i(31 downto 0);
  regs_in.srboo_stamp_i  <= srboo_pps_stamp_i(31 downto 0);
  regs_in.paps_hi_i      <= rfpaps_i(63 downto 32);
  regs_in.paps_lo_i      <= rfpaps_i(31 downto 0);
  regs_in.pbps_hi_i      <= rfpbps_i(63 downto 32);
  regs_in.pbps_lo_i      <= rfpbps_i(31 downto 0);

  -- LED on T0
  t0_led_o <= regs_out.csr_t0_led_o;

  -- MASTER / SLAVE bit
  slave_bit_o <= regs_out.csr_slave_o;

  -- master broadcasted master start
  master_start_o <= regs_out.csr_master_start_o;

  -- master broadcasted T0 stamp and triggers
  t0_start_o           <= regs_out.csr_t0_start_o;
  seq_start_o          <= regs_out.csr_seq_start_o;
  master_bcl_pipe_o    <= regs_out.csr_bcl_pipe_o;
  t0_rf_stamp_o(63 downto 32) <= regs_out.t0_m_stamp_hi_o;
  t0_rf_stamp_o(31 downto 0)  <= regs_out.t0_m_stamp_lo_o;

  -- master broadcasted RF@PPS
  rfpps_recv_o <= regs_out.csr_rfpps_recv_o;
  master_rfpps_recv_o(63 downto 32) <= regs_out.pps_m_stamp_hi_o;
  master_rfpps_recv_o(31 downto 0)  <= regs_out.pps_m_stamp_lo_o;
  master_srboo_recv_o(31 downto 0)  <= regs_out.pps_m_srboo_o;
  master_rfpaps_o(63 downto 32)     <= regs_out.paps_m_hi_o;
  master_rfpaps_o(31 downto 0)      <= regs_out.paps_m_lo_o;
  master_rfpbps_o(63 downto 32)     <= regs_out.pbps_m_hi_o;
  master_rfpbps_o(31 downto 0)      <= regs_out.pbps_m_lo_o;

  -- buclk : slok handshake & adjust
  slok_recv_o              <= regs_out.csr_slok_o; -- slok trigger to buclk
  regs_in.csr_done_slok_i  <= slok_run_i; -- from buclk
  slok_ack_o               <= regs_out.csr_slok_ack_o; -- reset received in buclk, OK
  slok_nok_o               <= regs_out.csr_slok_nok_o; -- reset received in buclk, NOT OK
  slok_adj_0_o             <= regs_out.csr_slok_adj_0_o; -- adjust -1 buclk_rf_counter
  slok_adj_1_o             <= regs_out.csr_slok_adj_1_o; -- adjust +1 buclk_rf_counter

end behavioral;
