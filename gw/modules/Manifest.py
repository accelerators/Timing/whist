modules = {
    "local" : [
        "bcc",
        "evt",
        "buclk",
        "wr_d3s",
        "wrnc",
        "i2c_mux",
        "general-cores",
        "etherbone-core",
        "gn4124-core"
    ]
}
