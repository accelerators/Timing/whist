library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.wishbone_pkg.all;
use work.buclk_wbgen2_pkg.all;

package buclk_pkg is

  type buclk_pulse_wid is array(natural range <>) of std_logic_vector(23 downto 0); 
  type buclk_pulse_del is array(natural range <>) of std_logic_vector(28 downto 0); 
  type buclk_tog31_ph is array(natural range <>) of std_logic_vector(2 downto 0); 
  type buclk_pulse_992 is array(natural range <>) of std_logic_vector(21 downto 0); 
  type buclk_pulse_srce is array(natural range <>) of std_logic_vector(2 downto 0); 
  type buclk_pulse_mode is array(natural range <>) of std_logic_vector(1 downto 0); 
  type buclk_pulse_pol is array(natural range <>) of std_logic; 
  type buclk_pulse_outen is array(natural range <>) of std_logic; 
  type buclk_pulse is array(natural range <>) of std_logic; 

  constant RFTIMEWID : integer := 50;
  constant FILLRFTIMEWID : std_logic_vector(63-RFTIMEWID downto 0) := (others => '0');
  constant c_max_pulse : integer := 16;
  
  component bu_sl_wb
  port (
    rst_n_i                                  : in     std_logic;
    clk_sys_i                                : in     std_logic;
    wb_adr_i                                 : in     std_logic_vector(13 downto 0);
    wb_dat_i                                 : in     std_logic_vector(31 downto 0);
    wb_dat_o                                 : out    std_logic_vector(31 downto 0);
    wb_cyc_i                                 : in     std_logic;
    wb_sel_i                                 : in     std_logic_vector(3 downto 0);
    wb_stb_i                                 : in     std_logic;
    wb_we_i                                  : in     std_logic;
    wb_ack_o                                 : out    std_logic;
    wb_stall_o                               : out    std_logic;
    buclk_sys                                : in     std_logic;
-- Ports for RAM: Bunch List
    buclk_blist_ram_addr_i                   : in     std_logic_vector(12 downto 0);
-- Read data output
    buclk_blist_ram_data_o                   : out    std_logic_vector(15 downto 0);
-- Read strobe input (active high)
    buclk_blist_ram_rd_i                     : in     std_logic;
    regs_i                                   : in     t_buclk_in_registers;
    regs_o                                   : out    t_buclk_out_registers
  );
  end component;

  component buclk
  generic (
    g_pulses : integer := 16
  );
  port (
    buclk_sys_i               : in    std_logic;
    buclk_352_i               : in    std_logic;
    clk_sys_i                 : in    std_logic;
    clk_125_i                 : in    std_logic;
    rst_n_i                   : in    std_logic;

    slave_i                   : in  t_wishbone_slave_in;
    slave_o                   : out t_wishbone_slave_out;

    slave_bit_i               : in    std_logic;
    evt_i                     : in    std_logic_vector(4 downto 1);

    fpga_version_i            : in    std_logic_vector(15 downto 0);
    link_ok_i                 : in    std_logic;
    pps_i                     : in    std_logic;   -- Pulse Per Second
    paps_i                    : in    std_logic;   -- Pulse bis Per Second
    pbps_i                    : in    std_logic;   -- Pulse ter Per Second
    pll_352_lkd_i             : in    std_logic;
    dds_pd_lkdet_i            : in    std_logic;
    
    rf_mast0_stamp_i          : in    std_logic_vector(RFTIMEWID-1 downto 0);  -- T0 master stamp
    rf_hot_pps_i              : in    std_logic_vector(RFTIMEWID-1 downto 0);  -- hot RFoE facility
    rf_hot_paps_i             : in    std_logic_vector(RFTIMEWID-1 downto 0);  -- master PPS broadcast
    rf_hot_pbps_i             : in    std_logic_vector(RFTIMEWID-1 downto 0);  -- master PPS broadcast
    master_t0_i               : in    std_logic;
    master_t0_seq_i           : in    std_logic;
    master_start_i            : in    std_logic;
    master_bcl_pipe_i         : in    std_logic;
    master_srboo_pps_i        : in    std_logic_vector(31 downto 0);
    rf_hot_ok_i               : in    std_logic;
    rf_phase_safe_i           : in    std_logic;
    slok_ack_i                : in    std_logic; -- bcc-core : slok_done ack OK
    slok_nok_i                : in    std_logic; -- bcc-core : slok_done ack not OK
    slok_adj_0_i              : in    std_logic; -- bcc-core : slok adjust minus 1
    slok_adj_1_i              : in    std_logic; -- bcc-core : slok adjust plus 1
    slok_trig_i               : in    std_logic; -- bcc-core : slok cmd
    pll_dld_i                 : in    std_logic;

    rt_d3s_run_i              : in    std_logic;
    rt_d3s_locked_i           : in    std_logic;
    tinrise_i                 : in    std_logic_vector(3 downto 0);

    input_pol_o               : out   std_logic_vector(3 downto 0);
    input_ena_o               : out   std_logic_vector(3 downto 0);

    pulse_o                   : out   buclk_pulse(g_pulses-1 downto 0);
    rf_en_o                   : out   std_logic;
    out_mux_o                 : out   std_logic_vector(1 downto 0);
    rst352_o                  : out   std_logic;
    rf_pps_rdy_bcc_o          : out   std_logic;
    sync_95xx_o               : out   std_logic;
    slok_run_o                : out   std_logic; -- bcc-core : slok_run
    srboo_pps_stamp_o         : out   std_logic_vector(31 downto 0);
    
    buclk_injerr_o            : out   std_logic;

    rf_pps_stamp_o            : out   std_logic_vector(RFTIMEWID-1 downto 0);
    rf_paps_stamp_o           : out   std_logic_vector(RFTIMEWID-1 downto 0);
    rf_pbps_stamp_o           : out   std_logic_vector(RFTIMEWID-1 downto 0);
--    rf_stampin_o              : out   std_logic_vector(RFTIMEWID-1 downto 0);  -- pipelined cnt_bu44
    rf_maincnt_o              : out   std_logic_vector(RFTIMEWID-1 downto 0)   -- main RF counter RF/8
  );
  end component;


  component bcc_core is
  generic (
    g_interface_mode      : t_wishbone_interface_mode      := CLASSIC;
    g_address_granularity : t_wishbone_address_granularity := WORD);
  port (
    -- Clocks & resets
    clk_sys_i           : in std_logic;
    rst_n_i             : in std_logic;

    -- DDS synth clock (RF/8) to synchronize trigger
    clk_dds_i           : in std_logic;
    -- RF vcxo through AD9510
    clk_xo_i            : in std_logic;
    -- RF Counter for trigger snapshot
    rf_counter_i        : in std_logic_vector(63 downto 0);
    -- register for RF counter PPS stamp
    rfpps_counter_i     : in std_logic_vector(63 downto 0);
    rfpaps_i            : in std_logic_vector(63 downto 0);
    rfpbps_i            : in std_logic_vector(63 downto 0);

    -- T0 trigger & PPS inputs
    t0_p_i              : in std_logic;
    pps_p_i             : in std_logic;
    pps_i               : in std_logic;

    -- SLOK management inputs
    slok_run_i          : in std_logic;
    srboo_pps_stamp_i   : in std_logic_vector(31 downto 0);

    -- LED on T0
    t0_led_o            : out std_logic;

    -- MASTER / SLAVE bit
    slave_bit_o         : out std_logic;

    -- Master ENABLE
    master_start_o      : out std_logic;
    rfpps_recv_o        : out std_logic;
    t0_start_o          : out std_logic;
    seq_start_o         : out std_logic;
    master_bcl_pipe_o   : out std_logic;

    -- SLOK management outputs
    slok_recv_o         : out std_logic;
    slok_ack_o          : out std_logic;
    slok_nok_o          : out std_logic;
    slok_adj_0_o        : out std_logic;
    slok_adj_1_o        : out std_logic;

-------------------------------------------------
-- T0, PPS, SR, Booster - RF Counter Stamps (received from Master)
-------------------------------------------------
    t0_rf_stamp_o       : out std_logic_vector(63 downto 0);
    master_rfpps_recv_o : out std_logic_vector(63 downto 0);
    master_rfpaps_o     : out std_logic_vector(63 downto 0);
    master_rfpbps_o     : out std_logic_vector(63 downto 0);
    master_srboo_recv_o : out std_logic_vector(31 downto 0);

-------------------------------------------------
-- Core control
-------------------------------------------------
    slave_i             : in  t_wishbone_slave_in;
    slave_o             : out t_wishbone_slave_out
    );
  end component;

  component evt_core is
  generic (
    g_interface_mode      : t_wishbone_interface_mode      := CLASSIC;
    g_address_granularity : t_wishbone_address_granularity := WORD);
  port (
    -- Clocks & resets
    clk_sys_i           : in std_logic;
    rst_n_i             : in std_logic;

    -- DDS synth clock (RF/8) to synchronize trigger
    clk_dds_i           : in std_logic;
    
    slave_bit_i         : in std_logic;
    input_pol_i         : in std_logic_vector(3 downto 0);
    input_ena_i         : in std_logic_vector(3 downto 0);
    
    -- cnt_bu44 counter for trigger snapshot
    rf_counter_i        : in std_logic_vector(63 downto 0);
    -- Tx trigger inputs
    tx_p_i              : in std_logic_vector(3 downto 0);

-------------------------------------------------
-- event monostables
-------------------------------------------------
    evt_o               : out std_logic_vector(4 downto 1);
    tinrise_o           : out std_logic_vector(3 downto 0);
-------------------------------------------------
-- Core control
-------------------------------------------------
    slave_i             : in  t_wishbone_slave_in;
    slave_o             : out t_wishbone_slave_out
    );
  end component;

end buclk_pkg;

