-----------------------------------------------------------------------------------------
-- Title          : prototype esrf timing sequencer
-----------------------------------------------------------------------------------------
-- File           : buclk.vhd
-- Author         : GG
-- Created        : 25/01/2019
-- Standard       : VHDL'87
-----------------------------------------------------------------------------------------
-- bunch clock sequencer
-- improving on version V3-78 used from october 2d 2018 -> shutdown
-- SYNC_RF connected, must be pad-synchronized in top.vhd, to be used with vcxo@704 MHz
-- bunch number checked to be 0-991
-- rf_phase_safe synchro logic removed because phase safe not synchronous between modules
-- added RF/8 resynchro for several xx_o type signals
-- comments cleaned up
-----------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
--library work;
use work.wishbone_pkg.all;
use work.buclk_pkg.all;
use work.buclk_wbgen2_pkg.all;
use work.genram_pkg.all;

entity buclk is
  generic (
    g_pulses : integer := 12);
  port (
    buclk_sys_i               : in    std_logic;
    buclk_352_i               : in    std_logic;
    clk_sys_i                 : in    std_logic;
    clk_125_i                 : in    std_logic;
    rst_n_i                   : in    std_logic;
    
    slave_i                   : in  t_wishbone_slave_in;
    slave_o                   : out t_wishbone_slave_out;

    slave_bit_i               : in    std_logic;
    evt_i                     : in    std_logic_vector(4 downto 1);

    fpga_version_i            : in    std_logic_vector(15 downto 0);
    link_ok_i                 : in    std_logic;
    pps_i                     : in    std_logic;    -- Pulse Per Second
    paps_i                    : in    std_logic;    -- Pulse bis Per Second for SLOK
    pbps_i                    : in    std_logic;    -- Pulse ter Per Second for SLOK
    pll_352_lkd_i             : in    std_logic;
    dds_pd_lkdet_i            : in    std_logic;
    
    rf_mast0_stamp_i          : in    std_logic_vector(RFTIMEWID-1 downto 0);  -- T0 master stamp
    rf_hot_pps_i              : in    std_logic_vector(RFTIMEWID-1 downto 0);  -- master PPS broadcast
    rf_hot_paps_i             : in    std_logic_vector(RFTIMEWID-1 downto 0);  -- master PAPS broadcast (SLOK)
    rf_hot_pbps_i             : in    std_logic_vector(RFTIMEWID-1 downto 0);  -- master PBPS broadcast (SLOK)
    master_t0_i               : in    std_logic;
    master_t0_seq_i           : in    std_logic;
    master_start_i            : in    std_logic;
    master_bcl_pipe_i         : in    std_logic;
    master_srboo_pps_i        : in    std_logic_vector(31 downto 0);
    rf_hot_ok_i               : in    std_logic; -- bcc-core : master PPS valid
    rf_phase_safe_i           : in    std_logic;
    slok_ack_i                : in    std_logic; -- bcc-core : slok ack OK
    slok_nok_i                : in    std_logic; -- bcc-core : slok ack not OK
    slok_adj_0_i              : in    std_logic; -- bcc-core : slok adjust minus 1
    slok_adj_1_i              : in    std_logic; -- bcc-core : slok adjust plus 1
    slok_trig_i               : in    std_logic; -- bcc-core : slok cmd
    pll_dld_i                 : in    std_logic;

    rt_d3s_run_i              : in    std_logic;
    rt_d3s_locked_i           : in    std_logic;
    tinrise_i                 : in    std_logic_vector(3 downto 0);

    input_pol_o               : out   std_logic_vector(3 downto 0);
    input_ena_o               : out   std_logic_vector(3 downto 0);

    pulse_o                   : out   buclk_pulse(g_pulses downto 1);
    rf_en_o                   : out   std_logic;
    out_mux_o                 : out   std_logic_vector(1 downto 0);
    rst352_o                  : out   std_logic;
    rf_pps_rdy_bcc_o          : out   std_logic;
    sync_95xx_o               : out   std_logic;
    slok_run_o                : out   std_logic; -- bcc-core : slok_run
    srboo_pps_stamp_o         : out   std_logic_vector(31 downto 0);

    buclk_injerr_o            : out   std_logic;

    rf_pps_stamp_o            : out   std_logic_vector(RFTIMEWID-1 downto 0);
    rf_paps_stamp_o           : out   std_logic_vector(RFTIMEWID-1 downto 0);
    rf_pbps_stamp_o           : out   std_logic_vector(RFTIMEWID-1 downto 0);
--    rf_stampin_o              : out   std_logic_vector(RFTIMEWID-1 downto 0);  -- pipelined cnt_bu44
    rf_maincnt_o              : out   std_logic_vector(RFTIMEWID-1 downto 0)   -- cnt_bu44 main RF counter RF/8
  );
end buclk;

architecture rtl of buclk is

-- constants
constant SUPER        : std_logic_vector(11 downto 0) := "010101010011";  -- 0x553 = 4x31x11 - 1 = RF/8 super period
constant MAXMB        : std_logic_vector(3 downto 0) := "1111";  -- max 16 bunches with injection rotation
constant TT0DELAY     : std_logic_vector(15 downto 0) := X"AC00";  -- 1ms at RF/8
constant ROTI40       : std_logic_vector(5 downto 0) := "000000"; -- 0
constant ROTI41       : std_logic_vector(5 downto 0) := "001011"; -- 1x11 0xb
constant ROTI42       : std_logic_vector(5 downto 0) := "010110"; -- 2x11 0x16
constant ROTI43       : std_logic_vector(5 downto 0) := "100001"; -- 3x11 0x21
constant MAXSRBADJ    : std_logic_vector(7 downto 0) := X"FF"; -- 256
constant TROIS        : std_logic_vector(2 downto 0) := "011"; -- 3
                                                                 
type etatic is
    (tic0, tic1, tic2, tic3, tic4, tic5, tic6, tic61, tic7, tic8, tic9, tic21, tic41
     );
signal toc, tac, trot : etatic;

type clock_sel    is array(g_pulses downto 1) of std_logic_vector(4 downto 0); 
type pseq_sel     is array(g_pulses downto 4) of std_logic_vector(6 downto 0); 

signal ph_adj_352     : std_logic_vector(2 downto 0);  -- RF precision phasing
signal cnt_adj_352    : std_logic_vector(2 downto 0);  -- cnt_352 phasing
signal cnt_bun        : std_logic_vector(2 downto 0);  -- counts bunches @ RF rate inside 32 pack
signal cnt_b31        : std_logic_vector(4 downto 0);  -- counts packs of 31
signal cnt31_reset    : std_logic;
signal cnt31_reset_352 : std_logic;
signal cnt31_raz      : std_logic;
signal cnt_ckph7      : std_logic_vector(2 downto 0);  -- counts packs of 8 for fine delays
signal cnt_7          : std_logic_vector(2 downto 0);  -- counts packs of 8 for rf/8 to rf domain transition
signal cnt_7b         : std_logic;  -- rises on cnt_7 = 011, active on cnt_7 = 100
signal cnt_3132     	: std_logic_vector(4 downto 0);  -- counts 32 packs of 31 in RF
signal tog31_ph       : buclk_tog31_ph(3 downto 1); -- phase prog channels[3..1]
--signal tog31_ph_352   : buclk_tog31_ph(3 downto 1); -- phase prog channels[3..1] RF
signal cnt_bu44          : std_logic_vector(RFTIMEWID-1 downto 0); -- counts packets of 8 bunches @ RF/8 rate
--signal cnt_bu44_stampin  : std_logic_vector(RFTIMEWID-1 downto 0); -- pipeline register
signal clk_4b         : std_logic;
signal clk_4p         : std_logic;
signal clk_16b        : std_logic;
signal clk_42         : std_logic;
signal clk_162        : std_logic;
signal bu44_adj_en    : std_logic;
signal bu44_adj_0     : std_logic;
signal bu44_adj_1     : std_logic;
signal slok_adj_en    : std_logic;
signal cnt_bu44_ini   : std_logic_vector(RFTIMEWID-1 downto 0); -- calculated initial value
signal cnt_bu44_ppsa  : std_logic_vector(RFTIMEWID-1 downto 0); -- pps stamp
signal cnt_bu44_ppsb  : std_logic_vector(RFTIMEWID-1 downto 0); -- stamp pipe
signal cnt_bu44_papsa  : std_logic_vector(RFTIMEWID-1 downto 0); -- pps stamp
signal cnt_bu44_pbpsa  : std_logic_vector(RFTIMEWID-1 downto 0); -- stamp pipe
signal cnt_bu44_incr  : std_logic_vector(1 downto 0);  -- 1 + slok adjust
signal cnt8_boo       : std_logic_vector(5 downto 0);  -- couts booster 8-packs

signal boo_inj_reg    : std_logic_vector(3 downto 0);    -- inj 32-pack memory at gun
signal cnt_del_inj    : std_logic_vector(15 downto 0);   -- injection 195 delay in RF/8 periods
signal cnt_rf_ext     : std_logic_vector(13 downto 0);   -- extraction delay RF counter (1 super period)
signal cnt_wid_inj    : std_logic_vector(15 downto 0);   -- extraction trig_inj width
signal cnt_del_ext    : std_logic_vector(15 downto 0);   -- extraction delay in super periods
signal cnt_wid_ext    : std_logic_vector(15 downto 0);   -- extraction trig_ext width
signal cnt8_sr        : std_logic_vector(6 downto 0);    -- counts SR 8-packs
signal cnt8_sr_tc     : std_logic;                    -- terminal count
signal cnt8_hsr       : std_logic_vector(5 downto 0);    -- counts half-SR 8-packs
signal cnt8_ppsa      : std_logic_vector(31 downto 0);   -- booster & SR stamper
signal hsr_togate     : buclk_pulse(g_pulses downto 1);      -- toggle gate for CLK_SR_DIV 50%
signal hsr_pi_togate  : buclk_pulse(g_pulses downto 1);      -- toggle gate for CLK_SR_DIV 50%

signal autoblist             : std_logic;
signal blist_add_mb          : std_logic;
signal blist_add_autorotinj  : std_logic;
signal blist_start           : std_logic_vector(12 downto 0);
signal blist_start_mb        : std_logic_vector(12 downto 0);
signal blist_stop            : std_logic_vector(12 downto 0);
signal blistrst_long         : std_logic;
signal blistrst_end          : std_logic;
signal blistrst_rf           : std_logic;
signal blistrst_1rf          : std_logic;
signal blistinc_long         : std_logic;
signal blistinc_end          : std_logic;
signal blistinc_rf           : std_logic;
signal blistinc_1rf          : std_logic;
signal bunch_en              : std_logic;
signal mblprst_long          : std_logic;
signal mblprst_end           : std_logic;
signal mblprst_rf            : std_logic;
signal mblprst_1rf           : std_logic;
signal rst_injerr_long       : std_logic;
signal rst_injerr_end        : std_logic;
signal rst_injerr_rf         : std_logic;
signal rst_injerr_1rf        : std_logic;
signal rflraz_long           : std_logic;
signal rflraz_end            : std_logic;
signal rflraz_rf             : std_logic;
signal rflraz_1rf            : std_logic;
signal rst_352_long          : std_logic;
signal rst_352_end           : std_logic;
signal rst352_cnt            : std_logic_vector(3 downto 0);
signal skip44_long           : std_logic;
signal skip44_end            : std_logic;
signal skip44_rf             : std_logic;
signal skip44_1rf            : std_logic;
signal jump44_long           : std_logic;
signal jump44_end            : std_logic;
signal jump44_rf             : std_logic;
signal jump44_1rf            : std_logic;
signal syncrf_long           : std_logic;
signal syncrf_rf             : std_logic;
signal syncrf_4rf            : std_logic;
signal syncrf_end            : std_logic;
signal syncrf_cnt            : std_logic_vector(1 downto 0);

signal buclk_run           : std_logic;
signal wr_link             : std_logic;
signal wr_link_lost        : std_logic;
signal error_inj           : std_logic;
signal done_ext            : std_logic;
signal done_inj            : std_logic;
signal gun_inj             : std_logic;  -- bunch clock gun
signal gun_inj_352         : std_logic;
signal guni_en             : std_logic;
signal fin_ext             : std_logic;
signal guno                : std_logic;
signal guneto              : std_logic;
signal gunetob             : std_logic;
signal gunetoc             : std_logic;
signal gunetod             : std_logic;
signal gun_pinj            : std_logic;  -- gun_inj (TINJ) + del_gun
signal gun_pipinj          : std_logic;  -- pinj pipeline
signal gun_pinj_done       : std_logic;
signal gun_sum             : std_logic;
signal gun_sum_352         : std_logic;
signal pinj_del            : std_logic;
signal inj_quiet           : std_logic;
signal inj_run             : std_logic;
signal t_inj               : std_logic;
signal t_ext               : std_logic;
signal tk_inj              : std_logic;
signal t0_sync             : std_logic;
signal t0sa                : std_logic;
signal inj_run_b           : std_logic;
signal t_inj_a             : std_logic;
signal t_inj_b             : std_logic;
signal t_ext_a             : std_logic;
signal t_ext_b             : std_logic;

signal pas8                : std_logic_vector(5 downto 0); -- to count up to 44 at RF/8 (= 1 booster)
signal mb_en               : std_logic;
signal start_mb_ini        : std_logic;
signal gun_mb              : std_logic;
signal gun_mb_352          : std_logic;
signal xbunch              : std_logic_vector(3 downto 0);  -- to count 10 bunches max

signal bu44_inj            : std_logic_vector(31 downto 0);
signal bu44_ext            : std_logic_vector(31 downto 0);
signal t0_received         : std_logic;
signal tt0_received        : std_logic;
signal t0_ack              : std_logic;
signal t0_lost             : std_logic;
signal t0_lost_cnt         : std_logic_vector(31 downto 0);
signal t0_cnt              : std_logic_vector(31 downto 0);
signal rf_t0               : std_logic;
signal master_start_mem    : std_logic;
signal master_start_pps    : std_logic;
signal ms_start_pps_a      : std_logic;
signal ms_start_pps_b      : std_logic;
signal ms_start_pps_c      : std_logic;
signal m_st_352            : std_logic;
signal m_st_pps_352        : std_logic;
signal m_st_1              : std_logic;
signal pps_sampled         : std_logic;
signal pps_a               : std_logic;
signal pps_b               : std_logic;
signal paps_rise           : std_logic;
signal paps_rise_a         : std_logic;
signal paps_rise_b         : std_logic;
signal paps_a              : std_logic;
signal paps_b              : std_logic;
signal pbps_rise           : std_logic;
signal pbps_rise_a         : std_logic;
signal pbps_rise_b         : std_logic;
signal pbps_a              : std_logic;
signal pbps_b              : std_logic;

signal cnt_spl             : std_logic_vector(3 downto 0);
signal rf_pps_rdy          : std_logic;
signal rf_pps_rdy_a        : std_logic;
signal rf_pps_rdy_b        : std_logic;
signal rf_pps_rdy_bcc      : std_logic;
signal new_t0_stamp        : std_logic_vector(RFTIMEWID-1 downto 0);
signal rf_inj_delay        : std_logic_vector(29 downto 0);

signal clk16_sel           : clock_sel; 
signal pulseq_sel          : pseq_sel; 
signal clkb                : buclk_pulse(g_pulses downto 4);
signal clkb_o              : buclk_pulse(g_pulses downto 1);
signal kontour             : buclk_pulse(g_pulses downto 1);
signal pulb_o              : buclk_pulse(g_pulses downto 4);
signal puloute             : std_logic_vector(3 downto 1); -- for 352 pipe
signal pipoute             : std_logic_vector(3 downto 1);
signal pulb_123_o          : std_logic_vector(3 downto 1);
signal pulb_inj            : buclk_pulse(g_pulses downto 1);
signal pulb_pinj           : buclk_pulse(g_pulses downto 1);
signal pulb_ext            : buclk_pulse(g_pulses downto 1);
signal pulb_seq            : buclk_pulse(g_pulses downto 4);
signal pulb_tt0            : buclk_pulse(g_pulses downto 1);
signal pulb_end            : buclk_pulse(g_pulses downto 1);
signal pulb_gate           : buclk_pulse(g_pulses downto 1);
signal pul_del             : buclk_pulse_del(g_pulses downto 1);
signal div_992             : buclk_pulse_992(g_pulses downto 1);
signal cnt_992             : buclk_pulse_992(g_pulses downto 1);
signal cnt_448             : buclk_pulse_992(g_pulses downto 1);
signal pul_wid             : buclk_pulse_wid(g_pulses downto 1);
signal pul_srce            : buclk_pulse_srce(g_pulses downto 1);
signal pul_evt             : std_logic_vector(g_pulses downto 9);
signal pul_pol             : buclk_pulse_pol(g_pulses downto 1);
signal pul_outen           : buclk_pulse_outen(g_pulses downto 1);
signal pul_mode            : buclk_pulse_mode(g_pulses downto 1);
signal pulb_del_cnt        : buclk_pulse_del(g_pulses downto 1);
signal pulb_wid_cnt        : buclk_pulse_wid(g_pulses downto 1);

signal pulb_1_352          : std_logic;
signal pulb_2_352          : std_logic;
signal pulb_3_352          : std_logic;
signal pulb_rf1_352        : std_logic;
signal pulb_rf2_352        : std_logic;
signal pulb_rf3_352        : std_logic;
signal pulb_rf_o           : std_logic_vector(3 downto 1);

signal fckph               : std_logic_vector(3 downto 1);
signal fckph_1             : std_logic_vector(3 downto 1);
signal fckph_352           : std_logic_vector(3 downto 1);
signal clkbph_352          : std_logic_vector(3 downto 1);

-- inputs stamping
type fifo_tab     is array(3 downto 0) of std_logic_vector(31 downto 0); 
signal ista_lo             : fifo_tab;
signal ista_hi             : fifo_tab;
signal reset_fifo          : std_logic_vector(3 downto 0);
signal read_fifo           : std_logic_vector(3 downto 0);

--TT0 special
signal tt0_new_stamp       : std_logic_vector(RFTIMEWID-1 downto 0);
signal tt0_wait            : std_logic;
signal tt0                 : std_logic;
signal pultt0              : buclk_pulse(g_pulses downto 1);
signal pultt0_done         : buclk_pulse(g_pulses downto 1);
--evt special
signal pulevt              : std_logic_vector(g_pulses downto 9); -- evt beeing processed

--RAM for bunch list
signal buclk_bun_addr    : std_logic_vector(12 downto 0) := (others => '0');
signal buclk_bun_data    : std_logic_vector(15 downto 0);
signal buclk_bun_nb      : std_logic_vector(9 downto 0); -- freeze bunch number for main sequencer

signal regs_in  : t_buclk_in_registers := c_buclk_in_registers_init_value;
signal regs_out : t_buclk_out_registers;

-- hot rf sync logic
signal freq_bu44         : std_logic_vector(RFTIMEWID-1 downto 0);
signal hot               : etatic;
signal pps_rise          : std_logic;
signal pps_rise_a        : std_logic;
signal pps_rise_b        : std_logic;
signal pps_rise_c        : std_logic;
signal slave_start       : std_logic;
signal slave_start_pps   : std_logic;
signal syncrf_a          : std_logic;
signal syncrf_b          : std_logic;
signal t0_rst            : std_logic;
signal slok_long             : std_logic;
signal slok_end              : std_logic;
signal slok_run              : std_logic;
signal slok_aok              : std_logic;
signal slok_nok              : std_logic;
signal slok_rf               : std_logic;
signal slok_a                : std_logic;
signal slok_b                : std_logic;
signal sric                  : etatic;
signal boc                   : etatic;
signal sr_aok                : std_logic;
signal sr_nok                : std_logic;
signal bo_aok                : std_logic;
signal bo_nok                : std_logic;
signal sr8_moins_un          : std_logic;
signal bo8_moins_un          : std_logic;
signal sr_adj_pass           : std_logic_vector(7 downto 0);
signal bo_adj_pass           : std_logic_vector(7 downto 0);
signal sr_lok_pass           : std_logic_vector(3 downto 0);
signal bo_lok_pass           : std_logic_vector(3 downto 0);
signal sr_lok                : std_logic_vector(3 downto 0);
signal bo_lok                : std_logic_vector(3 downto 0);
signal sr_aok_a              : std_logic;
signal sr_aok_rise           : std_logic;
signal sync_src_en           : std_logic;
signal sync_src_ack          : std_logic;
signal sync_src_pulse        : std_logic;
signal bu44_lsb              : std_logic_vector(7 downto 0);
signal pko                   : etatic;

-- watch dogs
signal wd4hz             : std_logic_vector(25 downto 0);
signal t0wd              : std_logic;

-- rotinj
signal rot_seq           : std_logic_vector(1 downto 0);
signal roti_del_cnt      : std_logic_vector(5 downto 0);
signal roti_lsb          : std_logic_vector(2 downto 0);
signal roti_pass         : std_logic_vector(1 downto 0);
signal roti_pas8         : std_logic_vector(3 downto 0);
signal roti_val          : std_logic_vector(1 downto 0);
signal moulin            : std_logic_vector(5 downto 0);
signal next_roti         : std_logic;
signal next_roti_ack     : std_logic;
signal pinj_del_run      : std_logic;
signal pinj_del_a        : std_logic;
signal rotinj_en         : std_logic;
signal bunch_bpf_352     : std_logic_vector(2 downto 0);
signal bunch_1st_352     : std_logic_vector(2 downto 0);
signal buclk_1st_data    : std_logic_vector(2 downto 0);

-- debug
signal dbg_adj_en        : std_logic;
signal dbg_adj_0         : std_logic; -- adjust minus 1
signal dbg_adj_1         : std_logic; -- adjust plus 1
signal mala              : std_logic;
signal sama              : std_logic;
signal supa              : std_logic;
signal malb              : std_logic;
signal samb              : std_logic;
signal supb              : std_logic;
signal malc              : std_logic;
signal samc              : std_logic;
signal supc              : std_logic;
signal delta_pps         : std_logic_vector(RFTIMEWID-1 downto 0); -- pps discrepancy
signal rf_lost           : std_logic;
signal rf_dbg_adj        : std_logic;
signal rf_adj_en         : std_logic;
signal rf_adj_stop       : std_logic;
signal rf_dbg_run        : std_logic;
signal cnt_hist0         : std_logic_vector(31 downto 0);
signal cnt_hist1         : std_logic_vector(31 downto 0);
signal cnt_hist2         : std_logic_vector(31 downto 0);
signal gab               : etatic;
signal gub               : etatic;
signal metas_0           : std_logic_vector(7 downto 0);
signal metas_1           : std_logic_vector(7 downto 0);
signal metas_2           : std_logic_vector(7 downto 0);
signal metas_3           : std_logic_vector(7 downto 0);

-- registers pipeline
signal  rego_t0_del           : std_logic_vector(29 downto 0);
signal  rego_wid_tinj         : std_logic_vector(15 downto 0);
signal  rego_wid_text         : std_logic_vector(15 downto 0);
signal  rego_del_gun          : std_logic_vector(15 downto 0);
signal  rego_del_ext          : std_logic_vector(15 downto 0);
signal  rego_out1_del         : std_logic_vector(31 downto 0);
signal  rego_out2_del         : std_logic_vector(31 downto 0);
signal  rego_out3_del         : std_logic_vector(31 downto 0);
signal  rego_out4_del         : std_logic_vector(31 downto 0);
signal  rego_out5_del         : std_logic_vector(31 downto 0);
signal  rego_out6_del         : std_logic_vector(31 downto 0);
signal  rego_out7_del         : std_logic_vector(31 downto 0);
signal  rego_out8_del         : std_logic_vector(31 downto 0);
signal  rego_out9_del         : std_logic_vector(31 downto 0);
signal  rego_out10_del        : std_logic_vector(31 downto 0);
signal  rego_out11_del        : std_logic_vector(31 downto 0);
signal  rego_out12_del        : std_logic_vector(31 downto 0);

attribute keep : string;
attribute keep of buclk_bun_data     : signal is "true";
attribute keep of buclk_1st_data     : signal is "true";
attribute keep of buclk_bun_nb       : signal is "true";
attribute keep of ms_start_pps_a     : signal is "true";
attribute keep of ms_start_pps_b     : signal is "true";
attribute keep of ms_start_pps_c     : signal is "true";
attribute keep of tog31_ph           : signal is "true";
--attribute keep of tog31_ph_352       : signal is "true";
attribute keep of pul_del            : signal is "true";
attribute keep of pul_mode           : signal is "true";
attribute keep of pul_pol            : signal is "true";
attribute keep of pul_outen          : signal is "true";
attribute keep of pul_srce           : signal is "true";
attribute keep of pul_wid            : signal is "true";
attribute keep of clk_4p             : signal is "true";
attribute keep of rego_out1_del      : signal is "true";
attribute keep of rego_out2_del      : signal is "true";
attribute keep of rego_out3_del      : signal is "true";
attribute keep of rego_out4_del      : signal is "true";
attribute keep of rego_out5_del      : signal is "true";
attribute keep of rego_out6_del      : signal is "true";
attribute keep of rego_out7_del      : signal is "true";
attribute keep of rego_out8_del      : signal is "true";
attribute keep of rego_out9_del      : signal is "true";
attribute keep of rego_out10_del     : signal is "true";
attribute keep of rego_out11_del     : signal is "true";
attribute keep of rego_out12_del     : signal is "true";
attribute keep of tt0_received       : signal is "true";
attribute keep of rego_t0_del        : signal is "true";
attribute keep of rego_del_gun       : signal is "true";
attribute keep of rego_del_ext       : signal is "true";
attribute keep of cnt_bu44_papsa     : signal is "true";
attribute keep of cnt_bu44_pbpsa     : signal is "true";

begin

-- **************************************************************************
-- Wishbone interface
  bu_sl_wbgen2 : bu_sl_wb
  port map (
    clk_sys_i                 => clk_sys_i,
    rst_n_i                   => rst_n_i,
    wb_adr_i                  => slave_i.adr(15 downto 2),
    wb_dat_i                  => slave_i.dat,
    wb_dat_o                  => slave_o.dat,
    wb_cyc_i                  => slave_i.cyc,
    wb_sel_i                  => slave_i.sel,
    wb_stb_i                  => slave_i.stb,
    wb_we_i                   => slave_i.we,
    wb_ack_o                  => slave_o.ack,
    wb_stall_o                => slave_o.stall,
    buclk_sys                 => buclk_sys_i,
    buclk_blist_ram_addr_i    => buclk_bun_addr,
    buclk_blist_ram_data_o    => buclk_bun_data,
    buclk_blist_ram_rd_i      => '1',
    regs_i                    => regs_in,
    regs_o                    => regs_out
  );

  slave_o.err    <= '0';
  slave_o.rty    <= '0';

-- RF/8 CLOCK DOMAIN transfer fron clk_sys
-- input register buffer for configuration parameters synchronization
-- **************************************************************************
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' then
        rego_t0_del           <= (29 downto 1 => '0') &  '1';
        rego_del_gun          <= (15 downto 1 => '0') &  '1';
        rego_del_ext          <= (15 downto 1 => '0') &  '1';
      elsif master_bcl_pipe_i = '1' then
        rego_t0_del           <= regs_out.t0_del_o;
        rego_del_gun          <= regs_out.del_gun_o;
        rego_del_ext          <= regs_out.del_ext_o;
      end if; 
    end if; 
  end process;

-- pipeline for outputs delay / width registers : T0 synchronization
-- **************************************************************************
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' then
        rego_wid_tinj         <= (15 downto 1 => '0') &  '1';
        rego_wid_text         <= (15 downto 1 => '0') &  '1';
        
        rego_out1_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(1)            <= (23 downto 1 => '0') &  '1';
        pul_srce(1)           <= (others => '0');
        pul_pol(1)            <= '0';
        pul_mode(1)           <= (others => '0');
        pul_outen(1)          <= '0';
        rego_out2_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(2)            <= (23 downto 1 => '0') &  '1';
        pul_srce(2)           <= (others => '0');
        pul_pol(2)            <= '0';
        pul_mode(2)           <= (others => '0');
        pul_outen(2)          <= '0';
        rego_out3_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(3)            <= (23 downto 1 => '0') &  '1';
        pul_srce(3)           <= (others => '0');
        pul_pol(3)            <= '0';
        pul_mode(3)           <= (others => '0');
        pul_outen(3)          <= '0';

        rego_out4_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(4)            <= (23 downto 1 => '0') &  '1';
        pul_srce(4)           <= (others => '0');
        pul_pol(4)            <= '0';
        pul_mode(4)           <= (others => '0');
        pul_outen(4)          <= '0';
        rego_out5_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(5)            <= (23 downto 1 => '0') &  '1';
        pul_srce(5)           <= (others => '0');
        pul_pol(5)            <= '0';
        pul_mode(5)           <= (others => '0');
        pul_outen(5)          <= '0';
        rego_out6_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(6)            <= (23 downto 1 => '0') &  '1';
        pul_srce(6)           <= (others => '0');
        pul_pol(6)            <= '0';
        pul_mode(6)           <= (others => '0');
        pul_outen(6)          <= '0';
        rego_out7_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(7)            <= (23 downto 1 => '0') &  '1';
        pul_srce(7)           <= (others => '0');
        pul_pol(7)            <= '0';
        pul_mode(7)           <= (others => '0');
        pul_outen(7)          <= '0';
        rego_out8_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(8)            <= (23 downto 1 => '0') &  '1';
        pul_srce(8)           <= (others => '0');
        pul_pol(8)            <= '0';
        pul_mode(8)           <= (others => '0');
        pul_outen(8)          <= '0';
        rego_out9_del         <= (31 downto 1 => '0') &  '1';
        pul_wid(9)            <= (23 downto 1 => '0') &  '1';
        pul_srce(9)           <= (others => '0');
        pul_evt(9)            <= '0';
        pul_pol(9)            <= '0';
        pul_mode(9)           <= (others => '0');
        pul_outen(9)          <= '0';
        rego_out10_del        <= (31 downto 1 => '0') &  '1';
        pul_wid(10)           <= (23 downto 1 => '0') &  '1';
        pul_srce(10)          <= (others => '0');
        pul_evt(10)           <= '0';
        pul_pol(10)           <= '0';
        pul_mode(10)          <= (others => '0');
        pul_outen(10)         <= '0';
        rego_out11_del        <= (31 downto 1 => '0') &  '1';
        pul_wid(11)           <= (23 downto 1 => '0') &  '1';
        pul_srce(11)          <= (others => '0');
        pul_evt(11)           <= '0';
        pul_pol(11)           <= '0';
        pul_mode(11)          <= (others => '0');
        pul_outen(11)         <= '0';
        rego_out12_del        <= (31 downto 1 => '0') &  '1';
        pul_wid(12)           <= (23 downto 1 => '0') &  '1';
        pul_srce(12)          <= (others => '0');
        pul_evt(12)           <= '0';
        pul_pol(12)           <= '0';
        pul_mode(12)          <= (others => '0');
        pul_outen(12)         <= '0';
--      elsif tt0_received = '1' then improved below for v3-81
      elsif (tt0_received = '1') or ((pps_rise_a = '1') and (inj_run = '0')) then
        rego_wid_tinj         <= regs_out.wid_tinj_o;
        rego_wid_text         <= regs_out.wid_text_o;
        
        rego_out1_del         <= regs_out.out1_del_o;
        pul_wid(1)            <= regs_out.wid_1_wid_o;
        pul_srce(1)           <= regs_out.wid_1_srce_o;
        pul_pol(1)            <= regs_out.wid_1_pol_o;
        pul_mode(1)           <= regs_out.wid_1_mode_o;
        pul_outen(1)          <= regs_out.wid_1_outen_o;
        rego_out2_del         <= regs_out.out2_del_o;
        pul_wid(2)            <= regs_out.wid_2_wid_o;
        pul_srce(2)           <= regs_out.wid_2_srce_o;
        pul_pol(2)            <= regs_out.wid_2_pol_o;
        pul_mode(2)           <= regs_out.wid_2_mode_o;
        pul_outen(2)          <= regs_out.wid_2_outen_o;
        rego_out3_del         <= regs_out.out3_del_o;
        pul_wid(3)            <= regs_out.wid_3_wid_o;
        pul_srce(3)           <= regs_out.wid_3_srce_o;
        pul_pol(3)            <= regs_out.wid_3_pol_o;
        pul_mode(3)           <= regs_out.wid_3_mode_o;
        pul_outen(3)          <= regs_out.wid_3_outen_o;
        
        rego_out4_del         <= regs_out.out4_del_o;
        pul_wid(4)            <= regs_out.wid_4_wid_o;
        pul_srce(4)           <= regs_out.wid_4_srce_o;
        pul_pol(4)            <= regs_out.wid_4_pol_o;
        pul_mode(4)           <= regs_out.wid_4_mode_o;
        pul_outen(4)          <= regs_out.wid_4_outen_o;
        rego_out5_del         <= regs_out.out5_del_o;
        pul_wid(5)            <= regs_out.wid_5_wid_o;
        pul_srce(5)           <= regs_out.wid_5_srce_o;
        pul_pol(5)            <= regs_out.wid_5_pol_o;
        pul_mode(5)           <= regs_out.wid_5_mode_o;
        pul_outen(5)          <= regs_out.wid_5_outen_o;
        rego_out6_del         <= regs_out.out6_del_o;
        pul_wid(6)            <= regs_out.wid_6_wid_o;
        pul_srce(6)           <= regs_out.wid_6_srce_o;
        pul_pol(6)            <= regs_out.wid_6_pol_o;
        pul_mode(6)           <= regs_out.wid_6_mode_o;
        pul_outen(6)          <= regs_out.wid_6_outen_o;
        rego_out7_del         <= regs_out.out7_del_o;
        pul_wid(7)            <= regs_out.wid_7_wid_o;
        pul_srce(7)           <= regs_out.wid_7_srce_o;
        pul_pol(7)            <= regs_out.wid_7_pol_o;
        pul_mode(7)           <= regs_out.wid_7_mode_o;
        pul_outen(7)          <= regs_out.wid_7_outen_o;
        rego_out8_del         <= regs_out.out8_del_o;
        pul_wid(8)            <= regs_out.wid_8_wid_o;
        pul_srce(8)           <= regs_out.wid_8_srce_o;
        pul_pol(8)            <= regs_out.wid_8_pol_o;
        pul_mode(8)           <= regs_out.wid_8_mode_o;
        pul_outen(8)          <= regs_out.wid_8_outen_o;
        rego_out9_del         <= regs_out.out9_del_o;
        pul_wid(9)            <= regs_out.wid_9_wid_o;
        pul_srce(9)           <= regs_out.wid_9_srce_o;
        pul_evt(9)            <= regs_out.wid_9_evt_o;
        pul_pol(9)            <= regs_out.wid_9_pol_o;
        pul_mode(9)           <= regs_out.wid_9_mode_o;
        pul_outen(9)          <= regs_out.wid_9_outen_o;
        rego_out10_del        <= regs_out.out10_del_o;
        pul_wid(10)           <= regs_out.wid_10_wid_o;
        pul_srce(10)          <= regs_out.wid_10_srce_o;
        pul_evt(10)           <= regs_out.wid_10_evt_o;
        pul_pol(10)           <= regs_out.wid_10_pol_o;
        pul_mode(10)          <= regs_out.wid_10_mode_o;
        pul_outen(10)         <= regs_out.wid_10_outen_o;
        rego_out11_del        <= regs_out.out11_del_o;
        pul_wid(11)           <= regs_out.wid_11_wid_o;
        pul_srce(11)          <= regs_out.wid_11_srce_o;
        pul_evt(11)           <= regs_out.wid_11_evt_o;
        pul_pol(11)           <= regs_out.wid_11_pol_o;
        pul_mode(11)          <= regs_out.wid_11_mode_o;
        pul_outen(11)         <= regs_out.wid_11_outen_o;
        rego_out12_del        <= regs_out.out12_del_o;
        pul_wid(12)           <= regs_out.wid_12_wid_o;
        pul_srce(12)          <= regs_out.wid_12_srce_o;
        pul_evt(12)           <= regs_out.wid_12_evt_o;
        pul_pol(12)           <= regs_out.wid_12_pol_o;
        pul_mode(12)          <= regs_out.wid_12_mode_o;
        pul_outen(12)         <= regs_out.wid_12_outen_o;        
      end if; 
    end if; 
  end process;

-- WR CLOCK DOMAIN
-- transfer bunch number pointed to in quiet time
-- **************************************************************************
  process(clk_sys_i)          -- WR CLOCK DOMAIN
  begin
    if clk_sys_i'event and clk_sys_i = '1' then
      if inj_run = '0' then
        regs_in.csr2_bunb_i   <= buclk_bun_data;
      end if;
		end if;
  end process;
  
-- T0 4 Hz watch dog
-- **************************************************************************
  process(clk_sys_i)          -- WR CLOCK DOMAIN
  begin
    if clk_sys_i'event and clk_sys_i = '1' then
      if regs_out.ctrl_t0wden_o = '1' then
        if master_t0_i = '1' then
          wd4hz    <= (others => '0');
        else
          wd4hz    <= wd4hz + 1;
        end if;
        -- 25000000*clk_sys = 0.4 s ; 1800000h = 25165824 => 0.4026 s : t0wdtrig default = 0110
        if wd4hz(25 downto 22) = regs_out.ctrl_t0wdtrig_o then
          t0wd     <= '1';
        end if;
      else
        t0wd       <= '0';
        wd4hz      <= (others => '0');
      end if;
		end if;
  end process;
  
-- Slave PLL watch dog : latch AD9510 PLL status bit
-- **************************************************************************
  process(clk_sys_i)          -- WR CLOCK DOMAIN
  begin
    if clk_sys_i'event and clk_sys_i = '1' then
      if regs_out.ctrl_ddslkden_o = '1' then
        if pll_dld_i = '1' then
          regs_in.csr1_plloss_i    <= '1';
        end if;
      else
        regs_in.csr1_plloss_i      <= '0';
      end if;
		end if;
  end process;
  
-- **************************************************************************
--       BUCLK LOGIC
  
-- RF/8 CLOCK DOMAIN
-- little RF/8 pre-RF pipeline process to avoid some timing errors
-- **************************************************************************
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' then
        gun_sum             <= '0';
        ms_start_pps_a      <= '0';
        ms_start_pps_b      <= '0';
        ms_start_pps_c      <= '0';
      else
        gun_sum             <= (gun_pipinj and guni_en) or gun_mb;
        ms_start_pps_a      <= master_start_pps or slave_start_pps;
        ms_start_pps_b      <= master_start_pps or slave_start_pps;
        ms_start_pps_c      <= master_start_pps or slave_start_pps;
      end if;
		end if;
  end process;

-- RF CLOCK DOMAIN
-- gunprocess
-- **************************************************************************
  process(buclk_352_i)        -- "true" RF
  begin
    if buclk_352_i'event and buclk_352_i = '1' then
      m_st_352            <= ms_start_pps_a;                       -- clock domain change
      m_st_1              <= m_st_352;
      m_st_pps_352        <= m_st_1 and not(m_st_352);  -- falling edge of RF/8 monostable !!
      gun_sum_352         <= gun_sum;
      pulb_1_352          <= pulb_123_o(1);
      pulb_2_352          <= pulb_123_o(2);
      pulb_3_352          <= pulb_123_o(3);
      cnt_adj_352         <= regs_out.ctrl_rfdomadj_o;
      ph_adj_352          <= regs_out.ctrl_ckphadj_o;
--      bunch_bpf_352       <= buclk_bun_data(2 downto 0);
--      bunch_1st_352       <= buclk_1st_data; v3-81 suppressed
      if rst_n_i = '0' or m_st_pps_352 = '1' then
				cnt_bun     	    <= cnt_adj_352;
				cnt_ckph7       	<= ph_adj_352;
				cnt_7           	<= TROIS;
        guneto            <= '0';
        cnt_7b            <= '0';
      else
  	    cnt_bun           <= cnt_bun + 1;      -- bunch counter
  	    cnt_ckph7         <= cnt_ckph7 + 1;    -- 8 counter
  	    cnt_7             <= cnt_7 + 1;        -- 8 counter
        if cnt_7(2 downto 0) = "011" then  -- gun_sum_352 fuzzy alignment to gun_inj_352 good alignment
          cnt_7b          <= '1';
        else
          cnt_7b          <= '0';
        end if;
        if cnt_7b = '1' then              -- cnt_7 = "100"
          gun_inj_352     <= gun_sum_352; -- 1 more pipe but all in 352 domain avoids bad guns
          pulb_rf1_352    <= pulb_1_352; -- 1 more pipe but all in 352 domain avoids bad guns
          pulb_rf2_352    <= pulb_2_352; -- 1 more pipe but all in 352 domain avoids bad guns
          pulb_rf3_352    <= pulb_3_352; -- 1 more pipe but all in 352 domain avoids bad guns
        end if;
--        if cnt_bun = bunch_bpf_352 then  -- inside  RF/8  period : whatever ctrl_mb
        if cnt_bun = buclk_bun_data(2 downto 0) then  -- inside  RF/8  period : whatever ctrl_mb
          guneto          <= gun_inj_352;
        else
          guneto          <= '0';
        end if;
--        if cnt_bun = bunch_1st_352 then  -- v3-81 suppressed - inside  RF/8  period : whatever ctrl_mb
        if cnt_bun = buclk_1st_data then  -- inside  RF/8  period : whatever ctrl_mb
          pulb_rf_o(1)    <= pulb_rf1_352; -- RF/8 wide RF precise
          pulb_rf_o(2)    <= pulb_rf2_352; -- RF/8 wide RF precise
          pulb_rf_o(3)    <= pulb_rf3_352; -- RF/8 wide RF precise
        end if;
        gunetob           <= guneto;
        gunetoc           <= guneto or gunetob;
        gunetod           <= guneto or gunetoc;
        guno              <= guneto or gunetod;
	 	  end if;
		end if;
  end process;

-- RF CLOCK DOMAIN
-- fine delay on clk_16b/4b channels[1] only
-- clk_4/16b specific to channel#1
  process(buclk_352_i)        -- "true" RF
  begin
    if buclk_352_i'event and buclk_352_i = '1' then
      if rst_n_i = '0' or  m_st_pps_352 = '1' then
        clk_4b            <= '0';
        clk_4p            <= '0';
        clk_16b           <= '0';
        clk_42            <= '0';
        clk_162           <= '0';
				cnt_b31           <= (others => '0');
        cnt_3132          <= (others => '0');
        cnt31_reset_352   <= '0';
        cnt31_raz         <= '0';
	    else
        cnt31_reset_352   <= cnt31_reset;  -- RF/8 to RF
        if cnt_7b = '1' then  -- outside metastabs
          cnt31_raz       <= cnt31_reset_352;  -- raz is true RF domain reset, SR phased
        end if;
        if cnt31_raz = '1' then
          cnt_b31         <= (others => '0');
          cnt_3132        <= (others => '0');
  	    elsif cnt_b31 < "11110" then              -- 31 counter 0 to 1E
  	      cnt_b31         <= cnt_b31 + 1;
        else
  	      cnt_b31         <= (others => '0');
          if cnt_3132 = "11111" then  -- super period for cnt_b31
            cnt_3132      <= (others => '0');
          else
            cnt_3132      <= cnt_3132 + 1;
          end if;
        end if;
--	    if cnt_b31 = pul_del(1)(1 downto 0) & tog31_ph(1) then
--        clk_162         <= cnt_3132(0);
--        if pul_del(1)(3 downto 2) = cnt_3132(1 downto 0) then
--          clk_42        <= cnt_3132(2);
--        end if;
--      end if;
  	    if cnt_b31 = pul_del(1)(1 downto 0) & tog31_ph(1) then
          clk_162         <= cnt_3132(0);
        end if;
        if pul_del(1)(3 downto 2) = cnt_3132(1 downto 0) then
          clk_4p          <= '1';
        else
          clk_4p          <= '0';
        end if;
        if clk_4p = '1' then
          clk_42          <= cnt_3132(2);
        end if;
        clk_16b           <= (clk_162 xor pul_del(1)(2)) and pul_outen(1);
        clk_4b            <= (clk_42 xor pul_del(1)(4)) and pul_outen(1);
      end if;
		end if;
  end process;

-- RF CLOCK DOMAIN
-- fine delay on clkb_o channels[1..3]
-- **************************************************************************
  fd_horl : for K in 1 to 3 generate
  process(buclk_352_i)        -- "true" RF
  begin
    if buclk_352_i'event and buclk_352_i = '1' then
--      tog31_ph_352(K)    <= tog31_ph(K); suppressed v3-82
      if pul_outen(K) = '1'  then
        fckph_352(K)     <= fckph_1(K);
        if cnt_7b = '1' then  -- same trick as gun_inj_352
          clkbph_352(K)  <= fckph_352(K); -- 
        end if;                    
--        if cnt_ckph7 = tog31_ph_352(K) then suppressed v3-82
        if cnt_ckph7 = tog31_ph(K) then -- modified for v3-82
          clkb_o(K)      <= clkbph_352(K);
        end if;
      else
        clkb_o(K)        <= '0';
        fckph_352(K)     <= '0';
        clkbph_352(K)    <= '0';
      end if;
    end if;
  end process;
  end generate;

-- RF CLOCK DOMAIN
-- output clocks
-- output sequencer pulses
-- RF tuned clocks
-- **************************************************************************
  about992 : for K in 1 to g_pulses generate
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or pul_mode(K)(1) = '0' or pul_mode(K)(0) = '0' then  -- mode 11 is clock
        hsr_togate(K)        <= '0';
        hsr_pi_togate(K)     <= '0';
        cnt_992(K)           <= (others => '0');  -- programmable SR counts
        cnt_448(K)           <= (others => '0');  -- programmable SR/2 counts
        kontour(K)           <= '0';              -- tc SR counts
      else
        if ms_start_pps_a = '1' or sync_src_pulse = '1' then    -- SR_COUNT clocks rephase
				  cnt_992(K)         <= (others => '0');
        else
          if cnt8_sr = "1111011" then -- end of tour : 7Bh=123 for 124x8 in SR
            if cnt_992(K) < div_992(K) then       -- ko frequency
              cnt_992(K)     <= cnt_992(K) +1;
            else
              cnt_992(K)     <= (others => '0');
            end if;
            if cnt_992(K) = pul_wid(K)(21 downto 0) then       -- ko phase SR pitch
              kontour(K)     <= '1';              -- one SR long
            else
              kontour(K)     <= '0';
            end if;
          end if;
          if cnt8_hsr = "111101" then      -- half tour : 3Dh=61 for 62x8 in 1/2SR
            if cnt8_sr = "1111011" and cnt_992(K) = pul_wid(K)(21 downto 0) then
              cnt_448(K)     <= (others => '0');
              hsr_togate(K)  <= '1';
            elsif cnt_448(K) < div_992(K) then       -- same division factor as 992
              cnt_448(K)     <= cnt_448(K) +1;
              hsr_togate(K)  <= '0';
            else
              cnt_448(K)     <= (others => '0');
              hsr_togate(K)  <= '1';
            end if;
            hsr_pi_togate(K) <= hsr_togate(K);
          end if;
        end if;
      end if;
		end if;
  end process;
  end generate;

  horloges : for K in 4 to g_pulses generate
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or pul_mode(K)(1) = '0' or pul_mode(K)(0) = '0' then  -- mode 11 is clock
        clkb_o(K)            <= '0';
        clkb(K)              <= '0';
      else        
        if (pul_srce(K) = "001") then
          if (pul_del(K)(6 downto 0) = cnt8_sr) then   -- phased 50% ck_sr
            clkb(K)          <= pul_pol(K) and pul_outen(K);
          elsif (pul_del(K)(13 downto 7) = cnt8_sr) then   -- phased 50% ck_sr
            clkb(K)          <= not(pul_pol(K)) and pul_outen(K);
          end if;
        elsif (pul_srce(K) = "010") then
          if (pul_del(K)(5 downto 0) = cnt8_boo) then  -- phased 50% ck_boo
            clkb(K)          <= pul_pol(K) and pul_outen(K);
          elsif (pul_del(K)(12 downto 7) = cnt8_boo) then -- phased 50% ck_boo
            clkb(K)          <= not(pul_pol(K)) and pul_outen(K);
          end if;
        elsif (pul_srce(K) = "011") then
          if (pul_del(K)(5 downto 0) = cnt8_hsr) and
              ((pul_del(K)(6) = '0' and hsr_togate(K) = '1') or (pul_del(K)(6) = '1' and hsr_pi_togate(K) = '1'))then
            if (kontour(K) = '1')  then -- phase clk_sr_div 50%
              clkb(K)          <= pul_outen(K);
            else                        -- phase clk_sr_div 50%
              clkb(K)          <= pul_outen(K) and not(clkb(K)); -- toggle on 1/2SR
            end if;
          end if;
        elsif (pul_srce(K) = "000") and (pul_del(K)(1 downto 0) = cnt8_boo(1 downto 0)) then -- tc_32
          clkb(K)            <= pul_outen(K);
        else
          clkb(K)            <= '0';
        end if;
        clkb_o(K)            <= clkb(K);
      end if;
		end if;
  end process;
  end generate;
      
-- fine delay clock channels[1..3]
  fdhorloges : for H in 1 to 3 generate
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or pul_mode(H)(1) = '0' or pul_mode(H)(0) = '0' then  -- mode 11 is clock
        fckph(H)                <= '0';
        fckph_1(H)              <= '0';
       else
        if (pul_srce(H) = "001") then
          if (pul_del(H)(6 downto 0) = cnt8_sr) then   -- phased 50% ck_sr
            fckph(H)            <= pul_pol(H);
          elsif (pul_del(H)(13 downto 7) = cnt8_sr) then   -- phased 50% ck_sr
            fckph(H)            <= not(pul_pol(H));
          end if;
        elsif (pul_srce(H) = "010") then
          if (pul_del(H)(5 downto 0) = cnt8_boo) then  -- phased 50% ck_boo
            fckph(H)            <= pul_pol(H);
          elsif (pul_del(H)(12 downto 7) = cnt8_boo) then  -- phased 50% ck_boo
            fckph(H)            <= not(pul_pol(H));
          end if;
        elsif (pul_srce(H) = "011") then
          if (pul_del(H)(5 downto 0) = cnt8_hsr) and
              ((pul_del(H)(6) = '0' and hsr_togate(H) = '1') or (pul_del(H)(6) = '1' and hsr_pi_togate(H) = '1'))then
            if (kontour(H) = '1')  then -- phase clk_sr_div 50%
              fckph(H)          <= pul_outen(H);
            else                        -- phase clk_sr_div 50%
              fckph(H)          <= pul_outen(H) and not(fckph(H)); -- toggle on 1/2SR
            end if;
          end if;
        elsif (pul_srce(H) = "000") and (pul_del(H)(1 downto 0) = cnt8_boo(1 downto 0)) then -- tc_32
          fckph(H)              <= '1';
        else
          fckph(H)              <= '0';
        end if;
        fckph_1(H)              <= fckph(H);
      end if;
		end if;
  end process;
  end generate;

-- combinatorial for sequencer pulses : mode=10 (pulse) ; source=1xx (sequencer)
  pulseqs : for K in 4 to g_pulses generate
  pulseq_sel(K)      <= pul_outen(K) & pul_pol(K) & pul_mode(K)(1 downto 0) & pul_srce(K)(2 downto 0);
  with pulseq_sel(K) select
	pulb_seq(K)  <=       t_inj             when "1010101",
                        t_ext             when "1010110",
                        t0sa              when "1010100",
                        gun_pinj          when "1010111",
                        not(t_inj)        when "1110101",
                        not(t_ext)        when "1110110",
                        not(t0sa)         when "1110100",
                        not(gun_pinj)     when "1110111",
                        '0'               when others;
  end generate;

-- RF/8 CLOCK DOMAIN
-- Main bunch clock core - adapted to RfoE for network synchronous start on PPS
-- ****************************************************************************
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      buclk_run             <= wr_link and regs_out.ctrl_rfen_o and pll_352_lkd_i; -- main reset
      if rst_n_i = '0' or buclk_run = '0' then
				cnt_bu44      	    <= (others => '0');
--				cnt_bu44_stampin    <= (others => '0');
	    else
--        cnt_bu44_stampin    <= cnt_bu44;  -- pipe to unload cnt_bu44 fanout
        bu44_adj_en         <= slok_adj_en or dbg_adj_en; -- both are monostables
        bu44_adj_0          <= (slok_adj_0_i and not(dbg_adj_en)) or (dbg_adj_en and dbg_adj_0); -- dbg_adj_xx monostable
        bu44_adj_1          <= (slok_adj_1_i and not(dbg_adj_en)) or (dbg_adj_en and dbg_adj_1); -- dbg_adj_xx monostable
        if ms_start_pps_b = '1' then           -- for counters to start at PPS everywhere
				  cnt_bu44     	    <= cnt_bu44_ini;
        else
          cnt_bu44_incr(0)  <= not(skip44_1rf) and (not(bu44_adj_en) or (bu44_adj_en and not(bu44_adj_0) and not(bu44_adj_1)));
          cnt_bu44_incr(1)  <= jump44_1rf or (bu44_adj_en and bu44_adj_1);
          cnt_bu44          <= std_logic_vector(unsigned(cnt_bu44) + unsigned(cnt_bu44_incr)); -- incr = 0, 1 or 2
				end if;
	 	  end if;
		end if;
  end process;
-- booster clock active rephasing by minus one steps
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' then
				cnt8_boo      	    <= (others => '0');
	    else
        if ms_start_pps_c = '1' then           -- for counters to start at PPS everywhere
				  cnt8_boo          <= (others => '0');
        elsif bo8_moins_un = '0' then
          if cnt8_boo < "101011" then             -- 2Bh=43 for 44x8 in booster
            cnt8_boo        <=  cnt8_boo + 1;
          else
            cnt8_boo        <= (others => '0');
          end if;
				end if;
	 	  end if;
		end if;
  end process;
-- SR clock active rephasing by minus one steps
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' then
				cnt8_sr       	    <= (others => '0'); -- SR counter
				cnt8_hsr            <= (others => '0'); -- half SR counter
        cnt8_sr_tc          <= '0';
        sr_aok_a            <= '0';
        sr_aok_rise         <= '0';
        cnt31_reset         <= '0';
	    else
        sr_aok_a            <= sr_aok;
        if sr_aok = '1' and sr_aok_a = '0' then
          sr_aok_rise       <= '1';  -- record rising edge
        end if;
        if ms_start_pps_b = '1' then           -- for counters to start at PPS everywhere
				  cnt8_sr           <= (others => '0');
				  cnt8_hsr          <= (others => '0');
          cnt31_reset       <= '1';     -- for master to stay in phase with slaves
        else
          if sr8_moins_un = '0' then
            if cnt8_sr < "1111011" then             -- 7Bh=123 for 124x8 in SR
              cnt8_sr         <= cnt8_sr + 1;
              cnt8_sr_tc      <= '0';       -- monostable phased tc
            else
              cnt8_sr         <= (others => '0');
              cnt8_sr_tc      <= '1';
            end if;
            if cnt8_hsr < "111101" then             -- 3Dh=61 for 62x8 in 1/2SR
              cnt8_hsr        <= cnt8_hsr + 1;
            else
              cnt8_hsr        <= (others => '0');
            end if;
          end if;
          if sr_aok_rise = '1' and cnt8_sr_tc = '1' then
            sr_aok_rise       <= '0';
            cnt31_reset       <= '1';
          else
            cnt31_reset       <= '0';  -- rf/8 monostable whatever rising edge cause
          end if;
        end if;
	 	  end if;
		end if;
  end process;
 
-- RF/8 CLOCK DOMAIN
-- output pulses
-- provide coarse envelop / phasing at RF/8 for tuned pulses
-- ***********************************************************
  horions : for K in 1 to 8 generate
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' then
        pulb_inj(K)        <= '0';
        pulb_pinj(K)       <= '0';
        pulb_ext(K)        <= '0';
        pulb_tt0(K)        <= '0';
      else
        if pul_mode(K)(1) = '1' and pul_mode(K)(0) = '0' then -- pulse mode
          if inj_run = '1' then
            if pul_srce(K) = "001" then                  -- inj related pulse (priority encode)
              pulb_inj(K)                  <= '1';
              pulb_pinj(K)                 <= '0';
              pulb_ext(K)                  <= '0';
              pulb_tt0(K)                  <= '0';
            elsif pul_srce(K) = "010" then               -- ext related pulse
              pulb_inj(K)                  <= '0';
              pulb_pinj(K)                 <= '0';
              pulb_ext(K)                  <= '1';
              pulb_tt0(K)                  <= '0';
            elsif pul_srce(K) = "011" then               -- tk_inj related pulse
              pulb_inj(K)                  <= '0';
              pulb_pinj(K)                 <= '1';
              pulb_ext(K)                  <= '0';
              pulb_tt0(K)                  <= '0';
            else                                         -- mind pul_seq programming on srce=1xx
              pulb_inj(K)                  <= '0';
              pulb_pinj(K)                 <= '0';
              pulb_ext(K)                  <= '0';
            end if;
          end if;
          if tt0_wait = '1' and pul_srce(K) = "000" then   -- tt0 related pulse, outside inj_run
            pulb_inj(K)                  <= '0';
            pulb_pinj(K)                 <= '0';
            pulb_ext(K)                  <= '0';
            pulb_tt0(K)                  <= '1';
          end if;
          if pulb_end(K) = '1' then                      -- non tt0 pulse
            pulb_inj(K)                  <= '0';
            pulb_pinj(K)                 <= '0';
            pulb_ext(K)                  <= '0';
          end if;
          if pultt0_done(K) = '1' then                   -- tt0 pulse : done exclusive with wait
            pulb_tt0(K)                  <= '0';
          end if;
        else
          pulb_inj(K)                    <= '0';
          pulb_pinj(K)                   <= '0';
          pulb_ext(K)                    <= '0';
          pulb_tt0(K)                    <= '0';
        end if;
      end if;
		end if;
  end process;
  end generate;

  horioevts : for K in 9 to g_pulses generate
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' then
        pulb_inj(K)        <= '0';
        pulb_pinj(K)       <= '0';
        pulb_ext(K)        <= '0';
        pulb_tt0(K)        <= '0';
      else
        if pul_mode(K)(1) = '1' and pul_mode(K)(0) = '0' and pul_evt(K) = '0' then -- pulse mode
          if inj_run = '1' then
            if pul_srce(K) = "001" then                  -- inj related pulse (priority encode)
              pulb_inj(K)                  <= '1';
              pulb_pinj(K)                 <= '0';
              pulb_ext(K)                  <= '0';
              pulb_tt0(K)                  <= '0';
            elsif pul_srce(K) = "010" then               -- ext related pulse
              pulb_inj(K)                  <= '0';
              pulb_pinj(K)                 <= '0';
              pulb_ext(K)                  <= '1';
              pulb_tt0(K)                  <= '0';
            elsif pul_srce(K) = "011" then               -- tk_inj related pulse
              pulb_inj(K)                  <= '0';
              pulb_pinj(K)                 <= '1';
              pulb_ext(K)                  <= '0';
              pulb_tt0(K)                  <= '0';
            else                                         -- mind pul_seq programming on srce=1xx
              pulb_inj(K)                  <= '0';
              pulb_pinj(K)                 <= '0';
              pulb_ext(K)                  <= '0';
            end if;
          end if;
          if tt0_wait = '1' and pul_srce(K) = "000" then   -- tt0 related pulse, outside inj_run
            pulb_inj(K)                  <= '0';
            pulb_pinj(K)                 <= '0';
            pulb_ext(K)                  <= '0';
            pulb_tt0(K)                  <= '1';
          end if;
          if pulb_end(K) = '1' then                      -- non tt0 pulse
            pulb_inj(K)                  <= '0';
            pulb_pinj(K)                 <= '0';
            pulb_ext(K)                  <= '0';
          end if;
          if pultt0_done(K) = '1' then                   -- tt0 pulse : done exclusive with wait
            pulb_tt0(K)                  <= '0';
          end if;
        else
          pulb_inj(K)                    <= '0';
          pulb_pinj(K)                   <= '0';
          pulb_ext(K)                    <= '0';
          pulb_tt0(K)                    <= '0';
        end if;
      end if;
		end if;
  end process;
  end generate;

-- RF/8 CLOCK DOMAIN pulses
-- output pulses 4 to 8 then 9-12
-- **************************************************************************
  slimpulsions : for K in 4 to 8 generate
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or pul_mode(K)(1) = '0' or pul_mode(K)(0) = '1' or pul_outen(K) = '0' then  -- defines pulse mode
        pulb_o(K)            <= pul_pol(K);
        pulb_end(K)          <= '0';
        pulb_gate(K)         <= '0';
        pultt0(K)            <= '0';
        pultt0_done(K)       <= '0';
        pulb_wid_cnt(K)      <= (others => '0');
        pulb_del_cnt(K)      <= (others => '0');
      else
--      *************************************************** inj related pulse
        if pulb_inj(K) = '1' and pulb_gate(K) = '0' and gun_inj = '1' then  -- gun_inj referred not gun_pinj
          pulb_gate(K)           <= '1';
        end if;
--      *************************************************** Ki related pulse
        if pulb_pinj(K) = '1' and pulb_gate(K) = '0' and tk_inj = '1' then  -- tk_inj referred for Ki
          pulb_gate(K)           <= '1';
        end if;
--      *************************************************** ext related pulse
        if pulb_ext(K) = '1' and pulb_gate(K) = '0' and t_ext = '1' then  -- ext 
          pulb_gate(K)           <= '1';
        end if;
--      *************************************************** t0 related pulse
        if pulb_tt0(K) = '1' and pulb_gate(K) = '0' and tt0 = '1' then -- TT0 is monostable...
          pulb_gate(K)           <= '1';
          pultt0(K)              <= '1'; -- to discriminate with other pulses
        end if;
--      *************************************************** start pulse after delay
        if pulb_gate(K) = '1' and pulb_o(K) = pul_pol(K) and pulb_end(K) = '0' and pultt0_done(K) = '0' then
          if (pul_del(K) = pulb_del_cnt(K)) then        -- delay condition possibly 0
            pulb_o(K)            <= not(pul_pol(K));
          else
            pulb_del_cnt(K)      <= pulb_del_cnt(K) + 1;
          end if;
        end if ;
--      *************************************************** now end of pulse
        if pulb_o(K) = not(pul_pol(K)) then
          pulb_wid_cnt(K)        <= pulb_wid_cnt(K) + 1;
          if pul_wid(K) = pulb_wid_cnt(K) then
            pulb_o(K)            <=  pul_pol(K);
            if pultt0(K) = '1' then -- tt0 pulse independent of inj_run
              pultt0_done(K)     <= '1'; --
            else
              pulb_end(K)        <= '1';
            end if;
          end if;
        else
          pulb_wid_cnt(K)        <= (others => '0');
        end if;
        if tt0_wait = '1' then 
          pultt0_done(K)         <= '0'; -- ready for new tt0 pulse
        end if;
        if pulb_end(K) = '1' and inj_run = '0' then  -- non tt0 pulse once in cycle
          pulb_end(K)          <= '0';
          pulb_gate(K)         <= '0';
          pulb_del_cnt(K)      <= (others => '0');
        end if;
        if pultt0_done(K) = '1' then -- tt0 pulse : done reset by tt0_wait
          pultt0(K)            <= '0';
          pulb_gate(K)         <= '0'; -- also on tt0 pulse
          pulb_del_cnt(K)      <= (others => '0');            
        end if;
	 	  end if;
		end if;
  end process;
  end generate;

  evtpulsions : for K in 9 to g_pulses generate
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or pul_mode(K)(1) = '0' or pul_mode(K)(0) = '1' or pul_outen(K) = '0' then  -- defines pulse mode
        pulb_o(K)            <= pul_pol(K);
        pulb_end(K)          <= '0';
        pulb_gate(K)         <= '0';
        pultt0(K)            <= '0';
        pulevt(K)            <= '0';
        pultt0_done(K)       <= '0';
        pulb_wid_cnt(K)      <= (others => '0');
        pulb_del_cnt(K)      <= (others => '0');
      else
--      *************************************************** inj related pulse
        if pulb_inj(K) = '1' and pulb_gate(K) = '0' and gun_inj = '1' then  -- gun_inj referred not gun_pinj
          pulb_gate(K)           <= '1';
        end if;
--      *************************************************** Ki related pulse
        if pulb_pinj(K) = '1' and pulb_gate(K) = '0' and tk_inj = '1' then  -- tk_inj referred for Ki
          pulb_gate(K)           <= '1';
        end if;
--      *************************************************** ext related pulse
        if pulb_ext(K) = '1' and pulb_gate(K) = '0' and t_ext = '1' then  -- ext 
          pulb_gate(K)           <= '1';
        end if;
--      *************************************************** t0 related pulse
        if pulb_tt0(K) = '1' and pulb_gate(K) = '0' and tt0 = '1' then -- TT0 is monostable...
          pulb_gate(K)           <= '1';
          pultt0(K)              <= '1'; -- to discriminate with other pulses
        end if;
--      *************************************************** evt related pulse exclusive to others
        if pul_evt(K) = '1' and pulb_gate(K) = '0' and evt_i(K-8) = '1' then -- evt is monostable...
          -- evt ignored if preceding still beeing processed (pulb_gate=1) ...
          -- evt#1-2-3-4 to output#9-10-11-12
          pulb_gate(K)           <= '1';
          pulevt(K)              <= '1'; -- to discriminate with other pulses
        end if;
--      *************************************************** start pulse after delay
        if pulb_gate(K) = '1' and pulb_o(K) = pul_pol(K) and pulb_end(K) = '0' and pultt0_done(K) = '0' then
          if (pul_del(K) = pulb_del_cnt(K)) then        -- delay condition possibly 0
            pulb_o(K)            <= not(pul_pol(K));
          else
            pulb_del_cnt(K)      <= pulb_del_cnt(K) + 1;
          end if;
        end if ;
--      *************************************************** now end of pulse
        if pulb_o(K) = not(pul_pol(K)) then
          pulb_wid_cnt(K)        <= pulb_wid_cnt(K) + 1;
          if pul_wid(K) = pulb_wid_cnt(K) then
            pulb_o(K)            <=  pul_pol(K);
            if pultt0(K) = '1' then -- tt0 pulse independent of inj_run
              pultt0_done(K)     <= '1'; --
            else
              pulb_end(K)        <= '1';
            end if;
          end if;
        else
          pulb_wid_cnt(K)        <= (others => '0');
        end if;
        if tt0_wait = '1' then 
          pultt0_done(K)         <= '0'; -- ready for new tt0 pulse
        end if;
        if pulb_end(K) = '1' then
          if (pulevt(K) = '1') or (inj_run = '0' and pulevt(K) = '0') then  -- inj_run pulse once in cycle
            pulb_end(K)          <= '0';
            pulb_gate(K)         <= '0';
            pulevt(K)            <= '0';
            pulb_del_cnt(K)      <= (others => '0');
          end if;
        end if;
        if pultt0_done(K) = '1' then -- tt0 pulse : done reset by tt0_wait
          pultt0(K)            <= '0';
          pulb_gate(K)         <= '0'; -- also on tt0 pulse
          pulb_del_cnt(K)      <= (others => '0');            
        end if;
	 	  end if;
		end if;
  end process;
  end generate;

-- RF/8 CLOCK DOMAIN output_2-3 pulse special
-- **************************************************************************
  impulsions23 : for T in 2 to 3 generate
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or pul_mode(T)(1) = '0' or pul_mode(T)(0) = '1' or pul_outen(T) = '0' then  -- defines pulse mode
        pulb_123_o(T)        <= pul_pol(T);
        pulb_end(T)          <= '0';
        pulb_gate(T)         <= '0';
        pultt0(T)            <= '0';
        pultt0_done(T)       <= '0';
        pulb_del_cnt(T)      <= (others => '0');
      else
--      *************************************************** Ki related pulse
        if pulb_pinj(T) = '1' and pulb_gate(T) = '0' and tk_inj = '1' then  -- tk_inj referred for Ki
          pulb_gate(T)           <= '1';
        end if;
--      *************Fast timing only : NO t0 related pulse
--      *************************************************** start pulse after delay
        if pulb_gate(T) = '1' and pulb_123_o(T) = pul_pol(T) and pulb_end(T) = '0' and pultt0_done(1) = '0' then
          if (pul_del(T) = pulb_del_cnt(T)) then        -- delay condition possibly 0
            pulb_123_o(T)        <= not(pul_pol(T));
            pulb_end(T)          <= '1'; -- no width, actual width = 1*RF/8
          else
            pulb_del_cnt(T)      <= pulb_del_cnt(T) + 1;
          end if;
        else
          pulb_123_o(T)          <= pul_pol(T); -- no width, actual width = 1*RF/8
        end if ;
--      *************************************************** now wait for end of sequence
        if pulb_end(T) = '1' and inj_run = '0' then  -- non tt0 pulse once in cycle
          pulb_end(T)          <= '0';
          pulb_gate(T)         <= '0';
          pulb_del_cnt(T)      <= (others => '0');
        end if;
	 	  end if;
		end if;
  end process;
  end generate;

-- RF/8 CLOCK DOMAIN output_1 pulse special
-- **************************************************************************
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or pul_mode(1)(1) = '0' or pul_mode(1)(0) = '1' or pul_outen(1) = '0' then  -- defines pulse mode
        pulb_123_o(1)        <= pul_pol(1);
        pulb_end(1)          <= '0';
        pulb_gate(1)         <= '0';
        pultt0(1)            <= '0';
        pultt0_done(1)       <= '0';
        pulb_del_cnt(1)      <= (others => '0');
      else
--      *************************************************** inj related pulse
        if pulb_inj(1) = '1' and pulb_gate(1) = '0' and gun_inj = '1' then  -- gun_inj referred not gun_pinj
          pulb_gate(1)           <= '1';
        end if;
--      *************************************************** Ki related pulse
        if pulb_pinj(1) = '1' and pulb_gate(1) = '0' and tk_inj = '1' then  -- tk_inj referred for Ki
          pulb_gate(1)           <= '1';
        end if;
--      *************************************************** ext related pulse
        if pulb_ext(1) = '1' and pulb_gate(1) = '0' and t_ext = '1' then  -- ext 
          pulb_gate(1)           <= '1';
        end if;
--      *************Fast timing only : NO t0 related pulse
--      *************************************************** start pulse after delay
        if pulb_gate(1) = '1' and pulb_123_o(1) = pul_pol(1) and pulb_end(1) = '0' and pultt0_done(1) = '0' then
          if (pul_del(1) = pulb_del_cnt(1)) then        -- delay condition possibly 0
            pulb_123_o(1)        <= not(pul_pol(1));
            pulb_end(1)          <= '1'; -- no width, actual width = 1*RF/8
          else
            pulb_del_cnt(1)      <= pulb_del_cnt(1) + 1;
          end if;
        else
          pulb_123_o(1)          <= pul_pol(1); -- no width, actual width = 1*RF/8
        end if ;
--      *************************************************** now wait for end of sequence
        if pulb_end(1) = '1' and inj_run = '0' then  -- non tt0 pulse once in cycle
          pulb_end(1)          <= '0';
          pulb_gate(1)         <= '0';
          pulb_del_cnt(1)      <= (others => '0');
        end if;
	 	  end if;
		end if;
  end process;

-- little process for RF/8 resync of some static inputs
-- **************************************************************************
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' then
        bunch_en              <= '0';
        blist_add_mb          <= '0';
        blist_add_autorotinj  <= '0';
        guni_en               <= '0';
      else
        bunch_en              <= regs_out.ctrl_bunchen_o;
        blist_add_mb          <= regs_out.blistadd_mb_o;
        blist_add_autorotinj  <= regs_out.blistadd_autorotinj_o;
        guni_en               <= regs_out.ctrl_gunen_o;
        rf_adj_en             <= regs_out.csr0_rfadjen_o;
        rf_adj_stop           <= regs_out.csr0_rfadjstop_o;
        sync_src_en           <= regs_out.csr0_sync_src_en_o;
  	  end if;
		end if;
  end process;

-- RF/8 CLOCK DOMAIN
-- get T0 and calculate inj
-- **************************************************************************
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' then
				done_inj          <= '0';
				done_ext          <= '0';
				gun_inj           <= '0';
				fin_ext           <= '0';
				t_inj             <= '0';
  	    t_ext             <= '0';
  	    t0sa              <= '0';
  	    inj_run           <= '0';
  	    error_inj         <= '0';
        bu44_inj          <= (others => '0');
        bu44_ext          <= (others => '0');
   	    boo_inj_reg       <= (others => '0');
  	    cnt_wid_inj       <= (others => '0');
  	    cnt_del_ext       <= (others => '0');
  	    cnt_rf_ext        <= (others => '0');
  	    cnt_wid_ext       <= (others => '0');
			else
        if master_start_mem = '1' or rst_injerr_1rf = '1' then
          error_inj       <= '0';
        end if;
--      full sequence wrapper
				if t0_sync = '1' and error_inj = '0' and master_start_mem = '0' and  bunch_en = '1' then   -- T0 rising edge
					if inj_run = '0' and buclk_bun_data < X"3E0" then
						inj_run         <= '1';        -- start on T0 rise if no error
            buclk_bun_nb    <= buclk_bun_data(9 downto 0);
					else
						error_inj       <= '1';        -- sequence / T0 overlap, reset by buclk_en
					end if;
				end if;
-- full sequence end OK/end
				if done_ext = '1' and inj_run = '1' then
					inj_run           <= '0';
				end if;
-- end of full sequence end OK/end
--      full injection sequence start
				if inj_run = '1' then
--      start on resynced signal : gun sequence ending with done_inj
					if done_inj = '0' then
			 			if buclk_bun_nb(4 downto 3) = cnt_bu44(1 downto 0) then    --1 out of 4 
		 					gun_inj                     <= '1';        -- immediate gun
	 						boo_inj_reg                 <= cnt8_boo(5 downto 2);
              bu44_inj                    <= cnt_bu44(31 downto 0);
 							t_inj                       <= '1';        -- immediate trig
 							done_inj         <= '1';       -- injection done wihout waiting end of tinj<<del_ext
					 		cnt_wid_inj                 <= (others => '0');
						else
					 		gun_inj          <= '0';   -- one clock
				 		end if;
					else                                           -- done_inj =1
--        end injection sequence by closing t_inj according to width (<< extraction seq.)
            if cnt_wid_inj = rego_wid_tinj then
							t_inj         <= '0';                        -- actual size is regs_out.wid_cr_o+1
						else
							cnt_wid_inj   <= cnt_wid_inj + 1;      -- width counters on RF
						end if;
--      extraction sequence starting with done_inj=1
--      wait for extraction delay
            if cnt_rf_ext < SUPER and cnt_del_ext < rego_del_ext then  -- 1 super period ext cnt
							cnt_rf_ext        <= cnt_rf_ext + 1;
						elsif cnt_del_ext < rego_del_ext then
							cnt_rf_ext        <= (others => '0');
							cnt_del_ext       <= cnt_del_ext + 1;
						end if;
						if cnt_del_ext >= rego_del_ext then   -- >= to avoid stall risk
--      now wait for extraction coincidence
--      when cnt8_boo(5 downto 2) =  boo_inj_reg AND cnt8_sr(6 downto 2) = buclk_bun_nb
							if cnt8_boo(5 downto 2) = boo_inj_reg and done_ext = '0' and t_ext = '0' and
									cnt8_sr(6 downto 2) = buclk_bun_nb(9 downto 5) and
									cnt_bu44(1 downto 0) = buclk_bun_nb(4 downto 3) then            -- condition to be tested at 32-pack frontier
								t_ext                      <= '1';         -- starts at beginning of 32-pack number buclk_bun_nb+1
								fin_ext                    <= '1';         -- start last state
                bu44_ext                   <= cnt_bu44(31 downto 0);
								cnt_wid_ext                <= (others => '0');
							end if;
							if fin_ext = '1' then
								if cnt_wid_ext = rego_wid_text then  -- actual size is regs_out.wid_cr_o+1
									t_ext             <= '0';
									done_ext          <= '1';   -- end of full sequence
								else
									cnt_wid_ext       <= cnt_wid_ext + 1;      -- width counters on RF
								end if;
							end if;
						end if;
					end if;
				else
					cnt_del_ext       <= (others => '0');
					cnt_rf_ext        <= (others => '0');
					cnt_wid_ext       <= (others => '0');
					cnt_wid_inj       <= (others => '0');
					done_ext          <= '0';
					gun_inj           <= '0';
					done_inj          <= '0';
					fin_ext           <= '0';
				end if;
  	  end if;
		end if;
  end process;

-- injection rotation and gun delay process
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or ms_start_pps_c = '1' then
  	    cnt_del_inj       <= (others => '0');
				gun_pinj          <= '0';
				gun_pipinj        <= '0';
  	    gun_pinj_done     <= '0';
  	    next_roti         <= '0';
  	    pinj_del          <= '0';
  	    pinj_del_run      <= '0';
  	    moulin            <= (others => '0');
        roti_del_cnt      <= (others => '0');
        roti_pass         <= (others => '0');
        roti_val          <= (others => '0');
        tk_inj            <= '0';
      else
        if regs_out.blistadd_roticode_o = '1' then
          roti_val        <= buclk_bun_data(14 downto 13);
        else
          roti_val        <= roti_pass;
        end if;
        if rotinj_en = '0' then -- roti_pass not reset here
          roti_del_cnt    <= (others => '0');
          roti_pass       <= (others => '0'); -- to make sure all network in phase
        else
          if roti_val = "00" then
            moulin        <= ROTI40;
          elsif roti_val = "01" then
            moulin        <= ROTI41;
          elsif roti_val = "10" then
            moulin        <= ROTI42;
          elsif roti_val = "11" then
            moulin        <= ROTI43;
          end if;
        end if;
				if inj_run = '1' then
          if gun_inj = '1' then         -- 1 shot
            pinj_del_run                <= '1';  -- 1 shot memory up to seq end
          end if;
          if pinj_del_run = '1' then
            if regs_out.ctrl_rotinj_o = '0' or moulin = "000000" then -- immediate
              pinj_del                  <= '1';
            else                      -- run moulinette first
              if roti_del_cnt < moulin and pinj_del = '0' then
                roti_del_cnt            <= roti_del_cnt + 1;
              else
                pinj_del                <= '1';
                roti_del_cnt            <= (others => '0');
              end if;
            end if;
            pinj_del_a                  <= pinj_del;
            if pinj_del = '1' and pinj_del_a = '0' then  -- rising edge monostable
              tk_inj                    <= '1';
              buclk_1st_data            <= buclk_bun_data(2 downto 0);
              if blist_add_autorotinj = '1' then
                next_roti               <= '1';
              end if;
              if roti_pass(1 downto 0) = "11" then -- manage pass and next bunch number
                roti_pass               <= (others => '0');
              else
                roti_pass               <= roti_pass + 1;
              end if;
            else
              tk_inj                    <= '0'; -- monostable
            end if;
            -- now normal delay
            if pinj_del = '1' and cnt_del_inj < rego_del_gun and gun_pinj_done = '0' then
              cnt_del_inj           <= cnt_del_inj + 1;
            elsif pinj_del = '1' and cnt_del_inj = rego_del_gun and gun_pinj_done = '0' then 
              gun_pinj              <= '1';
              gun_pinj_done         <= '1';
            else
              gun_pinj              <= '0';
            end if;
          end if;
        else
          if next_roti_ack = '1' then
            next_roti               <= '0';
          end if;
          roti_del_cnt      <= (others => '0');
					cnt_del_inj       <= (others => '0');
					gun_pinj          <= '0';
					gun_pinj_done     <= '0';
					pinj_del          <= '0';
					pinj_del_run      <= '0';
        end if;
        gun_pipinj          <= gun_pinj;
      end if;
    end if;
  end process;
  
-- RF/8 CLOCK DOMAIN
-- extra process to add up to 11 pulses when multibunch bit set
-- for each added pulse : 16 bits : [15] = end of list ; [8..0] = delay to previous
-- ********************************************************************************
  process(buclk_sys_i)      -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
	    if rst_n_i = '0' or buclk_run = '0' or ms_start_pps_c = '1' then
        tac               <= tic0;
        trot              <= tic0;
        buclk_bun_addr    <= (others => '0');
        mb_en             <= '0';
        autoblist         <= '0';
        gun_mb            <= '0';
        rotinj_en         <= '0';
        start_mb_ini      <= '1';
        pas8              <= (others => '0');     -- counts packs of 8 with RF/8
        xbunch            <= (others => '0');     -- multi-bunch counter
        blist_start       <= (others => '0');
        blist_start_mb    <= (others => '0');
        rot_seq           <= "01"; -- counts additional pulses 1,2,3
			else
        autoblist            <= regs_out.blistadd_autoblist_o;     -- enter rf/8 clock domain
        blist_start          <= regs_out.blistadd_start_o;     -- enter rf/8 clock domain
        blist_stop           <= regs_out.blistadd_stop_o;      -- enter rf/8 clock domain
        start_mb_ini         <= '0';
        --set mb_en and bun_addr_start pointer in quiet time, + 1 clk after reset
        -- mb has priority over rotinj
        if inj_run = '0' and start_mb_ini = '0' then  -- 1 clk after reset
          if blist_add_mb = '1' then
            if mb_en = '0' then
              mb_en                  <= '1';
              rotinj_en              <= '0';
              blist_start_mb         <= blist_start;
            end if;
          else
            if mb_en = '1' then
              mb_en                  <= '0';
              buclk_bun_addr         <= blist_start;  -- with start of rotinj
            else
              -- set rotinj_en and bun_addr_start pointer in quiet time, + 1 clk after reset
              -- mb has priority
              if regs_out.ctrl_rotinj_o = '1' then
                if rotinj_en = '0' then
                  rotinj_en          <= '1';
                  buclk_bun_addr     <= blist_start;  -- with start of rotinj
                end if;
              else
                if rotinj_en = '1' then
                  rotinj_en          <= '0';
                  buclk_bun_addr     <= blist_start;  -- with start of rotinj
                else
                  rot_seq            <= "01"; -- counts additional pulses
                end if;
              end if;
            end if;
          end if;
        end if;
        -- next if outside preceding if because mblprst already validated by inj_run=0
        if mblprst_1rf = '1' then
          blist_start_mb         <= blist_start;
        end if;
        if mb_en = '1' then -- mb priority over single over rotinj
          trot                     <= tic0;
          case tac is
            when tic0 =>
              if autoblist = '0' then        -- define blist_start_mb when autoblist=0
                if blistrst_1rf = '1' then -- secured outside inj_run
                  blist_start_mb       <= blist_start;
                  -- in multi bunch mode no manual increment is allowed when autoblist=0
                  -- because next start_mb unknown, and feature with very poor interest
                  -- only reset is allowed in this mode
                end if;            
              end if;
              buclk_bun_addr       <= blist_start_mb; --will occur any time between end of sequence and next gun
              gun_mb               <= '0';
              xbunch               <= (others => '0');
              -- if bit15 ON then same as single bunch = no mb start = stay on tic0
              if gun_pinj = '1' and guni_en = '1' and buclk_bun_data(15) = '0'  then  -- 1 pipeline for gun#0
                tac                <= tic1;
              end if;
            when tic1 =>                  -- necessary to phase pas and bun_addr after gun_pinj
              tac                  <= tic2;
              pas8                 <= pas8 + 1;
            when tic2 =>                  
              pas8                 <= pas8 + 1;
              tac                  <= tic21;
              buclk_bun_addr       <= buclk_bun_addr + 1; -- only now otherwise bad gun due to pipe
            when tic21 =>                  
              pas8                 <= pas8 + 1;
              tac                  <= tic3;
            when tic3 =>                  -- main cycle
              if unsigned(buclk_bun_data(8 downto 3)) = unsigned(pas8) then -- only once in cycle
                gun_mb             <= '1';
                if buclk_bun_data(15) = '0' then
                  xbunch           <= xbunch + 1;         -- one more bunch...
                  tac              <= tic4;
                  pas8             <= (others => '0');
                else
                  tac              <= tic6;
                  pas8             <= (others => '0');
                end if;
              else
                pas8               <= pas8 + 1;
              end if;
            when tic4 =>                  -- blist ram pipe
              gun_mb               <= '0';
              pas8                 <= pas8 + 1;
              tac                  <= tic41;
            when tic41 =>
              if xbunch < MAXMB then           -- overall limit multi-bunch
                pas8               <= pas8 + 1;
                tac                <= tic5;
                buclk_bun_addr     <= buclk_bun_addr + 1;
              else
                tac                <= tic6; -- exception
                pas8               <= (others => '0');
              end if;
            when tic5 =>                  -- blist ram pipe
              pas8                 <= pas8 + 1;
              tac                  <= tic3;
            when tic6 =>                  -- must wait before tic0 and bun_addr reset
              gun_mb               <= '0';
              tac                  <= tic7;
            when tic7 =>                  -- must wait before tic0 and bun_addr reset
              tac                  <= tic8;
            when tic8 =>                  -- must wait before tic0 and bun_addr reset
              -- define blist_start_mb when autoblist=1, must wait for end of multi bunch cycle
              if autoblist = '1' then
                if done_ext = '1' and inj_run = '1' then  -- monostable and : normal end of main sequence
                  if (buclk_bun_addr < blist_stop) and (buclk_bun_addr > blist_start) then
                    blist_start_mb     <= buclk_bun_addr + 1; -- increment bunch list RAM address to next multibunch list
                  else
                    blist_start_mb     <= blist_start;  -- loop on bunch list
                  end if;
                  tac                  <= tic0;
                end if;
              else
                tac                    <= tic0; -- ends with end of mb
              end if;
            when others =>
              tac                  <= tic0;
              gun_mb               <= '0';
              buclk_bun_addr       <= blist_start_mb;
              pas8                 <= (others => '0');
          end case;
        elsif mb_en = '0' and rotinj_en = '0' then -- single
          tac                      <= tic0;
          trot                     <= tic0;
          gun_mb                   <= '0';
          pas8                     <= (others => '0');
          -- auto blist management - single bunch
          if autoblist = '1' then
            if done_ext = '1' and inj_run = '1' then  -- normal end of main sequence
              if buclk_bun_addr < blist_stop then
                buclk_bun_addr     <= buclk_bun_addr + 1; -- increment bunch list RAM address to get next bunch to inject
              else
                buclk_bun_addr     <= blist_start;  -- loop on bunch list
              end if;
            end if;
          -- manual blist management - single bunch
          else
            if blistrst_1rf = '1' then -- secured outside inj_run, reset when return to single bunch
             buclk_bun_addr       <= blist_start;
            elsif blistinc_1rf = '1' then -- secured outside inj_run
              if buclk_bun_addr < blist_stop then
                buclk_bun_addr     <= buclk_bun_addr + 1; -- increment bunch list RAM address to get next bunch to inject
              else
                buclk_bun_addr     <= blist_start;  -- loop on bunch list
              end if;
            end if;
          end if;
          -- end of auto blist management - single bunch
          -- now rotinj
        elsif rotinj_en = '1' then    --  rotinj
          tac                  <= tic0; -- security
          -- handshake with moulinette process
          if next_roti = '0' and next_roti_ack = '1' then
            next_roti_ack      <= '0';
          end if;
          -- main rotinj
          case trot is
            when tic0 =>
              -- set rotinj running  parameters
              roti_pas8                <= "1010";
              -- manual blist management when not auto (debug)
              if blist_add_autorotinj = '0' then
                if blistrst_1rf = '1' then -- secured outside inj_run, reset when return to single bunch
                buclk_bun_addr         <= blist_start;
                elsif blistinc_1rf = '1' then -- secured outside inj_run
                  if buclk_bun_addr < blist_stop then
                    buclk_bun_addr     <= buclk_bun_addr + 1; -- increment bunch list RAM address to get next bunch to inject
                  else
                    buclk_bun_addr     <= blist_start;  -- loop on bunch list
                  end if;
                end if;
              end if;
              -- end blist management
              gun_mb               <= '0';
              rot_seq              <= "01"; -- counts additional pulses
              if gun_pinj = '1' and guni_en = '1' then  -- same pipeline for gun#0
                trot               <= tic1;
              end if;
            when tic1 =>                  -- necessary to phase pas and bun_addr after gun_pinj
              trot                 <= tic2;
              pas8                 <= pas8 + 1;
            when tic2 =>                  
              pas8                 <= pas8 + 1;
              trot                 <= tic21;
            when tic21 =>                  
              pas8                 <= pas8 + 1;
              trot                 <= tic3;
            when tic3 =>                  -- main cycle
              if pas8(3 downto 0) = roti_pas8 then -- fixed spacing
                gun_mb             <= '1';
                if rot_seq < 3 then
                  rot_seq          <= rot_seq + 1;         -- one more bunch...
                  trot             <= tic4;
                  pas8             <= (others => '0');
                else
                  trot             <= tic6; -- end of moulinette
                  pas8             <= (others => '0');
                end if;
              else
                pas8               <= pas8 + 1;
              end if;
            when tic4 =>                  -- same pipe as blist ram
              gun_mb               <= '0';
              pas8                 <= pas8 + 1;
              trot                 <= tic41;
            when tic41 =>
              pas8                 <= pas8 + 1;
              trot                 <= tic5;
            when tic5 =>                  -- same pipe as blist ram
              pas8                 <= pas8 + 1;
              trot                 <= tic3;
            when tic6 =>                  -- must wait before tic0 and bun_addr reset
              gun_mb               <= '0';
              trot                 <= tic7;
            when tic7 =>                  -- must wait before tic0 and bun_addr reset
              trot                 <= tic8;
            when tic8 =>                  -- must wait before tic0 and bun_addr reset
              -- autorotinj managed in moulinette
              if next_roti = '1' and blist_add_autorotinj = '1' then
                next_roti_ack        <= '1';
                if buclk_bun_addr < blist_stop then
                  buclk_bun_addr     <= buclk_bun_addr + 1; -- increment bunch list RAM address to get next bunch to inject
                else
                  buclk_bun_addr     <= blist_start;  -- loop on bunch list
                end if;
              end if;
              trot                 <= tic0; -- same as end of mb
           when others =>
              trot                 <= tic0;
              gun_mb               <= '0';
              pas8                 <= (others => '0');
          end case;
        end if;
      end if;
		end if;
  end process;

-- WR CLOCK DOMAIN
-- link survey during bunch clock
-- clearing wr_link_lost requires toggling regs_out.ctrl_rfen_o
-- **************************************************************************
  process(clk_sys_i)       -- WR CLOCK DOMAIN
  begin
    if clk_sys_i'event and clk_sys_i = '1' then
      if rst_n_i = '0' or regs_out.ctrl_rfen_o = '0' then
        wr_link          <= '0';
        wr_link_lost     <= '0';
	    else
        if link_ok_i = '1' and wr_link_lost = '0' then
          wr_link        <= '1';
        else
					if wr_link = '1' and link_ok_i = '0' then
						wr_link_lost   <= '1';
          	wr_link        <= '0';
					end if;
        end if;
	 	  end if;
		end if;
  end process;

-- RF/8 CLOCK DOMAIN
--         start counters
--         continuous PPS sampling after start
-- **************************************************************************
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' then
        cnt_spl                <= (others => '0');
        cnt8_ppsa              <= (others => '0');
        cnt_bu44_ppsa          <= (others => '0');
        cnt_bu44_ppsb          <= (others => '0');
        freq_bu44              <= (others => '0');
        pps_sampled            <= '0';
        pps_rise               <= '0';
        master_start_mem       <= '0';
        master_start_pps       <= '0';
        slave_start_pps        <= '0';
        rf_pps_rdy_a           <= '0';
        rf_pps_rdy_b           <= '0';
        rf_pps_rdy_bcc         <= '0';
	    else
        pps_a                  <= pps_i;
        pps_b                  <= pps_a;
        pps_rise_a             <= pps_rise;
        pps_rise_b             <= pps_rise_a;
        pps_rise_c             <= pps_rise_b;
-- master start subprocess
        if master_start_i = '1' and master_start_mem = '0' then
          master_start_mem     <= '1';                 -- record monostable input
        elsif pps_rise_c = '1' then              -- after master_start_pps monostable
          master_start_mem     <= '0';                 -- ... until pps
        end if;
        if master_start_mem = '1' and pps_a = '1' and pps_b = '0'  then
          master_start_pps     <= '1';                 -- pps resynced monostable
        else
          master_start_pps     <= '0';                 -- ... to reset counters, blist sequencer
        end if;
-- slave start subprocess
        if slave_bit_i = '1' and slave_start = '1' and pps_a = '1' and pps_b = '0' then
          slave_start_pps      <= '1';
        else
          slave_start_pps      <= '0';
        end if;
-- pps_rdy handshake logic to WR clock space
        if pps_rise = '1' and pps_sampled = '0' then           -- rising PPS
          pps_sampled          <= '1';
        end if;
        if pps_sampled = '1' and cnt_spl < 5 then  -- must last quite long for clock domain transfer
          cnt_spl              <= cnt_spl + 1;
        elsif pps_sampled = '1' then
          pps_sampled          <= '0';
          cnt_spl              <= (others => '0');
        end if;
        rf_pps_rdy_a           <= pps_sampled;
        rf_pps_rdy_b           <= rf_pps_rdy_a;
        if rf_pps_rdy_a = '1' and rf_pps_rdy_b = '0' then
          rf_pps_rdy_bcc       <= '1';
        else
          rf_pps_rdy_bcc       <= '0';
        end if;
-- hot plug and slave autolock pps raw sampling
-- frequency meter, calculate after stamp on rise_c, does not work with phase safe sampling
        if pps_a = '1' and pps_b = '0' then           -- rising PPS
          pps_rise             <= '1';
        else
          pps_rise             <= '0';
        end if;
        if pps_rise_b = '1' then  -- pipe 2 to keep stamp in phase with cnt_bu44 init
	        cnt_bu44_ppsa            <= cnt_bu44;
	        cnt_bu44_ppsb            <= cnt_bu44_ppsa;
          cnt8_ppsa(6 downto 0)    <= cnt8_sr;
          cnt8_ppsa(21 downto 16)  <= cnt8_boo;
        end if;
        if pps_rise_c = '1' then           -- a-b, a larger b
          freq_bu44            <= std_logic_vector(unsigned(cnt_bu44_ppsa) - unsigned(cnt_bu44_ppsb));
        end if;
	 	  end if;
		end if;
  end process;

-- further PPS sampling for debug, a&b
-- **************************************************************************
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' then
        cnt_bu44_papsa         <= (others => '0');
        paps_rise              <= '0';
        paps_rise_a            <= '0';
        paps_rise_b            <= '0';
        paps_a                 <= '0';
        paps_b                 <= '0';
	    else
        paps_a                 <= paps_i;
        paps_b                 <= paps_a;
        if paps_a = '1' and paps_b = '0' then           -- rising PPS
          paps_rise            <= '1';
        else
          paps_rise            <= '0';
        end if;
        paps_rise_a            <= paps_rise;
        paps_rise_b            <= paps_rise_a;
        if paps_rise_b = '1' and rf_lost = '0' then  -- pipe 2 to keep stamp in phase with cnt_bu44 init
	        cnt_bu44_papsa       <= cnt_bu44;
        end if;  
	 	  end if;
		end if;
  end process;
-- **************************************************************************
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' then
        cnt_bu44_pbpsa         <= (others => '0');
        pbps_rise              <= '0';
        pbps_rise_a            <= '0';
        pbps_rise_b            <= '0';
        pbps_a                 <= '0';
        pbps_b                 <= '0';
	    else
        pbps_a                 <= pbps_i;
        pbps_b                 <= pbps_a;
        if pbps_a = '1' and pbps_b = '0' then           -- rising PPS
          pbps_rise            <= '1';
        else
          pbps_rise            <= '0';
        end if;
        pbps_rise_a            <= pbps_rise;
        pbps_rise_b            <= pbps_rise_a;
        if pbps_rise_b = '1' and rf_lost = '0' then  -- pipe 2 to keep stamp in phase with cnt_bu44 init
	        cnt_bu44_pbpsa       <= cnt_bu44;
        end if;  
	 	  end if;
		end if;
  end process;

-- WR CLOCK DOMAIN
-- PPs sampling acknowledge in clk_sys clock space
-- **************************************************************************
  process(clk_sys_i)          -- WR CLOCK DOMAIN
  begin
    if clk_sys_i'event and clk_sys_i = '1' then
      if rst_n_i = '0' then
        rf_pps_rdy          <= '0';
	    else
        if pps_sampled = '1' then      -- long enough to be clocked, short enough before clr
          rf_pps_rdy        <= '1';
        elsif regs_out.csr0_ppsack_o = '1' then  -- 1 clk monostable synchronous
          rf_pps_rdy        <= '0';
        end if;
	 	  end if;
		end if;
  end process;

-- RF/8 CLOCK DOMAIN
-- add master RF T0 stamp to programmed inj delay
-- **************************************************************************
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' or master_start_mem = '1' then
        new_t0_stamp(RFTIMEWID-1 downto 0)    <= (others => '0');
        rf_inj_delay                          <= (others => '0');
        t0_cnt                                <= (others => '0');
        t0_received                           <= '0';
        tt0_received                          <= '0';
        tt0_new_stamp(RFTIMEWID-1 downto 0)   <= (others => '0');
      else
        rf_inj_delay                          <= rego_t0_del(29 downto 0);   -- clock domain transfer
        t0_received                           <= master_t0_seq_i;                -- small pipe
        tt0_received                          <= master_t0_i;                -- small pipe
        if master_t0_seq_i = '1' and t0_lost = '0' then
          new_t0_stamp(RFTIMEWID-1 downto 0)  <= std_logic_vector(unsigned(rf_mast0_stamp_i(RFTIMEWID-1 downto 0)) + unsigned(TT0DELAY) + unsigned(rf_inj_delay));
          t0_cnt                              <= t0_cnt + 1;
        end if;
        if master_t0_i = '1' then                          -- TT0 specific always done
          tt0_new_stamp(RFTIMEWID-1 downto 0) <= std_logic_vector(unsigned(rf_mast0_stamp_i(RFTIMEWID-1 downto 0)) + unsigned(TT0DELAY));
        end if;
      end if;
    end if;
  end process;

-- RF/8 CLOCK DOMAIN
-- T0 delay hit
-- **************************************************************************
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' or master_start_mem = '1' or t0_rst = '1' or rst_injerr_1rf = '1' then
        t0_sync           <= '0';
        rf_t0             <= '0';
        t0_lost           <= '0';
        t0_lost_cnt       <= (others => '0');
        tt0               <= '0';
        tt0_wait          <= '0';
      else
        if rf_t0 = '0' and t0_received = '1' and t0_lost = '0' then
          rf_t0           <= '1';                        -- log T0 received
        elsif rf_t0 = '1' and t0_sync = '1' then
          rf_t0           <= '0';                        -- normal end
        elsif rf_t0 = '1' and master_t0_seq_i = '1' then
          rf_t0           <= '0';                        -- did the job, if not reset buclk_run by rf_en...
          t0_lost         <= '1';                        -- to be reset by buclk_run
          t0_lost_cnt     <= t0_lost_cnt + 1;            -- to be reset with bit
        end if;                      
        if t0_sync = '0' and rf_t0 = '1' and (new_t0_stamp(RFTIMEWID-1 downto 0) = cnt_bu44) then   -- wait for hit
          t0_sync         <= '1';
        elsif t0_sync = '1' then
          t0_sync         <= '0';                        -- monostable
        end if;
        if tt0_wait = '0' and tt0_received = '1' then     -- tt0 process
          tt0_wait        <= '1';                        -- log T0 received always
        elsif tt0_wait = '1' and tt0 = '1' then
          tt0_wait        <= '0';                        -- normal end
        end if;
        if tt0 = '0' and tt0_wait = '1' and (tt0_new_stamp(RFTIMEWID-1 downto 0) = cnt_bu44) then  -- hit
          tt0             <= '1';
        else
          tt0             <= '0';  -- TT0 monostable
        end if;
      end if;
    end if;
  end process;

-- WR CLOCK DOMAIN toggles extension to RF/8
-- 352 PLL reset logic, soft T0, bunch list increment and reset
-- **************************************************************************
  process(clk_sys_i)          -- WR CLOCK DOMAIN
  begin
    if clk_sys_i'event and clk_sys_i = '1' then
      if rst_n_i = '0' then
        rst_352_long          <= '0';
        rst_352_end           <= '0';
        rst352_cnt            <= (others => '0');
        rst_injerr_long       <= '0';
        rst_injerr_end        <= '0';
        skip44_long           <= '0';
        skip44_end            <= '0';
        jump44_long           <= '0';
        jump44_end            <= '0';
        rflraz_long           <= '0';
        rflraz_end            <= '0';
        blistinc_long         <= '0';
        blistinc_end          <= '0';
        blistrst_long         <= '0';
        blistrst_end          <= '0';
        mblprst_long          <= '0';
        mblprst_end           <= '0';
        syncrf_long           <= '0';
        syncrf_end            <= '0';
        slok_long             <= '0';
        slok_end              <= '0';
	    else
        rst_injerr_end        <= rst_injerr_1rf;
        skip44_end            <= skip44_1rf;
        jump44_end            <= jump44_1rf;
        rflraz_end            <= rflraz_1rf;
        blistinc_end          <= blistinc_1rf;
        blistrst_end          <= blistrst_1rf;
        mblprst_end           <= mblprst_1rf;
        syncrf_end            <= syncrf_a;
        slok_end              <= slok_a;
        if regs_out.csr0_mrst352_o = '1' then
          rst_352_long        <= '1';
        elsif rst_352_end = '1' then
          rst_352_long        <= '0';
        end if;
        if rst_352_long = '1' then  -- make it last 16 WR clocks > 4xRF/8
          rst352_cnt          <= rst352_cnt + 1;
          if rst352_cnt = "1111" then
            rst_352_end       <= '1';
          end if;
        else
          rst352_cnt          <= (others => '0');
          rst_352_end         <= '0';
        end if;
        if regs_out.csr0_skip44_o = '1' then
          skip44_long         <= '1';
        elsif skip44_end = '1' then
          skip44_long         <= '0';
        end if;
        if regs_out.csr0_jump44_o = '1' then
          jump44_long         <= '1';
        elsif jump44_end = '1' then
          jump44_long         <= '0';
        end if;
        if regs_out.csr0_injerr_rst_o = '1' then
          rst_injerr_long        <= '1';
        elsif rst_injerr_end = '1' then
          rst_injerr_long        <= '0';
        end if;
        if regs_out.csr0_rflraz_o = '1' then
          rflraz_long         <= '1';
        elsif rflraz_end = '1' then
          rflraz_long         <= '0';
        end if;
        if regs_out.csr0_blistinc_o = '1' then
          blistinc_long       <= '1';
        elsif blistinc_end = '1' then
          blistinc_long       <= '0';
        end if;
        if regs_out.csr0_blistrst_o = '1' then
          blistrst_long       <= '1';
        elsif blistrst_end = '1' then
          blistrst_long       <= '0';
        end if;
        if regs_out.csr0_mblprst_o = '1' then
          mblprst_long        <= '1';
        elsif mblprst_end = '1' then
          mblprst_long        <= '0';
        end if;
        if regs_out.csr0_syncrf_o = '1' then
          syncrf_long         <= '1';
        elsif syncrf_end = '1' then
          syncrf_long         <= '0';
        end if;
        if slok_trig_i = '1' then
          slok_long           <= '1';
        elsif slok_end = '1' then
          slok_long           <= '0';
        end if;
      end if;
    end if;
  end process;
          
-- toggles extension to RF/8 acknowledge in RF/8 clock domain
-- one RF/8 for soft T0 and bunch list inc. and reset
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' then
        rst_injerr_rf         <= '0';
        rst_injerr_1rf        <= '0';
        skip44_rf             <= '0';
        skip44_1rf            <= '0';
        jump44_rf             <= '0';
        jump44_1rf            <= '0';
        rflraz_rf             <= '0';
        rflraz_1rf            <= '0';
        blistinc_rf           <= '0';
        blistinc_1rf          <= '0';
        blistrst_rf           <= '0';
        blistrst_1rf          <= '0';
        mblprst_rf            <= '0';
        mblprst_1rf           <= '0';
        syncrf_rf             <= '0';
        syncrf_a              <= '0';
        syncrf_b              <= '0';
        slok_rf               <= '0';
        slok_a                <= '0';
        slok_b                <= '0';
      else
        rst_injerr_rf         <= rst_injerr_long and not(rst_injerr_1rf);
        skip44_rf             <= skip44_long and not(skip44_1rf);
        jump44_rf             <= jump44_long and not(jump44_1rf);
        rflraz_rf             <= rflraz_long and not(rflraz_1rf);
        blistinc_rf           <= blistinc_long and not(blistinc_1rf) and not(inj_run); -- better move blist outside inj_run
        blistrst_rf           <= blistrst_long and not(blistrst_1rf) and not(inj_run);
        mblprst_rf            <= mblprst_long and not(mblprst_1rf) and not(inj_run);
        if rst_injerr_rf = '1' and rst_injerr_1rf = '0' then
          rst_injerr_1rf      <= '1';
        else
          rst_injerr_1rf      <= '0';   -- 1 shot
        end if;
        if skip44_rf = '1' and skip44_1rf = '0' then
          skip44_1rf          <= '1';
        else
          skip44_1rf          <= '0';   -- 1 shot
        end if;
        if jump44_rf = '1' and jump44_1rf = '0' then
          jump44_1rf          <= '1';
        else
          jump44_1rf          <= '0';   -- 1 shot
        end if;
        if rflraz_rf = '1' and rflraz_1rf = '0' then
          rflraz_1rf          <= '1';
        else
          rflraz_1rf          <= '0';   -- 1 shot
        end if;
        if blistinc_rf = '1' and blistinc_1rf = '0' then
          blistinc_1rf        <= '1';
        else
          blistinc_1rf        <= '0';   -- 1 shot
        end if;
        if blistrst_rf = '1' and blistrst_1rf = '0' then
          blistrst_1rf        <= '1';
        else
          blistrst_1rf        <= '0';   -- 1 shot
        end if;
        if mblprst_rf = '1' and mblprst_1rf = '0' then
          mblprst_1rf         <= '1';
        else
          mblprst_1rf         <= '0';   -- 1 shot
        end if;
-- modified syncrf to RF/8 monostable for RF/8 lock logic
        syncrf_a              <= syncrf_long;
        syncrf_b              <= syncrf_a;
        if syncrf_a = '1' and syncrf_b = '0' then
          syncrf_rf           <= '1';
        else
          syncrf_rf           <= '0';
        end if;
-- slok clk_sys monostable toto RF/8 monostable
        slok_a                <= slok_long;
        slok_b                <= slok_a;
        if slok_a = '1' and slok_b = '0' then
          slok_rf             <= '1';
        else
          slok_rf             <= '0';
        end if;
      end if;
    end if;
  end process;

-- Delta_t test on input#0 only
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or  regs_out.stamp_csr_ddt_o = '0'  or slave_bit_i = '0' then
        gub                <= tic0;
        metas_0            <= (others => '0');
        metas_1            <= (others => '0');
        metas_2            <= (others => '0');
        metas_3            <= (others => '0');
      else
        case gub is
          when tic0 =>
            if regs_out.stamp_csr_ddt_gate_o = '1' and tinrise_i(0) = '1' then -- 1st rising edge, resyncro with gate re-activated
              gub          <= tic1;
              metas_0      <= (others => '0'); -- only now to allow read
              metas_1      <= (others => '0');
              metas_2      <= (others => '0');
              metas_3      <= (others => '0');
            end if;
          when tic1 =>
            if regs_out.stamp_csr_ddt_gate_o = '1' then    -- check gate active
              if tinrise_i(0) = '1' then  -- start looping
                gub            <= tic2;
              end if;
            else                         -- gate end
              gub              <= tic0;
            end if;
          when tic2 =>
            if regs_out.stamp_csr_ddt_gate_o = '1' then    -- check gate active
            -- in loop : no condition on tinrise monostable
              gub              <= tic3;
            else                         -- gate end
              gub              <= tic0;
            end if;
          when tic3 =>
            if tinrise_i(0) = '0' then   -- in loop
              gub              <= tic4;
            else
              gub              <= tic1; -- short loop
              if metas_0 = X"FF" then
                gub            <= tic6;
              else
                metas_0        <= metas_0 + 1;
              end if;
            end if;
          when tic4 =>
            if tinrise_i(0) = '0' then   -- in loop
              gub              <= tic5;
            else
              gub              <= tic1; -- short loop
              if metas_1 = X"FF" then
                gub            <= tic6;
              else
                metas_1        <= metas_1 + 1;
              end if;
            end if;
          when tic5 =>
            if tinrise_i(0) = '1' then   -- loop OK
              gub              <= tic2;
              if metas_2 < X"FF" then
                metas_2        <= metas_2 + 1;
              end if;
            else
              gub              <= tic1; -- force loop
              if metas_3 = X"FF" then
                gub            <= tic6;
              else
                metas_3        <= metas_3 + 1;
              end if;
            end if;
          when tic6 =>
            if regs_out.stamp_csr_ddt_gate_o = '1' then   -- gate active stats full
              gub          <= tic6; -- stay here untill be reset by stamdge
            else
              gub          <= tic0;
            end if;
          when others =>
            gub            <= tic0;
            metas_0        <= (others => '0');
            metas_1        <= (others => '0');
            metas_2        <= (others => '0');
            metas_3        <= (others => '0');
        end case;   
      end if;
    end if;
  end process;
           
-- rf counter lock to master
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' or slave_bit_i = '0' then
        hot                      <= tic0;
        slok_adj_en              <= '0';
        slave_start              <= '0';
        slok_aok                 <= '0';
        slok_nok                 <= '0';
        slok_run                 <= '0';
        t0_rst                   <= '0';
      else
        case hot is
          when tic0 =>
            if slok_ack_i = '1' then -- issued by bcc-core after tic3 & rt-bcc adjust
              slok_aok           <= '1';
              slok_run           <= '0';
              slok_adj_en        <= '0'; -- must be reset on slave and master start
            end if;
            if slok_nok_i = '1' then -- issued by bcc-core after tic3 & rt-bcc adjust
              slok_nok           <= '1';
              slok_run           <= '0';
              slok_adj_en        <= '0'; -- must be reset on slave and master start
            end if;
            if  master_start_i = '1' and pps_sampled = '0' then  -- 
              cnt_bu44_ini       <= (others => '0'); -- true master start
              slok_adj_en        <= '1';  -- prepare adjust enable for next cnt_bu44 re-init on master start
              slave_start        <= '0';
              slok_run           <= '0';
              slok_aok           <= '0';  -- master start overrides slok
              slok_nok           <= '0';  -- master start overrides slok
            elsif slok_rf = '1' then   -- slave autolock process start from bcc-core
              slok_aok           <= '0';
              slok_nok           <= '0';
              hot                <= tic1;
              t0_rst             <= '1'; -- reset T0 logic
              slave_start        <= '0';
            end if;
          when tic1 =>
            if pps_rise_c = '1' then  -- to avoid overlap with possible synrf during pps
              hot                <= tic2;
            end if;
          when tic2 =>
            if rf_hot_ok_i = '1' then
              cnt_bu44_ini       <= std_logic_vector(unsigned(rf_hot_pps_i) + unsigned(freq_bu44)); -- freq_bu44 assumed to be reliable
              slave_start        <= '1';
              hot                <= tic3;
            end if;
          when tic3 =>
            if slave_start_pps = '1' and slave_start = '1' then -- cnt_bu44_ini -> cnt_bu44 done
              slave_start        <= '0';
              t0_rst             <= '0'; -- ready for T0 logic again
              slok_run           <= '1'; -- -> to bcc-core start adjust proc.
              slok_adj_en        <= '1'; -- prepare adjust enable for next cnt_bu44 re-init on slave start
              hot                <= tic0;
            end if;
          when others =>
            hot                  <= tic0;
            slok_adj_en          <= '0';
            slave_start          <= '0';
            slok_run             <= '0';
        end case;
      end if;
    end if;
  end process;

---------------------------------------------------------------------------------
-- rf lost : PPS stamps discrepancy between master braodcast & local slave stamps
---------------------------------------------------------------------------------
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' or slave_bit_i = '0'
                       or slok_aok = '0' or regs_out.csr0_rfdbgen_o = '0' or rflraz_1rf = '1' then
        delta_pps               <= (others => '0');
        regs_in.csr2_rf_delta_i <= (others => '0');
        cnt_hist0               <= (others => '0');
        cnt_hist1               <= (others => '0');
        cnt_hist2               <= (others => '0');
        sama                    <= '0';
        samb                    <= '0';
        samc                    <= '0';
        supa                    <= '0';
        supb                    <= '0';
        supc                    <= '0';
        mala                    <= '0';
        malb                    <= '0';
        malc                    <= '0';
        dbg_adj_0               <= '0';
        dbg_adj_1               <= '0';
        dbg_adj_en              <= '0';
        rf_dbg_adj              <= '0';
        rf_dbg_run              <= '0';
        rf_lost                 <= '0';
        gab                     <= tic0;
      else
        case gab is
          when tic0 =>            
            if rf_hot_ok_i = '1' then
              gab              <= tic1;
            end if;
          when tic1 =>
            if rf_hot_ok_i = '1' then
              gab              <= tic2; -- 2 seconds to start OK
            end if;
            sama               <= '0';
            samb               <= '0';
            samc               <= '0';
            supa               <= '0';
            supb               <= '0';
            supc               <= '0';
            mala               <= '0';
            malb               <= '0';
            malc               <= '0';
            dbg_adj_0          <= '0';
            dbg_adj_1          <= '0';
            dbg_adj_en         <= '0';
          when tic2 =>
            gab                <= tic3;
            if rf_hot_pps_i = cnt_bu44_ppsa then
              sama             <= '1';
            elsif rf_hot_pps_i > cnt_bu44_ppsa then
              supa             <= '1';
            else
              mala             <= '1';
            end if;
            if rf_hot_paps_i = cnt_bu44_papsa then
              samb             <= '1';
            elsif rf_hot_paps_i > cnt_bu44_papsa then
              supb             <= '1';
            else
              malb             <= '1';
            end if;
            if rf_hot_pbps_i = cnt_bu44_pbpsa then
              samc             <= '1';
            elsif rf_hot_pbps_i > cnt_bu44_pbpsa then
              supc             <= '1';
            else
              malc             <= '1';
            end if;
          when tic3 =>
            if (sama = '1' and samb = '1') or
              (sama = '1' and samc = '1') or
              (samc = '1' and samb = '1') then
              cnt_hist0        <= cnt_hist0 + 1;
              rf_dbg_run       <= '0'; -- rf_counter assumed recovered
              gab              <= tic1;
            elsif (supa = '1' and supb = '1') or
              (supa = '1' and supc = '1') or
              (supc = '1' and supb = '1') then
              gab              <= tic4;
            elsif (mala = '1' and malb = '1') or
              (mala = '1' and malc = '1') or
              (malc = '1' and malb = '1') then
              gab              <= tic6;
            else
              gab              <= tic61;
            end if;
          when tic4 => -- one step behind
            cnt_hist1          <= cnt_hist1 + 1;
            if rf_adj_en = '1' then
              dbg_adj_0        <= '0';
              dbg_adj_1        <= '1'; -- adjust plus 1
              dbg_adj_en       <= '1';
              rf_dbg_run       <= '1'; -- adjust running
            end if;
            rf_dbg_adj         <= '1'; -- adjust required at least once
            if supa = '1' then
              delta_pps    <= std_logic_vector(unsigned(rf_hot_pps_i) - unsigned(cnt_bu44_ppsa));
            elsif supb = '1' then  -- at least supa or supb
              delta_pps    <= std_logic_vector(unsigned(rf_hot_paps_i) - unsigned(cnt_bu44_papsa));
            end if;
            gab                <= tic7;
          when tic6 => -- one step ahead
            cnt_hist2          <= cnt_hist2 + 1;
            if rf_adj_en = '1' then
              dbg_adj_0        <= '1'; -- adjust minus 1
              dbg_adj_1        <= '0';
              dbg_adj_en       <= '1';
              rf_dbg_run       <= '1'; -- adjust running
            end if;
            rf_dbg_adj         <= '1'; -- adjust required at least once
            if mala = '1' then
              delta_pps    <= std_logic_vector(unsigned(cnt_bu44_ppsa) - unsigned(rf_hot_pps_i));
            elsif malb = '1' then  -- at least mala or malb
              delta_pps    <= std_logic_vector(unsigned(cnt_bu44_papsa) - unsigned(rf_hot_paps_i));
            end if;
            gab                <= tic7;
          when tic7 => -- record and loop if not stop on 1st adjust
            dbg_adj_0               <= '0';
            dbg_adj_1               <= '0';
            dbg_adj_en              <= '0';
            regs_in.csr2_rf_delta_i    <= delta_pps(15 downto 0);
            if rf_adj_stop = '0' then
              gab              <= tic1;
            end if;
          when tic61 => -- junk : record then stop
            gab                <= tic9;
          when tic9 => -- junk : stop here & wait for reset
            rf_lost            <= '1';
          when others =>
            gab              <= tic0;
            rf_lost          <= '0';
            rf_dbg_adj       <= '0';
            rf_dbg_run       <= '0';
            delta_pps        <= (others => '0');
            cnt_hist0        <= (others => '0');
            cnt_hist1        <= (others => '0');
            cnt_hist2        <= (others => '0');              
        end case;
      end if;
    end if;
  end process;

--------------------------------------------------------------------------------
  -- sr counter lock to master
--------------------------------------------------------------------------------
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' or slave_bit_i = '0' then
        sric                     <= tic0;
        sr8_moins_un             <= '0';
        sr_aok                   <= '0';
        sr_nok                   <= '0';
        sr_lok_pass              <= (others => '0');
        sr_adj_pass              <= (others => '0');
      else
        if master_start_pps = '1' then
          sr_aok                 <= '1'; -- force
          sr_nok                 <= '0';
          sric                   <= tic0;
          sr_lok_pass            <= (others => '0');
          sr_adj_pass            <= (others => '0');
        elsif slave_start_pps = '1' then -- ready for slok
          sr_aok                 <= '0';
          sr_nok                 <= '0';
          sric                   <= tic0;
          sr_lok_pass            <= (others => '0');
          sr_adj_pass            <= (others => '0');
        else
          case sric is
            when tic0 =>
              sr_lok_pass          <= (others => '0');
              sr_adj_pass          <= (others => '0');
              if slok_aok = '1' then
                if sr_aok = '0' and sr_nok = '0' then
                  if rf_hot_ok_i = '1' then
                    sric           <= tic1;
                  end if;
                end if;
              else
                sr_aok             <= '0';
                sr_nok             <= '0';
              end if;
            when tic1 =>
              if rf_hot_ok_i = '1' then
                sric               <= tic2;
              end if;
            when tic2 =>
              if cnt8_ppsa(6 downto 0) = master_srboo_pps_i(6 downto 0) then --compare both pps
                sric               <= tic4;
                sr_lok_pass        <= sr_lok_pass + 1; -- start lok process
              else
                sr8_moins_un       <= '1';
                sric               <= tic3;
              end if;
            when tic3 =>
              sr8_moins_un         <= '0'; -- one shot
              if sr_adj_pass < MAXSRBADJ then
                sr_adj_pass        <= sr_adj_pass + 1;
                sric               <= tic1; -- loop new adj
              else
                sric               <= tic7;
              end if;
            when tic4 =>   -- confirm lok
              if rf_hot_ok_i = '1' then
                sric               <= tic5;
              end if;
            when tic5 =>
              sr_lok_pass          <= sr_lok_pass + 1;
              sric                 <= tic6;
              if cnt8_ppsa(6 downto 0) = master_srboo_pps_i(6 downto 0) then --compare both pps
                sr_lok             <= sr_lok + 1;
              end if;
            when tic6 =>
              if sr_lok_pass < 7 then
                if sr_lok < 3 then
                  sric             <= tic4; -- loop new lok pass
                else
                  sric             <= tic0; -- success
                  sr_aok           <= '1';
                end if;
              else                 -- new adj pass
                sric               <= tic1;
                sr_lok_pass        <= (others => '0'); -- reset for a new lok pass
              end if;
            when tic7 =>   -- fail
              sric                 <= tic0;
              sr_nok               <= '1'; -- requires M or S start to reset
            when others =>
              sric                 <= tic0;
              sr_nok               <= '0';
              sr_aok               <= '0';
          end case;
        end if;
      end if;
    end if;
  end process;

--------------------------------------------------------------------------------
  -- booster counter lock to master
--------------------------------------------------------------------------------
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' or buclk_run = '0' or slave_bit_i = '0' then
        boc                      <= tic0;
        bo8_moins_un             <= '0';
        bo_aok                   <= '0';
        bo_nok                   <= '0';
        bo_lok_pass              <= (others => '0');
        bo_adj_pass              <= (others => '0');
      else
        if master_start_pps = '1' then
          bo_aok                 <= '1'; -- force
          bo_nok                 <= '0';
          boc                   <= tic0;
          bo_lok_pass            <= (others => '0');
          bo_adj_pass            <= (others => '0');
        elsif slave_start_pps = '1' then -- ready for slok
          bo_aok                 <= '0';
          bo_nok                 <= '0';
          boc                   <= tic0;
          bo_lok_pass            <= (others => '0');
          bo_adj_pass            <= (others => '0');
        else
          case boc is
            when tic0 =>
              bo_lok_pass          <= (others => '0');
              bo_adj_pass          <= (others => '0');
              if slok_aok = '1' then
                if bo_aok = '0' and bo_nok = '0' then
                  if rf_hot_ok_i = '1' then
                    boc           <= tic1;
                  end if;
                end if;
              else
                bo_aok             <= '0';
                bo_nok             <= '0';
              end if;
            when tic1 =>
              if rf_hot_ok_i = '1' then
                boc               <= tic2;
              end if;
            when tic2 =>
              if cnt8_ppsa(21 downto 16) = master_srboo_pps_i(21 downto 16) then --compare both pps
                boc               <= tic4;
                bo_lok_pass        <= bo_lok_pass + 1; -- start lok process
              else
                bo8_moins_un       <= '1';
                boc               <= tic3;
              end if;
            when tic3 =>
              bo8_moins_un         <= '0'; -- one shot
              if bo_adj_pass < MAXSRBADJ then
                bo_adj_pass        <= bo_adj_pass + 1;
                boc               <= tic1; -- loop new adj
              else
                boc               <= tic7; -- fail
              end if;
            when tic4 =>   -- confirm lok
              if rf_hot_ok_i = '1' then
                boc               <= tic5;
              end if;
            when tic5 =>
              bo_lok_pass          <= bo_lok_pass + 1;
              boc                 <= tic6;
              if cnt8_ppsa(6 downto 0) = master_srboo_pps_i(6 downto 0) then --compare both pps
                bo_lok             <= bo_lok + 1;
              end if;
            when tic6 =>
              if bo_lok_pass < 7 then
                if bo_lok < 3 then
                  boc             <= tic4; -- loop new lok pass
                else
                  boc             <= tic0; -- success
                  bo_aok           <= '1';
                end if;
              else                 -- new adj pass
                boc               <= tic1;
                bo_lok_pass        <= (others => '0'); -- reset for a new lok pass
              end if;
            when tic7 =>   -- fail
              boc                 <= tic0;
              bo_nok               <= '1'; -- requires M or S start to reset
            when others =>
              boc                 <= tic0;
              bo_nok               <= '0';
              bo_aok               <= '0';
          end case;
        end if;
      end if;
    end if;
  end process;
              
--------------------------------------------------------------------------------------------
-- synchronize sr_count clocks on next rf_hot_ok, stamp cnt_bu44_pps LSByte and acknowledge
--------------------------------------------------------------------------------------------
  process(buclk_sys_i)        -- RF/8
  begin
    if buclk_sys_i'event and buclk_sys_i = '1' then
      if rst_n_i = '0' then
        pko                     <= tic0;
      else
        case pko is
          when tic0 =>
            sync_src_pulse      <= '0';
            sync_src_ack        <= '0';
            if sync_src_en = '1' then
              pko               <= tic1;
            end if;
          when tic1 =>
            if rf_hot_ok_i = '1' then -- 1/sec ; network synchronous
              sync_src_pulse    <= '1';
              bu44_lsb          <= cnt_bu44_ppsb(7 downto 0);
              pko               <= tic2;
            end if;
          when tic2 =>
            sync_src_pulse      <= '0'; -- monostable
            sync_src_ack        <= '1';
            pko                 <= tic3;
          when tic3 =>
            if sync_src_en = '0' then
              pko               <= tic0;
            end if;
          when others =>
            pko                 <= tic0;
        end case;        
      end if;
    end if;
  end process;

--------------------------------------------------------------------------------------------
-- Inputs hardware time stamping (events managed by evt-core)
--------------------------------------------------------------------------------------------
  reset_fifo(0)   <= not(regs_out.stamp_csr_in0_fifo_rst_o) and rst_n_i; 
  reset_fifo(1)   <= not(regs_out.stamp_csr_in1_fifo_rst_o) and rst_n_i; 
  reset_fifo(2)   <= not(regs_out.stamp_csr_in2_fifo_rst_o) and rst_n_i; 
  reset_fifo(3)   <= not(regs_out.stamp_csr_in3_fifo_rst_o) and rst_n_i; 
  read_fifo(0)   <= regs_out.stamp_csr_in0_fifo_rd_o; 
  read_fifo(1)   <= regs_out.stamp_csr_in1_fifo_rd_o; 
  read_fifo(2)   <= regs_out.stamp_csr_in2_fifo_rd_o; 
  read_fifo(3)   <= regs_out.stamp_csr_in3_fifo_rd_o; 
  
  stampin_hi_buf : for K in 0 to 3 generate
  in_hi_fifos : generic_async_fifo
  generic map (
    g_data_width      => 32,
    g_size            => 256,

    -- Read-side flag selection
    g_with_rd_empty            => true,
    g_with_rd_full             => true,

    -- Write-side flag selection
    g_with_wr_empty            => true,
    g_with_wr_full             => true
    )
  port map (
    rst_n_i                    => reset_fifo(K),

    -- write port
    clk_wr_i                   => buclk_sys_i,
--    d_i                        => FILLRFTIMEWID & cnt_bu44_stampin(RFTIMEWID-1 downto 32),
    d_i                        => FILLRFTIMEWID & cnt_bu44(RFTIMEWID-1 downto 32),
    we_i                       => tinrise_i(K),
    wr_empty_o                 => open,
    wr_full_o                  => open,  -- sourced from lo

    -- read port
    clk_rd_i                   => clk_sys_i,
    q_o                        => ista_hi(K),
    rd_i                       => read_fifo(K),
    rd_empty_o                 => open,  -- sourced from lo
    rd_full_o                  => open   -- sourced from lo
    );
  end generate;

  stampin_lo_buf : for K in 0 to 3 generate
  in_lo_fifos : generic_async_fifo
  generic map (
    g_data_width      => 32,
    g_size            => 256,

    -- Read-side flag selection
    g_with_rd_empty            => true,
    g_with_rd_full             => true,

    -- Write-side flag selection
    g_with_wr_empty            => true,
    g_with_wr_full             => true
    )
  port map (
    rst_n_i                    => reset_fifo(K),

    -- write port
    clk_wr_i                   => buclk_sys_i,
--    d_i                        => cnt_bu44_stampin(31 downto 0),
    d_i                        => cnt_bu44(31 downto 0),
    we_i                       => tinrise_i(K),
    wr_empty_o                 => open,
    wr_full_o                  => regs_in.stamp_csr_w_full_i(K),

    -- read port
    clk_rd_i                   => clk_sys_i,
    q_o                        => ista_lo(K),
    rd_i                       => read_fifo(K),
    rd_empty_o                 => regs_in.stamp_csr_r_empty_i(K),
    rd_full_o                  => regs_in.stamp_csr_r_full_i(K)
    );
  end generate;

  process(clk_sys_i)        -- WR clock domain
  begin
    if clk_sys_i'event and clk_sys_i = '1' then
      if rst_n_i = '0' then
        regs_in.in0_stamp_lo_i    <= (others => '0');
        regs_in.in0_stamp_hi_i    <= (others => '0');
        regs_in.in1_stamp_lo_i    <= (others => '0');
        regs_in.in1_stamp_hi_i    <= (others => '0');
        regs_in.in2_stamp_lo_i    <= (others => '0');
        regs_in.in2_stamp_hi_i    <= (others => '0');
        regs_in.in3_stamp_lo_i    <= (others => '0');
        regs_in.in3_stamp_hi_i    <= (others => '0');
      else
        regs_in.in0_stamp_lo_i    <= ista_lo(0);
        regs_in.in0_stamp_hi_i    <= ista_hi(0);
        regs_in.in1_stamp_lo_i    <= ista_lo(1);
        regs_in.in1_stamp_hi_i    <= ista_hi(1);
        regs_in.in2_stamp_lo_i    <= ista_lo(2);
        regs_in.in2_stamp_hi_i    <= ista_hi(2);
        regs_in.in3_stamp_lo_i    <= ista_lo(3);
        regs_in.in3_stamp_hi_i    <= ista_hi(3);
      end if;
		end if;
  end process;

  regs_in.csr1_pllstatus_i      <= pll_dld_i;
  regs_in.ctrl_vers_i           <= fpga_version_i;
  regs_in.csr1_ppsrdy_i         <= rf_pps_rdy;
  regs_in.csr1_wrlost_i         <= wr_link_lost;
  regs_in.csr1_wrok_i           <= link_ok_i;
  regs_in.csr1_slokrun_i        <= slok_run;
  regs_in.csr1_slokaok_i        <= slok_aok;
  regs_in.csr1_sloknok_i        <= slok_nok;
  regs_in.csr1_sraok_i          <= sr_aok;
  regs_in.csr1_srnok_i          <= sr_nok;
  regs_in.csr1_boosaok_i        <= bo_aok;
  regs_in.csr1_boosnok_i        <= bo_nok;
  regs_in.csr1_t0wd_i           <= t0wd;
  regs_in.csr1_bu44_lsb_i       <= bu44_lsb;
  regs_in.csr1_sync_src_ack_i   <= sync_src_ack;
  regs_in.csr1_dbg_i            <= "000000";
  
  regs_in.csr0_d3srun_i     <= rt_d3s_run_i;
  regs_in.csr0_352lkd_i     <= pll_352_lkd_i;
  regs_in.csr0_pdlkdet_i    <= dds_pd_lkdet_i;
  regs_in.csr0_injerr_i     <= error_inj;
  regs_in.csr0_rfoe_i       <= rt_d3s_locked_i;
  regs_in.csr0_t0lost_i     <= t0_lost;
  regs_in.csr0_rflost_i     <= rf_lost;
  regs_in.csr0_t0quiet_i    <= not(regs_out.ctrl_bunchen_o) or not(inj_run);
  regs_in.csr0_slave_i      <= slave_bit_i;
  
  regs_in.csr0_rfadjdet_i   <= rf_dbg_adj;
  regs_in.csr0_rfadjrun_i   <= rf_dbg_run;
  regs_in.rf_pps_lo_i                        <= cnt_bu44_ppsa(31 downto 0);
  regs_in.rf_pps_hi_i(RFTIMEWID-33 downto 0) <= cnt_bu44_ppsa(RFTIMEWID-1 downto 32);

  regs_in.pps_delta_freq_i  <= freq_bu44(31 downto 0);
  
  regs_in.t0lost_cnt_i      <= t0_lost_cnt;
  regs_in.t0_cnt_i          <= t0_cnt;
  regs_in.hist0_i           <= cnt_hist0;
  regs_in.hist1_i           <= cnt_hist1;
  regs_in.hist2_i           <= cnt_hist2;
  regs_in.ddt_hist_i        <= metas_3 & metas_2 & metas_1 & metas_0;

  regs_in.latch_gundel_i                    <= rego_del_gun;
  regs_in.latch2_t0del_i(29 downto 0)       <= rego_t0_del;
  regs_in.latch_extdel_i                    <= rego_del_ext;  
  
  out_mux_o      <= regs_out.t0_in_mux_o(1 downto 0);
  sync_95xx_o    <= syncrf_rf;
  rf_en_o        <= regs_out.ctrl_rfen_o;
  rst352_o       <= rst_352_long;
  buclk_injerr_o <= error_inj;
  slok_run_o     <= slok_run;
  
  rf_maincnt_o(RFTIMEWID-1 downto 0) <= cnt_bu44;
--  rf_stampin_o(RFTIMEWID-1 downto 0) <= cnt_bu44_stampin;
  rf_pps_stamp_o                     <= cnt_bu44_ppsa;
  rf_paps_stamp_o                    <= cnt_bu44_papsa;
  rf_pbps_stamp_o                    <= cnt_bu44_pbpsa;
  srboo_pps_stamp_o                  <= cnt8_ppsa;
  rf_pps_rdy_bcc_o                   <= rf_pps_rdy_bcc;

  input_pol_o                        <= regs_out.stamp_csr_pol_o;
  input_ena_o                        <= regs_out.stamp_csr_enable_o;
  
--Map 12 outputs from wishbone interface

  pul_del(1)    <= rego_out1_del(31 downto 3);
  tog31_ph(1)   <= rego_out1_del(2 downto 0);
  div_992(1)    <= rego_out1_del(31 downto 10);
  
  pul_del(2)    <= rego_out2_del(31 downto 3);
  tog31_ph(2)   <= rego_out2_del(2 downto 0);
  div_992(2)    <= rego_out2_del(31 downto 10);
  
  pul_del(3)    <= rego_out3_del(31 downto 3);
  tog31_ph(3)   <= rego_out3_del(2 downto 0);
  div_992(3)    <= rego_out3_del(31 downto 10);

  pul_del(4)    <= rego_out4_del(31 downto 3);
  div_992(4)    <= rego_out4_del(31 downto 10);

  pul_del(5)    <= rego_out5_del(31 downto 3);
  div_992(5)    <= rego_out5_del(31 downto 10);

  pul_del(6)    <= rego_out6_del(31 downto 3);
  div_992(6)    <= rego_out6_del(31 downto 10);

  pul_del(7)    <= rego_out7_del(31 downto 3);
  div_992(7)    <= rego_out7_del(31 downto 10);

  pul_del(8)    <= rego_out8_del(31 downto 3);
  div_992(8)    <= rego_out8_del(31 downto 10);

  pul_del(9)    <= rego_out9_del(31 downto 3);
  div_992(9)    <= rego_out9_del(31 downto 10);

  pul_del(10)   <= rego_out10_del(31 downto 3);
  div_992(10)   <= rego_out10_del(31 downto 10);

  pul_del(11)   <= rego_out11_del(31 downto 3);
  div_992(11)   <= rego_out11_del(31 downto 10);

  pul_del(12)   <= rego_out12_del(31 downto 3);
  div_992(12)   <= rego_out12_del(31 downto 10);

  pulsiones : for K in 4 to 12 generate
  pulse_o(K)          <=  clkb_o(K) or pulb_o(K) or pulb_seq(K);
  end generate;

  pulsiongun : for K in 2 to 3 generate
  process(buclk_352_i)        -- "true" RF
  begin
    if buclk_352_i'event and buclk_352_i = '1' then
      if pul_mode(K)(1) = '0' and pul_mode(K)(0) = '1' then -- gun
        pulse_o(K)      <= guno xor pul_pol(K);
      elsif pul_mode(K)(1) = '1' and pul_mode(K)(0) = '0' then -- pulse
        puloute(K)      <= pulb_rf_o(K);
        pulse_o(K)      <= puloute(K); -- to keep same pipe as channel#1
      else                             -- other
        pipoute(K)      <= clkb_o(K);
        pulse_o(K)      <= pipoute(K);
      end if;
    end if;
  end process;
  end generate;
            
  process(buclk_352_i)        -- "true" RF
  begin
    if buclk_352_i'event and buclk_352_i = '1' then
      if pul_mode(1) = "11" then
        if pul_srce(1) = "100" then
          puloute(1)      <= clk_16b;
        elsif pul_srce(1) = "101" then
          puloute(1)      <= clk_4b;
        else
          puloute(1)      <= clkb_o(1);
        end if;
      else
        puloute(1)        <= pulb_rf_o(1);
      end if;
      pipoute(1)          <= puloute(1);
      pulse_o(1)          <= pipoute(1);
    end if;
  end process;
--  end generate;
            
end rtl;
