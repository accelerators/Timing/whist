library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.wishbone_pkg.all;

package buclk_sdb_pkg is

  -----------------------------------------------------------------------------
  -- WR bunch clock proto 
  -----------------------------------------------------------------------------
  constant c_buclk_sdb : t_sdb_device := (
    abi_class     => x"0000", -- undocumented device
    abi_ver_major => x"01",
    abi_ver_minor => x"01",
    wbd_endian    => c_sdb_endian_big,
    wbd_width     => x"7", -- 8/16/32-bit port granularity
    sdb_component => (
    addr_first    => x"0000000000000000",
    addr_last     => x"000000000000ffff",
    product => (
    vendor_id     => x"000000000000CE42", -- CERN
    device_id     => x"00000016",              -- value specific to buclk
    version       => x"00000001",
    date          => x"20141217",
    name          => "WR-BUCLK-2         ")));	 -- GG caution 19 chars string here
	 
end buclk_sdb_pkg;

