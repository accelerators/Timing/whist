#ifndef DAC_H
#define DAC_H

//define
#define DAC_ADDR 0x90
#define BUS_TIME_OUT 3000000

//Write Commands
#define DAC_WRITE_CHANNEL_INPUT_REG 0x0
#define DAC_UPDATE_CHANNEL_REG 0x1
#define DAC_WRITE_CHANNEL_REG_AND_UPDATE_ALL 0x2
#define DAC_WRITE_CHANNEL_REG_AND_UPDATE 0x3
#define DAC_POWER_DOWN 0x4
#define DAC_WRITE_TO_CLR_CODE_REG 0x5
#define DAC_WRITE_TO_LDAC_REG 0x6
#define DAC_SOFT_RST 0x7

//Read Commands
#define DAC_READ_CHANNEL_INPUT_REG 0x0
#define DAC_READ_CHANNEL_REG 0x1
#define DAC_READ_POWER_DOWN_REG 0x4
#define DAC_READ_FROM_CLR_CODE_REG 0x5
#define DAC_READ_FROM_LDAC_REG 0x6

//Access Commands
#define DAC_CHANNEL_A 0x0
#define DAC_CHANNEL_B 0x1
#define DAC_CHANNEL_C 0x2
#define DAC_CHANNEL_D 0x3
#define DAC_CHANNEL_E 0x4
#define DAC_CHANNEL_F 0x5
#define DAC_CHANNEL_G 0x6
#define DAC_CHANNEL_H 0x7
#define DAC_CHANNEL_ALL 0xf

#define CA_BYTE(c,a) ((c << 4) | a)

uint8_t has_dac = 0;
static int i2cif;

//routine
uint8_t dac_init(void)
{
  i2cif = 0;

  mi2c_init(i2cif);
  has_dac = 0;

  if(mi2c_poll(BUS_TIME_OUT))
  {
    has_dac = 1;
    mi2c_lock();
    //the device address is shifted to right by 1
    //because the routine shifts the i2c_addr to left by 1!!!
    if(!mi2c_devprobe(i2cif, DAC_ADDR >> 1))
    {
      if(!mi2c_devprobe(i2cif, DAC_ADDR >> 1))
      {
        has_dac = 0;
      }
    }
    mi2c_unlock();
  }
  return has_dac;
}

uint8_t dac_rd_channel(uint8_t channel, uint8_t *data)
{
  uint8_t dummy;
  if(mi2c_poll(BUS_TIME_OUT))
  {
    mi2c_lock();
    mi2c_start(i2cif);
    //device address + write bit (bit 0 = 0)
    mi2c_put_byte(i2cif,DAC_ADDR);
    //send read command
    mi2c_put_byte(i2cif,CA_BYTE(DAC_READ_CHANNEL_REG,channel));
    //send repeat start
    mi2c_repeat_start(i2cif);
    //device address + read bit (bit 0 = 1)
    mi2c_put_byte(i2cif,DAC_ADDR|0x1);
    //read MSDB byte with ack (last = 0)
    mi2c_get_byte(i2cif,data,0);
    //read LSDB byte with no ack (last = 1)
    mi2c_get_byte(i2cif,&dummy,1);
    mi2c_stop(i2cif);
    mi2c_unlock();
    return 1;
  }
  else
  {
    //bus time out. Another master holds the bus
    return 0;
  }
}

uint8_t dac_wr_channel(uint8_t channel, uint8_t data)
{
  if(mi2c_poll(BUS_TIME_OUT))
  {
    mi2c_lock();
    mi2c_start(i2cif);
    //device address + write bit (bit 0 = 0)
    mi2c_put_byte(i2cif,DAC_ADDR);
    //write dac channel input register and update channel output
    mi2c_put_byte(i2cif,CA_BYTE(DAC_WRITE_CHANNEL_REG_AND_UPDATE,channel));
    //write MSDB byte
    mi2c_put_byte(i2cif,data);
    //write LSDB byte
    mi2c_put_byte(i2cif,0); //dummy byte, DAC don't care
    mi2c_stop(i2cif);
    mi2c_unlock();
    return 1;
  }
  else
  {
    //bus time out. Another master holds the bus
    return 0;
  }
}

#endif //DAC_H
