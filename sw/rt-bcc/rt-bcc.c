#include <rt.h>
#include "bcc_regs.h"
#include "bcc.h"
#include "ioconf.h"
#include "dac.h"
#include "si571.h"

#define DEBUG

#ifdef DEBUG
const char* display_mode(struct bcc_control ctrl)
{
  if(ctrl.mode == BCC_MASTER_MODE)
  {
    return "Master";
  }
  else
  {
    return "Slave";
  }
}
#endif

static inline volatile struct bcc_remote_message* bcc_prepare_message(uint32_t id)
{
  volatile struct bcc_remote_message *msg = mq_map_out_buffer(REMOTE, BCC_OUT_SLOT);

  mq_claim(REMOTE, BCC_OUT_SLOT);

  msg->hdr.target_ip = 0xffffffff;    /* broadcast */
  msg->hdr.target_port = 0xebd0;      /* port */
  msg->hdr.target_offset = MQ_IN(BCC_IN_SLOT);    /* target EB slot */

  msg->event_id = id;
  return msg;
}

static inline void bcc_master_start_event(void)
{
  volatile struct bcc_remote_message *msg = bcc_prepare_message(BCC_MASTER_START_ID);
  mq_send(REMOTE,BCC_OUT_SLOT,32);
	//write monostable master_start
  dp_writel((dp_readl(BCC_REG_CSR) |  BCC_CSR_MASTER_START),BCC_REG_CSR);
#ifdef DEBUG
  pp_printf("[Master] master start\n");
#endif
}

static inline void bcc_master_rfpps_event(struct bcc_control *ctrl)
{
  if(ctrl->master_pps_armed == 0)
  {
    //write monostable PPS_arm
    dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_ARM_PPS),BCC_REG_CSR);
    ctrl->master_pps_armed = 1;
  }
  else
  {
    if( (dp_readl(BCC_REG_CSR) & BCC_CSR_DONE_PPS) == BCC_CSR_DONE_PPS )
    {
      ctrl->master_pps_armed = 0;
      //read RF@PPS - a - b and srboo from buclk (RF stamp)
      uint32_t rfpps_stamp_hi = dp_readl(BCC_REG_PPS_STAMP_HI);
      uint32_t rfpps_stamp_lo = dp_readl(BCC_REG_PPS_STAMP_LO);
      uint32_t rfpaps_hi = dp_readl(BCC_REG_PAPS_HI);
      uint32_t rfpaps_lo = dp_readl(BCC_REG_PAPS_LO);
      uint32_t rfpbps_hi = dp_readl(BCC_REG_PBPS_HI);
      uint32_t rfpbps_lo = dp_readl(BCC_REG_PBPS_LO);
      uint32_t srboo_stamp = dp_readl(BCC_REG_SRBOO_STAMP);
			ctrl->rf_at_pps_hi = rfpps_stamp_hi;
			ctrl->rf_at_pps_lo = rfpps_stamp_lo;
			ctrl->tai_sec_at_pps = lr_readl(WRN_CPU_LR_REG_TAI_SEC);
      volatile struct bcc_remote_message *msg = bcc_prepare_message(BCC_MASTER_RFPPS_ID);
      msg->rf_cnta_h = rfpps_stamp_hi;
      msg->rf_cnta_l = rfpps_stamp_lo;
      msg->rf_cntb_h = rfpaps_hi;
      msg->rf_cntb_l = rfpaps_lo;
      msg->rf_cntc_h = rfpbps_hi;
      msg->rf_cntc_l = rfpbps_lo;
      msg->srboo = srboo_stamp;
      //send RF@PPS message
      mq_send(REMOTE,BCC_OUT_SLOT,32);

      //write RF@PPS and srboo to buclk
      dp_writel(rfpps_stamp_hi,BCC_REG_PPS_M_STAMP_HI);
      dp_writel(rfpps_stamp_lo,BCC_REG_PPS_M_STAMP_LO);
      dp_writel(rfpaps_hi,BCC_REG_PAPS_M_HI);
      dp_writel(rfpaps_lo,BCC_REG_PAPS_M_LO);
      dp_writel(rfpbps_hi,BCC_REG_PBPS_M_HI);
      dp_writel(rfpbps_lo,BCC_REG_PBPS_M_LO);
      dp_writel(srboo_stamp,BCC_REG_PPS_M_SRBOO);
      //set RFPPS_received monostable bit to buclk
      dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_RFPPS_RECV),BCC_REG_CSR);
#ifdef DEBUG
      pp_printf("[Master] RF@PPS   %x %x\n",rfpps_stamp_hi,rfpps_stamp_lo);
#endif
    }
  }
}

static inline void bcc_master_t0_event(struct bcc_control *ctrl)
{
  if(ctrl->master_t0_armed == 0)
  {
    //write monostable t0_arm bit
    dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_ARM_T0),BCC_REG_CSR);
    ctrl->master_t0_armed = 1;
  }
  else
  {
    if( (dp_readl(BCC_REG_CSR) & BCC_CSR_DONE_T0) == BCC_CSR_DONE_T0 )
    {
      ctrl->master_t0_armed = 0;
      //read trigger snapshot (RF stamp)
      uint32_t trigger_stamp_hi = dp_readl(BCC_REG_T0_STAMP_HI);
      uint32_t trigger_stamp_lo = dp_readl(BCC_REG_T0_STAMP_LO);
      volatile struct bcc_remote_message *msg = bcc_prepare_message(BCC_MASTER_T0_ID);
      msg->rf_cnta_h = trigger_stamp_hi;
      msg->rf_cnta_l = trigger_stamp_lo;
      msg->rf_cntb_h = 0;
			//transfer bcl_pipe bit from master here, cleared below
      msg->rf_cntb_h = ctrl->master_bcl_pipe;
			//transfer seq_en bit from master here
      msg->rf_cntb_l = ctrl->seq_enable;
      //send T0 stamp message
      mq_send(REMOTE,BCC_OUT_SLOT,32);

      //write T0 stamp to buclk
      dp_writel(trigger_stamp_hi,BCC_REG_T0_M_STAMP_HI);
      dp_writel(trigger_stamp_lo,BCC_REG_T0_M_STAMP_LO);
      //set monostable T0 start and-or Seq_start bit(s) to buclk
      dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_T0_START),BCC_REG_CSR);
			if(ctrl->seq_enable == 1)
				{
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SEQ_START),BCC_REG_CSR);
				}
      //set monostable BCL_PIPE bit to buclk if active and clear it
			if(ctrl->master_bcl_pipe == 1)
				{
					//actualize bunch clock params in buclk
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_BCL_PIPE),BCC_REG_CSR);
					ctrl->master_bcl_pipe = 0;
				}
      //toggle the led
			if(ctrl->t0_led == 0)
				{
					ctrl->t0_led = 1;
					//set t0_led static bit into CSR
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_T0_LED),BCC_REG_CSR);
				}
			else
				{
					ctrl->t0_led = 0;
					//reset t0_led static bit into CSR
					dp_writel((dp_readl(BCC_REG_CSR) & ~BCC_CSR_T0_LED),BCC_REG_CSR);
				}
#ifdef DEBUG
      pp_printf("[Master] %x %x\n",trigger_stamp_hi,trigger_stamp_lo);
#endif
      //send again T0 stamp message after 120 us
			delay(7500);
      volatile struct bcc_remote_message *msgbis = bcc_prepare_message(BCC_MASTER_T0_ID);
      msgbis->rf_cnta_h = trigger_stamp_hi;
      msgbis->rf_cnta_l = trigger_stamp_lo;
      msgbis->rf_cntb_h = 0;
			//transfer seq_en bit from master here
      msgbis->rf_cntb_l = ctrl->seq_enable;
      //send T0 stamp message
      mq_send(REMOTE,BCC_OUT_SLOT,32);
    }
  }
}

void pps_mon_razhisto(struct bcc_pps_mon *ppsm)
{
	ppsm->histo_pps_0 = 0;
	ppsm->histo_pps_1 = 0;
	ppsm->histo_pps_2 = 0;
	ppsm->histo_pps_3 = 0;
	ppsm->histo_pps_4 = 0;
	ppsm->histo_pps_5 = 0;
	ppsm->histo_pps_6 = 0;
	ppsm->histo_pps_7 = 0;
	ppsm->histo_pps_8 = 0;
#ifdef DEBUG
  pp_printf("RAZ histo\n");
#endif
}

void pps_mon_init(struct bcc_pps_mon *ppsm)
{
	ppsm->index_max = 10; //default
	ppsm->adjust_max = 2;
	ppsm->histo_ok = 7; //default
	ppsm->status = 0;
	ppsm->state = 0;
	ppsm->adjust_ena = 1; //default
	ppsm->adjust_cnt = 0;
	ppsm->index = 0;
#ifdef DEBUG
  pp_printf("pps_mon init ; ");
#endif
	pps_mon_razhisto(ppsm);
}

void pps_mon_mstart_init(struct bcc_pps_mon *ppsm)
{
	ppsm->state = 1; //enable monitoring
	ppsm->adjust_ena = 1; //enable auto eval
	ppsm->adjust_cnt = 0; //clear auto eval parameters
	ppsm->index = 0; //clear auto eval parameters
#ifdef DEBUG
  pp_printf("pps_mon mstart init ; ");
#endif
	pps_mon_razhisto(ppsm);
}

void bcc_slave_ppsmon(struct bcc_pps_mon *ppsm)  //just wait for slok_done
{
	if(((dp_readl(BCC_REG_CSR) & BCC_CSR_DONE_SLOK) == BCC_CSR_DONE_SLOK) && (ppsm->state == 0)){
		ppsm->state = 1;
		ppsm->adjust_ena = 1;
		ppsm->index = 0;
		ppsm->index_max = 10; //default for auto eval
#ifdef DEBUG
		pp_printf("in bcc_slave_ppsmon : slok_done received. ");
		pp_printf("csr = %x\n",dp_readl(BCC_REG_CSR));
#endif
		pps_mon_razhisto(ppsm);
	}
}
void bcc_slave_rx(struct bcc_control *ctrl, struct bcc_pps_mon *ppsm)
{
  if(rmq_poll(BCC_IN_SLOT))
  {
    struct bcc_remote_message *msg = mq_map_in_buffer(REMOTE,BCC_IN_SLOT) - sizeof(struct rmq_message_addr);
    switch(msg->event_id)
    {
    case BCC_MASTER_START_ID:
			//write monostable master_start
      dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_MASTER_START),BCC_REG_CSR);
			pps_mon_mstart_init(ppsm);
	    ctrl->master_start = 1;
#ifdef DEBUG
      pp_printf("[Slave] master start\n");
#endif
      break;
    case BCC_MASTER_T0_ID:
			//do if stamp(n-1) != message stamp
			if((ctrl->t0stamp_lo != msg->rf_cnta_l) || (ctrl->t0stamp_hi != msg->rf_cnta_h))
				{
					//write T0 stamp to buclk
					dp_writel(msg->rf_cnta_h,BCC_REG_T0_M_STAMP_HI);
					dp_writel(msg->rf_cnta_l,BCC_REG_T0_M_STAMP_LO);
					//set monostable T0_start bit to buclk
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_T0_START),BCC_REG_CSR);
					//set monostable Seq_start bit to buclk
					if(msg->rf_cntb_l == 1)
						{
							dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SEQ_START),BCC_REG_CSR);
						}
					else
						{
							ctrl->t0bis = 1;
						}
					//set monostable BCL_PIPE bit to buclk if in message
					if(msg->rf_cntb_h == 1)
						{
							//actualize bunch clock params in buclk
							dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_BCL_PIPE),BCC_REG_CSR);
						}
					//toggle the led
					if(ctrl->t0_led == 0)
						{
							ctrl->t0_led = 1;
							//set t0_led static bit into CSR
							dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_T0_LED),BCC_REG_CSR);
						}
					else
						{
							ctrl->t0_led = 0;
							//reset t0_led static bit into CSR
							dp_writel((dp_readl(BCC_REG_CSR) & ~BCC_CSR_T0_LED),BCC_REG_CSR);
						}
#ifdef DEBUG
					pp_printf("[Slave] %x %x\n\n",dp_readl(BCC_REG_T0_M_STAMP_HI),
                                    dp_readl(BCC_REG_T0_M_STAMP_LO));
#endif
					if(ctrl->t0bis == 0)
					{
						ctrl->t0bis_tai_sec = lr_readl(WRN_CPU_LR_REG_TAI_SEC);
						ctrl->t0bis_tai_cyc = lr_readl(WRN_CPU_LR_REG_TAI_CYCLES);
					}
					else
					{
						ctrl->t0bis = 0;
					}
					//update ctrl stamp
					ctrl->t0stamp_lo = msg->rf_cnta_l;
					ctrl->t0stamp_hi = msg->rf_cnta_h;
				}
			else
				{
					if(msg->rf_cntb_l == 1)
					{
					  //t0bis_cnt counts 2d T0, to be compared with vhdl t0_cnt when t0 stopped
					  ctrl->t0bis_cnt += 1;
						ctrl->t0bis = 1;
					}
					else
					{
						ctrl->t0bis = 1;
					}
				}
      break;
    case BCC_MASTER_RFPPS_ID:
			if(ctrl->master_start == 1)
			{
			  //reset t0bis_cnt
			  ctrl->t0bis_cnt = 0;
				ctrl->t0bis = 1;
				ctrl->master_start = 0;
			}
      //write RF@PPS to buclk
      dp_writel(msg->rf_cnta_h,BCC_REG_PPS_M_STAMP_HI);
      dp_writel(msg->rf_cnta_l,BCC_REG_PPS_M_STAMP_LO);
      dp_writel(msg->rf_cntb_h,BCC_REG_PAPS_M_HI);
			dp_writel(msg->rf_cntb_l,BCC_REG_PAPS_M_LO);
      dp_writel(msg->rf_cntc_h,BCC_REG_PBPS_M_HI);
      dp_writel(msg->rf_cntc_l,BCC_REG_PBPS_M_LO);
      dp_writel(msg->srboo,BCC_REG_PPS_M_SRBOO);
      //set monostable RFPPS_received bit to buclk
      dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_RFPPS_RECV),BCC_REG_CSR);
#ifdef DEBUG
      pp_printf("[Slave] RF@PPS master stamp  %x %x\n\n",dp_readl(BCC_REG_PPS_M_STAMP_HI),
                                           dp_readl(BCC_REG_PPS_M_STAMP_LO));
      pp_printf("[Slave] RF@PPS slave cnt_bu44 %x %x\n\n",dp_readl(BCC_REG_PPS_STAMP_HI),
                                           dp_readl(BCC_REG_PPS_STAMP_LO));
      pp_printf("[Slave] SRBOO@PPS slave  %x \n\n", dp_readl(BCC_REG_SRBOO_STAMP));
#endif
			//now add process pps_mon if active
			if((ppsm->state == 1) && (ppsm->index <= ppsm->index_max))
			{
				//read RF@PPS from buclk (RF stamp) and set PPS from Master
				uint32_t rfpps_stamp_hi = dp_readl(BCC_REG_PPS_STAMP_HI);
				uint32_t rfpps_stamp_lo = dp_readl(BCC_REG_PPS_STAMP_LO);
				uint64_t rfpps_stamp = ((uint64_t)(uint32_t)(rfpps_stamp_hi) << 32) | rfpps_stamp_lo;
				uint64_t pps_m_stamp = (((uint64_t)(msg->rf_cnta_h) << 32) | (uint64_t)(msg->rf_cnta_l));
				//calculate delta and histogram
				int64_t delta_stamp = (int64_t)(rfpps_stamp - pps_m_stamp);
				if (delta_stamp == 0) {
					ppsm->histo_pps_4 += 1;
				  }
				if (delta_stamp == -1) {
					ppsm->histo_pps_3 += 1;
				  }
				if (delta_stamp == -2) {
					ppsm->histo_pps_2 += 1;
				  }
				if (delta_stamp == -3) {
					ppsm->histo_pps_1 += 1;
				  }
				if (delta_stamp <= -4) {
					ppsm->histo_pps_0 += 1;
				  }
				if (delta_stamp == 1) {
					ppsm->histo_pps_5 += 1;
				  }
				if (delta_stamp == 2) {
					ppsm->histo_pps_6 += 1;
				  }
				if (delta_stamp == 3) {
					ppsm->histo_pps_7 += 1;
				  }
				if (delta_stamp >= 4) {
					ppsm->histo_pps_8 += 1;
				  }
				ppsm->index += 1;				
#ifdef DEBUG
				pp_printf("ppsmon eval loop; index %d\n", ppsm->index);
				pp_printf("histo_4= %d\n", ppsm->histo_pps_4);
				pp_printf("histo_3= %d\n", ppsm->histo_pps_3);
				pp_printf("histo_2= %d\n", ppsm->histo_pps_2);
				pp_printf("histo_5= %d\n", ppsm->histo_pps_5);
				pp_printf("histo_6= %d\n", ppsm->histo_pps_6);
				pp_printf("rfpps_hi= %x ; rfpps_lo= %x\n", rfpps_stamp_hi, rfpps_stamp_lo);
				pp_printf("master_stamp_hi= %x ; master_stamp_lo= %x\n", (uint32_t)(pps_m_stamp>>32),(uint32_t)(pps_m_stamp&0xffffffff));
				pp_printf("rfpps_stamp_hi= %x ; rfpps_stamp_lo= %x\n", (uint32_t)(rfpps_stamp>>32), (uint32_t)(rfpps_stamp&0xffffffff));
				pp_printf("delta_stamp= %d\n", (uint32_t)(delta_stamp&0xffffffff));
#endif
			}
			else if (ppsm->state == 1) //evaluate and set : adjust, status and loop_end
			{
				if (ppsm->histo_pps_4 >= ppsm->histo_ok) {
					ppsm->status = 1;
					//write monostable slok_ack
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLOK_ACK),BCC_REG_CSR); //just in case it was triggered by slok
					ppsm->state = 0; //job finished ok
					ppsm->adjust_cnt = 0; // associated with state=0 : end of eval
#ifdef DEBUG
					pp_printf("ppsmon OK\n");
#endif
				  }
				else if ((ppsm->adjust_ena == 1) && (ppsm->adjust_cnt < ppsm->adjust_max)) {
				  if ((ppsm->histo_pps_5 >= ppsm->histo_ok) || (ppsm->histo_pps_6 >= ppsm->histo_ok) || (ppsm->histo_pps_7 >= ppsm->histo_ok)) {
						//write monostable slok_adj_0
					  dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLOK_ADJ_0),BCC_REG_CSR); //buclk_rf_cnt ahead : minus 1 adjust
					  ppsm->adjust_cnt += 1; //new histo loop
						ppsm->index = 0;
#ifdef DEBUG
						pp_printf("ppsmon adjust -1, cnt=%d\n", ppsm->adjust_cnt);
#endif
					  pps_mon_razhisto(ppsm);
						}
					else if ((ppsm->histo_pps_1 >= ppsm->histo_ok) || (ppsm->histo_pps_2 >= ppsm->histo_ok) || (ppsm->histo_pps_3 >= ppsm->histo_ok)) {
						//write monostable slok_adj_1
					  dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLOK_ADJ_1),BCC_REG_CSR); //buclk_rf_cnt late : plus 1 adjust
					  ppsm->adjust_cnt += 1; //new histo loop
						ppsm->index = 0;				
#ifdef DEBUG
						pp_printf("ppsmon adjust +1, cnt=%d\n", ppsm->adjust_cnt);
#endif
					  pps_mon_razhisto(ppsm);
						}
					else {
					  ppsm->status == 2; //histogram to be evaluated by supervisor : no decision
						//write monostable slok_nok : not OK
						dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLOK_NOK),BCC_REG_CSR); //just in case it was triggered by slok
						ppsm->state = 0; //job finished ko
						ppsm->adjust_cnt = 0; // associated with state=0 : end of eval
#ifdef DEBUG
						pp_printf("ppsmon bad histogram, cnt=%d\n", ppsm->adjust_cnt);
#endif
						}
					}
				else if (ppsm->adjust_ena == 1) { //adjust_max adjusts could not do the job
					ppsm->status == 2; //histogram to be evaluated by supervisor : no decision
					//write monostable slok_nok : not ok
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLOK_NOK),BCC_REG_CSR); //just in case it was triggered by slok
					ppsm->state = 0; //job finished ko
					ppsm->adjust_cnt = 0; // associated with state=0 : end of eval
#ifdef DEBUG
					pp_printf("ppsmon adjust overflow, cnt=%d\n", ppsm->adjust_cnt);
#endif
				  }
				else  { //adjust disabled
					ppsm->status == 0; //no eval, no status
					//write monostable slok_ack
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLOK_ACK),BCC_REG_CSR); //just in case it was triggered by slok
					ppsm->state = 0; //job finished ok
					ppsm->adjust_cnt = 0; // associated with state=0 : end of eval
#ifdef DEBUG
					pp_printf("end ppsmon single\n");
#endif
				  }
			}
      break;
    }
    mq_discard(REMOTE,BCC_IN_SLOT);
  }
}

void init_control(struct bcc_control *ctrl)
{
  ctrl->version = 6;
  ctrl->enabled = 0;
  ctrl->seq_enable = BCC_T0_DISABLE;
  ctrl->master_start = 0;
  ctrl->t0stamp_lo = 0;
  ctrl->t0stamp_hi = 0;
  ctrl->t0bis_cnt = 0;
  ctrl->master_t0_armed = 0;
  ctrl->master_pps_armed = 0;
  ctrl->master_bcl_pipe = 0; //to actualize bunch clock params in network
  ctrl->t0_led = 0;
	ctrl->t0bis = 1;
	ctrl->t0bis_tai_sec = 0;
	ctrl->t0bis_tai_cyc = 0;
	ctrl->tai_sec_at_pps = 3;
	ctrl->rf_at_pps_hi = 1;
	ctrl->rf_at_pps_lo = 2;
  ctrl->mode = BCC_SLAVE_MODE;
	//now write slave mode to vhdl
	dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLAVE),BCC_REG_CSR);
}

static inline void do_control(struct bcc_control *ctrl, struct bcc_pps_mon *ppsm)
{
  uint32_t p = mq_poll();
  //check if message is received on HMQ from host
  if(p & (1 << BCC_IN_SLOT))
  {
    volatile struct bcc_control_message *msg = mq_map_in_buffer(HOST,BCC_IN_SLOT);
#ifdef DEBUG
    pp_printf("[%s] CMD:0x%x - DATA:0x%x\n",display_mode(*ctrl),msg->cmd,msg->data[0]);
#endif
    switch(msg->cmd)
    {
    case BCC_CMD_ENABLE:
      ctrl->enabled = 1;
      break;
    case BCC_CMD_MODE:
      ctrl->mode = msg->data[0];
			//now write slave mode to vhdl as SLAVE=1, MASTER=0
			if(ctrl->mode == BCC_MASTER_MODE)
				{
					dp_writel((dp_readl(BCC_REG_CSR) & ~BCC_CSR_SLAVE),BCC_REG_CSR);
				}
			else
				{
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLAVE),BCC_REG_CSR);
				}
      break;
    case BCC_MASTER_CMD_SEQ_EN:
      //msg->data[0] = read/write request (0=>read | 1=>write)
      //msg->data[1] = enable (0=>disable | 1=>enable)
      if(msg->data[0])
      {
        //write
				ctrl->seq_enable = msg->data[1];
				//write static bit to CSR
				if(msg->data[1] == 0)
				{
					//reset seq_en static bit into CSR
					dp_writel((dp_readl(BCC_REG_CSR) & ~BCC_CSR_SEQ_EN),BCC_REG_CSR);
				}
			  else
				{
					//set seq_en static bit into CSR
					dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SEQ_EN),BCC_REG_CSR);
				}
#ifdef DEBUG
				pp_printf("t0 enable, CSR=%x\n", dp_readl(BCC_REG_CSR));
#endif
      }
      else
      {
				mq_claim(HOST,BCC_OUT_SLOT);
				mq_writel(HOST,(uint32_t)ctrl->seq_enable,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
				mq_send(HOST,BCC_OUT_SLOT,16);
#ifdef DEBUG
				pp_printf("read sequencer_enable\n");
#endif
      }
      break;
    case BCC_MASTER_CMD_START:
      bcc_master_start_event();
      break;
    case BCC_MASTER_CMD_BCL_PIPE:
			//command to be sent to master after programming bunch clock params over network
			if(ctrl->mode == BCC_MASTER_MODE)
				{
					//bcl_pipe monostable will be broadcast by master at next T0
					ctrl->master_bcl_pipe = 1;
				}
      break;
    case BCC_CMD_IOCONF:
      //msg->data[0] = i2c_addr
      //msg->data[1] = read/write request (0=>read | 1=>write)
      //msg->data[2] = data to write if msg->data[0]=1 (write request). Discarded otherwise
      if(msg->data[1])
      {
        //write to i2c ioconf dev
        ioconf_wr_reg(msg->data[0],msg->data[2]);
      }
      else
      {
	uint8_t reg;
        ioconf_rd_reg(msg->data[0],&reg);
        mq_claim(HOST,BCC_OUT_SLOT);
        mq_writel(HOST,reg,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
	mq_send(HOST,BCC_OUT_SLOT,16);
      }
      break;
    case BCC_CMD_DAC:
      //msg->data[0] = read/write request (0=>read | 1=>write)
      //msg->data[1] = DAC channel (0-7 = A-H)
      //msg->data[2] = data to write if msg->data[0]=1 (write request). Discarded otherwise
      if(msg->data[0])
      {
        //write to DAC channel
        dac_wr_channel((uint8_t)msg->data[1],(uint8_t)msg->data[2]);
      }
      else
      {
	uint8_t reg;
        dac_rd_channel((uint8_t)msg->data[1],&reg);
        mq_claim(HOST,BCC_OUT_SLOT);
        mq_writel(HOST,(uint32_t)reg,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
	mq_send(HOST,BCC_OUT_SLOT,16);
      }
      break;
    case BCC_CMD_571:
      //msg->data[0] = read/write request (0=>read | 1=>write)
      //msg->data[1] = register (7-12, 135, 137)
      //msg->data[2] = data to write if msg->data[0]=1 (write request). Discarded otherwise
      if(msg->data[0])
      {
        //write to Si571 register
        si571_wr_channel((uint8_t)msg->data[1],(uint8_t)msg->data[2]);
      }
      else
      {
	uint8_t reg;
        si571_rd_channel((uint8_t)msg->data[1],&reg);
        mq_claim(HOST,BCC_OUT_SLOT);
        mq_writel(HOST,(uint32_t)reg,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
	mq_send(HOST,BCC_OUT_SLOT,16);
      }
      break;
 	  case BCC_CMD_RD_HISTO:
			mq_claim(HOST,BCC_OUT_SLOT);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_0,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_1,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x4);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_2,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x8);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_3,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0xc);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_4,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x10);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_5,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x14);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_6,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x18);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_7,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x1c);
			mq_writel(HOST,(uint32_t)ppsm->histo_pps_8,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x20);
			mq_send(HOST,BCC_OUT_SLOT,16);
#ifdef DEBUG
      pp_printf("Sending histo values for read\n");
#endif
      break;
    case BCC_CMD_SLOK:
			//write monostable slok
      dp_writel((dp_readl(BCC_REG_CSR) | BCC_CSR_SLOK),BCC_REG_CSR);
#ifdef DEBUG
      pp_printf("Slave lock_to_master mono trigger\n");
#endif
      break;
    case BCC_CMD_RD_CSR:
			//read CSR
			mq_claim(HOST,BCC_OUT_SLOT);
      mq_writel(HOST,(uint32_t)dp_readl(BCC_REG_CSR),MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
			mq_send(HOST,BCC_OUT_SLOT,16);
			break;
    case BCC_CMD_RD_VERSION:
			//read version
			mq_claim(HOST,BCC_OUT_SLOT);
      mq_writel(HOST,(uint32_t)ctrl->version,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
			mq_send(HOST,BCC_OUT_SLOT,16);
			break;
    case BCC_CMD_RD_T0BIS:
			//read t0bis_cnt
			mq_claim(HOST,BCC_OUT_SLOT);
      mq_writel(HOST,(uint32_t)ctrl->t0bis_cnt,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
			mq_send(HOST,BCC_OUT_SLOT,16);
			break;
		case BCC_CMD_RD_T0BIS_MISS:
			//read t0bis_miss
			mq_claim(HOST,BCC_OUT_SLOT);
      mq_writel(HOST,(uint32_t)ctrl->t0bis_tai_sec,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
      mq_writel(HOST,(uint32_t)ctrl->t0bis_tai_cyc,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x4);
			mq_send(HOST,BCC_OUT_SLOT,16);
			break;
		case BCC_MASTER_CMD_RD_TAI_PPS:
			//read tai_sec at corresponding rf_at_pps stamp
			mq_claim(HOST,BCC_OUT_SLOT);
      mq_writel(HOST,(uint32_t)ctrl->tai_sec_at_pps,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
      mq_writel(HOST,(uint32_t)ctrl->rf_at_pps_lo,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x4);
      mq_writel(HOST,(uint32_t)ctrl->rf_at_pps_hi,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START+0x8);
			mq_send(HOST,BCC_OUT_SLOT,16);
			break;
		case BCC_CMD_PPS_MON:
      ppsm->index = msg->data[0]; //default is 10
      ppsm->state = 1; //on demand pps_mon
      ppsm->adjust_ena = 0; //just build and read histogram, no adjust
      ppsm->adjust_cnt = 0;
      ppsm->status = 0;
			pps_mon_razhisto(ppsm);
#ifdef DEBUG
      pp_printf("PPS_MON without adjust, index=0x%x\n", msg->data[0]);
#endif
      break;
    case BCC_CMD_VCXO_FREQ:
			; //empty to avoid : a label can only be part of a statement and a declaration is not a statement
      //read from from bcc-core
      uint32_t vcxo_freq = dp_readl(BCC_REG_XO_FREQ);
			mq_claim(HOST,BCC_OUT_SLOT);
      mq_writel(HOST,vcxo_freq,MQ_OUT(BCC_OUT_SLOT)+MQ_SLOT_DATA_START);
      mq_send(HOST,BCC_OUT_SLOT,16);
			break;
    }
    mq_discard(0,BCC_IN_SLOT);
  }
}

int main(void)
{
  pp_printf("RT BCC firmware 1.0\n");

  struct bcc_control ctrl;
  struct bcc_pps_mon ppsm;
  init_control(&ctrl);
  /* TODO: write error bit status if ioconf init fails. */
  if(ioconf_init()==0)
  {
    pp_printf("ERROR: one ore more IOCONF chips does not respond\n");
  }
  if(dac_init()==0)
  {
    pp_printf("ERROR: DAC chip does not respond\n");
  }
  if(si571_init()==0)
  {
    pp_printf("ERROR: Si571 chip does not respond\n");
  }
	
	pps_mon_init(&ppsm);
	
  //wait for bcc enabled
  do
  {
    do_control(&ctrl, &ppsm);
  }while(!ctrl.enabled);

  for(;;)
  {
    if(ctrl.mode == BCC_SLAVE_MODE)
    {
      bcc_slave_rx(&ctrl, &ppsm);
      bcc_slave_ppsmon(&ppsm);
    }
    else
    {
      //master mode
			bcc_master_t0_event(&ctrl);
      bcc_master_rfpps_event(&ctrl);
    }
    do_control(&ctrl, &ppsm);
  }
  return 0;
}
