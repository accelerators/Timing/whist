#ifndef IOCONF_H
#define IOCONF_H

//header to interface the PCF8574 chips allowing configuration:
//First chip
//FINE DELAY LSb -> P0:P7
//Second chip
//FINE DELAY MSb -> P0:P1
//SHDN_ECL       -> P2
//RF_SRCE_MUX    -> P3
//TTLOUT_EN0     -> P4
//TTLOUT_EN1     -> P5
//CLK10_EN       -> P6
//I2C_OK         -> P7

//define
//PCF8574 device addresses (see datasheet)
#define IOCONF1_ADDR 0x70
#define IOCONF2_ADDR 0x72
#define IOCONF3_ADDR 0x74
#define IOCONF4_ADDR 0x76
#define BUS_TIME_OUT 3000000

uint8_t has_ioconf1 = 0;
uint8_t has_ioconf2 = 0;
uint8_t has_ioconf3 = 0;
uint8_t has_ioconf4 = 0;
static int i2cif;

//routine

//description: initialize the RTC chip. It sends a start condition followed by
//             the device address. Retries if no ack is received.
//return     : 1 if the RTC sent an ack, 0 otherwise.
uint8_t ioconf_init(void)
{
  i2cif = 0;

  mi2c_init(i2cif);
  has_ioconf1 = 0;
  has_ioconf2 = 0;

  if(mi2c_poll(BUS_TIME_OUT))
  {
    has_ioconf1 = 1;
    has_ioconf2 = 1;
    mi2c_lock();
    //the device address is shifted to right by 1
    //because the routine shifts the i2c_addr to left by 1!!!
    if(!mi2c_devprobe(i2cif, IOCONF1_ADDR >> 1))
    {
      if(!mi2c_devprobe(i2cif, IOCONF1_ADDR >> 1))
      {
        has_ioconf1 = 0;
      }
    }
    if(!mi2c_devprobe(i2cif, IOCONF2_ADDR >> 1))
    {
      if(!mi2c_devprobe(i2cif, IOCONF2_ADDR >> 1))
      {
        has_ioconf2 = 0;
      }
    }
    if(!mi2c_devprobe(i2cif, IOCONF3_ADDR >> 1))
    {
      if(!mi2c_devprobe(i2cif, IOCONF3_ADDR >> 1))
      {
        has_ioconf3 = 0;
      }
    }
    if(!mi2c_devprobe(i2cif, IOCONF4_ADDR >> 1))
    {
      if(!mi2c_devprobe(i2cif, IOCONF4_ADDR >> 1))
      {
        has_ioconf4 = 0;
      }
    }
    mi2c_unlock();
  }
  return (has_ioconf1+has_ioconf2+has_ioconf3+has_ioconf4);
}

//description: read the value of IOs pins (P0:P7) on chip <addr>:
// IOCONF1_ADDR => 0x70 => delay (P0:P7)
// IOCONF2_ADDR => 0x72 => delay (P0:P1) + en + mux + ...
uint8_t ioconf_rd_reg(uint8_t addr, uint8_t *data)
{
  if(mi2c_poll(BUS_TIME_OUT))
  {
    mi2c_lock();
    mi2c_start(i2cif);
    //device address + read bit (bit 0 = 1)
    mi2c_put_byte(i2cif,addr|0x1);
    //get 1 byte with no ack (last = 1)
    mi2c_get_byte(i2cif,data,1);
    mi2c_stop(i2cif);
    mi2c_unlock();
    return 1;
  }
  else
  {
    //bus time out. Another master holds the bus
    return 0;
  }
}

//description: write a value to IOs pins (P0:P7) on chip <addr>:
// IOCONF1_ADDR => 0x70 => delay (P0:P7)
// IOCONF2_ADDR => 0x72 => delay (P0:P1) + en + mux + ...
uint8_t ioconf_wr_reg(uint8_t addr, uint8_t data)
{
  if(mi2c_poll(BUS_TIME_OUT))
  {
    mi2c_lock();
    mi2c_start(i2cif);
    //device address + write bit (bit 0 = 0)
    mi2c_put_byte(i2cif,addr);
    //write data to register
    mi2c_put_byte(i2cif,data);
    mi2c_stop(i2cif);
    mi2c_unlock();
    return 1;
  }
  else
  {
    //bus time out. Another master holds the bus
    return 0;
  }
}

//description: read a bit of IOs pins (P0:P7) on chip <addr>:
// IOCONF1_ADDR => 0x70 => delay (P0:P7)
// IOCONF2_ADDR => 0x72 => delay (P0:P1) + en + mux + ...
uint8_t ioconf_rd_bit(uint8_t addr, uint8_t bit, uint8_t *data)
{
  uint8_t tmp=0;
  if(mi2c_poll(BUS_TIME_OUT))
  {
    mi2c_lock();
    mi2c_start(i2cif);
    //device address + read bit (bit 0 = 1)
    mi2c_put_byte(i2cif,addr|0x1);
    //get 1 byte with no ack (last = 1)
    mi2c_get_byte(i2cif,&tmp,1);
    mi2c_stop(i2cif);
    mi2c_unlock();
    *data = (tmp >> bit) & 1;
    return 1;
  }
  else
  {
    //bus time out. Another master holds the bus
    return 0;
  }
}

//description: write a bit to IOs pins (P0:P7) on chip <addr>:
// IOCONF1_ADDR => 0x70 => delay (P0:P7)
// IOCONF2_ADDR => 0x72 => delay (P0:P1) + en + mux + ...
uint8_t ioconf_wr_bit(uint8_t addr, uint8_t bit, uint8_t data)
{
  uint8_t tmp=0;
  if(mi2c_poll(BUS_TIME_OUT))
  {
    mi2c_lock();
    mi2c_start(i2cif);
    //device address + read bit (bit 0 = 1)
    mi2c_put_byte(i2cif,addr|0x1);
    //get 1 byte with no ack (last = 1)
    mi2c_get_byte(i2cif,&tmp,1);
    if(data)
    {
      //data = 1, set bit to 1
      tmp |= (1 << bit);
    }
    else
    {
      //data = 0, set bit to 0
      tmp &= ~(1 << bit);
    }
    //device address + write bit (bit 0 = 0)
    mi2c_put_byte(i2cif,addr);
    //write data to register
    mi2c_put_byte(i2cif,tmp);
    mi2c_stop(i2cif);
    mi2c_unlock();
    return 1;
  }
  else
  {
    //bus time out. Another master holds the bus
    return 0;
  }
}

#endif //IOCONF_H
