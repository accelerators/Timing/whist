#ifndef SI571_H
#define SI571_H

//define
#define SI571_ADDR 0x55
#define BUS_TIME_OUT 3000000

uint8_t has_571 = 0;
static int i2cif;

//routine
uint8_t si571_init(void)
{
  i2cif = 0;

  mi2c_init(i2cif);
  has_571 = 0;

  if(mi2c_poll(BUS_TIME_OUT))
  {
    has_571 = 1;
    mi2c_lock();
    //the i2c_addr is 7 bits shifted 1 to left by devprobe
    if(!mi2c_devprobe(i2cif, SI571_ADDR))
    {
      if(!mi2c_devprobe(i2cif, SI571_ADDR))
      {
        has_571 = 0;
      }
    }
    mi2c_unlock();
  }
  return has_571;
}

uint8_t si571_rd_channel(uint8_t channel, uint8_t *data)
{
  //uint8_t dummy;
  if(mi2c_poll(BUS_TIME_OUT))
  {
    mi2c_lock();
    mi2c_start(i2cif);
    //device address + write bit (bit 0 = 0)
    mi2c_put_byte(i2cif,(SI571_ADDR << 1));
    //write register address
    mi2c_put_byte(i2cif,channel);
    //send repeat start
    mi2c_repeat_start(i2cif);
    //device address + read bit (bit 0 = 1)
    mi2c_put_byte(i2cif,(SI571_ADDR << 1)|0x1);
    //device supports read single byte (datasheet P-22) with no ack (last = 1)
    mi2c_get_byte(i2cif,data,1);
    mi2c_stop(i2cif);
    mi2c_unlock();
    return 1;
  }
  else
  {
    //bus time out. Another master holds the bus
    return 0;
  }
}

uint8_t si571_wr_channel(uint8_t channel, uint8_t data)
{
  if(mi2c_poll(BUS_TIME_OUT))
  {
    mi2c_lock();
    mi2c_start(i2cif);
    //device address + write bit (bit 0 = 0)
    mi2c_put_byte(i2cif,(SI571_ADDR << 1));
    //write register address
    mi2c_put_byte(i2cif,channel);
    //single byte write supported by device (datasheet P-22)
    mi2c_put_byte(i2cif,data);
    mi2c_stop(i2cif);
    mi2c_unlock();
    return 1;
  }
  else
  {
    //bus time out. Another master holds the bus
    return 0;
  }
}

#endif //SI571_H
