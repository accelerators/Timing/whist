#include <rt.h>
#include "evt_regs.h"
#include "evt.h"

// a fix delay is defined in order to guarantee
// all nodes receive the event.
// This delay is defined to 50us.
// 2300 ~ 50000ns / 22ns (RF/8 period)
//#define FIX_DELAY 2300
// 4500 yields 100us.
#define FIX_DELAY 4500

static inline volatile struct evt_remote_message* evt_prepare_message(uint32_t id)
{
  volatile struct evt_remote_message *msg = mq_map_out_buffer(REMOTE, EVT_OUT_SLOT);

  mq_claim(REMOTE, EVT_OUT_SLOT);

  msg->hdr.target_ip = 0xffffffff;    /* broadcast */
  msg->hdr.target_port = 0xebd0;      /* port */
  msg->hdr.target_offset = MQ_IN(EVT_IN_SLOT);    /* target EB slot */

  msg->event_id = id;
  return msg;
}

static inline void evt_input_event(struct evt_control *ctrl)
{
  /* TODO: do it more generic (for loop) */

  //IN0
	if((ctrl->config.in_en & IN0_EN_MASK) == IN0_EN_MASK)
		{
			if(ctrl->in0_armed == 0)
				{
					//write monostable in0_arm bit
					dp_writel((dp_readl(EVT_REG_CSR) | EVT_CSR_ARM_IN0),EVT_REG_CSR);
					ctrl->in0_armed = 1;
				}
			else
				{
					if((dp_readl(EVT_REG_CSR) & EVT_CSR_DONE_IN0) == EVT_CSR_DONE_IN0 )
						{
							ctrl->in0_armed = 0;
							//increment counter
							if((ctrl->config.cnt_t_en & IN0_EN_MASK) == IN0_EN_MASK)
								{
									ctrl->cntevt_t[0] += 1;
								}
							//read trigger snapshot (RF stamp)
							uint32_t trigger_stamp_hi = dp_readl(EVT_REG_IN0_STAMPED_MSB);
							uint32_t trigger_stamp_lo = dp_readl(EVT_REG_IN0_STAMPED_LSB);
							uint64_t trigger_target = ((uint64_t)(uint32_t)(trigger_stamp_hi) << 32) | trigger_stamp_lo;
							trigger_target += ctrl->delevt[0];
							uint32_t trigger_target_hi = (uint32_t)(trigger_target >> 32);
							uint32_t trigger_target_lo = (uint32_t)(trigger_target & 0xffffffff);
							volatile struct evt_remote_message *msg = evt_prepare_message(ctrl->config.lut_out_id[0]);
							msg->rf_stamp_msb = trigger_target_hi;
							msg->rf_stamp_lsb = trigger_target_lo;
							//send event message
							mq_send(REMOTE,EVT_OUT_SLOT,32);
						}
				}
		}
  //IN1
  if((ctrl->config.in_en & IN1_EN_MASK) == IN1_EN_MASK)
		{
			if(ctrl->in1_armed == 0)
				{
					//write monostable in1_arm bit
					dp_writel((dp_readl(EVT_REG_CSR) | EVT_CSR_ARM_IN1),EVT_REG_CSR);
					ctrl->in1_armed = 1;
				}
			else
				{
					if((dp_readl(EVT_REG_CSR) & EVT_CSR_DONE_IN1) == EVT_CSR_DONE_IN1 )
						{
							ctrl->in1_armed = 0;
							//increment counter
							if((ctrl->config.cnt_t_en & IN1_EN_MASK) == IN1_EN_MASK)
								{
									ctrl->cntevt_t[1] += 1;
								}
							//read trigger snapshot (RF stamp)
							uint32_t trigger_stamp_hi = dp_readl(EVT_REG_IN1_STAMPED_MSB);
							uint32_t trigger_stamp_lo = dp_readl(EVT_REG_IN1_STAMPED_LSB);
							uint64_t trigger_target = ((uint64_t)(uint32_t)(trigger_stamp_hi) << 32) | trigger_stamp_lo;
							trigger_target += ctrl->delevt[1];
							uint32_t trigger_target_hi = (uint32_t)(trigger_target >> 32);
							uint32_t trigger_target_lo = (uint32_t)(trigger_target & 0xffffffff);
							volatile struct evt_remote_message *msg = evt_prepare_message(ctrl->config.lut_out_id[1]);
							msg->rf_stamp_msb = trigger_target_hi;
							msg->rf_stamp_lsb = trigger_target_lo;
							//send event message
							mq_send(REMOTE,EVT_OUT_SLOT,32);
						}
				}
		}
  //IN2
  if((ctrl->config.in_en & IN2_EN_MASK) == IN2_EN_MASK)
		{
			if(ctrl->in2_armed == 0)
				{
					//write monostable in2_arm bit
					dp_writel((dp_readl(EVT_REG_CSR) | EVT_CSR_ARM_IN2),EVT_REG_CSR);
					ctrl->in2_armed = 1;
				}
			else
				{
					if((dp_readl(EVT_REG_CSR) & EVT_CSR_DONE_IN2) == EVT_CSR_DONE_IN2 )
						{
							ctrl->in2_armed = 0;
							//increment counter
							if((ctrl->config.cnt_t_en & IN2_EN_MASK) == IN2_EN_MASK)
								{
									ctrl->cntevt_t[2] += 1;
								}
							//read trigger snapshot (RF stamp)
							uint32_t trigger_stamp_hi = dp_readl(EVT_REG_IN2_STAMPED_MSB);
							uint32_t trigger_stamp_lo = dp_readl(EVT_REG_IN2_STAMPED_LSB);
							uint64_t trigger_target = ((uint64_t)(uint32_t)(trigger_stamp_hi) << 32) | trigger_stamp_lo;
							trigger_target += ctrl->delevt[2];
							uint32_t trigger_target_hi = (uint32_t)(trigger_target >> 32);
							uint32_t trigger_target_lo = (uint32_t)(trigger_target & 0xffffffff);
							volatile struct evt_remote_message *msg = evt_prepare_message(ctrl->config.lut_out_id[2]);
							msg->rf_stamp_msb = trigger_target_hi;
							msg->rf_stamp_lsb = trigger_target_lo;
							//send event message
							mq_send(REMOTE,EVT_OUT_SLOT,32);
						}
				}
		}
  //IN3
  if((ctrl->config.in_en & IN3_EN_MASK) == IN3_EN_MASK)
		{
			if(ctrl->in3_armed == 0)
				{
					//write monostable in3_arm bit
					dp_writel((dp_readl(EVT_REG_CSR) | EVT_CSR_ARM_IN3),EVT_REG_CSR);
					ctrl->in3_armed = 1;
				}
			else
				{
					if((dp_readl(EVT_REG_CSR) & EVT_CSR_DONE_IN3) == EVT_CSR_DONE_IN3 )
						{
							ctrl->in3_armed = 0;
							//increment counter
							if((ctrl->config.cnt_t_en & IN3_EN_MASK) == IN3_EN_MASK)
								{
									ctrl->cntevt_t[3] += 1;
								}
							//read trigger snapshot (RF stamp)
							uint32_t trigger_stamp_hi = dp_readl(EVT_REG_IN3_STAMPED_MSB);
							uint32_t trigger_stamp_lo = dp_readl(EVT_REG_IN3_STAMPED_LSB);
							uint64_t trigger_target = ((uint64_t)(uint32_t)(trigger_stamp_hi) << 32) | trigger_stamp_lo;
							trigger_target += ctrl->delevt[3];
							uint32_t trigger_target_hi = (uint32_t)(trigger_target >> 32);
							uint32_t trigger_target_lo = (uint32_t)(trigger_target & 0xffffffff);
							volatile struct evt_remote_message *msg = evt_prepare_message(ctrl->config.lut_out_id[3]);
							msg->rf_stamp_msb = trigger_target_hi;
							msg->rf_stamp_lsb = trigger_target_lo;
							//send event message
							mq_send(REMOTE,EVT_OUT_SLOT,32);
						}
				}
		}
}

void evt_slave_rx(struct evt_control *ctrl)
{
  if(rmq_poll(EVT_IN_SLOT))
  {
    struct evt_remote_message *msg = mq_map_in_buffer(REMOTE,EVT_IN_SLOT) - sizeof(struct rmq_message_addr);
    uint8_t index;
    uint32_t rf_msb = msg->rf_stamp_msb;
    uint32_t rf_lsb = msg->rf_stamp_lsb;
    uint64_t rf_target = ((uint64_t)(uint32_t)(rf_msb) << 32) | rf_lsb;
    rf_target += FIX_DELAY;
    for(index = 0; index < 4; index++)
    {
      // check if output is enabled
      if((ctrl->config.out_en & (1 << index)) != 0)
      {
        if(ctrl->config.lut_in_id[index] == msg->event_id)
        {
          // OUT[index] is registered to this ID
					//increment counter
					if((ctrl->config.cnt_r_en & (1 << index)) == (1 << index))
								{
									ctrl->cntevt_r[index] += 1;
								}
          // write the new RF time to be compared in evt_core
          dp_writel((uint32_t)(rf_target >> 32),EVT_REG_OUT0_STAMP_DLY_MSB + 2*(4*index));
          dp_writel((uint32_t)(rf_target & 0xffffffff),EVT_REG_OUT0_STAMP_DLY_LSB + 2*(4*index));
          // notify evt_core to start comparing
          // no need to read first CSR and masking because there are only
          // monostable and read only bits in CSR
          dp_writel(WBGEN2_GEN_MASK(index,1), EVT_REG_CSR);
        }
      }
    }
    mq_discard(REMOTE,EVT_IN_SLOT);
  }
}

void init_control(struct evt_control *ctrl)
{
  ctrl->version = 2;
  ctrl->enabled = 0;
  ctrl->mode = EVT_MASTER_MODE;
  ctrl->in0_armed = 0;
  ctrl->in1_armed = 0;
  ctrl->in2_armed = 0;
  ctrl->in3_armed = 0;
  ctrl->config.in_en = 0;
  ctrl->config.out_en = 0;
  ctrl->config.cnt_t_en = 0;
  ctrl->config.cnt_r_en = 0;
  uint8_t i;
  for(i=0;i<4;i++)
  {
    ctrl->config.lut_in_id[i] = 0;
    ctrl->config.lut_out_id[i] = 0;
  }
	ctrl->srcevt[0] = 0;
	ctrl->srcevt[1] = 0;
	ctrl->srcevt[2] = 0;
	ctrl->srcevt[3] = 0;
	ctrl->cntevt_t[0] = 0;
	ctrl->cntevt_t[1] = 0;
	ctrl->cntevt_t[2] = 0;
	ctrl->cntevt_t[3] = 0;
	ctrl->cntevt_r[0] = 0;
	ctrl->cntevt_r[1] = 0;
	ctrl->cntevt_r[2] = 0;
	ctrl->cntevt_r[3] = 0;
	ctrl->delevt[0] = 0;
	ctrl->delevt[1] = 0;
	ctrl->delevt[2] = 0;
	ctrl->delevt[3] = 0;
}

static inline void do_control(struct evt_control *ctrl)
{
  uint32_t p = mq_poll();
  //check if message is received on HMQ from host
  if(p & (1 << EVT_IN_SLOT))
  {
    volatile struct evt_control_message *msg = mq_map_in_buffer(HOST,EVT_IN_SLOT);
    switch(msg->cmd)
    {
    case EVT_CMD_ENABLE:
      // msg format:
      // data[0]: enable [1] / disable [0]
      if(msg->data[0])
				{
					ctrl->enabled = msg->data[0];
				}
			else
				{
					init_control(&ctrl);
				}
      break;
    case EVT_CMD_MODE:
      ctrl->mode = msg->data[0];
      break;
		case EVT_CMD_DEL:
      // msg format:
      // data[0]: input transmit index (0 to 3)
      // data[1]: 32 bits value
			ctrl->delevt[msg->data[0]] = msg->data[1];
      break;
    case EVT_CMD_CNT_RAZ:
      // msg format:
      // data[0]: input index (0 to 3)
      // data[1]: 0 = r(eceive) ; 1 = t(ransmit)
      if(msg->data[1])
				{
					ctrl->cntevt_t[msg->data[0]] = 0;
				}
			else
				{
					ctrl->cntevt_r[msg->data[0]] = 0;
				}
      break;
    case EVT_CMD_CNT_EN:
      // msg format:
      // data[0]: input (transmit) output (receive) index (0 to 3)
      // data[1]: 0 = r(eceive) ; 1 = t(ransmit)
      // data[2]: enable/disable (0=disable, 1=enable)
      if(msg->data[1])
				{
					ctrl->config.cnt_t_en |= (msg->data[2] << msg->data[0]);
				}
			else
				{
					ctrl->config.cnt_r_en |= (msg->data[2] << msg->data[0]);
				}
			break;
    case EVT_CMD_TRANSMITTER:
      // msg format:
      // data[0]: input index (0 to 3)
      // data[1]: enable/disable (0=disable, 1=enable)
      // data[2]: ID to broadcast if enabled (data[1]=1)
			// data[3]: encoded source word
      if(msg->data[1])
      {
        ctrl->config.in_en |= (1 << msg->data[0]);
        ctrl->config.lut_out_id[msg->data[0]]=msg->data[2];
				ctrl->srcevt[msg->data[0]] = msg->data[3];
      }
      else
      {
        ctrl->config.in_en &= ~(1 << msg->data[0]);
        ctrl->config.lut_out_id[msg->data[0]]=0;
				ctrl->srcevt[msg->data[0]] = 0;
      }
      break;
    case EVT_CMD_RECEIVER:
      // msg format:
      // data[0]: output index (0 to 3)
      // data[1]: enable/disable (0=disable, 1=enable)
      // data[2]: network event ID registration / discarded if data[1]=0 (disable)
      if(msg->data[1])
      {
        ctrl->config.out_en |= (1 << msg->data[0]);
        ctrl->config.lut_in_id[msg->data[0]] = msg->data[2];
      }
      else
      {
        ctrl->config.out_en &= ~(1 << msg->data[0]);
        ctrl->config.lut_in_id[msg->data[0]] = 0;
      }
      break;
    case EVT_CMD_RD_STATUS:
			mq_claim(HOST,EVT_OUT_SLOT);
			uint32_t stator = ((uint32_t)ctrl->config.cnt_t_en << 24) | ((uint32_t)ctrl->config.cnt_r_en << 16)
				| ((uint32_t)ctrl->config.in_en << 8) | ((uint32_t)ctrl->config.out_en)
				| ((ctrl->mode-1) << 31) | (ctrl->enabled << 30)
				| (ctrl->in0_armed << 20) | (ctrl->in1_armed << 21) | (ctrl->in2_armed << 22) | (ctrl->in3_armed << 23);
			mq_writel(HOST,(uint32_t)stator,MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START);
			// receive luts
			mq_writel(HOST,(uint32_t)ctrl->config.lut_in_id[0],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x4);
			mq_writel(HOST,(uint32_t)ctrl->config.lut_in_id[1],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x8);
			mq_writel(HOST,(uint32_t)ctrl->config.lut_in_id[2],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0xc);
			mq_writel(HOST,(uint32_t)ctrl->config.lut_in_id[3],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x10);
			// transmit luts
			mq_writel(HOST,(uint32_t)ctrl->config.lut_out_id[0],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x14);
			mq_writel(HOST,(uint32_t)ctrl->config.lut_out_id[1],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x18);
			mq_writel(HOST,(uint32_t)ctrl->config.lut_out_id[2],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x1c);
			mq_writel(HOST,(uint32_t)ctrl->config.lut_out_id[3],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x20);
			// transmit src
			mq_writel(HOST,(uint32_t)ctrl->srcevt[0],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x24);
			mq_writel(HOST,(uint32_t)ctrl->srcevt[1],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x28);
			mq_writel(HOST,(uint32_t)ctrl->srcevt[2],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x2c);
			mq_writel(HOST,(uint32_t)ctrl->srcevt[3],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x30);
			// transmit delay
			mq_writel(HOST,(uint32_t)ctrl->delevt[0],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x34);
			mq_writel(HOST,(uint32_t)ctrl->delevt[1],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x38);
			mq_writel(HOST,(uint32_t)ctrl->delevt[2],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x3c);
			mq_writel(HOST,(uint32_t)ctrl->delevt[3],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x40);
			mq_send(HOST,EVT_OUT_SLOT,17);
      break;
    case EVT_CMD_CNT_RD:
			mq_claim(HOST,EVT_OUT_SLOT);
			mq_writel(HOST,(uint32_t)ctrl->cntevt_t[0],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START);
			mq_writel(HOST,(uint32_t)ctrl->cntevt_t[1],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x4);
			mq_writel(HOST,(uint32_t)ctrl->cntevt_t[2],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x8);
			mq_writel(HOST,(uint32_t)ctrl->cntevt_t[3],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0xc);
			mq_writel(HOST,(uint32_t)ctrl->cntevt_r[0],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x10);
			mq_writel(HOST,(uint32_t)ctrl->cntevt_r[1],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x14);
			mq_writel(HOST,(uint32_t)ctrl->cntevt_r[2],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x18);
			mq_writel(HOST,(uint32_t)ctrl->cntevt_r[3],MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START+0x1c);
			mq_send(HOST,EVT_OUT_SLOT,16);
      break;
    case EVT_CMD_RD_VERSION:
			//read version
			mq_claim(HOST,EVT_OUT_SLOT);
      mq_writel(HOST,(uint32_t)ctrl->version,MQ_OUT(EVT_OUT_SLOT)+MQ_SLOT_DATA_START);
			mq_send(HOST,EVT_OUT_SLOT,16);
			break;
    }
    mq_discard(HOST,EVT_IN_SLOT);
  }
}

int main(void)
{
  pp_printf("RT EVT firmware 1.0\n");

  struct evt_control ctrl;
  init_control(&ctrl);

  //wait for evt enabled
  do
  {
    do_control(&ctrl);
  }while(!ctrl.enabled);

  for(;;)
  {
    if(ctrl.mode == EVT_SLAVE_MODE)
    {
      evt_input_event(&ctrl);
      evt_slave_rx(&ctrl);
    }
    do_control(&ctrl);
  }
  return 0;
}
