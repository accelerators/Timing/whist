#ifndef BCC_H
#define BCC_H

#include <stdint.h>

#ifdef WRNODE_RT
#include "rt-mqueue.h"
#endif

#define BCC_CPU_MAIN 0x1 /* CPU1 controls the Events */

#define HOST   0x0
#define REMOTE 0x1

#define BCC_SLOT     0x1
#define BCC_IN_SLOT  BCC_SLOT
#define BCC_OUT_SLOT BCC_SLOT

#ifdef WRNODE_RT
struct bcc_remote_message {
  struct rmq_message_addr hdr;
  uint32_t event_id;
  uint32_t rf_cnta_h;
  uint32_t rf_cnta_l;
  uint32_t rf_cntb_h;
  uint32_t rf_cntb_l;
  uint32_t rf_cntc_h;
  uint32_t rf_cntc_l;
  uint32_t srboo;
};

struct bcc_control_message {
  uint32_t cmd;
  uint32_t data[32];
};

struct bcc_control {
  uint8_t enabled;
  uint8_t mode;
  uint8_t seq_enable;
  uint8_t master_start;
	uint8_t version;
	uint8_t t0bis;
  uint32_t t0stamp_lo;
  uint32_t t0stamp_hi;
  uint32_t t0bis_cnt;
	uint32_t t0bis_tai_sec;
	uint32_t t0bis_tai_cyc;
	uint32_t tai_sec_at_pps;
	uint32_t rf_at_pps_hi;
	uint32_t rf_at_pps_lo;
  uint8_t master_bcl_pipe;
  volatile uint8_t master_t0_armed;
  volatile uint8_t master_pps_armed;
  volatile uint8_t t0_led;
};
#endif

struct bcc_pps_mon {
	uint8_t state;
	uint8_t adjust_ena;
	uint8_t adjust_cnt;
	uint8_t adjust_max;
	uint8_t index;
	uint8_t index_max;
	uint8_t histo_ok;
	uint8_t status;
	uint8_t histo_pps_0;
	uint8_t histo_pps_1;
	uint8_t histo_pps_2;
	uint8_t histo_pps_3;
	uint8_t histo_pps_4;
	uint8_t histo_pps_5;
	uint8_t histo_pps_6;
	uint8_t histo_pps_7;
	uint8_t histo_pps_8;
};

//mode
#define BCC_MASTER_MODE 0x1
#define BCC_SLAVE_MODE  0x2

//T0 mode
#define BCC_T0_DISABLE     0x0
#define BCC_T0_ENABLE      0x1

// EVENT ID LIST
#define BCC_MASTER_START_ID  0xAA
#define BCC_MASTER_T0_ID     0xAB
#define BCC_MASTER_RFPPS_ID  0xAC

// COMMAND ID LIST
#define BCC_CMD_ENABLE           0x1
#define BCC_CMD_MODE             0x2
#define BCC_MASTER_CMD_SEQ_EN    0x3
#define BCC_MASTER_CMD_START     0x4
#define BCC_CMD_IOCONF           0x5
#define BCC_CMD_DAC              0x6
#define BCC_CMD_SLOK             0x7
#define BCC_CMD_PPS_MON          0x8
#define BCC_CMD_RD_HISTO         0x9
#define BCC_CMD_RD_CSR           0xa
#define BCC_CMD_571              0xb
#define BCC_CMD_VCXO_FREQ        0xc
#define BCC_CMD_RD_VERSION       0xd
#define BCC_CMD_RD_T0BIS         0xe
#define BCC_CMD_RD_T0BIS_MISS    0xf
#define BCC_MASTER_CMD_BCL_PIPE     0x10
#define BCC_MASTER_CMD_RD_TAI_PPS   0x11

#endif // BCC_H
