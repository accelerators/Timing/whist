#ifndef EVT_H
#define EVT_H

#include <stdint.h>

#ifdef WRNODE_RT
#include "rt-mqueue.h"
#endif

#define EVT_CPU_MAIN 0x1 /* CPU1 controls the Events */

#define HOST   0x0
#define REMOTE 0x1

#define EVT_SLOT     0x2
#define EVT_IN_SLOT  EVT_SLOT
#define EVT_OUT_SLOT EVT_SLOT

#ifdef WRNODE_RT
struct evt_remote_message {
  struct rmq_message_addr hdr;
  uint32_t event_id;
  uint32_t rf_stamp_msb;
  uint32_t rf_stamp_lsb;
};
#endif

struct evt_control_message {
  uint32_t cmd;
  uint32_t data[32];
};

struct evt_config {
  uint32_t in_en;
  uint32_t out_en;
  uint32_t cnt_t_en;
  uint32_t cnt_r_en;
  uint32_t lut_in_id[4];
  uint32_t lut_out_id[4];
};

struct evt_control {
  uint8_t enabled;
  uint8_t mode;
	uint8_t version;
  struct evt_config config;
  volatile uint8_t in0_armed;
  volatile uint8_t in1_armed;
  volatile uint8_t in2_armed;
  volatile uint8_t in3_armed;
	uint32_t srcevt[4];
	uint32_t cntevt_t[4];
	uint32_t cntevt_r[4];
	uint32_t delevt[4];
};

//mode
#define EVT_MASTER_MODE 0x1
#define EVT_SLAVE_MODE  0x2

//T0 mode
#define EVT_T0_DISABLE     0x0
#define EVT_T0_ENABLE      0x1

// COMMAND ID LIST
#define EVT_CMD_ENABLE      0x1
#define EVT_CMD_MODE        0x2
#define EVT_CMD_TRANSMITTER 0x3
#define EVT_CMD_RECEIVER    0x4
#define EVT_CMD_RD_STATUS   0x5
#define EVT_CMD_CNT_RAZ     0x6
#define EVT_CMD_CNT_RD      0x7
#define EVT_CMD_CNT_EN      0x8
#define EVT_CMD_DEL         0x9
#define EVT_CMD_RD_VERSION  0xa

#define IN0_EN_MASK 0x1UL
#define IN1_EN_MASK 0x2UL
#define IN2_EN_MASK 0x4UL
#define IN3_EN_MASK 0x8UL

#endif // EVT_H
