# jTiming

Application for manage Timing

## Cloning

Any instructions or special flags required to clone the project. For example, if the project has submodules in git, then specify this and give the command to c>

```
git clone --recurse-submodules git@gitlab.esrf.fr:accelerators/Timing/whist.gitgit@gitlab.esrf.fr:accelerators/Timing/whist.git
```

### Dependencies

* jTango Controls 9 or higher.
* AtkCore/AtkWidget
* atkpanel.jar
* jive.jar

#### Toolchain Dependencies

* jdk 1.8 and higher
* build.xml from netbeans

### Build

Instructions on building the project.

Make example:

```bash
open netbeans
open project whist/sw/tango/client/JTiming in netbeans
build
```
