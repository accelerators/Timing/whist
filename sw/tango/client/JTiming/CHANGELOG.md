# Changelog
All notable changes to this project will be documented in this file.

## [1.7.8] - 27-05-2022

### Fixed
* fixed display error in output / input device Tree
* fixed WhistFrame shortcut to Jive

## [1.7.7] - 12-01-2022

### Added
* link in help to Thoubleshooting (confluence)

## [1.7.6] - 10-01-2022

### Fixed
* Improve Output configuration (Event)

## [1.7.5] - 23-11-2021

### Removed
* remove NotCritical management

## [1.7.4] - 29-09-2021

### Fixed
* bug in output frame for reconfigure GUN output
* link to the documentation

## [1.7.3] - 27-05-2021

### Added
* add CHANGELOG.md and add link in application

## [1.7.2] - 27-05-2021

### Added
* jTiming add check for the output and input devices name

add CHANGELOG.md and add link in application

## [1.7.1] - 30-04-2021

### Added
* Update master reset message and add link

## [1.7.0] - 22-03-2021

### Added
* improve lab side without T0 source manage

## [1.6.6] - 04-02-2021

### Added
* Fix path for Synoptic Cross Settings jdraw

## [1.6.5] - 11-01-2021

### Added
* update jdraw path for image (now is no longer on segfs)
