/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import atkpanel.MainPanel;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.AttributeProxy;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.IBooleanScalar;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IEnumScalar;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.core.IStringSpectrum;
import fr.esrf.tangoatk.widget.attribute.ScalarListViewer;
import fr.esrf.tangoatk.widget.attribute.SimpleStringSpectrumViewer;
import fr.esrf.tangoatk.widget.jdraw.SimpleSynopticAppli;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.SettingsManagerProxy;
import fr.esrf.tangoatk.widget.util.Splash;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

/**
 *
 * @author tappret
 */
public class MainFrame extends javax.swing.JFrame{
    ICommand onCmd = null;
    ICommand offCmd = null;
    ICommand resetCmd = null;
    ICommand resetWatchDog = null;
    ICommand resetInjError = null;
    private SettingsManagerProxy smProxy = null;
    
    private final Splash splash = new Splash();
    private ErrorPopup errorPopup = null;
    public ErrorHistory errh = new ErrorHistory();
    final String versionText = new String("$Revision: 1.7.8 $");
    final String versNumber;
    SimpleSynopticAppli crossSyno = null;
    TreeDelayFrame dependingDelayView = null;
    atkpanel.MainPanel atkBunchClock = null;
    private JFrame              statusResetJFrame = null;
    private SimpleStringSpectrumViewer        statusv = new SimpleStringSpectrumViewer();
    ArrayList<atkpanel.MainPanel> atkPanelList = new ArrayList<>();
    boolean expertViewAtkpanel = false;
    private JPanel smPanel;
    private JScrollPane jScrollPane8 = new JScrollPane();
    private ScalarListViewer scalarListViewerT0_Source = new ScalarListViewer();
    private boolean isLabInstance = false;
    
    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        int colon_idx, dollar_idx;
        colon_idx = versionText.lastIndexOf(":");
        dollar_idx = versionText.lastIndexOf("$");
        versNumber = versionText.substring(colon_idx + 1, dollar_idx);
        setIconImage(WhistConstants.wrLogo);
        
        setTitle("jTiming  " + versNumber);

        splash.setTitle("jTiming  " + versNumber);
        splash.setCopyright("(c) ESRF 2005-2017");
        splash.setMessage("Connecting to Sequencer devices ...");
        splash.initProgress();
        splash.setMaxProgress(5);
        
        String tangoHost = System.getProperty("TANGO_HOST");
        if(tangoHost != null){
            if(!(tangoHost.contains("acs:10000") || tangoHost.contains("acs.esrf.fr:10000"))){
                isLabInstance = true;
            }
        }

        errorPopup = ErrorPopup.getInstance();

        splash.progress(1);
        splash.setMessage("Init ...");

        initComponents();
        
        splash.progress(2);
        splash.setMessage("Get Setting Manager config");
        
        addSettingsManagerSupport();
        
        splash.setMessage("loadind Other Devices ...");
        splash.progress(3);
        
        AttributeList al = new AttributeList();
        al.addErrorListener(errh);
        al.addSetErrorListener(errh);
        al.addSetErrorListener(errorPopup);
        
        CommandList cmdL = new CommandList();
        cmdL.addErrorListener(errh);
        cmdL.addErrorListener(errorPopup);
        
        splash.progress(4);
        
        try {
            stateViewerBunchClock.setModel((IDevStateScalar)al.add(WhistConstants.DEV_BUNCHCLOCK + "/State"));
            stateViewerBunchClock.setLabel("BunchClock");
            statusViewerSequencer.setModel((IStringScalar)al.add(WhistConstants.DEV_BUNCHCLOCK + "/Status"));
            
            statusv.setModel((IStringSpectrum)al.add(WhistConstants.DEV_BUNCHCLOCK + "/StatusThread"));
            
            stateViewerOutput.setModel((IDevStateScalar)al.add(WhistConstants.DEV_NETWORK + "/OutputState"));
            stateViewerOutput.setLabel("Global Outputs State");
            
            stateViewerPPS.setModel((IDevStateScalar)al.add(WhistConstants.DEV_NETWORK + "/PpsState"));
            stateViewerPPS.setLabel("PPS State");
            
            if(!isLabInstance){
                stateViewerT0_Source.setModel((IDevStateScalar)al.add(WhistConstants.DEV_BUNCHCLOCK + "/T0_Source"));
                stateViewerT0_Source.setLabel("T0 Source");
            }else{
                stateViewerT0_Source.setVisible(false);
            }
            
            MouseAdapter mouseListenerNetDev = new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if(e.getButton() == MouseEvent.BUTTON1){
                        new atkpanel.MainPanel(WhistConstants.DEV_NETWORK, false);
                    }
                }
            };
            stateViewerPPS.addMouseListener(mouseListenerNetDev);
            stateViewerOutput.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    dependingViewlauncher();
                }
            });
                        
            //ComboBoxModel
            enumScalarFillingPattern.setAttModel((IEnumScalar)al.add(WhistConstants.DEV_BUNCHCLOCK + "/FillingPattern"));
            booleanScalarSetPanelRotatingPulse.setAttModel((IBooleanScalar)al.add(WhistConstants.DEV_BUNCHCLOCK + "/rotatingPulse"));
            
            resetCmd = (ICommand)cmdL.add(WhistConstants.DEV_BUNCHCLOCK + "/Reset");
            ((Device)resetCmd.getDevice()).setDevTimeout(10000);
            
            
            onCmd = (ICommand)cmdL.add(WhistConstants.DEV_BUNCHCLOCK + "/On");
            offCmd = (ICommand)cmdL.add(WhistConstants.DEV_BUNCHCLOCK + "/Off");
            resetWatchDog = (ICommand)cmdL.add(WhistConstants.DEV_NETWORK + "/resetWatchDog");
            resetInjError = (ICommand)cmdL.add(WhistConstants.DEV_NETWORK + "/resetInjError");
            
            
            ((NumberSpectrumListViewer)numberSpectrumListViewer1).setModel((INumberSpectrum)al.add(WhistConstants.DEV_BUNCHCLOCK + "/BoosterBunchList"));
            ((NumberSpectrumListViewer)numberSpectrumListViewer2).setModel((INumberSpectrum)al.add(WhistConstants.DEV_BUNCHCLOCK + "/StorageRingBunchList"));


            //get attribute to display form the settingManager
            AttributeProxy apAttrList = new AttributeProxy(WhistConstants.TIMING_SETTING_MANAGER + "/DefaultAttributes");
            DeviceAttribute dp = apAttrList.read();
            String [] sAttrList = dp.extractStringArray();
            
            
            AttributeList alBunchClockBase = new AttributeList();

            alBunchClockBase.addErrorListener(errh);
            alBunchClockBase.addSetErrorListener(errh);
            alBunchClockBase.addSetErrorListener(errorPopup);
            alBunchClockBase.setFilter(new DelayEntityFilter());
            alBunchClockBase.add(sAttrList);
            scalarListViewerBunchClock.setModel(alBunchClockBase);
            alBunchClockBase.startRefresher();
                        
            AttributeList alWidthBase = new AttributeList();

            alWidthBase.addErrorListener(errh);
            alWidthBase.addSetErrorListener(errh);
            alWidthBase.addSetErrorListener(errorPopup);
            alWidthBase.setFilter(new WidthEntityFilter());
            alWidthBase.add(sAttrList);
            scalarListViewerWidth.setModel(alWidthBase);
            alWidthBase.startRefresher();
            
            AttributeList alPhaseBase = new AttributeList();

            alPhaseBase.addErrorListener(errh);
            alPhaseBase.addSetErrorListener(errh);
            alPhaseBase.addSetErrorListener(errorPopup);
            alPhaseBase.setFilter(new PhaseEntityFilter());
            alPhaseBase.add(sAttrList);
            scalarListViewerPhase.setModel(alPhaseBase);
            alPhaseBase.startRefresher();
            
            if(!isLabInstance){
                String [] attrT0_Source = new String[] {WhistConstants.T0_SOURCE_DEVICE + "/OscillatorTest_ON"
                        ,WhistConstants.T0_SOURCE_DEVICE + "/FreqT0"
                        ,WhistConstants.T0_SOURCE_DEVICE + "/FreqT0_Test"};

                AttributeList alT0_Source = new AttributeList();
                alT0_Source.addErrorListener(errh);
                alT0_Source.addSetErrorListener(errh);
                alT0_Source.addSetErrorListener(errorPopup);
                alT0_Source.add(attrT0_Source);
                scalarListViewerT0_Source.setModel(alT0_Source);
                alT0_Source.startRefresher();
            }
        } catch (ConnectionException ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
            splash.toBack();
            javax.swing.JOptionPane.showMessageDialog(
                    null, (String) ex.getDescription(), "ERREUR",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
            splash.toFront();
        } catch (DevFailed ex) {
            Logger.getLogger(MainFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        al.startRefresher();
        splash.progress(5);
        
        
        WhistsPane wp = new WhistsPane(this);
        wp.startRefresher();
        JScrollPane JScrollPane7 = new JScrollPane(wp);
        
        JScrollPane7.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        JScrollPane7.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        jTabbedPane1.addTab("Modules", JScrollPane7);
        
        if(!isLabInstance){
            //add T0 source management
            jScrollPane8.setBorder(null);
            jScrollPane8.setViewportView(scalarListViewerT0_Source);
            jTabbedPane1.addTab("T0 Source",jScrollPane8);
        }
        
        //Special Look for Labs   
        if(isLabInstance){
            jPanel1.setBackground(Color.PINK);
            jTabbedPane1.setBackground(Color.PINK);
            wp.setBackground(Color.PINK);
            smPanel.setBackground(Color.PINK);
            jPanel4.setBackground(Color.PINK);   
        }
                
        splash.setVisible(false);
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        enumScalarSetPanel2 = new fr.esrf.tangoatk.widget.attribute.EnumScalarSetPanel();
        enumScalarComboEditor1 = new fr.esrf.tangoatk.widget.attribute.EnumScalarComboEditor();
        jPanel4 = new javax.swing.JPanel();
        stateViewerBunchClock = new fr.esrf.tangoatk.widget.attribute.StateViewer();
        statusViewerSequencer = new fr.esrf.tangoatk.widget.attribute.StatusViewer();
        stateViewerOutput = new fr.esrf.tangoatk.widget.attribute.StateViewer();
        stateViewerPPS = new fr.esrf.tangoatk.widget.attribute.StateViewer();
        stateViewerT0_Source = new fr.esrf.tangoatk.widget.attribute.StateViewer();
        jSplitPane1 = new javax.swing.JSplitPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        scalarListViewerBunchClock = new fr.esrf.tangoatk.widget.attribute.ScalarListViewer();
        jPanelWidth = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        scalarListViewerWidth = new fr.esrf.tangoatk.widget.attribute.ScalarListViewer();
        jPanelPhase = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        scalarListViewerPhase = new fr.esrf.tangoatk.widget.attribute.ScalarListSetter();
        jPanel1 = new javax.swing.JPanel();
        enumScalarFillingPattern = new fr.esrf.tangoatk.widget.attribute.EnumScalarSetPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        numberSpectrumListViewer1 = new jtiming.NumberSpectrumListViewer();
        jScrollPane4 = new javax.swing.JScrollPane();
        numberSpectrumListViewer2 = new jtiming.NumberSpectrumListViewer();
        booleanScalarSetPanelRotatingPulse = new fr.esrf.tangoatk.widget.attribute.BooleanScalarSetPanel();
        jPanel3 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenuView = new javax.swing.JMenu();
        jMenuItemPreview = new javax.swing.JMenuItem();
        jMenuItemLoad = new javax.swing.JMenuItem();
        jMenuItemSave = new javax.swing.JMenuItem();
        jMenuItemPause = new javax.swing.JMenuItem();
        jMenuItemUnPause = new javax.swing.JMenuItem();
        jMenuItemQuit = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItemResetInjError = new javax.swing.JMenuItem();
        jMenuItemReset = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItemShowMasterResetHisto = new javax.swing.JMenuItem();
        errorHistMenuItem = new javax.swing.JMenuItem();
        diagMenuItem = new javax.swing.JMenuItem();
        helpMenu = new javax.swing.JMenu();
        jMenuItemCrossConfig = new javax.swing.JMenuItem();
        jMenuItemChangeLog = new javax.swing.JMenuItem();
        jMenuItemTroubleshooting = new javax.swing.JMenuItem();
        aboutMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(802, 578));

        stateViewerBunchClock.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stateViewerBunchClockMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(stateViewerBunchClock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(stateViewerT0_Source, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stateViewerPPS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(stateViewerOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(statusViewerSequencer, javax.swing.GroupLayout.DEFAULT_SIZE, 766, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(stateViewerBunchClock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stateViewerOutput, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stateViewerPPS, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(stateViewerT0_Source, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(statusViewerSequencer, javax.swing.GroupLayout.DEFAULT_SIZE, 126, Short.MAX_VALUE))
        );

        jSplitPane1.setDividerLocation(375);
        jSplitPane1.setFocusCycleRoot(true);
        jSplitPane1.setMinimumSize(new java.awt.Dimension(250, 300));
        jSplitPane1.setPreferredSize(new java.awt.Dimension(250, 300));

        jTabbedPane1.setDoubleBuffered(true);
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(100, 100));

        jScrollPane2.setBorder(null);
        jScrollPane2.setViewportView(scalarListViewerBunchClock);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );

        jTabbedPane1.addTab("Delay", jPanel2);

        jPanelWidth.setMinimumSize(new java.awt.Dimension(100, 100));
        jPanelWidth.setPreferredSize(new java.awt.Dimension(101, 101));

        jScrollPane5.setViewportView(scalarListViewerWidth);

        javax.swing.GroupLayout jPanelWidthLayout = new javax.swing.GroupLayout(jPanelWidth);
        jPanelWidth.setLayout(jPanelWidthLayout);
        jPanelWidthLayout.setHorizontalGroup(
            jPanelWidthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
        );
        jPanelWidthLayout.setVerticalGroup(
            jPanelWidthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Width", jPanelWidth);

        jPanelPhase.setAutoscrolls(true);
        jPanelPhase.setMinimumSize(new java.awt.Dimension(100, 100));
        jPanelPhase.setPreferredSize(new java.awt.Dimension(101, 101));

        scalarListViewerPhase.setViewerVisible(false);
        jScrollPane6.setViewportView(scalarListViewerPhase);

        javax.swing.GroupLayout jPanelPhaseLayout = new javax.swing.GroupLayout(jPanelPhase);
        jPanelPhase.setLayout(jPanelPhaseLayout);
        jPanelPhaseLayout.setHorizontalGroup(
            jPanelPhaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 369, Short.MAX_VALUE)
        );
        jPanelPhaseLayout.setVerticalGroup(
            jPanelPhaseLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Phase", jPanelPhase);

        jSplitPane1.setLeftComponent(jTabbedPane1);
        jTabbedPane1.getAccessibleContext().setAccessibleName("Delay");

        jPanel1.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new java.awt.Insets(1, 0, 1, 0);
        jPanel1.add(enumScalarFillingPattern, gridBagConstraints);

        jScrollPane3.setViewportView(numberSpectrumListViewer1);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.1;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 4, 4);
        jPanel1.add(jScrollPane3, gridBagConstraints);

        numberSpectrumListViewer2.setColumns(20);
        numberSpectrumListViewer2.setRows(5);
        jScrollPane4.setViewportView(numberSpectrumListViewer2);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 0, 4);
        jPanel1.add(jScrollPane4, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        jPanel1.add(booleanScalarSetPanelRotatingPulse, gridBagConstraints);

        jSplitPane1.setRightComponent(jPanel1);

        jPanel3.setLayout(new java.awt.BorderLayout());

        fileMenuView.setText("File");

        jMenuItemPreview.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_V, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItemPreview.setText("Preview ...");
        jMenuItemPreview.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPreviewActionPerformed(evt);
            }
        });
        fileMenuView.add(jMenuItemPreview);

        jMenuItemLoad.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItemLoad.setText("Load ...");
        jMenuItemLoad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemLoadActionPerformed(evt);
            }
        });
        fileMenuView.add(jMenuItemLoad);

        jMenuItemSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_DOWN_MASK));
        jMenuItemSave.setText("Save ...");
        jMenuItemSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemSaveActionPerformed(evt);
            }
        });
        fileMenuView.add(jMenuItemSave);

        jMenuItemPause.setText("Pause ...");
        jMenuItemPause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPauseActionPerformed(evt);
            }
        });
        fileMenuView.add(jMenuItemPause);

        jMenuItemUnPause.setText("Unpause");
        jMenuItemUnPause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemUnPauseActionPerformed(evt);
            }
        });
        fileMenuView.add(jMenuItemUnPause);

        jMenuItemQuit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_DOWN_MASK));
        jMenuItemQuit.setText("Quit");
        jMenuItemQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemQuitActionPerformed(evt);
            }
        });
        fileMenuView.add(jMenuItemQuit);

        jMenuBar1.add(fileMenuView);

        jMenu1.setText("Reset");

        jMenuItem2.setText("Reset watchdog T0");
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuItemResetInjError.setText("Reset Inj Error");
        jMenuItemResetInjError.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemResetInjErrorActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemResetInjError);

        jMenuItemReset.setText("Master Reset ...");
        jMenuItemReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemResetActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemReset);

        jMenuBar1.add(jMenu1);

        viewMenu.setText("View");

        jMenuItem1.setText("Depending Delays view");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        viewMenu.add(jMenuItem1);

        jMenuItemShowMasterResetHisto.setText("Master Reset History");
        jMenuItemShowMasterResetHisto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemShowMasterResetHistoActionPerformed(evt);
            }
        });
        viewMenu.add(jMenuItemShowMasterResetHisto);

        errorHistMenuItem.setText("Error History");
        errorHistMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                errorHistMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(errorHistMenuItem);

        diagMenuItem.setText("Diagnostic ...");
        diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diagMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(diagMenuItem);

        jMenuBar1.add(viewMenu);

        helpMenu.setText("Help");

        jMenuItemCrossConfig.setText("Synoptic Cross Settings");
        jMenuItemCrossConfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemCrossConfigActionPerformed(evt);
            }
        });
        helpMenu.add(jMenuItemCrossConfig);

        jMenuItemChangeLog.setText("changelog");
        jMenuItemChangeLog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemChangeLogActionPerformed(evt);
            }
        });
        helpMenu.add(jMenuItemChangeLog);

        jMenuItemTroubleshooting.setText("Confluence troubleshooting");
        jMenuItemTroubleshooting.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemTroubleshootingActionPerformed(evt);
            }
        });
        helpMenu.add(jMenuItemTroubleshooting);

        aboutMenuItem.setText("About");
        aboutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                aboutMenuItemActionPerformed(evt);
            }
        });
        helpMenu.add(aboutMenuItem);

        jMenuBar1.add(helpMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSplitPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItemLoadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemLoadActionPerformed
        smProxy.loadSettingsFile();
    }//GEN-LAST:event_jMenuItemLoadActionPerformed

    private void jMenuItemSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemSaveActionPerformed
        smProxy.saveSettingsFile();
    }//GEN-LAST:event_jMenuItemSaveActionPerformed

    private void jMenuItemQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemQuitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItemQuitActionPerformed

    private void errorHistMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorHistMenuItemActionPerformed
        errh.setVisible(true);
    }//GEN-LAST:event_errorHistMenuItemActionPerformed

    private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
        fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
    }//GEN-LAST:event_diagMenuItemActionPerformed

    private void aboutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_aboutMenuItemActionPerformed
        JOptionPane.showMessageDialog(this,
            "Tango Control for jTiming Version" + this.versNumber + "\n"
            + "Powered by Java version " + System.getProperty("java.version"),
            "About jTiming",
            JOptionPane.INFORMATION_MESSAGE,WhistConstants.jawr);
    }//GEN-LAST:event_aboutMenuItemActionPerformed

    private void jMenuItemPreviewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPreviewActionPerformed
        smProxy.previewSettingsFile();
    }//GEN-LAST:event_jMenuItemPreviewActionPerformed
       
    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        dependingViewlauncher();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItemCrossConfigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemCrossConfigActionPerformed
        if(crossSyno == null){
            crossSyno = new SimpleSynopticAppli( "/operation/rf/jdraw_files/Timing/Main_timing.jdw" , false);
            crossSyno.pack();
        }
        if(!crossSyno.isVisible()){
            crossSyno.setVisible(true);
        }
    }//GEN-LAST:event_jMenuItemCrossConfigActionPerformed

    private void stateViewerBunchClockMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stateViewerBunchClockMouseClicked
        if(atkBunchClock != null && !atkBunchClock.isVisible()) {
            atkBunchClock.dispose();
            atkBunchClock = null;
        }
        if(atkBunchClock == null) {
            atkBunchClock = new atkpanel.MainPanel(WhistConstants.DEV_BUNCHCLOCK, false);
        }
    }//GEN-LAST:event_stateViewerBunchClockMouseClicked

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        resetWatchDog.execute();
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenuItemResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemResetActionPerformed
        launcheReset();
    }//GEN-LAST:event_jMenuItemResetActionPerformed

    private void jMenuItemPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPauseActionPerformed
        int userAnswer = JOptionPane.NO_OPTION;
	
	try{
	    userAnswer = JOptionPane.showConfirmDialog(this, "Are you sure you want to pause","Pause BunchClock ?" ,JOptionPane.YES_NO_OPTION);
	} catch (HeadlessException hex) {}
	
	if (userAnswer == JOptionPane.YES_OPTION){
            offCmd.execute();
        }
        
    }//GEN-LAST:event_jMenuItemPauseActionPerformed

    private void jMenuItemUnPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemUnPauseActionPerformed
        onCmd.execute();
    }//GEN-LAST:event_jMenuItemUnPauseActionPerformed

    private void jMenuItemResetInjErrorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemResetInjErrorActionPerformed
        resetInjError.execute();
    }//GEN-LAST:event_jMenuItemResetInjErrorActionPerformed

    private void jMenuItemShowMasterResetHistoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemShowMasterResetHistoActionPerformed
        showStatusResetJFrame();
    }//GEN-LAST:event_jMenuItemShowMasterResetHistoActionPerformed

    private void jMenuItemChangeLogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemChangeLogActionPerformed
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(new URI("https://gitlab.esrf.fr/accelerators/Timing/whist/-/blob/master/sw/tango/client/JTiming/CHANGELOG.md"));
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(rootPane, ex.getMessage());
            }
        }
    }//GEN-LAST:event_jMenuItemChangeLogActionPerformed

    private void jMenuItemTroubleshootingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemTroubleshootingActionPerformed
        if (Desktop.isDesktopSupported() && Desktop.getDesktop().isSupported(Desktop.Action.BROWSE)) {
            try {
                Desktop.getDesktop().browse(new URI("https://confluence.esrf.fr/display/ACUWK/Timing"));
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(rootPane, ex.getMessage());
            }
        }
    }//GEN-LAST:event_jMenuItemTroubleshootingActionPerformed

    static boolean expert = false;
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        for(int i = 0; i < args.length; i++){
            if(args[i].equals("-expert")){
                expert = true;
            }
        }
                /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainFrame mf = new MainFrame();
                mf.expertViewAtkpanel = expert;
                mf.setLocationRelativeTo(null);
                mf.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem aboutMenuItem;
    private fr.esrf.tangoatk.widget.attribute.BooleanScalarSetPanel booleanScalarSetPanelRotatingPulse;
    private javax.swing.JMenuItem diagMenuItem;
    private fr.esrf.tangoatk.widget.attribute.EnumScalarComboEditor enumScalarComboEditor1;
    private fr.esrf.tangoatk.widget.attribute.EnumScalarSetPanel enumScalarFillingPattern;
    private fr.esrf.tangoatk.widget.attribute.EnumScalarSetPanel enumScalarSetPanel2;
    private javax.swing.JMenuItem errorHistMenuItem;
    private javax.swing.JMenu fileMenuView;
    private javax.swing.JMenu helpMenu;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItemChangeLog;
    private javax.swing.JMenuItem jMenuItemCrossConfig;
    private javax.swing.JMenuItem jMenuItemLoad;
    private javax.swing.JMenuItem jMenuItemPause;
    private javax.swing.JMenuItem jMenuItemPreview;
    private javax.swing.JMenuItem jMenuItemQuit;
    private javax.swing.JMenuItem jMenuItemReset;
    private javax.swing.JMenuItem jMenuItemResetInjError;
    private javax.swing.JMenuItem jMenuItemSave;
    private javax.swing.JMenuItem jMenuItemShowMasterResetHisto;
    private javax.swing.JMenuItem jMenuItemTroubleshooting;
    private javax.swing.JMenuItem jMenuItemUnPause;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanelPhase;
    private javax.swing.JPanel jPanelWidth;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea numberSpectrumListViewer1;
    private javax.swing.JTextArea numberSpectrumListViewer2;
    private fr.esrf.tangoatk.widget.attribute.ScalarListViewer scalarListViewerBunchClock;
    private fr.esrf.tangoatk.widget.attribute.ScalarListSetter scalarListViewerPhase;
    private fr.esrf.tangoatk.widget.attribute.ScalarListViewer scalarListViewerWidth;
    private fr.esrf.tangoatk.widget.attribute.StateViewer stateViewerBunchClock;
    private fr.esrf.tangoatk.widget.attribute.StateViewer stateViewerOutput;
    private fr.esrf.tangoatk.widget.attribute.StateViewer stateViewerPPS;
    private fr.esrf.tangoatk.widget.attribute.StateViewer stateViewerT0_Source;
    private fr.esrf.tangoatk.widget.attribute.StatusViewer statusViewerSequencer;
    private javax.swing.JMenu viewMenu;
    // End of variables declaration//GEN-END:variables

    private void showMessage(DevFailed ex, JFrame aThis, String failed_to_read_file) {
        StringWriter errors = new StringWriter();
        ex.printStackTrace(new PrintWriter(errors));

        JOptionPane.showMessageDialog(aThis,
                ex.getMessage() + "\n" + errors.toString(),
                failed_to_read_file,
                JOptionPane.ERROR_MESSAGE);
    }
    
    private void addSettingsManagerSupport()
    {
        //try{
            smProxy = new SettingsManagerProxy(WhistConstants.TIMING_SETTING_MANAGER);
            String username = System.getProperty("user.name");
            String author = "JTiming ("+ username + ")";
            smProxy.setFileAuthor(author);
            smProxy.setErrorHistoryWindow(errh);
            smPanel = smProxy.getSettingsPanel();
            smProxy.settingsPanelHideChild(SettingsManagerProxy.FILE_LABEL);
            //smProxy.settingsPanelHideChild(SettingsManagerProxy.PREVIEW_BUTTON);
            //smProxy.settingsPanelHideChild(SettingsManagerProxy.SAVE_BUTTON);
            //smProxy.settingsPanelHideChild(SettingsManagerProxy.LOAD_BUTTON);
            jPanel3.add(smPanel,BorderLayout.CENTER);
            

        /*}catch (DevFailed ex){
            ErrorPane.showErrorMessage(this, TIMING_SETTING_MANAGER, ex);
            
        }*/
    }

    private void launcheReset() {
        int userAnswer = JOptionPane.NO_OPTION;
        
	try{
            // char &#9 is used to move the link to the left.
            userAnswer = JOptionPane.showConfirmDialog(this, new MessageWithLink(
                    "Running this command will change bunch #0, some actions will be required after that."
                + "<br><br>"
                + "<center>Check <a href=\"https://confluence.esrf.fr/x/IJ5PAQ\">this page</a> for more details&#9&#9&#9&#9</center><br>"
                + "Do you want to proceed?"),
                                    "Reset Sequencer", JOptionPane.YES_NO_OPTION , JOptionPane.WARNING_MESSAGE);
	} catch (HeadlessException hex) {}
	
	if (userAnswer == JOptionPane.YES_OPTION){
            ThreadDlg t = new ThreadDlg(this, "Reset in Progress ...", false, new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        //force timeout
                        ((Device)resetCmd.getDevice()).setDevTimeout(10000);
                    }catch(ConnectionException ex){
                        ErrorPane.showErrorMessage(rootPane, resetCmd.getName(), ex);
                    }
                    resetCmd.execute();
                }
            }));
            t.showDlg();
	    
            showStatusResetJFrame();
        }
    }

    private void showStatusResetJFrame() {
        if (statusResetJFrame == null) {
            GridBagConstraints gbc = new GridBagConstraints();
            statusResetJFrame = new JFrame();
            statusResetJFrame.setTitle("Master Reset Status");
            statusResetJFrame.setIconImage(WhistConstants.wrLogo);
            statusResetJFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            statusResetJFrame.getContentPane().setLayout(new GridBagLayout());
            statusv.setBorder(new javax.swing.border.TitledBorder(WhistConstants.DEV_BUNCHCLOCK + "/StatusThread"));
            gbc = new GridBagConstraints();
            gbc.fill = GridBagConstraints.BOTH;
            gbc.insets = new Insets(3, 3, 3, 3);
            gbc.gridx = 0;
            gbc.gridy = 0;
            gbc.weightx = 1.0;
            gbc.weighty = 1.0;
            statusResetJFrame.getContentPane().add(statusv, gbc);
            gbc.anchor = GridBagConstraints.EAST;
            gbc.fill = GridBagConstraints.NONE;
            gbc.insets = new Insets(0, 0, 13, 15);
            gbc.gridy = 1;
            gbc.weighty = 0.0;
            JButton buttonEnd = new JButton("Close");
            buttonEnd.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    statusResetJFrame.setVisible(false);
                }
            });
            statusResetJFrame.getContentPane().add(buttonEnd, gbc);
            statusResetJFrame.setPreferredSize(new Dimension(700, 400));
            statusResetJFrame.pack();
            statusResetJFrame.setLocationRelativeTo(this);
            statusResetJFrame.setVisible(true);

        } else {
            statusResetJFrame.setVisible(true);
        }

    }
    
    private void dependingViewlauncher() {       
        if( dependingDelayView == null){
            try {
                dependingDelayView = new TreeDelayFrame();
                ATKGraphicsUtils.centerFrame(jSplitPane1, dependingDelayView);
            } catch (DevFailed ex) {
                ErrorPane.showErrorMessage(rootPane, WhistConstants.DEV_NETWORK, ex);
            }
        }
        if(dependingDelayView != null && !dependingDelayView.isVisible()){
            dependingDelayView.setVisible(true);
        }
    }

    MainPanel atkPanelAlreadyStarted(String device) {
        MainPanel [] panels = new MainPanel[atkPanelList.size()];
        for(MainPanel p : atkPanelList.toArray(panels)){
            if(p.getName().equalsIgnoreCase(device)){
                return p;
            }
        }
        return null;
    }

}
