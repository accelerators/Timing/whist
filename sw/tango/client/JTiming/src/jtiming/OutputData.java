/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.WrongNameSyntax;
import fr.esrf.TangoDs.Except;
import java.util.Objects;

/**
 *
 * @author tappret
 */
public class OutputData {

    OutputData(String deviceName) {
        this.name = deviceName;
    }

    /**
     * Create a object OutputData
     * @param if deviceName and readDataFromDatabase is given the contrutor
     * read Configuration from Tango database.
     * 
     * **/
    public OutputData(String deviceName,boolean readDataFromDatabase) throws DevFailed {
        this(deviceName);
        if(readDataFromDatabase){
            Database db = ApiUtil.get_db_obj();
            DbDatum[] data = db.get_device_property(deviceName, db.get_device_property_list(deviceName, "*"));
        
            String mainPropName = "ParentAttribute";
            String className = db.get_class_for_device(deviceName);
            className = className.replace("WhistOutput", "");
            if(className.equals("Pulse")){
                this.kind = OutputData.Kind.Pulse;
                mainPropName = "OutputSignalSource";
            }else if(className.equals("Device")){
                this.kind = OutputData.Kind.Device;
                mainPropName = "ParentAttribute";
            }else if(className.equals("Clock")){
                this.kind = OutputData.Kind.Clock;
                mainPropName = "OutputClock";
            }else if(className.equals("BunchClock")){
                this.kind = OutputData.Kind.BunchClock;
                mainPropName = "OutputBunchClockSignal";
            }else if(className.equals("Event")){
                this.kind = OutputData.Kind.Event;
                mainPropName = "InputParentDevice";
            }

            String mainPropValue = null;
            for(int i = 0 ; i < data.length ; i++ ){
                String value = data[i].extractString();
                System.out.println(data[i].name + ":" + value);
                if(data[i].name.equalsIgnoreCase("FastTiming")){
                    this.isFast = value.equalsIgnoreCase(Boolean.TRUE.toString());
                }
                if(data[i].name.equalsIgnoreCase("OutputNumber")){
                    this.number = data[i].extractLong();
                }
                if(data[i].name.equalsIgnoreCase(mainPropName)){
                    mainPropValue = value;
                }
            }

            if(className.equals("Pulse")){
                this.signal = OutputData.matchPulseSignal(mainPropValue);
            }else if(className.equals("Device")){
                //2 case pulse fro bunchClock (ex: Tti) or other device
                if(mainPropValue.endsWith("/delayFromT0")){
                    mainPropValue = mainPropValue.substring(0,mainPropValue.length() - 12);
                }
                
                //Special Case
                mainPropValue = mainPropValue.toLowerCase();
                if(mainPropValue.startsWith(WhistConstants.DEV_BUNCHCLOCK)){
                    kind = kind.Pulse;
                    if(mainPropValue.endsWith("/tti")){
                        this.signal = PulseSignal.Tti;
                    }else if(mainPropValue.endsWith("/tte")){
                        this.signal = PulseSignal.Tte;
                    }
                }else{
                    this.parentDevice = mainPropValue;
                }
            }else if(className.equals("Clock")){
                this.clock = OutputData.matchClock(mainPropValue);
            }else if(className.equals("BunchClock")){
                if(OutputData.isGun(mainPropValue)){
                    this.kind = Kind.GUN;
                }else{
                    this.bcSignal = OutputData.matchBunchClockPulseSignal(mainPropValue);
                }
            }else if(className.equals("Event")){
                this.parentDevice = mainPropValue.toLowerCase();
            }
        }
    }

    public enum Kind {
        Pulse,
        Clock,
        Device,
        Event,
        GUN,
        BunchClock
    }
    
    public enum PulseSignal {
        T0,
        inj,
        Tti,
        ki,
        ext,
        Tte
    }
    
    public enum BunchClockPulseSignal{
        T0,
        inj,
        ext,
        gun_rf8
    }
    
    public enum Clock {
        SRTC,
        BOOTC,
        CLK16b,
        CLK4b,
        TC32,
        SR_Count
    }
    
    Kind kind = null;
    String name = null;
    int number = -1;
    Boolean isFast = null;
    PulseSignal signal = null;
    BunchClockPulseSignal bcSignal = null;
    Clock clock = null;
    String parentDevice = null;
    boolean creation = false;
       
    static PulseSignal matchPulseSignal(String str) throws WrongNameSyntax{
        for (PulseSignal value : PulseSignal.values()) {
            if(value.toString().equals(str)){
                return value;
            }
        }
        Except.throw_wrong_syntax_exception("Need value like T0, Tinj, Tti, Text, Tte. Please Check property", "Parsing Error" , "OutputData.matchPulseSignal()");
        return null;
    }
    
    static boolean isGun(String str){
        if(str.equals("gun")){
            return true;
        }
        return false;
    }
    
    static BunchClockPulseSignal matchBunchClockPulseSignal(String str) throws WrongNameSyntax{
        for (BunchClockPulseSignal value : BunchClockPulseSignal.values()) {
            if(value.toString().equals(str)){
                return value;
            }
        }
        Except.throw_wrong_syntax_exception("Need value like T0, inj, ki , ext. Please Check property", "Parsing Error" , "OutputData.matchBunchClockPulseSignal()");
        return null;
    }
    
    static Clock matchClock(String str) throws WrongNameSyntax {
        if(str.equalsIgnoreCase("tc32")){
            return Clock.TC32;
        }else if(str.equalsIgnoreCase("srtc")){
            return Clock.SRTC;
        }else if(str.equalsIgnoreCase("bootc")){
            return Clock.BOOTC;
        }else if(str.equalsIgnoreCase("sr_count")){
            return Clock.SR_Count;
        }else if(str.equalsIgnoreCase("clk16b")){
            return Clock.CLK16b;
        }else if(str.equalsIgnoreCase("clk4b")){
            return Clock.CLK4b;
        }
         
        Except.throw_wrong_syntax_exception("Need value like TC32, SRTC, BOOTC, SR_Count, CLK16b or CLK4b  Please Check property", "Parsing Error" , "OutputData.matchClock()");
        return null;
    }

    @Override
    public boolean equals(Object obj) {
        OutputData od;
        if(!(obj instanceof OutputData)){
            return false;
        }
        od=(OutputData)obj;
            
        if(this.kind != od.kind || this.number != od.number
                || !Objects.equals(this.isFast, od.isFast)
                || !(this.name.equals(od.name))
                || !Objects.equals(this.parentDevice, od.parentDevice)
                || !Objects.equals(this.clock, od.clock)
                || !Objects.equals(this.bcSignal, od.bcSignal)){
            return false;
        }
        return true;
    }   
}
