/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.WrongNameSyntax;
import fr.esrf.TangoDs.Except;
import java.util.Objects;

/**
 *
 * @author tappret
 */
public class InputData {

    InputData(String deviceName) {
        this.name = deviceName;
    }

    public InputData(String deviceName,boolean readDataFromDatabase) throws DevFailed {
        this(deviceName);
        if(readDataFromDatabase){
            Database db = ApiUtil.get_db_obj();
            DbDatum[] data = db.get_device_property(deviceName, db.get_device_property_list(deviceName, "*"));
        
            for(int i = 0 ; i < data.length ; i++ ){
                if(data[i].name.equalsIgnoreCase("InputNumber")){
                    this.number = data[i].extractLong();
                }
                if(data[i].name.equalsIgnoreCase("inputMode")){
                    String mode = data[i].extractString();
                    if(mode.compareToIgnoreCase("stamp") == 0){
                        this.kind = InputKind.Stamping;
                    }else if(mode.compareToIgnoreCase("event") == 0){
                        this.kind = InputKind.Event;
                    }
                    
                }
            }
        }
    }

    public enum InputKind {
        Stamping,
        Event,
    }
    
    InputKind kind = null;
    String name = null;
    int number = -1;
    boolean creation = false;

    @Override
    public boolean equals(Object obj) {
        InputData id;
        if(!(obj instanceof InputData)){
            return false;
        }
        id=(InputData)obj;
            
        if(this.kind != id.kind || this.number != id.number
                || !(this.name.equals(id.name))){
            return false;
        }
        return true;
    }   
}
