/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.Tango.DevState;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.BooleanSpectrumEvent;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DevStateScalarEvent;
import fr.esrf.tangoatk.core.DevStateSpectrumEvent;
import fr.esrf.tangoatk.core.Device;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.IBooleanSpectrum;
import fr.esrf.tangoatk.core.IBooleanSpectrumListener;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IDevStateScalarListener;
import fr.esrf.tangoatk.core.IDevStateSpectrum;
import fr.esrf.tangoatk.core.IDevStateSpectrumListener;
import fr.esrf.tangoatk.core.IStringSpectrum;
import fr.esrf.tangoatk.core.IStringSpectrumListener;
import fr.esrf.tangoatk.core.StringSpectrumEvent;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeSelectionModel;

/**
 *
 * @author tappret
 */
public class MyJTreeWhistOutput extends JTree implements IStringSpectrumListener,IDevStateSpectrumListener,IDevStateScalarListener,IBooleanSpectrumListener, KeyListener{
    IDevStateScalar bunchClockAttributeState;
    JTextField researchTf;
    Delay master = new Delay("masterBuncheClock",DevState.UNKNOWN,false);
    DevState bunchClockState;
    boolean hadAnError = false;
    
    DevState [] outputsStateValues = null;
    String [] outputsNameValues = null;
    String [] outputsParentNameValues = null;
    boolean [] outputsFastValues = null;
    
    DevState [] inputsStateValues = null;
    String [] inputsNameValues = null;
    String [] inputsModeValues = null;
    
    String textToFind = null;
    boolean onlyInput = false;
    
    IStringSpectrum outputsName;
    IStringSpectrum outputsParentName;
    IDevStateSpectrum outputsState;
    IBooleanSpectrum outputsFast;
    
    IStringSpectrum inputsName;
    IStringSpectrum inputsMode;
    IDevStateSpectrum inputsState;
    
    String [] masterFiltring = {};
    String [] bunchClockSignalFiltring = WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK;
    
    boolean firstPass = true;
    
    public MyJTreeWhistOutput() {    
        AttributeList al = new AttributeList();
        try {
            bunchClockAttributeState = (IDevStateScalar)al.add(WhistConstants.DEV_BUNCHCLOCK + "/state");
            
            bunchClockAttributeState.addDevStateScalarListener(this);
            
            al.startRefresher();
            
            bunchClockState = DevState.UNKNOWN;
            //set master BunchClock Signal
            master = makeRootModel();
            bunchClockAttributeState.refresh();
            
        } catch (ConnectionException ex) {
            Logger.getLogger(MyJTreeWhistOutput.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        this.setModel(new DefaultTreeModel(master));
        
        this.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        
        this.setEditable(false);
        
        this.setCellRenderer(new DefaultTreeCellRenderer() {
            @Override
            public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
                super.getTreeCellRendererComponent( tree, value, selected, expanded, leaf, row, hasFocus);

                setIcon(ATKConstant.getSmallBallIcon4State(((Delay)value).state.toString()));

                return this;
            }
        });
    }
    
    public void setMasterFiltering(String [] filtringList){
        masterFiltring = filtringList;
        
        ArrayList<String> signalList = new ArrayList<>(Arrays.asList(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK));
        
        for (String filter : filtringList) {
            if (isSignalIsContante(signalList,filter)) {
                if (filter.compareToIgnoreCase("Clock") == 0) {
                    int indexClock = findIndexOf(signalList,"Clock");
                    
                    //remove Clock sr sy 16 bunches 4 bunches etc...
                    for(int j = 0 ; j < 7 ; j++ ){
                        signalList.remove(indexClock);
                    }
                } else {
                    signalList.remove(filter);
                }
            }
        }
        
        bunchClockSignalFiltring = new String[signalList.size()];
        bunchClockSignalFiltring = signalList.toArray(bunchClockSignalFiltring);
        
        refreshModel();
    }
    
    private int findIndexOf(ArrayList<String> array, String toFind ){
        for(int i = 0; i < array.size();i++){
            if(array.get(i).compareToIgnoreCase(toFind) == 0){
                return i;
            }
        }
        return -1;
    }
    
    private boolean isSignalIsContante(ArrayList<String> list,String str){
        for(int i = 0 ; i < list.size();i++){
            if (list.get(i).compareToIgnoreCase(str) == 0) {
                return true;
            }
        }
        return false;
    }

    public void setModel(JTextField researchTf,IStringSpectrum outputsName
            ,IStringSpectrum outputsParentName,IDevStateSpectrum outputsState
            ,IBooleanSpectrum outputsFast,IStringSpectrum inputsName
            ,IStringSpectrum inputsMode,IDevStateSpectrum inputsState) {
        clearModel();
        //bunchClockAttributeState
        this.researchTf = researchTf;
        textToFind = researchTf.getText();
        researchTf.addKeyListener(this);
        
        this.outputsName = outputsName;
        this.outputsParentName = outputsParentName;
        this.outputsState = outputsState;
        this.outputsFast = outputsFast;
        
        this.inputsName = inputsName;
        this.inputsMode = inputsMode;
        this.inputsState = inputsState;
        
        outputsName.addListener(this);
        outputsParentName.addListener(this);
        outputsState.addDevStateSpectrumListener(this);
        outputsFast.addBooleanSpectrumListener(this);
        
        inputsName.addListener(this);
        inputsMode.addListener(this);
        inputsState.addDevStateSpectrumListener(this);
                
        outputsName.refresh();
        outputsParentName.refresh();
        outputsState.refresh();
        outputsFast.refresh();
        
        inputsName.refresh();
        inputsMode.refresh();
        inputsState.refresh();
    }
    
    public void clearModel() {
        if(researchTf != null){
            researchTf.removeKeyListener(this);
            researchTf = null;
            textToFind = null;
        }
        if (outputsName != null) {
            outputsName.removeListener(this);
            outputsName = null;
            outputsNameValues = null;
        }
        if (outputsParentName != null) {
            outputsParentName.removeListener(this);
            outputsParentName = null;
            outputsParentNameValues = null;
        }
        if (outputsState != null) {
            outputsState.removeDevStateSpectrumListener(this);
            outputsState = null;
            outputsStateValues = null;
        }
        if (outputsFast != null) {
            outputsFast.removeBooleanSpectrumListener(this);
            outputsFast = null;
            outputsFastValues = null;
        }
        if (inputsName != null) {
            inputsName.removeListener(this);
            inputsName = null;
            inputsNameValues = null;
        }
        if (inputsMode != null) {
            inputsMode.removeListener(this);
            inputsMode = null;
            inputsModeValues = null;
        }
        if (inputsState != null) {
            inputsState.removeDevStateSpectrumListener(this);
            inputsState = null;
            inputsStateValues = null;
        }
        this.setModel(new DefaultTreeModel(master));
        setToolTipText(null);
    }

    private Delay makeRootModel() {
        Delay root = new Delay("masterBuncheClock",DevState.UNKNOWN,false);
        root.add(new Delay(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[0],bunchClockState,false));
        for(int i = 1 ; i < WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK.length ; i++ ){
            Delay d = new Delay(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[i],bunchClockState,true);
            if(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[i].equals("Tinj")){
                d.add(new Delay(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[i+1],bunchClockState, false));
                i++;
                d.add(new Delay(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[i+1],bunchClockState, true));
                i++;
            }
            if(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[i].equals("Text")){
                d.add(new Delay(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[i+1],bunchClockState, false));
                i++;
            }
            if(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[i].equals("Clock")){
                for(int j = 1 ; j < 7 ; j++){
                    d.add(new Delay(WhistConstants.BUNCHCLOCK_SIGNAL_AND_CLOCK[i+j],bunchClockState,false));
                }
                i=i+6;
            }
            root.add(d);
        }
        
        for(int i = 0 ; i < masterFiltring.length ; i++){
            root.filtring(masterFiltring[i]);
        }
        
        return root;
    }

    @Override
    public void stringSpectrumChange(StringSpectrumEvent sse) {
        //System.out.println(sse.getSource());
        if(sse.getSource() == outputsName){
            outputsNameValues = sse.getValue();
        }
        if(sse.getSource() == outputsParentName){
           outputsParentNameValues = sse.getValue();
        }
        if(sse.getSource() == inputsName){
           inputsNameValues = sse.getValue();
        }
        if(sse.getSource() == inputsMode){
           inputsModeValues = sse.getValue();
        }
        
        refreshModel();
    }

    @Override
    public void stateChange(AttributeStateEvent ase) {
        if(hadAnError){
            hadAnError = false;
            refreshModel();
        }
    }

    @Override
    public void errorChange(ErrorEvent ee) {
        Delay rootTmp = new Delay("root",DevState.UNKNOWN,false);
        rootTmp.add(new Delay("Error: \"" + WhistConstants.DEV_NETWORK +"\" is Unreachable",DevState.UNKNOWN,false));
        this.setModel(new DefaultTreeModel(rootTmp));
        hadAnError = true;
    }

    @Override
    public void devStateSpectrumChange(DevStateSpectrumEvent dsse) {
        if(dsse.getSource() == outputsState){
            String [] stateString = dsse.getValue();
            outputsStateValues = new DevState[stateString.length];
            for(int i = 0 ; i < stateString.length ; i++){
                outputsStateValues[i] = Device.getStateFromString(stateString[i]);
            }
        }
        
        if(dsse.getSource() == inputsState){
            String [] stateString = dsse.getValue();
            inputsStateValues = new DevState[stateString.length];
            for(int i = 0 ; i < stateString.length ; i++){
                inputsStateValues[i] = Device.getStateFromString(stateString[i]);
            }
        }
        refreshModel();
    }

    @Override
    public void devStateScalarChange(DevStateScalarEvent dsse) {
        if(dsse.getSource() == bunchClockAttributeState){
            bunchClockState = Device.getStateFromString(dsse.getValue());
            
            for (String BUNCHCLOCK_SIGNAL1 : bunchClockSignalFiltring) {
                Delay matched = master.findDelay(BUNCHCLOCK_SIGNAL1);
                if(matched != null){
                    matched.state = bunchClockState;
                }
            }
            this.repaint();
        }
    }

    private void refreshModel() {
        try{
            if(outputsNameValues != null && outputsParentNameValues != null
                    && outputsStateValues != null && outputsFastValues != null
                    && inputsNameValues != null && inputsModeValues != null
                    && inputsStateValues != null){
                master = makeRootModel();
                //long start = System.currentTimeMillis();
                ArrayList<Integer> restOutputIndex = new ArrayList<>();

                if(outputsParentNameValues.length != outputsNameValues.length
                        || outputsNameValues.length != outputsStateValues.length
                        || outputsStateValues.length != outputsFastValues.length
                        || inputsNameValues.length != inputsModeValues.length
                        || inputsModeValues.length != inputsStateValues.length){
                    //ignore whene the size change and the change have not propaged
                    return;
                }

                if(!onlyInput){
                    for(int i = 0 ; i < outputsNameValues.length ; i++){
                        findAndAdd(outputsParentNameValues[i],outputsNameValues[i],outputsStateValues[i],outputsFastValues[i],i,restOutputIndex);
                    }
                }
                
                for(int i = 0 ; i < inputsNameValues.length ; i++){
                    findAndAdd(inputsModeValues[i],inputsNameValues[i],inputsStateValues[i],false,i,restOutputIndex);
                }

                int cptRetry = 0;
                ArrayList<Integer> subRestOutputIndex;
                do{
                    subRestOutputIndex = new ArrayList<>();
                    cptRetry++;
                    for(int i = 0 ; i < restOutputIndex.size(); i++){
                        findAndAdd(outputsParentNameValues[restOutputIndex.get(i)]
                                ,outputsNameValues[restOutputIndex.get(i)]
                                ,outputsStateValues[restOutputIndex.get(i)]
                                ,outputsFastValues[restOutputIndex.get(i)]
                                ,restOutputIndex.get(i)
                                ,subRestOutputIndex);
                    }
                    restOutputIndex = subRestOutputIndex;
                }while (subRestOutputIndex.size() > 0 && cptRetry < 50);

                if(textToFind.isEmpty()){
                    super.setModel(new DefaultTreeModel(master));
                }else{
                    Delay filtredMaster = master.ifChildrenMatch(textToFind.toLowerCase());
                    if(filtredMaster == null){      // havn't children
                        Delay rootTmp = new Delay("root",DevState.UNKNOWN,false);
                        rootTmp.add(new Delay("Delay not Found!",DevState.UNKNOWN,false));
                        this.setModel(new DefaultTreeModel(rootTmp));
                    } else {
                        this.setModel(new DefaultTreeModel(filtredMaster));
                    }
                }

                //System.out.println("Before repaint:" + (System.currentTimeMillis() - start) + "ms");
                this.repaint();
                //System.out.println("after repaint:" + (System.currentTimeMillis() - start) + "ms");

                for(int i = 0; i < this.getRowCount(); i++) {    
                    this.expandRow(i);
                }

            }else{
                super.setModel(new DefaultTreeModel(makeRootModel()));
            }
        }catch(NullPointerException ex){
            ex.printStackTrace();
        }
        
    }
    
    private void findAndAdd(String source, String delay,DevState state,boolean isFastTiming, int index, ArrayList<Integer> restIndex ) {
        Delay d = master.findMyParent(source);

        if(d != null){
            //System.out.println("source found :"+source);
            d.add(new Delay(delay,state,isFastTiming));
        }else{
            if(! (source.equals("NO PARENT") || source.equals("infra/t-whist/bunchclock") ) ){
                //System.out.println("source not found :"+source);
                restIndex.add(index);
            }
        }
    }

    @Override
    public void booleanSpectrumChange(BooleanSpectrumEvent bse) {
        if(bse.getSource() == outputsFast){
            outputsFastValues = bse.getValue();
        }
        refreshModel();
    }

    @Override
    public void keyTyped(KeyEvent e) {
        //nothing to do
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //nothing to do
    }

    @Override
    public void keyReleased(KeyEvent e) {
        String tmp = researchTf.getText();
        if(tmp.compareTo(textToFind) != 0){
            textToFind = tmp;
            firstPass = true;
            refreshModel();
        }
    }

    void setOnlyInput(boolean onlyInput) {
        this.onlyInput = onlyInput;
    }
}
