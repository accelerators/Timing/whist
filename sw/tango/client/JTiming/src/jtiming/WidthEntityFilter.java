package jtiming;


import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.IEntityFilter;
import fr.esrf.tangoatk.core.INumberScalar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author tappret
 */
public class WidthEntityFilter implements IEntityFilter {

    @Override
    public boolean keep(IEntity ie) {
        if (ie.isOperator() && (ie instanceof INumberScalar)) {
            String name = ie.getNameSansDevice().toLowerCase();
            if (name.contains("width")) {
                return true;
            }
        }
        return false;
    }
}
