/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.Tango.DevState;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Image;
import java.util.regex.Pattern;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author tappret
 */
public class WhistConstants {

    public final static Image wrLogo = new ImageIcon(WhistConstants.class.getResource("img/wrLogo16pxWithoutBackGround.png")).getImage();
    public final static ImageIcon jawr = new ImageIcon(WhistConstants.class.getResource("img/jawr_resized.jpg"));
    
    static String[] BUNCHCLOCK_SIGNAL = {"T0", "Tinj", "Tti", "Ki", "Gun", "Text", "Tte"};
    static String[] CLOCK_SIGNAL = {"Clock", "SR", "SY", "16 Bunches", "4 Bunches", "SR Count", "32 Count"};
    static String[] BUNCHCLOCK_SIGNAL_AND_CLOCK = {"T0", "Tinj", "Tti", "Ki", "Gun", "Text", "Tte", "Clock", "SR", "SY", "16 Bunches", "4 Bunches", "SR Count", "32 Count","Event","Input Stamping"};

    
    public final static String DEV_BUNCHCLOCK = "infra/t-whist/bunchclock";
    public final static String DEV_NETWORK = "infra/t-whist-net/1";
    public final static String TIMING_SETTING_MANAGER = "sys/settings/timing";
    public final static String T0_SOURCE_DEVICE = "sy/t0-source/manage";

    public final static Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
    public final static Cursor defaultCursor = new Cursor(Cursor.DEFAULT_CURSOR);

    final static String TIMING_SYNOPTIC_RESOURCE = "/jtiming/jdrawTiming.jdw";

    final static int NB_OUTPUT_BY_WHIST = 12;
    final static int NB_INPUT_BY_WHIST = 4;

    public final static Color myLigthGray = new Color(238, 238, 238);
    //public final static Color myLigthGrayTRANSLUCENT = new Color(238, 238, 238, 255);
    public final static Color TRANSLUCENT = new Color(0, 0, 0, 0);

    public static String extractMember(String name) {
        int idx = name.lastIndexOf('/');
        if (idx >= 0) {
            String n = name.substring(idx + 1);
            if (n.startsWith("cell")) {
                return "c" + n.substring(4);
            } else {
                return n;
            }
        } else {
            return "???";
        }
    }

    public static boolean isBunchClockSignal(String deviceNameToLower) {
        for (String signal : BUNCHCLOCK_SIGNAL) {
            if (deviceNameToLower.equalsIgnoreCase(signal)) {
                return true;
            }
        }
        return false;
    }

    static boolean isClockSignal(String deviceNameToLower) {
        for (String signal : CLOCK_SIGNAL) {
            if (deviceNameToLower.equalsIgnoreCase(signal)) {
                return true;
            }
        }
        return false;
    }
    
    public static boolean isDeviceName(String devName)
    {

        boolean devNamePattern;

        String s = new String(devName);
        String devicePattern = "[a-zA-Z_0-9[-][\\.]]+/[a-zA-Z_0-9[-][\\.]]+/[a-zA-Z_0-9[-][\\.]]+";

        // Remove the 'tango:'
        if (s.startsWith("tango:")) {
            s = s.substring(6);
        }

        // Check full syntax: //hostName:portNumber/domain/family/member
        devNamePattern = Pattern.matches("//[a-zA-Z_0-9]+:[0-9]+/" + devicePattern, s);

        // Check classic syntax: domain/family/member
        if (devNamePattern == false) {
            devNamePattern = Pattern.matches(devicePattern, s);
        }

        // Check taco syntax: taco:domain/family/member
        if (devNamePattern == false) {
            devNamePattern = Pattern.matches("taco:" + devicePattern, s);
        }

        // Check taco syntax: taco://nethost/domain/family/member
        if (devNamePattern == false) {
            devNamePattern = Pattern.matches("taco://" + devicePattern, s);
        }

        // Change added to support device names beginning with TANGO_HOST ip adress
        // Check full syntax: //ipAddress:portNumber/domain/family/member
        // Modification sent by  "Alan David Zoldan" <alan@dataeco.com.br>
        // Change included by F. Poncet on 15th April 2005
        if (devNamePattern == false) {
            devNamePattern = Pattern.matches("//[0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+:[0-9]+/" + devicePattern, s);
        }

        return devNamePattern;
   }

}
