/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.Tango.DevState;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.MutableTreeNode;

/**
 *
 * @author tappret
 */
class Delay extends DefaultMutableTreeNode {

    public Delay(Delay d) {
        this(d.deviceName, d.state, d.isFastTiming);
    }

    public Delay(String deviceName, DevState state, boolean fastTiming) {
        super(deviceName);
        this.deviceName = deviceName;
        this.state = state;
        this.deviceNameToLower = deviceName.toLowerCase();
        this.isFastTiming = fastTiming;
    }

    boolean isFastTiming;
    DevState state;
    String deviceName;
    String deviceNameToLower;

    Delay findMyParent(String name) {
        if (deviceName.equalsIgnoreCase(name)) {
            return this;
        }
        
        if(deviceName.equals("Input Stamping")){
            if(name.equalsIgnoreCase("Stamp")){
                return this;
            }
        }

        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                Delay rtn = ((Delay) children.get(i)).findMyParent(name);
                if (rtn != null) {
                    return rtn;
                }
            }
        }
        return null;
    }

    public void add(Delay newChild) {
        super.add(newChild);
    }

    @Override
    public void add(MutableTreeNode newChild) {
        throw new UnsupportedOperationException("Not Usable like this. Please Add Delay instead of MutableTreeNode");
    }

    Delay ifChildrenMatch(String toFind) {
        if (this.isLeaf()) {
            if (this.deviceNameToLower.contains(toFind)) {
                return new Delay(this);
            } else {
                return null;
            }
        } else {
            //if branch
            Delay fildredThis = new Delay(deviceName,state, isFastTiming);

            //System.out.println(fildredThis.deviceName);
            //System.out.println(this.getChildCount());
            for (int i = 0; i < this.children.size(); i++) {
                Delay filtredChild = ((Delay) this.children.get(i)).ifChildrenMatch(toFind);

                if (filtredChild != null) {
                    //System.out.println(filtredChild.deviceName);
                    fildredThis.add(filtredChild);
                }
            }

            if (fildredThis.isLeaf()) {
                if (!deviceNameToLower.contains(toFind)) {
                    return null;
                }
            }
            return fildredThis;
        }
    }
    
    
    Delay findDelay(String toFind) {
        if (deviceName.equals(toFind)) {
            return this;
        }

        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                Delay rtn = ((Delay) children.get(i)).findDelay(toFind);
                if (rtn != null) {
                    return rtn;
                }
            }
        }
        return null;
    }
    
    void filtring(String toRemove){
        if (this.isLeaf()) {
            return;
        } else {
            for (int i = 0; i < this.children.size(); i++) {
                Delay current = ((Delay)children.get(i));
                if(current.deviceNameToLower.compareToIgnoreCase(toRemove) == 0){
                    children.remove(i);
                }else{
                    current.filtring(toRemove);
                }
            }
        }
    }
}
