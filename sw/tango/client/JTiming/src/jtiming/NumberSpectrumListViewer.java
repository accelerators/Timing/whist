/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.INumberSpectrum;
import fr.esrf.tangoatk.core.ISpectrumListener;
import fr.esrf.tangoatk.core.NumberSpectrumEvent;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
 *
 * @author tappret
 */
public class NumberSpectrumListViewer extends JTextArea implements ISpectrumListener{
    private String currentText;
    private NumberSpectrumListViewer thisDs;
    private INumberSpectrum model;
    private final NumberFormat nf = new DecimalFormat("#0");
    private boolean forceRefresh = false;
    public NumberSpectrumListViewer() {
        thisDs = this;
        currentText = "";
        setWrapStyleWord(true);
        setLineWrap(true);
        setEditable(false);
        addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent event) {
                viewerActionPerformed(event);
            }
        });
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER
                        || e.getKeyCode() == KeyEvent.VK_TAB) {
                    String text = getText();
                    
                    //delete eventual double space
                    text = text.replace("\n", " ").replace("\r", " ");
                    while (text.contains("  ")){
                        text = text.replace("  ", " ");
                    }
                        
                    String [] split = text.split(" ");
                    double [] numberList = new double [split.length];
                    try{
                        for (int i = 0; i < split.length; i++) {
                            numberList[i] = Double.parseDouble(split[i]);
                        }
                        model.setValue(numberList);
                    }catch(NumberFormatException | IllegalStateException ex){
                        ErrorPane.showErrorMessage(thisDs, model.getName(), ex);
                    }
                    
                    if(e.getKeyCode() == KeyEvent.VK_TAB) {
                        KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent();
                    }
                }
                
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    forceRefresh = true;
                    model.refresh();
                }
            }    
        });
    }

    /**
     *  To avoid to set edition mode by a click on the text area by mistake
     *  a double click is now needed and a clear  Apply/Cancel dialog is launched
     */
    private void viewerActionPerformed(MouseEvent event) {
        if (event.getClickCount()==2) {
            Component component = this;
            while (!(component instanceof JFrame))
                component = component.getParent();
                new InputBunchListDialog((JFrame) component, model, currentText, getPreferredSize()).setVisible(true);
            forceRefresh = true;
            model.refresh();
        }
    }

    @Override
    public void spectrumChange(NumberSpectrumEvent nse) {
        if(!forceRefresh){
            this.setBorder(new TitledBorder(model.getLabel()));
        }
        forceRefresh = false;

        double[] dArray = nse.getDeviceValue();
        StringBuilder sb = new StringBuilder();
        for (double v : dArray) {
            sb.append(nf.format(v));
            sb.append(" ");
        }
        currentText = sb.toString();

        if(currentText.isEmpty()){
            currentText = "Empty array";
        }else{
            try{
                currentText = currentText.substring(0, (currentText.length() - 1));
            }catch(ArrayIndexOutOfBoundsException ex){
                ex.printStackTrace();

                System.err.println("currentText.length():\"" + currentText.length() + "\"");
                System.err.println("currentText:\"" + currentText + "\"");
            }
        }
        //System.out.println("\"" + currentText + "\"");

        SwingUtilities.invokeLater(() -> {
            thisDs.setEnabled(true);
            thisDs.setBackground(Color.white);
            thisDs.setText(currentText);
        });
        setToolTipText("Double click to edit.");
    }

    @Override
    public void stateChange(AttributeStateEvent ase) {
        System.out.println(ase.getState());
        
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void errorChange(ErrorEvent ee) {
        
        SwingUtilities.invokeLater(() -> {
            setEnabled(false);
            setBackground(WhistConstants.myLigthGray);
            setText("INVALID");
        });
    }
    
    void setModel(INumberSpectrum ins) {
        //System.out.println(ins.getName());
        clearModel();
        this.model = ins;

        setToolTipText(model.getName());
        
        model.addSpectrumListener(this);
        model.refresh();
    }
    
    private void clearModel() {
        if (model != null) {
            model.removeSpectrumListener(this);
            setText("WAIT MODEL");
            setToolTipText(null);
            this.setBorder(null);
            
            model = null;
        }

        setToolTipText(null);
    }
}
