/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.CommandInfo;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.IDevStateScalar;
import fr.esrf.tangoatk.core.IStringScalar;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTree;

/**
 *
 * @author tappret
 */
public class TreeDelayFrame extends javax.swing.JFrame {
    public ErrorHistory errh = new ErrorHistory();
    private ErrorPopup errorPopup = null;
    TreeSearchPane pane;
    JTree jTree;

    AttributeList attrListStateStatus;
    AttributeList attrListOther;
    ConfigOutputFrame cof;

    /**
     * Creates new form TreeDelay
     * @throws fr.esrf.Tango.DevFailed
     */
    public TreeDelayFrame() throws DevFailed {
        initComponents();

        setIconImage(WhistConstants.wrLogo);
        setTitle("Delays dependencies View");
        
        errorPopup = ErrorPopup.getInstance();
        
        jButtonSelect.setVisible(false);
        jButtonCancel.setVisible(false);

        pane = new TreeSearchPane();

        jSplitPane1.setLeftComponent(pane);

        jTree = pane.getJTree();
        pane.addErrorListener(errh);
        
        jTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e); //To change body of generated methods, choose Tools | Templates.
                mouseClickedInteraction(e);
            }
        });

        showInternalDevice(false);
    }

    TreeDelayFrame(ConfigOutputFrame aThis) throws DevFailed {
        this();
        
        cof = aThis;
        jButtonSelect.setVisible(true);
        jButtonCancel.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jPanel1 = new javax.swing.JPanel();
        stateViewer1 = new fr.esrf.tangoatk.widget.attribute.StateViewer();
        commandComboViewer1 = new fr.esrf.tangoatk.widget.command.CommandComboViewer();
        statusViewer1 = new fr.esrf.tangoatk.widget.attribute.StatusViewer();
        scalarListViewer1 = new fr.esrf.tangoatk.widget.attribute.ScalarListViewer();
        jLabel2 = new javax.swing.JLabel();
        jPanelDummy = new javax.swing.JPanel();
        jButtonCancel = new javax.swing.JButton();
        jButtonSelect = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        errorHistMenuItem = new javax.swing.JMenuItem();
        diagMenuItem = new javax.swing.JMenuItem();

        jSplitPane1.setDividerLocation(390);

        jPanel1.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.weightx = 0.5;
        gridBagConstraints.insets = new java.awt.Insets(6, 6, 6, 6);
        jPanel1.add(stateViewer1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.weightx = 0.5;
        jPanel1.add(commandComboViewer1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 6, 6, 6);
        jPanel1.add(statusViewer1, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 6, 0);
        jPanel1.add(scalarListViewer1, gridBagConstraints);

        jLabel2.setText("Select device for more infomation");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.gridwidth = 2;
        jPanel1.add(jLabel2, gridBagConstraints);

        javax.swing.GroupLayout jPanelDummyLayout = new javax.swing.GroupLayout(jPanelDummy);
        jPanelDummy.setLayout(jPanelDummyLayout);
        jPanelDummyLayout.setHorizontalGroup(
            jPanelDummyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        jPanelDummyLayout.setVerticalGroup(
            jPanelDummyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.weighty = 1.0;
        jPanel1.add(jPanelDummy, gridBagConstraints);

        jScrollPane2.setViewportView(jPanel1);

        jSplitPane1.setRightComponent(jScrollPane2);

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        jButtonSelect.setText("Select");
        jButtonSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSelectActionPerformed(evt);
            }
        });

        jMenu1.setText("File");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        jMenuItem1.setText("Exit");
        jMenu1.add(jMenuItem1);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("View");

        errorHistMenuItem.setText("Error History");
        errorHistMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                errorHistMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(errorHistMenuItem);

        diagMenuItem.setText("Diagnostic");
        diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diagMenuItemActionPerformed(evt);
            }
        });
        jMenu2.add(diagMenuItem);

        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1079, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonSelect)
                .addGap(18, 18, 18)
                .addComponent(jButtonCancel)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 634, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCancel)
                    .addComponent(jButtonSelect))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButtonCancelActionPerformed

    private void jButtonSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSelectActionPerformed
        if(!jTree.isSelectionEmpty()){
            Object [] objPath = jTree.getSelectionPath().getPath();
            Delay [] path = Arrays.copyOf(objPath,objPath.length,Delay[].class);
            
            cof.setSelectedDevice(path[path.length-1].deviceName);
            this.dispose();
        }else{
            JOptionPane.showMessageDialog(null, "Please Select device in device tree");
        }
    }//GEN-LAST:event_jButtonSelectActionPerformed

    private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
        fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
    }//GEN-LAST:event_diagMenuItemActionPerformed

    private void errorHistMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorHistMenuItemActionPerformed
        errh.setVisible(true);
    }//GEN-LAST:event_errorHistMenuItemActionPerformed

    @Override
    public void dispose() {
        this.setVisible(false);
        super.dispose(); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            TreeDelayFrame thisIs;
            try {
                thisIs = new TreeDelayFrame();
                
                thisIs.setVisible(true);
                thisIs.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                thisIs.dispose();
                
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(TreeDelayFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                thisIs = new TreeDelayFrame();
                thisIs.setVisible(true);
                thisIs.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                
            } catch (DevFailed ex) {
                ErrorPane.showErrorMessage(null, WhistConstants.DEV_NETWORK, ex);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private fr.esrf.tangoatk.widget.command.CommandComboViewer commandComboViewer1;
    private javax.swing.JMenuItem diagMenuItem;
    private javax.swing.JMenuItem errorHistMenuItem;
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonSelect;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelDummy;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private fr.esrf.tangoatk.widget.attribute.ScalarListViewer scalarListViewer1;
    private fr.esrf.tangoatk.widget.attribute.StateViewer stateViewer1;
    private fr.esrf.tangoatk.widget.attribute.StatusViewer statusViewer1;
    // End of variables declaration//GEN-END:variables

    private void showInternalDevice(boolean deviceVisible) {
        this.jLabel2.setVisible(!deviceVisible);
        this.jPanelDummy.setVisible(deviceVisible);
        this.commandComboViewer1.setVisible(deviceVisible);
        this.stateViewer1.setVisible(deviceVisible);
        this.statusViewer1.setVisible(deviceVisible);
        this.scalarListViewer1.setVisible(deviceVisible);
    }

    private void mouseClickedInteraction(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1 && jTree.getSelectionModel().getSelectionPath() != null) {
            try {
                Delay selectedDelay = (Delay) jTree.getSelectionModel().getSelectionPath().getLastPathComponent();
                String device = selectedDelay.deviceName;
                if (WhistConstants.isBunchClockSignal(selectedDelay.deviceNameToLower)) {
                    device = WhistConstants.DEV_BUNCHCLOCK;
                }else if(WhistConstants.isClockSignal(selectedDelay.deviceNameToLower)){
                    device = null;
                }
                if (e.getClickCount() == 2) {
                    if(device != null){
                        new atkpanel.MainPanel(device, false);
                    }
                }

                if (attrListStateStatus != null) {
                    attrListStateStatus.clear();
                }

                if(device != null){
                    attrListStateStatus = new AttributeList();
                    IDevStateScalar stateEntity = (IDevStateScalar) attrListStateStatus.add(device + "/state");
                    stateViewer1.clearModel();
                    stateViewer1.setModel(stateEntity);

                    attrListStateStatus.addErrorListener(errh);
                    attrListStateStatus.addSetErrorListener(errh);
                    attrListStateStatus.addSetErrorListener(errorPopup);

                    IStringScalar statusEntity = (IStringScalar) attrListStateStatus.add(device + "/status");
                    statusViewer1.clearModel();
                    statusViewer1.setModel(statusEntity);

                    attrListStateStatus.startRefresher();

                    try {
                        DeviceProxy dp = new DeviceProxy(device);

                        if (attrListOther != null) {
                            attrListOther.clear();
                        }

                        String[] attrNames = dp.get_attribute_list();
                        attrListOther = new AttributeList();

                        attrListOther.addErrorListener(errh);
                        attrListOther.addSetErrorListener(errh);
                        attrListOther.addSetErrorListener(errorPopup);

                        for (String attrName : attrNames) {
                            if (!(attrName.equals("State") || attrName.equals("Status"))) {
                                attrListOther.add(device + "/" + attrName);
                            }
                        }
                        scalarListViewer1.setModel(attrListOther);

                        CommandInfo[] cmdInfo = dp.command_list_query();

                        CommandList cl = new CommandList();
                        for (CommandInfo cmdInfo1 : cmdInfo) {
                            cl.add(device + "/" + cmdInfo1.cmd_name);
                        }
                        commandComboViewer1.setModel(cl);

                        attrListOther.startRefresher();
                    } catch (DevFailed ex) {
                        Logger.getLogger(TreeDelayFrame.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                showInternalDevice(true);
            } catch (ConnectionException ex) {
                
                //Logger.getLogger(TreeDelayFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
