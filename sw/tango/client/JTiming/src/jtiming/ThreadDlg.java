package jtiming;

import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.SwingUtilities;
import jtiming.ThreadPanel;

/**
 * Class for handling a long task in an asynchronous way without blocking GUI
 *
 * @author pons
 */
public class ThreadDlg extends JDialog {

    static public boolean stopflag;
    static public ThreadDlg progDlg = null;

    private final ThreadPanel panel;
    private Thread subProc;

    // Construction
    public ThreadDlg(Frame parent, String title, boolean hasProgress, Thread process) {
        super(parent, true);
        getContentPane().setLayout(new BorderLayout());

        panel = new ThreadPanel();
        panel.setMessage(title);
        panel.setOpaque(false);
        panel.setBorder(BorderFactory.createEtchedBorder());
        if (!hasProgress) {
            panel.hideProgress();
        }
        setContentPane(panel);
        setUndecorated(true);

        // Add a thread listener
        subProc = process;

        stopflag = false;

        progDlg = (ThreadDlg) this;
        // Add window listener to start the subProc
        // when the dialog is displayed
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                subProc.start();
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            subProc.join();
                            hideDlg();
                        } catch (InterruptedException ex) {
                            Logger.getLogger(ThreadDlg.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }).start();
            }
        });

    }

    @Override
    public void setTitle(String text) {
        panel.setMessage(text);
    }

    public void setProgress(double p, String prefix) {
        panel.setProgress(p, prefix);
    }

    public void showDlg() {
        ATKGraphicsUtils.centerDialog(this);
        setVisible(true);
    }

    public void hideDlg() {
        setVisible(false);
        progDlg = null;
    }

}
