/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbAttribute;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DbDevImportInfo;
import fr.esrf.TangoApi.DbServInfo;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.TangoDs.Except;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;

/**
 *
 * @author tappret
 */
public class ConfigInputFrame extends javax.swing.JFrame {
    
    DeviceProxy dpModule;
    String moduleSimpleName = new String();
    String baseTitleLabel = new String();
    //TreeDelayFrame tdf = null;
    WhistFrame parent = null;
    boolean createOrModified;
    InputData lastDataLoad;
    int lastInputSelected;
    ThreadDlg progressThread;
    
    /**
     * Creates new form ConfigInputFrame
     */
    public ConfigInputFrame(String moduleDeviceName,boolean createOrModified) throws DevFailed{
        initComponents();
        
        setIconImage(WhistConstants.wrLogo);
        
        this.createOrModified = createOrModified;
        
        dpModule = new DeviceProxy(moduleDeviceName);
        moduleSimpleName = moduleDeviceName.split("/")[2];
        
        baseTitleLabel = jLabelTitle.getText();
        
        jLabelTitle.setText(baseTitleLabel + " "+ moduleSimpleName);

        
        jButtonCreate.setVisible(createOrModified);
        jButtonModify.setVisible(!createOrModified);
        if(createOrModified){
            setTitle("Create Input on whist module " + moduleSimpleName);
            
        }else{
            setTitle("Modified Input on whist module " + moduleSimpleName);
        }
        
        //set Default Formular
        //setFormToKind(jComboBoxKind.getSelectedIndex());
        //setFormFrom(jComboBoxFrom.getSelectedIndex());
    }
    
    ConfigInputFrame(WhistFrame parent, String moduleDeviceName, boolean createOrModified) throws DevFailed {
        this(moduleDeviceName,createOrModified);
        this.parent = parent;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelTitle = new javax.swing.JLabel();
        jLabelKind = new javax.swing.JLabel();
        jComboBoxKind = new javax.swing.JComboBox<>();
        jLabelOutputNumber = new javax.swing.JLabel();
        jComboBoxInputNumber = new javax.swing.JComboBox<>();
        jButtonCreate = new javax.swing.JButton();
        jButtonModify = new javax.swing.JButton();
        jButtonCancel = new javax.swing.JButton();
        jLabelDevName = new javax.swing.JLabel();
        jTextFieldDevName = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabelTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabelTitle.setText("Whist Module Name :");

        jLabelKind.setText("Kind of Input :");

        jComboBoxKind.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Stamping", "Event" }));

        jLabelOutputNumber.setText("Whist Input :");

        jComboBoxInputNumber.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "0", "1", "2", "3" }));
        jComboBoxInputNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxInputNumberActionPerformed(evt);
            }
        });

        jButtonCreate.setText("Create Input");
        jButtonCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCreateActionPerformed(evt);
            }
        });

        jButtonModify.setText("Modify Input");
        jButtonModify.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonModifyActionPerformed(evt);
            }
        });

        jButtonCancel.setText("Cancel");
        jButtonCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelActionPerformed(evt);
            }
        });

        jLabelDevName.setText("Device Name :");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabelTitle, javax.swing.GroupLayout.PREFERRED_SIZE, 577, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabelKind)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBoxKind, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabelOutputNumber)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jComboBoxInputNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonCreate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonModify)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonCancel))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelDevName)
                        .addGap(144, 144, 144)
                        .addComponent(jTextFieldDevName, javax.swing.GroupLayout.PREFERRED_SIZE, 322, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabelTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxKind, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelKind))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBoxInputNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelOutputNumber))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabelDevName, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldDevName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonCancel)
                    .addComponent(jButtonModify)
                    .addComponent(jButtonCreate))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBoxInputNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxInputNumberActionPerformed
        String oldDevName = jTextFieldDevName.getText();
        String defaultDevName = getDefaultDeviceName(lastInputSelected);

        if(oldDevName.equals(defaultDevName)){
            jTextFieldDevName.setText(getDefaultDeviceName((Integer)jComboBoxInputNumber.getSelectedItem()));
        }

        lastInputSelected = (Integer)jComboBoxInputNumber.getSelectedItem();
    }//GEN-LAST:event_jComboBoxInputNumberActionPerformed

    private void jButtonCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCreateActionPerformed
        final InputData id = getDataFromComponent();

        if (!id.equals(lastDataLoad)) {
            progressThread = new ThreadDlg(parent, "Update device server in progress ... ", false, new Thread(() -> {
                try {
                    //setVisible(false);
                    updateProperty(id);

                    //force Refresh output
                    DeviceData dd = new DeviceData();

                    dpModule.command_inout("CheckInOutUsage");
                    parent.setModuleDevice(dpModule.get_name());
                    progressThread.hideDlg();

                    parent.setVisible(true);
                    dispose();

                } catch (DevFailed ex) {
                    progressThread.hideDlg();
                    setVisible(true);
                    ErrorPane.showErrorMessage(rootPane, jTextFieldDevName.getText(), ex);
                }
            }));
            this.setVisible(false);
            progressThread.showDlg();
        }
    }//GEN-LAST:event_jButtonCreateActionPerformed

    private void jButtonModifyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonModifyActionPerformed
        final InputData id = getDataFromComponent();
        if (!id.equals(lastDataLoad)) {
            progressThread = new ThreadDlg(parent, "Update device server in progress ... ", false, new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        //setVisible(false);
                        boolean ifServerWasRestart = updateProperty(id);

                        //remove CheckInOutUsage because is already do in restart server
                        //force Refresh output
                        dpModule.command_inout("CheckInOutUsage");
                        
                        parent.setModuleDevice(dpModule.get_name());
                        parent.reStartThisIfOpen(lastDataLoad.name,id.name);
                        progressThread.hideDlg();

                        parent.setVisible(true);
                        dispose();

                    } catch (DevFailed ex) {
                        progressThread.hideDlg();
                        setVisible(true);
                        ErrorPane.showErrorMessage(rootPane, jTextFieldDevName.getText(), ex);
                    }
                }
            }));
            this.setVisible(false);
            progressThread.showDlg();
        }else{
            jButtonCancelActionPerformed(evt);
        }
    }//GEN-LAST:event_jButtonModifyActionPerformed

    private void jButtonCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelActionPerformed
        if(this.getDefaultCloseOperation() == JFrame.EXIT_ON_CLOSE){
            System.exit(0);
        }else{
            this.setVisible(false);
            this.dispose();
        }
    }//GEN-LAST:event_jButtonCancelActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ConfigInputFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ConfigInputFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ConfigInputFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ConfigInputFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                //new ConfigInputFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancel;
    private javax.swing.JButton jButtonCreate;
    private javax.swing.JButton jButtonModify;
    private javax.swing.JComboBox<String> jComboBoxInputNumber;
    private javax.swing.JComboBox<String> jComboBoxKind;
    private javax.swing.JLabel jLabelDevName;
    private javax.swing.JLabel jLabelKind;
    private javax.swing.JLabel jLabelOutputNumber;
    private javax.swing.JLabel jLabelTitle;
    private javax.swing.JTextField jTextFieldDevName;
    // End of variables declaration//GEN-END:variables

    @Override
    public void dispose() {
        super.dispose(); //To change body of generated methods, choose Tools | Templates.
        parent.resetConfigInputFrame();
    }
    
    private Integer[] getInputAvaiable(String[] inputs, InputData od) throws DevFailed {
        int cptAvaiable = 0;
        for(String input: inputs){
            if(input.equals("Unused")){
                cptAvaiable++;
            }
        }
        
        if(cptAvaiable <= 0 && createOrModified){
            Except.throw_exception("No more Input Available", "NoInputAvailable");
        }
        
        Integer [] avaiable;
        if(!createOrModified){           //is Modified
            avaiable = new Integer[cptAvaiable + 1];
            avaiable[0] = od.number;
            cptAvaiable = 1;
        } else {                        //is Create
            avaiable = new Integer[cptAvaiable];
            cptAvaiable = 0;
        }
        
        
        
        for(int i = 0; i < inputs.length ; i++){
            if(inputs[i].equals("Unused")){
                avaiable[cptAvaiable] = i;
                cptAvaiable++;
            }
        }
        return avaiable;
    }
    
    void readConfigureFromDatabase(String deviceName) throws DevFailed {
        InputData id = new InputData(deviceName, true);
        
        
        DeviceAttribute da = dpModule.read_attribute("InputDevices");
        String[] outputs = da.extractStringArray();
        Integer [] outputAvailable = getInputAvaiable(outputs,id);
        
        jComboBoxInputNumber.setModel(new DefaultComboBoxModel(outputAvailable));
        
        setDataToComponent(id);
    }

    private void setDataToComponent(InputData id) {
        lastDataLoad = id;
        
        lastInputSelected = (Integer)jComboBoxInputNumber.getSelectedItem();
        
        if(id.kind == InputData.InputKind.Event){
            jComboBoxKind.setSelectedItem("Event");
        }else{
            jComboBoxKind.setSelectedItem("Stamping");
        }
        jComboBoxInputNumber.setSelectedItem(id.number);
        jTextFieldDevName.setText(id.name);
    }
    
    private InputData getDataFromComponent() {
        InputData  id = new InputData(jTextFieldDevName.getText());
        
        if(jButtonCreate.isVisible()){
            id.creation = true;
        }
        
        id.number = (int)jComboBoxInputNumber.getSelectedItem();
        
        String kind = (String)jComboBoxKind.getSelectedItem();
        if(kind.equals("Stamping")){
            id.kind = InputData.InputKind.Stamping;
        }else{
            id.kind = InputData.InputKind.Event;
        }
        
        return id;
    }
    
    

    void checkCreation() throws DevFailed {
        DeviceAttribute da = dpModule.read_attribute("InputDevices");
        String[] inputs = da.extractStringArray();
        Integer [] outputAvailable = getInputAvaiable(inputs,null);
        
        InputData id = new InputData(getDefaultDeviceName(outputAvailable[0]));
        
        jComboBoxInputNumber.setModel(new DefaultComboBoxModel(outputAvailable));
        
        setDataToComponent(id);
    }
    

    private String getDefaultDeviceName(int inputNumber) {
        return "infra/t-" + this.moduleSimpleName + "/in" + inputNumber;
    }
    
    /**
     * 
     * @param outConfig
     * @return true if the device server have been restarted. otherwise false.
     * @throws DevFailed 
     */
    private boolean updateProperty(InputData inConfig) throws DevFailed {
        boolean ifServerRestared = false;
        Database db = ApiUtil.get_db_obj();
        String [] previewPropertiesName = db.get_device_property_list(lastDataLoad.name, "*");
        
        //add check correct device name
        if(!WhistConstants.isDeviceName(inConfig.name)){
            Except.throw_wrong_syntax_exception("Device Name have not a good value", "Device Name have not a good value. Please Check !" , getClass().getName() + ".updateProperty()");
        }
        
        // Check if the device exist
        DbDevImportInfo infoDevice = null;
        boolean alreadyExist = true;
        try{
            infoDevice = db.import_device(inConfig.name);
            alreadyExist = true;
        }catch(DevFailed ex){
            alreadyExist = false;
        }
        
        if(alreadyExist && !inConfig.name.equals(lastDataLoad.name)){
            Except.throw_communication_timeout("DeviceAlreadyExist"
                    , inConfig.name +" already exits.\\nServer: " + infoDevice.server
                    , getClass().getName() + ".updateProperty()");
        }
        
        boolean isAlive = false;
        DeviceProxy oldDs = null;
        String[] attList = null;
        try{
            oldDs = new DeviceProxy(lastDataLoad.name);
            oldDs.ping();
            isAlive = true;
            attList = oldDs.get_attribute_list();
        }catch(DevFailed ex){
        }
        
        //DbDatum[] data = db.get_device_property(lastDataLoad.name, db.get_device_property_list(lastDataLoad.name, "*"));
        String serverName = "WhistInput/" + moduleSimpleName;
        DbServInfo dbsi = db.get_server_info(serverName);
        String host = dbsi.host;
        if(host.endsWith(".esrf.fr")){
            int length = host.length();
            host = host.substring(0, length - ((".esrf.fr").length()));
        }
        
        String [] starterList = db.get_device_name("Starter/" + host, "Starter");
        if(starterList.length != 1){
            Except.throw_communication_timeout("Starter Name Not Found"
                    , "impossible to find the name of starter"
                    , getClass().getName() + ".updateProperty()");
        }
        String starterName = starterList[0];
        
        DeviceProxy starter = new DeviceProxy(starterName);
        boolean serverStopped = false;
        
        String className = "WhistInput";
        String modePropName = "inputMode";
        String modeValue;
        if(inConfig.kind == InputData.InputKind.Event){
            modeValue = "event";
        }else{
            modeValue = "stamp";
        }
        
        
        if(inConfig.kind != lastDataLoad.kind
                || !inConfig.name.equalsIgnoreCase(lastDataLoad.name)
                || inConfig.creation
                || true){   //to force restart server in every case to improuve the bug Tango with dynamique attribute
                            //Due tu bug in Tango with dynamique attribute the restart of 
                            //the device can time to time generate a crash
            ifServerRestared = true;
            DeviceData dd = new DeviceData();
            dd.insert(serverName);
            starter.command_inout("DevStop", dd);

            int cptRetry = 60;
            do {
                //wait server Stop
                DeviceAttribute da = starter.read_attribute("Servers");
                String[] serversState = da.extractStringArray();
                for (String serverState : serversState) {
                    StringTokenizer stk = new StringTokenizer(serverState);
                    String name = stk.nextToken();
                    String state = stk.nextToken();
                    if (name.equalsIgnoreCase(serverName)
                            && state.equals("FAULT")) {
                        serverStopped = true;
                    }
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ConfigOutputFrame.class.getName()).log(Level.SEVERE, null, ex);
                }
                cptRetry--;
                System.out.println("cptRetry:" + cptRetry);
                if (cptRetry <= 0) {
                    Except.throw_communication_timeout("TimeoutForKillServer", "Timeout for kill server \"" + serverName + "\"  : abort update", getClass().getName() + ".updateProperty()");
                }
            } while (!serverStopped);

            db.add_device(inConfig.name, className, serverName);
            
            if (attList != null && attList.length > 0) {
                DbAttribute[] adata = db.get_device_attribute_property(lastDataLoad.name, attList);
                db.put_device_attribute_property(inConfig.name, adata);
            }

            if (!inConfig.creation && !inConfig.name.equals(lastDataLoad.name)) {
                //delete old device
                db.delete_device(lastDataLoad.name);
            }
        }else{
            for(String prop : previewPropertiesName){
                if(!prop.equals(modePropName) && !prop.equals("__SubDevices")){
                    System.out.println(prop);
                }
            }
        }
              
        
        DbDatum [] dbd;
        dbd = new DbDatum[3];
        dbd[0] = new DbDatum("WhistModuleDevice",dpModule.name());
        dbd[1] = new DbDatum("InputNumber",inConfig.number);
        dbd[2] = new DbDatum(modePropName,modeValue);

        db.put_device_property(inConfig.name, dbd);
        
        if(serverStopped){
            DeviceData dd = new DeviceData();
            dd.insert(serverName);
            starter.command_inout("DevStart",dd);
        }else{
            //Due tu bug in Tango with dynamique attribute the restart of 
            //the device can time to time generate a crash
            DeviceProxy admDev = new DeviceProxy("dserver/"+serverName);
            DeviceData dd = new DeviceData();
            dd.insert(inConfig.name);
            admDev.command_inout("DevRestart",dd);
        }
        return ifServerRestared;
    }
}
