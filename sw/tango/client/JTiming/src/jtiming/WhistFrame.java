/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import atkpanel.MainPanel;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevSource;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.attribute.AAttribute;
import fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPane;
import fr.esrf.tangoatk.widget.util.jdraw.JDLabel;
import fr.esrf.tangoatk.widget.util.jdraw.JDMouseEvent;
import fr.esrf.tangoatk.widget.util.jdraw.JDMouseListener;
import fr.esrf.tangoatk.widget.util.jdraw.JDObject;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.InputStreamReader;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;




/**
 *
 * @author tappret
 */
public class WhistFrame extends javax.swing.JFrame implements JDMouseListener{
    public ErrorHistory errh = new ErrorHistory();
    String [] outputDevices = new String[WhistConstants.NB_OUTPUT_BY_WHIST];
    JDObject [] jdObjOutputComponent = new JDObject[WhistConstants.NB_OUTPUT_BY_WHIST];
    JDObject [] jdObjLedOutputComponent = new JDObject[WhistConstants.NB_OUTPUT_BY_WHIST];
    String [] inputDevices = new String[WhistConstants.NB_INPUT_BY_WHIST];
    JDObject [] jdObjInputComponent = new JDObject[WhistConstants.NB_INPUT_BY_WHIST];
    JDObject [] jdObjLedInputComponent = new JDObject[WhistConstants.NB_INPUT_BY_WHIST];
    
    JDObject whistObj = null;
    JDLabel whistLabelObj = null;
    boolean firstTime = true;
    String lastClickedDevice;
    String moduleDeviceName;
    ConfigOutputFrame configOut = null;
    ConfigInputFrame configIn = null;
    WhistFrame thisIs;
    MainFrame parent;
    jive3.MainPanel jive = null;
    HelpFineDelay helpFineDelay = null;

    void resetConfigOutputFrame() {
        configOut = null;
    }
    
    void resetConfigInputFrame() {
        configIn = null;
    }

    void reStartThisIfOpen(String oldName, String newName) {
        boolean findIt = false;
        for(int i = 0 ; i < parent.atkPanelList.size() ; i++){
            MainPanel p = parent.atkPanelList.get(i);
            if(p.getName().equalsIgnoreCase(oldName)){
                p.setVisible(false);
                p.dispose();
                parent.atkPanelList.remove(i);
                p = null;
                findIt = true;
            }
        }
        
        if(findIt){
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(ConfigInputFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
            MainPanel atk = new atkpanel.MainPanel(newName, false);
            atk.setName(newName);
            atk.setExpertView(parent.expertViewAtkpanel);
            parent.atkPanelList.add(atk);
        }
    }
    
    
    public class mySynopticFileViewer extends SynopticFileViewer{
        

        @Override
        protected void parseJdrawComponents() {
            super.parseJdrawComponents();
        }
        
        public void startRefrecher() {
            AttributeList attrList = getAttributeList();
            
            if (attrList.size() > 0) {
                for (int i = 0; i < attrList.size(); i++) {
                    ((AAttribute) attrList.elementAt(i)).refresh();
                }
                attrList.startRefresher();
            }
        }
    }
    
    //TangoSynopticHandler synoptic = new TangoSynopticHandler()
    /**
     * Creates new form WhistFrame
     */
    public WhistFrame(MainFrame parent) {
        initComponents();
        
        thisIs = this;
        this.parent = parent;
        
        setLocationRelativeTo(parent);
        
        this.setIconImage(WhistConstants.wrLogo);

        try {
            synopticWhist.setErrorHistoryWindow(errh);
            synopticWhist.loadSynopticFromStream(new InputStreamReader(getClass().getResourceAsStream(WhistConstants.TIMING_SYNOPTIC_RESOURCE)));
            
            synopticWhist.setAutoZoom(true);
            pack();
            
            Vector objects = synopticWhist.getObjects();
            for(int i = 0 ; i < objects.size() ; i++ ){
                JDObject obj = ((JDObject)objects.get(i));
                String nameObj = obj.getName();
                
                if(nameObj.startsWith("output")){
                    int index = Integer.parseInt(nameObj.substring(6,nameObj.length()));
                    jdObjOutputComponent[index-1] = obj;
                    obj.setFillStyle(JDObject.FILL_STYLE_SOLID);
                    //obj.setBackground(Color.black);
                    obj.setBackground(WhistConstants.TRANSLUCENT);
                    obj.setForeground(WhistConstants.TRANSLUCENT);
                }
                
                if(nameObj.startsWith("input")){
                    int index = Integer.parseInt(nameObj.substring(5,nameObj.length()));
                    jdObjInputComponent[index] = obj;
                    obj.setFillStyle(JDObject.FILL_STYLE_SOLID);
                    //obj.setBackground(Color.black);
                    obj.setBackground(WhistConstants.TRANSLUCENT);
                    obj.setForeground(WhistConstants.TRANSLUCENT);
                }
                
                if(nameObj.startsWith("led")){
                    if(!nameObj.startsWith("ledInput")){
                        int index = Integer.parseInt(nameObj.substring(3,nameObj.length()));
                        jdObjLedOutputComponent[index-1] = obj;
                        obj.setFillStyle(JDObject.FILL_STYLE_SOLID);
                        obj.addExtension("className");
                        obj.setExtendedParam("className", "atkpanel.MainPanel");
                    }else{
                        int index = Integer.parseInt(nameObj.substring(8,nameObj.length()));
                        jdObjLedInputComponent[index] = obj;
                        obj.setFillStyle(JDObject.FILL_STYLE_SOLID);
                        obj.addExtension("className");
                        obj.setExtendedParam("className", "atkpanel.MainPanel");
                    }
                }
                
                if(nameObj.equals("WhistState")){
                    whistObj = obj;
                    obj.setFillStyle(JDObject.FILL_STYLE_SOLID);
                }
                if(nameObj.equals("whistNameLabel")){
                    whistLabelObj = (JDLabel)obj;
                }
                
                
                //((mySynopticFileViewer)synopticWhist).addAttribute(led, nameObj);
                //((mySynopticFileViewer)synopticWhist).parseJdrawComponents();

            }
            pack();
            
            //force resize of Synoptic
            Dimension dSyno = synopticWhist.getSize();
            Dimension dFram = new Dimension();
            double ratio = (double)(dSyno.height * 1.5) / dSyno.width;
            int width = 1366;
            dFram.width = width;
            dFram.height = (int)(width * ratio);
            setSize(dFram);
        } catch (Exception ex) {
            javax.swing.JOptionPane.showMessageDialog(
                    null, "Failed to get the inputStream for the synoptic file resource : " + WhistConstants.TIMING_SYNOPTIC_RESOURCE + "\n",
                    "Resource error",
                    javax.swing.JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        synopticWhist = new mySynopticFileViewer();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        jMenuItemNewOutput = new javax.swing.JMenuItem();
        jMenuItemNewInput = new javax.swing.JMenuItem();
        quitItem = new javax.swing.JMenuItem();
        jMenu1 = new javax.swing.JMenu();
        jMenuItemRefresh = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();
        errorHistMenuItem = new javax.swing.JMenuItem();
        diagMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        getContentPane().add(synopticWhist, java.awt.BorderLayout.CENTER);

        fileMenu.setText("File");

        jMenuItemNewOutput.setText("New Output");
        jMenuItemNewOutput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemNewOutputActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItemNewOutput);

        jMenuItemNewInput.setText("New Input");
        jMenuItemNewInput.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemNewInputActionPerformed(evt);
            }
        });
        fileMenu.add(jMenuItemNewInput);

        quitItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_DOWN_MASK));
        quitItem.setText("Quit");
        quitItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quitItemActionPerformed(evt);
            }
        });
        fileMenu.add(quitItem);

        jMenuBar1.add(fileMenu);

        jMenu1.setText("Edit");

        jMenuItemRefresh.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F5, 0));
        jMenuItemRefresh.setText("Refresh");
        jMenuItemRefresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemRefreshActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItemRefresh);

        jMenuBar1.add(jMenu1);

        viewMenu.setText("View");

        errorHistMenuItem.setText("Error History");
        errorHistMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                errorHistMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(errorHistMenuItem);

        diagMenuItem.setText("Diagnostic ...");
        diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                diagMenuItemActionPerformed(evt);
            }
        });
        viewMenu.add(diagMenuItem);

        jMenuBar1.add(viewMenu);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void quitItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quitItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_quitItemActionPerformed

    private void errorHistMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorHistMenuItemActionPerformed
        errh.setVisible(true);
    }//GEN-LAST:event_errorHistMenuItemActionPerformed

    private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
        fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
    }//GEN-LAST:event_diagMenuItemActionPerformed

    private void jMenuItemNewOutputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemNewOutputActionPerformed
        if(configOut == null){
            try{
                configOut = new ConfigOutputFrame(thisIs,moduleDeviceName, true);
                configOut.checkCreation();
                configOut.setLocationRelativeTo(thisIs);
                configOut.setVisible(true);
            }catch(DevFailed ex){
                configOut.dispose();
                ErrorPane.showErrorMessage(thisIs, moduleDeviceName, ex);
            }
        }
    }//GEN-LAST:event_jMenuItemNewOutputActionPerformed

    private void jMenuItemRefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemRefreshActionPerformed
        try {
            setModuleDevice(moduleDeviceName);
        } catch (DevFailed ex) {
            ErrorPane.showErrorMessage(thisIs, moduleDeviceName, ex);
        }
    }//GEN-LAST:event_jMenuItemRefreshActionPerformed

    private void jMenuItemNewInputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemNewInputActionPerformed
        if(configIn == null){
            try{
                configIn = new ConfigInputFrame(thisIs,moduleDeviceName, true);
                configIn.checkCreation();
                configIn.setLocationRelativeTo(thisIs);
                configIn.setVisible(true);
            }catch(DevFailed ex){
                configIn.dispose();
                ErrorPane.showErrorMessage(thisIs, moduleDeviceName, ex);
            }
        }
    }//GEN-LAST:event_jMenuItemNewInputActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                MainFrame f = new MainFrame();
                WhistFrame whistFrame = new WhistFrame(f);
                try {
                    //whistFrame.setModuleDevice("test/t-wst-mod/master");
                    //whistFrame.setModuleDevice("infra/t-module/lab26");
                    whistFrame.setModuleDevice("sys/tw-module/sat4");
                    whistFrame.setLocationRelativeTo(null);
                    whistFrame.setVisible(true);
                }catch(DevFailed ex){
                    ex.printStackTrace();
                }
                
            }
        });
    }

    @Override
    public void dispose() {
        super.dispose(); //To change body of generated methods, choose Tools | Templates.
        if(parent == null){
            System.exit(0);
        }
    }
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem diagMenuItem;
    private javax.swing.JMenuItem errorHistMenuItem;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItemNewInput;
    private javax.swing.JMenuItem jMenuItemNewOutput;
    private javax.swing.JMenuItem jMenuItemRefresh;
    private javax.swing.JMenuItem quitItem;
    private fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer synopticWhist;
    private javax.swing.JMenu viewMenu;
    // End of variables declaration//GEN-END:variables

    public void setModuleDevice(String moduleDeviceName) throws DevFailed {
        DeviceProxy dp = new DeviceProxy(moduleDeviceName);
        this.moduleDeviceName = moduleDeviceName;
        
        dp.set_source(DevSource.DEV);
        DeviceAttribute da = dp.read_attribute("outputDevices");
        outputDevices = da.extractStringArray();
        
        da = dp.read_attribute("inputDevices");
        inputDevices = da.extractStringArray();
        
        whistObj.setName(moduleDeviceName + "/state");
        
        String member = WhistConstants.extractMember(moduleDeviceName);
        this.setTitle("Whist front view  :  " + member);
        
        whistLabelObj.setText(member);
        //whistObj.addMouseListener((JDMouseListener)this);
        
        for(int i = 0 ; i < outputDevices.length ; i++){
            jdObjOutputComponent[i].removeMouseListener((JDMouseListener)this);
            jdObjLedOutputComponent[i].removeMouseListener((JDMouseListener)this);
            
            boolean outputIsConfigured = !outputDevices[i].equals("Unused");
            jdObjLedOutputComponent[i].setVisible(outputIsConfigured);
            if(outputIsConfigured){
                jdObjLedOutputComponent[i].setForeground(Color.black);
                jdObjOutputComponent[i].addMouseListener((JDMouseListener)this);
                Vector rtn = new Vector();
                //JDRoundRectangle grp = (JDRoundRectangle)jdObjOutputDevice[i];
                jdObjOutputComponent[i].getObjectsByName(rtn, "led", false);
                
                System.out.println(rtn.size());
                
                jdObjLedOutputComponent[i].setName(outputDevices[i]+ "/state");
                
                jdObjLedOutputComponent[i].addMouseListener((JDMouseListener)this);
            }
        }
        
        for(int i = 0 ; i < inputDevices.length ; i++){
            jdObjInputComponent[i].removeMouseListener((JDMouseListener)this);
            jdObjLedInputComponent[i].removeMouseListener((JDMouseListener)this);
            
            boolean inputIsConfigured = !inputDevices[i].equals("Unused");
            jdObjLedInputComponent[i].setVisible(inputIsConfigured);
            if(inputIsConfigured){
                jdObjLedInputComponent[i].setForeground(Color.black);
                jdObjInputComponent[i].addMouseListener((JDMouseListener)this);
                Vector rtn = new Vector();
                //JDRoundRectangle grp = (JDRoundRectangle)jdObjOutputDevice[i];
                jdObjInputComponent[i].getObjectsByName(rtn, "led", false);
                
                System.out.println(rtn.size());
                
                jdObjLedInputComponent[i].setName(inputDevices[i]+ "/state");
                
                jdObjLedInputComponent[i].addMouseListener((JDMouseListener)this);
            }
        }
        
        ((mySynopticFileViewer)synopticWhist).parseJdrawComponents();
        ((mySynopticFileViewer)synopticWhist).startRefrecher();
    }
    
    int findIfOutputDevice(Object o){
        for(int i = 0 ; i < outputDevices.length ; i++){
            if(jdObjOutputComponent[i] == o){
                return i;
            }
        }
        return -1;
    }
    
    int findIfInputDevice(Object o){
        for(int i = 0 ; i < inputDevices.length ; i++){
            if(jdObjInputComponent[i] == o){
                return i;
            }
            if(jdObjLedInputComponent[i] == o){
                return i;
            }
        }
        return -1;
    }
    
    String findDeviceName(Object o){
        for(int i = 0 ; i < outputDevices.length ; i++){
            if(jdObjOutputComponent[i] == o){
                return outputDevices[i];
            }
            if(jdObjLedOutputComponent[i] == o){
                return outputDevices[i];
            }
        }
        for(int i = 0 ; i < inputDevices.length ; i++){
            if(jdObjInputComponent[i] == o){
                return inputDevices[i];
            }
            if(jdObjLedInputComponent[i] == o){
                return inputDevices[i];
            }
        }
        if(o == whistObj){
            return this.moduleDeviceName;
        }
        return null;
    }
    
    int findDeviceIndex(Object o){
        for(int i = 0 ; i < outputDevices.length ; i++){
            if(jdObjOutputComponent[i] == o){
                return i;
            }
            if(jdObjLedOutputComponent[i] == o){
                return i;
            }
        }
        return -1;
    }

    @Override
    public void mouseClicked(JDMouseEvent jdme) {
    }

    @Override
    public void mousePressed(JDMouseEvent jdme) {
        if(jdme.getButton() == MouseEvent.BUTTON1){
            Object obj = jdme.getSource();
            int index;
            if((index = findIfOutputDevice(obj)) != -1){
                MainPanel atk;
                if((atk = parent.atkPanelAlreadyStarted(outputDevices[index])) != null){
                    atk.dispose();
                    atk = null;
                    parent.atkPanelList.remove(atk);
                }
                atk = new atkpanel.MainPanel(outputDevices[index], false);
                atk.setName(outputDevices[index]);
                atk.setExpertView(parent.expertViewAtkpanel);
                parent.atkPanelList.add(atk);
            }
            if((index = findIfInputDevice(obj)) != -1){
                MainPanel atk;
                if((atk = parent.atkPanelAlreadyStarted(inputDevices[index])) != null){
                    atk.dispose();
                    atk = null;
                    parent.atkPanelList.remove(atk);
                }
                atk = new atkpanel.MainPanel(inputDevices[index], false);
                atk.setName(inputDevices[index]);
                atk.setExpertView(parent.expertViewAtkpanel);
                parent.atkPanelList.add(atk);
            }
        }else{
            Object obj = jdme.getSource();
            lastClickedDevice = findDeviceName(obj);
            int outputNumber = findDeviceIndex(obj) + 1;
                        
            JPopupMenu menu = new JPopupMenu();
            JMenuItem menuOutputConf = new JMenuItem("Configure output");
            menuOutputConf.addMouseListener(new MouseAdapter() {

                @Override
                public void mousePressed(MouseEvent e) {
                    if(configOut == null){
                        try{
                            configOut = new ConfigOutputFrame(thisIs,moduleDeviceName, false);
                            configOut.readConfigureFromDatabase(lastClickedDevice);
                            configOut.setLocationRelativeTo(thisIs);
                            configOut.setVisible(true);
                        }catch(DevFailed ex){
                            configOut.dispose();
                            ErrorPane.showErrorMessage(thisIs, moduleDeviceName, ex);
                        }
                    }
                }    
            });
            
            JMenuItem menuInputConf = new JMenuItem("Configure input");
            menuInputConf.addMouseListener(new MouseAdapter() {

                @Override
                public void mousePressed(MouseEvent e) {
                    if(configIn == null){
                        try{
                            configIn = new ConfigInputFrame(thisIs,moduleDeviceName, false);
                            configIn.readConfigureFromDatabase(lastClickedDevice);
                            configIn.setLocationRelativeTo(thisIs);
                            configIn.setVisible(true);
                        }catch(DevFailed ex){
                            configIn = null;
                            ErrorPane.showErrorMessage(thisIs, moduleDeviceName, ex);
                        }
                    }
                }    
            });
            
            JMenuItem menuJive = new JMenuItem("Configure With Jive");
            
            menuJive.addMouseListener(new MouseAdapter() {
                
                @Override
                public void mousePressed(MouseEvent e) {
                    if(jive == null){
                        jive = new jive3.MainPanel();
                    }
                    jive.setVisible(true);
                    jive.toFront();
                    jive.goToDeviceNode(lastClickedDevice);
                }    
            });
            if(!lastClickedDevice.equals(moduleDeviceName)){
                boolean isInput = (findIfInputDevice(obj) != -1);
                if(!isInput){
                    menu.add(menuOutputConf);
                }else{
                    menu.add(menuInputConf);
                }
            }
            menu.add(menuJive);
            if(outputNumber >= 1 && outputNumber <= 3){
                JMenuItem menuHelp = new JMenuItem("Help for fine delay");
                menuHelp.addMouseListener(new MouseAdapter() {
                
                @Override
                public void mousePressed(MouseEvent e) {
                    if(helpFineDelay == null){
                        helpFineDelay = new HelpFineDelay();
                        helpFineDelay.setLocationRelativeTo(thisIs.rootPane);
                    }
                    helpFineDelay.setVisible(true);
                }    
            });
                menu.add(menuHelp);
            }
            menu.show(this.synopticWhist, jdme.getX(), jdme.getY());
        }
    }

    @Override
    public void mouseReleased(JDMouseEvent jdme) {
    }

    @Override
    public void mouseEntered(JDMouseEvent jdme) {
        setCursor(WhistConstants.handCursor);
        
        Object obj = jdme.getSource();
        int index;
        if((index = findIfOutputDevice(obj)) != -1){
            synopticWhist.setToolTipText(outputDevices[index]);        
        }
        if((index = findIfInputDevice(obj)) != -1){
            synopticWhist.setToolTipText(inputDevices[index]);        
        }
        //setBackground(WhistConstants.myLigthGrayTRANSLUCENT);
    }

    @Override
    public void mouseExited(JDMouseEvent jdme) {
        setCursor(WhistConstants.defaultCursor);
        synopticWhist.setToolTipText(null);
        //setBackground(WhistConstants.TRANSLUCENT);
    }
}
