package jtiming;

import atkpanel.MainPanel;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.esrf.TangoApi.DbDatum;
import fr.esrf.TangoApi.DeviceAttribute;
import fr.esrf.TangoApi.DeviceData;
import fr.esrf.TangoApi.DeviceProxy;
import fr.esrf.tangoatk.core.*;
import fr.esrf.tangoatk.widget.util.ATKConstant;
import fr.esrf.tangoatk.widget.util.ErrorPane;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.border.Border;
import javax.tools.Diagnostic;

/**
 * A panel to show steerer states
 */
class SingleWhistPanel extends JPanel implements ActionListener {

    private JPanel stPanel;
    private JButton stState;
    String devName = null;
    String simpleName = null;
    String conf = null;
    WhistsPane parentComponent;
    int index = -1;
    SingleWhistPanel thisIs;

    SingleWhistPanel(WhistsPane parentComponent, int index) {
        thisIs=this;
        this.parentComponent = parentComponent;
        this.index = index;
        this.setPreferredSize(new Dimension(0, 35));
        setLayout(new BorderLayout());
        setBorder(BorderFactory.createEtchedBorder());

        stPanel = new JPanel();
        stPanel.setLayout(new BorderLayout());
        stPanel.setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));
        add(stPanel, BorderLayout.CENTER);

        stState = new JButton();
        stState.setFont(ATKConstant.labelFont);
        stState.setMargin(new Insets(0, 0, 0, 0));
        stState.addActionListener((ActionListener) this);
        stState.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e); //To change body of generated methods, choose Tools | Templates.
                if (e.getButton() == MouseEvent.BUTTON3) {
                    JPopupMenu menu = new JPopupMenu();

                    if (!simpleName.equalsIgnoreCase("master")) {

                        JMenuItem menuSync = new JMenuItem("Synchronize");
                        menuSync.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                try {
                                    DeviceProxy dp = new DeviceProxy(devName);

                                    dp.command_inout("synchronize");
                                } catch (DevFailed ex) {
                                    ErrorPane.showErrorMessage(parentComponent.parentFrame, devName, ex);
                                }
                            }
                        });

                        menu.add(menuSync);

                        JMenuItem menuDisableEnable;
                        if (conf.compareToIgnoreCase("Disable") == 0) {
                            menuDisableEnable = new JMenuItem("Enable");
                        } else {
                            menuDisableEnable = new JMenuItem("Disable");
                        }

                        menuDisableEnable.addActionListener(thisIs);

                        menu.add(menuDisableEnable);

                    }

                    JMenuItem menuAtk = new JMenuItem("Atkpanel");
                    menuAtk.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            MainPanel atk = new atkpanel.MainPanel(devName, false);
                            atk.setExpertView(parentComponent.parentFrame.expertViewAtkpanel);
                        }
                    });

                    menu.add(menuAtk);
                    menu.show(stState, e.getX(), e.getY());

                }
            }
        });
        stState.setBackground(ATKConstant.getColor4State(IDevice.UNKNOWN));
        stPanel.add(stState, BorderLayout.CENTER);
    }

    String[] addToStartArray(String[] array, String toAdd) {
        String[] newArray = new String[array.length + 1];
        newArray[0] = toAdd;
        System.arraycopy(array, 0, newArray, 1, array.length);
        return newArray;
    }

    String[] addToEndArray(String[] array, String toAdd) {
        String[] newArray = new String[array.length + 1];
        newArray[array.length] = toAdd;
        System.arraycopy(array, 0, newArray, 0, array.length);
        return newArray;
    }

    String[] removeFormArray(String[] array, String toRemove) {
        String[] newArray = new String[array.length - 1];
        int offset = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i].equalsIgnoreCase(toRemove)) {
                offset = -1;
            } else {
                newArray[i + offset] = array[i];
            }
        }
        return newArray;
    }

    void updateState(String state) {
        stState.setBackground(ATKConstant.getColor4State(state));
    }

    void updateDeviceName(String text) {
        devName = text;
        simpleName = WhistConstants.extractMember(text);
        stState.setText(simpleName);
    }

    void updateConf(String conf) {
        this.conf = conf;
        if (conf.equalsIgnoreCase("Critical")) {
            stState.setFont(ATKConstant.labelFont.deriveFont(Font.BOLD));
            stPanel.setBackground(Color.LIGHT_GRAY);
        } else if (conf.equalsIgnoreCase("NotCritical")
                || conf.equalsIgnoreCase("Disable")) {
            stState.setFont(ATKConstant.labelFont);
            stPanel.setBackground(Color.GRAY);
            if(conf.equalsIgnoreCase("NotCritical")){
                conf = "Not Critical";
            }
        }
        stState.setToolTipText(simpleName + " is " + conf + " module");
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource() == stState) {
            if (devName != null) {
                try {
                    parentComponent.whistFrame.setModuleDevice(devName);
                    parentComponent.whistFrame.setLocationRelativeTo(parentComponent.whistFrame);
                    parentComponent.whistFrame.setVisible(true);
                    //MainPanel atk = new atkpanel.MainPanel(devName, false);
                } catch (DevFailed ex) {
                    Logger.getLogger(SingleWhistPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } else {
            String deviceSourcePropertie;
            String deviceDestinationPropertie;
            if(((JMenuItem) evt.getSource()).getText().equalsIgnoreCase("Disable")){
                deviceSourcePropertie = "modulesList";
                deviceDestinationPropertie = "disableModulesList";
            }else{
                deviceSourcePropertie = "disableModulesList";
                deviceDestinationPropertie = "modulesList";
            }
            try {
                Database db = ApiUtil.get_db_obj();

                String[] sourceList = db.get_device_property(WhistConstants.DEV_NETWORK, deviceSourcePropertie).extractStringArray();
                String[] destList = db.get_device_property(WhistConstants.DEV_NETWORK, deviceDestinationPropertie).extractStringArray();

                if (sourceList[0].isEmpty()) {
                    sourceList = new String[0];
                }
                if (destList[0].isEmpty()) {
                    destList = new String[0];
                }
                
                String[] newSourceList = removeFormArray(sourceList,devName);
                String[] newDestList = addToEndArray(destList,devName);
                
                DbDatum[] properties = new DbDatum[2];
                properties[0] = new DbDatum(deviceSourcePropertie, newSourceList);
                properties[1] = new DbDatum(deviceDestinationPropertie, newDestList);
                db.put_device_property(WhistConstants.DEV_NETWORK, properties);

                db.get_device_info(WhistConstants.DEV_NETWORK);
                //restart Network Device
                String devAdmin = "dserver/" + db.get_device_info(WhistConstants.DEV_NETWORK).server;
                DeviceData dd = new DeviceData();
                dd.insert(WhistConstants.DEV_NETWORK);
                new DeviceProxy(devAdmin).command_inout("DevRestart", dd);
            } catch (DevFailed ex) {
                ErrorPane.showErrorMessage(parentComponent.parentFrame, devName, ex);
            }
        }
    }

}

public class WhistsPane extends JPanel implements IDevStateSpectrumListener, IStringSpectrumListener, IEnumSpectrumListener {

    final static int MAX_ROW_NUMBER = 8;
    final static int MAX_COLUMN_NUMBER = 3;

    private AttributeList attList;

    private JPanel stPanel;
    private ArrayList<SingleWhistPanel> stPanels;

    private IDevStateSpectrum stModel = null;
    private IStringSpectrum devNameModel = null;
    private IEnumSpectrum confModel = null;
    GridLayout mainGrid;
    MainFrame parentFrame;

    WhistFrame whistFrame;

    WhistsPane(MainFrame parentFrame) {
        this.parentFrame = parentFrame;

        attList = new AttributeList();
        attList.addErrorListener(((MainFrame) parentFrame).errh);

        setLayout(new BorderLayout());

        stPanel = new JPanel();
        mainGrid = new GridLayout(1, 1);
        stPanel.setLayout(mainGrid);

        add(stPanel, BorderLayout.CENTER);

        stPanels = new ArrayList<>();

        try {

            stModel = (IDevStateSpectrum) attList.add(WhistConstants.DEV_NETWORK + "/modulesState");
            stModel.addDevStateSpectrumListener((IDevStateSpectrumListener) this);

            devNameModel = (IStringSpectrum) attList.add(WhistConstants.DEV_NETWORK + "/modulesList");
            devNameModel.addListener((IStringSpectrumListener) this);

            confModel = (IEnumSpectrum) attList.add(WhistConstants.DEV_NETWORK + "/modulesConf");
            confModel.addEnumSpectrumListener((IEnumSpectrumListener) this);

        } catch (ConnectionException e) {
            e.printStackTrace();
        }

        attList.setRefreshInterval(1000);
        attList.startRefresher();
        
        whistFrame = new WhistFrame(this.parentFrame);

    }

    @Override
    public void devStateSpectrumChange(DevStateSpectrumEvent evt) {
        Object src = evt.getSource();

        if (src == stModel) {
            String[] st = evt.getValue();
            int delta = st.length - stPanels.size();
            if (delta != 0) {
                adaptePanels(delta);
            }

            for (int i = 0; i < stPanels.size(); i++) {
                stPanels.get(i).updateState(st[i]);
            }
        }

    }

    @Override
    public void stateChange(AttributeStateEvent evt) {
    }

    @Override
    public void errorChange(ErrorEvent evt) {

        Object src = evt.getSource();

        if (src == stModel) {
            for (int i = 0; i < stPanels.size(); i++) {
                stPanels.get(i).updateState(IDevice.UNKNOWN);
            }
        }
    }

    public void clearModel() {

        if (stModel != null) {
            stModel.removeDevStateSpectrumListener(this);
        }
        stModel = null;

        //attList.removeErrorListener(MainPanel.errWin);
        attList.stopRefresher();
        attList = null;
        setVisible(false);

    }

    public void startRefresher() {
        attList.startRefresher();
    }

    public void stopRefresher() {
        attList.stopRefresher();
    }

    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {

                MainFrame f = new MainFrame();
                WhistsPane st = new WhistsPane(f);
                st.startRefresher();
                f.setContentPane(st);
                f.pack();
                f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                f.setVisible(true);
            }
        });
    }

    private void adaptePanels(int delta) {
        int oldSize = stPanels.size();
        /**/

        DeviceProxy ds;
        try {
            ds = new DeviceProxy(WhistConstants.DEV_NETWORK);

            DeviceAttribute attData = ds.read_attribute("modulesList");
            String[] names = attData.extractStringArray();

            if (names.length < MAX_ROW_NUMBER) {
                mainGrid.setRows(names.length);
            } else {
                int nbColumn = (names.length / MAX_ROW_NUMBER) + 1;
                int nbRow = MAX_ROW_NUMBER;
                if (nbColumn > MAX_COLUMN_NUMBER) {
                    nbColumn = MAX_COLUMN_NUMBER;
                    nbRow = (names.length / MAX_COLUMN_NUMBER) + 1;
                }
                mainGrid.setRows(nbRow);
                mainGrid.setColumns(nbColumn);
            }

            /**/
            if (delta > 0) {
                for (int i = 0; i < delta; i++) {
                    stPanels.add(new SingleWhistPanel(this, i));
                    stPanel.add(stPanels.get(i + oldSize));
                }
            } else if (delta < 0) {
                delta = Math.abs(delta);
                for (int i = 1; i < delta + 1; i++) {
                    stPanel.remove(stPanels.get(oldSize - i));
                    stPanels.remove(stPanels.get(oldSize - i));
                }
            }
            parentFrame.pack();

        } catch (DevFailed ex) {
            Logger.getLogger(WhistsPane.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getState(int index) {
        stModel.refresh();
        return stModel.getValue()[index];
    }

    @Override
    public void stringSpectrumChange(StringSpectrumEvent sse) {
        Object src = sse.getSource();

        if (src == devNameModel) {
            String[] devName = sse.getValue();
            int delta = devName.length - stPanels.size();
            if (delta != 0) {
                adaptePanels(delta);
            }

            for (int i = 0; i < stPanels.size(); i++) {
                stPanels.get(i).updateDeviceName(devName[i]);
            }
        }

    }

    @Override
    public void enumSpectrumChange(EnumSpectrumEvent e) {
        Object src = e.getSource();

        if (src == confModel) {
            String[] conf = confModel.getEnumSpectrumValue();
            if(conf != null){
                int delta = conf.length - stPanels.size();
                if (delta != 0) {
                    adaptePanels(delta);
                }

                for (int i = 0; i < stPanels.size(); i++) {
                    stPanels.get(i).updateConf(conf[i]);
                }
            }
        }
    }
}
