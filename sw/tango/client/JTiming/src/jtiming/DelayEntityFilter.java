/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jtiming;

import fr.esrf.tangoatk.core.IEntity;
import fr.esrf.tangoatk.core.IEntityFilter;
import fr.esrf.tangoatk.core.INumberScalar;

/**
 *
 * @author tappret
 */
public class DelayEntityFilter implements IEntityFilter {
    @Override
    public boolean keep(IEntity ie) {
        if(ie.isOperator() && (ie instanceof INumberScalar)){
//            String dev = ie.getDevice().getName().toLowerCase();
//            if(dev.equals(WhistConstants.DEV_BUNCHCLOCK)){
//                String name = ie.getNameSansDevice().toLowerCase();
//                if( !name.contains("width")){
//                    return true;
//                }
//            }
            String name = ie.getNameSansDevice().toLowerCase();
            if( !name.contains("width") && !name.contains("phase")){
                return true;
            }
        }
        return false;
    }
}
