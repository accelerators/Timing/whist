//
// Created by tappret on 23/07/2020.
//

#include <WhistBunchClockThread.h>


namespace WhistBunchClock_ns {

    void *WhistBunchClockResetThread::run_undetached(void *ptr) {
        auto end = std::chrono::system_clock::now();
        std::time_t currtime = std::chrono::system_clock::to_time_t(end);

        string status = "Reset Starting at : ";
        status.append(std::ctime(&currtime));
        
        try {
            updateStatus(status, Tango::MOVING);
            cout << "WhistBunchClock::reset -- " << std::ctime(&currtime) << " -- writeT0Enable(false)" << endl ;
            
            //implement write rtd3s (pll) line 549 whtst.py
            status += "\nT0 Pause";
            updateStatus(status);
            parent->thisDp->command_inout("Off");

            // step 1 Disable Sequencer and other
            status += "\nDisable all Sequencer";
            updateStatus(status);
            uint32_t ctrlReg = parent->readMasterRegister(BUCLK_REG_CTRL);
            parent->writeAll(BUCLK_REG_CTRL, (ctrlReg & ~(BUCLK_CTRL_T0WDEN | BUCLK_CTRL_GUNEN | BUCLK_CTRL_RFEN | BUCLK_CTRL_BUNCHEN)));
            

            //step 2 configure Softcores
            status += "\nRestarting All SoftCore";
            updateStatus(status);
            parent->networkDp->command_inout("SoftcoreReset");

            status += "\nWaiting RfOverEthernet is Down (Fault)";
            updateStatus(status);
            WhistUtil::waitState((*parent->networkDp), "RfOverEthernetMaster", Tango::FAULT);

            //Configure (RFoWR) rtd3s SoftCore 0
            //On Master
            status += "\nConfigure RfOverEthernet on Master";
            updateStatus(status);
            vector<Tango::DevULong> addrData;
            addrData = {WhistUtil::SOFTCORE_D3S, WhistUtil::SOFTCORE_STREAM_CONF, 0, WhistUtil::SOFTCORE_MASTER_MODE
                , WhistUtil::SOFTCORE_STREAM_ID, WhistUtil::SOFTCORE_RF_MSB, WhistUtil::SOFTCORE_RF_LSB};
            parent->writeMasterSoftcore(addrData);

            //Wait RFoE OK
            status += "\nWaiting RfOverEthernet is ON on Master";
            updateStatus(status);
            WhistUtil::waitState((*parent->networkDp), "RfOverEthernetMaster", Tango::ON);

            //Configure (RFoWR) rtd3s SoftCore 0
            //On Slaves
            status += "\nConfigure RfOverEthernet on Slaves";
            updateStatus(status);
            addrData = {WhistUtil::SOFTCORE_D3S, WhistUtil::SOFTCORE_STREAM_CONF, 0
                , WhistUtil::SOFTCORE_SLAVE_MODE, WhistUtil::SOFTCORE_STREAM_ID, 0, 0};
            parent->writeSlavesSoftcore(addrData);
            //Wait RFoE OK
            status += "\nWaiting RfOverEthernet is ON on All Whist";
            updateStatus(status);
            WhistUtil::waitState((*parent->networkDp), "RfOverEthernetSlaves", Tango::ON);

            //Configure bcc SoftCore 1
            //On Master
            status += "\nConfigure BCC Softcore on Master";
            updateStatus(status);
            addrData = {WhistUtil::SOFTCORE_BCC, BCC_CMD_MODE, BCC_MASTER_MODE};
            parent->writeMasterSoftcore(addrData);
            addrData = {WhistUtil::SOFTCORE_BCC, BCC_MASTER_CMD_SEQ_EN, 1, 0};
            parent->writeMasterSoftcore(addrData);
            addrData = {WhistUtil::SOFTCORE_BCC, BCC_CMD_ENABLE};
            parent->writeMasterSoftcore(addrData);

            //Configure bcc SoftCore 1
            //Slaves
            status += "\nConfigure BCC Softcore on Slaves";
            updateStatus(status);
            addrData = {WhistUtil::SOFTCORE_BCC, BCC_CMD_MODE, BCC_SLAVE_MODE};
            parent->writeSlavesSoftcore(addrData);
            addrData = {WhistUtil::SOFTCORE_BCC, BCC_CMD_ENABLE};
            parent->writeSlavesSoftcore(addrData);

            //configure evt SoftCore 2
            //Enable Evt Softcore
            status += "\nConfigure Event Softcore";
            updateStatus(status);
            addrData = {WhistUtil::SOFTCORE_EVT, EVT_CMD_ENABLE, EVT_T0_ENABLE};
            //send on all Softcore slave and master
            parent->writeMasterSoftcore(addrData);
            parent->writeSlavesSoftcore(addrData);
            //set Slave mode
            addrData = {WhistUtil::SOFTCORE_EVT, EVT_CMD_MODE, EVT_SLAVE_MODE};
            //send on all Softcore slave and master
            parent->writeMasterSoftcore(addrData);
            parent->writeSlavesSoftcore(addrData);
            
            // step 3 configure the all sequencer
            status += "\nSet all Main Delay(Tinj, Gun, Text)";
            updateStatus(status);
            parent->thisDp->command_inout("updateTheMainDelays");
            
            // step 4 Enable Sequencer and other
            status += "\nEnable Sequencer";
            updateStatus(status);
            parent->writeAll(BUCLK_REG_CTRL, (ctrlReg | BUCLK_CTRL_T0WDEN | BUCLK_CTRL_GUNEN | BUCLK_CTRL_RFEN | BUCLK_CTRL_BUNCHEN));

            // step 5 Master Start SoftCore
            status += "\nMaster Start";
            updateStatus(status);
            addrData = {1, BCC_MASTER_CMD_START};
            parent->writeMasterSoftcore(addrData);
            //wait next pps
            sleep(1);

            status += "\nT0 Resuming";
            updateStatus(status);
            parent->thisDp->command_inout("On");

            parent->updateStampMasterStart();

            //enable pps Survey
            status += "\nEnable Hardware PPS Survey";
            updateStatus(status);
            uint32_t csr0 = parent->readMasterRegister(BUCLK_REG_CSR0);
            parent->writeAll(BUCLK_REG_CSR0, (csr0 | BUCLK_CSR0_RFADJSTOP | BUCLK_CSR0_RFDBGEN));

            status += "\nForce Outputs & Inputs Configuration";
            updateStatus(status);
            parent->networkDp->command_inout("forceConfigAllInOut");
            
            status += "\nRESET SUCCESSFUL COMPLET";
            updateStatus(status,Tango::OFF);

            cout << "RESET SUCCESSFUL !!" << endl;
        } catch (Tango::DevFailed &ex) {
            
            stringstream ss;
            ss << ex.errors[0].desc;
            status += "\nERROR !!!!!!!!!!!!!!!!!";
            status += "\n" + ss.str();
            updateStatus(status, Tango::FAULT);
            Tango::Except::print_exception(ex);
        }

        return nullptr;
    }
    
    void WhistBunchClockResetThread::updateStatus(string status) {
        parent->get_dev_monitor().get_monitor();
        Tango::string_free(*parent->attr_StatusThread_read);
        (*parent->attr_StatusThread_read) = Tango::string_dup(status);
        parent->get_dev_monitor().rel_monitor();
    }

    void WhistBunchClockResetThread::updateStatus(string status, Tango::DevState state) {
        updateStatus(status);
        parent->get_dev_monitor().get_monitor();
        (*parent->attr_StateThread_read) = state;
        parent->get_dev_monitor().rel_monitor();
    }

}