//
// Created by tappret on 7/13/18.
//

#ifndef WHISTMODULE_WHISTMODULETHREAD_H
#define WHISTMODULE_WHISTMODULETHREAD_H

#include <tango.h>
#include <WhistBunchClock.h>
#include <WhistBunchClockClass.h>

namespace WhistBunchClock_ns {

    class WhistBunchClockResetThread : public omni_thread{
        private:
            WhistBunchClock *parent;
            Tango::DevState oldState;

        public:
            explicit WhistBunchClockResetThread(WhistBunchClock *parent) {
                this->parent = parent;
                oldState = parent->get_state();
            };

            void start() {
                start_undetached();
            }

        private:
            void *run_undetached(void *ptr);
            void updateStatus(string status);
            void updateStatus(string status,Tango::DevState state);
    };
}



#endif //WHISTMODULE_WHISTMODULETHREAD_H
