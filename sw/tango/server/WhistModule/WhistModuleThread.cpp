//
// Created by tappret on 7/13/18.
//

#include <WhistModuleThread.h>


namespace WhistModule_ns {

    WhistModuleThread::WhistModuleThread(WhistModule *parent) {
        this->parent = parent;
        needStop = false;
        name = parent->get_name();
        refreshTime = parent->refreshTime;

        //copy group list to improve Thread parallelization
        devicesNameOutputs = parent->outputDevice->get_device_list();

        devicesNameInputs = parent->inputDevice->get_device_list();
    }

    WhistModuleThread::~WhistModuleThread() {
        delete outputDevice;
        delete inputDevice;
    }

    void *WhistModuleThread::run_undetached(void *ptr) {
        //long start = WhistUtil::timeMS();
        cout << name << " Thread starting" << endl;

        //create group Copy
        outputDevice = new Tango::Group("outputs");
        outputDevice->add(devicesNameOutputs, 750);
        sizeGroupOutput = outputDevice->get_size();

        inputDevice = new Tango::Group("inputs");
        inputDevice->add(devicesNameInputs, 750);
        sizeGroupInput = inputDevice->get_size();

        while (!threadNeedStop()) {
            long start = WhistUtil::timeMS();

            vector<string> attrsOutputToRead = {"state", "isFastTiming", "parentSignal"};
            long idAttrOut = outputDevice->read_attributes_asynch(attrsOutputToRead);

            vector<string> attrsInputToRead = {"state", "inputMode"};
            long idAttrIn = inputDevice->read_attributes_asynch(attrsInputToRead);

            //working
            try {
                unsigned long nbAttrs = attrsOutputToRead.size();
                Tango::GroupAttrReplyList garl = outputDevice->read_attributes_reply(idAttrOut, 750);

                //cout << "Time to aquired data " << (WhistUtil::timeMS() - start) << " ms" << endl;
                vector<Tango::DevState> outputStates;
                vector<Tango::DevBoolean> outputIsFast;
                vector<string> outputParentSignal;
                outputStates.resize(sizeGroupOutput);
                outputIsFast.resize(sizeGroupOutput);
                outputParentSignal.resize(sizeGroupOutput);

                //extract data form GroupAttrReplyList
                for (unsigned int i = 0; i < sizeGroupOutput; i++) {
                    for (unsigned int j = 0; j < nbAttrs; j++) {
                        if (j == 0) {
                            if (!garl[i * nbAttrs + j].has_failed()) {
                                garl[i * nbAttrs + j].get_data() >> outputStates[i];
                            } else {
                                outputStates[i] = Tango::UNKNOWN;
                            }
                        } else if (j == 1) {
                            if (!garl[i * nbAttrs + j].has_failed()) {
                                //Tango::DevBoolean * isFast = new Tango::DevBoolean();
                                bool isFast;
                                garl[i * nbAttrs + j].get_data() >> isFast;
                                outputIsFast[i] = (Tango::DevBoolean)isFast;
                            } else {
                                outputIsFast[i] = false;
                            }
                        } else if (j == 2) {
                            if (!garl[i * nbAttrs + j].has_failed()) {
                                garl[i * nbAttrs + j].get_data() >> outputParentSignal[i];
                            } else {
                                outputParentSignal[i] = "UNKNOWN";
                            }
                        }
                    }
                }

                //don't do this if you need exit Thread
                if (!threadNeedStop()) {
                    //get Monitor Device
                    WhistModuleThreadGuard guard(shardData);

                    for (unsigned int i = 0; i < sizeGroupOutput; i++) {
                        *(parent->outputStatesForGrp[i]) = outputStates[i];
                        *(parent->outputIsFastForGrp[i]) = outputIsFast[i];
                        *(parent->outputParentSignalForGrp[i]) = outputParentSignal[i];
                    }
                }

            } catch (Tango::DevFailed &e) {
                Tango::Except::print_exception(e);
            }

            try {
                unsigned long nbAttrs = attrsInputToRead.size();

                Tango::GroupReply::enable_exception(true);

                Tango::GroupAttrReplyList garl = inputDevice->read_attributes_reply(idAttrIn, 750);

                //cout << "Time to aquired data " << (WhistUtil::timeMS() - start) << " ms" << endl;
                vector<Tango::DevState> inputStates;
                vector<string> inputModes;
                inputStates.resize(sizeGroupInput);
                inputModes.resize(sizeGroupInput);

                //extract data form GroupAttrReplyList
                for (unsigned int i = 0; i < sizeGroupInput; i++) {
                    for (unsigned int j = 0; j < nbAttrs; j++) {
                        if (j == 0) {
                            if (!garl[i * nbAttrs + j].has_failed()) {
                                garl[i * nbAttrs + j].get_data() >> inputStates[i];
                            } else {
                                inputStates[i] = Tango::UNKNOWN;
                            }
                        } else if (j == 1) {
                            if (!garl[i * nbAttrs + j].has_failed()) {
                                garl[i * nbAttrs + j].get_data() >> inputModes[i];
                            } else {
                                inputModes[i] = "UNKNOWN";
                            }
                        }
                    }
                }

                //don't do this if you need exit Thread
                if (!threadNeedStop()) {
                    //get Monitor Device
                    WhistModuleThreadGuard guard(shardData);

                    for (unsigned int i = 0; i < sizeGroupInput; i++) {
                        *(parent->inputStatesForGrp[i]) = inputStates[i];
                        *(parent->inputModesForGrp[i]) = inputModes[i];
                    }
                }

            } catch (Tango::DevFailed &e) {
                Tango::Except::print_exception(e);
            }

            if (!threadNeedStop()) {
                long end = WhistUtil::timeMS();

                long timeToWait = refreshTime - (end - start);
                if (timeToWait < 100) {
                    timeToWait = 1000;
                }

                usleep((__useconds_t) (timeToWait * 1000));
            }
        }

        cout << name << " Thread exiting" << endl;
        return nullptr;
    }

    void WhistModuleThread::stopRequest() {
        cout << "stopRequest" << endl;
        omni_mutex_lock synchronized(tm);
        needStop = true;
    }

    bool WhistModuleThread::threadNeedStop() {
        omni_mutex_lock synchronized(tm);
        return needStop;
    }

    void *WhistModuleSynchronizeThread::run_undetached(void *ptr) {
        try {
            parent->updateStatus("In progress", Tango::MOVING);
            //lock parent to get data
            Tango::DeviceClass *dsClass = parent->get_device_class();
            WhistModuleClass *ds_class = (dynamic_cast<WhistModuleClass *> (dsClass));
            string bunchClockDeviceName = ds_class->bunchClockDevice;

            string devName = parent->get_name();

            Tango::DeviceProxy thisDevice(devName);

            Tango::DeviceProxy bunchClockDevice(bunchClockDeviceName);

            // step 1 Disable Sequencer and other
            parent->writeBit(BUCLK_REG_CTRL, BUCLK_CTRL_T0WDEN | BUCLK_CTRL_GUNEN | BUCLK_CTRL_RFEN | BUCLK_CTRL_BUNCHEN, false);

            Tango::DeviceData dd;
            dd << (Tango::DevUShort) 0;
            thisDevice.command_inout("SoftcoreReset", dd);

            WhistUtil::waitState(thisDevice, "RfOverEthernet", Tango::FAULT);

            // step 2 Configure SoftCore 1, 2, 3
            //Configure (RFoWR) rtd3s SoftCore 0
            //On Slaves
            vector<Tango::DevULong> addrData;
            addrData = {WhistUtil::SOFTCORE_D3S, WhistUtil::SOFTCORE_STREAM_CONF, 0, WhistUtil::SOFTCORE_SLAVE_MODE,
                WhistUtil::SOFTCORE_STREAM_ID, 0, 0};
            dd << addrData;
            thisDevice.command_inout("SoftcoreWrite", dd);

            //Wait RFoE OK
            WhistUtil::waitState(thisDevice, "RfOverEthernet", Tango::ON);

            dd << (Tango::DevUShort) WhistUtil::SOFTCORE_BCC;
            thisDevice.command_inout("SoftcoreReset", dd);

            //Configure bcc SoftCore 1
            //Slaves
            addrData = {WhistUtil::SOFTCORE_BCC, BCC_CMD_MODE, BCC_SLAVE_MODE};
            dd << addrData;
            thisDevice.command_inout("SoftcoreWrite", dd);
            addrData = {WhistUtil::SOFTCORE_BCC, BCC_CMD_ENABLE};
            dd << addrData;
            thisDevice.command_inout("SoftcoreWrite", dd);

            //configure evt SoftCore 2
            //Enable Evt Softcore
            addrData = {WhistUtil::SOFTCORE_EVT, EVT_CMD_ENABLE, EVT_T0_ENABLE};
            dd << addrData;
            thisDevice.command_inout("SoftcoreWrite", dd);
            //set Slave mode
            addrData = {WhistUtil::SOFTCORE_EVT, EVT_CMD_MODE, EVT_SLAVE_MODE};
            dd << addrData;
            thisDevice.command_inout("SoftcoreWrite", dd);

            // step 3 update tinj text gun delays
            bunchClockDevice.command_inout("updateTheMainDelays");

            // step 4 Enable Sequencer and other
            parent->writeBit(BUCLK_REG_CTRL, BUCLK_CTRL_T0WDEN | BUCLK_CTRL_GUNEN | BUCLK_CTRL_RFEN | BUCLK_CTRL_BUNCHEN, true);

            usleep(3000000);

            // step 4 launch SLOK
            addrData = {WhistUtil::SOFTCORE_BCC, BCC_CMD_SLOK};
            dd << addrData;
            thisDevice.command_inout("SoftcoreWrite", dd);

            //add sleep for good tansition after slok launch
            usleep(2000000);
            parent->updateStatus("", Tango::OFF);

            //enable pps Survey
            parent->writeBit(BUCLK_REG_CSR0, BUCLK_CSR0_RFADJSTOP | BUCLK_CSR0_RFDBGEN, true);

            cout << "SLOK LAUNCH !!" << endl;

            bool whileNotOk = true;
            int cptTimeout = 0;
            do {
                Tango::DevState state = parent->get_state();
                if (oldState == Tango::MOVING && (state == Tango::ON || state == Tango::ALARM)) {
                    long start = WhistUtil::timeMS();

                    thisDevice.command_inout("forceInputsConfiguration");

                    thisDevice.command_inout("forceOutputsConfiguration");

                    cout << "time to forceInOutConfiguration=" << (WhistUtil::timeMS() - start) << endl;
                    whileNotOk = false;
                } else {
                    cptTimeout++;
                    usleep(500000);
                }
                oldState = state;

                if (cptTimeout > 360) {
                    Tango::Except::throw_exception("Timeout For ForceOutputConfiguration()"
                            , "Wait SLOK finiched and after 3 minutes it did not happen"
                            , "WhistModuleSynchronizeThread::run_undetached()");
                }

            } while (whileNotOk);

            cout << "Synchronized COMPLET !!" << endl;
        } catch (Tango::DevFailed &ex) {
            stringstream ss;
            ss << ex.errors[0].desc;
            parent->updateStatus(ss.str(), Tango::FAULT);
            Tango::Except::print_exception(ex);
        }

        return nullptr;
    }

    void *WhistModuleFlashThread::run_undetached(void *ptr) {
        try {
            if (checkWrite == ThreadAction::CHECK) {
                parent->updateStatus("Check Flash In progress", Tango::MOVING);

                bitstreamCheck();

                parent->updateStatus("CHECK BITSTREAM Complete SUCCESSFUL", Tango::ON);
            } else if (checkWrite == ThreadAction::WRITE) { //false = write bit stream
                parent->updateStatus("Programing Flash In progress", Tango::MOVING);

                bitstreamWrite();

                parent->updateStatus("FLASH Complete", Tango::ON);
            } else if (checkWrite == ThreadAction::WRITE_CHECK) {
                parent->updateStatus("Programing Flash In progress", Tango::MOVING);

                bitstreamWrite();

                parent->updateStatus("FLASH Complete, Now Check the Flash", Tango::MOVING);

                bitstreamCheck();

                parent->updateStatus("FLASH & CHECK Complete SUCCESSFUL", Tango::ON);
            }
        } catch (Tango::DevFailed &ex) {
            stringstream ss;
            if (checkWrite) {//check if true
                ss << "Check BitStream FAILED:" << ex.errors[0].desc << endl;
            } else {
                ss << "Write BitStream FAILED:" << ex.errors[0].desc << endl;
            }
            parent->updateStatus(ss.str(), Tango::FAULT);
            Tango::Except::print_exception(ex);
        }

        return nullptr;
    }

    void WhistModuleFlashThread::bitstreamCheck() {
        size_t bitstreamPathLenght = parent->bitstreamPath.length();
        string extentionFile = parent->bitstreamPath.substr(bitstreamPathLenght - 4, 4);
        extentionFile = WhistUtil::toLowerCase(extentionFile);
        if (extentionFile != ".bin") {
            Tango::Except::throw_exception(
                    (const char*) "WhistModule Error of ExtentionFile for bitstreamPath Property",
                    (const char*) "You need to set bitstreamPath to the good \"*.bin\" file",
                    (const char*) "WhistModule::BitstreamCheck()");
        }
        //	Add your own code
        FlashAccess mp32(parent->ebdev, parent->bitstreamPath, parent->flashBase, GBADDR, MBADDR);

        long tStart = WhistUtil::timeMS();
        cout << dec << "start Reading File" << endl;

        parent->readBitStreamFile();

        cout << "Reading File Complete in" << (WhistUtil::timeMS() - tStart) << "ms" << endl;

        uint8_t *bitStreamWhist;
        size_t size = parent->bitstream_size;
        bitStreamWhist = (uint8_t*) malloc(sizeof (uint8_t) * size);

        tStart = WhistUtil::timeMS();
        cout << "start Reading from Whist" << endl;

        if (!mp32.read_bitstream(bitStreamWhist, size)) {
            Tango::Except::throw_exception(
                    (const char*) "WhistModule Error",
                    (const char*) "Reading bitstream failed. Check the bitstream path in "
                    "the class property or check the communication (try CheckLink command)",
                    (const char*) "WhistModule::BitstreamCheck()");
        }

        cout << "Reading form Whist Complete in" << (WhistUtil::timeMS() - tStart) << "ms" << endl;

        tStart = WhistUtil::timeMS();
        cout << "start Compare of the two bitStream" << endl;

        for (unsigned int i = 0; i < parent->bitstream_size; i++) {
            if (parent->bitstream[i] != bitStreamWhist[i]) {
                stringstream ss;
                ss << "BitStream Check failed at index " << i << "value from Whist is 0x" << hex << bitStreamWhist[i] << "value from file is 0x" << parent->bitstream[i] << endl;

                Tango::Except::throw_exception(
                        "Bitstream Check FAILED",
                        ss.str(),
                        "WhistModule::BitstreamCheck()");
            }
        }

        cout << dec << endl;
        cout << "Compare Complete Successful in" << (WhistUtil::timeMS() - tStart) << "ms" << endl;
    }

    void WhistModuleFlashThread::bitstreamWrite() {
        size_t bitstreamPathLenght = parent->bitstreamPath.length();
        string extentionFile = parent->bitstreamPath.substr(bitstreamPathLenght - 4, 4);
        extentionFile = WhistUtil::toLowerCase(extentionFile);
        if (extentionFile != ".bin") {
            Tango::Except::throw_exception(
                    (const char*) "WhistModule Error of ExtentionFile for bitstreamPath Property",
                    (const char*) "You need to set bitstreamPath to the good \"*.bin\" file",
                    (const char*) "WhistModule::BitstreamFlash()");
        }
        //	Add your own code
        FlashAccess mp32(parent->ebdev, parent->bitstreamPath, parent->flashBase, GBADDR, MBADDR);
        if (!mp32.program_bitstream(HAS_NOT_GOLDEN, false)) {
            Tango::Except::throw_exception(
                    (const char*) "WhistModule Error",
                    (const char*) "Programing bitstream failed. Check the bitstream path in "
                    "the class property or check the communication (try CheckLink command)",
                    (const char*) "WhistModule::BitstreamFlash()");
        }
    }
}