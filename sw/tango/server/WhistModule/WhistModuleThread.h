//
// Created by tappret on 7/13/18.
//

#ifndef WHISTMODULE_WHISTMODULETHREAD_H
#define WHISTMODULE_WHISTMODULETHREAD_H

#include <tango.h>
#include <WhistModule.h>
#include <WhistModuleClass.h>

enum ThreadAction {
    CHECK,WRITE,WRITE_CHECK
};

typedef std::lock_guard<std::mutex> WhistModuleThreadGuard;

namespace WhistModule_ns {

    class WhistModuleThread : public omni_thread{
        private:
            Tango::TangoMonitor tm;
            bool needStop;
            WhistModule *parent;
            string name;
            vector<string> devicesNameOutputs;
            long refreshTime;
            Tango::Group * outputDevice;
            long sizeGroupOutput;
            
            vector<string> devicesNameInputs;
            Tango::Group * inputDevice;
            long sizeGroupInput;
            

        public:
            std::mutex shardData;
            
            WhistModuleThread(WhistModule *parent);            
            ~WhistModuleThread();

            void start() {
                start_undetached();
            }

            void stopRequest(void);

        private:
            bool threadNeedStop();

            void *run_undetached(void *ptr) override;
        };

    class WhistModuleSynchronizeThread : public omni_thread{
        private:
            WhistModule *parent;
            Tango::DevState oldState;

        public:
            explicit WhistModuleSynchronizeThread(WhistModule *parent) {
                this->parent = parent;
                this->parent->latchedAfterSyncWaitBunchList = true;
                oldState = parent->get_state();
            };

            void start() {
                start_undetached();
            }

        private:
            void *run_undetached(void *ptr);
    };
    
    class WhistModuleFlashThread : public omni_thread{
                
        private:
            WhistModule *parent;
            ThreadAction checkWrite;

        public:
            explicit WhistModuleFlashThread(WhistModule *parent, ThreadAction checkOrWrite) {
                this->parent = parent;
                this->checkWrite = checkOrWrite;
            };

            void start() {
                start_undetached();
            }

        private:
            void *run_undetached(void *ptr);
            
            void bitstreamCheck(void);
            void bitstreamWrite(void);
    };
}



#endif //WHISTMODULE_WHISTMODULETHREAD_H
