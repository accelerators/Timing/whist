#include <tango.h>

void ut_getStatus(Tango::DeviceProxy *dev)
{
  cout << dev->status();
}

Tango::DevState ut_getState(Tango::DeviceProxy *dev)
{
  Tango::DevState state = dev->state();
  if(state == Tango::ON)
  {
    cout << "Device State: ON" << endl;
  }
  else if(state == Tango::ALARM)
  {
    cout << "Device State: ALARM" << endl;
  }
  else if(state == Tango::FAULT)
  {
    cout << "Device State: FAULT" << endl;
  }
  return state;
}

bool ut_BitstreamFlash_Disconnected(Tango::DeviceProxy *dev)
{
  Tango::DeviceData din;
  Tango::DevState st;
  din << true;
  try
  {
    dev->set_timeout_millis(3600000); //3600s=1h
    dev->command_inout("BitstreamFlash",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ut_BitstreamFlash_Connected(Tango::DeviceProxy *dev)
{
  Tango::DeviceData din;
  Tango::DevState st;
  din << true;
  try
  {
    dev->set_timeout_millis(3600000); //3600s=1h
    dev->command_inout("BitstreamFlash",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    return false;
  }
  else
  {
    return true;
  }
}

bool ut_BitstreamLoad_Disconnected(Tango::DeviceProxy *dev)
{
  Tango::DevState st;
  try
  {
    dev->command_inout("BitstreamLoad");
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ut_BitstreamLoad_Connected(Tango::DeviceProxy *dev)
{
  Tango::DevState st;
  try
  {
    dev->command_inout("BitstreamLoad");
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    return false;
  }
  else
  {
    return true;
  }
}

bool ut_CheckLink_Disconnected(Tango::DeviceProxy *dev)
{
  bool linkState;
  Tango::DeviceData cmd_reply;
  try
  {
    cmd_reply = dev->command_inout("CheckLink");
    cmd_reply >> linkState;
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  ut_getState(dev);
  ut_getStatus(dev);
  if(linkState==false)
    return true;
  else
    return false;
}

bool ut_CheckLink_Connected(Tango::DeviceProxy *dev)
{
  bool linkState=false;
  Tango::DeviceData cmd_reply;
  try
  {
    cmd_reply = dev->command_inout("CheckLink");
    cmd_reply >> linkState;
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  ut_getState(dev);
  ut_getStatus(dev);
  return linkState;
}

bool ut_RegisterRW_Disconnected(Tango::DeviceProxy *dev)
{
  bool r=false,w=false;
  Tango::DevState st;
  Tango::DeviceData din;
  Tango::DevVarULongArray in;
  in.length(2);
  in[0] = 0x10000;
  in[1] = 0x3;
  din << in;
  try
  {
    dev->command_inout("RegisterWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = dev->state();
  if(st == Tango::FAULT)
  {
    w = true;
  }
  Tango::DeviceData dout,dinr;
  uint32_t data;
  dinr << (uint32_t)0x10000;
  try
  {
    dout = dev->command_inout("RegisterRead",dinr);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  dout >> data;
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    r = true;
  }
  return r&w;
}

bool ut_RegisterRW_Connected(Tango::DeviceProxy *dev)
{
  bool r=true;

  try
  {
    //test wrong input args
    cout << "Test \"RegisterWrite\" wrong input args: 1 arg, 2 requested" << endl;
    Tango::DeviceData din;
    Tango::DevVarULongArray in;
    in.length(1);
    in[0] = 0x10010;
    din << in;
    dev->command_inout("RegisterWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
    cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong input args
    cout << "Test \"RegisterWrite\" wrong input args: 3 args, 2 requested" << endl;
    Tango::DeviceData din;
    Tango::DevVarULongArray in;
    in.length(3);
    in[0] = 0x10010;
    in[1] = 0xdeadbeef;
    in[2] = 0xcafefade;
    din << in;
    dev->command_inout("RegisterWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  //test success
  cout << "Test success..." << endl;
  Tango::DeviceData din;
  Tango::DevVarULongArray in;
  in.length(2);
  in[0] = 0x10010;
  in[1] = 0xdeadbeef;
  din << in;
  try
  {
    dev->command_inout("RegisterWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  Tango::DeviceData dout,dinr;
  Tango::DevState st;
  uint32_t data;
  dinr << (uint32_t)0x10010;
  try
  {
    dout = dev->command_inout("RegisterRead",dinr);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  dout >> data;
  cout << hex << data << endl;
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    r = false;
  }
  if(data != 0xdeadbeef)
  {
    r = false;
  }
  return r;
}

bool ut_RegisterRWblock_Disconnected(Tango::DeviceProxy *dev)
{
  bool r=false,w=false;
  Tango::DevState st;
  Tango::DeviceData din;
  Tango::DevVarULongArray in;
  in.length(11);
  in[0] = 0x18000;
  for(int i=1;i<11;i++)
  {
    in[i] = i-1;
  }
  din << in;
  try
  {
    dev->command_inout("RegisterWriteBlock",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = dev->state();
  if(st == Tango::FAULT)
  {
    w = true;
  }
  Tango::DeviceData dout;
  in.length(2);
  in[0] = 0x18000;
  in[1] = 10;
  din << in;
  try
  {
    dout = dev->command_inout("RegisterReadBlock",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    r = true;
  }
  return r&w;
}

bool ut_RegisterRWblock_Connected(Tango::DeviceProxy *dev)
{
  bool r=true,w=true;
  Tango::DevState st;

  try
  {
    //test wrong input args
    cout << "Test \"RegisterWriteBlock\" wrong input args: 1 arg, at least 2 requested" << endl;
    Tango::DeviceData din;
    Tango::DeviceData dout;
    Tango::DevVarULongArray in;
    in.length(1);
    in[0] = 0x18000;
    din << in;
    dev->command_inout("RegisterWriteBlock",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong input args
    cout << "Test \"RegisterReadBlock\" wrong input args: 3 args, 2 requested" << endl;
    Tango::DeviceData din;
    Tango::DeviceData dout;
    Tango::DevVarULongArray in;
    in.length(3);
    in[0] = 0x18000;
    in[1] = 10;
    in[2] = 11;
    din << in;
    dout = dev->command_inout("RegisterReadBlock",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  //test success
  cout << "Test success..." << endl;
  Tango::DeviceData din;
  Tango::DeviceData dout;
  Tango::DevVarULongArray in;
  in.length(8193);
  in[0] = 0x18000;
  for(int i=1;i<8193;i++)
  {
    in[i] = i-1;
  }
  din << in;
  try
  {
    dev->command_inout("RegisterWriteBlock",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = dev->state();
  if(st == Tango::FAULT)
  {
    w = false;
  }
  in.length(2);
  in[0] = 0x18000;
  in[1] = 8192;
  din << in;
  try
  {
    dout = dev->command_inout("RegisterReadBlock",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    r = false;
  }
  vector<Tango::DevULong> data(8192);
  dout >> data;
  for(int i=0;i<8192;i++)
  {
    if(data[i] != i)
    {
  	r = false;
  	break;
    }
  }
  return r&w;
}

bool ut_SoftcoreLoad_Disconnected(Tango::DeviceProxy *dev)
{
  bool ans=false;
  Tango::DevState st;
  Tango::DeviceData din;
  Tango::DevVarLongStringArray in;
  in.lvalue.length(1);
  in.lvalue[0] = 0x1;
  in.svalue.length(1);
  in.svalue[0] = CORBA::string_dup("/segfs/linux/timing/whist/labo/bin/rt-bcc.bin");
  din << in;
  try
  {
    dev->command_inout("SoftcoreLoad",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    ans = true;
  }
  return ans;
}

bool ut_SoftcoreLoad_Connected(Tango::DeviceProxy *dev)
{
  try
  {
    //test wrong input args
    cout << "Test \"SoftcoreLoad\" wrong input args: no index, 1 filename" << endl;
    Tango::DeviceData din;
    Tango::DevVarLongStringArray in;
    in.lvalue.length(0);
    in.svalue.length(1);
    in.svalue[0] = CORBA::string_dup("/segfs/linux/timing/whist/labo/bin/rt-bcc.bin");
    din << in;
    dev->command_inout("SoftcoreLoad",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong input args
    cout << "Test \"SoftcoreLoad\" wrong input args: 1 index, no filename" << endl;
    Tango::DeviceData din;
    Tango::DevVarLongStringArray in;
    in.lvalue.length(1);
    in.lvalue[0] = 1;
    in.svalue.length(0);
    din << in;
    dev->command_inout("SoftcoreLoad",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong input args
    cout << "Test \"SoftcoreLoad\" wrong input args: 2 index, 1 filename" << endl;
    Tango::DeviceData din;
    Tango::DevVarLongStringArray in;
    in.lvalue.length(2);
    in.lvalue[0] = 1;
    in.lvalue[1] = 2;
    in.svalue.length(1);
    in.svalue[0] = CORBA::string_dup("/segfs/linux/timing/whist/labo/bin/rt-bcc.bin");
    din << in;
    dev->command_inout("SoftcoreLoad",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong input args
    cout << "Test \"SoftcoreLoad\" wrong input args: 1 index, 2 filenames" << endl;
    Tango::DeviceData din;
    Tango::DevVarLongStringArray in;
    in.lvalue.length(1);
    in.lvalue[0] = 1;
    in.svalue.length(2);
    in.svalue[0] = CORBA::string_dup("/segfs/linux/timing/whist/labo/bin/rt-bcc.bin");
    in.svalue[1] = CORBA::string_dup("test.bin");
    din << in;
    dev->command_inout("SoftcoreLoad",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong Softcore index
    cout << "Test \"SoftcoreLoad\" wrong index arg" << endl;
    Tango::DeviceData din;
    Tango::DevVarLongStringArray in;
    in.lvalue.length(1);
    in.lvalue[0] = 3;
    in.svalue.length(1);
    in.svalue[0] = CORBA::string_dup("/segfs/linux/timing/whist/labo/bin/rt-bcc.bin");
    din << in;
    dev->command_inout("SoftcoreLoad",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong filename
    cout << "Test \"SoftcoreLoad\" wrong filename arg" << endl;
    Tango::DeviceData din;
    Tango::DevVarLongStringArray in;
    in.lvalue.length(1);
    in.lvalue[0] = 1;
    in.svalue.length(1);
    in.svalue[0] = CORBA::string_dup("/segfs/linux/timing/whist/labo/bin/dummy.bin");
    din << in;
    dev->command_inout("SoftcoreLoad",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  //Test success
  cout << "Test success..." << endl;
  bool ans=false;
  Tango::DevState st;
  Tango::DeviceData din;
  Tango::DevVarLongStringArray in;
  in.lvalue.length(1);
  in.lvalue[0] = 0x1;
  in.svalue.length(1);
  in.svalue[0] = CORBA::string_dup("/segfs/linux/timing/whist/labo/bin/rt-bcc.bin");
  din << in;
  try
  {
    dev->set_timeout_millis(10000);
    dev->command_inout("SoftcoreLoad",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::ON)
  {
    ans = true;
  }
  return ans;
}

bool ut_SoftcoreReset_Disconnected(Tango::DeviceProxy *dev)
{
  bool ans=false;
  Tango::DevState st;
  Tango::DeviceData din;
  din << (Tango::DevUShort)0x1;
  try
  {
    dev->command_inout("SoftcoreReset",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    ans = true;
  }
  return ans;
}

bool ut_SoftcoreReset_Connected(Tango::DeviceProxy *dev)
{
  try
  {
    //test wrong Softcore index
    cout << "Test \"SoftcoreReset\" wrong index arg" << endl;
    Tango::DeviceData din;
    din << (Tango::DevUShort)0x3;
    dev->command_inout("SoftcoreReset",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  //Test success
  cout << "Test success..." << endl;
  bool ans=false;
  Tango::DevState st;
  Tango::DeviceData din;
  din << (Tango::DevUShort)0x1;
  try
  {
    dev->command_inout("SoftcoreReset",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::ON)
  {
    ans = true;
  }
  return ans;
}

bool ut_SoftcoreRW_Disconnected(Tango::DeviceProxy *dev)
{
  bool r=false,w=false;
  Tango::DevState st;
  Tango::DeviceData din;
  Tango::DevVarULongArray in;
  //write to CPU 1 a read request of DAC channel G
  in.length(4);
  in[0] = 0x1; //CPU index
  in[1] = 0x7; //DAC command
  in[2] = 0x0; //read
  in[3] = 0x6; //DAC channel G
  din << in;
  try
  {
    dev->command_inout("SoftcoreWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = dev->state();
  if(st == Tango::FAULT)
  {
    w = true;
  }
  Tango::DeviceData dout;
  Tango::DevVarUShortArray inr;
  //read 1 word from CPU 1 with 200ms time out
  inr.length(3);
  inr[0] = 0x1; //CPU index
  inr[1] = 0x1; //Nb words to read
  inr[2] = 200; //time out in ms
  din << inr;
  try
  {
    dout = dev->command_inout("SoftcoreRead",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    r = true;
  }
  return r&w;
}

bool ut_SoftcoreRW_Connected(Tango::DeviceProxy *dev)
{
  Tango::DevState st;

  try
  {
    //test wrong input args
    cout << "Test \"SoftcoreWrite\" wrong input args: 1 arg, at least 2 requested" << endl;
    Tango::DeviceData din;
    Tango::DevVarULongArray in;
    in.length(1);
    in[0] = 0x1;
    din << in;
    dev->command_inout("SoftcoreWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong input args
    cout << "Test \"SoftcoreRead\" wrong input args: 1 arg, 3 requested" << endl;
    Tango::DeviceData din;
    Tango::DeviceData dout;
    Tango::DevVarUShortArray in;
    in.length(1);
    in[0] = 0x1;
    din << in;
    dout = dev->command_inout("SoftcoreRead",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong input args
    cout << "Test \"SoftcoreRead\" wrong input args: 4 args, 3 requested" << endl;
    Tango::DeviceData din;
    Tango::DeviceData dout;
    Tango::DevVarUShortArray in;
    in.length(4);
    in[0] = 0x1;
    in[1] = 0x2;
    in[2] = 0x3;
    in[3] = 0x4;
    din << in;
    dout = dev->command_inout("SoftcoreRead",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong index
    cout << "Test \"SoftcoreWrite\" wrong index arg" << endl;
    Tango::DeviceData din;
    Tango::DevVarULongArray in;
    //write to CPU 3 (which doesn't exist) a read request of DAC channel G
    in.length(4);
    in[0] = 0x3; //CPU index
    in[1] = 0x7; //DAC command
    in[2] = 0x0; //read
    in[3] = 0x6; //DAC channel G
    din << in;
    dev->command_inout("SoftcoreWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test wrong index
    cout << "Test \"SoftcoreRead\" wrong index arg" << endl;
    Tango::DeviceData din;
    Tango::DeviceData dout;
    Tango::DevVarUShortArray in;
    //read 1 word from CPU 3 (which doesn't exist)  with 200ms time out
    in.length(3);
    in[0] = 0x3; //CPU index
    in[1] = 0x1; //Nb words to read
    in[2] = 200; //time out in ms
    din << in;
    dout = dev->command_inout("SoftcoreRead",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test bad CPU request (less words than requested)
    cout << "Test \"SoftcoreRead\" less words sent than requested" << endl;
    Tango::DeviceData din;
    Tango::DeviceData dout;
    Tango::DevVarULongArray in;
    //write to CPU 1 a read request of DAC channel G
    in.length(4);
    in[0] = 0x1; //CPU index
    in[1] = 0x7; //DAC command
    in[2] = 0x0; //read
    in[3] = 0x6; //DAC channel G
    din << in;
    dev->command_inout("SoftcoreWrite",din);
    //read 20 words from CPU 1 with 200ms time out
    Tango::DevVarUShortArray inr;
    inr.length(3);
    inr[0] = 0x1; //CPU index
    inr[1] = 20;  //Nb words to read
    inr[2] = 200; //time out in ms
    din << inr;
    dout = dev->command_inout("SoftcoreRead",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  try
  {
    //test CPU time out. Initiate a read while no request have been sent
    cout << "Test \"SoftcoreRead\" time out" << endl;
    Tango::DeviceData din;
    Tango::DeviceData dout;
    Tango::DevVarUShortArray in;
    //read 1 word from CPU 1 with 2s time out (to be visible)
    in.length(3);
    in[0] = 0x1;  //CPU index
    in[1] = 0x1;  //Nb words to read
    in[2] = 1000; //time out in ms
    din << in;
    dout = dev->command_inout("SoftcoreRead",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  st = dev->state();
  if(st != Tango::ALARM)
  {
    return false;
  }
  
  //test success
  cout << "Test success..." << endl;
  Tango::DeviceData din;
  Tango::DeviceData dout;
  Tango::DevVarULongArray in;
  Tango::DevVarUShortArray inr;
  //write to CPU 1 a read request of DAC channel G
  in.length(4);
  in[0] = 0x1; //CPU index
  in[1] = 0x7; //DAC command
  in[2] = 0x0; //read
  in[3] = 0x6; //DAC channel G
  din << in;
  try
  {
    dev->command_inout("SoftcoreWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  //read 1 words from CPU 1 with 200ms time out
  inr.length(3);
  inr[0] = 0x1; //CPU index
  inr[1] = 0x1; //Nb words to read
  inr[2] = 200; //time out in ms
  din << inr;
  try
  {
    dout = dev->command_inout("SoftcoreRead",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  vector<Tango::DevULong> dac_init(1);
  dout >> dac_init;
  cout << "DAC channel G value: " << dac_init[0] << endl;

  //write to CPU 1 a write request to DAC channel G
  in.length(5);
  in[0] = 0x1; //CPU index
  in[1] = 0x7; //DAC command
  in[2] = 0x1; //write
  in[3] = 0x6; //DAC channel G
  in[4] = dac_init[0]+1;
  din << in;
  try
  {
    dev->command_inout("SoftcoreWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }

  //write to CPU 1 a read request of DAC channel G
  in.length(4);
  in[0] = 0x1; //CPU index
  in[1] = 0x7; //DAC command
  in[2] = 0x0; //read
  in[3] = 0x6; //DAC channel G
  din << in;
  try
  {
    dev->command_inout("SoftcoreWrite",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  //read 1 word from CPU 1 with 200ms time out
  inr[0] = 0x1; //CPU index
  inr[1] = 0x1; //Nb words to read
  inr[2] = 200; //time out in ms
  din << inr;
  try
  {
    dout = dev->command_inout("SoftcoreRead",din);
  }
  catch (Tango::DevFailed &e)
  {
      cout << "============ Tango Exception ============" << endl;
      Tango::Except::print_exception(e);
      cout << "=========================================" << endl;
  }
  vector<Tango::DevULong> dac_end(1);
  dout >> dac_end;
  cout << "DAC channel G value: " << dac_end[0] << endl;

  st = ut_getState(dev);
  ut_getStatus(dev);
  if(st == Tango::FAULT)
  {
    return false;
  }
  if(dac_end[0] == (dac_init[0]+1))
  {
    return true;
  }
  else
  {
    return false;
  }
}
