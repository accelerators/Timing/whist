#include <termios.h>
#include <fcntl.h>
#include <tango.h>

#include "ut_functions.h"

using namespace std;

void help(char *name)
{
  fprintf(stderr,"\n== Utility for Tango device server Unit Tests ==\n\n");
  fprintf(stderr,"Description: Allows you to test a device instantiated in ");
  fprintf(stderr,"the Tango database of your environment. This utility connects ");
  fprintf(stderr,"to the device put in argument and calls a serie of unit test (function) ");
  fprintf(stderr,"defined in the ut_functions.h");
  fprintf(stderr,"Usage: %s [options] <device name>\n",name);
  fprintf(stderr,"[options] are:\n-h to print this help\n");
//  fprintf(stderr,"-b<bar> BAR where to read/write register\n");
//  fprintf(stderr,"-o<offset> offset at which read/write register\n");
//  fprintf(stderr,"-r to read register at offset <offset> in BAR <bar>\n");
  fprintf(stderr,"example: %s sys/tg_test/1\n",name);
  exit(1);
}

void user_check()
{
  char key;
  printf("Press [Enter] key to continue...\n");
  do
  {
    key = getchar();
  }while(key != '\n');
}


int main(int ac, char** av)
{
//  int c;
//
//  //OPTIONS NOT YET IMPLEMENTED
//  while ((c = getopt (ac, av, "hp:b:o:rw:")) != -1)
//  {
//    switch(c)
//    {
//    case 'p':
//      sscanf(optarg, "%i", &card_nb);
//      break;
//    case 'b':
//      sscanf(optarg, "%"SCNu8"", &bar);
//      break;
//    case 'o':
//      sscanf(optarg,"%i", &offset);
//      break;
//    case 'r':
//      read=1;
//      break;
//    case 'w':
//      sscanf(optarg,"%i",&data);
//      write=1;
//      break;
//    case 'h':
//      help(av[0]);
//      break;
//    default:
//      help(av[0]);
//    }
//  }

  if(ac != 2)
  {
    help(av[0]);
  }
  char *devName = av[1];
  try
  {
    Tango::DeviceProxy dev(devName);
    ut_getState(&dev);
    ut_getStatus(&dev);

//------------------------------------------------------------------------------
//             BitstreamFlash
//------------------------------------------------------------------------------
    cout << endl << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "             BitstreamFlash" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    printf("Disconnect the Whist Module from the network\n");
    user_check();
    if(ut_BitstreamFlash_Disconnected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");

    printf("\n\n\nConnect the Whist Module to the network\n");
    user_check();
    if(ut_BitstreamFlash_Connected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");



//------------------------------------------------------------------------------
//             BitstreamLoad
//------------------------------------------------------------------------------
    cout << endl << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "             BitstreamLoad" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    printf("Disconnect the Whist Module from the network\n");
    user_check();
    if(ut_BitstreamLoad_Disconnected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");

    printf("\n\n\nConnect the Whist Module to the network\n");
    user_check();
    if(ut_BitstreamLoad_Connected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");


    
//------------------------------------------------------------------------------
//             CheckLink
//------------------------------------------------------------------------------
    cout << endl << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "             CheckLink" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    printf("Disconnect the Whist Module from the network\n");
    user_check();
    if(ut_CheckLink_Disconnected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");

    printf("\n\n\nConnect the Whist Module to the network\n");
    user_check();
    if(ut_CheckLink_Connected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");




//------------------------------------------------------------------------------
//             RegisterRW
//------------------------------------------------------------------------------
    cout << endl << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "             RegisterRW" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    printf("Disconnect the Whist Module from the network\n");
    user_check();
    if(ut_RegisterRW_Disconnected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");

    printf("\n\n\nConnect the Whist Module to the network\n");
    user_check();
    if(ut_RegisterRW_Connected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");




//------------------------------------------------------------------------------
//             RegisterRWblock
//------------------------------------------------------------------------------
    cout << endl << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "             RegisterRWblock" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    printf("Disconnect the Whist Module from the network\n");
    user_check();
    if(ut_RegisterRWblock_Disconnected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");

    printf("\n\n\nConnect the Whist Module to the network\n");
    user_check();
    if(ut_RegisterRWblock_Connected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");




//------------------------------------------------------------------------------
//             SoftcoreLoad
//------------------------------------------------------------------------------
    cout << endl << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "             SoftcoreLoad" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    printf("Disconnect the Whist Module from the network\n");
    user_check();
    if(ut_SoftcoreLoad_Disconnected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");

    printf("\n\n\nConnect the Whist Module to the network\n");
    user_check();
    if(ut_SoftcoreLoad_Connected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");




//------------------------------------------------------------------------------
//             SoftcoreReset
//------------------------------------------------------------------------------
    cout << endl << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "             SoftcoreReset" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    printf("Disconnect the Whist Module from the network\n");
    user_check();
    if(ut_SoftcoreReset_Disconnected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");

    printf("\n\n\nConnect the Whist Module to the network\n");
    user_check();
    if(ut_SoftcoreReset_Connected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");



//------------------------------------------------------------------------------
//             SoftcoreRW
//------------------------------------------------------------------------------
    cout << endl << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    cout << "             SoftcoreRW" << endl;
    cout << "------------------------------------------------------------------------------" << endl;
    printf("Disconnect the Whist Module from the network\n");
    user_check();
    if(ut_SoftcoreRW_Disconnected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");

    printf("\n\n\nConnect the Whist Module to the network\n");
    user_check();
    if(ut_SoftcoreRW_Connected(&dev))
      printf("\n[UT Success]\n");
    else
      printf("\n[UT Failed]\n");



  }
  catch (Tango::DevFailed &e)
  {
      Tango::Except::print_exception(e);
  }

  return 0;
}

