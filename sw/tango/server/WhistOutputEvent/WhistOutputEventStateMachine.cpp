/*----- PROTECTED REGION ID(WhistOutputEventStateMachine.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        WhistOutputEventStateMachine.cpp
//
// description : State machine file for the WhistOutputEvent class
//
// project :     WHIST output Event interface class
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2019
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================

#include <WhistOutputEvent.h>

/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::WhistOutputEventStateMachine.cpp

//================================================================
//  States  |  Description
//================================================================


namespace WhistOutputEvent_ns
{
//=================================================
//		Attributes Allowed Methods
//=================================================

//--------------------------------------------------------
/**
 *	Method      : WhistOutputEvent::is_delay_allowed()
 *	Description : Execution allowed for delay attribute
 */
//--------------------------------------------------------
bool WhistOutputEvent::is_delay_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delay attribute in Write access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::delayStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::delayStateAllowed_WRITE

	//	Not any excluded states for delay attribute in read access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::delayStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::delayStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputEvent::is_delayRf8_allowed()
 *	Description : Execution allowed for delayRf8 attribute
 */
//--------------------------------------------------------
bool WhistOutputEvent::is_delayRf8_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for delayRf8 attribute in Write access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::delayRf8StateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::delayRf8StateAllowed_WRITE

	//	Not any excluded states for delayRf8 attribute in read access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::delayRf8StateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::delayRf8StateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputEvent::is_width_allowed()
 *	Description : Execution allowed for width attribute
 */
//--------------------------------------------------------
bool WhistOutputEvent::is_width_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for width attribute in Write access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::widthStateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::widthStateAllowed_WRITE

	//	Not any excluded states for width attribute in read access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::widthStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::widthStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputEvent::is_widthRf8_allowed()
 *	Description : Execution allowed for widthRf8 attribute
 */
//--------------------------------------------------------
bool WhistOutputEvent::is_widthRf8_allowed(TANGO_UNUSED(Tango::AttReqType type))
{
	//	Not any excluded states for widthRf8 attribute in Write access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::widthRf8StateAllowed_WRITE) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::widthRf8StateAllowed_WRITE

	//	Not any excluded states for widthRf8 attribute in read access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::widthRf8StateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::widthRf8StateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputEvent::is_EventId_allowed()
 *	Description : Execution allowed for EventId attribute
 */
//--------------------------------------------------------
bool WhistOutputEvent::is_EventId_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for EventId attribute in read access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::EventIdStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::EventIdStateAllowed_READ
	return true;
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputEvent::is_EventCounterReceive_allowed()
 *	Description : Execution allowed for EventCounterReceive attribute
 */
//--------------------------------------------------------
bool WhistOutputEvent::is_EventCounterReceive_allowed(TANGO_UNUSED(Tango::AttReqType type))
{

	//	Not any excluded states for EventCounterReceive attribute in read access.
	/*----- PROTECTED REGION ID(WhistOutputEvent::EventCounterReceiveStateAllowed_READ) ENABLED START -----*/
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::EventCounterReceiveStateAllowed_READ
	return true;
}


//=================================================
//		Commands Allowed Methods
//=================================================


/*----- PROTECTED REGION ID(WhistOutputEvent::WhistOutputEventStateAllowed.AdditionalMethods) ENABLED START -----*/

//	Additional Methods

/*----- PROTECTED REGION END -----*/	//	WhistOutputEvent::WhistOutputEventStateAllowed.AdditionalMethods

}	//	End of namespace
