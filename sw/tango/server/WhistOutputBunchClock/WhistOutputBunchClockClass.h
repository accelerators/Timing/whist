/*----- PROTECTED REGION ID(WhistOutputBunchClockClass.h) ENABLED START -----*/
//=============================================================================
//
// file :        WhistOutputBunchClockClass.h
//
// description : Include for the WhistOutputBunchClock root class.
//               This class is the singleton class for
//                the WhistOutputBunchClock device class.
//               It contains all properties and methods which the 
//               WhistOutputBunchClock requires only once e.g. the commands.
//
// project :     WHIST output BunchClock class
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef WhistOutputBunchClockClass_H
#define WhistOutputBunchClockClass_H

#include <tango.h>
#include <WhistOutputClass.h>
#include <WhistOutputBunchClock.h>


/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClockClass.h


namespace WhistOutputBunchClock_ns
{
/*----- PROTECTED REGION ID(WhistOutputBunchClockClass::classes for dynamic creation) ENABLED START -----*/


/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClockClass::classes for dynamic creation

//=========================================
//	Define classes for attributes
//=========================================
//	Attribute signalSource class definition
class signalSourceAttrib: public Tango::Attr
{
public:
	signalSourceAttrib():Attr("signalSource",
			Tango::DEV_ENUM, Tango::READ) {};
	~signalSourceAttrib() {};
	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att)
		{(static_cast<WhistOutputBunchClock *>(dev))->read_signalSource(att);}
	virtual bool is_allowed(Tango::DeviceImpl *dev,Tango::AttReqType ty)
		{return (static_cast<WhistOutputBunchClock *>(dev))->is_signalSource_allowed(ty);}
	virtual bool same_type(const type_info &in_type) {return typeid(signalSourceEnum) == in_type;}
	virtual string get_enum_type() {return string("signalSourceEnum");}
};


//=========================================
//	Define classes for commands
//=========================================

/**
 *	The WhistOutputBunchClockClass singleton definition
 */

#ifdef _TG_WINDOWS_
class __declspec(dllexport)  WhistOutputBunchClockClass : public WhistOutput_ns::WhistOutputClass
#else
class WhistOutputBunchClockClass : public WhistOutput_ns::WhistOutputClass
#endif
{
	/*----- PROTECTED REGION ID(WhistOutputBunchClockClass::Additionnal DServer data members) ENABLED START -----*/
	public:
            vector<string> signalSourceEnumString;

	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClockClass::Additionnal DServer data members

	public:
		//	write class properties data members
		Tango::DbData	cl_prop;
		Tango::DbData	cl_def_prop;
		Tango::DbData	dev_def_prop;
	
		//	Method prototypes
		static WhistOutputBunchClockClass *init(const char *);
		static WhistOutputBunchClockClass *instance();
		~WhistOutputBunchClockClass();
		Tango::DbDatum	get_class_property(string &);
		Tango::DbDatum	get_default_device_property(string &);
		Tango::DbDatum	get_default_class_property(string &);
	
	protected:
		WhistOutputBunchClockClass(string &);
		static WhistOutputBunchClockClass *_instance;
		void command_factory();
		void attribute_factory(vector<Tango::Attr *> &);
		void pipe_factory();
		void write_class_property();
		void set_default_property();
		void get_class_property();
		string get_cvstag();
		string get_cvsroot();
	
	private:
		void device_factory(const Tango::DevVarStringArray *);
		void create_static_attribute_list(vector<Tango::Attr *> &);
		void erase_dynamic_attributes(const Tango::DevVarStringArray *,vector<Tango::Attr *> &);
		vector<string>	defaultAttList;
		Tango::Attr *get_attr_object_by_name(vector<Tango::Attr *> &att_list, string attname);
};

}	//	End of namespace

#endif   //	WhistOutputBunchClock_H
