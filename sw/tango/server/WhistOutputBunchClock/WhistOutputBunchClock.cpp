/*----- PROTECTED REGION ID(WhistOutputBunchClock.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        WhistOutputBunchClock.cpp
//
// description : C++ source for the WhistOutputBunchClock class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               WhistOutputBunchClock are implemented in this file.
//
// project :     WHIST output BunchClock class
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <WhistOutputBunchClock.h>
#include <WhistOutputBunchClockClass.h>

/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock.cpp

/**
 *  WhistOutputBunchClock class description:
 *    For configure Output of the module WHIST to Sequencer Output.
 *    The WHIte rabbit Synchronization and Timing (WHIST) module has been
 *    developped for the refurbishment of the current ESRF timing system.
 *    It is based on a White Rabbit network. Consequently, a module can be
 *    accessed by a control computer, plugged to this network, communicating
 *    through the UDP transport layer thanks to the Etherbone protocol.
 *    
 *    this class is write to configure in manage outputs of the WHIST Modules
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name        |  Method name
//================================================================
//  State               |  Inherited (no method)
//  Status              |  Inherited (no method)
//  on                  |  Inherited (no method)
//  off                 |  Inherited (no method)
//  forceConfiguration  |  Inherited (no method)
//================================================================

//================================================================
//  Attributes managed are:
//================================================================
//  mode              |  Tango::DevEnum	Scalar
//  signalSource      |  Tango::DevEnum	Scalar
//  negativePolarity  |  Tango::DevBoolean	Scalar
//  OutputNumber      |  Tango::DevShort	Scalar
//  parentSignal      |  Tango::DevString	Scalar
//  isFastTiming      |  Tango::DevBoolean	Scalar
//================================================================

namespace WhistOutputBunchClock_ns
{
/*----- PROTECTED REGION ID(WhistOutputBunchClock::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::WhistOutputBunchClock()
 *	Description : Constructors for a Tango device
 *                implementing the classWhistOutputBunchClock
 */
//--------------------------------------------------------
WhistOutputBunchClock::WhistOutputBunchClock(Tango::DeviceClass *cl, string &s)
 : WhistOutput(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::constructor_1
}
//--------------------------------------------------------
WhistOutputBunchClock::WhistOutputBunchClock(Tango::DeviceClass *cl, const char *s)
 : WhistOutput(cl, s)
{
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::constructor_2
}
//--------------------------------------------------------
WhistOutputBunchClock::WhistOutputBunchClock(Tango::DeviceClass *cl, const char *s, const char *d)
 : WhistOutput(cl, s, d)
{
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void WhistOutputBunchClock::delete_device()
{
	DEBUG_STREAM << "WhistOutputBunchClock::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::delete_device
	delete[] attr_signalSource_read;

	if (Tango::Util::instance()->is_svr_shutting_down()==false  &&
		Tango::Util::instance()->is_device_restarting(device_name)==false &&
		Tango::Util::instance()->is_svr_starting()==false)
	{
		//	If not shutting down call delete device for inherited object
		WhistOutput_ns::WhistOutput::delete_device();
	}
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void WhistOutputBunchClock::init_device()
{
	DEBUG_STREAM << "WhistOutputBunchClock::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
        	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::init_device_before
	
	if (Tango::Util::instance()->is_svr_starting() == false  &&
		Tango::Util::instance()->is_device_restarting(device_name)==false)
	{
		//	If not starting up call init device for inherited object
		WhistOutput_ns::WhistOutput::init_device();
	}

	//	Get the device properties from database
	get_device_property();
	
	attr_signalSource_read = new signalSourceEnum[1];
	//	No longer if mandatory property not set. 
	if (mandatoryNotDefined)
		return;

	/*----- PROTECTED REGION ID(WhistOutputBunchClock::init_device) ENABLED START -----*/
	
        //	Initialize device
        *attr_signalSource_read = signalSourceEnum::_UNKOWN;
        //due to reread of the property in the database, recover backup
        outputNumber = outputNumberSave;
        
        signalBunchClockRequested = parsingSignalProperties();
        
        
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::init_device
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void WhistOutputBunchClock::get_device_property()
{
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::get_device_property_before) ENABLED START -----*/
	
	//	Initialize property data members
        	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::get_device_property_before

	mandatoryNotDefined = false;

	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("WhistModuleDevice"));
	dev_prop.push_back(Tango::DbDatum("OutputNumber"));
	dev_prop.push_back(Tango::DbDatum("refreshTime"));
	dev_prop.push_back(Tango::DbDatum("OutputBunchClockSignal"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on WhistOutputBunchClockClass to get class property
		Tango::DbDatum	def_prop, cl_prop;
		WhistOutputBunchClockClass	*ds_class =
			(static_cast<WhistOutputBunchClockClass *>(get_device_class()));
		int	i = -1;

		//	Try to initialize WhistModuleDevice from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  whistModuleDevice;
		else {
			//	Try to initialize WhistModuleDevice from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  whistModuleDevice;
		}
		//	And try to extract WhistModuleDevice value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  whistModuleDevice;
		//	Property StartDsPath is mandatory, check if has been defined in database.
		check_mandatory_property(cl_prop, dev_prop[i]);

		//	Try to initialize OutputNumber from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  outputNumber;
		else {
			//	Try to initialize OutputNumber from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  outputNumber;
		}
		//	And try to extract OutputNumber value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  outputNumber;
		//	Property StartDsPath is mandatory, check if has been defined in database.
		check_mandatory_property(cl_prop, dev_prop[i]);

		//	Try to initialize refreshTime from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  refreshTime;
		else {
			//	Try to initialize refreshTime from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  refreshTime;
		}
		//	And try to extract refreshTime value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  refreshTime;

		//	Try to initialize OutputBunchClockSignal from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  outputBunchClockSignal;
		else {
			//	Try to initialize OutputBunchClockSignal from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  outputBunchClockSignal;
		}
		//	And try to extract OutputBunchClockSignal value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  outputBunchClockSignal;
		//	Property StartDsPath is mandatory, check if has been defined in database.
		check_mandatory_property(cl_prop, dev_prop[i]);

	}

	/*----- PROTECTED REGION ID(WhistOutputBunchClock::get_device_property_after) ENABLED START -----*/
	
	//	Check device property data members init
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::get_device_property_after
}
//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::check_mandatory_property()
 *	Description : For mandatory properties check if defined in database.
 */
//--------------------------------------------------------
void WhistOutputBunchClock::check_mandatory_property(Tango::DbDatum &class_prop, Tango::DbDatum &dev_prop)
{
	//	Check if all properties are empty
	if (class_prop.is_empty() && dev_prop.is_empty())
	{
		TangoSys_OMemStream	tms;
		tms << endl <<"Property \'" << dev_prop.name;
		if (Tango::Util::instance()->_UseDb==true)
			tms << "\' is mandatory but not defined in database";
		else
			tms << "\' is mandatory but cannot be defined without database";
		append_status(tms.str());
		mandatoryNotDefined = true;
		/*----- PROTECTED REGION ID(WhistOutputBunchClock::check_mandatory_property) ENABLED START -----*/
		cerr << tms.str() << " for " << device_name << endl;
		
		/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::check_mandatory_property
	}
}


//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void WhistOutputBunchClock::always_executed_hook()
{
	DEBUG_STREAM << "WhistOutputBunchClock::always_executed_hook()  " << device_name << endl;
	if (mandatoryNotDefined)
	{
		Tango::Except::throw_exception(
					(const char *)"PROPERTY_NOT_SET",
					get_status().c_str(),
					(const char *)"WhistOutputBunchClock::always_executed_hook()");
	}
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::always_executed_hook) ENABLED START -----*/
        
        //	code always executed before all requests
        
        this->refresh();
        
        //getCurrentSignal set UNKNOWN if the state is UNKNOWN
        *attr_signalSource_read = getCurrentSignal();
        
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void WhistOutputBunchClock::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "WhistOutputBunchClock::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
        
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::read_attr_hardware
}
//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::write_attr_hardware()
 *	Description : Hardware writing for attributes
 */
//--------------------------------------------------------
void WhistOutputBunchClock::write_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "WhistOutputBunchClock::write_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::write_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::write_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute signalSource related method
 *	Description: 
 *
 *	Data type:	Tango::DevEnum (signalSourceEnum)
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void WhistOutputBunchClock::read_signalSource(Tango::Attribute &attr)
{
	DEBUG_STREAM << "WhistOutputBunchClock::read_signalSource(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::read_signalSource) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_signalSource_read);
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::read_signalSource
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void WhistOutputBunchClock::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	WhistOutput::add_dynamic_attributes();
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : WhistOutputBunchClock::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void WhistOutputBunchClock::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(WhistOutputBunchClock::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::add_dynamic_commands
}

/*----- PROTECTED REGION ID(WhistOutputBunchClock::namespace_ending) ENABLED START -----*/

//	Additional Methods


signalSourceEnum WhistOutputBunchClock::parsingSignalProperties(){
    
        outputBunchClockSignal = WhistUtil::toLowerCase(outputBunchClockSignal = WhistUtil::trim(outputBunchClockSignal));
        
        if(outputBunchClockSignal == "inj"){
            parentSignal = "Tinj";
            return signalSourceEnum::_INJ;
        } else if (outputBunchClockSignal == "ext"){
            parentSignal = "Text";
            return signalSourceEnum::_EXT;
        } else if (outputBunchClockSignal == "t0"){
            parentSignal = "T0";
            return signalSourceEnum::_T0;
        } else if (outputBunchClockSignal == "gun"){
            parentSignal = "Gun";
            return signalSourceEnum::_GUN;
        } else if (outputBunchClockSignal == "gun_rf8"){
            parentSignal = "Gun";
            return signalSourceEnum::_GUNRF8;
            
        }else{
            Tango::Except::throw_exception("OutputPulseSignalNotMatch"
                                    ,"Please Check property OutputBunchClockSignal of \"" + device_name + "\" need have"
                                    " value like [inj, ext, T0, gun or gun_rf8 ]"
                                    ,"WhistOutputBunchClock::parsingSignalProperties()");
            //dummy throw for warning compile
			throw;
        }
}

signalSourceEnum WhistOutputBunchClock::getCurrentSignal(){
    uint32_t source = BUCLK_WID_1_SRCE_R(this->regDataWidthConf);
    uint32_t mode = BUCLK_WID_1_MODE_R(this->regDataWidthConf);
    
    if(get_state() == Tango::UNKNOWN){
        return signalSourceEnum::_UNKOWN;
    }
    
    if(mode == MODE_GUN){
        return signalSourceEnum::_GUN;
    }else if(source == WhistUtil::SOURCE_T0_SEQ){
        return signalSourceEnum::_T0;
    }else if(source == WhistUtil::SOURCE_INJ_SEQ){
        return signalSourceEnum::_INJ;
    }else if(source == WhistUtil::SOURCE_EXT_SEQ){
        return signalSourceEnum::_EXT;
    }else if(source == WhistUtil::SOURCE_GUN_RF8){
        return signalSourceEnum::_GUNRF8;
    }
    return signalSourceEnum::_UNKOWN;
}

uint32_t WhistOutputBunchClock::getSourceReg(){
    if(signalBunchClockRequested == signalSourceEnum::_GUN){
        //the real important setting for this mode is mode register
        return 0;
    }else if(signalBunchClockRequested == signalSourceEnum::_T0){
        return WhistUtil::SOURCE_T0_SEQ;
    }else if(signalBunchClockRequested == signalSourceEnum::_INJ){
        return WhistUtil::SOURCE_INJ_SEQ;
    }else if(signalBunchClockRequested == signalSourceEnum::_EXT){
        return WhistUtil::SOURCE_EXT_SEQ;
    }else if(signalBunchClockRequested == signalSourceEnum::_GUNRF8){
        return WhistUtil::SOURCE_GUN_RF8;
    }
    Tango::Except::throw_exception("OutputPulseSignalNotMatch"
                                    ,"Please Check property OutputBunchClockSignal of \"" + device_name + "\" need have"
                                " value like [inj, ext, T0, gun or gun_rf8 ]"
                                    ,"WhistOutputBunchClock::getSourceReg()");
	//dummy throw for warning compile
	throw;
}

uint32_t WhistOutputBunchClock::getModeReg(){
    if(signalBunchClockRequested == signalSourceEnum::_GUN){
        return WhistOutput::MODE_GUN;
    }else{
        return WhistOutput::MODE_PULSE;
    }
}

WhistOutput_ns::modeEnum WhistOutputBunchClock::getModeUser(){
    return WhistOutput_ns::modeEnum::_BUNCHCLOCK;
}

string WhistOutputBunchClock::getSpecialStatus(){
    WhistOutputBunchClockClass	*ds_class =
                    (dynamic_cast<WhistOutputBunchClockClass *>(get_device_class()));
    vector<string> signalSourceEnumStr = ds_class->signalSourceEnumString;
    int index = (int)*attr_signalSource_read;
    if(index < 0 || index >= (int)signalSourceEnumStr.size()){
        Tango::Except::throw_exception("IncoherentInternalSignalSource","Incoherent Internal Signal Source","WhistOutputBunchClock::getSpecialStatus()");
    }
    return "It is BunchClock signal \"" + signalSourceEnumStr[*attr_signalSource_read] + "\"";
}

string WhistOutputBunchClock::getParentSignal(){
    return parentSignal;
}

bool WhistOutputBunchClock::isFastTiming(){
    return true;
}

void WhistOutputBunchClock::firstRefreshSpecialized(void){
    
}
/*----- PROTECTED REGION END -----*/	//	WhistOutputBunchClock::namespace_ending
} //	namespace
