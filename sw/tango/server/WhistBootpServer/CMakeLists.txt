#=============================================================================
#
# file :        CMakeLists.txt
#
# description : File to generate a TANGO device server using cmake.
#
# project :     WhistBootpServer
#
#=============================================================================
#                This file is generated by POGO
#        (Program Obviously used to Generate tango Object)
#=============================================================================
#
#

cmake_minimum_required (VERSION 2.8)
set(CMAKE_SKIP_RPATH TRUE)

# MAKE_ENV is the path to find common environment to buil project
#
set(MAKE_ENV /segfs/tango/cppserver/env)
#
# Project definitions
#
project(WhistBootpServer)

#
# optional compiler flags
#
set(CXXFLAGS_USER "-g -Wall")
set(CMAKE_EXE_LINKER_FLAGS "-Wl,--rpath=/opt/os/lib")


#
# Get global information
#
include(${MAKE_ENV}/cmake_tango.opt)

#
# Files for WhistBootpServer TANGO class
#
set(WHISTBOOTPSERVER WhistBootpServer)
set(WHISTBOOTPSERVER_INCLUDE ${CMAKE_SOURCE_DIR})
set(WHISTBOOTPSERVER_SRC ${WHISTBOOTPSERVER}.cpp ${WHISTBOOTPSERVER}Class.cpp ${WHISTBOOTPSERVER}StateMachine.cpp BootpServerThread.cpp IpTabBrowser.cpp)


#
# User additional include, link folders/libraries and source files
#
set(USER_INCL_DIR )
set(USER_LIB_DIR )
set(USER_LIBS )
set(USER_SRC_FILES )

#
# Set global info and include directories
#
set(ALL_CLASS_INCLUDE  ${WHISTBOOTPSERVER_INCLUDE}  ${USER_INCL_DIR})
set(SERVER_SRC ${WHISTBOOTPSERVER_SRC}  ${USER_SRC_FILES} ClassFactory.cpp main.cpp)
include_directories(${ALL_CLASS_INCLUDE}  ${USER_INCL_DIR} ${TANGO_INCLUDES})

#
# Device Server generation
#
set(SERVER_NAME WhistBootpServer)
include(${MAKE_ENV}/cmake_common_target.opt)

