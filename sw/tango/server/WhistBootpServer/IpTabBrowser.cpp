#include <iostream>
#include <string.h>
#include <arpa/inet.h>

#include "IpTabBrowser.h"

IpTabBrowser::IpTabBrowser(vector<string> _tab) : tab(_tab)
{
  cout << "IpTabBrowser --T--> " << "constructor" << endl;
  size = tab.size();
}

IpTabBrowser::~IpTabBrowser()
{
  cout << "IpTabBrowser --T--> " << "destructor" << endl;

}

bool IpTabBrowser::getIp(uint8_t *haddr, char *ip4)
{
  //convert haddr to string
  char haddr_str[17];
  sprintf(haddr_str,"%02x:%02x:%02x:%02x:%02x:%02x",
          haddr[0],
          haddr[1],
          haddr[2],
          haddr[3],
          haddr[4],
          haddr[5]);
  size_t i;
  //find MAC address in IP map table
  for(i=0;i<size;i++)
  {
    if(tab[i].find((const char*)haddr_str,0) != string::npos)
    {
      //MAC address matches IP table item
      //get corresponding IP address to assign
      string ip = tab[i].substr( tab[i].find('=',0)+1, tab[i].length() );
      strcpy(ip4,ip.c_str());
      printf("IP table matches: %s\n",tab[i].c_str());
      return true;
    }
  }
  return false;
}

bool IpTabBrowser::getIp(uint8_t *haddr, uint32_t *ip4)
{
  //convert HADDR address haddr to string
  char haddr_str[17];
  sprintf(haddr_str,"%02x:%02x:%02x:%02x:%02x:%02x",
          haddr[0],
          haddr[1],
          haddr[2],
          haddr[3],
          haddr[4],
          haddr[5]);
  size_t i;
  //find MAC address in IP map table
  for(i=0;i<size;i++)
  {
    if(tab[i].find((const char*)haddr_str,0) != string::npos)
    {
      //MAC address matches IP table item
      //get corresponding IP address to assign
      string ip = tab[i].substr( tab[i].find('=',0)+1, tab[i].length() );
      struct in_addr addr;
      inet_aton(ip.c_str(),&addr);
      *ip4 = addr.s_addr;
      printf("IP table matches: %s\n",tab[i].c_str());
      return true;
    }
  }
  return false;
}
