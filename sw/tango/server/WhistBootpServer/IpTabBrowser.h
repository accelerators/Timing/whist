/*
 * IpTabBrowser.h
 *
 *  Created on: Dec 12, 2016
 *      Author: broquet
 */

#ifndef IPTABBROWSER_H_
#define IPTABBROWSER_H_

#include <vector>

using namespace std;

class IpTabBrowser
{
public:
  //constructor
  IpTabBrowser(vector<string> _tab);
  //destructor
  virtual ~IpTabBrowser();

  //attributes

  //methods
  bool getIp(uint8_t *haddr, char *ip4);
  bool getIp(uint8_t *haddr, uint32_t *ip4);

private:
  //attributes
  vector<string> tab;
  size_t size;
  //methods

};

#endif /* IPTABBROWSER_H_ */
