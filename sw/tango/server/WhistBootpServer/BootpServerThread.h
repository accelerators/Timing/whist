/*
 * Bootpserverthread.h
 *
 *  Created on: Dec 8, 2016
 *      Author: broquet
 */

#ifndef BOOTPSERVERTHREAD_H_
#define BOOTPSERVERTHREAD_H_

#include <WhistBootpServer.h>
#include <net/if.h>
#include <netinet/ether.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <linux/if_packet.h>
#include <arpa/inet.h>

//------- BOOTP Server thread -------
//This thread aims to reply all nodes BOOTP request.
//The BOOTP request allows all nodes to get an IP address.
//This thread implements the BOOTP protocol as defined in the RFC 951.

//As a node has not yet its IP address when sending bootp request,
//it will not answer to ARP request. To avoid to manually construct
//ARP cache entry, this thread works at the link layer level (Ethernet),
//AF_PACKET socket type in linux.
//For those reasons, this process must run as root

//BOOTP Ethernet packet:
//here is description of a BOOTP packet encapsulation. Numbers are in byte
//|ETH HDR - 14|-------------------- ETHERNET DATA ---------------------|

//             |IP HDR - 20|----------------- IP DATA ------------------|

//                         |UDP HDR - 8| UDP DATA => BOOTP payload - 300|

//size of BOOTP ethernet packet:
//14 Ethernet header + 20 IP header + 8 UDP header + 300 payload (BOOTP)
// = 342 bytes
#define BOOTP_PACKET_SIZE 342

struct bootp {
  uint8_t op;
  uint8_t htype;
  uint8_t hlen;
  uint8_t hops;
  uint32_t xid;
  uint16_t secs;
  uint16_t unused;
  uint32_t ciaddr;
  uint32_t yiaddr;
  uint32_t siaddr;
  uint32_t giaddr;
  uint8_t chaddr[16];
  uint8_t sname[64];
  uint8_t file[128];
  uint8_t vend[64];
};

namespace WhistBootpServer_ns
{

class BootpServerThread : public omni_thread
{
public:
  //constructor
  BootpServerThread(WhistBootpServer *_dev, omni_mutex &m, const char *iface, vector<string> &_ipTable);
  //destructor
  virtual ~BootpServerThread();

  //methods
  void stop_server();

private:
  //attributes
  WhistBootpServer *dev;
  omni_mutex &mutex;
  bool must_stop;
  char status[256];
  const char *ifaceName;
  vector<string> &ipTab;
  
  struct timeval timeout;
  //socket file descriptor
  int sockfd;
  //server network interface
  int if_index;
  struct sockaddr server_hwaddr;
  struct in_addr server_addr;
  //BOOTP Ethernet packet
  struct ethhdr *eh;
  struct iphdr *iph;
  struct udphdr *udph;
  struct bootp *bootp_data;
  uint8_t packet[BOOTP_PACKET_SIZE];
  //packet server address (for binding)
  struct sockaddr_ll server;
  //packet node address (for sending packets)
  struct sockaddr_ll node;
  socklen_t nodelen;

  //methods
  void run(void *arg);
  bool start_server();
  void haddr_copy(uint8_t *to, uint8_t *from, size_t n);
  char* haddr_ntoa(uint8_t *haddr, char *buf);
  void construct_ip_header(struct iphdr *iph, uint32_t ip_dest);
  void construct_udp_header(struct udphdr *udph);
  void construct_bootp_data(struct bootp *data, uint32_t ip_dest);
  uint16_t ip_crc(uint16_t *buf, size_t n);
};

} /* namespace WhistBootpServer_ns */
#endif /* BOOTPSERVERTHREAD_H_ */
