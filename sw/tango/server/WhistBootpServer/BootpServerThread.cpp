/*
 * BootpServerThread.cpp
 *
 *  Created on: Dec 8, 2016
 *      Author: broquet
 */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <string.h>

#include "BootpServerThread.h"
#include "IpTabBrowser.h"

//IP header length not defined in standard/linux includes
#define IP_HLEN  20
#define IP_P_UDP 0x11
//UDP header length not defined in standard/linux includes
#define UDP_HLEN 8

#define BOOTP_SERVER_PORT 0x43
#define BOOTP_CLIENT_PORT 0x44
#define BOOTREQUEST 0x1
#define BOOTREPLY   0x2
#define BOOTP_DATA_SIZE 308

namespace WhistBootpServer_ns
{

  BootpServerThread::BootpServerThread(WhistBootpServer *_dev, omni_mutex &m, const char *iface, vector<string> &_ipTable) : omni_thread(), mutex(m), ifaceName(iface), ipTab(_ipTable)
{
  cout << "BootpServerThread --T--> " << "constructor" << endl;
  this->dev = _dev;
  
  if(start_server())
  {
    //start the thread
    start();
  }
  else
  {
    delete(this);
  }
}

BootpServerThread::~BootpServerThread()
{
  cout << "BootpServerThread --T--> " << "destructor" << endl;
  close(sockfd);
}

void BootpServerThread::run(void *arg)
{
  cout << "BootpServerThread --T--> " << "Run with arg" << arg << endl;

  ssize_t nrecv = 0, nsend = 0;
  uint8_t haddr[ETH_ALEN];
  uint32_t ip;
  bool ip_found;

  {
    omni_mutex_lock oml(mutex);
    dev->set_state(Tango::ON);
    dev->set_status("BOOTP server running\n");
    dev->srvRunning = true;
  }

  must_stop = false;
  nodelen = sizeof(node);
  

  while(!must_stop)
  {
    int rv;
    
    fd_set set;
    FD_ZERO(&set); /* clear the set */
    FD_SET(sockfd, &set);
    
    timeout.tv_sec = 1;  //1s timeout
    timeout.tv_usec = 0;

    //wait node request (for 1s)
    rv = select(sockfd + 1, &set, nullptr, nullptr, &timeout);
    if(rv < -1){
        //select error
    }else if(rv == 0){
        //timeout, socket does not have anything to read
    }else{
        nrecv = recv(sockfd,(uint8_t*)&packet,BOOTP_PACKET_SIZE,0);
        //check if the packet has the good length, otherwise it is ignored
        if(nrecv == BOOTP_PACKET_SIZE)
        {
           eh = (struct ethhdr*) (packet);
           iph = (struct iphdr*) (packet + ETH_HLEN);
           udph = (struct udphdr*) (packet + ETH_HLEN + IP_HLEN);
           bootp_data = (struct bootp*) (packet + ETH_HLEN + IP_HLEN + UDP_HLEN);
           //check if the packet is an UDP packet, otherwise it is ignored
           if(iph->protocol == IP_P_UDP)
           {
              //check if the UDP packet targets to BOOTP server port, otherwise it is ignored
              if(udph->dest == htons(BOOTP_SERVER_PORT))
              {
                //check if it is a BOOTP request, otherwise it is ignored
                if(bootp_data->op == BOOTREQUEST)
                {
                 //increment request counter
                {
                  omni_mutex_lock oml(mutex);
                  ++dev->attr_requestCounter_read[0];
                }
                //get mac address of the from packet
                //and retreive corresponding IP address in the IP map
                haddr_copy(haddr,(uint8_t*)eh->h_source,(size_t)ETH_ALEN);
                {
                  omni_mutex_lock oml(mutex);
                  IpTabBrowser iptb(ipTab);
                  ip_found = iptb.getIp(haddr,&ip);
                }
                if(ip_found)
                {
                  //modify packet to content for sending to node
                  //construct Ethernet header
                  haddr_copy((uint8_t*)eh->h_dest,haddr,(size_t)ETH_ALEN);
                  haddr_copy((uint8_t*)eh->h_source,(uint8_t*)server_hwaddr.sa_data,(size_t)ETH_ALEN);
                  eh->h_proto = htons(ETH_P_IP);
                  //construct IP header
                  construct_ip_header(iph,ip);
                  //construct UDP header
                  construct_udp_header(udph);
                  //construct BOOTP data
                  construct_bootp_data(bootp_data,ip);
                  //finish constructing packet with IP checksum
                  iph->check = ip_crc((uint16_t*)(packet+ETH_HLEN),BOOTP_PACKET_SIZE-ETH_HLEN);
                  //send packet
                  nsend = sendto(sockfd,(uint8_t*)&packet,BOOTP_PACKET_SIZE,MSG_DONTWAIT,
                                 (struct sockaddr*)&node,nodelen);
                  {
                    omni_mutex_lock oml(mutex);
                    if(nsend == BOOTP_PACKET_SIZE)
                    {
                      ++dev->attr_replyCounter_read[0];
                    }
                    else
                    {
                      struct in_addr addr;
                      addr.s_addr = ip;
                      char haddr_str[18]; //18-> xx:xx:xx:xx:xx:xx'\0'
                      sprintf(status,"ERROR while sending BOOTP reply to [%s|%s]: %s\n",
                              haddr_ntoa(haddr,haddr_str),
                              inet_ntoa(addr),
                              strerror(errno));
                      fprintf(stderr,"%s",status);
                      dev->set_state(Tango::ALARM);
                      dev->set_status(status);
                    }
                  }
                }
                else
                {
                  //IP address not found in IP table, set device ALARM
                  char haddr_str[18]; //18-> xx:xx:xx:xx:xx:xx'\0'
                  sprintf(status,"Node [%s] not found in IP table\n",haddr_ntoa(haddr,haddr_str));
                  fprintf(stderr,"%s",status);
                  {
                    omni_mutex_lock oml(mutex);
                    dev->set_state(Tango::ALARM);
                    dev->set_status(status);
                  }
                }
              }
            }
        }
      }
    }
  }
}

bool BootpServerThread::start_server(void)
{
  //create raw packet endpoint communication
  //SOCK_RAW: chosen in order to have access to link layer level header
  //ETH_P_IP: for IP protocol (0x0800) identification in Ethernet header
  //socket protocol requires network byte order, so use htons()
  sockfd = socket(AF_PACKET, SOCK_RAW, htons(ETH_P_IP));
  if(sockfd < 0)
  {
    sprintf(status,"ERROR opening socket: %s\n",strerror(errno));
    fprintf(stderr,"%s",status);
    {
      omni_mutex_lock oml(mutex);
      dev->set_state(Tango::FAULT);
      dev->set_status(status);
    }
    return false;
  }

  //set time out for receiving node's requests in order to be able to kill
  //this thread from device server (because thread loop runs recv()
  //blocking method)
  
  timeout.tv_sec = 1;  //1s timeout
  timeout.tv_usec = 0;
  //setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(struct timeval));

  //set the interface device to bind
  setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (char*)ifaceName, strlen(ifaceName));

  //initialize interface
  struct ifreq ifr;
  memset(&ifr,0,sizeof(struct ifreq));
  strncpy(ifr.ifr_name,ifaceName,strlen(ifaceName));
  //get interface information
  //index
  if_index = if_nametoindex(ifaceName);
  //MAC address
  if(ioctl(sockfd,SIOCGIFHWADDR,&ifr) < 0)
  {
    sprintf(status,"ERROR getting MAC address of interface (%s): %s\n",ifaceName,strerror(errno));
    fprintf(stderr,"%s",status);
    {
      omni_mutex_lock oml(mutex);
      dev->set_state(Tango::FAULT);
      dev->set_status(status);
    }
    close(sockfd);
    return false;
  }
  server_hwaddr = ifr.ifr_hwaddr;
  //IP address
  if(ioctl(sockfd,SIOCGIFADDR,&ifr) < 0)
  {
    sprintf(status,"ERROR getting IP address of interface (%s): %s\n",ifaceName,strerror(errno));
    fprintf(stderr,"%s",status);
    {
      omni_mutex_lock oml(mutex);
      dev->set_state(Tango::FAULT);
      dev->set_status(status);
    }
    close(sockfd);
    return false;
  }
  server_addr = ((struct sockaddr_in*)&ifr.ifr_addr)->sin_addr;

  printf("\n\nInterface info\n");
  printf("--------------\n");
  printf("name    : %s\n",ifr.ifr_name);
  printf("index   : %d\n",if_index);
  printf("MAC addr: %02x:%02x:%02x:%02x:%02x:%02x\n",
         (unsigned char)server_hwaddr.sa_data[0],
         (unsigned char)server_hwaddr.sa_data[1],
         (unsigned char)server_hwaddr.sa_data[2],
         (unsigned char)server_hwaddr.sa_data[3],
         (unsigned char)server_hwaddr.sa_data[4],
         (unsigned char)server_hwaddr.sa_data[5]);
  printf("IP addr : %s\n",inet_ntoa(server_addr));

  //initialize server and node address struct with zero
  memset(&server,0,sizeof(server));
  memset(&node,0,sizeof(node));
  //family
  node.sll_family = AF_PACKET;
  //index of the network interface
  node.sll_ifindex = if_index;

  server.sll_family = AF_PACKET;
  //for bind, only sll_protocol and sll_ifindex are used. See man packet(7)
  server.sll_protocol = htons(ETH_P_IP);
  server.sll_ifindex  = if_index;
  if(bind(sockfd,(struct sockaddr*)&server,sizeof(struct sockaddr_ll)) < 0)
  {
    sprintf(status,"ERROR binding on interface (%s): %s\n",ifaceName,strerror(errno));
    fprintf(stderr,"%s",status);
    {
      omni_mutex_lock oml(mutex);
      dev->set_state(Tango::FAULT);
      dev->set_status(status);
    }
    close(sockfd);
    return false;
  }

  //initialize packet
  memset(packet,0,BOOTP_PACKET_SIZE);

  return true;
}

void BootpServerThread::stop_server(void)
{
  must_stop = true;
}

void BootpServerThread::haddr_copy(uint8_t *to, uint8_t *from, size_t n)
{
  for(size_t i=0;i<n;i++)
  {
    to[i] = from[i];
  }
}

char* BootpServerThread::haddr_ntoa(uint8_t *haddr, char *buf)
{
  sprintf(buf,"%02x:%02x:%02x:%02x:%02x:%02x",
          haddr[0],
          haddr[1],
          haddr[2],
          haddr[3],
          haddr[4],
          haddr[5]);
  return buf;
}

void BootpServerThread::construct_ip_header(struct iphdr *iph, uint32_t ip_dest)
{
  iph->version = 4;
  iph->ihl = 5;
  iph->tos = 0;
  iph->tot_len = BOOTP_PACKET_SIZE - ETH_HLEN; //always the same size for BOOTP packet
  iph->id = 0;
  iph->frag_off = htons(0x4000); //don't fragment
  iph->ttl = 63; //as set by node...
  iph->protocol = IP_P_UDP;
  iph->saddr = server_addr.s_addr;
  iph->daddr = ip_dest;
}

void BootpServerThread::construct_udp_header(struct udphdr *udph)
{
  udph->source = htons(BOOTP_SERVER_PORT);
  udph->dest = htons(BOOTP_CLIENT_PORT);
  udph->len = BOOTP_DATA_SIZE; //always same size in BOOTP protocol
  udph->check = 0; //discard
}

void BootpServerThread::construct_bootp_data(struct bootp *data, uint32_t ip_dest)
{
  data->op = BOOTREPLY;
  data->yiaddr = (uint32_t)ip_dest;
  data->siaddr = (uint32_t)server_addr.s_addr;
}

uint16_t BootpServerThread::ip_crc(uint16_t *buf, size_t n)
{
  uint16_t sum = 0;
  size_t s;
  for(s=0; s<n; s++)
  {
    sum += *buf++;
  }
  sum = (sum >> 16) + (sum &0xffff);
  sum += (sum >> 16);
  return (uint16_t)(~sum);
}

} /* namespace WhistBootpServer_ns */
