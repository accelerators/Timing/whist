#ifndef WhistUtil_H
#define WhistUtil_H

#include <tango.h>
#include <buclk_regs.h>



#define BUCLK_OUT_CLOCK_RISING_MASK                   WBGEN2_GEN_MASK(0, 10)
#define BUCLK_OUT_CLOCK_RISING_SHIFT                  0
#define BUCLK_OUT_CLOCK_RISING_W(value)               WBGEN2_GEN_WRITE(value, 0, 10)
#define BUCLK_OUT_CLOCK_RISING_R(value)               WBGEN2_GEN_READ(value, 0, 10)

#define BUCLK_OUT_CLOCK_FALLING_MASK                   WBGEN2_GEN_MASK(10, 7)
#define BUCLK_OUT_CLOCK_FALLING_SHIFT                  10
#define BUCLK_OUT_CLOCK_FALLING_W(value)               WBGEN2_GEN_WRITE(value, 10, 7)
#define BUCLK_OUT_CLOCK_FALLING_R(value)               WBGEN2_GEN_READ(value, 10, 7)

#define BUCLK_OUT_CLOCK_MODULO_MASK                    WBGEN2_GEN_MASK(17, 9)
#define BUCLK_OUT_CLOCK_MODULO_SHIFT                   17
#define BUCLK_OUT_CLOCK_MODULO_W(value)                WBGEN2_GEN_WRITE(value, 17, 9)
#define BUCLK_OUT_CLOCK_MODULO_R(value)                WBGEN2_GEN_READ(value, 17, 9)

#define BUCLK_OUT_SRCOUNT_SR_DIVIDER_MASK              WBGEN2_GEN_MASK(10, 21)
#define BUCLK_OUT_SRCOUNT_SR_DIVIDER_SHIFT             10
#define BUCLK_OUT_SRCOUNT_SR_DIVIDER_W(value)          WBGEN2_GEN_WRITE(value, 10, 21)
#define BUCLK_OUT_SRCOUNT_SR_DIVIDER_R(value)          WBGEN2_GEN_READ(value, 10, 21)

#define BUCLK_OUT_SRCOUNT_SR_TURN_PHASE_MASK            WBGEN2_GEN_MASK(0, 10)
#define BUCLK_OUT_SRCOUNT_SR_TURN_PHASE_SHIFT           0
#define BUCLK_OUT_SRCOUNT_SR_TURN_PHASE_W(value)        WBGEN2_GEN_WRITE(value, 0, 10)
#define BUCLK_OUT_SRCOUNT_SR_TURN_PHASE_R(value)        WBGEN2_GEN_READ(value, 0, 10)

#define BUCLK_OUT_SRCOUNT_SR_TURN_SKIP_COARSE_MASK            WBGEN2_GEN_MASK(0, 22)
#define BUCLK_OUT_SRCOUNT_SR_TURN_SKIP_COARSE_SHIFT           0
#define BUCLK_OUT_SRCOUNT_SR_TURN_SKIP_COARSE_W(value)        WBGEN2_GEN_WRITE(value, 0, 22)
#define BUCLK_OUT_SRCOUNT_SR_TURN_SKIP_COARSE_R(value)        WBGEN2_GEN_READ(value, 0, 22)

#define BUCLK_IN0_ENABLE_MASK                           WBGEN2_GEN_MASK(8, 1)
#define BUCLK_IN1_ENABLE_MASK                           WBGEN2_GEN_MASK(9, 1)
#define BUCLK_IN2_ENABLE_MASK                           WBGEN2_GEN_MASK(10, 1)
#define BUCLK_IN3_ENABLE_MASK                           WBGEN2_GEN_MASK(11, 1)
#define BUCLK_IN0_POLARITY_MASK                         WBGEN2_GEN_MASK(12, 1)
#define BUCLK_IN1_POLARITY_MASK                         WBGEN2_GEN_MASK(13, 1)
#define BUCLK_IN2_POLARITY_MASK                         WBGEN2_GEN_MASK(14, 1)
#define BUCLK_IN3_POLARITY_MASK                         WBGEN2_GEN_MASK(15, 1)
#define BUCLK_IN0_EMPTY_MASK                            WBGEN2_GEN_MASK(16, 1)
#define BUCLK_IN1_EMPTY_MASK                            WBGEN2_GEN_MASK(17, 1)
#define BUCLK_IN2_EMPTY_MASK                            WBGEN2_GEN_MASK(18, 1)
#define BUCLK_IN3_EMPTY_MASK                            WBGEN2_GEN_MASK(19, 1)
#define BUCLK_IN0_FULL_MASK                             WBGEN2_GEN_MASK(20, 1)
#define BUCLK_IN1_FULL_MASK                             WBGEN2_GEN_MASK(21, 1)
#define BUCLK_IN2_FULL_MASK                             WBGEN2_GEN_MASK(22, 1)
#define BUCLK_IN3_FULL_MASK                             WBGEN2_GEN_MASK(23, 1)

#define EVT_STATUS_ADR_RECEIVE_ID_OUT9                   1
#define EVT_STATUS_ADR_RECEIVE_ID_OUT10                  2
#define EVT_STATUS_ADR_RECEIVE_ID_OUT11                  3
#define EVT_STATUS_ADR_RECEIVE_ID_OUT12                  4
#define EVT_STATUS_ADR_TRANSMIT_ID_IN0                   5
#define EVT_STATUS_ADR_TRANSMIT_ID_IN1                   6
#define EVT_STATUS_ADR_TRANSMIT_ID_IN2                   7
#define EVT_STATUS_ADR_TRANSMIT_ID_IN3                   8
#define EVT_STATUS_ADR_TRANSMIT_DELAY_IN0                13
#define EVT_STATUS_ADR_TRANSMIT_DELAY_IN1                14
#define EVT_STATUS_ADR_TRANSMIT_DELAY_IN2                15
#define EVT_STATUS_ADR_TRANSMIT_DELAY_IN3                16

#define EVT_STATUS_0_ENABLE_OUT9                          WBGEN2_GEN_MASK(0, 1)
#define EVT_STATUS_0_ENABLE_OUT10                         WBGEN2_GEN_MASK(1, 1)
#define EVT_STATUS_0_ENABLE_OUT11                         WBGEN2_GEN_MASK(2, 1)
#define EVT_STATUS_0_ENABLE_OUT12                         WBGEN2_GEN_MASK(3, 1)
#define EVT_STATUS_0_ENABLE_IN0                           WBGEN2_GEN_MASK(8, 1)
#define EVT_STATUS_0_ENABLE_IN1                           WBGEN2_GEN_MASK(9, 1)
#define EVT_STATUS_0_ENABLE_IN2                           WBGEN2_GEN_MASK(10, 1)
#define EVT_STATUS_0_ENABLE_IN3                           WBGEN2_GEN_MASK(11, 1)
#define EVT_STATUS_0_CNT_ENABLE_OUT9                      WBGEN2_GEN_MASK(16, 1)
#define EVT_STATUS_0_CNT_ENABLE_OUT10                     WBGEN2_GEN_MASK(17, 1)
#define EVT_STATUS_0_CNT_ENABLE_OUT11                     WBGEN2_GEN_MASK(18, 1)
#define EVT_STATUS_0_CNT_ENABLE_OUT12                     WBGEN2_GEN_MASK(19, 1)
#define EVT_STATUS_0_CNT_ENABLE_IN0                       WBGEN2_GEN_MASK(24, 1)
#define EVT_STATUS_0_CNT_ENABLE_IN1                       WBGEN2_GEN_MASK(25, 1)
#define EVT_STATUS_0_CNT_ENABLE_IN2                       WBGEN2_GEN_MASK(26, 1)
#define EVT_STATUS_0_CNT_ENABLE_IN3                       WBGEN2_GEN_MASK(27, 1)
#define EVT_STATUS_0_SOFTCORE_ENABLE                      WBGEN2_GEN_MASK(30, 1)

//my define to get source Reg with Event bit
#define BUCLK_WID_MY_SRCE_MASK                            WBGEN2_GEN_MASK(24, 4)
#define BUCLK_WID_MY_SRCE_SHIFT                           24
#define BUCLK_WID_MY_SRCE_W(value)                        WBGEN2_GEN_WRITE(value, 24, 4)
#define BUCLK_WID_MY_SRCE_R(reg)                          WBGEN2_GEN_READ(reg, 24, 4)
   
using namespace std;

class WhistUtil{
    
    
public:
    static const uint32_t MASK_BULCLK_BASE = 0x10000;
    static const uint32_t NUMBER_BUNCH_STORAGE_RING = 992;
    static const uint32_t NUMBER_BUNCH_BOOSTER = 352;
    static const int MAX_LINAC_SHOT = 10;
    static const int BUNCH_LIST_BUCLK_MAX_SIZE = 8192;
    static constexpr double RF_FREQ = 352.202 * 1.0e6;
    static constexpr double SR_FREQ = RF_FREQ / NUMBER_BUNCH_STORAGE_RING;
    static constexpr double RF_PERIOD = 1 / RF_FREQ;
    static constexpr double RF_ON_8 = RF_PERIOD * 8.0;
    static constexpr double RF_SUPER_PERIOD = 1.0 / (RF_FREQ / (NUMBER_BUNCH_STORAGE_RING * 11.0));
    static constexpr double DAC_CONVERT_FACTOR = 3.3 / 256.0;
    static constexpr double WR_FREQ = 62.5 * 1.0e6;
    static constexpr double WR_PERIOD = 1 / WR_FREQ;
    
    static const uint32_t SOFTCORE_D3S = 0;
    static const uint32_t SOFTCORE_BCC = 1;
    static const uint32_t SOFTCORE_EVT = 2;

    static const uint32_t SOFTCORE_STREAM_CONF = 5;
    static const uint32_t SOFTCORE_MASTER_MODE = 1;
    static const uint32_t SOFTCORE_SLAVE_MODE = 2;
    static const uint32_t SOFTCORE_STREAM_ID = 0x8f;
    
    static const uint32_t SOFTCORE_RF_MSB = 0x2d1;
    static const uint32_t SOFTCORE_RF_LSB = 0xa7f83d4f;
    /*old value for 352.202 Mhz
    static const uint32_t SOFTCORE_RF_MSB = 0x2d1;
    static const uint32_t SOFTCORE_RF_LSB = 0x4f483caf;
     */
    
    /* see http://wikiserv.esrf.fr/electronics_unit/index.php/WHIST_based_timing_system_:_hardware#SY89296_programming_and_Phase_.2F_Fine_Delay_correspondence*/
    static constexpr Tango::DevLong FINE_DELAY_OFFSET_OUT_1 = ((512.0 / 350.0) / 2.0) * RF_FREQ * 1.0e-6 - 3;
    static constexpr double          FINE_DELAY_GAIN_TO_DEGRE_OUT_1 = 350.0 / RF_FREQ / 1.0e-6;
    //9 coils 512 value * 2.3 1177.6  ==> 1177
    // for optimied range +-deg    1177 / 2 = 588
    static const     Tango::DevLong FINE_DELAY_OFFSET_OUT_23 = 588;
    static constexpr     double          FINE_DELAY_GAIN_TO_DEGRE_OUT_23 = 2.3;
    
    static double uIntToDoubleRF8(uint32_t bin);
    static double uIntToDoubleRf(uint32_t bin);
    static uint32_t doubleRF8ToUInt(double val);
    static uint32_t doubleRfToUInt(double val);
    static double uIntToDoubleSuperPeriods(uint32_t bin);
    static uint32_t doubleSuperPeriodsToUInt(double val);
    static string getBunchClockDeviceName(void);
    static string getEventManagerDeviceName(void);
    
    static const uint32_t SOURCE_T0 = 0;
    static const uint32_t SOURCE_INJ = 1;
    static const uint32_t SOURCE_EXT = 2;
    static const uint32_t SOURCE_KI = 3;
    static const uint32_t SOURCE_T0_SEQ = 4;
    static const uint32_t SOURCE_INJ_SEQ = 5;
    static const uint32_t SOURCE_EXT_SEQ = 6;
    static const uint32_t SOURCE_GUN_RF8 = 7;
    static const uint32_t SOURCE_EVENT = 8;
    
    static const uint32_t SOURCE_32TC = 0;
    static const uint32_t SOURCE_SRTC = 1;
    static const uint32_t SOURCE_BOOTC = 2;
    static const uint32_t SOURCE_SR_COUNT = 3;
    static const uint32_t SOURCE_CLK16B = 4;
    static const uint32_t SOURCE_CLK4B = 5;
    
    static const uint32_t SOURCE_DEVICE = 0;
    
    static const uint32_t MAX_VAL_REG_24BITS = 0x00FFFFFF;
    static const uint32_t MAX_VAL_REG_26BITS = 0x03FFFFFF;
    
    static const uint32_t MAX_VAL_BOOSTER_STEP = 351;
    static const uint32_t MAX_VAL_BOOSTER_STEP_RF8 = 43;
    static const uint32_t MAX_VAL_SR_STEP = 991;
    static const uint32_t MAX_VAL_SR_STEP_RF8 = 123;
    static const uint32_t MAX_VAL_32TC_STEP = 31;
    static const uint32_t MAX_VAL_32TC_STEP_RF8 = 3;
    static const uint32_t MAX_VAL_SRCOUNT_STEP = 123;
    static const uint32_t MAX_VAL_CLK16B_4B_STEP = 61;
    
    static const uint32_t* IN_VAL_HI_REGS;
    static const uint32_t* IN_VAL_LO_REGS;
    static const uint32_t* BUCKL_IN_MASK_FIFO_RD;
    static const uint32_t* BUCLK_IN_ENABLE_MASK;
    static const uint32_t* BUCLK_IN_POLARITY_MASK;
    static const uint32_t* BUCLK_IN_EMPTY_MASK;
    static const uint32_t* BUCLK_IN_FULL_MASK;
    static const uint32_t* BUCLK_IN_WRITABLE_MASK;
    
    static const uint32_t EVT_OUTPUT_NUMBER_START = 8;
    
    static const uint32_t* EVT_STATUS_0_ENABLE_IN;
    static const uint32_t* EVT_STATUS_0_ENABLE_OUT;
    static const uint32_t* EVT_STATUS_0_CNT_ENABLE_IN;
    static const uint32_t* EVT_STATUS_0_CNT_ENABLE_OUT;
    static const uint32_t* EVT_STATUS_ADR_RECEIVE_ID_OUT;
    static const uint32_t* EVT_STATUS_ADR_TRANSMIT_ID_IN;
    static const uint32_t* EVT_STATUS_ADR_TRANSMIT_DELAY_IN;
    
    static constexpr const char* INPUT_MODE_EVENT = (char*)"event";
    static constexpr const char* INPUT_MODE_STAMP = (char*)"stamp";
    
    static string getMaxValueRf8(uint32_t maxValueBin);
    static string getMaxValueRf(uint32_t maxValueBin);
        
    static long timeMS(void);

    static uint64_t timeUS(void);
    static string timeUSToString(uint64_t & timeUs);
    static string timeToString(uint64_t &epochTime);
    static uint64_t getEpochStamp(uint64_t &freqRf8 ,uint64_t &ppsRef,uint64_t &utcRef,uint64_t &stampRf8);
    static uint64_t rf8ToEpoch(uint64_t &masterStartEpoch,uint64_t &stampRf8);
    static string rf8ToHumanTime(uint64_t &masterStartEpoch,uint64_t &stampRf8);

    static string trim(string & s);
    static string toLowerCase(string & s);
    static Tango::DevState getWorst(Tango::DevState s1, Tango::DevState s2);
    static vector<string> split(const string & toSplit, char separator);
    static void waitState(Tango::DeviceProxy &device,const string &stateAttributeName, Tango::DevState stateToReach);
    static string extractDeviceName(string & attributeName,string propName);
    static void updateAttributePropertie(Tango::DbDevice * db_dev,const string &attrName,const string &propName, const string &attrValue);
    static string getAttributePropertie(Tango::DbDevice * db_dev,const string &attrName,const string &propName);
    static Tango::DevShort getCurrentSignal(uint32_t regDataWidthConf);
};

#endif   //	WhistUtil_H
