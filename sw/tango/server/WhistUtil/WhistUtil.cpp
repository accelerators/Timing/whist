//=============================================================================
//
// file :        WhistUtil.cpp
//
// description : C++ source for the WhistUtil class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               WhistUtil are implemented in this file.
//
// project :     WHIST Util class
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//

#include <WhistUtil.h>
#include <ratio>


    static const uint32_t inValHiRegs[] = {BUCLK_REG_IN0_STAMP_HI, BUCLK_REG_IN1_STAMP_HI,
                                                BUCLK_REG_IN2_STAMP_HI, BUCLK_REG_IN3_STAMP_HI};
    const uint32_t* WhistUtil::IN_VAL_HI_REGS = inValHiRegs;

    static const uint32_t inValLoRegs[] = {BUCLK_REG_IN0_STAMP_LO, BUCLK_REG_IN1_STAMP_LO,
                                                BUCLK_REG_IN2_STAMP_LO, BUCLK_REG_IN3_STAMP_LO};
    const uint32_t* WhistUtil::IN_VAL_LO_REGS = inValLoRegs;
    
    static const uint32_t buclkinFifiRd[] = {BUCLK_STAMP_CSR_IN0_FIFO_RD, BUCLK_STAMP_CSR_IN1_FIFO_RD,
                                                BUCLK_STAMP_CSR_IN2_FIFO_RD, BUCLK_STAMP_CSR_IN3_FIFO_RD};
    const uint32_t* WhistUtil::BUCKL_IN_MASK_FIFO_RD = buclkinFifiRd;
    
    static const uint32_t buclkinEnMask[] = {BUCLK_IN0_ENABLE_MASK, BUCLK_IN1_ENABLE_MASK,
                                                BUCLK_IN2_ENABLE_MASK, BUCLK_IN3_ENABLE_MASK}; 
    const uint32_t* WhistUtil::BUCLK_IN_ENABLE_MASK = buclkinEnMask;
    
    static const uint32_t buclkInPolarityMask[] = {BUCLK_IN0_POLARITY_MASK, BUCLK_IN1_POLARITY_MASK,
                                                BUCLK_IN2_POLARITY_MASK, BUCLK_IN3_POLARITY_MASK};
    const uint32_t* WhistUtil::BUCLK_IN_POLARITY_MASK = buclkInPolarityMask;
    
    static const uint32_t buclkInEmptyMask[] = {BUCLK_IN0_EMPTY_MASK, BUCLK_IN1_EMPTY_MASK,
                                                   BUCLK_IN2_EMPTY_MASK, BUCLK_IN3_EMPTY_MASK};
    const uint32_t* WhistUtil::BUCLK_IN_EMPTY_MASK = buclkInEmptyMask;
    
    static const uint32_t buclkInFullMask[] = {BUCLK_IN0_FULL_MASK, BUCLK_IN1_FULL_MASK,
                                                   BUCLK_IN2_FULL_MASK, BUCLK_IN3_FULL_MASK};
    const uint32_t* WhistUtil::BUCLK_IN_FULL_MASK = buclkInFullMask;
    
    static const uint32_t buclkInWritableMask[] = {BUCLK_IN0_ENABLE_MASK|BUCLK_IN0_POLARITY_MASK,
                                            BUCLK_IN1_ENABLE_MASK|BUCLK_IN1_POLARITY_MASK,
                                            BUCLK_IN2_ENABLE_MASK|BUCLK_IN2_POLARITY_MASK,
                                            BUCLK_IN3_ENABLE_MASK|BUCLK_IN3_POLARITY_MASK};
    const uint32_t* WhistUtil::BUCLK_IN_WRITABLE_MASK = buclkInWritableMask;
    
    
    static const uint32_t evtStatusEnableIn[] = {EVT_STATUS_0_ENABLE_IN0, EVT_STATUS_0_ENABLE_IN1,
                                                   EVT_STATUS_0_ENABLE_IN2, EVT_STATUS_0_ENABLE_IN3};
    const uint32_t* WhistUtil::EVT_STATUS_0_ENABLE_IN = evtStatusEnableIn;
    
    static const uint32_t evtStatusEnableOut[] = {EVT_STATUS_0_ENABLE_OUT9, EVT_STATUS_0_ENABLE_OUT10,
                                                   EVT_STATUS_0_ENABLE_OUT11, EVT_STATUS_0_ENABLE_OUT12};
    const uint32_t* WhistUtil::EVT_STATUS_0_ENABLE_OUT = evtStatusEnableOut;
    
    static const uint32_t evtStatusCntEnableIn[] = {EVT_STATUS_0_CNT_ENABLE_IN0, EVT_STATUS_0_CNT_ENABLE_IN1,
                                                   EVT_STATUS_0_CNT_ENABLE_IN2, EVT_STATUS_0_CNT_ENABLE_IN3};
    const uint32_t* WhistUtil::EVT_STATUS_0_CNT_ENABLE_IN = evtStatusCntEnableIn;
    
    static const uint32_t evtStatusCntEnableOut[] = {EVT_STATUS_0_CNT_ENABLE_OUT9, EVT_STATUS_0_CNT_ENABLE_OUT10,
                                                   EVT_STATUS_0_CNT_ENABLE_OUT11, EVT_STATUS_0_CNT_ENABLE_OUT12};
    const uint32_t* WhistUtil::EVT_STATUS_0_CNT_ENABLE_OUT = evtStatusCntEnableOut;
    
    
    static const uint32_t evtStatusAdrReceiveIdOut[] = {EVT_STATUS_ADR_RECEIVE_ID_OUT9, EVT_STATUS_ADR_RECEIVE_ID_OUT10,
                                                   EVT_STATUS_ADR_RECEIVE_ID_OUT11, EVT_STATUS_ADR_RECEIVE_ID_OUT12};
    const uint32_t* WhistUtil::EVT_STATUS_ADR_RECEIVE_ID_OUT = evtStatusAdrReceiveIdOut;
    
    static const uint32_t evtStatusAdrTransmitIdIn[] = {EVT_STATUS_ADR_TRANSMIT_ID_IN0, EVT_STATUS_ADR_TRANSMIT_ID_IN1,
                                                   EVT_STATUS_ADR_TRANSMIT_ID_IN2, EVT_STATUS_ADR_TRANSMIT_ID_IN3};
    const uint32_t* WhistUtil::EVT_STATUS_ADR_TRANSMIT_ID_IN = evtStatusAdrTransmitIdIn;
    
    static const uint32_t evtStatusAdrTransmitDelayIn[] = {EVT_STATUS_ADR_TRANSMIT_DELAY_IN0, EVT_STATUS_ADR_TRANSMIT_DELAY_IN1,
                                                   EVT_STATUS_ADR_TRANSMIT_DELAY_IN2, EVT_STATUS_ADR_TRANSMIT_DELAY_IN3};
    const uint32_t* WhistUtil::EVT_STATUS_ADR_TRANSMIT_DELAY_IN = evtStatusAdrTransmitDelayIn;
    
    
string WhistUtil::getMaxValueRf8(uint32_t maxValueBin){
    return to_string(uIntToDoubleRF8(maxValueBin));
}

string WhistUtil::getMaxValueRf(uint32_t maxValueBin){
    return to_string(uIntToDoubleRf(maxValueBin));
}

double WhistUtil::uIntToDoubleRF8(uint32_t bin){
    return bin * RF_ON_8;
}

double WhistUtil::uIntToDoubleRf(uint32_t bin){
    return bin * RF_PERIOD;
}

uint32_t WhistUtil::doubleRF8ToUInt(double val){
    return (uint32_t)((val / RF_ON_8) + (0.5));
}

uint32_t WhistUtil::doubleRfToUInt(double val){
    return (uint32_t)((val / RF_PERIOD) + (0.5));
}

double WhistUtil::uIntToDoubleSuperPeriods(uint32_t bin){
    return bin * RF_SUPER_PERIOD;
}

uint32_t WhistUtil::doubleSuperPeriodsToUInt(double val){
    return (uint32_t)((val / RF_SUPER_PERIOD) + (0.5));
}

int64_t WhistUtil::timeMS(){
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    return tv.tv_sec * 1000 + tv.tv_usec / 1000; 
}

uint64_t WhistUtil::timeUS(){
    struct timeval tv;
    gettimeofday(&tv, nullptr);
    return (uint64_t)tv.tv_sec * 1000000 + tv.tv_usec; 
}

string WhistUtil::timeUSToString(uint64_t &timeUs){
    
    time_t datePart;
    time_t usPart;
    struct tm *convertedTp;
    char tmbuf[25], buf[128];
    datePart = (time_t)timeUs / 1000000;
    usPart = (time_t)(uint64_t)(timeUs -(uint64_t)datePart * 1000000);
    convertedTp = localtime(&datePart);
    strftime(tmbuf, sizeof tmbuf, "%Y-%m-%d %H:%M:%S", convertedTp);
    snprintf(buf, sizeof buf, "%s.%06ld", tmbuf, usPart);
    
    return string(buf); 
}

string WhistUtil::timeToString(uint64_t &epochTime){
    struct tm *convertedTp;
    char buf[64];
    time_t datePart = epochTime;
    
    convertedTp = localtime(&datePart);
    strftime(buf, sizeof buf, "%Y-%m-%d %H:%M:%S", convertedTp);
    
    return string(buf); 
}

uint64_t WhistUtil::getEpochStamp(uint64_t &freqRf8 ,uint64_t &ppsRef,uint64_t &utcRef,uint64_t &stampRf8){
    double rf8Period = 1.0 / freqRf8;
    int64_t deltaRf8 = stampRf8 - ppsRef;
    return utcRef + deltaRf8 * rf8Period * 1.0e6;
}

uint64_t WhistUtil::rf8ToEpoch(uint64_t &masterStartEpoch,uint64_t &stampRf8){
    uint64_t timeUsStampFromMasterStart = stampRf8 * RF_ON_8  * 1.0e6;
    return masterStartEpoch + timeUsStampFromMasterStart;
}

string WhistUtil::rf8ToHumanTime(uint64_t &masterStartEpoch,uint64_t &stampRf8){
    uint64_t timeUs = rf8ToEpoch(masterStartEpoch,stampRf8);
    return timeUSToString(timeUs);
}

string WhistUtil::trim(string & s){
    s.erase(s.begin(), find_if(s.begin(),s.end(),not1(ptr_fun<int, int>(isspace))));
    s.erase(find_if(s.rbegin(),s.rend(),not1(ptr_fun<int, int>(isspace))).base(), s.end());
    return s;
}

string WhistUtil::toLowerCase(string & s){
    transform(s.begin(),s.end(),s.begin(), ::tolower);
    return s;
}

Tango::DevState WhistUtil::getWorst(Tango::DevState s1, Tango::DevState s2) {
    if (s1 == Tango::UNKNOWN || s2 == Tango::UNKNOWN) {
        return Tango::UNKNOWN;
    }
    if (s1 == Tango::FAULT || s2 == Tango::FAULT) {
        return Tango::FAULT;
    }
    if (s1 == Tango::ALARM || s2 == Tango::ALARM) {
        return Tango::ALARM;
    }
    if (s1 == Tango::MOVING || s2 == Tango::MOVING) {
        return Tango::MOVING;
    }
    if (s1 == Tango::STANDBY || s2 == Tango::STANDBY) {
        return Tango::STANDBY;
    }
    if (s1 == Tango::ON && s2 == Tango::ON) {
        return Tango::ON;
    }
    if (s1 == Tango::OFF && s2 == Tango::OFF) {
        return Tango::OFF;
    }
    return Tango::ALARM;
}

vector<string> WhistUtil::split(const string & toSplit, char separator){
    istringstream split(toSplit); // flux d'exatraction sur un std::string
    vector<string> toReturn;
    for (string each; std::getline(split, each, separator);){
        if(!(each = trim(each)).empty()){
            toReturn.push_back(each);
        }
    }
    return toReturn;
}

void WhistUtil::waitState(Tango::DeviceProxy &device,const string &stateAttributeName, Tango::DevState stateToReach){
    long start,now;
    start = timeMS();
    Tango::DevState stateRead;
    Tango::DeviceAttribute da;
    do{
        now = timeMS();

        if( (now - start) > 10000){
            Tango::Except::throw_exception("WaitStateTimeOut","the state " + string(Tango::DevStateName[stateToReach]) + " not reach on attribute " + stateAttributeName ,"WhistBunchClock::waitState(" + stateAttributeName + "," +Tango::DevStateName[stateToReach]+ ")");
        }
        //read attr
        da = device.read_attribute(stateAttributeName.c_str());
        da >> stateRead;
        if(stateRead != stateToReach){
            cout << "state:" << Tango::DevStateName[stateRead] << " state attended:"<< Tango::DevStateName[stateToReach] <<endl;
            usleep(200000);
        }
        //cout << std::dec << (now - start) << endl;
    }while(stateRead != stateToReach);
    cout << "waitState:" << Tango::DevStateName[stateToReach] << " ";
    cout << std::dec << (now - start) << "ms" << endl;
}

string WhistUtil::extractDeviceName(string & attributeName,string propName){
    vector<string> splits = split(attributeName,'/');
    if(splits.size() != 4){
        Tango::Except::throw_exception("InvalidePropertiesContante", "Please set a attribute name in the property \""+ propName + "\"","PhaseShifter::extractDeviceName()");
    }
    return splits[0] + "/" + splits[1] + "/" + splits[2];
}


/**
 * for update __Value atributeProperty
 **/
void WhistUtil::updateAttributePropertie(Tango::DbDevice * db_dev, const string &attrName, const string &propName, const string &attrValue) {
    // set correct attribute property
    Tango::DbDatum datumBase(attrName), datumNameContent(propName);
    Tango::DbData dataNew;
    string attrValueNoConst = attrValue;


    datumBase << (short) 1;

    datumNameContent << attrValueNoConst;
    dataNew.push_back(datumBase);
    dataNew.push_back(datumNameContent);

    db_dev->put_attribute_property(dataNew);
}
//=============================================================================
//=============================================================================

 
 /**
  *for get __Value of atributeProperty
  **/
string WhistUtil::getAttributePropertie(Tango::DbDevice * db_dev,const string &attrName,const string &propName) {
    Tango::DbData data;

    data.push_back(Tango::DbDatum(attrName));

    db_dev->get_attribute_property(data);

    for(unsigned int i = 0 ; i < data.size();i++){
        if(data[i].name == "__value"){
            string value;
            data[i] >> value;
            if(value.empty()){
                return "";
            }
            return value;
        }
    }    
    return "";
}


/**
  * return the content of the free Property Whist/BunchClockDeviceName
  * Otherwise return a DevFailed exception
  **/
string WhistUtil::getBunchClockDeviceName(void){
    string rtn;
    Tango::DbData	prop;
    prop.push_back(Tango::DbDatum("BunchClockDeviceName"));
    Tango::Database *db = Tango::Util::instance()->get_database();
    db->get_property("Whist", prop);
    if (prop[0].is_empty())
        Tango::Except::throw_exception(
                (const char *) "PropertyNotSet",
                (const char *) "Free property Whist/BunchClockDeviceName not set",
                (const char *) "WhistUtil::getBunchClockDeviceName()");

    prop[0] >> rtn;
    return rtn;
}


/**
  * return the content of the free Property Whist/EventManagerDeviceName
  * Otherwise return a DevFailed exception
  **/
string WhistUtil::getEventManagerDeviceName(void){
    string rtn;
    Tango::DbData	prop;
    prop.push_back(Tango::DbDatum("EventManagerDeviceName"));
    Tango::Database *db = Tango::Util::instance()->get_database();
    db->get_property("Whist", prop);
    if (prop[0].is_empty())
        Tango::Except::throw_exception(
                (const char *) "PropertyNotSet",
                (const char *) "Free property Whist/EventManagerDeviceName not set",
                (const char *) "WhistUtil::getEventManagerDeviceName()");

    prop[0] >> rtn;
    return rtn;
}


Tango::DevShort WhistUtil::getCurrentSignal(uint32_t regDataWidthConf){
    uint32_t source = BUCLK_WID_1_SRCE_R(regDataWidthConf);

    if(source == WhistUtil::SOURCE_T0){
        return 0;
    }else if(source == WhistUtil::SOURCE_INJ){
        return 1;
    }else if(source == WhistUtil::SOURCE_EXT){
        return 2;
    }else if(source == WhistUtil::SOURCE_KI){
        return 3;
    }else{
        return 4;
    }
}