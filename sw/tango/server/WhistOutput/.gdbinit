Signal        Stop      Print   Pass to program  Description

SIGHUP        Yes       Yes     Yes              Hangup
SIGINT        Yes       Yes     No               Interrupt
SIGQUIT       Yes       Yes     Yes              Quit
SIGILL        Yes       Yes     Yes              Illegal instruction
SIGTRAP       Yes       Yes     No               Trace/breakpoint trap
