/*----- PROTECTED REGION ID(PhaseShifter.cpp) ENABLED START -----*/
//=============================================================================
//
// file :        PhaseShifter.cpp
//
// description : C++ source for the PhaseShifter class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               PhaseShifter are implemented in this file.
//
// project :     PhaseShifter
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//
// Copyright (C): 2018
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <PhaseShifter.h>
#include <PhaseShifterClass.h>

/*----- PROTECTED REGION END -----*/	//	PhaseShifter.cpp

/**
 *  PhaseShifter class description:
 *    new phase shifter for the whist system
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  Inherited (no method)
//  Status        |  Inherited (no method)
//================================================================

//================================================================
//  Attributes managed are:
//================================================================
//  phase_SY_SR           |  Tango::DevDouble	Scalar
//  phase_PREBUNCHING_SY  |  Tango::DevDouble	Scalar
//  phase_GUN_SY          |  Tango::DevDouble	Scalar
//================================================================

namespace PhaseShifter_ns
{
/*----- PROTECTED REGION ID(PhaseShifter::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	PhaseShifter::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::PhaseShifter()
 *	Description : Constructors for a Tango device
 *                implementing the classPhaseShifter
 */
//--------------------------------------------------------
PhaseShifter::PhaseShifter(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(PhaseShifter::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::constructor_1
}
//--------------------------------------------------------
PhaseShifter::PhaseShifter(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(PhaseShifter::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::constructor_2
}
//--------------------------------------------------------
PhaseShifter::PhaseShifter(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(PhaseShifter::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void PhaseShifter::delete_device()
{
	DEBUG_STREAM << "PhaseShifter::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(PhaseShifter::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
        delete sySrDp;
        delete prebunchingSyDp;
        delete gunSyDp;
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::delete_device
	delete[] attr_phase_SY_SR_read;
	delete[] attr_phase_PREBUNCHING_SY_read;
	delete[] attr_phase_GUN_SY_read;
}

//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void PhaseShifter::init_device()
{
	DEBUG_STREAM << "PhaseShifter::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(PhaseShifter::init_device_before) ENABLED START -----*/
	
        lastRead = WhistUtil::timeMS();
	//	Initialization before get_device_property() call
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::init_device_before
	

	//	Get the device properties from database
	get_device_property();
	
	attr_phase_SY_SR_read = new Tango::DevDouble[1];
	attr_phase_PREBUNCHING_SY_read = new Tango::DevDouble[1];
	attr_phase_GUN_SY_read = new Tango::DevDouble[1];
	/*----- PROTECTED REGION ID(PhaseShifter::init_device) ENABLED START -----*/
	
	//	Initialize device
        sySrAttrId = this->get_device_attr()->get_attr_ind_by_name("phase_SY_SR");
        prebunchingSyAttrId = this->get_device_attr()->get_attr_ind_by_name("phase_PREBUNCHING_SY");
        gunSyAttrId = this->get_device_attr()->get_attr_ind_by_name("phase_GUN_SY");
        
        string devicePhaseSySr , devicePhasePrebunchingSy , devicePhaseGunSy;
        devicePhaseSySr = WhistUtil::extractDeviceName(attributePhaseSySr,"attributePhaseSySr");
        devicePhasePrebunchingSy = WhistUtil::extractDeviceName(attributePhasePrebunchingSy,"attributePhasePrebunchingSy");
        devicePhaseGunSy = WhistUtil::extractDeviceName(attributePhaseGunSy,"attributePhaseGunSy");
        sySrAttrName = WhistUtil::split(attributePhaseSySr,'/')[3];
        prebunchingSyAttrName = WhistUtil::split(attributePhasePrebunchingSy,'/')[3];
        gunSyAttrName = WhistUtil::split(attributePhaseGunSy,'/')[3];

        sySrDp = new Tango::DeviceProxy(devicePhaseSySr);
        prebunchingSyDp = new Tango::DeviceProxy(devicePhasePrebunchingSy);
        gunSyDp = new Tango::DeviceProxy(devicePhaseGunSy);
        
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::init_device
}

//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void PhaseShifter::get_device_property()
{
	/*----- PROTECTED REGION ID(PhaseShifter::get_device_property_before) ENABLED START -----*/
	
	//	Initialize property data members
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::get_device_property_before


	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("attributePhaseSySr"));
	dev_prop.push_back(Tango::DbDatum("attributePhasePrebunchingSy"));
	dev_prop.push_back(Tango::DbDatum("attributePhaseGunSy"));
	dev_prop.push_back(Tango::DbDatum("refreshTime"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on PhaseShifterClass to get class property
		Tango::DbDatum	def_prop, cl_prop;
		PhaseShifterClass	*ds_class =
			(static_cast<PhaseShifterClass *>(get_device_class()));
		int	i = -1;

		//	Try to initialize attributePhaseSySr from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  attributePhaseSySr;
		else {
			//	Try to initialize attributePhaseSySr from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  attributePhaseSySr;
		}
		//	And try to extract attributePhaseSySr value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  attributePhaseSySr;

		//	Try to initialize attributePhasePrebunchingSy from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  attributePhasePrebunchingSy;
		else {
			//	Try to initialize attributePhasePrebunchingSy from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  attributePhasePrebunchingSy;
		}
		//	And try to extract attributePhasePrebunchingSy value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  attributePhasePrebunchingSy;

		//	Try to initialize attributePhaseGunSy from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  attributePhaseGunSy;
		else {
			//	Try to initialize attributePhaseGunSy from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  attributePhaseGunSy;
		}
		//	And try to extract attributePhaseGunSy value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  attributePhaseGunSy;

		//	Try to initialize refreshTime from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  refreshTime;
		else {
			//	Try to initialize refreshTime from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  refreshTime;
		}
		//	And try to extract refreshTime value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  refreshTime;

	}

	/*----- PROTECTED REGION ID(PhaseShifter::get_device_property_after) ENABLED START -----*/
	
	//	Check device property data members init
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::get_device_property_after
}

//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void PhaseShifter::always_executed_hook()
{
	DEBUG_STREAM << "PhaseShifter::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(PhaseShifter::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void PhaseShifter::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "PhaseShifter::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(PhaseShifter::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
    long now = WhistUtil::timeMS();
    if (now - lastRead >= refreshTime/*1sec*/) {
        lastRead = now;

        try{
            long idSySr = sySrDp->read_attribute_asynch(sySrAttrName);
            long idPrebunchingSy = prebunchingSyDp->read_attribute_asynch(prebunchingSyAttrName);
            long idGunSy = gunSyDp->read_attribute_asynch(gunSyAttrName);

            double syPhase,preBunchingPhase,gunPhase;
            (*sySrDp->read_attribute_reply(idSySr,800)) >> syPhase;
            (*prebunchingSyDp->read_attribute_reply(idPrebunchingSy,800)) >> preBunchingPhase;
            (*gunSyDp->read_attribute_reply(idGunSy,800)) >> gunPhase;

            *attr_phase_SY_SR_read = syPhase;
            this->get_device_attr()->get_w_attr_by_ind(sySrAttrId).get_write_value(syPhase);
            *attr_phase_PREBUNCHING_SY_read = preBunchingPhase - syPhase;
            *attr_phase_GUN_SY_read = gunPhase + syPhase;

            set_state(Tango::ON);
            set_status("All work fine");
        }catch (Tango::DevFailed &ex){
            //Tango::Except::print_exception(ex.);
            *attr_phase_SY_SR_read = NAN;
            *attr_phase_SY_SR_read = NAN;
            *attr_phase_GUN_SY_read = NAN;
            set_state(Tango::UNKNOWN);
            string status;
            status = ex.errors[0].desc;
            set_status(status);
        }
    }
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::read_attr_hardware
}
//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::write_attr_hardware()
 *	Description : Hardware writing for attributes
 */
//--------------------------------------------------------
void PhaseShifter::write_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "PhaseShifter::write_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(PhaseShifter::write_attr_hardware) ENABLED START -----*/

	//	Add your own code
    for(unsigned int i = 0 ;i < attr_list.size();i++){
        if(attr_list[i] == sySrAttrId){
            Tango::DeviceAttribute daSySr(sySrAttrName,attr_phase_SY_SR_write);
            sySrDp->write_attribute(daSySr);

            this->get_device_attr()->get_w_attr_by_ind(prebunchingSyAttrId).get_write_value(attr_phase_PREBUNCHING_SY_write);
            this->get_device_attr()->get_w_attr_by_ind(gunSyAttrId).get_write_value(attr_phase_GUN_SY_write);

            calcAndWritePrebunchingSy();

            calcAndWriteGunSy();

        }else {
            this->get_device_attr()->get_w_attr_by_ind(sySrAttrId).get_write_value(attr_phase_SY_SR_write);
        }

        if(attr_list[i] == prebunchingSyAttrId){
            calcAndWritePrebunchingSy();
        }else if(attr_list[i] == gunSyAttrId){
            calcAndWriteGunSy();
        }
    }
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::write_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute phase_SY_SR related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void PhaseShifter::read_phase_SY_SR(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PhaseShifter::read_phase_SY_SR(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(PhaseShifter::read_phase_SY_SR) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_phase_SY_SR_read);
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::read_phase_SY_SR
}
//--------------------------------------------------------
/**
 *	Write attribute phase_SY_SR related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void PhaseShifter::write_phase_SY_SR(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PhaseShifter::write_phase_SY_SR(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(PhaseShifter::write_phase_SY_SR) ENABLED START -----*/

	attr_phase_SY_SR_write = w_val;
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::write_phase_SY_SR
}
//--------------------------------------------------------
/**
 *	Read attribute phase_PREBUNCHING_SY related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void PhaseShifter::read_phase_PREBUNCHING_SY(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PhaseShifter::read_phase_PREBUNCHING_SY(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(PhaseShifter::read_phase_PREBUNCHING_SY) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_phase_PREBUNCHING_SY_read);
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::read_phase_PREBUNCHING_SY
}
//--------------------------------------------------------
/**
 *	Write attribute phase_PREBUNCHING_SY related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void PhaseShifter::write_phase_PREBUNCHING_SY(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PhaseShifter::write_phase_PREBUNCHING_SY(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(PhaseShifter::write_phase_PREBUNCHING_SY) ENABLED START -----*/

    attr_phase_PREBUNCHING_SY_write = w_val;
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::write_phase_PREBUNCHING_SY
}
//--------------------------------------------------------
/**
 *	Read attribute phase_GUN_SY related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void PhaseShifter::read_phase_GUN_SY(Tango::Attribute &attr)
{
	DEBUG_STREAM << "PhaseShifter::read_phase_GUN_SY(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(PhaseShifter::read_phase_GUN_SY) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_phase_GUN_SY_read);
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::read_phase_GUN_SY
}
//--------------------------------------------------------
/**
 *	Write attribute phase_GUN_SY related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void PhaseShifter::write_phase_GUN_SY(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "PhaseShifter::write_phase_GUN_SY(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(PhaseShifter::write_phase_GUN_SY) ENABLED START -----*/

    attr_phase_GUN_SY_write = w_val;
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::write_phase_GUN_SY
}

//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void PhaseShifter::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(PhaseShifter::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Method      : PhaseShifter::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void PhaseShifter::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(PhaseShifter::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	PhaseShifter::add_dynamic_commands
}

/*----- PROTECTED REGION ID(PhaseShifter::namespace_ending) ENABLED START -----*/

//	Additional Methods
void PhaseShifter::calcAndWritePrebunchingSy(){
    
    double setPointPrebunching = attr_phase_PREBUNCHING_SY_write + attr_phase_SY_SR_write;
    Tango::DeviceAttribute daPreSy(prebunchingSyAttrName,setPointPrebunching);
    prebunchingSyDp->write_attribute(daPreSy);
    
}

void PhaseShifter::calcAndWriteGunSy(){
 
    double setPointGun = attr_phase_GUN_SY_write - attr_phase_SY_SR_write;
    Tango::DeviceAttribute daGunSy(gunSyAttrName,setPointGun);
    gunSyDp->write_attribute(daGunSy);
    
}

/*----- PROTECTED REGION END -----*/	//	PhaseShifter::namespace_ending
} //	namespace
