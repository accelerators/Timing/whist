#!/bin/env python

from __future__ import print_function
import sys
import csv
#import string
import time
import datetime as pendul

import numpy as np

from whist_whdds import SpecWhist
from whkit import *

# ------------------------------------------------------
def bli(wliste, buckparam, opt):
# ------------------------------------------------------
    """Single_bunch list
       sequential bucket numbers
             menu :[min, max] default [0,991]
       devices involved : default wliste, select via menu
       stop master_seqen
       load devices
       start master_seqen
       """
    # build up module list if not all
    if opt == "a":
        loclist = wliste
    else:
        fin = False
        loclist = []
        while not fin:
            choice = raw_input(" >> Module bus number for test : ")
            m0 = get_v(0, choice)
            if m0 in wliste:
                loclist.append(m0)
            else:
                print("module not declared")
    # define bucket list
    choi = raw_input(" >> Bucket list : [0..991] (default=CR) or enter start stop : ")
    if choi == "":
        bkl = range(0, 992)
    else:
        asta = get_v(0, choi)
        asto = get_v(1, choi)
        if asto == -1: asto = 991
        if asta < asto:
            bkl = range(asta, asto+1)
        else:
            bkl1 = range(asta, 992)
            bkl2 = range(0, asto+1)
            bkl = bkl1 + bkl2
    print("bkl={}".format(bkl))
    # erase seqen
    seqen(wliste[0], "-w", 0, "-noverbose")
    # make sure buckparam everywhere
    # ... and fill default bunchlist in module list
    # ... reset injerror
    # ... and set autoblist
    for dev in loclist:
        dwprog(dev, buckparam)
        blifill(dev, bkl)
        injerrst(dev)
        autoblist(dev)
    # enable seqen
    seqen(wliste[0], "-w", 1, "-noverbose")

# ------------------------------------------------------
def blifill(buss, liste):
# ------------------------------------------------------
    """Fill device bunch list from address 0
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    
    for jj in range(0, len(liste)):
        blad = jj*4
        dd.ddsSetBL(blad, liste[jj])

    dd.ddsSetBlistStart(0)
    dd.ddsSetBlistStop(len(liste)-1)

# ------------------------------------------------------
def blirst(buss):
# ------------------------------------------------------
    """reset bunch list pointer on module
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    
    dd.ddsSetBlistRst()

# ------------------------------------------------------
def autobl(buss, tsr):
# ------------------------------------------------------
    """Test module AUTOBLIST bit
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    # test
    if (tsr == "-t"): return dd.ddsTestAutoBlist()[0]
    # set
    if (tsr == "-s"): dd.ddsSetAutoBlist()
    # reset
    if (tsr == "-r"): dd.ddsClearAutoBlist()

# ------------------------------------------------------
def autoro(buss, tsr):
# ------------------------------------------------------
    """Test AUTOROTINJ bit
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    # test
    if (tsr == "-t"): return dd.ddsTestAutoRotinj()[0]
    # set
    if (tsr == "-s"): dd.ddsSetAutoRotinj()
    # reset
    if (tsr == "-r"): dd.ddsClearAutoRotinj()

# ------------------------------------------------------
def mbsetup(buss):
# ------------------------------------------------------
    """Set mb & autoblist bits to prepare multi-bunch mode
       Clear other uncompatible bits"""
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    
    dd.ddsSetAutoBlist()
    dd.ddsSetMBEn()
    dd.ddsClearRotinj()
    dd.ddsClearAutoRotinj()

# ------------------------------------------------------
def rotinj(buss, eneb, oto, debu):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    if (eneb == 0) or (eneb == -1):
        dd.ddsClearRotinj()
        dd.ddsClearAutoRotinj()
    else:
        if (debu < 0) or (debu > 991):
            print("Bad bunch number {}".format(debu))
            return False
        if (oto == 0) or (oto == -1):
            dd.ddsClearAutoRotinj()
        else:
            dd.ddsSetAutoRotinj()
        delta = 352/4
        # organize lists for HW programming then program HW memory
        dd.ddsSetBlistStart(0x0)
        blad = 0x0
        # just write delta consecutive numbers starting from debu
        for bu in range(delta):
            jecri = (debu + bu)%992
            dd.ddsSetBL(blad, jecri)
            print("at {} : {}".format(hex(blad), jecri))
            blad += 4
        dd.ddsSetBlistStop((blad/4)-1)
        dd.ddsClearAutoBlist()
        dd.ddsClearMBEn()
        dd.ddsSetRotinj()

# ------------------------------------------------------
def menuo(buss, outnu):
# ------------------------------------------------------
    print("Available functions for OUT#{} :".format(outnu))
    print("Disable -----> 99")
    print("Clock :")
    print("   TC32 -----> 0")
    print("   SR  ------> 1")
    print("   Booster --> 2")
    print("   SRDIV ----> 3")
    if outnu == 1:
        print("   CLK_16b --> 4")
        print("   CLK_4b  --> 5")
    print("Pulse :")
    if outnu not in range(1, 4): print("   T0 -------> 20")
    if outnu not in range(2, 4):
        print("   INJ ------> 21")
        print("   EXT ------> 22")
    print("   Ki -------> 23")
    if outnu in range(2, 4): print("   GUN ------> 11")
    ych = raw_input("     >>> your choice : ")
    conf = get_v(0, ych)
    # Disable : enable=0, others=0
    if (conf == 99): return ckprog(buss, outnu, 0, 0, 0, 0, 0)
    # GUN
    if conf == 11:
        pola = raw_input("     >>> Polarity (0=pos, 1=neg) : ")
        pp = get_v(0, pola)
        if (pp != 1): pp = 0
        # for gun puprog with enable=1, source=1, others=0
        return puprog(buss, outnu, 1, pp, 1, 0, 0, 0)
    # clocks
    if conf in range(0, 6):
        # SRDIV
        if (conf == 3):
            dewi = raw_input("     >>> Div_factor 0-DivFactor_phase 0-992_phase : ")
            df = get_v(0, dewi)
            dfd = get_v(1, dewi)
            if (dfd <= 0): dfd = 0
            srd = get_v(2, dewi)
            if (srd not in range(0, 992)): srd = 0
            # full srdiv with all parameters
            return srdiv(buss, outnu, df, dfd, srd)
        # other clocks
        else:
            dewi = raw_input("     >>>  polarity 0-992_phase : ")
            pp = get_v(0, dewi)
            if (pp != 1): pp = 0
            ph = get_v(1, dewi)
            if ph not in range(0, 992): ph = 0
            # enable=1, last arg=0
            if outnu == 1:
                return ckprog(buss, outnu, 1, conf, 0, 0, 0)
            elif conf in range(0, 3):
                return ckprog(buss, outnu, 1, conf, 0, 0, 0)
            else: return False
    # pulses : valid conf values are filtered by menu
    if conf in range(20, 24):
        dewi = raw_input("     >>> Polarity Pulse_Delay Pulse_Width : ")
        pp = get_v(0, dewi)
        if (pp != 1): pp = 0
        ph = get_v(1, dewi)
        if ph  <0: ph = 0
        ww = get_v(2, dewi)
        if ww < 0: ww = 0
        # source=2
        return puprog(buss, outnu, 1, pp, 2, conf-20, ph, ww)
        
        
          
