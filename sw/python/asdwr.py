#!/bin/env python
# -*- coding: utf-8 -*-
#title           :asdwr.py
#description     :This program displays an interactive menu on CLI
#author          :GG from menu_exple.py
#date            :
#version         :0.1sstup
#usage           :python2.7 asdwr.py
#notes           :
#python_version  :3+  
#=======================================================================
# Import the modules needed to run the script.
import sys
import readline
import datetime as pendul
import os

from whkit import *
 
# Main definition - constants
menu_actions  = {}  
 
# =======================
#     MENUS FUNCTIONS
# =======================
# Main menu
def main_menu():
#    os.system('clear')
    global pasini

    if pasini == False:
        menu_ini()
        pasini = True
    else:
        choice = raw_input("ebs>>  ")
        exec_menu(choice)
 
    return
 
# Execute menu
def exec_menu(choice):
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print("Invalid whebs.py selection, please try again.\n")
            menu_actions['main_menu']()
    return

# initialization
def menu_ini():
    global wlist
    global wnames
    global wms
    global buckparam
    rr = 0
    # ckeck current setup file
    suer = False
    try:
        with open('whistlut.csv', "r") as lut:
            vola = csv.DictReader(lut,delimiter=",")
            #print("vola = {}".format(vola))
            wlist = []
            wnames = []
            wms = []
            for line in vola:
                wnames.append(line['NAME'])
                wlist.append(int(line['BUS']))
                wms.append(int(line['MS']))
            print("currentsu = {}".format(sorted(wlist)))
            print("current names = {}".format(wnames))
    except IOError:
        print("No current setup file.")
        exit()

# input whist number or name
def veribus(liste, names, menu):
    buso = raw_input("   >> " + menu + " > whist number or name :  ")
    dex = -1
    vlu = buso.lower()
    try:
        buss = -1
        if vlu.startswith("0X") or vlu.startswith("0x"):
            buss = int(vlu, base=16)
        else:
            buss = int(vlu)
        if buss >= 0:
            if (buss in liste):
                dex = liste.index(buss)
    except Exception:
        buss = vlu
        if (buss.strip('-') in names):
            dex = names.index(buss.strip('-'))
    return dex

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! module commands !!!!!!!!!!!!!!!!!!!

# board status : light
def menu_st():
    print("board status : enter board_bus_address or name")
    dex = veribus(wlist, wnames, "st")
    if wms[dex] ==  1:
        stm(wlist[dex], wnames[dex])
    else:
        sts(wlist[dex], wnames[dex])

# board status : full
def menu_stfull():
    print("board status : enter board_bus_address or name")
    dex = veribus(wlist, wnames, "stfull")
    if dex >= 0:
        print("WHIST#{} - {} : FULL STATUS".format(wlist[dex], wnames[dex]))
        statspec(wlist[dex])

# outputs status
def menu_os():
    print("   outputs status : enter board_bus_address or name")
    dex = veribus(wlist, wnames, "os")
    if dex >= 0:
        print("\n---> WHIST#{} - {}".format(wlist[dex], wnames[dex]))
        toto = statout(wlist[dex],  "-v")
    return toto

# inputs status
def menu_is():
    print("   inputs status : enter board_bus_address or name")
    dex = veribus(wlist, wnames, "is")
    if dex >= 0:
        print("\n---> WHIST#{} - {}".format(wlist[dex], wnames[dex]))
        toto = statin(wlist[dex], "-v")
    return toto

# t0 watch dog menu
def menu_t0wd():
    print("   T0 Watch Dog menu : enter board_bus_address or name")
    dex = veribus(wlist, wnames, "t0wd")
    if dex >= 0:
        print("\n---> WHIST#{} - {}".format(wlist[dex], wnames[dex]))
        toto = t0wd(wlist[dex], "-v")
    else:
        toto = [-1, -1]
    return toto

# check T0 counters
def menu_t0stat():
    print("   T0 counters status : enter board_bus_address or name")
    dex = veribus(wlist, wnames, "t0stat")
    if wms[dex] == 0:
        print("\n---> WHIST#{}-SLAVE - {}".format(wlist[dex], wnames[dex]))
    else:
        print("\n---> WHIST#{}-MASTER - {}".format(wlist[dex], wnames[dex]))
    toto = t0stat(wlist[dex], wms[dex], "-v")
    return toto

# board RF debug status
def menu_rfdbg():
    print("   RF debug : enter board_bus_address or name")
    dex = veribus(wlist, wnames, "rfdbg")
    if dex >= 0:
        print("\n---> WHIST#{} - {}".format(wlist[dex], wnames[dex]))
        toto = rfdbg(wlist[dex], "-v")
    else:
        toto = [-1, -1, -1, -1]
    return toto

# input conf
def menu_setin():
    print("Configure inputs : bus ch en pol threshold")
    choice = raw_input(" >> setin > bus ch 1/0 0=pos/1=neg level(<= 0x80):  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            setin(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# read (empty) input fifo
def menu_rdin():
    print("Read input fifo till empty or 256 words : bus ch")
    choice = raw_input(" >> rdin > bus ch :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            rdin(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_srdiv():
    print("CLK_SR_DIV menu : bus channel division_factor coarse_phase(<div) SR_phase(0-992)")
    choice = raw_input(" >> SR_count > bus ch div phsr ph :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            srdiv(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_ssrdiv():
    print("Synchronize SRDIV clocks on slaves a&b : bus_a bus_b")
    choice = raw_input(" >> SYNC_SRDIV > bus_a bus_b :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            ssrdiv(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_rsrdiv():
    print("Display explicit parameters of SRDIV outputs (if any). arg : bus")
    choice = raw_input(" >> Read SRDIV > bus :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            srdivout(choice, "-v")
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! network commands !!!!!!!!!!!!!!!!!!!

# versions
def menu_nw_version():
    print("Network hard&software versions")
    print("{:7s}, {:15s}, {:5s}, {:8s}, {:8s}".format("WHIST#", "NAME", "FPGA", "BccCore", "EvtCore"))
    for idev in range(len(wlist)):
        tv = statversion(wlist[idev])
        print("{:<7d}, {:15s}, {:<5d}, {:<8d}, {:<8d}".format(wlist[idev], wnames[idev], tv[0], tv[1], tv[2]))
    
# network T0 watch dog status
def menu_nw_t0wd():
    print("NETWORK T0 WATCH DOG STATUS")
    t0wd_disa = []
    t0_bit = []

    for dev in wlist:
        toto = t0wd(dev, "-noverbose")
        if toto[0] == 0: t0wd_disa.append(dev)
        if toto[1] == 1: t0_bit.append(dev)
    if t0wd_disa == []:
        print("     WR network T0 WATCH DOG enabled : OK")
    else:
        print("!!!!!T0 WATCH DOG DISABLED on {}.".format(t0wd_disa))
    if t0_bit == []:
        print("     WR network T0 WATCH DOG : OK")
    else:
        print("!!!!!WATCH DOG BIT TRIGGERED on {}.".format(t0_bit))

# network check of T0 counters - full dispay
def menu_nw_t0stat():
    global wlist
    # make sure seqen is inactive
    seqpause = seqen(wlist[0], "-r", 0, "-n")
    if seqpause[0] == 1:
        print("Sequencer is enabled.\nJust checking last T0 broadcast lost")
        for dev in range(1, len(wms)):
            lt0 = t0stat(wlist[dev], wms[dev], "-n")
            if lt0[2] > 0:
                print("\n     dev#{} !!!!! T0bis = {} < T0_cnt = {}".format(wlist[dev], lt0[1], lt0[0]))
                print("     ---> TAI sec/cyc = {}/{}".format(lt0[2], lt0[3]))
                temps = pendul.datetime(1970, 1, 1, 0, 0, 0) + pendul.timedelta(seconds=lt0[2])
                print("   !!!! TAI time of last T0 broadcast lost = {}".format(temps))
    else:
        #get t0_cnt from wlist[0] as reference
        print("NETWORK T0 STATUS")
        lt0 = t0stat(wlist[0], wms[0], "-n")
        if wms[0] == 1:
            print("     DEV#{}    -->  MASTER_T0_cnt = {}".format(wlist[0], lt0[0]))
        else:
            print("     DEV#{}    -->  Reference_Slave_T0_cnt = {}".format(wlist[0], lt0[0]))
        for dev in range(1, len(wms)):
            lt0 = t0stat(wlist[dev], wms[dev], "-n")
            if lt0[0] == lt0[1]:
                if lt0[2] == 0:
                    print("     dev#{} OK".format(wlist[dev]))
                else:
                    print("     dev#{} T0 counters OK - found TAI sec/cyc = {}/{}".format(wlist[dev], lt0[2], lt0[3]))
            elif lt0[0] < lt0[1]:
                print("     dev#{} !!!!! T0bis = {} > T0_cnt = {}".format(wlist[dev], lt0[1], lt0[0]))
                print("     ---> TAI sec/cyc = {}/{}".format(lt0[2], lt0[3]))
                temps = pendul.datetime(1970, 1, 1, 0, 0, 0) + pendul.timedelta(seconds=lt0[2])
                print("   !!!! TAI time of last T0 broadcast lost = {}".format(temps))
            else:
                print("     dev#{} !!!!! T0bis = {} < T0_cnt = {}".format(wlist[dev], lt0[1], lt0[0]))
                temps = pendul.datetime(1970, 1, 1, 0, 0, 0) + pendul.timedelta(seconds=lt0[2])
                print("   !!!! TAI time of last T0 broadcast lost = {}".format(temps))
                print("     ---> TAI sec/cyc = {}/{}".format(lt0[2], lt0[3]))

# network check of T0 counters - write to csv type file
def menu_nw_t0log():
    global wlist
    # list last registered TAI

    aecrire = open('t0log', "w")
    skr = csv.DictWriter(aecrire, ('BUS', 'TAI', 'CYC', 'NAME'),
                         dialect=csv.excel, extrasaction='ignore', restval='',
                             delimiter=',')
    skr.writeheader()
    for dev in range(1, len(wlist)):
        lt0 = t0stat(wlist[dev], wms[dev], "-n")
        uneligne = {'BUS':wlist[dev], 'TAI':lt0[2], 'CYC':lt0[3], 'NAME':wnames[dev]}
        skr.writerow(uneligne)
    aecrire.close()

# network RF debug status
def menu_nw_rfdbg():
    print("NETWORK RF STATUS")
    l_352 = []
    rf_lost = []
    adjted = []
    dben = []
    slokaok = []
    sloknok = []
    for dev in wlist[1:]:
        toto = rfdbg(dev, "-noverbose")
        if toto[0] == 0: l_352.append(dev)
        if toto[1] == 1: rf_lost.append(dev)
        if toto[2] == 1: adjted.append(dev)
        if toto[3] == 0: dben.append(dev)
        if toto[4] == 0: slokaok.append(dev)
        if toto[5] == 1: sloknok.append(dev)
    if dben == []:
        print("   RF DBG enabled : EVERYWHERE")
    else:
        print("   !!!!! RF DBG disabled on : {}".format(dben))
    if l_352 == []:
        print("   352_locked :     EVERYWHERE")
    else:
        print("   !!!!! 352 unlocked on : {}".format(l_352))
    if rf_lost == []:
        print("   RF_LOST :        NOWHERE")
    else:
        print("   !!!!! RF_LOST on : {}".format(rf_lost))
    if adjted == []:
        print("   RF adjust needed :    NOWHERE")
    else:
        print("   !!!!! RF adjust needed on : {}".format(adjted))
    if slokaok == []:
        print("   SLOK OK :        EVERYWHERE")
    else:
        print("   !!!!! Bad SLOK on : {}".format(slokaok))
    if sloknok == []:
        print("   SLOK NOT OK :    NOWHERE")
    else:
        print("   !!!!! SLOK NOT OK on : {}".format(sloknok))

# check injection / extraction parameters
def menu_nw_dw():
    print("NETWORK BUNCH CLOCK parameters consistency : del_inj del_ext wid_inj wid_ext")
    print("   REFERENCE : WHIST#{}".format(wlist[0]))
    idw = dwrd(wlist[0])
    print("   --> WHIST#{} : RF/8 = {}".format(wlist[0], idw[5]))
    print("          TANGO : Tinj (HW : t0_del)  = 0x{:0x} -> {:e}s.".format(idw[0], float(idw[0]*8) / float(OLDRF)))
    print("                                         actual -> {:e}s.".format(float(idw[0]) / float(idw[5])))
    print("          TANGO : Gun (HW : Injection delay)  = 0x{:0x} -> {:e}s / width = 0x{:0x}".format(idw[1], float(idw[1]*8) / float(OLDRF), idw[3]))
    print("                                                 actual -> {:e}s / width = 0x{:0x}".format(float(idw[1]) / float(idw[5]), idw[3]))
    print("          TANGO : Text (HW : Extraction delay) = 0x{:0x} -> {:e}s  / width = 0x{:0x}".format(idw[2], float(idw[2]) / (float(OLDRF)/(31*32*11)), idw[4]))
    print("                                                  actual -> {:e}s  / width = 0x{:0x}".format(float(idw[2]) / (float(idw[5])/(31*4*11)), idw[4]))
    discrep = 0
    for dev in wlist[1:]:
        toto = dwrd(dev)
        if toto[0] != idw[0]:
            print("   --> WHIST#{} TANGO Tinj (HW : t0_del) = 0x{:0x} -> {:e}s.".format(dev, toto[0], float(toto[0]) / float(idw[5])))
            discrep += 1
        if toto[1] != idw[1]:
            print("   --> WHIST#{} TANGO Gun (HW : inj_del) = 0x{:0x} -> {:e}s.".format(dev, toto[1], float(toto[1]) / float(idw[5])))
            discrep += 1
        if toto[3] != idw[3]:
            print("   --> WHIST#{} (HW : Sequencer Tinj pulse width : inj_wid) = 0x{:0x}.".format(dev, toto[3]))
            discrep += 1
        if toto[2] != idw[2]:
            print("   --> WHIST#{} TANGO Text (HW : ext_del) = 0x{:0x} -> {:e}s.".format(dev, toto[2], float(toto[2]) / (float(idw[5])/(4*31*11))))
            discrep += 1
        if toto[4] != idw[4]:
            print("   --> WHIST#{} HW : Sequencer Text pulse width : ext_wid) = 0x{:0x}.".format(dev, toto[4]))
            discrep += 1
    if discrep == 0: print("-------- ALL WHISTS OK with REFERENCE --------")
    
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! write expert debug commands !!!!!!!!!!!!!!!!!!!

# INJERR reset
def menu_res_injerr():
    print("Reset INJERR bit in module : enter board bus_address or name")
    dex = veribus(wlist, wnames, "INJERR_rst")
    if dex >= 0:
        trige = trigger(wlist[dex], "InjErrRst")
        if trige == True: print("....... WHIST#{} : Reset done".format(wlist[dex]))
        else: print("!!!!!!!!!! WHIST#{} : Reset failed".format(wlist[dex]))
    return trige

# T0LOST reset
def menu_res_t0lost():
    print("Reset T0LOST bit in WHIST module")
    dex = veribus(wlist, wnames, "T0_lost_rst")
    if dex >= 0:
        trige = trigger(wlist[dex], "InjErrRst")
        if trige == True: print("........ WHIST#{} : Reset done".format(wlist[dex]))
        else: print("!!!!!!!!!! WHIST#{} : Reset failed".format(wlist[dex]))
    return trige

# RFLOST and RF_ADJ_DET reset
def menu_res_rflost():
    print("Reset RFLOST and RF_ADJ_DET bits in WHIST module")
    dex = veribus(wlist, wnames, "RF_lost_rst")
    if dex >= 0:
        trige = trigger(wlist[dex], "RfLostRst")
        if trige == True: print("........ WHIST#{} : Reset done\n".format(wlist[dex]))
        else: print("!!!!!!!!!! WHIST#{} : Reset failed\n".format(wlist[dex]))
    return trige

# skip one RF clock
def menu_skip44():
    print("Skip ONE RF clock in WHIST module")
    dex = veribus(wlist, wnames, "Skip RF/8")
    if dex >= 0:
        js = trigger(wlist[dex], "Skip44")
        if js == True: print("........ WHIST#{} : RF_counter = RF-counter-1".format(wlist[dex]))
        else: print("!!!!!!!!!!  WHIST#{} : Operation failed".format(wlist[dex]))
    return js

# jump one RF clock
def menu_jump44():
    print("Jump forward ONE RF clock in WHIST module")
    dex = veribus(wlist, wnames, "Jump RF/8")
    if dex >= 0:
        js = trigger(wlist[dex], "Jump44")
        if js == True: print("........ WHIST#{} : RF_counter = RF-counter+1".format(wlist[dex]))
        else: print("!!!!!!!!!! WHIST#{} : Operation failed".format(wlist[dex]))
    return js

# Enable / Disable rfadj_en
def menu_rfadj_en():
    print("Control RFADJ_EN_BIT on WHIST slave module")
    dex = veribus(wlist, wnames, "RFADJ_EN control")
    if dex > 0:
        choua = raw_input("     >> enable [1] / disable [0, CR] :  ")
        chou = get_v(0, choua)
        if chou != 1: chou = 0
        rep = rfadj(wlist[dex], chou)
        print("rfadj_en {} on whist#{}".format(rep[1], rep[0]))
    else:
        print("rfadj only on slave")

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! older to do commands !!!!!!!!!!!!!!!!!!!

# bunch list read
def menu_blrd():
    print("bunch list read : give board_bus_address, start_ptr, stop_ptr")
    choice = raw_input(" >> bl > bus start_ptr stop_ptr :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            blread(ch)
        except KeyboardInterrupt:
            return
    return

# slave lock rf_counter to master
def menu_slok():
    print("rf_counter slave lock to master : bus")
    choice = raw_input(" >> slave lock RF > bus :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            slok(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# read i2c config registers status
def menu_ioread():
    print("i2c configuration registers status")
    choice = raw_input(" >> ioread > bus : ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            iord(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# ad9516/10 rw registers
def menu_ad95():
    print("ad9516 action : bus 0(WR)/1(RF) -r/w add val")
    choice = raw_input(" >> ad95 > bus 0(WR:9516)/1(RF:9510) -r/w add val :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            ad95(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# list ad9516-9510 registers
def menu_lad95():
    print("list ad9516 registers : bus 0(WR)/1(RF)")
    choice = raw_input(" >> lad95 > bus 0(WR)/1(RF) :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            lad95(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# manage RF/n output from AD9510
def menu_rfn():
    print("RF devided by n : bus 0(dis)/1(enable) n-value")
    choice = raw_input(" >> rfn > bus 0/1 n :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            rfn(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set DAC value
def menu_dac ():
    print("set dac for trigger inputs reference voltages : bus r/w channel value")
    choice = raw_input(" >> dac > bus -rw channel value :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            dac(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_pll23():
    print("set or test PLL fine delay phase adjust on channels 2-3")
    choice = raw_input(" >> pll23 > bus channel -sc(an)/-pi(coarse-phase) value 0/1_if(-pi) :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            pll23(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return



# set mode parameters
def menu_sb():
    print("modes : Bus AutoBlist MultiBunch")
    choice = raw_input(" >> sb > Bus AutoBlist MultiBunch :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            sbprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# global enables management
def menu_en():
    print("modes : Bus Bunch_en Rf_en Gun_inj_en")
    choice = raw_input(" >> sb > Bus buclk_en rf_en gun_inj_en:  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            enprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# bunch list fill
def menu_bl():
    os.system('clear')
    print("bunch list fill with ptr_value : give , start_ptr, stop_ptr, start_value")
    choice = raw_input(" >> bl > bus start_ptr stop_ptr start_value :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            bl_fill(ch)
        except KeyboardInterrupt:
            return
    return

# set multi-bunch delays
def menu_mb():
    print("parameters : bus del_1 to 4")
    choice = raw_input(" >> mb > bus up_to_10_bunch# :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            mbprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return


# pulse menu
def menu_pu():
    print("Pulse output : channel enable polarity mode source delay width")
    choice = raw_input(" >> pu > bus chan en pol mode srce del wid :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            puprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# 352 phase adjust menu
def menu_ph():
    print("352 MHz Phase adjust : FPGA_PLL/OUT_CK value")
    choice = raw_input(" >> ph > bus -fpga/-outck val-0-7 :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            phprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# clock menu
def menu_ck():
    print("Clock output : channel enable srce polarity phase")
    choice = raw_input(" >> ck > bus chan en srce pol phase :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            ckprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# load bunch list menu
def menu_ldbx():
    print("load bunch list from file : channel file_name")
    choice = raw_input(" >> ldbx > bus -file_name :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            ldbx(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# command
def menu_cmd():
    print("execute a SpecWhist class command : bus -command")
    choice = raw_input(" >> cmd > bus -cmd :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            libcmd(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# configure master or slave
def menu_ms():
    print("configure master or slave : bus -m -s")
    choice = raw_input(" >> ms > bus -m -s :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            msconf(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# load lm32 program
def menu_lm32():
    print("load lm32 program : bus index -file_path")
    choice = raw_input(" >> lm32 > bus index -file_path :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            lm32Loader(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set ioconf bitfield value
def menu_ioconf():
    print("i2c configuration some IO control bits : bus (-ttlo -clko -fplo -ck10 -vcxo) value")
    choice = raw_input(" >> ioconf > bus -bitfield value :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            ioc(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set fine delay value on channels 1-3
def menu_fdel():
    print("i2c configuration of fine delays : bus channel(1-3) value")
    choice = raw_input(" >> fine_delay > bus channel(1-3) value :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            fdel(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return


# Set Si571 parameters
def menu_siset():
    print("Set Si571 registers and set options -f(requency)/-hs(HS_DIV)/-n1(N1)/-rf(RFREQ)/-u(nlock)/-r(ecall) value")
    choice = raw_input(" >> Si571_set > bus -f/-hs/-n1/-rf/-u/-r value: ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            si571_set(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# display Si571 config
def menu_sidis():
    print("Display Si571")
    choice = raw_input(" >> Si571_dis > bus -f/-r : ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            si571_dis(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# purge bcc HMQOUT
def menu_purbcc ():
    print("DEBUG : purge bcc HMQOUT")
    choice = raw_input(" >> purbcc > bus : ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            purbcc(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# master start
def menu_mstart():
    print("master start : bus")
    choice = raw_input(" >> mstart > bus :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            mstart(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_dld():
    print("ad9510 Digital Lock Detect management : bus enable_9510 enable/reset_latch")
    choice = raw_input(" >> AD9510_DLD > bus dis/enable_9510(1/0) -l/-r(enable_latch/reset_latch) :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            dld(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_tsts():
    print("time stamping test : bus input0-3 gate_length(seconds)")
    choice = raw_input(" >> tsts > bus 0-3 s :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            tsts(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# display menu on help
def disp_menu():
    print("\nhlp or help : this")
    print("\n*********************************")
    print("MODULE commands")
    print("*********************************")
    print("1st PARAMETER IS WHIST LOCATION ON PCIe bus or WHIST name : ALWAYS MANDATORY")
    print("remaining parameters are optional, separated by SPACE, 0=unchanged or default")
    print("parameters are integers, decimal or hexa")
    print("!!!")
    print("is          : inputs status. args : bus")
    print("rdin        : read (empty) input fifo. args : bus/in")
    print("setin       : inputs configuration programming. args : bus/ch/en/pol/threshold")
    print("os          : outputs status. args : bus")
    print("st          : basic module status. arg : bus/name")
    print("stfull      : full module status. arg : bus/name")
    print("srdiv       : intuitive srdiv programming. arg : bus/output/div_factor/coarse_phase/fine_phase")
    print("ssrdiv      : synchronize SRDIV outputs. arg : bus/outputs")
    print("rsrdiv      : display explicit parameters of SRDIV outputs (if any). arg : bus")
    print("rfdbg       : RFoE status. arg : bus/name")
    print("t0stat      : check T0 & T0bis counters. arg : bus/name")
    print("t0wd        : T0 watch dog : parameters and status. arg : bus/name")
    print("\n*********************************")
    print("NETWORK commands")
    print("*********************************")
    print("check parameters consistency over current active network")
    print("nw_dw       : Bunch clock parameters consistency")
    print("nw_rfdbg    : RFoE status")
    print("nw_t0log    : write to file last T0bis lost")
    print("nw_t0stat   : check T0 & T0bis counters")
    print("nw_t0wd     : Watch dog status")
    print("nw_version  : FPGA & spftcore versions")
    print("\n*********************************")
    print("WRITE / EXPERT commands")
    print("*********************************")
    print("write_injerr_reset     : reset INJERR_bit (also resets T0_LOST_BIT")
    print("write_t0lost_reset     : reset T0_LOST_bit (also resets INJERR_BIT")
    print("write_rflost_reset     : reset RF_LOST and RF_ADJ_DET bits")
    print("write_skip44           : skip 1 RF/8 : RF_counter = RF_counter-1")
    print("write_jump44           : jump 1 RF/8 : RF_counter = RF_counter+1")
    print("write_rfadj_en         : Enable / Disable rfadj_en")
    print("*********************************\n")
    print("q           : quit\n")
    return
 
# Exit program
def exit():
    print("White Rabbit Timing Bye !")
    sys.exit()
 
# =======================
#    MENUS DEFINITIONS
# =======================
 
# Menu definition
menu_actions = {
    'main_menu': main_menu,
    'hlp': disp_menu,
    'help': disp_menu,
    't0wd': menu_t0wd,
    'nw_t0wd' : menu_nw_t0wd,
    'st': menu_st,
    'stfull': menu_stfull,
    'rfdbg': menu_rfdbg,
    'nw_rfdbg': menu_nw_rfdbg,
    'is': menu_is,
    'rdin': menu_rdin,
    'os': menu_os,
    't0stat' : menu_t0stat,
    'nw_t0stat' : menu_nw_t0stat,
    'nw_t0log' : menu_nw_t0log,
    'nw_dw': menu_nw_dw,
    'nw_version': menu_nw_version,
    'write_injerr_reset' : menu_res_injerr,
    'write_t0lost_reset' : menu_res_t0lost,
    'write_rflost_reset' : menu_res_rflost,
    'write_skip44' : menu_skip44,
    'write_jump44' : menu_jump44,
    'write_rfadj_en' : menu_rfadj_en,
    'blrd': menu_blrd,
    'mb': menu_mb,
    'pu': menu_pu,
    'ph': menu_ph,
    'ck': menu_ck,
    'lm32': menu_lm32,
    'ioconf': menu_ioconf,
    'fdel': menu_fdel,
    'ioread': menu_ioread,
    'siset': menu_siset,
    'sidis': menu_sidis,
    'purbcc': menu_purbcc,
    'dac': menu_dac,
    'ad95': menu_ad95,
    'lad95': menu_lad95,
    'rfn': menu_rfn,
    'mstart': menu_mstart,
    'setin': menu_setin,
    'pll23': menu_pll23,
    'srdiv': menu_srdiv,
    'ssrdiv': menu_ssrdiv,
    'rsrdiv': menu_rsrdiv,
    'dld': menu_dld,
    'tsts': menu_tsts,
    'q': exit,
}
 
# =======================
#      MAIN PROGRAM
# =======================
 
# Main Program
if __name__ == "__main__":
    # Launch main menu
    os.system('clear')
    stop = None
    global pasini
    pasini = False
    while not stop:
        main_menu()
