'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''

__author__ = "Antonin Broquet based on Javier D. Garcia-Lasheras"
__copyright__ = "Copyright 2016, ESRF"
__license__ = "GPLv3 or later"
__version__ = "1.0"
__maintainer__ = "Antonin Broquet"
__email__ = "antonin.broquet@esrf.fr"
__status__ = "Development"


from ctypes import *
import os, errno, re, sys, struct


class Whist():
    """A Class that creates WHIST objects. This includes basic controls
    and methods in order to handle an Etherbone connection to WHIST module.
    Access to a valid libwhist.so is mandatory for a proper operation.

    Attributes:
        ip           IP address of the node to interface.
        iface        Network interface of the computer connected to the WR network
        libc         Path to a valid libwhist.so shared library.
                     If empty, it points to /usr/lib/libwhist.so
    """

    def __init__(self, ip='10.10.10.10', iface='eth1', libc='/segfs/linux/timing/whist/labo/dep/lib/libwhist.so'):

        if not os.path.isfile(libc):
            print('ERROR: libwhist.so library not found')
            raise Exception()

        # Load library
        self.whistlib = CDLL(libc)

        # redefine return types
        self.whistlib.eb_read.argtypes = [c_uint, c_void_p]
        self.whistlib.eb_read.restype = c_bool
        self.whistlib.eb_write.restype = c_bool
        self.whistlib.eb_wrnc_lm32_loader.restype = c_bool
        self.whistlib.eb_wrnc_lm32_restart.restype = c_bool

        self.whistlib.WLopen(c_char_p(ip),c_char_p(iface))
        self.whistlib.eb_set_retry(c_uint(1))
        self.whistlib.eb_set_timeout(c_uint(200))
        


    def __del__(self):
        '''Destroying the WHIST card object'''
        self.whistlib.WLclose()


    # 32 bit register operations in the Wishbone addressing space

    def whistWriteL(self, address, data):
        '''Write a 32 bit integer data into the register at address'''
        return self.whistlib.eb_write(c_uint(address), c_uint(data))


    def whistReadL(self, address, hexformat=True):
        '''Read a 32 bit integer data from the register at address.
        Return an hex string if hexformat=True, an integer otherwise'''
        data = c_uint()
        dataHex = 0xffffffff
        err = self.whistlib.eb_read(c_uint(address),byref(data))
        if hexformat:
            dataHex = hex(data.value)
            return [dataHex,err]
        else:
            return [data.value,err]


    # loading LM32 application program to WR Node Core cpus

    def whistWrncLm32Loader(self, address, cpu_index, file_path):
        '''Load an application program to the LM32 cpu of the WR Node Core'''

        self.whistlib.eb_wrnc_lm32_loader(c_uint(address),c_int(cpu_index),c_char_p(file_path))

    # restarting a WR Node Core cpu

    def whistWrncLm32Restart(self, address, cpu_index):
        '''Restart a LM32 cpu of the WR Node Core'''

        self.whistlib.eb_wrnc_lm32_restart(c_uint(address),c_int(cpu_index))

    # bitwise register operations in the Wishbone addressing space 

    def whistTestBit(self, address, offset):
        '''Test the bit placed at offset in the register at address.
        returns a nonzero result, 2**offset, if the bit is 1'''
        answer = self.whistReadL(address, hexformat=False)
        if answer[1] == False:
          return [0,False]
        else:
          mask = 1 << offset
          return [(answer[0] & mask) >> offset,True]

     
    def whistSetBit(self, address, offset):
        '''Set to 1 the bit placed at offset in the register at address'''
        register = self.whistReadL(address, hexformat=False)
        mask = 1 << offset
        self.whistWriteL(address, register[0] | mask)


    def whistClearBit(self, address, offset):
        '''Clear to 0 the bit placed at offset in the register at address'''
        register = self.whistReadL(address, hexformat=False)
        mask = ~(1 << offset)
        self.whistWriteL(address, register[0] & mask)


    def whistToggleBit(self, address, offset):
        '''Toggle/invert the bit placed at offset in the register at address'''
        register = self.whistReadL(address, hexformat=False)
        mask = 1 << offset
        self.whistWriteL(address, register[0] ^ mask)


