#!/bin/env python
# -*- coding: utf-8 -*-
#title           :menu.py
#description     :This program displays an interactive menu on CLI
#author          :GG from menu_exple.py
#date            :
#version         :0.1sstup
#usage           :python menu.py
#notes           :
#python_version  :3+  
#=======================================================================
# Import the modules needed to run the script.
import sys
import os
import csv

from whkit import *
 
# Main definition - constants
menu_actions  = {}  
global wlist
global fdelcsv
# =======================
#     MENUS FUNCTIONS
# =======================
 
# Main menu
def main_menu():
    global step
    global wlist
    if step == 0:
        print("\n ******** Lab Acceptance Test *********\n")
        menu_glob()
    elif step == 1:
        print("\n----------------------> Full network initialization.")
        menu_init()
    elif step == 2:
        menu_ck10()
    elif step == 3:
        print("\n----------------------> Test RF output")
        menu_rf()
    elif step == 4:
        print("\n----------------------> Test RF/n output")
        menu_rfn()
    elif step == 5:
        print("\n----------------------> Test output#1")
        menu_sma1()
    elif step == 6:
        print("\n----------------------> Test output#2")
        menu_sma23(2)
    elif step == 7:
        print("\n----------------------> Test output#3")
        menu_sma23(3)
    elif step == 8:
        print("\n----------------------> Test output#4")
        menu_smalemo(4)
    elif step == 9:
        print("\n----------------------> Test output#5")
        menu_smalemo(5)
    elif step == 10:
        print("\n----------------------> Test output#6")
        menu_smalemo(6)
    elif step == 11:
        print("\n----------------------> Test output#7")
        menu_smalemo(7)
    elif step == 12:
        print("\n----------------------> Test output#8")
        menu_smalemo(8)
    elif step == 13:
        print("\n----------------------> Test output#9")
        menu_smalemo(9)
    elif step == 14:
        print("\n----------------------> Test output#10")
        menu_smalemo(10)
    elif step == 15:
        print("\n----------------------> Test output#11")
        menu_smalemo(11)
    elif step == 16:
        print("\n----------------------> Test output#12")
        menu_smalemo(12)
    elif step == 17:
        print("\n++++++++++++++++++++++> Stamping test TRIG_0")
        menu_stampin(0)
    elif step == 18:
        print("\n++++++++++++++++++++++> Stamping test TRIG_1")
        menu_stampin(1)
    elif step == 19:
        print("\n++++++++++++++++++++++> Stamping test TRIG_2")
        menu_stampin(2)
    elif step == 20:
        print("\n++++++++++++++++++++++> Stamping test TRIG_3")
        menu_stampin(3)
    elif step == 21:
        print("\n!!!!!!!!!!!!!!!!!!!!!!> HOT PLUG TEST")
        menu_hotplug()
    elif step == 22:
        print("\n<<<<........<<<<<<<<<<< Event receive TEST")
        menu_evt("r")
    elif step == 23:
        print("\n>>>>........>>>>>>>>>>> Event transmit TEST")
        menu_evt("t")
    elif step == 24:
        print("\n----------------------> Output#2 stability TEST")
        menu_ddt(2)
    else:
        print("glub")
        return True
    return
 
# Execute menu
def menu_glob():
    global step
    global wlist
    global bucklist
    # ckeck current setup file
    suer = False
    try:
        with open('currentsu') as suf:
            wlist = []
            for line in suf:
                wlist.append(int(line.rstrip()))
            print("currentsu = {}".format(wlist))
    except IOError:
        print("No current setup file.")
        suer = True
    # change setup and save to file, or not
    if suer == True:
        jn = 'y'
    else:
        choice = raw_input("Change current setup ? : y/[n] >>  ")
        jn = choice.lower()
    rr = 0
    if jn == 'y':
        wlist = tliste_y()
        print("wlist={}".format(wlist))
        with open('currentsu', 'w') as suf:
            for sui in range(len(wlist)):
                suf.write(str(wlist[sui])+"\n")
    # ckeck fine_delay_file
    fdelbool = False
    try:
        with open('fdelcsv', "r") as fd:
            fdelbool = True
            print("---------------> Fine_delay_file detected")
    except IOError:
        print("No current fine_delay_file. Creating it.")
        aecrire = open('fdelcsv', "w")
        skr = csv.DictWriter(aecrire, ('BUS', 'fd1', 'fd2',
                                   'fd3', 'md2', 'pi2',
                                   'md3', 'pi3'),
                         dialect=csv.excel, extrasaction='ignore', restval='',
                             delimiter=',')

        skr.writeheader()
        aecrire.close()
        fdelbool = True
    # set i2c parameters to fdelcsv file value or default
    seti2cparams(wlist, "fdelcsv")
    # set bunch clock parameters
    if rr == 0:
        choice = raw_input("Default bunch clock list = ebs_labo : [y]/ ebs_tango : n >>  ")
        jn = choice.lower()
        if jn == 'n':
            bucklist = dw_tango
            print("Installing dw_tango : {}".format(dw_tango))
            step += 1
        else:
            bucklist = dw_labo
            print("Installing dw_labo : {}".format(dw_labo))
            step += 1
    return rr

def menu_init():
    global step
    global wlist
    global bucklist
    global fdelcsv
    choice = raw_input("     Skip network init ? [y]/n >>  ")
    jn = choice.lower()
    rr = 0
    if jn == 'n':
        nw_init(wlist, bucklist, "fdelcsv")
    else:
        print("!!! NETWORK INIT SKIPPED !!!")
    step += 1
    return rr
 
def menu_ck10():
    global step
    global wlist
    global bucklist
    choice = raw_input("Start from beginning [CR] or Test_INDEX :\n[OUPUT#1-12 = (1->12)] / [INPUT#1-4 = (13->16)] / Hot plug [17] / Event r-t [18-19] / Delta_t [20] >>  ")
    stj = choice.lower()
    rr = 0
    if stj == "":
        print("\n----------------------> Test rear panel 10 MHz output")
        ck10_check(wlist)
        step += 1
    else:
        na = get_v(0, stj)
        if na in range(1, 21):
            step = na + 4
        else:
            rr = 1
            step += 1
    return rr

def menu_rf():
    global step
    global wlist
    rf_check(wlist)
    step += 1
    #print("After rfn step {}".format(step))
    return

def menu_rfn():
    global step
    global wlist
    rfn_check(wlist)
    step += 1
    #print("After rfn step {}".format(step))
    return

def menu_sma1():
    global step
    global wlist
    global bucklist
    global fdelcsv
    print("\nTest with CLK_TC32 signal\n")
    tst_sma_1(wlist, "tc32", "fdelcsv")
    print("\nTest with CLK_16b signal\n")
    tst_sma_1(wlist, "ck16b", "fdelcsv")
    print("\nTest fine phase shift on SR_DIV\n")
    tst_sma_1(wlist, "fph", "fdelcsv")
    step += 1
    return

def menu_sma23(ch):
    global step
    global wlist
    global bucklist
    print("\nTest with GUN signal\n")
    tst_sma_23(wlist, ch, "gun", "fdelcsv")
    print("\nTest OUTPUT#{} with CLK_TC32 signal\n".format(ch))
    tst_sma_23(wlist, ch, "tc32", "fdelcsv")
    print("\nTest fine phase shift\n")
    tst_sma_23(wlist, ch, "fph", "fdelcsv")
    step += 1
    return

def menu_smalemo(ch):
    global step
    global wlist
    global bucklist
    tst_sma_lemo(wlist, ch)
    step += 1
    return

def menu_stampin(ch):
    global step
    global wlist
    global bucklist
    tst_stampin(wlist, ch, "-v")
    step += 1
    return

def menu_hotplug():
    global step
    global wlist
    global bucklist
    tst_hotplug(wlist)
    step += 1
    return

def menu_evt(opt):
    global step
    global wlist
    global bucklist
    tst_evt(wlist, opt)
    step += 1
    return

def menu_ddt(ch):
    global step
    global wlist
    global bucklist
    tst_ddt(wlist, ch)
    step += 1
    return

# =======================
#      MAIN PROGRAM
# =======================
 
# Main Program
if __name__ == "__main__":
    # Launch main menu
    os.system('clear')
    stop = None
    step = 0
    while not stop:
        #print("In main step = {}".format(step))
        stop = main_menu()
