'''
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

'''

__author__ = "Antonin Broquet based on Javier D. Garcia-Lasheras"
__copyright__ = "Copyright 2016, ESRF"
__license__ = "GPLv3 or later"
__version__ = "1.0"
__maintainer__ = "Antonin Broquet"
__email__ = "antonin.broquet@esrf.fr"
__status__ = "Development"


from ctypes import *
import os, errno, re, sys, struct


class Spec():
    """A Class that creates SPEC objects. This includes basic controls
    and methods in order to handle an Etherbone connection to SPEC board.
    Access to a valid libspeceb.so is mandatory for a proper operation.

    Attributes:
        ip           IP address of the node to interface.
        libc         Path to a valid libspec.so shared library.
                     If empty, it points to /usr/lib/libspec.so
    """

    def __init__(self, ip='10.10.10.10', libc='/segfs/linux/timing/whist/python/lib/libspeceb.so'):

        if not os.path.isfile(libc):
            print('ERROR: libspeceb.so library not found')
            raise Exception()

        # Load library
        self.speclib = CDLL(libc)

        # redefine return types
        self.speclib.eb_read.restype = c_uint

        self.speclib.eb_open(c_char_p(ip))

        print('a new SPEC object has been created')


    def __del__(self):
        '''Destroying the SPEC card object'''
        self.speclib.eb_close()
        print('The SPEC object has been destroyed')


    # 32 bit register operations in the Wishbone addressing space

    def specWriteL(self, address, data):
        '''Write a 32 bit integer data into the register at address'''
        self.speclib.eb_write(c_uint(address), c_uint(data))


    def specReadL(self, address, hexformat=True):
        '''Read a 32 bit integer data from the register at address.
        Return an hex string if hexformat=True, an integer otherwise'''
        data = self.speclib.eb_read(c_uint(address))
        if hexformat:
            data = hex(data)
        return data


    # loading LM32 application program to WR Node Core cpus

    def specWrncLm32Loader(self, address, cpu_index, file_path):
        '''Load an application program to the LM32 cpu of the WR Node Core'''

        self.speclib.eb_wrnc_lm32_loader(c_uint(address),c_int(cpu_index),c_char_p(file_path))

    # restarting a WR Node Core cpu

    def specWrncLm32Restart(self, address, cpu_index):
        '''Restart a LM32 cpu of the WR Node Core'''

        self.speclib.eb_wrnc_lm32_restart(c_uint(address),c_int(cpu_index))

    # bitwise register operations in the Wishbone addressing space 

    def specTestBit(self, address, offset):
        '''Test the bit placed at offset in the register at address.
        returns a nonzero result, 2**offset, if the bit is 1'''
        register = self.specReadL(address, hexformat=False)
        mask = 1 << offset
        return(register & mask)

     
    def specSetBit(self, address, offset):
        '''Set to 1 the bit placed at offset in the register at address'''
        register = self.specReadL(address, hexformat=False)
        mask = 1 << offset
        self.specWriteL(address, register | mask)


    def specClearBit(self, address, offset):
        '''Clear to 0 the bit placed at offset in the register at address'''
        register = self.specReadL(address, hexformat=False)
        mask = ~(1 << offset)
        self.specWriteL(address, register & mask)


    def specToggleBit(self, address, offset):
        '''Toggle/invert the bit placed at offset in the register at address'''
        register = self.specReadL(address, hexformat=False)
        mask = 1 << offset
        self.specWriteL(address, register ^ mask)


