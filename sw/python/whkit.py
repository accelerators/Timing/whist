#!/bin/env python

from __future__ import print_function
import sys
import csv
#import string
import time
import datetime as pendul
import readline

import numpy as np

from whist_whdds import SpecWhist

# constants
# super period esrf value
#SUPERP        = 31.0*11*32/352202000
# super period lab value with SPEC local oscillator
SUPERP        = 31.0*11*8/75000000
SI571F        = 352370000
ESRFRF        = 352370000
OLDRF         = 352202000
TAQUET        = 25
rf_pps_a      = 1
rf_pps_b      = 2
dacadd        = 0x90
i2c_70        = 0x70
i2c_72        = 0x72
i2c_74        = 0x74
i2c_76        = 0x76

PHADJUST      = 0x1b # 2Capacitor=011b,800uA=011b
#PHADJUST      = 0x3b # 1Capacitor=111b,800uA=011b

ctrl9516      = [0x0,  0x3,  0x4,
                 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
                 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
                 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab,
                 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5,
                 0x140, 0x141, 0x142, 0x143,
                 0x190, 0x191, 0x192, 0x193, 0x194, 0x195, 0x196, 0x197, 0x198,
                 0x199, 0x19a, 0x19b, 0x19c, 0x19d,
                 0x19e, 0x19f, 0x1a0, 0x1a1, 0x1a2,
                 0x1e0, 0x1e1, 0x230]
ctrl9510      = [0x0,  0x2,  0x4,  0x5,  0x6,  0x7,  0x8,  0x9,  0xa,  0xb,  0xc,  0xd,
                 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
                 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,
                 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x5a]

# COMMAND ID LIST copied from evt.h
EVT_CMD_ENABLE         = 0x1
EVT_CMD_MODE           = 0x2
EVT_CMD_TRANSMITTER    = 0x3
EVT_CMD_RECEIVER       = 0x4
EVT_CMD_RD_STATUS      = 0x5
EVT_CMD_CNT_RAZ        = 0x6
EVT_CMD_CNT_RD         = 0x7
EVT_CMD_CNT_EN         = 0x8
EVT_CMD_DEL            = 0x9
EVT_CMD_RD_VERSION     = 0xa
#EVT_CMD_WRTS           = 0x9

# COMMAND ID LIST copied from bcc.h
BCC_CMD_ENABLE         = 0x1
BCC_CMD_MODE           = 0x2
BCC_MASTER_CMD_SEQ_EN  = 0x3
BCC_MASTER_CMD_START   = 0x4
BCC_CMD_IOCONF         = 0x5
BCC_CMD_DAC            = 0x6
BCC_CMD_SLOK           = 0x7
BCC_CMD_PPS_MON        = 0x8
BCC_CMD_RD_HISTO       = 0x9
BCC_CMD_RD_CSR         = 0xa
BCC_CMD_571            = 0xb
BCC_CMD_VCXO_FREQ      = 0xc
BCC_CMD_RD_VERSION     = 0xd
BCC_CMD_RD_T0BIS       = 0xe
BCC_CMD_RD_T0BIS_MISS  = 0xf
BCC_MASTER_CMD_BCL_PIPE   = 0x10
BCC_MASTER_CMD_RD_TAI_PPS = 0x11

# COMMAND ID LIST copied from d3s.h
D3S_CMD_9516           = 0xa
D3S_CMD_9510           = 0xb

# bunch clock parameters
# ebs tango
dw_tango = [0x527ff, 0x2321, 0x1100, 1, 1]
dw_labo = [0x527ff, 0x2321, 0x130, 100, 100]

# Factory fine delays outputs#1,2,3 on lab modules
# [device fine_del-1,2,3 meta_del_2 pi_2 meta_del_3 pi_3]
# MASTER has no pi option (vcxo=RF), bypass instead => 0x80
MASTER_0          = [0, 240, 0, 0, 17, 0x80, 17, 0x80]
# SLAVES have pi option (vcxo=2*RF): 0/1
SLAVE_1           = [1, 240, 0, 0, 6, 0, 6, 0]
SLAVE_2           = [2, 260, 0, 0, 6, 0, 3, 0]

# Misc
SRCE_B          = [-1, 0, 1] # external, non valid output source,div=1 = no delay = assumed clock
SRCE_NO         = [-1, 0, 0] # external, non valid output source, long delay = assumed pulse
SRDIVAL         = 11
SRDIVPH         = 3
SRDIVSTAMPING   = 77
SR1KHZ          = 1000
SRDIVEVTA       = 0x80000 # 0.67 Hz
SRDIVEVTB       = 0x60000 # 0.9 Hz
SR50            = 0xf800
SRFREQ          = 355220
BOO50           = 0x5800
INSEUILBA       = 0x20
INSEUILHO       = 0x80
LONGDEL         = 0xfffff0 # 24 bits near max
EVTA            = 0xa
EVTB            = 0xb
EVTDUT          = [0xc, 0xd, 0xe, 0xf, 0x9, 0x8, 0x7, 0x6, 0x5]
SRDIVDUT        = [0xa0000, 0xb0000, 0xc0000, 0xd0000, 0xe0000, 0xf0000, 0xa000a, 0xb000b, 0xc000c] #[0.54, 0.49, 0.45, 0.42, 0.39, 0.36, ...] Hz

# ------------------------------------------------------
def get_v(nn, args):
# ------------------------------------------------------
    """returns nnth arg from args as an integer
    if missing returns 0"""
    #argums = args.split(" ")
    argums = args.split()
    try:
        if len(argums) < (nn+1): raise
        else:
            try:
                vlu = argums[nn]
                if vlu.startswith("0X") or vlu.startswith("0x"):
                    vv = int(vlu, base=16)
#                elif vlu.startswith("-"):
                elif vlu.startswith(('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')):
#                    vv = vlu
                    vv = int(vlu)
                else:
#                    vv = int(vlu)
                    vv = vlu
            except Exception:
                print("arg#", nn, "integer")
                return 0
    except Exception:
        print("arg#", nn, "missing")
        return -1
    return vv

# ------------------------------------------------------
def get_fv(nn, args):
# ------------------------------------------------------
    """returns nnth arg from args as a float    accepts string starting with -
    if missing returns 0.0"""
    argums = args.split(" ")
    try:
        if len(argums) < (nn+1): raise
        else:
            try:
                vlu = argums[nn]
                if vlu.startswith("-"):
                    vv = vlu
                else:
                    vv = float(vlu)
            except Exception:
                print("arg#", nn, "integer")
                return 0.0
    except Exception:
        print("arg#", nn, "missing")
        return -1
    return vv

# ------------------------------------------------------
def getput_dw(dev, ch, pol, srce):
# ------------------------------------------------------
    """loop to try various values of delay/width on given output
    if empty returns [1,1]"""

    lop = False
    while not lop:
        dwme = raw_input("   ++ delay / width / CR=skip >> ")
        cwx = dwme.lower()
        if cwx == "":
            dela = 1
            wida = 1
            lop = True
        else:
            dela = get_v(0, dwme)
            if dela < 1: dela = 1
            wida = get_v(1, dwme)
            if wida < 1: wida = 1
        puprog(dev, ch, 1, pol, 2, srce, dela, wida)

# ------------------------------------------------------
def bus_lup(args):
# ------------------------------------------------------
    """allocate IP address acdording to Pcie bus"""
    bus = get_v(0, args)

    if bus == "-cr104":              # mac :0b
        ipa = '10.10.10.11'
    elif bus == "-cr104-diag1":      # mac :03
        ipa = '10.10.10.3'
    elif bus == "-cr104-diag2":      # mac :0d
        ipa = '10.10.10.13'
    elif bus == "-ctrm-srrf1":       # mac :16
        ipa = '10.10.10.22'
    elif bus == "-ctrm-srrf3":       # mac :31
        ipa = '10.10.10.49'
    elif bus == "-d-mbf-c31"  :      # mac :05
        ipa = '10.10.10.5'
    elif bus == "-ext1":             # mac :23
        ipa = '10.10.10.35'
    elif bus == "-ext2":             # mac :0a
        ipa = '10.10.10.10'
    elif bus == "-ext3":             # mac :0e
        ipa = '10.10.10.14'
    elif bus == "-id04-diag1":       # mac :2f
        ipa = '10.10.10.47'
    elif bus == "-inj1":             # mac :38
        ipa = '10.10.10.56'
    elif bus == "-linac":            # mac :04
        ipa = '10.10.10.4'
    elif bus == "-master-ctrm":      # mac :08
        ipa = '10.10.10.8'
    elif bus == "-mn104-diag1":      # mac :2b
        ipa = '10.10.10.43'
    elif bus == "-mn105-diag1":      # mac :33
        ipa = '10.10.10.51'
    elif bus == "-pinj-diag1":       # mac :2e
        ipa = '10.10.10.46'
    elif bus == "-spare-cr104":      # mac :21
        ipa = '10.10.10.33'
    elif bus == "-spare-injext":     # mac :0f
        ipa = '10.10.10.15'
    elif bus == "-spare-linac":      # mac :09
        ipa = '10.10.10.9'
    elif bus == "-sypc1":            # mac :12
        ipa = '10.10.10.12'
    elif bus == "-syrf1"      :      # mac :11
        ipa = '10.10.10.17'
    elif bus == "-tz01-diag1":       # mac :1c
        ipa = '10.10.10.28'
    elif bus == "-tz02-diag1":       # mac :15
        ipa = '10.10.10.21'
    elif bus == "-tz03-diag1":       # mac :12
        ipa = '10.10.10.18'
    elif bus == "-tz04-diag1":       # mac :18
        ipa = '10.10.10.24'
    elif bus == "-tz04-spectra":     # mac :13
        ipa = '10.10.10.19'
    elif bus == "-tz06-diag1":       # mac :1d
        ipa = '10.10.10.29'
    elif bus == "-tz07-diag1":       # mac :1e
        ipa = '10.10.10.30'
    elif bus == "-tz08-diag1":       # mac :1a
        ipa = '10.10.10.26'
    elif bus == "-tz10-diag1":       # mac :1b
        ipa = '10.10.10.27'
    elif bus == "-tz12-diag1":       # mac :30
        ipa = '10.10.10.48'
    elif bus == "-tz15-diag1":       # mac :14
        ipa = '10.10.10.20'
    elif bus == "-tz19-diag1":       # mac :1f
        ipa = '10.10.10.31'
    elif bus == "-tz26-diag1":       # mac :20
        ipa = '10.10.10.32'
    elif bus == "-tz27-diag1":       # mac :22
        ipa = '10.10.10.34'
    elif bus == "-tz29-diag1":       # mac :2a
        ipa = '10.10.10.42'
    elif bus == 0:                   # mac :00
        ipa = '10.10.10.99'
    elif bus == 1:                   # mac :01
        ipa = '10.10.10.98'
    elif bus == 2:                   # mac :02
        ipa = '10.10.10.2'
    elif bus == 3:                   # mac :03
        ipa = '10.10.10.3'
    elif bus == 4:                   # mac :04
        ipa = '10.10.10.4'
    elif bus == 5:                   # mac :05
        ipa = '10.10.10.5'
    elif bus == 6:                   # mac :06
        ipa = '10.10.10.6'
    elif bus == 7:                   # mac :07
        ipa = '10.10.10.7'
    elif bus == 8:                   # mac :08
        ipa = '10.10.10.8'
    elif bus == 9:                   # mac :09
        ipa = '10.10.10.9'
    elif bus == 10:                  # mac :0a
        ipa = '10.10.10.10'
    elif bus == 11:                  # mac :0b
        ipa = '10.10.10.11'
    elif bus == 12:                  # mac :0c
        ipa = '10.10.10.12'
    elif bus == 13:                  # mac :0d
        ipa = '10.10.10.13'
    elif bus == 14:                  # mac :0e
        ipa = '10.10.10.14'
    elif bus == 15:                  # mac :0f
        ipa = '10.10.10.15'
    elif bus == 16:                  # mac :10
        ipa = '10.10.10.16'
    elif bus == 17:                  # mac :11
        ipa = '10.10.10.17'
    elif bus == 18:                  # mac :12
        ipa = '10.10.10.18'
    elif bus == 19:                  # mac :13
        ipa = '10.10.10.19'
    elif bus == 20:                  # mac :14
        ipa = '10.10.10.20'
    elif bus == 21:                  # mac :15
        ipa = '10.10.10.21'
    elif bus == 22:                  # mac :16
        ipa = '10.10.10.22'
    elif bus == 23:                  # mac :17
        ipa = '10.10.10.23'
    elif bus == 24:                  # mac :18
        ipa = '10.10.10.24'
    elif bus == 25:                  # mac :19
        ipa = '10.10.10.25'
    elif bus == 26:                  # mac :1a
        ipa = '10.10.10.26'
    elif bus == 27:                  # mac :1b
        ipa = '10.10.10.27'
    elif bus == 28:                  # mac :1c
        ipa = '10.10.10.28'
    elif bus == 29:                  # mac :1d
        ipa = '10.10.10.29'
    elif bus == 30:                  # mac :1e
        ipa = '10.10.10.30'
    elif bus == 31:                  # mac :1f
        ipa = '10.10.10.31'
    elif bus == 32:                  # mac :20
        ipa = '10.10.10.32'
    elif bus == 33:                  # mac :21
        ipa = '10.10.10.33'
    elif bus == 34:                  # mac :22
        ipa = '10.10.10.34'
    elif bus == 35:                  # mac :23
        ipa = '10.10.10.35'
    elif bus == 36:                  # mac :24
        ipa = '10.10.10.36'
    elif bus == 37:                  # mac :25
        ipa = '10.10.10.37'
    elif bus == 38:                  # mac :26
        ipa = '10.10.10.38'
    elif bus == 39:                  # mac :27
        ipa = '10.10.10.39'
    elif bus == 40:                  # mac :28
        ipa = '10.10.10.40'
    elif bus == 41:                  # mac :29
        ipa = '10.10.10.41'
    elif bus == 42:                  # mac :2a
        ipa = '10.10.10.42'
    elif bus == 43:                  # mac :2b
        ipa = '10.10.10.43'
    elif bus == 44:                  # mac :2c
        ipa = '10.10.10.44'
    elif bus == 45:                  # mac :2d
        ipa = '10.10.10.45'
    elif bus == 46:                  # mac :2e
        ipa = '10.10.10.46'
    elif bus == 47:                  # mac :2f
        ipa = '10.10.10.47'
    elif bus == 48:                  # mac :30
        ipa = '10.10.10.48'
    elif bus == 49:                  # mac :31
        ipa = '10.10.10.49'
    elif bus == 50:                  # mac :32
        ipa = '10.10.10.50'
    elif bus == 51:                  # mac :33
        ipa = '10.10.10.51'
    elif bus == 52:                  # mac :34
        ipa = '10.10.10.52'
    elif bus == 53:                  # mac :35
        ipa = '10.10.10.53'
    elif bus == 54:                  # mac :36
        ipa = '10.10.10.54'
    elif bus == 55:                  # mac :37
        ipa = '10.10.10.55'
    elif bus == 56:                  # mac :38
        ipa = '10.10.10.56'
    elif bus == 57:                  # mac :39
        ipa = '10.10.10.57'
    elif bus == 58:                  # mac :3a
        ipa = '10.10.10.58'
    elif bus == 59:                  # mac :3b
        ipa = '10.10.10.59'
    elif bus == 60:                  # mac :3c
        ipa = '10.10.10.60'
    elif bus == 61:                  # mac :3d
        ipa = '10.10.10.61'
    elif bus == 62:                  # mac :3e
        ipa = '10.10.10.62'
    elif bus == 63:                  # mac :3f
        ipa = '10.10.10.63'
    elif bus == 64:                  # mac :40
        ipa = '10.10.10.64'
    elif bus == 65:                  # mac :41
        ipa = '10.10.10.65'
    elif bus == 66:                  # mac :42
        ipa = '10.10.10.66'
    else:
        ipa = '0'
    return ipa

# ------------------------------------------------------
def csvupdate(fdcsv, field, bus, valw):
# ------------------------------------------------------
    """update fdelcsv file
    Give : file name (string)
    field to be updated (string)
    dut bus
    value updated"""

    # read and copy file, test if update or append
    hit = False
    lignes = []
    with open(fdcsv, "r") as fd:
        lu = csv.DictReader(fd,delimiter=",")
        titres = lu.fieldnames
        for row in lu:
            uneligne = row
            if (uneligne['BUS'] == str(bus)):
                # update field
                uneligne[field] = valw
                hit = True # update
            # else hit=False : append
            lignes.append(uneligne)
    # now write file and append new bus line if any
    with open(fdcsv, "w") as fd:
        skr = csv.DictWriter(fd, fieldnames=titres)
        skr.writeheader()
        for raw in lignes:
            skr.writerow(raw)
        if hit == False:
            # add new line for bus with updated value and others=0
            nline = {'BUS':bus, 'fd1':0, 'fd2':0, 'fd3':0, 'md2':0, 'pi2':0, 'md3':0, 'pi3':0}
            nline[field] = valw
            print("---------> Adding line {} to csv file.".format(nline))
            skr.writerow(nline)
        else:
            print("---------> csv file modified.")

# ------------------------------------------------------
def wr_i2c(ipp, add, valw):
# ------------------------------------------------------
    """write i2c device"""

    dd = SpecWhist(ipp)

    # request
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    # set control command @ 0x24008 : 0x6
    dd.ddsSetCoreBcc(0x8, BCC_CMD_IOCONF)
    # 0x2400c 0x0
    dd.ddsSetCoreBcc(0xc, add)
    # set write @ 0x24010 r/w=1
    dd.ddsSetCoreBcc(0x10, 1)
    # value to write
    dd.ddsSetCoreBcc(0x14, valw)
    # send request @ 0x24000 : 0x4000010
    dd.ddsSetCoreBcc(0x0, 0x4000010)

# ------------------------------------------------------
def rd_i2c(ipp, add):
# ------------------------------------------------------
    """read i2c device"""

    dd = SpecWhist(ipp)

    # request
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    #set control command @ 0x24008 : 0x6
    dd.ddsSetCoreBcc(0x8, BCC_CMD_IOCONF)
    # 0x2400c 0x0
    dd.ddsSetCoreBcc(0xc, add)
    # set read @ 0x24010 r/w=0
    dd.ddsSetCoreBcc(0x10, 0)
    # send request @ 0x24000 : 0x4000010
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    # read data
    time.sleep(0.01)
    vallu = dd.ddsGetCoreBcc(0x8)
    # discard
    dd.ddsDisCoreBcc()
    # optional purge
    dd.ddsPurCoreBcc()

    return vallu

# ------------------------------------------------------
def rd_evt_status(buss, verbose):
# ------------------------------------------------------

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetCoreEvt(0x0, 0x1000000)
    dd.ddsSetCoreEvt(0x8, EVT_CMD_RD_STATUS)
    dd.ddsSetCoreEvt(0x0, 0x4000010)            

    time.sleep(0.01)

    stator = dd.ddsGetCoreEvt(0x8)
    armed = (stator & 0xF00000) >> 20
    deven = (stator & 0x40000000) >> 30
    devmod = (stator & 0x80000000) >> 31
    devoen = stator & 0xf
    devien = (stator & 0xf00) >> 8
    devcren = (stator & 0xf0000) >> 16
    devcten = (stator & 0xf000000) >> 24
    envoi = []
    if verbose == "-v": print("\n   -> Device#{} : EVT_ENABLE    = {}".format(buss, deven))
    if verbose == "-v": print("        ->   evt_mode      = {}".format(devmod))
    if verbose == "-v": print("        ->   armed         = {}".format(armed))
    if verbose == "-v": print("        ->   evt_out_en    = {}".format(devoen))
    if verbose == "-v": print("        ->   evt_in_en     = {}".format(devien))
    if verbose == "-v": print("        ->   evt_cnt_r_en  = {}".format(devcren))
    if verbose == "-v": print("        ->   evt_cnt_t_en  = {}".format(devcten))
    # receive lut
    for tul in range(4):
        stalur = dd.ddsGetCoreEvt(0x8 + 0x4 + 4*tul)
        if verbose == "-v": print("              ->   evt_LUT_in{}   = 0x{:0x}".format(tul, stalur))
        if ((devoen >> tul) & 0x1) == 1:
            envoi.append(["o", buss, tul, stalur, (devcren >> tul) & 0x1])
    # transmit lut
    for tal in range(4):
        stalut = dd.ddsGetCoreEvt(0x8 + 0x14 + 4*tal)
        if verbose == "-v": print("        ->   evt_LUT_out{}  = 0x{:0x}".format(tal, stalut))
        # transmit source
        stasrc = dd.ddsGetCoreEvt(0x8 + 0x24 + 4*tal)
        srd = stasrc & 0x3fffff
        sout = (stasrc & 0x3c00000) >> 22
        sbus = (stasrc & 0xfc000000) >> 26
        if verbose == "-v": print("        ->   evt_SRCE{}  = device#{}, out#{}, SRDIV={}".format(tal, sbus, sout, srd))
        # transmit long delay
        dlong = dd.ddsGetCoreEvt(0x8 + 0x34 + 4*tal)
        if verbose == "-v": print("        ->   evt_long_delay{}  = {}".format(tal, dlong))
        if ((devien >> tal) & 0x1) == 1:
            # return lut & source & delay
            envoi.append(["i", buss, tal, stalut, (devcten >> tal) & 0x1, [sbus, sout, srd], dlong])
    
    #discard
    dd.ddsDisCoreEvt()
    #optional purge
    dd.ddsPurCoreEvt()

    return envoi
    
# ------------------------------------------------------
def rd_evt_cnt(buss, tr, kan, verbose):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetCoreEvt(0x0, 0x1000000)
    dd.ddsSetCoreEvt(0x8, EVT_CMD_CNT_RD)
    dd.ddsSetCoreEvt(0x0, 0x4000010)            

    time.sleep(0.01)
    vla = -1
    for bb in range(4):
        cntlu = dd.ddsGetCoreEvt(0x8 + 4*bb)
        if verbose == "-v": print("  -> Device#{}        : CNT_T({}) = 0x{:0x}".format(buss, bb, cntlu))
        if (kan == bb) and (tr == "-t"): vla = cntlu
    for cc in range(4):
        cntlu = dd.ddsGetCoreEvt(0x8 + 0x10 +  4*cc)
        if verbose == "-v": print("  -> Device#{}  : CNT_R({}) = 0x{:0x}".format(buss, cc, cntlu))
        if (kan == cc) and (tr == "-r"): vla = cntlu
    #discard
    dd.ddsDisCoreEvt()
    #optional purge
    dd.ddsPurCoreEvt()
    
    return vla
    
# ------------------------------------------------------
def en_evt_cnt(buss, tr, kan, ena):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    #CoreEvt
    dd.ddsSetCoreEvt(0x0, 0x1000000)
    #set command to CNT_EN
    dd.ddsSetCoreEvt(0x8, EVT_CMD_CNT_EN)
    #index
    dd.ddsSetCoreEvt(0xc, kan)
    # transmit / receive
    if tre == "-t": dd.ddsSetCoreEvt(0x10, 1)
    if tre == "-r": dd.ddsSetCoreEvt(0x10, 0)
    #enable
    dd.ddsSetCoreEvt(0x14, ena)
    #send data
    dd.ddsSetCoreEvt(0x0, 0x4000010)

# ------------------------------------------------------
def clear_evt_cnt(buss, tr, kan, ena):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    #CoreEvt
    dd.ddsSetCoreEvt(0x0, 0x1000000)
    #set command to CNT_EN
    dd.ddsSetCoreEvt(0x8, EVT_CMD_CNT_RAZ)
    #index
    dd.ddsSetCoreEvt(0xc, kan)
    # transmit / receive
    if tr == "-t": dd.ddsSetCoreEvt(0x10, 1)
    if tr == "-r": dd.ddsSetCoreEvt(0x10, 0)
    #send data
    dd.ddsSetCoreEvt(0x0, 0x4000010)

# ------------------------------------------------------
def testin(buss, kanal, verbose):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    ins = []
    popol = dd.ddsTestInPol(kanal)[0]
    poen = dd.ddsTestInEn(kanal)[0]
    if verbose == "-v": print("   INPUT#{}, POLAR={}, ENABLE = {}, STAMP_VALUE = {}.".format(kanal, popol, poen, hex(dd.ddsGetTrigStamp(kanal))))
    return [kanal, popol, poen]

# ------------------------------------------------------
def statin(buss, verbose):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ins = []
    for kanal in range(4):
        ins.append(testin(buss, kanal, verbose))
    return ins

# ------------------------------------------------------
def testout(buss, cha, verbose):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    eno = dd.ddsTestPuCkEn(cha)[0]
    mode = dd.ddsGetPuCkMode(cha)
    sur = dd.ddsGetPuCkSrce(cha)
    pol = dd.ddsTestPuCkPol(cha)[0]
    puldelo = dd.ddsGetPulDel(cha, False)
    pulwido = dd.ddsGetPulWid(cha, False)
    if mode == 3:
        strmode = "Clock"
        if sur == 3:
            strsur = "SRDIV"
        elif sur == 2:
            strsur = "Clk_Booster"
        elif sur == 1:
            strsur = "Clk_SR"
        elif sur == 0:
            strsur = "Tc32"
        elif sur == 4:
            strsur = "Clk_16b"
        elif sur == 5:
            strsur = "Clk_4b"
        else:
            strsur = hex(sur)
        if verbose == "-v":
            if strsur == "SRDIV":
                print("{:<3d}, {:<7d}, {:<6d}, {:<16s}, {:16s}, {:#10x}, {:#10x}.".format(cha+1, eno, pol, strmode, strsur, puldelo, pulwido))
            else:
                print("{:<3d}, {:<7d}, {:<6d}, {:<16s}, {:16s}.".format(cha+1, eno, pol, strmode, strsur))
    elif mode == 2:
        if sur < 4:
            strmode = "WidePulse"
            if sur == 3:
                strsur = "Ki"
            elif sur == 2:
                strsur = "Ext"
            elif sur == 1:
                strsur = "Inj"
            elif sur == 0:
                strsur = "T0"
            if verbose == "-v": print("{:<3d}, {:<7d}, {:<6d}, {:<16s}, {:<16s}, {:#10x}, {:#10x}.".format(cha+1, eno, pol, strmode, strsur, puldelo, pulwido))
        elif sur < 8:
            strmode = "SequencerPulse"
            if sur == 4:
                strsur = "Sequencer_Start"
                puldelo = 0
                pulwido = 1
            elif sur == 5:
                strsur = "Sequencer_INJ"
                puldelo = 0
                pulwido = 1
            elif sur == 6:
                strsur = "Sequencer_EXT"
                strsur = "Sequencer_GUN"
                puldelo = 0
                pulwido = 1
            if verbose == "-v": print("{:<3d}, {:<7d}, {:<6d}, {:<16s}, {:<16s}, {:#10x}, {:#10x}.".format(cha+1, eno, pol, strmode, strsur, puldelo, pulwido))
        else:
            strmode = "Event"
            if verbose == "-v": print("{:<3d}, {:<7d}, {:<6d}, {:<16s}, {:<16d}.".format(cha+1, eno, pol, strmode, sur))
    elif mode == 1:                
        strmode = "GunPulse"
        if verbose == "-v": print("{:<3d}, {:<7d}, {:<6d}, {:<16s}.".format(cha+1, eno, pol, strmode))
    else:
        if verbose == "-v": print("{:<3d}, {:<7d}, {:<6d}, {:<16d}, {:<16d}, {:#10x}, {:#10x}.".format(cha+1, eno, pol, mode, sur, puldelo, pulwido))
    return [cha, eno, pol, mode, sur, puldelo, pulwido]

# ------------------------------------------------------
def statout(buss, verbose):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    outs = []
    if verbose == "-v": print("=============================================")
    if verbose == "-v": print("{:3s}, {:7s}, {:6s}, {:16s}, {:16s}, {:10s}, {:10s}.".format("CH#", "ENABLE", "POLAR", "MODE", "SRCE", "DEL_REG", "WID_REG"))
    for kano in range(12):
        outs.append(testout(buss, kano, verbose))
    if verbose == "-v": print("=============================================")
    return outs

# ------------------------------------------------------
def srdivout(buss, verbose):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    outs = []
    if verbose == "-v": print("=============================================")
    if verbose == "-v": print("{:3s}, {:7s}, {:6s}, {:12s}, {:10s}, {:10s}, {:4s}.".format("CH#", "ENABLE", "POLAR", "DIV_FACTOR+1", "FREQUENCY", "COARSE_PH", "SR_PH", ))
    for kano in range(12):
        luto = testout(buss, kano, "-n")
        if (luto[3] == 3) and (luto[4] == 3): #mode clock srce srdiv
            divf = ((luto[5] & 0XFFFFFC00) >> 10)+1
            coarph = luto[6] & 0X3FFFFF
            srph = luto[5] & 0X3FF
            print("{:<3d}, {:<7d}, {:<6d}, {:<12d}, {:<9.2f} , {:<10d}, {:#3d}.".format(luto[0]+1, luto[2], luto[3], divf, (float(SRFREQ) / float(divf)), coarph, srph))
    if verbose == "-v": print("=============================================")
    return outs

# ------------------------------------------------------
def setin(buss, cha, ena, pola, daca):
# ------------------------------------------------------
    """set trigger inputs : channel enable polarity threshold"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False
    if cha not in range(0, 4):
        print("setin : Bad channel number {}".format(cha))
        return False

    dd = SpecWhist(ipp)

    if ena != 1 :
        dd.ddsClearInEn(cha)
    else:
        dd.ddsSetInEn(cha)
        
    if pola != 1 :
        dd.ddsClearInPol(cha)
    else:
        dd.ddsSetInPol(cha)

    if not 0 <= daca <= 0x80:
        print("No threshold change")
    else:
        # write
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_DAC)
        dd.ddsSetCoreBcc(0xc, 1)          #write
        dd.ddsSetCoreBcc(0x10, (7-2*cha)) #dac reverse cabling...
        dd.ddsSetCoreBcc(0x14, daca)
        dd.ddsSetCoreBcc(0x0, 0x4000010)

# ------------------------------------------------------
def trigger(buss, chx):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    rrr = True

    if chx == "InjErrRst":
        dd.ddsSetInjerRst()
    elif chx == "T0LostRst":
        dd.ddsSetInjerRst()
    elif chx == "BlistInc":
        dd.ddsSetBlistInc()
    elif chx == "BlistRst":
        dd.ddsSetBlistRst()
    elif chx == "SncRf":
        dd.ddsSetSyncRf()
    elif chx == "RfLostRst":
        dd.ddsSetRflRaz()
    elif chx == "Skip44":
        dd.ddsSetSkip44()
    elif chx == "Jump44":
        dd.ddsSetJump44()
    else:
        rrr = False
    return rrr

# ------------------------------------------------------
def ioc(buss, chx, vala, verbose):
# ------------------------------------------------------
    """set ioconf bitfields"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    if verbose == "-v": print("chx is {}".format(chx))

    rr = True
    if chx == "-ttlo":
        lu72 = rd_i2c(ipp, i2c_72)
        val72 = (lu72 & 0xFB) | ((vala & 0x1)<<2)
        wr_i2c(ipp, i2c_72, val72)
        if verbose == "-v": print("TTLOUT enable")
    elif chx == "-fplo":
        lu74 = rd_i2c(ipp, i2c_74)
        val74 = (lu74 & 0xEF) | ((vala & 0x1)<<4)
        wr_i2c(ipp, i2c_74, val74)
        if verbose == "-v": print("Font Pwnel LEDs enable")
    elif chx == "-ck10":
        lu76 = rd_i2c(ipp, i2c_76)
        val76 = (lu76 & 0xBF) | ((vala & 0x1)<<6)
        wr_i2c(ipp, i2c_76, val76)
        if verbose == "-v": print("CK10 enable = {}".format(vala))
    elif chx == "-clko":
        lu70 = rd_i2c(ipp, i2c_70)
        val70 = (lu70 & 0xFE) | vala & 0x1
        wr_i2c(ipp, i2c_70, val70)
        if verbose == "-v": print("RF clocks out enable")
    elif chx == "-vcxo":
        lu76 = rd_i2c(ipp, i2c_76)
        val76 = (lu76 & 0x7F) | ((vala & 0x1)<<7)
        wr_i2c(ipp, i2c_76, val76)
        if vala == 1:
            if verbose == "-v": print("VCXO enabled, Slave mode.")
        else:
            if verbose == "-v": print("VCXO disabled, Master mode.")
    else:
        rr = False
    return rr

# ------------------------------------------------------
def seti2cparams(liste, fdcsv):
# ------------------------------------------------------
    """Set i2c parameters ONLY to recorded or default values
       takes list of modules under test
             fdelcsv file"""

    m0 = liste[0]
    s1 = liste[1]
    s2 = liste[2]
    rr = 0
    print("   Set factory fine delays on Master & reference slaves S1/S2\n")
    if m0 == 0:
        rr += setfdel(m0, 1, MASTER_0[1]) # only fd1/2/3 for master
        rr += setfdel(m0, 2, MASTER_0[2]) # only fd1/2/3 for master
        rr += setfdel(m0, 3, MASTER_0[3]) # only fd1/2/3 for master
    if s1 == 1:
        rr += setfdel(s1, 1, SLAVE_1[1])
        rr += setfdel(s1, 2, SLAVE_1[2])
        rr += setfdel(s1, 3, SLAVE_1[3])
    if s2 == 2:
        rr += setfdel(s2, 1, SLAVE_2[1])
        rr += setfdel(s2, 2, SLAVE_2[2])
        rr += setfdel(s2, 3, SLAVE_2[3])
    # for all
    for dev in liste:
        rr += ioc(dev, "-fplo", 1, "-n") # enables front panel leds
        rr += ioc(dev, "-ck10", 0, "-n") # disable CLK_10
        rr += ioc(dev, "-ttlo", 1, "-n") # enable TTL
    # now DUTs
    for ii in range(3, len(liste)):
        dut = liste[ii]
        #print("dut init {}".format(dut))
        # take ad9510 params from file
        with open(fdcsv, "r") as fd:
            lu = csv.DictReader(fd,delimiter=",")
            hitlu = False
            for row in lu:
                if (row['BUS'] == str(dut)):
                    hitlu = True
                    fde1 = int(row["fd1"])
                    fde2 = int(row["fd2"])
                    fde3 = int(row["fd3"])
        if hitlu == False:
            fde1 = 0
            fde2 = 0
            fde3 = 0
        rr += setfdel(dut, 1, fde1)
        rr += setfdel(dut, 2, fde2)
        rr += setfdel(dut, 3, fde3)

# ------------------------------------------------------
def dacLoader(buss, rww, chh, valw, verbose):
# ------------------------------------------------------
    """load / read DAC"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    if rww == 1:
        # write
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_DAC)
        dd.ddsSetCoreBcc(0xc, rww)
        dd.ddsSetCoreBcc(0x10, chh)
        dd.ddsSetCoreBcc(0x14, valw)
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        if verbose == "-v": print("set dac_chan {} : {}".format(chh, hex(valw)))
    else:
        # read
        for chacha in range(8):
            dd.ddsSetCoreBcc(0x0, 0x1000000)
            dd.ddsSetCoreBcc(0x8, BCC_CMD_DAC)
            dd.ddsSetCoreBcc(0xc, rww)
            dd.ddsSetCoreBcc(0x10, chacha)
            dd.ddsSetCoreBcc(0x0, 0x4000010)
            time.sleep(0.01)
            vallu = dd.ddsGetCoreBcc(0x8)
            print("dac_chan {} : {}".format(chacha, hex(vallu)))
            #discard
            dd.ddsDisCoreBcc()
            #optional purge
            dd.ddsPurCoreBcc()

# ------------------------------------------------------
def ldbl(buss, file):
# ------------------------------------------------------

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False
    # process file name : add full path
    fname = "/segfs/linux/timing/whist/labo/bunchlists/" + file
    print(fname)
    alire = open(fname, "r")

    dd = SpecWhist(ipp)

    # set start to default
    dd.ddsSetBlistStart(0x0)
    nbpul = 0
    try:
        lu = csv.reader(alire,delimiter=" ")
        for row in lu:
            nbpul += 1
            blad = (int(row[0]))*4
            if row[1].startswith("0X") or row[1].startswith("0x"):
                blv = int(row[1], base=16)
            else:
                blv = int(row[1])
            print("at {} : {}".format(hex(blad), hex(blv)))
            # fill RAM
            dd.ddsSetBL(blad, blv)

    except Exception:
        # dont forget to set stop.
        dd.ddsSetBlistStop(nbpul-1)
        alire.close()
        print("nbpul={} ; write stop at {}".format(nbpul, 0x1000+nbpul))
        return True
                     
    finally:
        # dont forget to set stop
        dd.ddsSetBlistStop(nbpul-1)
        alire.close()
        return True

# ------------------------------------------------------
def slok(buss):
# ------------------------------------------------------
    """trigger lock slave_rf_counter to master_rf_counter"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_SLOK)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    #discard
    dd.ddsDisCoreBcc()
    #optional purge
    dd.ddsPurCoreBcc()

# ------------------------------------------------------
def msconf(buss, ms):
# ------------------------------------------------------
    """configure master or slave"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return 1

    echec = msset(buss, ms)
    #i2c stuf must be kept outside msset to avoid recursive SpecWhist calls
    
    if echec == 1:
        return echec
    elif ms == "-m":
        print("Disable vcxo")
        ioc(buss, "-vcxo", 0, "-n")
    elif ms == "-s":
        print("Enable vcxo")
        ioc(buss, "-vcxo", 1, "-n")
    return echec
            
# ------------------------------------------------------
def msset(buss, ms):
# ------------------------------------------------------
    """configure master or slave"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False
    echec = 1
    
    dd = SpecWhist(ipp)

    #restart wrnc/CPU0 dds
    dd.ddsLm32Restart(0)
    #restart wrnc/CPU1 bcc
    dd.ddsLm32Restart(1)

    if ms == "-m":
        print("configuring #{} as master".format(ipp))
        #CoreDDS actions 
        dd.ddsSetCoreDds(0x0, 0x1000000)
        #set control command
        dd.ddsSetCoreDds(0x8, 0x5)
        dd.ddsSetCoreDds(0xc, 0x0)
        #set master mode
        dd.ddsSetCoreDds(0x10, 0x1)
        #set event id
        dd.ddsSetCoreDds(0x14, 0x8f)
        #set tune RF center freq
        dd.ddsSetCoreDds(0x18, 0x2d1)
        #        dd.ddsSetCoreDds(0x1c, 0x4f483caf) # 352202000
        #        dd.ddsSetCoreDds(0x1c, 0xa87e750c) # 352372158
        dd.ddsSetCoreDds(0x1c, 0xa7f83d4f) # 352371158
        #send data
        dd.ddsSetCoreDds(0x0, 0x4000010)
        #Purge
        time.sleep(2)
        dd.ddsPurCoreDds()

        #Now CoreBcc actions : set master mode
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #set command to mode
        dd.ddsSetCoreBcc(0x8, BCC_CMD_MODE)
        #set mode to master
        dd.ddsSetCoreBcc(0xc, 0x1)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)

        #Now CoreBcc actions : disable SEQ_EN
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #set command to T0 mode
        dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_SEQ_EN)
        #write
        dd.ddsSetCoreBcc(0xc, 1)
        #reset SEQ_EN
        dd.ddsSetCoreBcc(0x10, 0)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)

        #Now CoreBcc actions : enable rt-bcc LM32
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #enable rt-bcc (wait for event)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_ENABLE)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        echec = 0
        
        #CoreEvt actions : disable CoreEvt for master, T0 priority
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        #set command to enable
        dd.ddsSetCoreEvt(0x8, EVT_CMD_ENABLE)
        #disable
        dd.ddsSetCoreEvt(0xc, 0x0)
        #send data
        dd.ddsSetCoreEvt(0x0, 0x4000010)

        #CoreEvt actions : set mode master
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        #set command to mode
        dd.ddsSetCoreEvt(0x8, EVT_CMD_MODE)
        #set master
        dd.ddsSetCoreEvt(0xc, 0x1)
        #send data
        dd.ddsSetCoreEvt(0x0, 0x4000010)
        
    elif ms == "-s":
        print("configuring #{} as slave".format(ipp))
        #CoreDDS actions
        dd.ddsSetCoreDds(0x0, 0x1000000)
        #set control command
        dd.ddsSetCoreDds(0x8, 0x5)
        dd.ddsSetCoreDds(0xc, 0x0)
        #set slave mode
        dd.ddsSetCoreDds(0x10, 0x2)
        #set event id
        dd.ddsSetCoreDds(0x14, 0x8f)
        #set tune RF center freq
        dd.ddsSetCoreDds(0x18, 0x0)
        dd.ddsSetCoreDds(0x1c, 0x0)
        #send data
        dd.ddsSetCoreDds(0x0, 0x4000010)
        #Purge
        time.sleep(2)
        dd.ddsPurCoreDds()

        #Now CoreBcc actions : set slave mode
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #set command to mode
        dd.ddsSetCoreBcc(0x8, BCC_CMD_MODE)
        #set mode to slave
        dd.ddsSetCoreBcc(0xc, 0x2)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)

        #Now CoreBcc actions : enable rt-bcc LM32
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #enable rt-bcc (wait for event)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_ENABLE)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()

        #CoreEvt actions : enable CoreEvt
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        #set command to enable
        dd.ddsSetCoreEvt(0x8, EVT_CMD_ENABLE)
        #set enable
        dd.ddsSetCoreEvt(0xc, 0x1)
        #send data
        dd.ddsSetCoreEvt(0x0, 0x4000010)

        #CoreEvt actions : set mode slave
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        #set command to mode
        dd.ddsSetCoreEvt(0x8, EVT_CMD_MODE)
        #set slave
        dd.ddsSetCoreEvt(0xc, 0x2)
        #send data
        dd.ddsSetCoreEvt(0x0, 0x4000010)
        
    #wait some time for RFoE locked
    print("Wait for RFoE locked")
    for ttt in range(10):
        time.sleep(2)
        if dd.ddsTestRfoe()[0] == 1:
            print("====> RFoE locked - PLL enabled")
            echec = 0
            break
    if echec == 1:
        print("!!! RFoE remains unlocked after 20 s !!!!")

    return echec

# ------------------------------------------------------
def clr_fl(buss):
# ------------------------------------------------------
    """Clear flashed_bit and set Si571 to working frequency"""
    ipp = bus_lup(str(buss))
    
    dd = SpecWhist(ipp)
    fla = dd.ddsTestFlashed()[0]
    if fla == 1:
        print("Clearing flashed BIT")
        dd.ddsClearFlashed()
    return fla

# ------------------------------------------------------
def taipps(buss, master_bit, verbose):
# ------------------------------------------------------
    """Get master module tai_sec time and synchronous rf_cnt"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    if verbose == "-v" :print("taipi de {} mbit {}".format(buss, master_bit))

    if master_bit == 1:
        if verbose == "-v" : print("datetime before TAI read is {}".format(pendul.datetime.now()))
        #get t0bis_miss WR stamp from CoreBcc
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_RD_TAI_PPS)
        #dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010) 
        time.sleep(0.01)
        #value to be printed lower
        tai_sec_at_pps = dd.ddsGetCoreBcc(0x8)
        rf_at_pps_lo = dd.ddsGetCoreBcc(0xc)
        rf_at_pps_hi = dd.ddsGetCoreBcc(0x10)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        if verbose == "-v" :
            print("datetime now after TAI read is {}".format(pendul.datetime.now()))
            print("datetime TAI is        {}".format(pendul.datetime.utcfromtimestamp(tai_sec_at_pps)))
            #print("datetime tzinfo is     {}".format(pendul.datetime.tzinfo))
            if pendul.tzinfo is None: print("tzinfo is none")
            else: print("tzinfo exists")
            #print("datetime utcoffset is  {}".format(pendul.tzinfo.utcoffset(pendul)))
            #print("datetime POSIX is      {}".format(pendul.datetime.fromtimestamp(tai_sec_at_pps)))
            temps = pendul.datetime(1970, 1, 1, 0, 0, 0) + pendul.timedelta(seconds=tai_sec_at_pps)
            print("calculated datetime from 1970/1/1 {}".format(temps))
        leretour = [tai_sec_at_pps, rf_at_pps_lo, rf_at_pps_hi]
        return leretour
    else:
        return [0, 0, 0]

# ------------------------------------------------------
def t0stat(buss, master_bit, verbose):
# ------------------------------------------------------
    """Get module T0 counters values and check consistency
       master : t0_cnt only
       slave  : t0_cnt and t0bis_cnt"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    # get vhdl t0_cnt
    t0_cnt = dd.ddsGetT0Cnt()
    if verbose == "-v" :print("t0_cnt    = {}".format(t0_cnt))
    # now t0bis slave only
    if master_bit == 0:
        #get t0bis_cnt value
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_T0BIS)
        dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010)            
        time.sleep(0.01)
        t0bis_cnt = dd.ddsGetCoreBcc(0x8)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        #get t0bis_miss WR stamp from CoreBcc
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_T0BIS_MISS)
        dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010) 
        time.sleep(0.01)
        #value to be printed lower
        t0bis_tai_sec = dd.ddsGetCoreBcc(0x8)
        t0bis_tai_cyc = dd.ddsGetCoreBcc(0xc)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        if verbose == "-v" :print("t0bis_cnt = {}".format(t0bis_cnt))
        if verbose == "-v" :print("t0bis_TAI sec/cyc = {}/{}".format(t0bis_tai_sec, t0bis_tai_cyc))
        if t0bis_tai_sec > 0:
            temps = pendul.datetime(1970, 1, 1, 0, 0, 0) + pendul.timedelta(seconds=t0bis_tai_sec)
            if verbose == "-v": print("   !!!! TAI time of last T0 broadcast lost = {}".format(temps))
    else:
        t0bis_cnt = 0
        t0bis_tai_sec = 0
        t0bis_tai_cyc = 0
        
    #return
    return [t0_cnt, t0bis_cnt, t0bis_tai_sec, t0bis_tai_cyc]

# ------------------------------------------------------
def mstart(buss):
# ------------------------------------------------------
    """Network wide start/resynchro to be sent to master"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    #request for sending data
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    #set master start id
    dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_START)
    #send data
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    #discard
    dd.ddsDisCoreBcc()
    #optional purge
    dd.ddsPurCoreBcc()

# ------------------------------------------------------
def seqen(buss, rw, vv2, verbose):
    """Monitor SEQ_EN bit in master BCC_CSR"""
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False
    
    if (vv2 == 0) or (vv2 == -1):
        ena = 0
    else:
        ena = 1
    if (verbose == "-v") and (rw == "-w") :print("t0 enable = {}".format(ena))
    
    dd = SpecWhist(ipp)

    #get SEQ_EN bit from BCC_CSR
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_SEQ_EN)
    dd.ddsSetCoreBcc(0xc, 0)  # read
    dd.ddsSetCoreBcc(0x0, 0x4000010)            
    time.sleep(0.01)
    vallu = dd.ddsGetCoreBcc(0x8)
    #discard
    dd.ddsDisCoreBcc()
    #optional purge
    dd.ddsPurCoreBcc()
    if rw == "-r":
        return [vallu, 0]
    elif rw == "-w":
        if vallu == ena:
            tog = 0
        else:
            tog = 1
        #now set SEQ_EN bit value 
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_SEQ_EN)
        dd.ddsSetCoreBcc(0xc, 1)  # write
        dd.ddsSetCoreBcc(0x10, ena)
        dd.ddsSetCoreBcc(0x0, 0x4000010)            
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        if verbose == "-v" :print("t0 enable toggle = {}".format(tog))
        return [vallu, tog]
    else:
        return [0, 0]
# ------------------------------------------------------
def fdbg(buss, dbgen, adjen, stopen, verbose):
# ------------------------------------------------------
    """PPS debug facility management"""
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    
    if dbgen == 0:
        dd.ddsClearRfDbgEn()
    elif dbgen == 1:
        dd.ddsSetRfDbgEn()

    if adjen == 0:
        dd.ddsClearRfAdjEn()
    elif adjen == 1:
        dd.ddsSetRfAdjEn()

    if stopen == 0:
        dd.ddsClearRfAdjStop()
    elif stopen == 1:
        dd.ddsSetRfAdjStop()

    if verbose == "-v":
        print("Device#{}, RFLOST={}, ADJDET={}, ADJRUN={}".format(buss, dd.ddsTestRfLost()[0], dd.ddsTestRfAdjd()[0], dd.ddsTestRfAdjRun()[0]))
        
# ------------------------------------------------------
def rfadj(buss, adjen):
# ------------------------------------------------------
    """RFADJ_EN_BITmanagement"""
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    if adjen == 0:
        dd.ddsClearRfAdjEn()
    elif adjen == 1:
        dd.ddsSetRfAdjEn()
    return [buss, adjen]
    
# ------------------------------------------------------
def si_recall(buss, verbose):
# ------------------------------------------------------
    """Recall Si571 defaults"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    if verbose == "-v": print("Set Si571 Recall")
    #Recall Si571
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 135)
    #Recall is 0x1, auto cleared
    dd.ddsSetCoreBcc(0x14, 0x11)
    dd.ddsSetCoreBcc(0x0, 0x4000010)

# ------------------------------------------------------
def si_dis(buss, dis):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)
    sireg = []
    for chacha in range(7, 13):
        ddm.ddsSetCoreBcc(0x0, 0x1000000)
        ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
        # 0 for read then address
        ddm.ddsSetCoreBcc(0xc, 0)
        ddm.ddsSetCoreBcc(0x10, chacha)
        ddm.ddsSetCoreBcc(0x0, 0x4000010)
        time.sleep(0.01)
        sireg.append(ddm.ddsGetCoreBcc(0x8))
        if dis == True: print("si_reg {} : {}".format(chacha, hex(sireg[chacha-7])))
        #discard
        ddm.ddsDisCoreBcc()
        #optional purge
        ddm.ddsPurCoreBcc()
    # now 135
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 0 for read
    ddm.ddsSetCoreBcc(0xc, 0)
    ddm.ddsSetCoreBcc(0x10, 135)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    sireg.append(ddm.ddsGetCoreBcc(0x8))
    if dis == True: print("si_reg {} : {}".format(135, hex(sireg[6])))
    #discard
    ddm.ddsDisCoreBcc()
    #optional purge
    ddm.ddsPurCoreBcc()
    
    # now 137
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 0 for read
    ddm.ddsSetCoreBcc(0xc, 0)
    ddm.ddsSetCoreBcc(0x10, 137)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    sireg.append(ddm.ddsGetCoreBcc(0x8))
    if dis == True: print("si_reg {} : {}".format(137, hex(sireg[7])))
    #discard
    ddm.ddsDisCoreBcc()
    #optional purge
    ddm.ddsPurCoreBcc()

    # now calculate
    hs_div = ((sireg[0] & 0xE0) >> 5) + 4
    n1 = (((sireg[0] & 0x1F) << 2) | ((sireg[1] & 0xC0) >> 6)) + 1
    rfreq = ((sireg[1] & 0x3F) << 32) | (sireg[2] << 24) | (sireg[3] << 16) | (sireg[4] << 8) | sireg[5]
    rst_bit = (sireg[6] & 0x80) >> 7
    nf_bit = (sireg[6] & 0x40) >> 6
    fm_bit = (sireg[6] & 0x20) >> 5
    vcadc_bit = (sireg[6] & 0x10) >> 4
    recall_bit = sireg[6] & 0x1
    fdco_bit = (sireg[7] & 0x10) >> 4

    # now display
    if dis == True:
        print("++++++++++++++++++++++++++++")
        print("===> HS_DIV       = {}".format(hex(hs_div)))
        print("===> N1           = {}".format(hex(n1)))
        print("===> RFREQ        = {}".format(hex(rfreq)))
        print("===> RST_REG      = {}".format(rst_bit))
        print("===> NewFreq      = {}".format(nf_bit))
        print("===> FreezeM      = {}".format(fm_bit))
        print("===> FreezeVCADC  = {}".format(vcadc_bit))
        print("===> RECALL       = {}".format(recall_bit))
        print("===> FreezeDCO    = {}".format(fdco_bit))
        print("++++++++++++++++++++++++++++")

    return [hs_div, n1, rfreq, fm_bit, vcadc_bit]

# ------------------------------------------------------
def si_fset(buss, valo, n1, verbose):
# ------------------------------------------------------
    """Set Si571 RFREQ register"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    if verbose == "-v": print("writing new RFREQ {}".format(hex(valo)))

    # freeze DCO
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 137)
    dd.ddsSetCoreBcc(0x14, 0x10)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)

    # write new RFREQ
    regw = []
    regw.append(((valo >> 32) & 0x3F) | ((n1 & 0x3) << 6))
    if verbose == "-v": print("r8={}".format(hex(regw[0])))
    regw.append((valo >> 24) & 0xFF)
    regw.append((valo >> 16) & 0xFF)
    regw.append((valo >> 8) & 0xFF )
    regw.append(valo & 0xFF)
    # RFREQNEW on registers 8-12
    for chacha in range(8, 13):
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
        # 1 for write, then address and data
        dd.ddsSetCoreBcc(0xc, 1)
        dd.ddsSetCoreBcc(0x10, 20-chacha)
        dd.ddsSetCoreBcc(0x14, regw[20-chacha-8])
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        time.sleep(0.01)
        if verbose == "-v": print("r{}={}".format((20-chacha), hex(regw[20-chacha-8])))

    # unfreeze DCO
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 137)
    dd.ddsSetCoreBcc(0x14, 0x0)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    
    # Assert NewFreq bit
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 135)
    dd.ddsSetCoreBcc(0x14, 0x40)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
           
    print("New frequency set...")

# ------------------------------------------------------
def stup(buss, factor, verbose):
# ------------------------------------------------------
    """Startup module : clear flashed bit, set Si571 frequency"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    rrr = True

    bout = clr_fl(buss)
    #always recall first to be sure starting from factory
    si_recall(buss, verbose)

    time.sleep(1)
    siregs = si_dis(buss, False)
    hsdiv = siregs[0]
    n1 = siregs[1]
    rfreq = siregs[2]
    if verbose == "-v": print("factory RFREQ = {}".format(hex(rfreq)))
    fxtal = (SI571F * n1 * hsdiv * pow(2, 28)) / rfreq
    if verbose == "-v": print("Fxtal = {}".format(fxtal))

    # now set AD9510 outputs accordingly
    if factor == 2:
        # B divider : 0x06 set to 8
        fad95(buss, D3S_CMD_9510, 1, 0x06, 0x08)
        # OUT#0 rfx2 : 0x48 = 0x0 = div 2 -> RF
        fad95(buss, D3S_CMD_9510, 1, 0x48, 0x0)
        # OUT#0 rfx2 : 0x49 = 0x0 unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x49, 0x0)
        # OUT#1 rfx2 : 0x4a = divide by 16 (7+1)x2 -> RF/8
        fad95(buss, D3S_CMD_9510, 1, 0x4a, 0x77)
        # OUT#1 rfx2 : 0x4b =  0x0 unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x4b, 0x0)
        # OUT#2 rfx2 : 0x4c = div 4 (1+1)x2 -> RF/2
        fad95(buss, D3S_CMD_9510, 1, 0x4c, 0x11)
        # OUT#2 rfx2 : 0x4d = 0x0 unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x4d, 0x0)
        # OUT#3 off
        # OUT#4 rfx2 : 0x50 = div 16 (7+1)x2 -> RF/8
        fad95(buss, D3S_CMD_9510, 1, 0x50, 0x77)
        # OUT#4 rfx2 : 0x51 = 0x0 unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x51, 0x0)
        # OUT#5 rfx2 : 0x52 = no change 0x0 = div 2 -> RF
        fad95(buss, D3S_CMD_9510, 1, 0x52, 0x0)
        # OUT#5 rfx2 : 0x53 = unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x53, 0x0)
        # OUT#6 rfx2 : 0x54 = no change 0x0 = div 2 -> RF
        fad95(buss, D3S_CMD_9510, 1, 0x54, 0x0)
        # OUT#6 rfx2 : 0x55 = unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x55, 0x0)
        # OUT#7 rfx2 : 0x56 = no change 0x0 = div 2 -> RF
        fad95(buss, D3S_CMD_9510, 1, 0x56, 0x0)
        # OUT#7 rfx2 : 0x57 = unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x57, 0x0)
        # FUNCTION PIN to SYNCB : [6..5]=01b -> 0x20
        fad95(buss, D3S_CMD_9510, 1, 0x58, 0x20)
        # update regs 0x5a=1
        fad95(buss, D3S_CMD_9510, 1, 0x5a, 0x1)
    else:
        # B divider : 0x06 set to 4
        fad95(buss, D3S_CMD_9510, 1, 0x06, 0x04)
        # OUT#0 rf : 0x48 = 0x0 = div 2 default
        fad95(buss, D3S_CMD_9510, 1, 0x48, 0x0)
        # OUT#0 rf : 0x49 = 0x80 bypass divider, phase=0 -> RF
        fad95(buss, D3S_CMD_9510, 1, 0x49, 0x80)
        # OUT#1 rf : 0x4a = divide by 8 (3+1)x2 -> RF/8
        fad95(buss, D3S_CMD_9510, 1, 0x4a, 0x33)
        # OUT#1 rf : 0x4b =  0x0 unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x4b, 0x0)
        # OUT#2 rf : 0x4c = div 2 (0+1)x2 -> RF/2
        fad95(buss, D3S_CMD_9510, 1, 0x4c, 0x0)
        # OUT#2 rf : 0x4d = 0x0 unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x4d, 0x0)
        # OUT#3 off
        # OUT#4 rf : 0x50 = div 8 (3+1)x2 -> RF/8
        fad95(buss, D3S_CMD_9510, 1, 0x50, 0x33)
        # OUT#4 rf : 0x51 = 0x0 unbypass divider, phase=0
        fad95(buss, D3S_CMD_9510, 1, 0x51, 0x0)
        # OUT#5 rf : 0x52 = no change 0x0 = div 2 default
        fad95(buss, D3S_CMD_9510, 1, 0x52, 0x0)
        # OUT#5 rf : 0x53 = 0x80 bypass divider, phase=0 -> RF
        fad95(buss, D3S_CMD_9510, 1, 0x53, 0x80)
        # OUT#6 rf : 0x54 = no change 0x0 = div 2 default
        fad95(buss, D3S_CMD_9510, 1, 0x54, 0x0)
        # OUT#6 rf : 0x55 = 0x80 bypass divider, phase=0 -> RF
        fad95(buss, D3S_CMD_9510, 1, 0x55, 0x80)
        # OUT#7 rf : 0x56 = no change 0x0 = div 2 default
        fad95(buss, D3S_CMD_9510, 1, 0x56, 0x0)
        # OUT#7 rf : 0x57 = 0x80 bypass divider, phase=0 -> RF
        fad95(buss, D3S_CMD_9510, 1, 0x57, 0x80)
        # update regs 0x5a=1
        fad95(buss, D3S_CMD_9510, 1, 0x5a, 0x1)

    # then set esrf frequency
    if factor == 2:
        if verbose == "-v": print("Setting RFx2, n1=0")
        rfreqnew = int(rfreq * ESRFRF / SI571F)
        if verbose == "-v": print("writing rfx2 RFREQ {}".format(hex(rfreqnew)))
        si_fset(buss, rfreqnew, 0, verbose)
    else:
        if verbose == "-v": print("Resetting RF, n1=1")
        rfreqnew = int(rfreq * ESRFRF / SI571F)
        if verbose == "-v": print("writing rfx1 RFREQ {}".format(hex(rfreqnew)))
        si_fset(buss, rfreqnew, 1, verbose)
        # align outputs phase
    fpllrfs(buss)

    return rrr

# ------------------------------------------------------
def fpgaver(buss):
# ------------------------------------------------------
    '''function to get FPGA version
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False
    return hex(dd.ddsGetFPGA())

# ------------------------------------------------------
def fad95(buss, cmd95x, rwb, addb, valb):
# ------------------------------------------------------
    '''function to R/W into AD9516/10
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    if cmd95x ==  D3S_CMD_9510:
        upda = 0x5a
    elif cmd95x ==  D3S_CMD_9516:
        upda = 0x232
        
    dd = SpecWhist(ipp)

    #request
    dd.ddsSetCoreDds(0x0, 0x1000000)
    #set control command 0xa for ad9516, 0xb for ad9510
    dd.ddsSetCoreDds(0x8, cmd95x)
    #dummy
    dd.ddsSetCoreDds(0xc, 0)
    #set r=0/w=1
    dd.ddsSetCoreDds(0x10, rwb)
    #offset
    dd.ddsSetCoreDds(0x14, addb)
    #value to write
    if rwb == 1:
        dd.ddsSetCoreDds(0x18, valb)
    #send request  0x4000010 r or w
    dd.ddsSetCoreDds(0x0, 0x4000010)
    #Now update regs if w otherwise read value
    if rwb == 1:
        dd.ddsSetCoreDds(0x0, 0x1000000)
        dd.ddsSetCoreDds(0x8, cmd95x)
        dd.ddsSetCoreDds(0xc, 0)
        dd.ddsSetCoreDds(0x10, rwb)
        #update register offset
        dd.ddsSetCoreDds(0x14, upda)
        #write 1 to bit 0
        dd.ddsSetCoreDds(0x18, 1)
        #send request  0x4000010
        dd.ddsSetCoreDds(0x0, 0x4000010)
    elif rwb == 0:
        #read data
        time.sleep(0.01)
        vallu = dd.ddsGetCoreDds(0x10)
        #print("@ {} : {}".format(hex(addb), hex(vallu)))
        #discard
        dd.ddsDisCoreDds()
        #optional purge
        dd.ddsPurCoreDds()
        return vallu

# ------------------------------------------------------
def puprog(buss, ch, enab, pol, mod, srce, delm, widi):
# ------------------------------------------------------

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    cha = ch-1
    if (cha < 0) or (cha > 11):
        print("bad channel")
        return False

    if srce == -1: srce = 0
    # delay is now set as RF periods but counted as RF/8 for pulses
    if delm == -1:
        deli = 0
    else:
        deli = 8 * delm
    if widi == -1: widi = 0

    dd = SpecWhist(ipp)

    if (enab == 0) or (enab == -1):
        dd.ddsClearPuCkEn(cha) # disable
    else:
        dd.ddsSetPuCkEn(cha)  # set enable
        if mod == 2:
            dd.ddsSetPuCkMode(cha, 0x2)  # set type rf/8 pulse
        elif mod == 1:
            dd.ddsSetPuCkMode(cha, 0x1)  # set type gun
        else:
            dd.ddsSetPuCkMode(cha, 0x0)  # unused default
        if pol == 1:
            dd.ddsSetPuCkPol(cha)
        else:
            dd.ddsClearPuCkPol(cha)
        dd.ddsSetPuCkSrce(cha, srce)
        dd.ddsSetPulDel(cha, deli)
        dd.ddsSetPulWid(cha, widi)

# ------------------------------------------------------
def ckprog(buss, ch, ene, src, pol, pha, kof):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False
        
    cha = ch-1
    if (cha < 0) or (cha > 11):
        print("bad channel")
        return False

    dd = SpecWhist(ipp)

    if (ene == 0) or (ene == -1):
        dd.ddsClearPuCkEn(cha) # disable
    else:
        dd.ddsSetPuCkEn(cha)  # set enable + type clock
        dd.ddsSetPuCkMode(cha, 0x3)  # set clock mode
        if pol == 1:
            dd.ddsSetPuCkPol(cha)
        else:
            dd.ddsClearPuCkPol(cha)
        dd.ddsSetPuCkSrce(cha, src)
        if src != 3:
            if pha == -1: pha = 0
            dd.ddsSetPulDel(cha, pha)
        else:
            if kof == -1: kof = 0
            kopha = (kof<<10) + (pha<<21)
            dd.ddsSetPulDel(cha, kopha)

# ------------------------------------------------------
def phck(buss, ch, sq, pas, div):
# ------------------------------------------------------
    """shift  delay on channel clock  with time step
    if -srd mode add division factor and 50% option"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    cha = ch-1
    div = div-1
    if (div == -1) or (div == 0): div = 1

    dd = SpecWhist(ipp)

    dd.ddsSetPuCkEn(cha)  # set enable + type clock
    dd.ddsSetPuCkMode(cha, 0x3)  # set clock mode
    dd.ddsClearPuCkPol(cha) # no polarity
    if (sq == "-4"):
        dd.ddsSetPuCkSrce(cha, 5) # ck4 
    elif (sq == "-16"):
        dd.ddsSetPuCkSrce(cha, 4) # ck16   
    elif (sq == "-sr"):
        dd.ddsSetPuCkSrce(cha, 1) # tcsr
    elif (sq == "-srd"):
        dd.ddsSetPuCkSrce(cha, 3) # clk_sr_div
    elif (sq == "-bo"):
        dd.ddsSetPuCkSrce(cha, 2) # tcboo
    elif (sq == "-tc32"):
        dd.ddsSetPuCkSrce(cha, 0) # tc32
    else:
        dd.ddsSetPuCkSrce(cha, 1) # tcsr default        
    
    feuvert = True
# main loop
    deli = 0
    delk = 0
    print("Parti !   = {}".format(deli))
    while feuvert:
        if (sq == "-4") or (sq == "-16"):
            dd.ddsSetPulDel(cha, deli + (delk % 8)*32)            
        elif (sq == "-srd"):
            pha = deli//8
            kpha = pha//62
            ppha = pha%62
            rpha = (kpha<<6) + ppha
            kopha = (div<<10) + (rpha<<3) + deli%8
            dd.ddsSetPulDel(cha, kopha)
            dd.ddsSetSrDivPh(cha, delk)
        elif (sq == "-sr"):
            delo = deli & 0x3ff # basic delay
            dela = ((delo/8) + 62) % 124# toggle delay 1/2 period later
            delw = (dela << 10) | delo
            #print("delw={}".format(hex(delw)))
            dd.ddsSetPulDel(cha, delw)
        elif (sq == "-bo"):
            delo = deli & 0x1ff # basic delay
            dela = ((delo/8) + 22) % 44# toggle delay 1/2 period later
            delw = (dela << 10) | delo
            #print("delw={}".format(hex(delw)))
            dd.ddsSetPulDel(cha, delw)
        elif (sq == "-tc32"):
            delo = deli & 0x1ff # basic delay
            #print("delw={}".format(hex(delw)))
            dd.ddsSetPulDel(cha, delo)
        try:
            time.sleep(float(pas)/10)
        except KeyboardInterrupt:
            return
        deli += 1
        if (deli == 31) and ((sq == "-4") or (sq == "-16")):
            deli = 0
            delk += 1
        elif (sq == "-bo") and (deli == 352):
            deli = 0
        elif (sq == "-tc32") and (deli == 32):
            deli = 0
        elif (sq == "-sr") and (deli == 992):
            deli = 0
        elif (sq == "-srd") and (deli == 992):
            deli = 0
            # coarse phase up to div
            if delk < div:
                delk += 1
            else:
                delk = 0
            
# ------------------------------------------------------
def srdiv(buss, ch, division, phsr, phafi):
# ------------------------------------------------------
    '''easy CLK_SR_DIV programming with intuitive parameters
    channel, division factor, coarse phase, fine phase
    Forces enable. To disable use ck command.
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False
    dd = SpecWhist(ipp)
    
    cha = ch-1
    div = division-1
    if (cha < 0) or (cha > 11):
        print("bad channel")
        return False
    # min value
    if (div == -1) or (div == 0): div = 1
    if phsr < 0 : phsr = 0
    if phsr > div: phsr = 0
    #reorganize phsr as k*62+p
    pha = phafi//8
    kpha = pha//62
    ppha = pha%62
    rpha = (kpha<<6) + ppha
    # enable, clock mode, clear polarity
    dd.ddsSetPuCkMode(cha, 0x3)
    dd.ddsSetPuCkEn(cha)
    dd.ddsClearPuCkPol(cha)
    # source = sr_div
    dd.ddsSetPuCkSrce(cha, 0x3)
    # program
    kopha = (div<<10) + (rpha<<3) + phafi%8
    dd.ddsSetPulDel(cha, kopha)
    dd.ddsSetSrDivPh(cha, phsr)
            
# ------------------------------------------------------
def rfn(buss, enab, divi):
# ------------------------------------------------------
    '''program RF/n into AD9510 OUTPUT#2
    calls fad95 function
    '''
    if (enab == 0):
        # set disable : AD9510 output#2, register 0x3e
        #OUT2: [1..0] 2= safe power down = LVPECL OFF
        #      [3..2] 2=default=810mV output
        fad95(buss, D3S_CMD_9510, 1, 0x3e, 0x0a)
        return True
    else:
        if (divi > 32): divi = 32
        # set enable : AD9510 output#2, register 0x3e
        #OUT2: [1..0] 0=LVPECL ON
        #      [3..2] 2=default=810mV output
        fad95(buss, D3S_CMD_9510, 1, 0x3e, 0x08)
        #bypass or not
        if (divi == 1):
            # set bypass divider to AD9510 output#2, register 0x4d
            #OUT2: [3..0] 0=no phase offset
            #      [7..4] 8=bypass
            fad95(buss, D3S_CMD_9510, 1, 0x4d, 0x80)
        else:
            # unset bypass divider to AD9510 output#2, register 0x4d
            #OUT2: [3..0] 0=no phase offset
            #      [7..4] 0=No bypass, No Sync, No Force, Start = Low
            fad95(buss, D3S_CMD_9510, 1, 0x4d, 0x00)
            #now set division
            divh = (divi // 2) - 1 + (divi % 2)
            divl = (divi // 2) - 1
            #now program division in reg 0x4c
            #OUT2: [3..0] divl=nb of cycles high
            #      [7..4] divh=nb of cycles low
            divp = (divl << 4) + divh
            fad95(buss, D3S_CMD_9510, 1, 0x4c, divp)

# ------------------------------------------------------
def fpllrfs(buss):
# ------------------------------------------------------
    '''function trig RF_sync monostable
       function used in pll23 and ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    return dd.ddsSetSyncRf()

# ------------------------------------------------------
def pll23(buss, outch, raval, phcoarse, verbose):
# ------------------------------------------------------
    """scan PLL phase adjust on PLL-outputs 5-6, or set to value"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return 1

    if (outch != 2) and (outch != 3):
        print("Bad channel for fine delay phase adjust {}".format(outch))
        return 1

    # get 0x58 to disriminate master/slave
    r58 = fad95(buss, D3S_CMD_9510, 0, 0x58, 0)
    if r58 == 0x80:
        masl = 1
        printf("Writing into MASTER AD9510 registers")
    else:
        masl = 0
    # unbypass fine delay for channel, PLL5 on ch3, PLL6 on ch2
    fad95(buss, D3S_CMD_9510, 1, (0x38 + (2-outch)*4), 0) 
    # set capacitor & current to default for channel
    fad95(buss, D3S_CMD_9510, 1, (0x39 + (2-outch)*4), PHADJUST) 

    if (raval != "-sc") and (raval != "-pi"): # then set both !
        pfad = (raval << 1) & 0x7f
        # set fine delay for channel
        fad95(buss, D3S_CMD_9510, 1, (0x3a + (2-outch)*4), pfad)
        fad95(buss, D3S_CMD_9510, 1, (0x55 + (2-outch)*2), phcoarse) # 0x80=Master=BypassDiv
        if (phcoarse != 0x80) and (masl == 0): fpllrfs(buss)    # on master this is a AD9510 RESET !
        return 0
    elif raval == "-pi":
        # set phase offset at resync if required
        if (phcoarse == 0) or (phcoarse == 1) or (phcoarse == 0x80): # 0x80=Master=BypassDiv
            fad95(buss, D3S_CMD_9510, 1, (0x55 + (2-outch)*2), phcoarse)
            if (phcoarse != 0x80) and (masl == 0): fpllrfs(buss)    # on master this is a AD9510 RESET !
            return 0
        else:
            return 1
    else:
        # -sc option : scan delta_t
        if verbose == "-v": print("scanning PLL fine phase delay adjust")
        # read PI coarse phase
        pif = (fad95(buss, D3S_CMD_9510, 0, (0x55 + (2-outch)*2), 0)) & 0x1
        if verbose == "-v": print("Pi coarse phase for channel {} is {}.".format(outch, pif))
        irampbits = PHADJUST & 0x7
        iramp = 200*(irampbits+1)
        nocapsbits = (PHADJUST & 0x38) >> 3
        nocaps = 1
        # count number of 0s in nocapsbits
        for nc in range(3):
            if verbose == "-v": print("nocapsbits={}".format(nocapsbits))
            bnc = nocapsbits & 0x1
            if bnc == 0:
                nocaps += 1
            nocapsbits = nocapsbits >> 1
        if verbose == "-v": print("Delay parameters : iramp={} uamp ; nocaps={}".format(iramp, nocaps))
        derange = (200*(nocaps+3)*1.3286)/iramp
        if verbose == "-v": print("Delay range = {}".format(derange))
        #        offsetns = 0.34 + (6*(nocaps-1)/iramp) + ((1600-iramp)/1000)
        offsetns = float(0.34 + (6.0*(nocaps-1)/iramp) + ((1600.0-iramp)/10000))
        if verbose == "-v": print("Offset ns   = {}".format(offsetns))
        delayfs = derange*24/31 + offsetns
        if verbose == "-v": print("Delay FS    = {}".format(delayfs))
        feuvert = True
        sck = 0
        while feuvert:
            pfad = (sck << 1) & 0x7f
            fad95(buss, D3S_CMD_9510, 1, (0x3a + (2-outch)*4), pfad)
            ajuste = offsetns + derange*sck/31
            if verbose == "-v":
                print("delay adjust {} :   {} ns".format(sck, ajuste))
            else:
                print(".", end=' ')
                sys.stdout.flush()
            try:
                time.sleep(0.5)
            except KeyboardInterrupt:
                print("\nScan stopped at {}.".format(sck))
                return 0
            sck += 1
            if sck == 0x19:
                sck = 0
            
# ------------------------------------------------------
def setfdel(buss, chan, vala):
# ------------------------------------------------------
    """set i2c fine delays"""
    #print("buss {}".format(buss))
    ipp = bus_lup(str(buss))
    #print("ipp {}".format(ipp))
    if ipp == '0':
        return 1

    if chan == 1:
        lu70 = rd_i2c(ipp, i2c_70)
        lu72 = rd_i2c(ipp, i2c_72)
        valu = (lu70>>1) | (lu72&0x3)<<7
        #print("70 {}, 72 {}, delay is {}".format(hex(lu70), hex(lu72), hex(valu)))
        val70 = ((vala&0x7f)<<1) | (lu70&0x1)
        val72 = ((vala&0x180)>>7) | (lu72&0xfc)
        #print("70 {}, 72 {}, delay is {}".format(hex(val70), hex(val72), hex(vala)))
        wr_i2c(ipp, i2c_70, val70)
        wr_i2c(ipp, i2c_72, val72)
        return 0
    elif chan == 2:
        lu72 = rd_i2c(ipp, i2c_72)
        lu74 = rd_i2c(ipp, i2c_74)
        valu = (lu72>>3) | (lu74&0xf)<<5
        #print("72 {}, 74 {}, delay is {}".format(hex(lu72), hex(lu74), hex(valu)))
        val72 = ((vala&0x1f)<<3) | (lu72&0x7)
        val74 = ((vala&0x1e0)>>5) | (lu74&0xf0)
        #print("72 {}, 74 {}, delay is {}".format(hex(val72), hex(val74), hex(vala)))
        wr_i2c(ipp, i2c_72, val72)
        wr_i2c(ipp, i2c_74, val74)
        return 0
    elif chan == 3:
        lu74 = rd_i2c(ipp, i2c_74)
        lu76 = rd_i2c(ipp, i2c_76)
        valu = (lu74>>5) | (lu76&0x3f)<<3
        #print("74 {}, 76 {}, delay is {}".format(hex(lu74), hex(lu76), hex(valu)))
        val74 = ((vala&0x7)<<5) | (lu74&0x1f)
        val76 = ((vala&0x1f8)>>3) | (lu76&0xc0)
        #print("74 {}, 76 {}, delay is {}".format(hex(val74), hex(val76), hex(vala)))
        wr_i2c(ipp, i2c_74, val74)
        wr_i2c(ipp, i2c_76, val76)
        return 0
    else:
        return 1

# ------------------------------------------------------
def rfdel(buss, cha, sta, sto, dodo):
# ------------------------------------------------------
    """ramp i2c fine delays"""

    if (dodo == 0) or (dodo == -1) : dodo = 1

    feuvert = True
    # main loop
    ilo = sta
    while feuvert:
        setfdel(buss, cha, ilo)
        print("set fdel {}".format(ilo))

        try:
            time.sleep(dodo)
        except KeyboardInterrupt:
            return
        ilo += 10
        if (ilo >= sto): ilo = sta
        
# ------------------------------------------------------
def stm(buss, nombus):
# ------------------------------------------------------

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    # now set SpecWhist
    dd = SpecWhist(ipp)

    print("\n================> Master : {} -  whist#{}".format(nombus, buss))
    print("RFoE_BIT             : {}".format(dd.ddsTestRfoe()[0]))
    print("---------------------------")
    print("BunchClock enable    : {}".format(dd.ddsTestBunchEn()[0]))
    print("RF enable            : {}".format(dd.ddsTestRfEn()[0]))
    print("GunInj enable        : {}".format(dd.ddsTestGuniEn()[0]))
    print("---------------------------")
    print("T0WD_BIT             : {}".format(dd.ddsTestT0Wd()[0]))
    print("T0 lost              : {}".format(dd.ddsTestT0Lost()[0]))
    print("---------------------------")
    print("352 locked           : {}".format(dd.ddsTest352Lkd()[0]))
    print("WR OK                : {}".format(dd.ddsTestWrOK()[0]))
    print("WR lost              : {}".format(dd.ddsTestWrLost()[0]))
    print("INJ ERROR            : {}".format(dd.ddsTestInjErr()[0]))
    print("---------------------------")
    print("RF Frequency         : {}".format(dd.ddsGetRfFreq()))
    print("---------------------------")
    print("RF at PPS is         : {}".format(hex(dd.ddsGetRfPps()[0])))
    print("RF LOST              : {}".format(dd.ddsTestRfLost()[0]))
    print("========================> whist#{}".format(buss))
    print()
    
# ------------------------------------------------------
def sts(buss, nombus):
# ------------------------------------------------------

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    # now set SpecWhist
    dd = SpecWhist(ipp)

    print("\n================> Slave : {} - whist#{}".format(nombus, buss))
    print("RFoE_BIT             : {}".format(dd.ddsTestRfoe()[0]))
    print("---------------------------")
    print("BunchClock enable    : {}".format(dd.ddsTestBunchEn()[0]))
    print("RF enable            : {}".format(dd.ddsTestRfEn()[0]))
    print("GunInj enable        : {}".format(dd.ddsTestGuniEn()[0]))
    print("---------------------------")
    print("T0WD_BIT             : {}".format(dd.ddsTestT0Wd()[0]))
    print("T0 lost              : {}".format(dd.ddsTestT0Lost()[0]))
    print("---------------------------")
    print("352 locked           : {}".format(dd.ddsTest352Lkd()[0]))
    print("WR OK                : {}".format(dd.ddsTestWrOK()[0]))
    print("WR lost              : {}".format(dd.ddsTestWrLost()[0]))
    print("INJ ERROR            : {}".format(dd.ddsTestInjErr()[0]))
    print("---------------------------")
    print("RF Frequency         : {}".format(dd.ddsGetRfFreq()))
    print("---------------------------")
    print("RF at PPS is         : {}".format(hex(dd.ddsGetRfPps()[0])))
    print("RF LOST              : {}".format(dd.ddsTestRfLost()[0]))
    print("---------------------------")
    print("SlokAOK_BIT          : {}".format(dd.ddsTestSlokAok()[0]))
    print("SlokNOK_BIT          : {}".format(dd.ddsTestSlokNok()[0]))
    print("SR clock AOK         : {}".format(dd.ddsTestSRAok()[0]))
    print("Booster clock AOK    : {}".format(dd.ddsTestBoAok()[0]))
    print("========================> whist#{}".format(buss))
    print()
    
# ------------------------------------------------------
def rfdbg(buss, verbose):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    # now set SpecWhist
    dd = SpecWhist(ipp)
    
    if verbose == "-v":
        for cc in range(8):
            print("!!{}!!".format(buss), end="")
        print()

    msbit = dd.ddsTestSlave()[0]
    if msbit == 1:
        slokrun = dd.ddsTestSlokRun()[0]
        sloknok = dd.ddsTestSlokNok()[0]
        srnok = dd.ddsTestSRNok()[0]
        boonok = dd.ddsTestBoNok()[0]
        slokaok = dd.ddsTestSlokAok()[0]
        sraok = dd.ddsTestSRAok()[0]
        booaok = dd.ddsTestBoAok()[0]
        slokli = [slokrun, sloknok, srnok, boonok, slokaok, sraok, booaok]
        if verbose == "-v":
            if slokrun == 1: print(" !! SlokRUN_BIT            : {}".format(slokrun))
            if sloknok == 1: print(" !! SlokNOK_BIT            : {}".format(sloknok))
            if srnok == 1: print(" !! SR clock NOK           : {}".format(srnok))
            if boonok == 1: print(" !! Booster clock NOK      : {}".format(boonok))
            print("SlokAOK_BIT          : {}".format(slokaok))
            print("SR clock AOK         : {}".format(sraok))
            print("Booster clock AOK    : {}".format(booaok))
            print("---------------------------")
    else:
        slokaok = 0
        sloknok = 0
    dben = dd.ddsTestRfDbgEn()[0]
    l_352 = dd.ddsTest352Lkd()[0]
    rf_lost = dd.ddsTestRfLost()[0]
    adjted = dd.ddsTestRfAdjd()[0]
    if verbose == "-v":
        print("352 locked           : {}".format(l_352))
        print("--- DBG config :")
        print("    RF_DBG_EN            : {}".format(dben))
        print("    RF_ADJ_EN            : {}".format(dd.ddsTestRfAdjEn()[0]))
        print("    RF_ADJ_STOP          : {}".format(dd.ddsTestRfAdjStop()[0]))
        print("--- RF status :")
        print("RF LOST              : {}".format(rf_lost))
        print("RF ADJust needeD     : {}".format(adjted))
        print("RF ADJUST RUN        : {}".format(dd.ddsTestRfAdjRun()[0]))
        if rf_lost == 1:
            print("---------------------------")
            print(" !! RF at PPS=bu44_ppsa  : {}".format(hex(dd.ddsGetRfPps()[0])))
            print(" !! Histo register 0     : {}".format(dd.ddsGetHist0()))
            print(" !! Histo register 1     : {}".format(dd.ddsGetHist1()))
            print(" !! Histo register 2     : {}".format(dd.ddsGetHist2()))
            print(" !! RF DELTA             : {}".format(hex(dd.ddsGetRfDelta())))
            print("---------------------------")
        for cc in range(8):
            print("!!{}!!".format(buss), end="")
        print()
    return [l_352, rf_lost, adjted, dben, slokaok, sloknok]

# ------------------------------------------------------
def statspec(buss):
# ------------------------------------------------------

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    # start with ad9510 bits
    # read Digital Lock Detect enable Bit
    d_9510 = fad95(buss, D3S_CMD_9510, 0, 0xD, 0)
    dldbit = (d_9510 & 0x40) >> 6

    # now set SpecWhist
    dd = SpecWhist(ipp)

    for cc in range(20):
        print("{}-".format(buss), end="")
    print()
#   
    print("FPGA version         : {}".format(hex(dd.ddsGetFPGA())))
    msbit = dd.ddsTestSlave()[0]
    print("T0WDTRIG register    : {}".format(dd.ddsGetT0WdTrig()))
    print("T0WDEN_BIT           : {}".format(dd.ddsTestT0WdEn()[0]))
    print("T0WD_BIT             : {}".format(dd.ddsTestT0Wd()[0]))
    print("---------------------------")
    print("Slave_bit            : {}".format(msbit))
    if msbit == 0:
        #get SEQ_EN bit from BCC_CSR
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_SEQ_EN)
        dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010)            
        time.sleep(0.01)
        vallu = dd.ddsGetCoreBcc(0x8)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        print("SEQ_EN bit (master)  : {}".format(vallu))
        print("T0mux                : {}".format(dd.ddsGetT0Mux()))
    else:
        #get t0bis_cnt from CoreBcc
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_T0BIS)
        dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010)            
        time.sleep(0.01)
        #value to be printed lower
        t0bis = dd.ddsGetCoreBcc(0x8)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        #get t0bis_miss WR stamp from CoreBcc
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_T0BIS_MISS)
        dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010) 
        time.sleep(0.01)
        #value to be printed lower
        t0bis_tai_sec = dd.ddsGetCoreBcc(0x8)
        t0bis_tai_cyc = dd.ddsGetCoreBcc(0xc)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        print("SlokAOK_BIT          : {}".format(dd.ddsTestSlokAok()[0]))
        print("AD9510_DLD_DISABLE_BIT    : {}".format(dldbit))
        print("AD9510_DLD_LOSS_BIT       : {}".format(dd.ddsTestPllStat()[0]))
        print("DLD_LOSS_LATCH_EN_BIT     : {}".format(dd.ddsTestDdsLkdEn()[0]))
        print("LATCHED_DLD_LOSS_BIT      : {}".format(dd.ddsTestPllLoss()[0]))
        print("SlokNOK_BIT            : {}".format(dd.ddsTestSlokNok()[0]))
        print("SR clock AOK           : {}".format(dd.ddsTestSRAok()[0]))
        print("Booster clock AOK      : {}".format(dd.ddsTestBoAok()[0]))
    print("---------------------------")
    print("Flashed_BIT          : {}".format(dd.ddsTestFlashed()[0]))
    print("Quiet_BIT            : {}".format((dd.ddsTestQuiet()[0])))
    print("RtD3sRun_BIT         : {}".format(dd.ddsTestRtD3sRun()[0]))
    print("RFoE_BIT             : {}".format(dd.ddsTestRfoe()[0]))
    print("---------------------------")
    print("BunchClock enable    : {}".format(dd.ddsTestBunchEn()[0]))
    print("RF enable            : {}".format(dd.ddsTestRfEn()[0]))
    print("GunInj enable        : {}".format(dd.ddsTestGuniEn()[0]))
    print("---------------------------")
    print("Stamping register    : {}".format(dd.ddsGetStamp()))
    print("---------------------------")
    print("Phase Detect. locked : {}".format(dd.ddsTestPdLkd()[0]))
    print("352 locked           : {}".format(dd.ddsTest352Lkd()[0]))
    print("352 phase adjust     : {}".format(dd.ddsGetFpgaPhAdj()))
    print("Clocks phase adjust  : {}".format(dd.ddsGetOutckPhAdj()))
    print("---------------------------")
    print("WR OK                : {}".format(dd.ddsTestWrOK()[0]))
    print("WR lost              : {}".format(dd.ddsTestWrLost()[0]))
    print("INJ ERROR            : {}".format(dd.ddsTestInjErr()[0]))
    print("T0 lost              : {}".format(dd.ddsTestT0Lost()[0]))
    print("T0 counter           : {}".format(dd.ddsGetT0Cnt()))
    if msbit != 0:
        print("T0BIS counter        : {}".format(t0bis))
        print("T0BIS_MISS TAI SEC   : {}".format(t0bis_tai_sec))
        print("T0BIS_MISS TAI CYC   : {}".format(t0bis_tai_cyc))
    print("---------------------------")
    print("RF Frequency         : {}".format(dd.ddsGetRfFreq()))
    print("T0 delay             : {}".format(dd.ddsGetT0Del()))
    print("Injection delay      : {} ; width : {}".format(dd.ddsGetInjDel(), dd.ddsGetInjWid()))
    print("Extraction delay     : {} ; width : {}".format(dd.ddsGetExtDel(), dd.ddsGetExtWid()))
    print("---------------------------")
    print("MB mode              : {}".format(dd.ddsTestMBEn()[0]))
    print("Bunch List Start     : {}".format(dd.ddsGetBlistStart()))
    print("Bunch List Stop      : {}".format(dd.ddsGetBlistStop()))
    print("Current Bunch        : {}".format(hex(dd.ddsGetBunchNum())))
    print("Auto Bunch List      : {}".format(dd.ddsTestAutoBlist()[0]))
    print("---------------------------")
    print("Rotinj               : {}".format(dd.ddsTestRotinj()[0]))
    print("Auto Rotinj          : {}".format(dd.ddsTestAutoRotinj()[0]))
    print("Rotinj Code          : {}".format(dd.ddsTestRotiCode()[0]))
    print("---------------------------")
    print("RF at PPS is         : {}".format(hex(dd.ddsGetRfPps()[0])))
    print("RF LOST              : {}".format(dd.ddsTestRfLost()[0]))
    print("---------------------------")
    for cc in range(20):
        print("{}-".format(buss), end="")
    print()

# ------------------------------------------------------
def statversion(buss):
# ------------------------------------------------------

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    # start with ad9510 bits
    # read Digital Lock Detect enable Bit
    d_9510 = fad95(buss, D3S_CMD_9510, 0, 0xD, 0)
    dldbit = (d_9510 & 0x40) >> 6

    # now set SpecWhist
    dd = SpecWhist(ipp)

    fpgaver = dd.ddsGetFPGA()
    #get CoreBcc version
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_VERSION)
    dd.ddsSetCoreBcc(0xc, 0)  # read
    dd.ddsSetCoreBcc(0x0, 0x4000010)            
    time.sleep(0.01)
    bccver = dd.ddsGetCoreBcc(0x8)
    #discard
    dd.ddsDisCoreBcc()
    #optional purge
    dd.ddsPurCoreBcc()
    #get CoreEvt version, not for 78
    if fpgaver >= 80:
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        dd.ddsSetCoreEvt(0x8, EVT_CMD_RD_VERSION)
        dd.ddsSetCoreEvt(0xc, 0)  # read
        dd.ddsSetCoreEvt(0x0, 0x4000010)            
        time.sleep(0.01)
        evtver = dd.ddsGetCoreEvt(0x8)
        #discard
        dd.ddsDisCoreEvt()
        #optional purge
        dd.ddsPurCoreEvt()
    else:
        evtver = 0
        
    return [fpgaver, bccver, evtver]

# ------------------------------------------------------
def t0wd(buss, verbose):
# ------------------------------------------------------
    """T0 Watch Dog menu : board_bus_address dis/enable (trig level)"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False
    dd = SpecWhist(ipp)

    t0wdlev = dd.ddsGetT0WdTrig()
    t0wdena = dd.ddsTestT0WdEn()[0]
    t0wdbit = dd.ddsTestT0Wd()[0]
    if verbose == "-v": print("T0WDTRIG register    : {}".format(t0wdlev))
    if verbose == "-v": print("T0WDEN_BIT           : {}".format(t0wdena))
    if verbose == "-v": print("T0WD_BIT             : {}".format(t0wdbit))

    # manage watch dog bit
    if (verbose == "-v") and (t0wdbit == 1):
        choice = raw_input("t0wdbit has triggered : Reset bit ? : y/[n] >>  ")
        jn = choice.lower()
        if jn == 'y':
            dd.ddsClearT0WdEn()
            time.sleep(0.1)
            dd.ddsSetT0WdEn()
    # set enable bit
    elif (verbose == "-v") and (t0wdena == 0):
        choice = raw_input("Enable T0 watch dog ? : y/[n] >>  ")
        jn = choice.lower()
        if jn == 'y':
            dd.ddsSetT0WdEn()
    return [t0wdena, t0wdbit]

# ------------------------------------------------------
def enprog(buss, bunch, rf, gun):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    if bunch != 0:
        dd.ddsSetBunchEn()
    else:
        dd.ddsClearBunchEn()
    if rf != 0:
        dd.ddsSetRfEn()
    else:
        dd.ddsClearRfEn()
    if gun != 0:
        dd.ddsSetGuniEn()
    else:
        dd.ddsClearGuniEn()
        
# ------------------------------------------------------
def injerrst(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    dd.ddsSetInjerRst()
    
# ------------------------------------------------------
def autoblist(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    dd.ddsSetAutoBlist()
    
# ------------------------------------------------------
def wrlostclear(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    wrlink = dd.ddsTestWrOK()[0]
    wrlost = dd.ddsTestWrLost()[0]

    if wrlost == 1:
        print("       WR lost bit  = {}".format(wrlost))
        print("       Now wait for automatic WR recovering")
        print("RFoE is {}".format(dd.ddsTestRfoe()[0]))
    else:
        print("************** UNEXPECTED WR STATUS ***************")
        return False

    # Wait 80s for WR re-lock
    echec = False
    for ttt in range(40):
        time.sleep(2)
        if dd.ddsTestWrOK()[0] == 1:
            print("====> WR re-locked")
            break
        else:
            print("!!! WR remains unlocked after 80 s !!!!")
            echec = True

    # if OK proceed with WR_lost clear
    if echec == True: return False
    else:
        print("       Now clear WR_lost")
        dd.ddsClearRfEn()
        time.sleep(0.1)
        dd.ddsSetRfEn()
        wrlink1 = dd.ddsTestWrOK()[0]
        wrlost1 = dd.ddsTestWrLost()[0]
        print("       WR OK bit    = {}".format(wrlink1))
        print("       WR lost bit  = {}".format(wrlost1))
        if (wrlink1 != 1) and (wrlost1 != 0):
            print("************** UNEXPECTED WR STATUS ***************")
            return False

    #wait some time for RFoE locked
    print("Wait for RFoE locked")
    echec = True
    for ttt in range(30):
        time.sleep(2)
        if dd.ddsTestRfoe()[0] == 1:
            print("====> RFoE locked - PLL enabled")
            echec = False
            break
    if echec == True:
        print("!!! RFoE remains unlocked after 60 s !!!!")

    return echec

# ------------------------------------------------------
def buckliste_y():
# ------------------------------------------------------
    """Bunch clock parameters : default
       """
    mlist = dw_tango
    return mlist

# ------------------------------------------------------
def tliste_y():
# ------------------------------------------------------
    """Network architecture : menu
       """
    choice = raw_input(" >> Network config > Master_bus :  ")
    m0 = get_v(0, choice)
    choice = raw_input(" >> Network config > Slave#1_bus :  ")
    sl1 = get_v(0, choice)
    choice = raw_input(" >> Network config > Slave#2_bus :  ")
    sl2 = get_v(0, choice)
    choice = raw_input(" >> Network config > dut#1_bus :  ")
    dut1 = get_v(0, choice)
    choice = raw_input(" >> Network config > dut#2_bus :  ")
    dut2 = get_v(0, choice)
    if dut2 == 0:
        return [m0, sl1, sl2, dut1]
    choice = raw_input(" >> Network config > dut#3_bus :  ")
    dut3 = get_v(0, choice)
    if dut3 == 0:
        return [m0, sl1, sl2, dut1, dut2]
    choice = raw_input(" >> Network config > dut#4_bus :  ")
    dut4 = get_v(0, choice)
    if dut4 == 0:
        return [m0, sl1, sl2, dut1, dut2, dut3]
    choice = raw_input(" >> Network config > dut#5_bus :  ")
    dut5 = get_v(0, choice)
    if dut5 == 0:
        return [m0, sl1, sl2, dut1, dut2, dut3, dut4]
    choice = raw_input(" >> Network config > dut#6_bus :  ")
    dut6 = get_v(0, choice)
    if dut6 == 0:
        return [m0, sl1, sl2, dut1, dut2, dut3, dut4, dut5]
    choice = raw_input(" >> Network config > dut#7_bus :  ")
    dut7 = get_v(0, choice)
    if dut7 == 0:
        return [m0, sl1, sl2, dut1, dut2, dut3, dut4, dut5, dut6]
    choice = raw_input(" >> Network config > dut#8_bus :  ")
    dut8 = get_v(0, choice)
    if dut8 == 0:
        return [m0, sl1, sl2, dut1, dut2, dut3, dut4, dut5, dut6, dut7]
    choice = raw_input(" >> Network config > dut#9_bus :  ")
    dut9 = get_v(0, choice)
    if dut9 == 0:
        return [m0, sl1, sl2, dut1, dut2, dut3, dut4, dut5, dut6, dut7, dut8]
    else:
        return [m0, sl1, sl2, dut1, dut2, dut3, dut4, dut5, dut6, dut7, dut8, dut9]

# ------------------------------------------------------
def buckliste_n():
# ------------------------------------------------------
    """Bunch clock parameters : menu
       """
    choice = raw_input(" >> Bunch clock params > T0_DELAY :  ")
    t0_del = get_v(0, choice)
    choice = raw_input(" >> Bunch clock params > INJ_DELAY :  ")
    inj_del = get_v(0, choice)
    choice = raw_input(" >> Bunch clock params > EXT_DELAY :  ")
    ext_del = get_v(0, choice)
    choice = raw_input(" >> Bunch clock params > INJ_WIDTH :  ")
    wid_inj = get_v(0, choice)
    choice = raw_input(" >> Bunch clock params > EXT_WIDTH :  ")
    dwid_ext = get_v(0, choice)
    return [t0_del, inj_del, ext_del, wid_inj, wid_ext]

# ------------------------------------------------------
def dwprog(buss, bulle):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetT0Del(bulle[0])
    dd.ddsSetInjDel(bulle[1])
    dd.ddsSetExtDel(bulle[2])
    dd.ddsSetInjWid(bulle[3])
    dd.ddsSetExtWid(bulle[4])
    # reset bunch enable
    #dd.ddsClearBunchEn()
    #dd.ddsSetBunchEn()

# ------------------------------------------------------
def t0delprog(buss, tz):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetT0Del(tz)

# ------------------------------------------------------
def idelprog(buss, ide):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetInjDel(ide)

# ------------------------------------------------------
def edelprog(buss, ede):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetExtDel(ede)

# ------------------------------------------------------
def iwidprog(buss, iwi):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetInjWid(iwi)

# ------------------------------------------------------
def ewidprog(buss, ewi):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetExtWid(ewi)

# ------------------------------------------------------
def pipebunch(buss):
# ------------------------------------------------------
    """Tell master to broadcast bunch clock parameters pipeline trigger
       abort/return if not master"""
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    #test master
    msbit = dd.ddsTestSlave()[0]
    if msbit == 0:
        #pipe bunch clock registers
        #CoreBcc request for sending data
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #set master command 
        dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_BCL_PIPE)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        print("     Bunch clock parameters transfered to pipeline.")
    else:
        print("     !!!! Requested module IS NOT MASTER !!!!")

# ------------------------------------------------------
def dwrd(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    t0del = dd.ddsGetT0Del(hexformat=False)
    deli = dd.ddsGetInjDel(hexformat=False)
    dele = dd.ddsGetExtDel(hexformat=False)
    widi = dd.ddsGetInjWid(hexformat=False)
    wide = dd.ddsGetExtWid(hexformat=False)
    rfreq = dd.ddsGetRfFreq()
    # reset bunch enable
    dd.ddsClearBunchEn()
    dd.ddsSetBunchEn()
    return [t0del, deli, dele, widi, wide, rfreq]

# ------------------------------------------------------
def ffdset(fdliste):
# ------------------------------------------------------
    """Set factory metastability and output fine delays of outputs#1,2,3
       """
    print("ffdset liste : {}".format(fdliste))
    rr = 0
    # metastability
    rr += setfdel(fdliste[0], 1, fdliste[1])
    rr += pll23(fdliste[0], 2, fdliste[4], fdliste[5], "-nov")
    rr += pll23(fdliste[0], 3, fdliste[6], fdliste[7], "-nov")
    # output
    rr += setfdel(fdliste[0], 2, fdliste[2])
    rr += setfdel(fdliste[0], 3, fdliste[3])
    return rr

# ------------------------------------------------------
def evt(buss, tre, dex, ena, ide, verbose, source, delay):
# ------------------------------------------------------
    """configure event :
       transmit input_index enable/disable event_id
       receive output_index enable/disable event_id
       """

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    if dex not in range(4):
        print("Bad index")
        return False

    dd = SpecWhist(ipp)

    # just in case
    if ide == 0:
        dd.ddsClearInEn(dex)
    # set transmit or receive
    if tre == "-t":
        # build up source word to be written to CoreEvt
        srcecore = ((source[0] & 0x3f) << 26) | ((source[1] & 0xf) << 22) | (source[2] & 0x3fffff)
        if verbose == "-v": print("    evt_core SOURCE = {}".format(hex(srcecore)))
        if verbose == "-v": print("    Configuring #{} as event transmitter on INPUT#{}, EVENT_ID={}".format(ipp, dex, ide))
        #CoreEvt
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        #set command to transmit
        dd.ddsSetCoreEvt(0x8, EVT_CMD_TRANSMITTER)
        goon = 1
    elif tre == "-r":
        if verbose == "-v": print("    Configuring #{} as event receiver, EVENT_ID={}, OUTPUT#{}".format(ipp, ide, dex+9))    
        #CoreEvt
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        #set command to receive
        dd.ddsSetCoreEvt(0x8, EVT_CMD_RECEIVER)
        goon = 1
    else:
        goon = 0
        
    if goon == 1:
        #index
        dd.ddsSetCoreEvt(0xc, dex)
        #enable / disable event
        dd.ddsSetCoreEvt(0x10, ena)
        #event_id
        dd.ddsSetCoreEvt(0x14, ide)
        if tre == "-t": dd.ddsSetCoreEvt(0x18, srcecore)
        #send data
        dd.ddsSetCoreEvt(0x0, 0x4000010)
        #add fifo reset in case...
        dd.ddsResetInFifo(dex)

        # now enable event counter
        #CoreEvt
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        #set command to CNT_EN
        dd.ddsSetCoreEvt(0x8, EVT_CMD_CNT_EN)
        #index
        dd.ddsSetCoreEvt(0xc, dex)
        # transmit / receive
        if tre == "-t": dd.ddsSetCoreEvt(0x10, 1)
        if tre == "-r": dd.ddsSetCoreEvt(0x10, 0)
        #enable
        dd.ddsSetCoreEvt(0x14, ena)
        #send data
        dd.ddsSetCoreEvt(0x0, 0x4000010)

        # now set transmit delay to value
        # if receive : reset delay
        #CoreEvt
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        # set command to DEL
        dd.ddsSetCoreEvt(0x8, EVT_CMD_DEL)
        # index
        dd.ddsSetCoreEvt(0xc, dex)
        # pass delay value
        if tre == "-t": dd.ddsSetCoreEvt(0x10, delay)
        if tre == "-r": dd.ddsSetCoreEvt(0x10, 0)
        # send data
        dd.ddsSetCoreEvt(0x0, 0x4000010)
        
# ------------------------------------------------------
def evt_raz(liste, verbose):
# ------------------------------------------------------
    """Disable all events in liste
       this clears lut_in & lut_out arrays in rt-evt.c
       + clear / disable all outputs#9-12 in liste
       """
    for razi in liste:
        print("RAZ EVT {}".format(razi))
        evt_board_enable(razi, 0)
        for oti in range(4):
            evt(razi, "-r", oti, 0, 0, verbose, SRCE_NO, 0)
            evt(razi, "-t", oti, 0, 0, verbose, SRCE_NO, 0)
            puprog(razi, 9+oti, 0, 0, 0, 0, 0, 0)

# ------------------------------------------------------
def get_evt_trans(liste, verbose):
# ------------------------------------------------------
    """collect active transmit events
       """
    trans = []
    for tranzi in liste[1:]:
        activevt = rd_evt_status(tranzi, "-verbose")
        if activevt != []:
            if verbose == "-v": print("Status_{} = {}".format(tranzi, activevt))
            for lact in activevt:
                if lact[0] == 'i': trans.append(lact)
    return trans

# ------------------------------------------------------
def get_evt_recos(liste, verbose):
# ------------------------------------------------------
    """collect active receive events
       """
    recos = []
    for rzi in liste[1:]:
        activevt = rd_evt_status(rzi, "-verbose")
        if activevt != []:
            if verbose == "-v": print("Status_{} = {}".format(rzi, activevt))
            for lact in activevt:
                if lact[0] == 'o': recos.append(lact)
    return recos

# ------------------------------------------------------
def disp_evts(tral):
# ------------------------------------------------------
    """display detailed events
       """
    tari = 0
    for hop in tral:
        if hop[0] == "i":
            if tari == 0:
                print("\nTransmitted\n   ID     Device#   Input#  Counting  Srce_device  Srce_output  Srce_freq (Hz) Long_Delay ")
                tari = 1
            if hop[5][0] != 0x3f:
                if hop[5][2] != 0:
                    print("  0x{:04x}     {:2d}        {:2d}        {}         {:2d}          {:2d}         {:<7.1f}          {}".format(hop[3], hop[1], hop[2], hop[4], hop[5][0], hop[5][1], (float(SRFREQ) / float((hop[5][2]))), hop[6]))
                else:
                    print("  0x{:04x}     {:2d}        {:2d}        {}         {:2d}          {:2d}                          {}".format(hop[3], hop[1], hop[2], hop[4], hop[5][0], hop[5][1], hop[6]))
            else:
                print("  0x{:04x}     {:2d}        {:2d}        {}         {}                                     {}".format(hop[3], hop[1], hop[2], hop[4], "ext", hop[6]))
        else:
            if tari == 0:
                print("\nReceived\n   ID     Device#   Output#    Counting")
                tari = 2
            print("  0x{:04x}     {:2d}        {:2d}          {}".format(hop[3], hop[1], hop[2]+9, hop[4]))

# ------------------------------------------------------
def evt_board_enable(buss, disen):
# ------------------------------------------------------
    """Disable / enable evt core in module
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    if disen != 1: disen = 0
    
    dd = SpecWhist(ipp)
    
    #CoreEvt actions : enable CoreEvt, skip mode slave by default
    dd.ddsSetCoreEvt(0x0, 0x1000000)
    #set command to enable
    dd.ddsSetCoreEvt(0x8, EVT_CMD_ENABLE)
    #set enable
    dd.ddsSetCoreEvt(0xc, disen)
    #send data
    dd.ddsSetCoreEvt(0x0, 0x4000010)
    return True

# ------------------------------------------------------
def evt_delta_wrts(buss):
# ------------------------------------------------------
    """Read network event latency
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    
    dd.ddsSetCoreEvt(0x0, 0x1000000)
    dd.ddsSetCoreEvt(0x8, EVT_CMD_WRTS)
    dd.ddsSetCoreEvt(0x0, 0x4000010)            

    time.sleep(0.01)

    txsec = dd.ddsGetCoreEvt(0x8)
    txcyc = dd.ddsGetCoreEvt(0xc)
    rxsec = dd.ddsGetCoreEvt(0x10)
    rxcyc = dd.ddsGetCoreEvt(0x14)

    #discard
    dd.ddsDisCoreEvt()
    #optional purge
    dd.ddsPurCoreEvt()

    return [txsec,txcyc,rxsec,rxcyc]

# ------------------------------------------------------
def evt_board_mode(buss, modd):
# ------------------------------------------------------
    """Set master/slave mode for events
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    if modd != 1: modd = 0
    
    dd = SpecWhist(ipp)
    
    #CoreEvt actions : enable CoreEvt, skip mode slave by default
    dd.ddsSetCoreEvt(0x0, 0x1000000)
    #set command to enable
    dd.ddsSetCoreEvt(0x8, EVT_CMD_MODE)
    #set enable
    dd.ddsSetCoreEvt(0xc, (modd+1))
    #send data
    dd.ddsSetCoreEvt(0x0, 0x4000010)

# ------------------------------------------------------
def ddt(buss, sort, opti, rfdel, valu, dodo):
# ------------------------------------------------------
    """Test delta_t stability on outputs#2-3
    scan option : scan Delta_t 0->21
    run : set Delta_t to value then loop"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    # set channel#0 input threshold
    if sort == 3:
        dacLoader(buss, 1, 7, 0x80, "-n")
    elif sort == 2:
        dacLoader(buss, 1, 7, 0x20, "-n")
    else:
        print("Bad output value for test {}".format(sort))
        return False
    # read PI coarse phase
    pif = (fad95(buss, D3S_CMD_9510, 0, (0x55 + (2-sort)*2), 0)) & 0x1
    if (opti == "-s") or (opti == "-r"): print("Pi coarse phase for channel {} is {}.".format(sort, pif))

    # prepare sort output as tc_32
    ftc32(buss, sort-1, rfdel)
    # prepare input#0 : enable / polarity=0 positive
    fddtin(buss, "-n")
    print("config {} and input#0 OK".format(sort))

    if opti == "-r":
        pfad = (valu << 1) & 0x7f
        # set fine delay for channel : out#2=9510#6 ;  out#3=9510#5
        fad95(buss, D3S_CMD_9510, 1, (0x3a + (2-sort)*4), pfad)
        fddtingate(buss, 2)
        print("Test Stats after 2 seconds : {}".format(hex(fddtstat(buss))))
        return True
    elif opti == "-s":
        # one scan Delta_t values
        stats = fddtstat(buss)
        print("Stats before test : {}, {}, {}, {}".format(hex(stats[0]), hex(stats[1]), hex(stats[2]), hex(stats[3])))
        for sck in range(23):
            pfad = (sck << 1) & 0x7f
            fad95(buss, D3S_CMD_9510, 1, (0x3a + (2-sort)*4), pfad)
            fddtingate(buss, 1)
            stats = fddtstat(buss)
            print("Test Stats at {} : {}, {}, {}, {}".format(sck, hex(stats[0]), hex(stats[1]), hex(stats[2]), hex(stats[3])))
    elif opti == "-ls":
        # full scan to detect metastability
        stats = fddtstat(buss)
        # PI loop (0-1)
        for pip in range(2):
            # write coarse phase 0/PI then loop 22 delta_t
            fad95(buss, D3S_CMD_9510, 1, (0x55 + (2-sort)*2), pip)
            fpllrfs(buss)
            print("Coarse phase : {}*PI".format(pip))
            # RF fine delay loop (0-7)
            for prf in range(8):
                # write prf using ftc32 function
                ftc32(buss, sort-1, prf)
                print("RF phase : {}".format(prf))
                # delta_t loop (0-22)
                for sck in range(23):
                    pfad = (sck << 1) & 0x7f
                    fad95(buss, D3S_CMD_9510, 1, (0x3a + (2-sort)*4), pfad)
                    fddtingate(buss, 1)
                    stats = fddtstat(buss)
                    print("Test Stats at {} : {}, {}, {}, {}".format(sck, hex(stats[0]), hex(stats[1]), hex(stats[2]), hex(stats[3])))
    elif opti == "-l":
        # loop on scan Delta_t values
        feuvert = True
        while feuvert:
            dtsta = []
            print("-------> at {}".format(pendul.datetime.now()))
            for sck in range(23):
                pfad = (sck << 1) & 0x7f
                fad95(buss, D3S_CMD_9510, 1, (0x3a + (2-sort)*4), pfad)
                # gate 1s
                fddtingate(buss, 1)
                stats = fddtstat(buss)
                if (stats[0]+stats[1]+stats[3]) == 0x0:
                    dtsta.append(0)
                else:
                    dtsta.append(1)
            print("{}".format(dtsta))
            try:
                time.sleep(dodo)
            except KeyboardInterrupt:
                return
    else:
        return False
    print("ddt end")

# ------------------------------------------------------
def ftc32(buss, sortie, rfdel):
# ------------------------------------------------------
    '''function to set tc_32 mode on output
       function used in ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    dd.ddsSetPuCkEn(sortie)  # set enable + type clock
    dd.ddsSetPuCkMode(sortie, 0x3)  # set clock mode
    dd.ddsClearPuCkPol(sortie) # positive pol
    dd.ddsSetPuCkSrce(sortie, 0) # 0=tc_32
    dd.ddsSetPulDel(sortie, rfdel)

# ------------------------------------------------------
def fddtin(buss, verbose):
# ------------------------------------------------------
    '''function to configure input#0 for test
       function used in ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    if verbose == "-v": print("Stamping register  before  : {}".format(dd.ddsGetStamp()))
    dd.ddsClearDdt()  #clear...
    if verbose == "-v": print("Stamping register  after   : {}".format(dd.ddsGetStamp()))    
    for valch in range(4):
        dd.ddsResetInFifo(valch)
    if verbose == "-v": print("Stamping register  after  reset  : {}".format(dd.ddsGetStamp()))    
    dd.ddsSetInEn(0)
    dd.ddsClearInPol(0)
    dd.ddsSetDdt()    #... then set : process cleared
    if verbose == "-v": print("Stamping register before return   : {}".format(dd.ddsGetStamp()))    
    return True

# ------------------------------------------------------
def fddtingate(buss, dur):
# ------------------------------------------------------
    '''function enable/disable input#0 for test
       function used in ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    time.sleep(dur/10)
    dd.ddsSetDdtGate()   # open gate
    time.sleep(dur)
    dd.ddsClearDdtGate() # close gate
    time.sleep(dur/10)
    return True

# ------------------------------------------------------
def fddtstat(buss):
# ------------------------------------------------------
    '''function read input#0 for test
       function used in ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    stato = dd.ddsGetDdtHisto()
    metas_0 = stato & 0xFF
    metas_1 = (stato & 0xFF00) >> 8
    metas_2 = (stato & 0xFF0000) >> 16
    metas_3 = (stato & 0xFF000000) >> 24
    return [metas_0, metas_1, metas_2, metas_3]

# ------------------------------------------------------
def nw_init(liste, buckliste, fdcsv):
# ------------------------------------------------------
    """Init network
       """
    m0 = liste[0]
    s1 = liste[1]
    s2 = liste[2]
    print("m0={}, s1={}, s2={}".format(m0, s1, s2))

    rrr = 0
    # Master
    print("\n   init master# {}".format(m0))
    enprog(m0, 0, 0, 0)       # disable buclk
    rrr += msconf(m0, "-m")   # configure
    enprog(m0, 1, 1, 1)       # enable buclk
    dwprog(m0, buckliste)
    ffdset(MASTER_0)
    # Slave 1
    print("\n   init slave#1 {}".format(s1))
    enprog(s1, 0, 0, 0)
    rrr += msconf(s1, "-s")
    enprog(s1, 1, 1, 1)
    dwprog(s1, buckliste)
    stup(s1, 2, "-nov")
    ffdset(SLAVE_1)
    # Slave 2 if there
    if s2 != 0:
        print("\n   init slave#2 {}".format(s2))
        enprog(s2, 0, 0, 0)
        rrr += msconf(s2, "-s")
        enprog(s2, 1, 1, 1)
        dwprog(s2, buckliste)
        stup(s2, 2, "-nov")
        ffdset(SLAVE_2)
        
    for dut in liste[3:]:
        print("\n   dut{}".format(dut))
        enprog(dut, 0, 0, 0)
        rrr += msconf(dut, "-s")
        enprog(dut, 1, 1, 1)
        dwprog(dut, buckliste)
        stup(dut, 2, "-nov")
        # restore ad9510 params if found in file
        with open(fdcsv, "r") as fd:
            lu = csv.DictReader(fd,delimiter=",")
            hitlu = False
            for row in lu:
                if (row['BUS'] == str(dut)):
                    hitlu = True
                    listuj = []
                    for titr in lu.fieldnames:
                        listuj.append(int(row[titr]))
        if hitlu == False:
            listuj = [dut] + [0]*7
        ffdset(listuj)
    # Now pipe bunch clock params over nw
    pipebunch(liste[0])
    # Now master start
    print("\n!!!!!!!!!!!!!!! MASTER START !!!!!!!!!!!!!!!!")
    mstart(m0)
    # Now enable PPS debug management, except Master : enable, disable adj_enable, stop_on_detect
    print("     --> Enable PPS debug for network RF consistency survey.")
    for dut in liste[1:]:
        fdbg(dut, 1, 0, 1, "-n")
    # end with enable T0
    print("     --> Enable T0")
    seqen(m0, "-w", 1, "-n")

    return rrr

# ------------------------------------------------------
def ck10_check(liste):
# ------------------------------------------------------
    """Check rear panel 10 MHz output : signal shape, compare with Master
       """
    # enable output
    for dev in liste:
        ioc(dev, "-ck10", 1, "-n")
    print("   All CLK_10 outputs enabled")
    choice = raw_input("   Check 10 MHz rear panel outputs. CR when done / Anything if failed >>  ")
    for ii in range(0, len(liste)):
        ioc(dev, "-ck10", 0, "-n")
    print("   All CLK_10 outputs disabled")
    if choice == "":
        return 0
    else:
        return 1

# ------------------------------------------------------
def rf_check(liste):
# ------------------------------------------------------
    """Check RF outputs : signal shape, lock status
       """
    for dev in liste[1:]:
        ioc(dev, "-clko", 1, "-n") # enable RF output
        rfn(dev, 0, 1)
        fpllrfs(dev)
    print("   All RFn outputs disabled")
    choice = raw_input("   Check RF outputs. CR when done / Anything if failed >>  ")
    if choice == "":
        return 0
    else:
        return 1

# ------------------------------------------------------
def rfn_check(liste):
# ------------------------------------------------------
    """Check RF/n outputs : signal shape, division factors
       """
    s1 = liste[1]
    s2 = liste[2]

    rfn(s1, 1, 14)
    rfn(s2, 1, 30)
    print("   Slave {} has been set to 2*RF/14".format(s1))
    print("   Slave {} has been set to 2*RF/30".format(s2))
    # enable all RFn on DUTs
    print("   DUTs set to 2*RF/4")
    for duv in liste[3:]:
        rfn(duv, 1, 4)
    choice = raw_input("CR when checked >>  ")
    print("   DUTs set to 2*RF/14")
    for duv in liste[3:]:
        rfn(duv, 1, 14)
    choice = raw_input("CR when checked >>  ")
    print("   DUTs set to 2*RF/28")
    for duv in liste[3:]:
        rfn(duv, 1, 28)
    choice = raw_input("CR when checked >>  ")
    print("   DUTs set to 2*RF/30")
    for duv in liste[3:]:
        rfn(duv, 1, 30)
    choice = raw_input("CR when checked >>  ")
    # disable all RFn
    for duv in liste[3:]:
        rfn(duv, 0, 1)

    return 0

# ------------------------------------------------------
def tst_sma_1(liste, opt, fdcsv):
# ------------------------------------------------------
    """Check OUTPUT#1. Metastability tuning
    signal programming
    phase shift test
       """
    s1 = liste[1]
    s2 = liste[2]

    cloque = 0
    if opt == "fph":
        srdiv(s2, 1, SRDIVPH, 0, 0)
        print("OUT#1 : Slave {} has been set to SRDIV({})".format(s2, SRDIVPH))
    elif opt == "tc32":
        ckprog(s2, 1, 1, 0, 0, 0, 0)
        print("OUT#1 : Slave {} has been set to CLK_TC32".format(s2))
    elif opt == "ck16b": 
        ckprog(s1, 1, 1, 4, 0, 0, 0)
        print("OUT#1 : Slave {} has been set to CLK_16b".format(s1))
        cloque = 4
    else:
        return 1

    for dut in liste[3:]:
        print("\n---> dut{}".format(dut))
        if opt != "fph":
            ckprog(dut, 1, 1, cloque, 0, 0, 0)
            choice = raw_input("     Metastability tuning : Connect out#1 to scope then [s]/n (scan fine phase / n=skip) >> ")
            if (choice == "s") or (choice == ""):
                rfdel(dut, 1, 0, 360, 1)
                dime = raw_input("      => Give selected fine phase value > ")
                pif = get_v(0, dime)
                if pif in range(360):
                    setfdel(dut, 1, pif)
                    csvupdate(fdcsv, "fd1", dut, pif)
        else:
            print("       --- Compare with slave{}-pin#1. SRDIV({}) test".format(s2, SRDIVPH))
            choix = raw_input("           ----------------> start phase shift y/[n] > ")
            if choix == "y": phck(dut, 1, "-srd", 1, SRDIVPH)

# ------------------------------------------------------
def tst_sma_23(liste, ch, opt, fdcsv):
# ------------------------------------------------------
    """Check OUTPUT#2,3. Metastability tuning
    signal programming
    phase shift test
       """
    s1 = liste[1]
    s2 = liste[2]

    if opt == "tc32":
        ckprog(s2, 1, 1, 0, 0, 0, 0)
        print("OUT#1 : Slave {} has been set to CLK_TC32".format(s2))
    elif opt == "fph":
        srdiv(s2, 1, SRDIVPH, 0, 0)
        print("OUT#1 : Slave {} has been set to SRDIV({})".format(s2, SRDIVPH))
    elif opt == "gun": 
        puprog(s1, 2, 1, 0, 1, 0, 0, 0)
        print("Slave {} OUTPUT#2 has been set to GUN_PULSE".format(s1))
    else:
        return 1

    for dut in liste[3:]:
        print("\n---> dut{}".format(dut))
        if opt != "fph":
            if opt == "tc32":
                ckprog(dut, ch, 1, 0, 0, 0, 0)
            elif opt == "gun": 
                puprog(dut, ch, 1, 0, 1, 0, 0, 0)
            choice = raw_input("     Connect out#{} to scope then type [s]/n (scan fine delay / n=skip) >> ".format(ch))
            if (choice == "s") or (choice == ""):
                fin = False
                while not fin:
                    print("     PI = 0")
                    pll23(dut, ch, "-pi", 0, "-nov")
                    pll23(dut, ch, "-sc", 0, "-nov")
                    choise = raw_input("     proceed with PI = 1 (CR) > ")
                    pll23(dut, ch, "-pi", 1, "-nov")
                    pll23(dut, ch, "-sc", 0, "-nov")
                    choise = raw_input("     repeat scan ? y/[n] > ")
                    if choise == "y":
                        fin = False
                    else:
                        dime = raw_input("     => Give selected pi and phase values > ")
                        pi = get_v(0, dime)
                        if (pi != 0) and (pi != 1): pi = 0
                        ph = get_v(1, dime)
                        if ph not in range(25): ph = 0
                        pll23(dut, ch, ph, pi, "-nov")
                        if ch == 2:
                            csvupdate(fdcsv, "pi2", dut, pi)
                            csvupdate(fdcsv, "md2", dut, ph)
                        if ch == 3:
                            csvupdate(fdcsv, "pi3", dut, pi)
                            csvupdate(fdcsv, "md3", dut, ph)
                        fin = True
                print("\n---> Now test output fine delay : try a few values")
                finu = False
                while not finu:
                    findel = raw_input("     fine delay value / CR=skip >> ")
                    if findel != "":
                        dval = get_v(0, findel)
                        setfdel(dut, ch, dval)
                    else: finu = True
                setfdel(dut, ch, 0)
        else:
            print("       --- Compare with slave{}-pin#{}. SRDIV({}) test".format(s2, ch, SRDIVPH))
            choix = raw_input("           ----------------> start phase shift y/[n] > ")
            if choix == "y": phck(dut, ch, "-srd", 1, SRDIVPH)
            
# ------------------------------------------------------
def tst_sma_lemo(liste, ch):
# ------------------------------------------------------
    """Check OUTPUTs#4-12.
       """
    s1 = liste[1]
    s2 = liste[2]

    # configure s1 to reference clocks
    ckprog(s1, 4, 1, 0, 0, 0, 0)   # tc_32
    ckprog(s1, 5, 1, 1, 0, SR50, 0)   # ck_sr
    ckprog(s1, 6, 1, 2, 0, BOO50, 0)   # ck_boo
    # s1, ch#7 : SRDIV with division factor SRDIVAL
    srdiv(s1, 7, SRDIVAL, 0, 0)
    print("   Slave {} clk#4 : tc_32, clk#5 : ck_sr, clk#6 : ck_boo, clk#7 : srdiv({})".format(s1, SRDIVAL))
    # configure s2 to reference pulses. mode=2, delay=1, width=1
    puprog(s2, 4, 1, 0, 2, 0, 1, 1)   # t0
    puprog(s2, 5, 1, 0, 2, 1, 1, 1)   # inj
    puprog(s2, 7, 1, 0, 2, 2, 1, 1)   # ext
    puprog(s2, 8, 1, 0, 2, 3, 1, 1)   # ki
    puprog(s2, 2, 1, 0, 1, 0, 0, 0)   # gun
    print("   Slave {} OUT#2 : GUN, pulse#4 : t0, pulse#5 : inj, pulse#7 : ext, pulse#8 : ki".format(s2))

    for dut in liste[3:]:
        print("\n---> dut{}".format(dut))
        fin = False
        while not fin:
            dime = raw_input("   Choose ch#{} clock : [tc32]/sr/boo/srdiv / n=skip >> ".format(ch))
            choix = dime.lower()
            if (choix == "tc32") or (choix == ""):
                phck(dut, ch, "-tc32", 1, 1)
            elif choix == "sr":
                phck(dut, ch, "-sr", 1, 1)
            elif choix == "boo":
                phck(dut, ch, "-bo", 1, 1)
            elif choix == "srdiv":
                phck(dut, ch, "-srd", 1, SRDIVAL)
            else:
                fin = True
            if fin == False:
                dime = raw_input("   Other clock ? : y/[n] >> ")
                choix = dime.lower()
                if choix != "y":
                    fin = True
        fin = False
        while not fin:
            dime = raw_input("   Choose ch#{} pulse : [inj]/t0/ext/ki / n=skip >> ".format(ch))
            choix = dime.lower()
            if choix == "t0":
                dw = getput_dw(dut, ch, 0, 0)
            elif (choix == "inj") or (choix == ""):
                dw = getput_dw(dut, ch, 0, 1)
            elif choix == "ext":
                dw = getput_dw(dut, ch, 0, 2)
            elif choix == "ki":
                dw = getput_dw(dut, ch, 0, 3)
            else:
                fin = True
            if fin == False:
                dime = raw_input("   Other pulse ? : y/[n] >> ")
                choix = dime.lower()
                if choix != "y":
                    fin = True
                
# ------------------------------------------------------
def rdstamp(buss, dex):
# ------------------------------------------------------
    """time stamp read:
       input index
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    if dex not in range(4):
        print("Bad input index")
        return False

    dd = SpecWhist(ipp)
    dd.ddsReadInFifo(dex)
    fiflu = dd.ddsGetTrigStamp(dex)
    return fiflu
        
# ------------------------------------------------------
def tsts(buss, dex, gat, verbose):
# ------------------------------------------------------
    """time stamping test on inputs#1-4:
       input index
       test signal set to CLK_SR/SRDIVSTAMPING
       time gate length set to 1/10s
       """
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    if dex not in range(4):
        print("Bad input index")
        return False

    if gat <= 0 :
        print("No time gate.")
        return False

    dd = SpecWhist(ipp)

    #close in case, reset fifo, check, open gate and close
    # print("Stamp_reg before clear = {}".format(dd.ddsGetStamp()))
    dd.ddsClearInEn(dex)
    dd.ddsResetInFifo(dex)
    vide = dd.ddsTestStampEpty(dex)[0]
    # print("Fifo empty bit = {}".format(vide))
    if vide == 0:
        print("Stamp_reg after clear = {}".format(dd.ddsGetStamp()))
        return False
    dd.ddsSetInEn(dex)
    # Next fifo reset to clear potential spurious stamp
    # if enable rise with input active
    dd.ddsResetInFifo(dex)
    # wait long enough to get some data
    time.sleep(gat)
    dd.ddsClearInEn(dex)
    #read if any
    yakek = dd.ddsTestStampEpty(dex)[0]
    if yakek == 1:
        print("No stamp in gate.")
    else:
        ii = 0
        lui = []
        while (yakek != 1):
#        while (yakek != 1) and (ii < 10):
            #put next fifo value into read register (monostbale strobe)
            dd.ddsReadInFifo(dex)
            lui.append(dd.ddsGetTrigStamp(dex))
            yakek = dd.ddsTestStampEpty(dex)[0]
            ii += 1
        if ii > 1:
            print("     In(0) = {}.".format(lui[0]))
            for pii in range(1, ii):
                odelta = lui[pii]-lui[pii-1]
                ofreq = (float(ESRFRF) / 8) / float(odelta)
                print("        In({}) - IN({}) = {}  -> freq = {:3.2f}.".format(pii, (pii-1), odelta, ofreq))
            if verbose == "-v": print("     Expected DELTA   = {}.".format(124*SRDIVSTAMPING))
                
# ------------------------------------------------------
def tst_stampin(liste, ch, verbose):
# ------------------------------------------------------
    """Test wrapper with menu
       calls tsts
       """

    s1 = liste[1]
    s2 = liste[2]

    # configure s1 as reference clock to be sampled
    # ch#8 : SRDIV with division factor SRDIVSTAMPING
    srdiv(s1, 8, SRDIVSTAMPING, 0, 0)
    print("   Slave {} clk#8 : srdiv({})".format(s1, SRDIVSTAMPING))

    for dut in liste[3:]:
        print("\n---> dut#{}, stamping TRIG_{}".format(dut, ch))
        print("       Input threshold TRIG_{} set at {}".format(ch, INSEUILBA))
        dacLoader(dut, 1, (7-2*ch), INSEUILBA, "-n")
        fin = False
        while not fin:
            pret = raw_input("     ---> Stamp=[s] / skip=n >> ")
            if pret != "n":
                tsts(dut, ch, 0.1, verbose)
            else:
                fin = True

# ------------------------------------------------------
def tst_hotplug(liste):
# ------------------------------------------------------
    """Hot plug test
       menu : unplug/replug fibre
       launch SLOK
       checkout
       """

    s1 = liste[1]
    s2 = liste[2]

    # configure s1 & s2 with some reference signals
    # s2, ch#4 : clk_sr
    ckprog(s2, 5, 1, 1, 0, SR50, 0)   # ck_sr
    # s2, ch#1 : clk_16b
    ckprog(s2, 1, 1, 4, 0, 0, 0)
    # s2, ch#8 : SRDIV with division factor 11
    srdiv(s2, 8, SRDIVAL, 0, 0)
    print("   Slave {} clk#1 : clk_16b, clk#5 : clk_sr, clk#8 : srdiv({})".format(s2, SRDIVAL))
    # configure s1 to reference pulses. mode=2, delay=1, width=1
    puprog(s1, 4, 1, 0, 2, 0, 1, 1)   # t0
    puprog(s1, 5, 1, 0, 2, 1, 1, 1)   # inj
    puprog(s1, 7, 1, 0, 2, 2, 1, 1)   # ext
    puprog(s1, 8, 1, 0, 2, 3, 1, 1)   # ki
    puprog(s1, 2, 1, 0, 1, 0, 0, 0)   # gun
    print("   Slave {} OUT#2 : GUN - PULSES : #4 : t0, #5 : inj, #7 : ext, #8 : ki".format(s1))
    for dut in liste[3:]:
        pret = raw_input("\n*****---> dut#{} hot plug test [CR, y] or skip (n) :".format(dut))
        if pret != "n":
            ckprog(dut, 1, 1, 4, 0, 0, 0)      # clk_16b
            ckprog(dut, 4, 1, 1, 0, SR50, 0)   # clk_sr
            puprog(dut, 5, 1, 0, 2, 0, 1, 1)   # t0
            puprog(dut, 6, 1, 0, 2, 1, 1, 1)   # inj
            puprog(dut, 7, 1, 0, 2, 2, 1, 1)   # ext
            puprog(dut, 8, 1, 0, 2, 3, 1, 1)   # ki
            puprog(dut, 2, 1, 0, 1, 0, 0, 0)   # gun
            print("\n    ---> Connect some signals :\n    CLOCKS :  #1 : clk_16b, #2 : gun, #4 : clk_sr - PULSES : #5 : t0, #6 : inj, #7 : ext, #8 : ki")
            fin = False
            while not fin:
                dime = raw_input("    ---> Unplug/replug WR link fibre, observe synchro loss. CR when done >> ")
                wrlostclear(dut)
                dime = raw_input("    ---> CR to start resynchro. >> ")
                slok(dut)
                dime = raw_input("    ---> Observe resynchro (may take up to 3mn). r = repeat test / CR to next >> ")
                if dime != "r":
                    fin = True
        
# ------------------------------------------------------
def tst_evt(liste, tr):
# ------------------------------------------------------
    """Event test
       transmit/receive parameter
       transmit/receive targets : s1/s2
       scope checkout
       """

    # RAZ ALL events in bench
    evt_raz(liste, "-noverbose")

    s1 = liste[1]
    s2 = liste[2]
    # configure s1 & s2 with some reference signals
    # event test source = s1-output#4 : 355000/SRDIVEVTA Hz
    srdiv(s1, 7, SRDIVEVTA, 0, 0)
    print("\n  Event source at Slave#{}-Out#{}, frequency={:2.2f} Hz.".format(s1, 7, float(355000)/float(SRDIVEVTA)))
    # s1 as transmiter of event#EVTA on input#1
    print("    Slave#{} : connect OUT#7 to TRIG_2. Transmit EVENT#{}".format(s1, EVTA))
    setin(s1, 2, 1, 0, INSEUILHO)
    evt(s1, "-t", 2, 1, EVTA, "-n", [s1, 4, int(355000/SRDIVEVTA)], 0)
    # s1 as receiver of event#EVTB on output#9
    print("    Slave#{} : receive EVENT#{} on OUT#9.".format(s1, EVTB))
    puprog(s1, 9, 1, 0, 2, 8, 1, 2)
    evt(s1, "-r", 0, 1, EVTB, "-n", SRCE_NO, 0)

    # event test source = s2-output#6 : 355000/SRDIVEVTB Hz
    srdiv(s2, 6, SRDIVEVTB, 0, 0)
    print("  Event source at Slave#{}-Out#{}, frequency={:2.2f} Hz.".format(s2, 6, float(355000)/float(SRDIVEVTB)))
    # s2 as transmiter of event#EVTB on input#1
    print("    Slave#{} : connect OUT#6 to TRIG_2. Transmit EVENT#{}".format(s2, EVTB))
    setin(s2, 2, 1, 0, INSEUILHO)
    evt(s2, "-t", 2, 1, EVTB, "-n", [s2, 6, int(355000/SRDIVEVTA)], 0)
    # s2 as receiver of event#EVTA on output#9
    print("    Slave#{} : receive EVENT#{} on OUT#9.".format(s2, EVTA))
    puprog(s2, 9, 1, 0, 2, 8, 1, 2)
    evt(s2, "-r", 0, 1, EVTA, "-n", SRCE_NO, 0)

    rrr = 0
    
    if tr == "r":
        for ii in range(3, len(liste)):
            dut = liste[ii]
            uti = 9 + (ii%4)
            for raz in range(4):
                puprog(dut, 9+raz, 0, 0, 2, 0, 1, 2)                
            puprog(dut, uti, 1, 0, 2, 8, 1, 2)
            if ii%2 ==0:
                print("\n    ---> dut{} receiving event#{} on OUTPUT#{}:".format(dut, EVTB, uti))
                evt(dut, "-r", uti-9, 1, EVTB, "-n", SRCE_NO, 0)
            else:
                print("\n    ---> dut{} receiving event#{} on OUTPUT#{}:".format(dut, EVTA, uti))
                evt(dut, "-r", uti-9, 1, EVTA, "-n", SRCE_NO, 0)
        raw_input("    >> Check behavior, CR to exit and remove all events >> ")
    elif tr == "t":
        for ii in range(3, len(liste)):
            dut = liste[ii]
            # event test source = dut-output#4 : 355000/SRDIVDUT Hz
            srdiv(dut, 4, SRDIVDUT[ii-3], 0, 0)
            print("\n  Event source at DUT{}-Out#{}, frequency={:2.2f} Hz.".format(dut, 4, float(355000)/float(SRDIVDUT[ii-3])))
            # dut as transmiter of event#EVTDUT on input#3
            print("    DUT{} : connect OUT#4 to TRIG_3. Transmit EVENT#{}".format(dut, EVTDUT[ii-3]))
            setin(dut, 3, 1, 0, INSEUILHO)
            evt(dut, "-t", 3, 1, EVTDUT[ii-3], "-n", [dut, 4, int(355000/SRDIVDUT[ii-3])], 0)
            # program receivers
            print("    Slave#{} : receive EVENT#{} on OUT#{}.".format(liste[ii-1], EVTDUT[ii-3], 9+(ii%3)))
            puprog(liste[ii-1],  9+(ii%3), 1, 0, 2, 8, 1, 2)
            evt(liste[ii-1], "-r",  (ii%3), 1, EVTDUT[ii-3], "-n", SRCE_NO, 0)
        raw_input("    >> Check behavior, CR to exit and remove all events >> ")
    else:
        print("BAD OPTION {}".format(tr))
        rrr += 1

    # RAZ ALL events in bench
    evt_raz(liste, "-noverbose")
    print("Removed all EVENTs from Bench")
    return rrr

# ------------------------------------------------------
def tst_ddt(liste, ch):
# ------------------------------------------------------
    """Delta_t drift test on AD9510 outputs#2-3
       output channel option (default 2)
       scope checkout for tuning
       then stats are printed on stdout
       """

    s1 = liste[1]
    s2 = liste[2]
    print("Available bench slaves for testing : {}, {}".format(s1, s2))
    print("DUTs : ", end=' ')
    sys.stdout.flush()
    for ii in range(3, len(liste)):
        dut = liste[ii]
        if ii == (len(liste)-1):
            print("{}.".format(dut), end=' ')
        else:
            print("{}, ".format(dut), end=' ')
        sys.stdout.flush()

    dime = raw_input("    ---> Choose module(s) to be tested : All bench / All DUTs / Single a/d/[s] >> ")

    if (dime != "a") and (dime != "d"):
        dime = "s"
    elif dime == "a":
        listart = 1
    elif dime == "d":
        listart = 3

    if dime == "s":
        fin = False
        while not fin:
            medi = raw_input("    ---> Choose module to be tested >> ")
            uti = get_v(0, medi)
            print("    -> testing {}. Start with PI metastability domain.".format(uti))
            medi = raw_input("Now plug output#{} to TRIG_0 input. CR when done > ".format(ch))
            kekidi = raw_input("Run metastability detection (CR) or skip and give known test result (s) : [CR]/s > ")
            if kekidi != "s":
                ddt(uti, 2, "-ls", 0, 0, 1)
            medi = raw_input("Give PI (0/1) and RF phase (0-7) resulting from ddt search > ")
            pii = get_v(0, medi)
            if pii != 0: pii = 1
            pll23(uti, ch, "-pi", pii, "-nov")
            rfd = get_v(1, medi)
            medi = raw_input("Now loop on Delta_t scan and look for drift. Enter display refresh in seconds [300] > ")
            dodo = get_v(0, medi)
            if dodo not in range(301, 10000): dodo = 300
            ddt(uti, 2, "-l", rfd, 0, dodo)
    else:
        print("*************************************")
        print("Plug output#{} to TRIG_0 on devices : ".format(2), end=' ')
        sys.stdout.flush()
        for ii in range(listart, len(liste)):
            dut = liste[ii]
            if ii == (len(liste)-1):
                print("{}.".format(dut), end=' ')
            else:
                print("{}, ".format(dut), end=' ')
                sys.stdout.flush()
        medi = raw_input("CR when cabling done > ")
        for jj in range(listart, len(liste)):
            uti = liste[jj]
            kekidi = raw_input("    -> Device {}. Run metastability detection (CR) or skip (s) : [CR]/s > ".format(uti))
            if kekidi != "s": ddt(uti, 2, "-ls", 0, 0, 1)
            medi = raw_input("Give PI (0/1) and RF phase (0-7) resulting from preceding test > ")
            pii = get_v(0, medi)
            if pii != 0: pii = 1, "-noverbose"
            pll23(uti, ch, "-pi", pii, "-nov")
            rfd = get_v(1, medi)
            ftc32(uti, ch-1, rfd)
        kekidi = raw_input("    -> Launch Delta_t test on selectedd devices : [CR] > ")
        tosta0 = []
        tosta1 = []
        for jj in range(listart, len(liste)):
            tosta0.append(23*[0])
            tosta1.append(23*[0])
        feuvert = True
        kuku = 0
        while feuvert:
            print("-------> at {}".format(pendul.datetime.now()))
            for pp in range(listart, len(liste)):
                tosta1[pp-listart] = []
                buse = liste[pp]
                for sck in range(23):
                    pfad = (sck << 1) & 0x7f
                    fad95(buse, D3S_CMD_9510, 1, (0x3a + (2-ch)*4), pfad)
                    # gate 1s
                    fddtingate(buse, 0.1)
                    stats = fddtstat(buse)
                    if (stats[0]+stats[1]+stats[3]) == 0x0:
                        tosta1[pp-listart].append(0)
                    else:
                        tosta1[pp-listart].append(1)
                if tosta1[pp-listart] != tosta0[pp-listart]:
                    print("    Device {} : {}".format(buse, tosta1[pp-listart]))
                    sys.stdout.flush()
            tosta0 = tosta1
            kuku += 1
            try:
                if kuku < 10:
                    time.sleep(100)
                else:
                    time.sleep(10000)
            except KeyboardInterrupt:
                return
            
        

                
