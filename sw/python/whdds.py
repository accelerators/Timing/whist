#!/bin/env python
# -*- coding: utf-8 -*-
#title           :menu.py
#description     :This program displays an interactive menu on CLI
#author          :GG from menu_exple.py
#date            :
#version         :0.1sstup
#usage           :python menu.py
#notes           :
#python_version  :3+  
#=======================================================================
# Import the modules needed to run the script.
import sys
import readline
import os

from whtst import *
 
# Main definition - constants
menu_actions  = {}  
 
# =======================
#     MENUS FUNCTIONS
# =======================
 
# Main menu
def main_menu():
#    os.system('clear')
    
    choice = raw_input("v3>>  ")
    exec_menu(choice)
 
    return
 
# Execute menu
def exec_menu(choice):
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print("Invalid selection, please try again.\n")
            menu_actions['main_menu']()
    return

# load bitfile
def menu_lo():
    print("load bit file : give board_bus_address file_version")
    choice = raw_input(" >> load >  bus version >>  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            loadbf(ch)
#            print("file loaded return to menu")
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return
 
def menu_stev():
    print("EVT Tx stamp : read Tx stamp : give board_bus_address index")
    choice = raw_input(" >> EVT Tx stamp > bus index :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            stev(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# clear flash bit, set Si571 frequency
def menu_stup():
    print("Start Up : clear flashed bit and set Si571 frequency : give board_bus_address -x2_option")
    choice = raw_input(" >> Start Up > bus (option) :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            stup(ch)
#            print("bit cleared return to menu")
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# dis/enable t0 watch dog (option set trig level) : bus 0/1 (value_0-15)
def menu_t0wd():
    print("T0 Watch Dog menu : board_bus_address dis/enable (trig level)")
    choice = raw_input(" >> t0wd > bus 0/1 (value_0-15): ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            t0wd(ch)
#            print("bit cleared return to menu")
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# board status
def menu_st():
    print("board status : give board_bus_address")
    choice = raw_input(" >> st > bus :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            statspec(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# board debug status
def menu_dbg():
    print("board status : give board_bus_address RFoE_unlck_clr_0/1 enable_dbg_0/1 enable_adj_0/1 stop_on_adj_0/1")
    choice = raw_input(" >> st > bus rfoe_unlck_clr dbg_en adj_en stop:  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            dbg_stat(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# outputs status
def menu_os():
    print("outputs status : give board_bus_address")
    choice = raw_input(" >> os > bus :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            statout(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# inputs status
def menu_is():
    print("outputs status : give board_bus_address")
    choice = raw_input(" >> 1s > bus :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            statin(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set mode parameters
def menu_sb():
    print("modes : Bus AutoBlist MultiBunch")
    choice = raw_input(" >> sb > Bus AutoBlist MultiBunch :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            sbprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# global enables management
def menu_en():
    print("modes : Bus Bunch_en Rf_en Gun_inj_en")
    choice = raw_input(" >> sb > Bus buclk_en rf_en gun_inj_en:  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            enprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# bunch list fill
def menu_bl():
    os.system('clear')
    print("bunch list fill with ptr_value : give , start_ptr, stop_ptr, start_value")
    choice = raw_input(" >> bl > bus start_ptr stop_ptr start_value :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            bl_fill(ch)
        except KeyboardInterrupt:
            return
    return

# bunch list fill + test
def menu_blrd():
    print("bunch list read : give board_bus_address, start_ptr, stop_ptr")
    choice = raw_input(" >> bl > bus start_ptr stop_ptr :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            blread(ch)
        except KeyboardInterrupt:
            return
    return

# set multi-bunch delays
def menu_mb():
    print("parameters : bus del_1 to 4")
    choice = raw_input(" >> mb > bus up_to_10_bunch# :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            mbprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# read SLOK histogram
def menu_histo():
    print("parameters : bus")
    choice = raw_input(" >> rd_histo > bus :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            rd_histo(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set injection / extraction parameters
def menu_dw():
    print("parameters : bus del_inj del_ext wid_inj wid_ext")
    choice = raw_input(" >> dw > bus del_t0 del_inj del_ext wid_inj wid_ext:  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            dwprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set T0 Mux
def menu_t0mux():
    print("select which trig_in is T0 : 0 to 3")
    choice = raw_input(" >> io > bus 0-3 :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            mux_t0(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# trog CSR Monostabel bit
def menu_tr():
    print("Trig monostable INJERR_RST, PPSack, Rst352, BunchListIncrement / reset, RFSync, MultiBLReset, RFLOST_RST, Skip44, Jump44")
    choice = raw_input(" >> tr > bus -ErrRst | -ppsa | -rrf | -bli | -blr | -rfs | -rflrst | -sk | -ju :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            trigger(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# test benches choice
def menu_tb():
    print("Select test bench on channel#")
    choice = raw_input(" >> tb > bus -b1 -ba -bx -st: ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            tbench(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# pulse menu
def menu_pu():
    print("Pulse output : channel enable polarity mode source delay width")
    choice = raw_input(" >> pu > bus chan en pol mode srce del wid :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            puprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# 352 phase adjust menu
def menu_ph():
    print("352 MHz Phase adjust : FPGA_PLL/OUT_CK value")
    choice = raw_input(" >> ph > bus -fpga/-outck val-0-7 :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            phprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# clock menu
def menu_ck():
    print("Clock output : channel enable srce polarity phase")
    choice = raw_input(" >> ck > bus chan en srce pol phase :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            ckprog(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# load bunch list menu
def menu_ldbx():
    print("load bunch list from file : channel file_name")
    choice = raw_input(" >> ldbx > bus -file_name :  ")
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            ldbx(ch)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# command
def menu_cmd():
    print("execute a SpecWhist class command : bus -command")
    choice = raw_input(" >> cmd > bus -cmd :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            libcmd(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# configure master or slave
def menu_ms():
    print("configure master or slave : bus -m -s")
    choice = raw_input(" >> ms > bus -m -s :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            msconf(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# load lm32 program
def menu_lm32():
    print("load lm32 program : bus index -file_path")
    choice = raw_input(" >> lm32 > bus index -file_path :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            lm32Loader(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set ioconf bitfield value
def menu_ioconf():
    print("i2c configuration some IO control bits : bus (-ttlo -clko -fplo -ck10 -vcxo) value")
    choice = raw_input(" >> ioconf > bus -bitfield value :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            ioc(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set fine delay value on channels 1-3
def menu_fdel():
    print("i2c configuration of fine delays : bus channel(1-3) value")
    choice = raw_input(" >> fine_delay > bus channel(1-3) value :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            fdel(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# ramp fine delay channels 1-3
def menu_rfdel():
    print("i2c configuration of fine delays : bus channel(1-3) val_start val_stop")
    choice = raw_input(" >> fine_delay > bus channel(1-3) val_start val_stop :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            rfdel(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# shift fine delay value on channels 1-3
def menu_sdel():
    print("fine delay shift : bus channel(1-3) value_start value_stop pas")
    choice = raw_input(" >> fine_delay > bus channel(1-3) value_a value_b pas:  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            sdel(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# slave lock rf_counter to master
def menu_slok():
    print("rf_counter slave lock to master : bus")
    choice = raw_input(" >> slave lock RF > bus :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            slok(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# shifting delay on clocks
def menu_phck():
    print("set shifting delay loop on clocks : bus channel clock_type time_step division_if-srd")
    choice = raw_input(" >> set clock_type time_step > bus channel -16/-4/-sr/-bo/-srd time_step (div) : ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            phck(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# enable master T0 input
def menu_seqen():
    print("enable T0 to master : bus 0/1")
    choice = raw_input(" >> enable T0 > bus enable-0/1: ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            seqen(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# pipeline bunch clock parameters (MASTER ONLY)
def menu_bcl():
    print("Trig pipeline bcl parameters (master command) : MASTER_bus ")
    choice = raw_input(" >> bcl > MASTER_bus  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            bcl(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# on demand pps monitoring
def menu_ppsmon():
    print("pps monitoring : bus")
    choice = raw_input(" >> pps monitoring > bus :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            ppsmon(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# read i2c config registers status
def menu_ioread():
    print("i2c configuration registers status")
    choice = raw_input(" >> ioread > bus : ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            iord(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# Set Si571 parameters
def menu_siset():
    print("Set Si571 registers and set options -f(requency)/-hs(HS_DIV)/-n1(N1)/-rf(RFREQ)/-u(nlock)/-r(ecall) value")
    choice = raw_input(" >> Si571_set > bus -f/-hs/-n1/-rf/-u/-r value: ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            si571_set(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# display Si571 config
def menu_sidis():
    print("Display Si571")
    choice = raw_input(" >> Si571_dis > bus -f/-r : ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            si571_dis(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# purge bcc HMQOUT
def menu_purbcc ():
    print("DEBUG : purge bcc HMQOUT")
    choice = raw_input(" >> purbcc > bus : ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            purbcc(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# set DAC value
def menu_dac ():
    print("set dac for trigger inputs reference voltages : bus r/w channel value")
    choice = raw_input(" >> dac > bus -rw channel value :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            dac(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# ad9516/10 rw registers
def menu_ad95():
    print("ad9516 action : bus 0(WR)/1(RF) -r/w add val")
    choice = raw_input(" >> ad95 > bus 0(WR:9516)/1(RF:9510) -r/w add val :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            ad95(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# list ad9516-9510 registers
def menu_lad95():
    print("list ad9516 registers : bus 0(WR)/1(RF)")
    choice = raw_input(" >> lad95 > bus 0(WR)/1(RF) :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            lad95(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# manage RF/n output from AD9510
def menu_rfn():
    print("RF devided by n : bus 0(dis)/1(enable) n-value")
    choice = raw_input(" >> rfn > bus 0/1 n :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            rfn(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# master start
def menu_mstart():
    print("master start : bus")
    choice = raw_input(" >> mstart > bus :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            mstart(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

# input conf
def menu_setin():
    print("set polarity of inputs : bus ch en pol threshold")
    choice = raw_input(" >> stamp > bus ch 1/0 0=pos/1=neg level(<= 0x80):  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            setin(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_rotinj():
    print("test clock phase programming : bus enable auto_increment start-bunch-nb")
    choice = raw_input(" >> rotinj > bus en auto start_b_nb:  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            rotinj(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_pll23():
    print("set or test PLL fine delay phase adjust on channels 2-3")
    choice = raw_input(" >> pll23 > bus channel -sc(an)/-pi(coarse-phase) value 0/1_if(-pi) :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            pll23(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_otb():
    print("outputs test bench configuration -pulse/clock_mode (polarity)")
    choice = raw_input(" >> outputs test bench > bus -pu/ck (0/1):  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            otb(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_srdiv():
    print("CLK_SR_DIV menu : bus channel division_factor coarse_phase(<div) SR_phase(0-992)")
    choice = raw_input(" >> SR_count > bus ch div phsr ph :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            srdiv(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_ssrdiv():
    print("Synchronize SR_COUNT clocks on slaves a&b : bus_a bus_b")
    choice = raw_input(" >> SYNC_SR_count > bus_a bus_b :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            ssrdiv(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_ddt():
    print("Test delta_t stability on outputs#2-3, RF_fine_delay scan/loop/run Delta_t_value_if_run")
    choice = raw_input(" >> Delta_t test > bus output(2/3) -s(scan)/-l(loop)/-r(run)/-ls(full_scan) RF_fine_del(0-7) Delta_t_value(0-22) : ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            ddt(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_dld():
    print("ad9510 Digital Lock Detect management : bus enable_9510 enable/reset_latch")
    choice = raw_input(" >> AD9510_DLD > bus dis/enable_9510(1/0) -l/-r(enable_latch/reset_latch) :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            dld(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_tsts():
    print("time stamping test : bus input0-3 gate_length(seconds)")
    choice = raw_input(" >> tsts > bus 0-3 s :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            tsts(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return

def menu_csv():
    print("save fine delays in fd78csv file")
    choice = raw_input(" >> save fine delays to fd78csv > bus -field value :  ")
    if choice == '':
        menu_actions['main_menu']()
    else:
        try:
            csvsave(choice)
        except KeyError:
            print("Invalid command.\n")
            menu_actions['main_menu']()
    return
        
# display menu on help
def disp_menu():
    print("hlp or help : this")
    print("!!!")
    print("1st PARAMETER IS BOARD LOCATION ON PCIe bus IN [1,5] : ALWAYS MANDATORY")
    print("remaining parameters are optional, separated by SPACE, 0=unchanged or default")
    print("parameters are integers, decimal or hexa")
    print("!!!")
    print("bl : fill bunch list. args : bus start_add stop_add start_val")
    print("ck : clock output programming. args : bus source")
    print("dw : del&wid parameters. args : bus del_inj del_ext wid_inj wid_ext")
    print("en : global enables management. args : bus buclk_en rf_en gun_inj_en")
    print("is : input status. args : bus")
    print("lo : load bitfile. args : bus bitfile_version")
    print("mb : multi bunch. args : bus up to 10 bunches")
    print("ms : configure master or slave : bus -m -s")
    print("os : pulse/clock output status. args : bus")
    print("ph : 352 MHz counter phase adjust. args : bus phase reset_value")
    print("pu : pulse output programming. args : bus chan en srce del wid")
    print("sb : some bits. args : bus autoblist multibunch")
    print("st : board status. args : bus")
    print("tr : trig monostable CR bit. args : bus bit_name")
    print("bcl    : pipeline bunch clock parameters (MASTER ONLY): MASTER_bus")
    print("cmd    : execute a SPecWhist class command : bus -command")
    print("dac    : rw dac value : bus -rw channel value")
    print("dbg    : debug status : bus")
    print("ddt    : test delta_t stability on output#2 or #3 : bus output-2/3")
    print("dld    : ad9510 Digital Lock Detect management : bus enable_9510 enable_latch")
    print("otb    : outputs test bench : bus -pu/ck polarity")
    print("rfn    : manage RF/n output : bus 0/1-enable n-value")
    print("ad95   : ad9516 action : bus 1(WR)/2(RF) -r/w add value")
    print("blrd   : read bunch list. args : bus start_add stop_add")
    print("fdel   : fine delay tuning : bus channel(1-3) value")
    print("ldbx   : load buch list from file : bus -filename")
    print("lm32   : load LM32 program : bus index -file_path")
    print("phck   : RF fine delay scan : bus value")
    print("sdel   : fine delay shift : bus channel(1-3) value_a value_b")
    print("slok   : slave lock rf_counter to master : bus")
    print("stev   : EVT Tx stamp : bus index")
    print("stup   : clear flash bit, set frequency : bus")
    print("tsts   : time stamp test : bus input0-3 gate_length")
    print("t0wd   : dis/enable t0 watch dog (option set trig level) : bus 0/1 (value_0-15)")
    print("bench  : looping test")
    print("histo  : read SLOK histo from : bus ")
    print("lad95  : list ad9516 regs : bus 1(WR)/2(RF)")
    print("pll23  : set or test PLL fine delay phase adjust on channels 2-3")
    print("rfdel  : ramp fine delay tuning : bus channel(1-3) val_start val_stop")
    print("seqen  : T0 enable on master : bus")
    print("setin  : input config menu")
    print("sidis  : Display Si571 config : bus")
    print("siset  : Set Si571 parameters : bus Param Value")
    print("srdiv  : CLK_SR_DIV menu : bus channel division_factor coarse_phase SR_phase")
    print("t0mux  : master : select which input is T0 : bus input1-3")
    print("ioconf : set ioconf value : bus bitfield value")
    print("ioread : read i2c status : bus")
    print("mstart : master start : bus")
    print("ppsmon : PPS monitoring  : bus")
    print("purbcc : DEBUG !! purge bcc HMQOUT  : bus")
    print("rotinj : make basic list for injection rotation trick")
    print("ssrdiv : synchronize CLK_SR_DIV clocks between 2 slaves")
    print("csvsave : save fine delays to file : bus -field value")
    print("q  : quit")
    return
 
# Exit program
def exit():
    print("ByeBunchBench !")
    sys.exit()
 
# =======================
#    MENUS DEFINITIONS
# =======================
 
# Menu definition
menu_actions = {
    'main_menu': main_menu,
    'hlp': disp_menu,
    'help': disp_menu,
    'bl': menu_bl,
    'ck': menu_ck,
    'dw': menu_dw,
    'en': menu_en,
    'is': menu_is,
    'lo': menu_lo,
    'mb': menu_mb,
    'ms': menu_ms,
    'os': menu_os,
    'ph': menu_ph,
    'pu': menu_pu,
    'sb': menu_sb,
    'st': menu_st,
    'tr': menu_tr,
    'bcl': menu_bcl,
    'cmd': menu_cmd,
    'dac': menu_dac,
    'dbg': menu_dbg,
    'ddt': menu_ddt,
    'dld': menu_dld,
    'otb': menu_otb,
    'rfn': menu_rfn,
    'ad95': menu_ad95,
    'blrd': menu_blrd,
    'fdel': menu_fdel,
    'ldbx': menu_ldbx,
    'lm32': menu_lm32,
    'phck': menu_phck,
    'sdel': menu_sdel,
    'slok': menu_slok,
    'stev': menu_stev,
    'stup': menu_stup,
    'tsts': menu_tsts,
    't0wd': menu_t0wd,
    'bench': menu_tb,
    'histo': menu_histo,
    'lad95': menu_lad95,
    'pll23': menu_pll23,
    'rfdel': menu_rfdel,
    'seqen': menu_seqen,
    'setin': menu_setin,
    'sidis': menu_sidis,
    'siset': menu_siset,
    'srdiv': menu_srdiv,
    't0mux': menu_t0mux,
    'ioconf': menu_ioconf,
    'ioread': menu_ioread,
    'mstart': menu_mstart,
    'ppsmon': menu_ppsmon,
    'purbcc': menu_purbcc,
    'rotinj': menu_rotinj,
    'ssrdiv': menu_ssrdiv,
    'csvsave': menu_csv,
    'q': exit,
}
 
# =======================
#      MAIN PROGRAM
# =======================
 
# Main Program
if __name__ == "__main__":
    # Launch main menu
    os.system('clear')
    stop = None
    while not stop:
        main_menu()
