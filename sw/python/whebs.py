#!/bin/env python
# -*- coding: utf-8 -*-
#title           :whebs.py
#description     :This program displays an interactive menu on CLI
#author          :GG from menu_exple.py
#date            :
#version         :0.1
#usage           :python whebs.py
#notes           :
#python_version  :3+  
#=======================================================================
# Import the modules needed to run the script.
import sys
import readline
import pickle
import os
import numpy as np

from whbck import *
from whkit import *

# =======================
#     MENUS FUNCTIONS
# =======================
 
# Main menu
def main_menu():
#    os.system('clear')
    global pasini

    if pasini == False:
        menu_ini()
        pasini = True
    else:
        choice = raw_input("ebs>>  ")
        exec_menu(choice)
 
    return
 
# Execute menu
def exec_menu(choice):
    ch = choice.lower()
    if ch == '':
        menu_actions['main_menu']()
    else:
        try:
            menu_actions[ch]()
        except KeyError:
            print("Invalid whebs.py selection, please try again.\n")
            menu_actions['main_menu']()
    return

# initialization
def menu_ini():
    global wlist
    global buckparam
    rr = 0
    # ckeck current setup file
    suer = False
    try:
        with open('currentsu') as suf:
            wlist = []
            for line in suf:
                wlist.append(int(line.rstrip()))
            print("currentsu = {}".format(wlist))
    except IOError:
        print("No current setup file.")
        suer = True

    # change setup and save to file, or not
    if suer == True:
        jn = 'y'
    else:
        choice = raw_input("Change current setup ? : y/[n] >>  ")
        jn = choice.lower()

    if jn == 'y':
        wlist = tliste_y()
        print("wlist={}".format(wlist))
        with open('currentsu', 'w') as suf:
            for sui in range(len(wlist)):
                suf.write(str(wlist[sui])+"\n")

    # set master/slave mode for events according to wlist
    # master
    evt_board_mode(wlist[0], 0)
    # slaves are event slaves
    for modu in wlist[1:]:
        evt_board_mode(modu, 1)

    # ckeck fine_delay_file
    fdelbool = False
    try:
        with open('fdelcsv', "r") as fd:
            fdelbool = True
            print("---------------> Fine_delay_file detected")
    except IOError:
        print("No current fine_delay_file.")
        rr += 1

    # set bunch clock parameters : tango : 4 Hz, labo : up to 50 Hz
    print("Bunch clock parameters init.")
    choix = raw_input("Labo (up to 50 Hz) [l] default, or Tango (4 Hz) [t] bunch clock parameters >>  ")
    tl = choix.lower()
    if tl == "t":
        buckparam = dw_tango
    else:
        buckparam = dw_labo
    print("Bunch clock parameters : {}.".format(buckparam))

    # do or skip init phase
    choice = raw_input("     Skip network init ? [y]/n >>  ")
    jn = choice.lower()
    if jn == 'n':
        nw_init(wlist, buckparam, "fdelcsv")
    else:
        print("!!! NETWORK INIT SKIPPED !!!")
    return rr

# event config save to file
def menu_evtsave():
    global wlist

    sof = raw_input("Save event config to file.\n   -> Enter file name >>  ")
    if sof == "": return False

    # file exists ?
    suer = False
    try:
        with open(sof) as aecrire:
            print("Found file {}. Confirm delete before rewriting.".format(sof))
    except IOError:
        print("Create file {}.".format(sof))
        aecrire = open(sof, "wb")
        nline = []
        #skr = csv.DictWriter(aecrire, ('IO', 'BUS', 'CH',
        #                               'ID', 'ENAB', 'SRCE'),
        #                 dialect=csv.excel, extrasaction='ignore', restval='',
        #                     delimiter=',')

        #skr.writeheader()
        suer = True

    # save to file
    if suer == True:
        trans = get_evt_trans(wlist, "-n")
        if trans != []:
            for thop in trans:
                nline.append({'IO':thop[0], 'BUS':thop[1], 'CH':thop[2], 'ID':thop[3], 'ENAB':thop[4], 'SRCE':thop[5], 'DLONG':thop[6]})
                #skr.writerow(nline)
                #pickle.dump(nline, aecrire)
        disp_evts(trans)
        recoi = get_evt_recos(wlist, "-n")
        if recoi != []:
            for trec in recoi:
                nline.append({'IO':trec[0], 'BUS':trec[1], 'CH':trec[2], 'ID':trec[3], 'ENAB':trec[4]})
                #skr.writerow(nline)
        disp_evts(recoi)
        if nline != []:
            pickle.dump(nline, aecrire)

    # close anyway
    aecrire.close()

# event config load from file
def menu_evtload():
    global wlist
    
    # 1st clean up events
    evt_raz(wlist, "-n")
    # then ask for file
    sof = raw_input("Load event config from file.\n   -> Enter file name (CR = cleanup ALL events !) >>  ")
    if sof == "": return False
    # file exists ? then load
    try:
        # load from file
        with open(sof, "rb") as fd:
            evtss = pickle.load(fd)
            for ligne in evtss:
                bubu = ligne['BUS']
                cha = ligne['CH']
                # enable module
                if (evt_board_enable(bubu, 1)) == False: print("Bad board address in file")
                if ligne['IO'] == "i":
                    # set input in disable state ready for enable/start
                    setin(bubu, cha, 0, 0, 0x20)
                    sour = ligne['SRCE']
                    dlong = ligne['DLONG']
                    print("sour={}".format(sour))
                    print("sour(0)={} (1)={} (2)={}".format(sour[0], sour[1], sour[2]))
                    # set transmit event at cha | enable | ID | verbose | Source_list | delay=0
                    evt(bubu, "-t", cha, ligne['ENAB'], ligne['ID'], "-n", sour, dlong)
                    if sour[2] == 0:
                        # long delay T0 pulse : evos | enable | polarity=0 | mod=2=pulse | 0=T0 | delay=dlong | wid=500
                        puprog(sour[0], sour[1], 1, 0, 2, 0, dlong, 500)
                    else:
                        # program source as psrce, no phase
                        srdiv(int(sour[0]), int(sour[1]), int(sour[2]), 0, 0)
                elif ligne['IO'] == "o":
                    # create event receiver. cha is encoded [0..3]
                    # set receive event at cha | enable | ID | verbose | Source_list NO | delay=0
                    evt(bubu, "-r", cha, ligne['ENAB'], ligne['ID'], "-n", SRCE_NO, 0)
                    # enable associated output. Add 9 to cha
                    puprog(bubu, cha+9, 1, 0, 2, 8, 1, 1)
    except pickle.PickleError:
        print("Problem opening file {}.".format(sof))

# bunch list menu
def menu_bli():
    global wlist
    global buckparam
    print("Loop on some buckets in bunch list - single bunch injection mode. Choose some modules or all")
    choice = raw_input(" >> Bunch list > l(ist) OR [a](all) :  ")
    ch = choice.lower()
    if ch != "l": ch = "a"
    bli(wlist, buckparam, ch)
    return

# event transmit menu
def menu_evtt():
    global wlist
    global buckparam
    print("Enable / disable / define some events to be transmitted.")
    #evt_raz(wlist, "-n")
    trans = get_evt_trans(wlist, "-v")
    if trans == []:
        choice = raw_input(" >> Asynchronous event on Slave#1-in#2 : enable : y / [n] ? ")
        if choice == "y":
            # enable module then channel
            evt_board_enable(wlist[1], 1)
            # set input in disable state ready for enable/start
            setin(wlist[1], 2, 0, 0, 0x20)
            # set transmit event at channel 2 | enable | ID | verbose | Source_list | delay=0
            evt(wlist[1], "-t", 2, 1, EVTB, "-n", SRCE_B, 0)
        else:
            # disable input
            setin(wlist[1], 2, 0, 0, 0x20)
            # set transmit event at channel 2 | enable=0 | ID | verbose | Source_list | delay=0
            evt(wlist[1], "-t", 2, 0, EVTB, "-n", SRCE_NO, 0)
        choi = raw_input(" >> SRDIV event on Slave#1-out#8 to Slave#2-in#3 : enable : y / [n] ? ")
        if choi == "y":
            dimoif = raw_input("    -> Frequency between [0.1 -- 12000] Hz (default 1000) : ")
            mif = float(dimoif)
            if (mif > 12000) or (mif <= 0.1): mif = SR1KHZ
            dif = int(float(SRFREQ) / float(mif))
            print("Division factor = {} / srfreq = {}, frequency = {:<7.1f}".format(dif, SRFREQ, float(SRFREQ)/float(dif)))
            # enable module then channel
            evt_board_enable(wlist[2], 1)
            # set input in disable state ready for enable/start
            setin(wlist[2], 3, 0, 0, 0x20)
            # set transmit event at channel 3 | enable | ID | verbose | Source_list | delay=0
            evt(wlist[2], "-t", 3, 1, EVTA, "-n", [1, 8, dif], 0)
            # program source : bus=1 | channel=8 | division_factor | no phase
            srdiv(1, 8, dif, 0, 0)
        else:
            # disable event and reset source in core, let physical source as is
            # set transmit event at channel 3 | enable=0 | ID | verbose | Source_list | delay=0
            evt(wlist[2], "-t", 3, 0, EVTA, "-n", SRCE_NO, 0)
            # disable input on transceiver
            setin(wlist[2], 3, 0, 0, 0x20)
    else: disp_evts(trans)
    remo = raw_input(" >> Remove existing event : y / [n] ? ")
    if remo == "y":
        lisrem = []
        for trem in trans:
            lisrem.append(int(trem[3]))
        saisi = raw_input("    Select even_ID to be removed from {} : ".format(map(hex,lisrem)))
        selrem = get_v(0, saisi)
        if selrem not in lisrem:
            print("Bad ID !")
        else:
            for trem in trans:
                if trem[3] == selrem:
                    # disable evt and reset source in core, let physical source as is
                    # set transmit event at channel | enable=0 | ID | verbose | Source_list | delay=0
                    evt(trem[1], "-t", trem[2], 0, selrem, "-n", SRCE_NO, 0)
    ote = raw_input(" >> Another event : y / [n] ? ")
    if ote == "y":
        moe = raw_input("     >> Enter device from {} evt_input[0..3] evt_ID :\n        > bus input ID :  ".format(wlist))
        buss = get_v(0, moe)
        verif = 0
        verif += verifbus(wlist, buss)
        evin = get_v(1, moe)
        verif += verifevi(evin)
        evid = get_v(2, moe)
        if verif == 0:
            exti = raw_input("        >> Event source external [x] | or from bench [b] : ")
            if exti == "x":
                # external source
                psrce = SRCE_B
            else:
                clo = raw_input("        --> Bench event source : clock [c or CR] or long-delay-pulse [l] : ")
                if clo == "l":
                    sour = raw_input("        --> Bench event source parameters : bus output long_delay : ")
                else:
                    sour = raw_input("        --> Bench event source parameters : bus output frequency : ")
                busrc = get_v(0, sour)
                verif += verifbus(wlist, busrc)
                evos = get_v(1, sour)
                verif += verifsrc(evos)
                if verif == 0:
                    if clo == "l":
                        delon = get_v(2, sour)
                        sfreq = 0
                        psrce = [busrc, evos, 0]
                    else:
                        sfreq = get_fv(2, sour)
                        delon = 0
                        # max 12 KHz
                        if (sfreq < 0.1) or (sfreq > 12000):
                            srdivi = SRFREQ
                            print("force frequency to 1 Hz")
                        else:
                            srdivi = int(float(SRFREQ) // float(sfreq))
                        psrce = [busrc, evos, srdivi]
                    evt_board_enable(buss, 1)
                    # set input in disable state ready for enable/start | polarity=0 | 0x20 threshold
                    setin(buss, evin, 0, 0, 0x20)
                    # set transmit event at channel=evin | enable | ID | verbose | Source_list | delay=0
                    evt(buss, "-t", evin, 1, evid, "-n", psrce, delon)
                    if psrce[2] == 0:
                        # long delay T0 pulse : evos | enable | polarity=0 | mod=2=pulse | 0=T0 | delay=longdel (max for pulse) | wid=200
                        puprog(busrc, evos, 1, 0, 2, 0, LONGDEL, 500)
                    else:
                        # program source as psrce, no phase
                        srdiv(psrce[0], psrce[1], psrce[2], 0, 0)
    
# event receive menu
def menu_evtr():
    global wlist
    global buckparam
    recoi = get_evt_recos(wlist, "-n")
    if recoi != []:
        print("Received events :")
        disp_evts(recoi)
        credel = raw_input(" >> Create new [CR] or delete existing [d] : ")
        if credel == "d":
            verif = 0
            adel = raw_input("      Select : device output : ")
            buss = get_v(0, adel)
            verif += verifbus(wlist, buss)
            odel = get_v(1, adel)
            verif += verifevo(odel)
            if verif == 0:
                # set receive event at channel | enable=0 | ID=0 | verbose | Source_list | delay=0
                # for disable evt_id=0=don't care
                evt(buss, "-r", odel-9, 0, 0, "-n", SRCE_NO, 0)
                puprog(buss, odel, 0, 0, 2, 8, 1, 1)
    else: print("-----------> No Received event yet.")
    print("Available events :")
    trans = get_evt_trans(wlist, "-n")
    disp_evts(trans)
    ave = []
    for ti in trans:
        ave.append(ti[3])
    rec = raw_input(" >> Enter : Receiver_bus event_id associated_output : ")
    verif = 0
    buss = get_v(0, rec)
    verif += verifbus(wlist, buss)
    evid = get_v(1, rec)
    if evid not in ave:
        print("!!!!!!!!!! Bad Event !\n")
        verif = 1
    sort = get_v(2, rec)
    verif += verifevo(sort)
    if verif == 0:
        evt_board_enable(buss, 1)
        # set receive event at channel | enable | ID=evid | verbose | Source_list | delay=0
        evt(buss, "-r", sort-9, 1, evid, "-n", SRCE_NO, 0)
        puprog(buss, sort, 1, 0, 2, 8, 1, 1)

# event control menu
def menu_evtc():
    global wlist
    trans = get_evt_trans(wlist, "-n")
    disp_evts(trans)
    recoi = get_evt_recos(wlist, "-n")
    disp_evts(recoi)
    for ti in trans:
        zon = testin(ti[1], ti[2], "-n")
        dito = raw_input("\n---> Input_enable for event_ID# 0x{:04x} is {}. Toggle [y]/n ? ".format(ti[3], zon[2]))
        if (dito == "y") or (dito == ""):
            # if event stopped print transmit counter
            if zon[2] == 1:
                # disable input = stop event
                setin(ti[1], ti[2], not zon[2], 0, 0x20)
                # print counters
                print("     Event [0x{:04x}, {:2d}, {}] Transmit counter = {}".format(ti[3], ti[1], ti[2], rd_evt_cnt(ti[1], "-t", ti[2], "-n")))
                for ri in recoi:
                    if ri[3] == ti[3]:
                        print("     Event [0x{:04x}, {:2d}, {}] Receive counter  = {}".format(ri[3], ri[1], ri[2], rd_evt_cnt(ri[1], "-r", ri[2], "-n")))
            else:
                # clear counters
                clear_evt_cnt(ti[1], "-t", ti[2], "-n")
                #rd_evt_cnt(ti[1], "-t", ti[2], "-v")
                for ri in recoi:
                    if ri[3] == ti[3]:
                        clear_evt_cnt(ri[1], "-r", ri[2], "-n")
                        #rd_evt_cnt(ri[1], "-r", ri[2], "-v")
                # re-enable input = run event
                setin(ti[1], ti[2], not zon[2], 0, 0x20)
        else:
            print("     Running Event [0x{:04x}, {:2d}, {}] Transmit counter = {}".format(ti[3], ti[1], ti[2], rd_evt_cnt(ti[1], "-t", ti[2], "-n")))
            for ri in recoi:
                if ri[3] == ti[3]:
                    print("     Running Event [0x{:04x}, {:2d}, {}] Receive counter  = {}".format(ri[3], ri[1], ri[2], rd_evt_cnt(ri[1], "-r", ri[2], "-n")))
    
# set event long delay 
def menu_evtldel():
    global wlist
    print("Specify event transmit delay : event_ID delay_value")
    busd = raw_input("   >> ID del : ")
    did = get_v(0, busd)
    transld = verifidl(wlist, did)
    if transld != []:
        ddel = get_v(1, busd)
        evt(transld[1], "-t", transld[2], 1, transld[3], "-n", transld[5], ddel)
    else:
        print("Bad parameters")

# global event status 
def menu_evtst():
    global wlist
    print("\n !!!!!!!!!!! events status !!!!!!!!!!!!!!!!")
    for dut in wlist[1:]:
        rd_evt_status(dut, "-v")
        # with "-v", '-t' + '0' is enough... to read all in once
        rd_evt_cnt(dut, "-t", 0, "-v")

# event latency compare
def menu_evtdt():
    global wlist
    rmo = raw_input(" >> bus bus : ")
    bu1 = get_v(0, rmo)
    bu2 = get_v(1, rmo)

    wrts1 = evt_delta_wrts(bu1)
    wrts2 = evt_delta_wrts(bu2)

    print("Message latency on {}: TX_TAI->{} | TX_CYC->{} || RX_TAI->{} | RX_CYC->{}".format(bu1,wrts1[0],wrts1[1],wrts1[2],wrts1[3]))
    print("Message latency on {}: TX_TAI->{} | TX_CYC->{} || RX_TAI->{} | RX_CYC->{}".format(bu2,wrts2[0],wrts2[1],wrts2[2],wrts2[3]))

# event mode
def menu_evtmod():
    global wlist
    rmo = raw_input(" >> bus enable mode : ")
    bu = get_v(0, rmo)
    mo = get_v(1, rmo)
    evt_board_mode(bu, mo)

# bunch list reset
def menu_blirst():
    global wlist
    print("\n !! Stop T0 & reset bunch list pointers !!")
    menu_stop()
    print("   -> Check AUTOBLIST & AUTOROTINJ bits, turn OFF if active.")
    otob = {}
    for dut in wlist:
        otob[dut] = autobl(dut, "-t")
        autobl(dut, "-r")
    otor = {}
    for dut in wlist:
        otor[dut] = autoro(dut, "-t")
        autoro(dut, "-r")
    print("otob = {} / otor = {}.".format(otob, otor))
    # reset pointers
    for dut in wlist:
        blirst(dut)
    print("   -> Restore AUTOBLIST & AUTOROTINJ bits to previous state.")
    for dut in otob.keys():
        if (otob[dut] == 1): autobl(dut, "-s")
    for dut in otor.keys():
        if (otor[dut] == 1): autoro(dut, "-s")
    print("   -> All modules bunch lists in phase. Ready for run.")

# MultiBunch list
def menu_mbli():
    print("Choose default multi-bunch list file : mb16b.csv, or another one")
    mbfi = raw_input(" >> mb16b.csv=[CR] or other file name : ")
    if mbfi == "": mulbf = "mb16b.csv"
    else: mulbf = mbfi
    # stop T0
    seqen(wlist[0], "-w", 0, "-noverbose")
    # load file
    print("Loading {} in module list...".format(mulbf))
    rr = 0
    for dev in wlist:
        rr += ldbl(dev, mulbf)
        # prepare multi-bunch mode : mb & autoblist bits
        mbsetup(dev)
    # restart T0
    if rr == 0:
        print("File name problem")
    else:
        print("Sequencer restarted")
        seqen(wlist[0], "-w", 1, "-noverbose")
    
# rotinj menu
def menu_rotinj():
    choice = raw_input(" >> Apply 4-bunch injection rotation on a 1/4 booster : enable : y / [n] ? ")
    menu_stop()
    if choice == "y":
        bust = raw_input(" >> enter starting bucket number (default = 0) : ")
        debu = get_v(0, bust)
        if debu == -1:debu = 0
        for dev in wlist:
            # enable rotinj + autorotinj for dev, start at bucket#debu
            rotinj(dev, 1, 1, debu)
    else:
        for dev in wlist:
            # reset rotinj & autorotinj
            rotinj(dev, 0, 0, 0)
    menu_run()
    
# run / stop T0
def menu_stop():
    tog = seqen(wlist[0], "-w", 0, "-noverbose")
    #tog = seqen(wlist[0], "-w", 0, "-v")
    return tog

def menu_run():
    tog = seqen(wlist[0], "-w", 1, "-noverbose")
    #tog = seqen(wlist[0], "-w", 1, "-v")
    return tog

# check T0 counters
def menu_t0stat():
    tog = menu_stop()
    #get master t0_cnt as reference
    lt0 = t0stat(wlist[0], 1, "-n")
    print("     dev#{}    -->  Master_T0_cnt = {}".format(wlist[0], lt0[0]))
    for dev in wlist[1:]:
        # enable rotinj + autorotinj for dev, start at add=0
        lt0 = t0stat(dev, 0, "-n")
        if lt0[0] == lt0[1]:
            print("     dev#{} OK".format(dev))
        elif lt0[0] < lt0[1]:
            print("     dev#{} !!!!! T0bis = {} > T0_cnt = {}".format(dev, lt0[1], lt0[0]))
        else:
            print("     dev#{} !!!!! T0bis = {} < T0_cnt = {}".format(dev, lt0[1], lt0[0]))
    if tog == 1: menu_run()

# get & check TAI at PPS
def menu_taipps():
    #read 2 values separated by 3 seconds
    taip1 = taipps(wlist[0], 1, "-n")
    time.sleep(3)
    taip2 = taipps(wlist[0], 1, "-v")
    #print last and check
    pps2 = (taip2[2] << 32) | taip2[1]
    pps1 = (taip1[2] << 32) | taip1[1]
    print("----> At TAI = {}, PPS = {}.".format(taip2[0], pps2))
    print("   -> Delta over 3s :")
    print("   -> Delta_TAI = {}".format(taip2[0] - taip1[0]))
    print("   -> Delta_PPS = {}".format(float(pps2-pps1)/float(ESRFRF/8)))

# program outputs
def menu_prout():
    buso = raw_input(" >> bus output_front_panel_number ")
    buss = get_v(0, buso)
    verif = 0
    verif += verifbus(wlist, buss)
    outn = get_v(1, buso)
    if outn not in range(1, 13):
        print("!!!!!!!!!! Bad OUTPUT !\n")
        verif = 1
    if verif == 0: menuo(buss, outn)
    
# outputs status
def menu_os():
    print("Outputs status : enter board_bus_address")
    bubu = raw_input(" >> bus : ")
    buso = get_v(0, bubu)
    vok = verifbus(wlist, buso)
    if vok == 0:
        print("\n=====-----> WHIST#{}".format(buso))
        toto = statout(buso,  "-v")

# SRDIV outputs
def menu_srdivo():
    print("Outputs status : enter board_bus_address")
    bubu = raw_input(" >> bus : ")
    buso = get_v(0, bubu)
    vok = verifbus(wlist, buso)
    if vok == 0:
        print("\n=====-----> WHIST#{}".format(buso))
        toto = srdivout(buso,  "-v")

# input stamping test
def menu_tsts():
    print("Input stamping test : enter board_bus_address input_nb gate_length(default=1s)")
    bubu = raw_input(" >> bus in# gate(s) : ")
    buso = get_v(0, bubu)
    vok = verifbus(wlist, buso)
    if (vok == 0) and (buso != wlist[0]):
        ino = get_v(1, bubu)
        gato = get_v(2, bubu)
        if gato <= 0: gato = 1
        print("\n=====-----> WHIST#{}".format(buso))
        toto = tsts(buso,  ino, gato, "-noverbose")

# input stamp read
def menu_rdstamp():
    print("Input stamp read : enter board_bus_address input_nb")
    bubu = raw_input(" >> bus in# : ")
    buso = get_v(0, bubu)
    vok = verifbus(wlist, buso)
    if (vok == 0) and (buso != wlist[0]):
        ino = get_v(1, bubu)
        stampe = rdstamp(buso, ino)
        print("   ---> W#{}, in#{} stamp = {}".format(buso, ino, stampe))

# program outputs
def menu_bcl():
    print("\nNetwork wide bunck clock parameters edit")
    print("     >> Current INJ delay : {}.".format(hex(buckparam[0])))
    print("     >> Current GUN delay : {}.".format(hex(buckparam[1])))
    print("     >> Current EXT delay : {}.".format(hex(buckparam[2])))
    para = raw_input(" >> enter parameter (inj/gun/ext) : ")
    pa = para.lower()
    ige = get_v(0, pa)
    if ige == "e":
        dwe = raw_input(" -----> extraction delay (optional EXT_width (default=1) : ")
        dw = dwe.lower()
        dlu = get_v(0, dw)
        wlu = get_v(1, dw)
        buckparam[2] = dlu
        if wlu > 0: buckparam[4] = wlu
    if ige == "g":
        dwi = raw_input(" -----> gun delay (optiional INJ_width (default=1) : ")
        dw = dwi.lower()
        dlu = get_v(0, dw)
        wlu = get_v(1, dw)
        buckparam[1] = dlu
        if wlu > 0:buckparam[3] = wlu
    if ige == "i":
        dwt = raw_input(" -----> injection delay : ")
        dw = dwt.lower()
        dlu = get_v(0, dw)
        buckparam[0] = dlu
    print("New buclk_param list {:#x}, {:#x}, {:#x}, {:#x}, {:#x},".format(*buckparam))
    for uist in wlist:
        dwprog(uist, buckparam)
    pipebunch(wlist[0])
    
# master start
def menu_mstart():
    menu_stop()
    mstart(wlist[0])    

# pipe bunchclock params
def menu_pipb():
    pipebunch(wlist[0])    

# verif functions
def verifbus(list, buss):
    verif = 0
    if buss not in list:
        print("!!!!!!!!!! Bad bus {} !".format(buss))
        print("           liste = {} \n".format(list))
        verif = 1
    return verif

def verifidl(list, id):
    # check that id is a recorded long delay event and return it
    trald = []
    trans = get_evt_trans(list, "-n")    
    for tild in trans:
        # [5][2] is long delay key, [3] is id
        if (tild[5][2] == 0) and (tild[3] == id): trald = tild
    return trald

def verifevo(evo):
    verif = 0
    if evo not in range(9, 13):
        print("!!!!!!!!!! Bad event output !\n")
        verif = 1
    return verif

def verifevi(evi):
    verif = 0
    if evi not in range(4):
        print("!!!!!!!!!! Bad event input !\n")
        verif = 1
    return verif

def verifsrc(src):
    verif = 0
    if src not in range(4, 9):
        print("!!!!!!!!!! Bad event source !\n")
        verif = 1
    return verif

# display menu on help
def disp_menu():
    global wlist
    print("hlp or help : this")
    print("\n!!!")
    print("1st PARAMETER IS BOARD LOCATION ON PCIe bus IN {} : ALWAYS MANDATORY".format(wlist))
    print("remaining parameters are optional, separated by SPACE, 0=unchanged or default")
    print("parameters are integers, decimal or hexa.")
    print("!!!\n")
    print("bcl      : change bunch clock parameters")
    print("pipb     : load pipeline bunchclock parameters")
    print("bli      : loop on up to 992 buckets - single bunch")
    print("blirst   : reset bunch list pointer on active modules")
    print("evtc     : event test control menu")
    print("evtdt    : event compare latency")
    print("evtldel  : event long delay program")
    print("evtload  : event load config")
    print("evtmod   : event board mode menu")
    print("evtr     : event test receive menu")
    print("evtsave  : event save config")
    print("evtst    : event general status")
    print("evtt     : event test transmit menu")
    print("ini      : network init")
    print("os       : module output status")
    print("srdivo   : SRDIV outputs detailed status")
    print("tsts     : stamping test")
    print("taipps   : get & check TAI at PPS")
    print("rdstamp  : read time stamp on input")
    print("prout    : program output")
    print("rotinj   : full injection rotation + bunchlist loop menu")
    print("mbli     : loop on multi bunch list")
    print("mstart   : master start")
    print("run      : run/enable T0")
    print("stop     : stop T0")
    print("t0stat   : check T0 counters")
    print("q        : quit")
    return
 
# Exit program
def exit():
    print("Bunch clock test end.")
    sys.exit()
 
# =======================
#    MENUS DEFINITIONSx1
# =======================
 
# Menu definition
menu_actions = {
    'main_menu': main_menu,
    'hlp': disp_menu,
    'help': disp_menu,
    'bcl': menu_bcl,
    'pipb': menu_pipb,
    'bli': menu_bli,
    'blirst': menu_blirst,
    'evtc': menu_evtc,
    'evtdt': menu_evtdt,
    'evtld': menu_evtldel,
    'evtload': menu_evtload,
    'evtmod': menu_evtmod,
    'evtr': menu_evtr,
    'evtsave': menu_evtsave,
    'evtst': menu_evtst,
    'evtt': menu_evtt,
    'ini': menu_ini,
    'os': menu_os,
    'srdivo': menu_srdivo,
    'tsts': menu_tsts,
    'taipps': menu_taipps,
    'rdstamp': menu_rdstamp,
    'prout': menu_prout,
    'rotinj': menu_rotinj,
    'mbli': menu_mbli,
    'mstart': menu_mstart,
    'run': menu_run,
    'stop': menu_stop,
    't0stat': menu_t0stat,
    'q': exit,
}
 
# =======================
#      MAIN PROGRAM
# =======================
 
# Main Program
if __name__ == "__main__":
    # Launch main menu
    os.system('clear')
    stop = None
    global pasini
    pasini = False
    while not stop:
        main_menu()
