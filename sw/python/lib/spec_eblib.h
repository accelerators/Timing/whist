#ifndef SPECEBLIB_H
#define SPECEBLIB_H

#include <stdint.h>

extern "C" {

//open an etherbone connection on device of IP address <ipaddr>
//return 1 on success, 0 otherwise
uint8_t eb_open(const char *ipaddr);

//close an etherbone coonection
void eb_close(void);

//read a register of a wishbone bus at address <address>
//return the data contained in the register
uint32_t eb_read(uint32_t address);

//write <data> to a register of a wishbone bus at address <address>
void eb_write(uint32_t address, uint32_t data);

//load LM32 firmware of a WR Node Core cpu
//address  : base address of the WR Node Core
//cpu_index: index of the CPU to program (from 0 to 7)
//file     : full path of the firmware binary
void eb_wrnc_lm32_loader(uint32_t address, int cpu_index, char *file);

//restart a WR Node Core cpu
//address  : base address of the WR Node Core
//cpu_index: index of the CPU to restart (from 0 to 7)
void eb_wrnc_lm32_restart(uint32_t address, int cpu_index);

}

#endif // SPECEBLIB_H
