#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <asm/byteorder.h>
#include <unistd.h>
#include <hw/wrn_cpu_csr.h>

#include "etherbone.h"
#include "spec_eblib.h"

#define ATTEMPS 2
#define FORMAT  0x14

#define WRNODE_CSR  0xC000
#define WRNODE_SMEM 0x10000


static eb_socket_t socket;
static eb_status_t ebst;
static eb_device_t device;
static eb_cycle_t cycle;
static eb_data_t eb_dev_data;
static uint32_t base_wrnode;


static void set_stop(eb_user_data_t user, eb_device_t dev, eb_operation_t op, eb_status_t status)
{
  int* stop = (int*)user;
  *stop = 1;
  
  if (status != EB_OK)
  {
    fprintf(stderr, "Etherbone cycle error: %s\n",eb_status(status));
  }
  else
  {
    eb_dev_data = 0;
    for (; op != EB_NULL; op = eb_operation_next(op))
    {
      /* We read low bits first */
      eb_dev_data <<= (eb_operation_format(op) & EB_DATAX) * 8;
      eb_dev_data |= eb_operation_data(op);

      if (eb_operation_had_error(op))
        fprintf(stderr, "Wishbone segfault reading %s %s bits from address 0x%08x\n",
                        eb_width_data(eb_operation_format(op)), 
                        eb_format_endian(eb_operation_format(op)), eb_operation_address(op));
    }
  }
}

uint8_t eb_open(const char *ipaddr)
{
  if ((ebst = eb_socket_open(
         EB_ABI_CODE, 0, EB_ADDRX|EB_DATAX, &socket)) != EB_OK)
  {
    fprintf(stderr,"Failed to open Etherbone socket: %s\n",eb_status(ebst));
    return 0;
  }
  else
  {
    char udpaddr[24];
    sprintf(udpaddr,"udp/%s",ipaddr);
    if ((ebst = eb_device_open(
  	 socket, (const char*)udpaddr, EB_ADDRX|EB_DATAX, ATTEMPS, &device)) != EB_OK)
    {
      fprintf(stderr,"Failed to open Etherbone device: %s\n",eb_status(ebst));
      return 0;
    }
  }
  return 1;
}

void eb_close(void)
{
  eb_socket_close(socket);
  eb_device_close(device);
}

uint32_t eb_read(uint32_t address)
{
  int stop;
  if((ebst = eb_cycle_open(device, &stop, &set_stop, &cycle)) != EB_OK)
  {
    fprintf(stderr,"Failed to create cycle: %s\n",eb_status(ebst));
    return 0xffffffff;
  }
  else
  {
    eb_cycle_read(cycle, address, FORMAT, 0);
    eb_cycle_close(cycle);
    stop = 0;
    while (!stop)
    {
      //wait 10ms (10ms->10000us)
      eb_socket_run(socket, 10000);
    }
  }
  return (uint32_t)eb_dev_data;
}

void eb_write(uint32_t address, uint32_t data)
{
  int stop;
  if((ebst = eb_cycle_open(device, &stop, &set_stop, &cycle)) != EB_OK)
  {
    fprintf(stderr,"Failed to create cycle: %s\n",eb_status(ebst));
  }
  else
  {
    eb_cycle_write(cycle, address, FORMAT,data);
    eb_cycle_close(cycle);
    stop = 0;
    while (!stop)
    {
      //wait 10ms (10ms->10000us)
      eb_socket_run(socket, 10000);
    }
  }
}


/******************************************************************************/
/*            WR NODE CORE - LM32 program loading related                     */
/******************************************************************************/
/**
 * Set the reset bit of the CPUs according to the mask
 */
void wrnc_lm32_reset_set(uint8_t mask)
{
	uint32_t reg_val;
	reg_val = eb_read(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_RESET);
	reg_val |= (mask & 0xFF);
	eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_RESET,reg_val);
}

/**
 * Clear the reset bit of the CPUs according to the mask
 */
static void wrnc_lm32_reset_clr(uint8_t mask)
{
	uint32_t reg_val;
	reg_val = eb_read(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_RESET);
	reg_val &= (~mask & 0xFF);
	eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_RESET,reg_val);
}

/**
 * Set the reset bit of the CPUs according to the mask
 * NOTE : for the CPU 1 means pause
 */
void wrnc_lm32_enable_set(uint8_t mask)
{
	uint32_t reg_val;
	reg_val = eb_read(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_ENABLE);
	reg_val |= (mask & 0xFF);
	eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_ENABLE,reg_val);
}

/**
 * Clear the reset bit of the CPUs according to the mask
 * NOTE : for the CPU 0 means run
 */
static void wrnc_lm32_enable_clr(uint8_t mask)
{
	uint32_t reg_val;
	reg_val = eb_read(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_ENABLE);
	reg_val &= (~mask & 0xFF);
	eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_ENABLE,reg_val);
}


static void wrnc_restart(uint8_t cpu_index)
{
        //stop (pause) the cpu
        wrnc_lm32_reset_set(1 << cpu_index);
        //disable the cpu
        wrnc_lm32_enable_set(1 << cpu_index);

        //enable the cpu
        wrnc_lm32_enable_clr(1 << cpu_index);
        //start (run) the cpu
        wrnc_lm32_reset_clr(1 << cpu_index);
}


/**
 * It loads a given application into the CPU memory
 */
static int wrnc_lm32_firmware_load(int cpu_index, void *fw_buf,
				  size_t count, loff_t off)
{
  uint32_t *fw = (uint32_t*)fw_buf, word, word_rb;
	int size, offset, i, cpu_memsize;

	/* Select the CPU memory to write */
	eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_CORE_SEL,cpu_index);
	cpu_memsize = eb_read(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_CORE_MEMSIZE);

	if (off + count > cpu_memsize) {
		fprintf(stderr,
			"Cannot load firmware: size limit %d byte\n",
			cpu_memsize);
		return -1;
	}

	/* Calculate code size in 32bit word*/
	size = (count + 3) / 4;
	offset = off / 4;

	/* Reset the CPU before overwrite its memory */
	wrnc_lm32_reset_set( (1 << cpu_index));

	/* Clean CPU memory */
	for (i = offset; i < cpu_memsize / 1024; ++i) {
	  eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_UADDR,i);
	  usleep(1);
	  eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_UDATA,0);
	  usleep(1);
	}

	/* Load the firmware */
	for (i = 0; i < size; ++i) {
		word = __cpu_to_be32(fw[i]);
		eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_UADDR,
			 i + offset);
		usleep(1);
		eb_write(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_UDATA,word);
		usleep(1);
		word_rb = eb_read(base_wrnode+WRNODE_CSR+WRN_CPU_CSR_REG_UDATA);
		usleep(1);
		if (word != word_rb) {
			fprintf(stderr,
				"failed to load firmware (byte %d | 0x%x != 0x%x)\n",
				i, word, word_rb);
			return -1;
		}
	}

	return 0;
}

/**
 * It loads a WRNC CPU firmware from a given file
 * @param[in] cpu_index CPU index
 * @param[in] path path to the firmware file
 * @return 0 on success, on error -1 and errno is set appropriately
 */
int wrnc_lm32_load_application_file(int cpu_index,
				   char *path)
{
	int i, len;
	void *code;
	FILE *f;

	f = fopen(path, "rb");
	if (!f)
		return -1;

	fseek(f, 0, SEEK_END);
	len = ftell(f);
	rewind(f);
	if (!len)
		return -1;

	code = malloc(len);
	if (!code)
		return -1;

	/* Get the code from file */
	i = fread(code, 1, len, f);
	fclose(f);
	if (!i || i != len) {
		/*TODO: maybe optimize me with a loop */
		free(code);
		return -1;
	}

	if(wrnc_lm32_firmware_load(cpu_index, code, len, 0)!=0)
	{
		return -1;
	}
	free(code);

	return 0;
}

void eb_wrnc_lm32_loader(uint32_t address, int cpu_index, char *file)
{
  int err;
  base_wrnode = address;
  if (!file)
  {
  	fprintf(stderr, "Missing binary file to load\n");
	return;
  }
  /* Write the content of a given file to the CPU application memory */
  err = wrnc_lm32_load_application_file(cpu_index, file);
  if (err)
  {
  	fprintf(stderr,
  		"Cannot load application to CPU %d\n",
  		cpu_index);
  }
  wrnc_restart(cpu_index);
}

void eb_wrnc_lm32_restart(uint32_t address, int cpu_index)
{
  base_wrnode = address;
  wrnc_restart(cpu_index);
}
