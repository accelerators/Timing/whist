#!/bin/env python

from __future__ import print_function
import sys
import csv
#import string
import time
import readline
import datetime as pendul

import numpy as np

from whist_whdds import SpecWhist

# constants
# super period esrf value
#SUPERP        = 31.0*11*32/352202000
# super period lab value with SPEC local oscillator
SUPERP        = 31.0*11*8/75000000
SI571F        = 352370000
ESRFRF        = 352370000
TAQUET        = 25
rf_pps_a      = 1
rf_pps_b      = 2
dacadd        = 0x90
i2c_70        = 0x70
i2c_72        = 0x72
i2c_74        = 0x74
i2c_76        = 0x76

PHADJUST      = 0x1b # 2Capacitor=011b,800uA=011b
#PHADJUST      = 0x3b # 1Capacitor=111b,800uA=011b

ctrl9516      = [0x0,  0x3,  0x4,
                 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17,
                 0x18, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x1E, 0x1F,
                 0xa0, 0xa1, 0xa2, 0xa3, 0xa4, 0xa5, 0xa6, 0xa7, 0xa8, 0xa9, 0xaa, 0xab,
                 0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5,
                 0x140, 0x141, 0x142, 0x143,
                 0x190, 0x191, 0x192, 0x193, 0x194, 0x195, 0x196, 0x197, 0x198,
                 0x199, 0x19a, 0x19b, 0x19c, 0x19d,
                 0x19e, 0x19f, 0x1a0, 0x1a1, 0x1a2,
                 0x1e0, 0x1e1, 0x230]
ctrl9510      = [0x0,  0x2,  0x4,  0x5,  0x6,  0x7,  0x8,  0x9,  0xa,  0xb,  0xc,  0xd,
                 0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0x3e, 0x3f,
                 0x40, 0x41, 0x42, 0x43, 0x44, 0x45, 0x48, 0x49, 0x4a, 0x4b, 0x4c, 0x4d, 0x4e, 0x4f,
                 0x50, 0x51, 0x52, 0x53, 0x54, 0x55, 0x56, 0x57, 0x58, 0x5a]

fd78csvcols   = ['BUS', 'fd1', 'fd2', 'fd3', 'md2', 'md3']

# COMMAND ID LIST copied from evt.h
EVT_CMD_ENABLE         = 0x1
EVT_CMD_MODE           = 0x2
EVT_CMD_TRANSMITTER    = 0x3
EVT_CMD_RECEIVER       = 0x4
EVT_CMD_RD_VERSION     = 0xa

# COMMAND ID LIST copied from bcc.h
BCC_CMD_ENABLE         = 0x1
BCC_CMD_MODE           = 0x2
BCC_MASTER_CMD_SEQ_EN  = 0x3
BCC_MASTER_CMD_START   = 0x4
BCC_CMD_IOCONF         = 0x5
BCC_CMD_DAC            = 0x6
BCC_CMD_SLOK           = 0x7
BCC_CMD_PPS_MON        = 0x8
BCC_CMD_RD_HISTO       = 0x9
BCC_CMD_RD_CSR         = 0xa
BCC_CMD_571            = 0xb
BCC_CMD_VCXO_FREQ      = 0xc
BCC_CMD_RD_VERSION     = 0xd
BCC_CMD_RD_T0BIS       = 0xe
BCC_CMD_RD_T0BIS_MISS  = 0xf
BCC_MASTER_CMD_BCL_PIPE = 0x10

# COMMAND ID LIST copied from d3s.h
D3S_CMD_9516           = 0xa
D3S_CMD_9510           = 0xb
D3S_CMD_RFOE           = 0xc

# ------------------------------------------------------
def get_v(nn, args):
# ------------------------------------------------------
    """returns nnth arg from args as an integer
    if missing returns 0"""
    argums = args.split(" ")
    try:
        if len(argums) < (nn+1): raise
        else:
            try:
                vlu = argums[nn]
                if vlu.startswith("0X") or vlu.startswith("0x"):
                    vv = int(vlu, base=16)
                elif vlu.startswith("-"):
                    vv = vlu
                else:
                    vv = int(vlu)
            except Exception:
                print("arg#", nn, "integer")
                return 0
    except Exception:
        print("arg#", nn, "missing")
        return -1
    return vv

# ------------------------------------------------------
def bus_lup(args):
# ------------------------------------------------------
    """allocate IP address acdording to Pcie bus"""
    bus = get_v(0, args)

    if bus == "-cr104":              # mac :0b
        ipa = '10.10.10.11'
    elif bus == "-cr104-diag1":      # mac :03
        ipa = '10.10.10.3'
    elif bus == "-cr104-diag2":      # mac :0d
        ipa = '10.10.10.13'
    elif bus == "-ctrm-srrf1":       # mac :16
        ipa = '10.10.10.22'
    elif bus == "-ctrm-srrf3":       # mac :31
        ipa = '10.10.10.49'
    elif bus == "-d-mbf-c31"  :      # mac :05
        ipa = '10.10.10.5'
    elif bus == "-ext1":             # mac :23
        ipa = '10.10.10.35'
    elif bus == "-ext2":             # mac :0a
        ipa = '10.10.10.10'
    elif bus == "-ext3":             # mac :0e
        ipa = '10.10.10.14'
    elif bus == "-id04-diag1":       # mac :2f
        ipa = '10.10.10.47'
    elif bus == "-inj1":             # mac :38
        ipa = '10.10.10.56'
    elif bus == "-linac":            # mac :04
        ipa = '10.10.10.4'
    elif bus == "-master-ctrm":      # mac :08
        ipa = '10.10.10.8'
    elif bus == "-mn104-diag1":      # mac :2b
        ipa = '10.10.10.43'
    elif bus == "-mn105-diag1":      # mac :33
        ipa = '10.10.10.51'
    elif bus == "-pinj-diag1":       # mac :2e
        ipa = '10.10.10.46'
    elif bus == "-spare-cr104":      # mac :21
        ipa = '10.10.10.33'
    elif bus == "-spare-injext":     # mac :0f
        ipa = '10.10.10.15'
    elif bus == "-spare-linac":      # mac :09
        ipa = '10.10.10.9'
    elif bus == "-sypc1":            # mac :12
        ipa = '10.10.10.12'
    elif bus == "-syrf1"      :      # mac :11
        ipa = '10.10.10.17'
    elif bus == "-tz01-diag1":       # mac :1c
        ipa = '10.10.10.28'
    elif bus == "-tz02-diag1":       # mac :15
        ipa = '10.10.10.21'
    elif bus == "-tz03-diag1":       # mac :12
        ipa = '10.10.10.18'
    elif bus == "-tz04-diag1":       # mac :18
        ipa = '10.10.10.24'
    elif bus == "-tz04-spectra":     # mac :13
        ipa = '10.10.10.19'
    elif bus == "-tz06-diag1":       # mac :1d
        ipa = '10.10.10.29'
    elif bus == "-tz07-diag1":       # mac :1e
        ipa = '10.10.10.30'
    elif bus == "-tz08-diag1":       # mac :1a
        ipa = '10.10.10.26'
    elif bus == "-tz10-diag1":       # mac :1b
        ipa = '10.10.10.27'
    elif bus == "-tz12-diag1":       # mac :30
        ipa = '10.10.10.48'
    elif bus == "-tz15-diag1":       # mac :14
        ipa = '10.10.10.20'
    elif bus == "-tz19-diag1":       # mac :1f
        ipa = '10.10.10.31'
    elif bus == "-tz26-diag1":       # mac :20
        ipa = '10.10.10.32'
    elif bus == "-tz27-diag1":       # mac :22
        ipa = '10.10.10.34'
    elif bus == "-tz29-diag1":       # mac :2a
        ipa = '10.10.10.42'
    #end of asd setup and module names, start of number entry
    elif bus == 0:                   # mac :00
        ipa = '10.10.10.99'
    elif bus == 1:                   # mac :01
        ipa = '10.10.10.98'
    elif bus == 2:                   # mac :02
        ipa = '10.10.10.2'
    elif bus == 3:                   # mac :03
        ipa = '10.10.10.3'
    elif bus == 4:                   # mac :04
        ipa = '10.10.10.4'
    elif bus == 5:                   # mac :05
        ipa = '10.10.10.5'
    elif bus == 6:                   # mac :06
        ipa = '10.10.10.6'
    elif bus == 7:                   # mac :07
        ipa = '10.10.10.7'
    elif bus == 8:                   # mac :08
        ipa = '10.10.10.8'
    elif bus == 9:                   # mac :09
        ipa = '10.10.10.9'
    elif bus == 10:                  # mac :0a
        ipa = '10.10.10.10'
    elif bus == 11:                  # mac :0b
        ipa = '10.10.10.11'
    elif bus == 12:                  # mac :0c
        ipa = '10.10.10.12'
    elif bus == 13:                  # mac :0d
        ipa = '10.10.10.13'
    elif bus == 14:                  # mac :0e
        ipa = '10.10.10.14'
    elif bus == 15:                  # mac :0f
        ipa = '10.10.10.15'
    elif bus == 16:                  # mac :10
        ipa = '10.10.10.16'
    elif bus == 17:                  # mac :11
        ipa = '10.10.10.17'
    elif bus == 18:                  # mac :12
        ipa = '10.10.10.18'
    elif bus == 19:                  # mac :13
        ipa = '10.10.10.19'
    elif bus == 20:                  # mac :14
        ipa = '10.10.10.20'
    elif bus == 21:                  # mac :15
        ipa = '10.10.10.21'
    elif bus == 22:                  # mac :16
        ipa = '10.10.10.22'
    elif bus == 23:                  # mac :17
        ipa = '10.10.10.23'
    elif bus == 24:                  # mac :18
        ipa = '10.10.10.24'
    elif bus == 25:                  # mac :19
        ipa = '10.10.10.25'
    elif bus == 26:                  # mac :1a
        ipa = '10.10.10.26'
    elif bus == 27:                  # mac :1b
        ipa = '10.10.10.27'
    elif bus == 28:                  # mac :1c
        ipa = '10.10.10.28'
    elif bus == 29:                  # mac :1d
        ipa = '10.10.10.29'
    elif bus == 30:                  # mac :1e
        ipa = '10.10.10.30'
    elif bus == 31:                  # mac :1f
        ipa = '10.10.10.31'
    elif bus == 32:                  # mac :20
        ipa = '10.10.10.32'
    elif bus == 33:                  # mac :21
        ipa = '10.10.10.33'
    elif bus == 34:                  # mac :22
        ipa = '10.10.10.34'
    elif bus == 35:                  # mac :23
        ipa = '10.10.10.35'
    elif bus == 36:                  # mac :24
        ipa = '10.10.10.36'
    elif bus == 37:                  # mac :25
        ipa = '10.10.10.37'
    elif bus == 38:                  # mac :26
        ipa = '10.10.10.38'
    elif bus == 39:                  # mac :27
        ipa = '10.10.10.39'
    elif bus == 40:                  # mac :28
        ipa = '10.10.10.40'
    elif bus == 41:                  # mac :29
        ipa = '10.10.10.41'
    elif bus == 42:                  # mac :2a
        ipa = '10.10.10.42'
    elif bus == 43:                  # mac :2b
        ipa = '10.10.10.43'
    elif bus == 44:                  # mac :2c
        ipa = '10.10.10.44'
    elif bus == 45:                  # mac :2d
        ipa = '10.10.10.45'
    elif bus == 46:                  # mac :2e
        ipa = '10.10.10.46'
    elif bus == 47:                  # mac :2f
        ipa = '10.10.10.47'
    elif bus == 48:                  # mac :30
        ipa = '10.10.10.48'
    elif bus == 49:                  # mac :31
        ipa = '10.10.10.49'
    elif bus == 50:                  # mac :32
        ipa = '10.10.10.50'
    elif bus == 51:                  # mac :33
        ipa = '10.10.10.51'
    elif bus == 52:                  # mac :34
        ipa = '10.10.10.52'
    elif bus == 53:                  # mac :35
        ipa = '10.10.10.53'
    elif bus == 54:                  # mac :36
        ipa = '10.10.10.54'
    elif bus == 55:                  # mac :37
        ipa = '10.10.10.55'
    elif bus == 56:                  # mac :38
        ipa = '10.10.10.56'
    elif bus == 57:                  # mac :39
        ipa = '10.10.10.57'
    elif bus == 58:                  # mac :3a
        ipa = '10.10.10.58'
    elif bus == 59:                  # mac :3b
        ipa = '10.10.10.59'
    elif bus == 60:                  # mac :3c
        ipa = '10.10.10.60'
    elif bus == 61:                  # mac :3d
        ipa = '10.10.10.61'
    elif bus == 62:                  # mac :3e
        ipa = '10.10.10.62'
    elif bus == 63:                  # mac :3f
        ipa = '10.10.10.63'
    elif bus == 64:                  # mac :40
        ipa = '10.10.10.64'
    elif bus == 65:                  # mac :41
        ipa = '10.10.10.65'
    elif bus == 66:                  # mac :42
        ipa = '10.10.10.66'
    else:
        ipa = '0'
    return ipa

# ------------------------------------------------------
def libcmd(args):
# ------------------------------------------------------
    """execute basic library function"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    cmdgot = get_v(1, args)
    cmdgot = cmdgot.lstrip("-")
    cmdname = "dd." + cmdgot
    print(cmdname)

    dd = SpecWhist(ipp)
    exec(cmdname)

# ------------------------------------------------------
def loadbf(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    versionbit = get_v(1, args)

    if bus:
        tb = SpecTbv7(bus)
        fichbi = '/segfs/linux/timing/tbv' + str(versionbit) + '_top.bin'
        print(fichbi)
        tb.specLoadBitstream(fichbi)
        fla = tb.tbTestFlash()
        if fla:
            print("V{0:d} : flashed bit set".format(versionbit))
#    del(tb) useless because automatically done why ?
        else:
            print("!!! V{0:d} : Error flashing device : flashed bit NOT set !!!".format(versionbit)) 
    else:
        return False

# ------------------------------------------------------
def lm32Loader(args):
# ------------------------------------------------------
    """load LM32 program"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    cpuIndex = get_v(1, args)
    lm32File = get_v(2, args)
    path = lm32File[1:]
    print("file:{}".format(path))
    dd = SpecWhist(ipp)

#restart wrnc/CPU0 dds
    dd.ddsLm32Loader(cpuIndex,path)

# ------------------------------------------------------
def si_freq(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)

    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_VCXO_FREQ)
    # 0 for read then address
    ddm.ddsSetCoreBcc(0xc, 0)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    sireg = ddm.ddsGetCoreBcc(0x8)
    #discard
    ddm.ddsDisCoreBcc()
    #optional purge
    ddm.ddsPurCoreBcc()

    return sireg

# ------------------------------------------------------
def si_dis(buss, dis):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)
    sireg = []
    for chacha in range(7, 13):
        ddm.ddsSetCoreBcc(0x0, 0x1000000)
        ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
        # 0 for read then address
        ddm.ddsSetCoreBcc(0xc, 0)
        ddm.ddsSetCoreBcc(0x10, chacha)
        ddm.ddsSetCoreBcc(0x0, 0x4000010)
        time.sleep(0.01)
        sireg.append(ddm.ddsGetCoreBcc(0x8))
        print("si_reg {} : {}".format(chacha, hex(sireg[chacha-7])))
        #discard
        ddm.ddsDisCoreBcc()
        #optional purge
        ddm.ddsPurCoreBcc()
    # now 135
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 0 for read
    ddm.ddsSetCoreBcc(0xc, 0)
    ddm.ddsSetCoreBcc(0x10, 135)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    sireg.append(ddm.ddsGetCoreBcc(0x8))
    print("si_reg {} : {}".format(135, hex(sireg[6])))
    #discard
    ddm.ddsDisCoreBcc()
    #optional purge
    ddm.ddsPurCoreBcc()
    
    # now 137
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 0 for read
    ddm.ddsSetCoreBcc(0xc, 0)
    ddm.ddsSetCoreBcc(0x10, 137)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    sireg.append(ddm.ddsGetCoreBcc(0x8))
    print("si_reg {} : {}".format(137, hex(sireg[7])))
    #discard
    ddm.ddsDisCoreBcc()
    #optional purge
    ddm.ddsPurCoreBcc()

    # now calculate
    hs_div = ((sireg[0] & 0xE0) >> 5) + 4
    n1 = (((sireg[0] & 0x1F) << 2) | ((sireg[1] & 0xC0) >> 6)) + 1
    rfreq = ((sireg[1] & 0x3F) << 32) | (sireg[2] << 24) | (sireg[3] << 16) | (sireg[4] << 8) | sireg[5]
    rst_bit = (sireg[6] & 0x80) >> 7
    nf_bit = (sireg[6] & 0x40) >> 6
    fm_bit = (sireg[6] & 0x20) >> 5
    vcadc_bit = (sireg[6] & 0x10) >> 4
    recall_bit = sireg[6] & 0x1
    fdco_bit = (sireg[7] & 0x10) >> 4

    # now display
    if dis == True:
        print("++++++++++++++++++++++++++++")
        print("===> HS_DIV       = {}".format(hex(hs_div)))
        print("===> N1           = {}".format(hex(n1)))
        print("===> RFREQ        = {}".format(hex(rfreq)))
        print("===> RST_REG      = {}".format(rst_bit))
        print("===> NewFreq      = {}".format(nf_bit))
        print("===> FreezeM      = {}".format(fm_bit))
        print("===> FreezeVCADC  = {}".format(vcadc_bit))
        print("===> RECALL       = {}".format(recall_bit))
        print("===> FreezeDCO    = {}".format(fdco_bit))
        print("++++++++++++++++++++++++++++")

    return [hs_div, n1, rfreq, fm_bit, vcadc_bit]
# ------------------------------------------------------
def si_recall(buss):
# ------------------------------------------------------
    """Recall Si571 defaults"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    print("Set Si571 Recall")
    #Recall Si571
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 135)
    #Recall is 0x1, auto cleared
    dd.ddsSetCoreBcc(0x14, 0x11)
    dd.ddsSetCoreBcc(0x0, 0x4000010)

# ------------------------------------------------------
def si571_dis(args):
# ------------------------------------------------------
    """Display Si571 parameters & status"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    opt = get_v(1, args)
    
    if opt == "-f":
        flu = 8 * si_freq(bus)
        print("--------> Si571 frequency = {}".format(flu))
    elif opt == "-r":
        si_dis(bus, True)
    else:
        print("!!!!!!!!!!! Bad option")

# ------------------------------------------------------
def si571_set(args):
# ------------------------------------------------------
    """Set Si571 frequency registers HSDIV, N1, RFREQ and freeze bits"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    opti = get_v(1, args)
    valu = get_v(2, args)
    val0 = False
    if (valu == 0) or (valu == -1):
        val0 = True
        valu = 0
        print("***Warning : no value or value = 0")

    if (opti == "-hs") and (val0 == False):
        hs_div = si_dis(bus, False)[0]
        print("HS_DIV = {} ; write {}".format(hs_div, opti))
        si_hsdiv(bus, valu)
    elif (opti == "-n1") and (val0 == False):
        n1 = si_dis(bus, False)[1]
        print("N1 = {} ; write {}".format(n1, opti))
        si_n1(bus, valu)
    elif (opti == "-rf") and (val0 == False):
        rfreq = si_dis(bus, False)[2]
        print("RFREQ = {} ; write {}".format(rfreq, opti))
        si_rfreq(bus, valu)
    elif opti == "-u":
        if valu == 1:
            print("Freeze VC control")
        else:
            print("Unfreeze VC control")        
        si_freeze(bus, valu)
    elif opti == "-r":
        print("Recall Si571 default setup")
        si_recall(bus)

# ------------------------------------------------------
def si_n1(buss, valeur):
# ------------------------------------------------------
    """Set Si571 VCADC freeze bit"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    #Read register#7
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 0 for read then address
    ddm.ddsSetCoreBcc(0xc, 0)
    ddm.ddsSetCoreBcc(0x10, 7)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    val7 = ddm.ddsGetCoreBcc(0x8)
    #discard
    ddm.ddsDisCoreBcc()
    #optional purge
    ddm.ddsPurCoreBcc()
    #Read register#8
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 0 for read then address
    ddm.ddsSetCoreBcc(0xc, 0)
    ddm.ddsSetCoreBcc(0x10, 8)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    val8 = ddm.ddsGetCoreBcc(0x8)
    #discard
    ddm.ddsDisCoreBcc()
    #optional purge
    ddm.ddsPurCoreBcc()

    new7 = (val7 & 0xE0) | ((valeur & 0x7C) >> 2)
    new8 = (val8 & ~0xC0) | ((valeur & 0x3) << 6)

    #Now rewrite register#7
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write then address and data
    ddm.ddsSetCoreBcc(0xc, 1)
    ddm.ddsSetCoreBcc(0x10, 7)
    dd.ddsSetCoreBcc(0x14, new7)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    #Now rewrite register#8
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write then address and data
    ddm.ddsSetCoreBcc(0xc, 1)
    ddm.ddsSetCoreBcc(0x10, 8)
    dd.ddsSetCoreBcc(0x14, new8)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)

# ------------------------------------------------------
def si_hsdiv(buss, valeur):
# ------------------------------------------------------
    """Set Si571 VCADC freeze bit"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    #Read register#7 first
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 0 for read then address
    ddm.ddsSetCoreBcc(0xc, 0)
    ddm.ddsSetCoreBcc(0x10, 7)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    vallu = ddm.ddsGetCoreBcc(0x8)
    #discard
    ddm.ddsDisCoreBcc()
    #optional purge
    ddm.ddsPurCoreBcc()

    newhs = (vallu & ~0xE0) | ((valeur & 0xE0) << 5)

    #Now rewrite register#7
    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write then address and data
    ddm.ddsSetCoreBcc(0xc, 1)
    ddm.ddsSetCoreBcc(0x10, 7)
    dd.ddsSetCoreBcc(0x14, newhs)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)

# ------------------------------------------------------
def si_freeze(buss, valeur):
# ------------------------------------------------------
    """Set Si571 VCADC freeze bit"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    print("Set Si571 Recall")
    #Recall Si571
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 135)
    #VCADC is 0x10, other bits all 0
    dd.ddsSetCoreBcc(0x14, ((valeur & 0x1)<<4))
    dd.ddsSetCoreBcc(0x0, 0x4000010)
        
# ------------------------------------------------------
def si_fset(buss, valo, n1):
# ------------------------------------------------------
    """Set Si571 RFREQ register"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    print("writing new RFREQ {}".format(hex(valo)))

    # freeze DCO
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 137)
    dd.ddsSetCoreBcc(0x14, 0x10)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)

    # write new RFREQ
    regw = []
    regw.append(((valo >> 32) & 0x3F) | ((n1 & 0x3) << 6))
    print("r8={}".format(hex(regw[0])))
    regw.append((valo >> 24) & 0xFF)
    regw.append((valo >> 16) & 0xFF)
    regw.append((valo >> 8) & 0xFF )
    regw.append(valo & 0xFF)
    # RFREQNEW on registers 8-12
    for chacha in range(8, 13):
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
        # 1 for write, then address and data
        dd.ddsSetCoreBcc(0xc, 1)
        dd.ddsSetCoreBcc(0x10, 20-chacha)
        dd.ddsSetCoreBcc(0x14, regw[20-chacha-8])
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        time.sleep(0.01)
        print("r{}={}".format((20-chacha), hex(regw[20-chacha-8])))

    # unfreeze DCO
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 137)
    dd.ddsSetCoreBcc(0x14, 0x0)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    
    # Assert NewFreq bit
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 135)
    dd.ddsSetCoreBcc(0x14, 0x40)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
           
    print("New frequency set...")

# ------------------------------------------------------
def si_rfreq(buss, valo):
# ------------------------------------------------------
    """Set Si571 RFREQ register"""
    
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    print("writing new RFREQ {}".format(hex(valo)))
    #read N1 + rfreq MSBs
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 0 for read then address
    dd.ddsSetCoreBcc(0xc, 0)
    dd.ddsSetCoreBcc(0x10, 0x8) #register#8
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    time.sleep(0.01)
    #save N1
    cnr = dd.ddsGetCoreBcc(0x8)
    n1 = (cnr & 0xC0) >> 6
    print("n1={}".format(n1))
    #discard
    dd.ddsDisCoreBcc()
    #optional purge
    dd.ddsPurCoreBcc()

    #freeze DCO
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 137)
    dd.ddsSetCoreBcc(0x14, 0x10)
    dd.ddsSetCoreBcc(0x0, 0x4000010)

    # write new RFREQ
    regw = []
    regw.append(((valo >> 32) & 0x3F) | ((n1 & 0x3) << 6))
    print("r8={}".format(regw[0]))
    regw.append((valo >> 24) & 0xFF)
    regw.append((valo >> 16) & 0xFF)
    regw.append((valo >> 8) & 0xFF )
    regw.append(valo & 0xFF)
    # RFREQNEW on registers 8-12
    for chacha in range(8, 13):
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
        # 1 for write, then address and data
        dd.ddsSetCoreBcc(0xc, 1)
        dd.ddsSetCoreBcc(0x10, chacha)
        dd.ddsSetCoreBcc(0x14, regw[chacha-8])
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        time.sleep(0.01)

    #unfreeze DCO
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 137)
    dd.ddsSetCoreBcc(0x14, 0x0)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
    
    #Assert NewFreq bit
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_571)
    # 1 for write, then address and data
    dd.ddsSetCoreBcc(0xc, 1)
    dd.ddsSetCoreBcc(0x10, 135)
    dd.ddsSetCoreBcc(0x14, 0x40)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
           
    print("New RFREQ done...")

# ------------------------------------------------------
def dac(args):
# ------------------------------------------------------
    """load / read DAC"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    rwr = get_v(1, args)
    cha = get_v(2, args)
    valw = get_v(3, args)

    if rwr == "-r":
        rwb = 0
    elif rwr == "-w":
        rwb = 1
    else:
        print("!!!!!!-----> -r / -w")
        return False

    dacLoader(bus, rwb, cha, valw)

# ------------------------------------------------------
def dacLoader(buss, rww, chh, valw):
# ------------------------------------------------------
    """load / read DAC"""

    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    if rww == 1:
        # write
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_DAC)
        dd.ddsSetCoreBcc(0xc, rww)
        dd.ddsSetCoreBcc(0x10, chh)
        dd.ddsSetCoreBcc(0x14, valw)
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        print("set dac_chan {} : {}".format(chh, hex(valw)))
    else:
        # read
        for chacha in range(8):
            dd.ddsSetCoreBcc(0x0, 0x1000000)
            dd.ddsSetCoreBcc(0x8, BCC_CMD_DAC)
            dd.ddsSetCoreBcc(0xc, rww)
            dd.ddsSetCoreBcc(0x10, chacha)
            dd.ddsSetCoreBcc(0x0, 0x4000010)
            time.sleep(0.01)
            vallu = dd.ddsGetCoreBcc(0x8)
            print("dac_chan {} : {}".format(chacha, hex(vallu)))
            #discard
            dd.ddsDisCoreBcc()
            #optional purge
            dd.ddsPurCoreBcc()

# ------------------------------------------------------
def ioc(args):
# ------------------------------------------------------
    """set ioconf bitfields"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    chx = get_v(1, args)
    vala = get_v(2, args)
    if vala != 1: vala = 0
    print("chx is {}".format(chx))

    if chx == "-ttlo":
        lu72 = rd_i2c(ipp, i2c_72)
        val72 = (lu72 & 0xFB) | ((vala & 0x1)<<2)
        wr_i2c(ipp, i2c_72, val72)
        print("TTLOUT enable")
    elif chx == "-fplo":
        lu74 = rd_i2c(ipp, i2c_74)
        val74 = (lu74 & 0xEF) | ((vala & 0x1)<<4)
        wr_i2c(ipp, i2c_74, val74)
        print("Font Pwnel LEDs enable")
    elif chx == "-ck10":
        lu76 = rd_i2c(ipp, i2c_76)
        val76 = (lu76 & 0xBF) | ((vala & 0x1)<<6)
        wr_i2c(ipp, i2c_76, val76)
        print("CK10 enable = {}".format(vala))
    elif chx == "-clko":
        lu70 = rd_i2c(ipp, i2c_70)
        val70 = (lu70 & 0xFE) | vala & 0x1
        wr_i2c(ipp, i2c_70, val70)
        print("RF clocks out enable")
    elif chx == "-vcxo":
        lu76 = rd_i2c(ipp, i2c_76)
        val76 = (lu76 & 0x7F) | ((vala & 0x1)<<7)
        wr_i2c(ipp, i2c_76, val76)
        if vala == 1:
            print("VCXO enabled : Slave mode.")
        else:
            print("VCXO disabled : Master mode.")
    else:
        return False

# ------------------------------------------------------
def rfdel(args):
# ------------------------------------------------------
    """ramp i2c fine delays"""
    bus = get_v(0, args)
    cha = get_v(1, args)
    sta = get_v(2, args)
    sto = get_v(3, args)
    dodo = get_v(4, args)

    if (dodo == 0) or (dodo == -1) : dodo = 1

    feuvert = True
# main loop
    ilo = sta
    while feuvert:
        setfdel(bus, cha, ilo)
        print("set fdel {}".format(ilo))

        try:
            time.sleep(dodo)
        except KeyboardInterrupt:
            return
        ilo += 10
        if (ilo >= sto): ilo = sta
        
# ------------------------------------------------------
def fdel(args):
# ------------------------------------------------------
    """set i2c fine delays"""

    bus = get_v(0, args)
    cha = get_v(1, args)
    vala = get_v(2, args) & 0x1ff
    retour = setfdel(bus, cha, vala)
    #print("fdel retour {}".format(retour))

# ------------------------------------------------------
def setfdel(buss, chan, vala):
# ------------------------------------------------------
    """set i2c fine delays"""
    #print("buss {}".format(buss))
    ipp = bus_lup(str(buss))
    #print("ipp {}".format(ipp))
    if ipp == '0':
        return False

    if chan == 1:
        lu70 = rd_i2c(ipp, i2c_70)
        lu72 = rd_i2c(ipp, i2c_72)
        valu = (lu70>>1) | (lu72&0x3)<<7
        #print("70 {}, 72 {}, delay is {}".format(hex(lu70), hex(lu72), hex(valu)))
        val70 = ((vala&0x7f)<<1) | (lu70&0x1)
        val72 = ((vala&0x180)>>7) | (lu72&0xfc)
        #print("70 {}, 72 {}, delay is {}".format(hex(val70), hex(val72), hex(vala)))
        wr_i2c(ipp, i2c_70, val70)
        wr_i2c(ipp, i2c_72, val72)
        return True
    elif chan == 2:
        lu72 = rd_i2c(ipp, i2c_72)
        lu74 = rd_i2c(ipp, i2c_74)
        valu = (lu72>>3) | (lu74&0xf)<<5
        #print("72 {}, 74 {}, delay is {}".format(hex(lu72), hex(lu74), hex(valu)))
        val72 = ((vala&0x1f)<<3) | (lu72&0x7)
        val74 = ((vala&0x1e0)>>5) | (lu74&0xf0)
        #print("72 {}, 74 {}, delay is {}".format(hex(val72), hex(val74), hex(vala)))
        wr_i2c(ipp, i2c_72, val72)
        wr_i2c(ipp, i2c_74, val74)
        return True
    elif chan == 3:
        lu74 = rd_i2c(ipp, i2c_74)
        lu76 = rd_i2c(ipp, i2c_76)
        valu = (lu74>>5) | (lu76&0x3f)<<3
        #print("74 {}, 76 {}, delay is {}".format(hex(lu74), hex(lu76), hex(valu)))
        val74 = ((vala&0x7)<<5) | (lu74&0x1f)
        val76 = ((vala&0x1f8)>>3) | (lu76&0xc0)
        #print("74 {}, 76 {}, delay is {}".format(hex(val74), hex(val76), hex(vala)))
        wr_i2c(ipp, i2c_74, val74)
        wr_i2c(ipp, i2c_76, val76)
        return True
    else:
        return False

# ------------------------------------------------------
def sdel(args):
# ------------------------------------------------------
    """fine delay shift"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    cha = get_v(1, args)
    vala = get_v(2, args) & 0x1ff
    valb = get_v(3, args) & 0x1ff
    valp = get_v(4, args) & 0x1ff

    if valb < vala:
        toto = vala
        vala = valb
        valb = toto
    print("================================range {}".format(valb-vala))

    feuvert = True
# main loop

    valx = vala
    while feuvert:
            valx = valx + valp
            print("------------------------------delay {}".format(valx))
            if cha == 1:
                lu70 = rd_i2c(ipp, i2c_70)
                lu72 = rd_i2c(ipp, i2c_72)
                valu = (lu70>>1) | (lu72&0x3)<<7
                print("70 {}, 72 {}, delay is {}".format(hex(lu70), hex(lu72), hex(valu)))
                val70 = ((valx&0x7f)<<1) | (lu70&0x1)
                val72 = ((valx&0x180)>>7) | (lu72&0xfc)
                print("70 {}, 72 {}, delay is {}".format(hex(val70), hex(val72), hex(valx)))
                wr_i2c(ipp, i2c_70, val70)
                wr_i2c(ipp, i2c_72, val72)
            elif cha == "2":
                lu72 = rd_i2c(ipp, i2c_72)
                lu74 = rd_i2c(ipp, i2c_74)
                valu = (lu72>>3) | (lu74&0xf)<<5
                print("72 {}, 74 {}, delay is {}".format(hex(lu72), hex(lu74), hex(valu)))
                val72 = ((valx&0x1f)<<3) | (lu72&0x3)
                val74 = ((valx&0x1e0)>>5) | (lu74&0xf0)
                print("72 {}, 74 {}, delay is {}".format(hex(val72), hex(val74), hex(valx)))
                wr_i2c(ipp, i2c_72, val72)
                wr_i2c(ipp, i2c_74, val74)
            elif cha == "3":
                lu74 = rd_i2c(ipp, i2c_74)
                lu76 = rd_i2c(ipp, i2c_76)
                valu = (lu74>>5) | (lu76&0x3f)<<3
                print("74 {}, 76 {}, delay is {}".format(hex(lu74), hex(lu76), hex(valu)))
                val74 = ((valx&0x7)<<5) | (lu74&0x1f)
                val76 = ((valx&0x1f8)>>3) | (lu76&0xc0)
                print("74 {}, 76 {}, delay is {}".format(hex(val74), hex(val76), hex(valx)))
                wr_i2c(ipp, i2c_74, val74)
                wr_i2c(ipp, i2c_76, val76)

            try:
                time.sleep(2)
            except KeyboardInterrupt:
                feuvert = False
                return
            if ((valx + valp) >= valb): valx = vala

# ------------------------------------------------------
def wr_i2c(ipp, add, valw):
# ------------------------------------------------------
    """write i2c device"""

    dd = SpecWhist(ipp)

#request
    dd.ddsSetCoreBcc(0x0, 0x1000000)
#set control command @ 0x24008 : 0x6
    dd.ddsSetCoreBcc(0x8, BCC_CMD_IOCONF)
#0x2400c 0x0
    dd.ddsSetCoreBcc(0xc, add)
#set write @ 0x24010 r/w=1
    dd.ddsSetCoreBcc(0x10, 1)
#value to write
    dd.ddsSetCoreBcc(0x14, valw)
#send request @ 0x24000 : 0x4000010
    dd.ddsSetCoreBcc(0x0, 0x4000010)

# ------------------------------------------------------
def rd_i2c(ipp, add):
# ------------------------------------------------------
    """read i2c device"""

    dd = SpecWhist(ipp)

#request
    dd.ddsSetCoreBcc(0x0, 0x1000000)
#set control command @ 0x24008 : 0x6
    dd.ddsSetCoreBcc(0x8, BCC_CMD_IOCONF)
# 0x2400c 0x0
    dd.ddsSetCoreBcc(0xc, add)
# set read @ 0x24010 r/w=0
    dd.ddsSetCoreBcc(0x10, 0)
#send request @ 0x24000 : 0x4000010
    dd.ddsSetCoreBcc(0x0, 0x4000010)
#read data
    time.sleep(0.01)
    vallu = dd.ddsGetCoreBcc(0x8)
#    print("@ {} : {}".format(hex(add), hex(vallu)))
#discard
    dd.ddsDisCoreBcc()
#optional purge
    dd.ddsPurCoreBcc()

    return vallu

# ------------------------------------------------------
def purbcc(args):
# ------------------------------------------------------
    """purge bcc HMQOUT"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

#optional purge
    dd.ddsPurCoreBcc()

# ------------------------------------------------------
def setin(args):
# ------------------------------------------------------
    """set trigger inputs : channel enable polarity threshold"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False
    cha = get_v(1, args)
    if not 0 <= cha < 4:
        print("Bad channel number")
        return False

    dd = SpecWhist(ipp)

    ena = get_v(2, args)
    if ena != 1 :
        dd.ddsClearInEn(cha)
    else:
        dd.ddsSetInEn(cha)
        print("Stamp_reg = {}".format(dd.ddsSetInEn(cha)))
        
    pola = get_v(3, args)
    if pola != 1 :
        dd.ddsClearInPol(cha)
    else:
        dd.ddsSetInPol(cha)

    daca = get_v(4, args)
    if not 0 <= daca <= 0x80:
        print("No threshold change")
    else:
        # write
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_DAC)
        dd.ddsSetCoreBcc(0xc, 1)          #write
        dd.ddsSetCoreBcc(0x10, (7-2*cha)) #dac reverse cabling...
        dd.ddsSetCoreBcc(0x14, daca)
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        print("set dac_chan {} : {}".format(cha, hex(daca)))

# ------------------------------------------------------
def iord(args):
# ------------------------------------------------------
    """read i2c status"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    vali2c0 = rd_i2c(ipp, i2c_70)
    vali2c1 = rd_i2c(ipp, i2c_72)
    vali2c2 = rd_i2c(ipp, i2c_74)
    vali2c3 = rd_i2c(ipp, i2c_76)
    print("i2c_0 = {}".format(hex(vali2c0)))
    print("i2c_1 = {}".format(hex(vali2c1)))
    print("i2c_2 = {}".format(hex(vali2c2)))
    print("i2c_3 = {}".format(hex(vali2c3)))

    clkout_en = vali2c0 & 0x01
    ttlout_en = (vali2c1 & 0x04)>>2
    print("ttlout_en     = {}".format(ttlout_en))
    clk10_en = (vali2c3 & 0x40)>>6
    vcxo_en = (vali2c3 & 0x80)>>7
    fpleds_en = (vali2c2 & 0x10)>>4
    print("clk_out_en    = {}".format(clkout_en))
    print("clk10_en      = {}".format(clk10_en))
    print("vcxo_en       = {}".format(vcxo_en))
    print("fpleds_en     = {}".format(fpleds_en))
    print("--------")
    delay_1 = ((vali2c0 & 0xfe) >> 1) | (vali2c1 & 0x3)<<7
    print("CH#1 Fine Delay = {}".format(delay_1))
    # PLL6 params for ch2
    pll38 = fad95(bus, D3S_CMD_9510, 0, 0x38, 0)
    if ((pll38 & 0x01) >> 1) == 1: byp = "bypassed"
    else: byp =  "not bypassed"
    print("--------")
    print("Ch#2 PLL delay {}".format(byp))
    pll39 = fad95(bus, D3S_CMD_9510, 0, 0x39, 0)
    iramp = 200 + 200*(pll39 & 0x7)
    ncap = caplut((pll39 & 0x38) >> 3)
    print("Ch#2 Iramp      = {}".format(iramp))
    print("Ch#2 NbCaps     = {}".format(ncap))
    pll3a = fad95(bus, D3S_CMD_9510, 0, 0x3a, 0)
    print("Ch#2 PLLDel     = {}".format((pll3a & 0x3e) >> 1))
    delay_2 = ((vali2c1 & 0xf8) >> 3) | (vali2c2 & 0xf)<<5
    print("Ch#2 Fine Delay = {}".format(delay_2))
    # PLL5 params for ch3
    pll34 = fad95(bus, D3S_CMD_9510, 0, 0x34, 0)
    if ((pll34 & 0x01) >> 1) == 1: byp = "bypassed"
    else: byp =  "not bypassed"
    print("--------")
    print("Ch#3 PLL delay {}".format(byp))
    pll35 = fad95(bus, D3S_CMD_9510, 0, 0x35, 0)
    iramp = 200 + 200*(pll35 & 0x7)
    ncap = caplut((pll35 & 0x38) >> 3)
    print("Ch#3 Iramp      = {}".format(iramp))
    print("Ch#3 NbCaps     = {}".format(ncap))
    pll36 = fad95(bus, D3S_CMD_9510, 0, 0x36, 0)
    print("Ch#3 PLLDel     = {}".format((pll36 & 0x3e) >> 1))
    delay_3 = ((vali2c2 & 0xe0) >> 5) | (vali2c3 & 0x3f)<<3
    print("Ch#3 Fine Delay = {}".format(delay_3))

def caplut(ii):
    if ii == 0: return 4
    elif ii == 1: return 3
    elif ii == 2: return 3
    elif ii == 3: return 2
    elif ii == 4: return 3
    elif ii == 5: return 2
    elif ii == 6: return 2
    elif ii == 7: return 1
    
# ------------------------------------------------------
def msconf(args):
# ------------------------------------------------------
    """configure master or slave"""
    msset(args)
    
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    msgot = get_v(1, args)

    if msgot == "-m":
        print("Setting Fine Delay synchro RF source to : front panel input")
        lu76 = rd_i2c(ipp, i2c_76)
        val76 = (lu76 & 0x7F) | ((1 & 0x1)<<7)
        wr_i2c(ipp, i2c_76, val76)
    elif msgot == "-s":
        #wait some time for RFoE locked then set PLL
        succes = 0
        dd = SpecWhist(ipp)
        print("Wait for RFoE locked")
        for ttt in range(10):
            time.sleep(2)
            if dd.ddsTestRfoe()[0] == 1:
                print("====> RFoE locked - PLL enabled")
                succes = 1
                break
        if succes == 0:
            print("!!! RFoE remains unlocked after 20 s !!!!")
            

# ------------------------------------------------------
def msset(args):
# ------------------------------------------------------
    """configure master or slave"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    msgot = get_v(1, args)

    dd = SpecWhist(ipp)

#restart wrnc/CPU0 dds
    dd.ddsLm32Restart(0)
#restart wrnc/CPU1 bcc
    dd.ddsLm32Restart(1)

    if msgot == "-m":
        print("configuring #{} as master".format(ipp))
        #CoreDDS actions 
        dd.ddsSetCoreDds(0x0, 0x1000000)
        #set control command
        dd.ddsSetCoreDds(0x8, 0x5)
        dd.ddsSetCoreDds(0xc, 0x0)
        #set master mode
        dd.ddsSetCoreDds(0x10, 0x1)
        #set event id
        dd.ddsSetCoreDds(0x14, 0x8f)
        #set tune RF center freq
        dd.ddsSetCoreDds(0x18, 0x2d1)
#        dd.ddsSetCoreDds(0x1c, 0x4f483caf) # 352202000
#        dd.ddsSetCoreDds(0x1c, 0xa87e750c) # 352372158
        dd.ddsSetCoreDds(0x1c, 0xa7f83d4f) # 352371158
        #send data
        dd.ddsSetCoreDds(0x0, 0x4000010)
        #Purge
        time.sleep(2)
        dd.ddsPurCoreDds()

        #Now CoreBcc actions : set master mode
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #set command to mode
        dd.ddsSetCoreBcc(0x8, BCC_CMD_MODE)
        #set mode to master
        dd.ddsSetCoreBcc(0xc, 0x1)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)

        #Now CoreBcc actions : disable SEQ_EN
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #set command to T0 mode
        dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_SEQ_EN)
        #write
        dd.ddsSetCoreBcc(0xc, 1)
        #reset SEQ_EN
        dd.ddsSetCoreBcc(0x10, 0)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)

        #Now CoreBcc actions : enable rt-bcc LM32
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #enable rt-bcc (wait for event)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_ENABLE)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        
    elif msgot == "-s":
        print("configuring #{} as slave".format(ipp))
        #CoreDDS actions
        dd.ddsSetCoreDds(0x0, 0x1000000)
        #set control command
        dd.ddsSetCoreDds(0x8, 0x5)
        dd.ddsSetCoreDds(0xc, 0x0)
        #set slave mode
        dd.ddsSetCoreDds(0x10, 0x2)
        #set event id
        dd.ddsSetCoreDds(0x14, 0x8f)
        #set tune RF center freq
        dd.ddsSetCoreDds(0x18, 0x0)
        dd.ddsSetCoreDds(0x1c, 0x0)
        #send data
        dd.ddsSetCoreDds(0x0, 0x4000010)
        #Purge
        time.sleep(2)
        dd.ddsPurCoreDds()

        #Now CoreBcc actions : set slave mode
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #set command to mode
        dd.ddsSetCoreBcc(0x8, BCC_CMD_MODE)
        #set mode to slave
        dd.ddsSetCoreBcc(0xc, 0x2)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)

        #Now CoreBcc actions : enable rt-bcc LM32
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #enable rt-bcc (wait for event)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_ENABLE)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()

        fpgaver = dd.ddsGetFPGA()
        #deal with CoreEvt for newer versions, else for 78 nothing
        if fpgaver != 78:
            #CoreEvt actions : enable CoreEvt, skip mode slave by default
            dd.ddsSetCoreEvt(0x0, 0x1000000)
            #set command to enable
            dd.ddsSetCoreEvt(0x8, EVT_CMD_ENABLE)
            #set mode to master
            dd.ddsSetCoreEvt(0xc, 0x1)
            #send data
            dd.ddsSetCoreEvt(0x0, 0x4000010)
        
# ------------------------------------------------------
def mstart(args):
# ------------------------------------------------------
    """Network wide start/resynchro to be sent to master"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    #request for sending data
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    #set master start id
    dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_START)
    #send data
    dd.ddsSetCoreBcc(0x0, 0x4000010)
#discard
    dd.ddsDisCoreBcc()
#optional purge
    dd.ddsPurCoreBcc()

# ------------------------------------------------------
def slok(args):
# ------------------------------------------------------
    """trigger lock slave_rf_counter to master_rf_counter"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_SLOK)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
#discard
    dd.ddsDisCoreBcc()
#optional purge
    dd.ddsPurCoreBcc()

# ------------------------------------------------------
def ppsmon(args):
# ------------------------------------------------------
    """trigger PPS monitoring"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_PPS_MON)
    dd.ddsSetCoreBcc(0x0, 0x4000010)
#discard
    dd.ddsDisCoreBcc()
#optional purge
    dd.ddsPurCoreBcc()

# ------------------------------------------------------
def clr_fl(buss):
# ------------------------------------------------------
    """Clear flashed_bit and set Si571 to working frequency"""
    ipp = bus_lup(str(buss))
    
    dd = SpecWhist(ipp)
    fla = dd.ddsTestFlashed()[0]
    if fla == 1:
        print("Clearing flashed BIT")
        dd.ddsClearFlashed()
    return fla

# ------------------------------------------------------
def stup(args):
# ------------------------------------------------------
    """Startup module : clear flashed bit, set Si571 frequency"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    rrr = True
    factor = get_v(1, args)

    bout = clr_fl(bus)
    #always recall first to be sure starting from factory
    si_recall(bus)

    time.sleep(1)
    siregs = si_dis(bus, False)
    hsdiv = siregs[0]
    n1 = siregs[1]
    rfreq = siregs[2]
    print("factory RFREQ = {}".format(hex(rfreq)))
    fxtal = (SI571F * n1 * hsdiv * pow(2, 28)) / rfreq
    print("Fxtal = {}".format(fxtal))

    # now set AD9510 outputs accordingly
    if factor == "-x2":
        # B divider : 0x06 set to 8
        fad95(bus, D3S_CMD_9510, 1, 0x06, 0x08)
        # OUT#0 rfx2 : 0x48 = 0x0 = div 2 -> RF
        fad95(bus, D3S_CMD_9510, 1, 0x48, 0x0)
        # OUT#0 rfx2 : 0x49 = 0x0 unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x49, 0x0)
        # OUT#1 rfx2 : 0x4a = divide by 16 (7+1)x2 -> RF/8
        fad95(bus, D3S_CMD_9510, 1, 0x4a, 0x77)
        # OUT#1 rfx2 : 0x4b =  0x0 unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x4b, 0x0)
        # OUT#2 rfx2 : 0x4c = div 4 (1+1)x2 -> RF/2
        fad95(bus, D3S_CMD_9510, 1, 0x4c, 0x11)
        # OUT#2 rfx2 : 0x4d = 0x0 unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x4d, 0x0)
        # OUT#3 off
        # OUT#4 rfx2 : 0x50 = div 16 (7+1)x2 -> RF/8
        fad95(bus, D3S_CMD_9510, 1, 0x50, 0x77)
        # OUT#4 rfx2 : 0x51 = 0x0 unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x51, 0x0)
        # OUT#5 rfx2 : 0x52 = no change 0x0 = div 2 -> RF
        fad95(bus, D3S_CMD_9510, 1, 0x52, 0x0)
        # OUT#5 rfx2 : 0x53 = unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x53, 0x0)
        # OUT#6 rfx2 : 0x54 = no change 0x0 = div 2 -> RF
        fad95(bus, D3S_CMD_9510, 1, 0x54, 0x0)
        # OUT#6 rfx2 : 0x55 = unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x55, 0x0)
        # OUT#7 rfx2 : 0x56 = no change 0x0 = div 2 -> RF
        fad95(bus, D3S_CMD_9510, 1, 0x56, 0x0)
        # OUT#7 rfx2 : 0x57 = unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x57, 0x0)
        # FUNCTION PIN to SYNCB : [6..5]=01b -> 0x20
        fad95(bus, D3S_CMD_9510, 1, 0x58, 0x20)
        # update regs 0x5a=1
        fad95(bus, D3S_CMD_9510, 1, 0x5a, 0x1)
    else:
        # B divider : 0x06 set to 4
        fad95(bus, D3S_CMD_9510, 1, 0x06, 0x04)
        # OUT#0 rfx2 : 0x48 = 0x0 = div 2 default
        fad95(bus, D3S_CMD_9510, 1, 0x48, 0x0)
        # OUT#0 rfx2 : 0x49 = 0x80 bypass divider, phase=0 -> RF
        fad95(bus, D3S_CMD_9510, 1, 0x49, 0x80)
        # OUT#1 rfx2 : 0x4a = divide by 8 (3+1)x2 -> RF/8
        fad95(bus, D3S_CMD_9510, 1, 0x4a, 0x33)
        # OUT#1 rfx2 : 0x4b =  0x0 unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x4b, 0x0)
        # OUT#2 rfx2 : 0x4c = div 2 (0+1)x2 -> RF/2
        fad95(bus, D3S_CMD_9510, 1, 0x4c, 0x0)
        # OUT#2 rfx2 : 0x4d = 0x0 unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x4d, 0x0)
        # OUT#3 off
        # OUT#4 rfx2 : 0x50 = div 8 (3+1)x2 -> RF/8
        fad95(bus, D3S_CMD_9510, 1, 0x50, 0x33)
        # OUT#4 rfx2 : 0x51 = 0x0 unbypass divider, phase=0
        fad95(bus, D3S_CMD_9510, 1, 0x51, 0x0)
        # OUT#5 rfx2 : 0x52 = no change 0x0 = div 2 default
        fad95(bus, D3S_CMD_9510, 1, 0x52, 0x0)
        # OUT#5 rfx2 : 0x53 = 0x80 bypass divider, phase=0 -> RF
        fad95(bus, D3S_CMD_9510, 1, 0x53, 0x80)
        # OUT#6 rfx2 : 0x54 = no change 0x0 = div 2 default
        fad95(bus, D3S_CMD_9510, 1, 0x54, 0x0)
        # OUT#6 rfx2 : 0x55 = 0x80 bypass divider, phase=0 -> RF
        fad95(bus, D3S_CMD_9510, 1, 0x55, 0x80)
        # OUT#7 rfx2 : 0x56 = no change 0x0 = div 2 default
        fad95(bus, D3S_CMD_9510, 1, 0x56, 0x0)
        # OUT#7 rfx2 : 0x57 = 0x80 bypass divider, phase=0 -> RF
        fad95(bus, D3S_CMD_9510, 1, 0x57, 0x80)
        # update regs 0x5a=1
        fad95(bus, D3S_CMD_9510, 1, 0x5a, 0x1)

    # then set esrf frequency
    if factor == "-x2":
        print("Setting RFx2, n1=0")
        rfreqnew = int(rfreq * ESRFRF / SI571F)
        print("writing rfx2 RFREQ {}".format(hex(rfreqnew)))
        si_fset(bus, rfreqnew, 0)
    else:
        print("Resetting RF, n1=1")
        rfreqnew = int(rfreq * ESRFRF / SI571F)
        print("writing rfx1 RFREQ {}".format(hex(rfreqnew)))
        si_fset(bus, rfreqnew, 1)

    return rrr

# ------------------------------------------------------
def t0wd(args):
# ------------------------------------------------------
    """T0 Watch Dog menu : board_bus_address dis/enable (trig level)"""

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False
    dd = SpecWhist(ipp)

    ben = get_v(1, args)
    lev = get_v(2, args)

    # if lev missing do nothing
    if lev != -1:
        dd.ddsSetT0WdTrig(lev)
    else:
        print("No Watch Dog time constant change")
    # set enable bit
    if (ben == 0) or (ben == -1):
        dd.ddsClearT0WdEn()
    else:
        dd.ddsSetT0WdEn()

# ------------------------------------------------------
def dbg_stat(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    # start with ad9510 bits
    # read Digital Lock Detect enable Bit
    d_9510 = fad95(bus, D3S_CMD_9510, 0, 0xd, 0)
    dldbit = (d_9510 & 0x40) >> 6

    # now set SpecWhist
    dd = SpecWhist(ipp)

    toto = get_v(1, args)
    if toto == 1:
        rfoe_unlck_clr = 1
    else:
        rfoe_unlck_clr = 0
    # read RFoE lost Bit
    #request
    dd.ddsSetCoreDds(0x0, 0x1000000)
    #set control command 0xc for rfoe
    dd.ddsSetCoreDds(0x8, D3S_CMD_RFOE)
    #dummy
    dd.ddsSetCoreDds(0xc, 0)
    #set r=0/w=1
    dd.ddsSetCoreDds(0x10, rfoe_unlck_clr)
    #send request  0x4000010 r or w
    dd.ddsSetCoreDds(0x0, 0x4000010)
    if rfoe_unlck_clr == 0:
        #read data
        time.sleep(0.01)
        vallu = dd.ddsGetCoreDds(0x10)
        #discard
        dd.ddsDisCoreDds()
        #optional purge
        dd.ddsPurCoreDds()
        print("\nRFoE unlocked bit = {}\n".format(vallu))
    else:
        print("\nRFoE unlocked bit is cleared\n")

    dbgen = get_v(2, args)
    adjen = get_v(3, args)
    sto = get_v(4, args)
    print("dbgen {} ; adjen {} ; sto {}".format(dbgen, adjen, sto))

    if dbgen == 0:
        dd.ddsClearRfDbgEn()
    elif dbgen == 1:
        dd.ddsSetRfDbgEn()

    if adjen == 0:
        dd.ddsClearRfAdjEn()
    elif adjen == 1:
        dd.ddsSetRfAdjEn()

    if sto == 0:
        dd.ddsClearRfAdjStop()
    elif sto == 1:
        dd.ddsSetRfAdjStop()

    for cc in range(8):
        print("!!{}!!".format(bus), end="")
    print()

    msbit = dd.ddsTestSlave()[0]
    if msbit == 1:
        print("SlokRUN_BIT          : {}".format(dd.ddsTestSlokRun()[0]))
        print("AD9510_DLD_DISABLE_BIT    : {}".format(dldbit))
        print("AD9510_DLD_LOSS_BIT       : {}".format(dd.ddsTestPllStat()[0]))
        print("DLD_LOSS_LATCH_EN_BIT     : {}".format(dd.ddsTestDdsLkdEn()[0]))
        print("LATCHED_DLD_LOSS_BIT      : {}".format(dd.ddsTestPllLoss()[0]))
        print("SlokNOK_BIT            : {}".format(dd.ddsTestSlokNok()[0]))
        print("SR clock NOK           : {}".format(dd.ddsTestSRNok()[0]))
        print("Booster clock NOK      : {}".format(dd.ddsTestBoNok()[0]))
        print("-----------")
        print("SlokAOK_BIT          : {}".format(dd.ddsTestSlokAok()[0]))
        print("SR clock AOK         : {}".format(dd.ddsTestSRAok()[0]))
        print("Booster clock AOK    : {}".format(dd.ddsTestBoAok()[0]))
        print("SYNCSRC_EN           : {}".format(dd.ddsTestSyncSrcEn()[0]))
        print("SYNCSRC_ACK          : {}".format(dd.ddsTestSyncSrcAck()[0]))
    print("---------------------------")
    print("T0WD_BIT             : {}".format(dd.ddsTestT0Wd()[0]))
    print("---------------------------")
    print("Phase Detect. locked : {}".format(dd.ddsTestPdLkd()[0]))
    print("352 locked           : {}".format(dd.ddsTest352Lkd()[0]))
    print("T0_lost counter      : {}".format(dd.ddsGetT0LostCnt()))
    print("---------------------------")
    print("RF_DBG_EN            : {}".format(dd.ddsTestRfDbgEn()[0]))
    print("RF_ADJ_EN            : {}".format(dd.ddsTestRfAdjEn()[0]))
    print("RF_ADJ_STOP          : {}".format(dd.ddsTestRfAdjStop()[0]))
    print("---------------------------")
    print("RF LOST              : {}".format(dd.ddsTestRfLost()[0]))
    print("RF ADJusteD          : {}".format(dd.ddsTestRfAdjd()[0]))
    print("RF ADJUST RUN        : {}".format(dd.ddsTestRfAdjRun()[0]))
    print("RF at PPS=bu44_ppsa  : {}".format(hex(dd.ddsGetRfPps()[0])))
    print("---------------------------")
    print("Histo register 0     : {}".format(dd.ddsGetHist0()))
    print("Histo register 1     : {}".format(dd.ddsGetHist1()))
    print("Histo register 2     : {}".format(dd.ddsGetHist2()))
    print("RF DELTA             : {}".format(hex(dd.ddsGetRfDelta())))
    print("---------------------------")
    for cc in range(8):
        print("!!{}!!".format(bus), end="")
    print()

# ------------------------------------------------------
def statspec(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    # start with ad9510 bits
    # read Digital Lock Detect enable Bit
    d_9510 = fad95(bus, D3S_CMD_9510, 0, 0xD, 0)
    dldbit = (d_9510 & 0x40) >> 6

    # now set SpecWhist
    dd = SpecWhist(ipp)

    for cc in range(8):
        print("{}-".format(bus), end="")
    print()
    #   
    fpgaver = dd.ddsGetFPGA()
    print("FPGA version         : {}".format(hex(fpgaver)))
    #get CoreBcc version
    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_VERSION)
    dd.ddsSetCoreBcc(0xc, 0)  # read
    dd.ddsSetCoreBcc(0x0, 0x4000010)            
    time.sleep(0.01)
    bccver = dd.ddsGetCoreBcc(0x8)
    #discard
    dd.ddsDisCoreBcc()
    #optional purge
    dd.ddsPurCoreBcc()
    print("CoreBcc version        : {}".format(bccver))
    #get CoreEvt version, not for 78
    if fpgaver != 78:
        dd.ddsSetCoreEvt(0x0, 0x1000000)
        dd.ddsSetCoreEvt(0x8, EVT_CMD_RD_VERSION)
        dd.ddsSetCoreEvt(0xc, 0)  # read
        dd.ddsSetCoreEvt(0x0, 0x4000010)            
        time.sleep(0.01)
        evtver = dd.ddsGetCoreEvt(0x8)
        #discard
        dd.ddsDisCoreEvt()
        #optional purge
        dd.ddsPurCoreEvt()
        print("CoreEvt version        : {}".format(evtver))
    print("---------------------------")
    msbit = dd.ddsTestSlave()[0]
    print("T0WDTRIG register    : {}".format(dd.ddsGetT0WdTrig()))
    print("T0WDEN_BIT           : {}".format(dd.ddsTestT0WdEn()[0]))
    print("T0WD_BIT             : {}".format(dd.ddsTestT0Wd()[0]))
    print("---------------------------")
    print("Slave_bit            : {}".format(msbit))
    if msbit == 0:
        #get SEQ_EN bit from BCC_CSR
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_SEQ_EN)
        dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010)            
        time.sleep(0.01)
        vallu = dd.ddsGetCoreBcc(0x8)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        print("SEQ_EN bit (master)  : {}".format(vallu))
    else:
        #get t0bis_cnt from CoreBcc
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_T0BIS)
        dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010)            
        time.sleep(0.01)
        #value to be printed lower
        t0bis = dd.ddsGetCoreBcc(0x8)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        #get t0bis_miss WR stamp from CoreBcc
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_T0BIS_MISS)
        dd.ddsSetCoreBcc(0xc, 0)  # read
        dd.ddsSetCoreBcc(0x0, 0x4000010) 
        time.sleep(0.01)
        #value to be printed lower
        t0bis_tai_sec = dd.ddsGetCoreBcc(0x8)
        t0bis_tai_cyc = dd.ddsGetCoreBcc(0xc)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        print("SlokAOK_BIT          : {}".format(dd.ddsTestSlokAok()[0]))
        print("AD9510_DLD_DISABLE_BIT    : {}".format(dldbit))
        print("AD9510_DLD_LOSS_BIT       : {}".format(dd.ddsTestPllStat()[0]))
        print("DLD_LOSS_LATCH_EN_BIT     : {}".format(dd.ddsTestDdsLkdEn()[0]))
        print("LATCHED_DLD_LOSS_BIT      : {}".format(dd.ddsTestPllLoss()[0]))
        print("SlokNOK_BIT            : {}".format(dd.ddsTestSlokNok()[0]))
        print("SR clock AOK           : {}".format(dd.ddsTestSRAok()[0]))
        print("Booster clock AOK      : {}".format(dd.ddsTestBoAok()[0]))
    print("---------------------------")
    print("Flashed_BIT          : {}".format(dd.ddsTestFlashed()[0]))
    print("Quiet_BIT            : {}".format((dd.ddsTestQuiet()[0])))
    print("RtD3sRun_BIT         : {}".format(dd.ddsTestRtD3sRun()[0]))
    print("RFoE_BIT             : {}".format(dd.ddsTestRfoe()[0]))
    print("---------------------------")
    print("BunchClock enable    : {}".format(dd.ddsTestBunchEn()[0]))
    print("RF enable            : {}".format(dd.ddsTestRfEn()[0]))
    print("GunInj enable        : {}".format(dd.ddsTestGuniEn()[0]))
    print("---------------------------")
    print("Stamping register    : {}".format(dd.ddsGetStamp()))
    print("T0mux                : {}".format(dd.ddsGetT0Mux()))
    print("---------------------------")
    print("Phase Detect. locked : {}".format(dd.ddsTestPdLkd()[0]))
    print("352 locked           : {}".format(dd.ddsTest352Lkd()[0]))
    print("352 phase adjust     : {}".format(dd.ddsGetFpgaPhAdj()))
    print("Clocks phase adjust  : {}".format(dd.ddsGetOutckPhAdj()))
    print("---------------------------")
    print("WR OK                : {}".format(dd.ddsTestWrOK()[0]))
    print("WR lost              : {}".format(dd.ddsTestWrLost()[0]))
    print("INJ ERROR            : {}".format(dd.ddsTestInjErr()[0]))
    print("T0 lost              : {}".format(dd.ddsTestT0Lost()[0]))
    print("T0 counter           : {:d}".format(dd.ddsGetT0Cnt()))
    if msbit != 0:
        print("T0BIS counter       : {}".format(t0bis))
        print("T0BIS_MISS TAI SEC  : {}".format(t0bis_tai_sec))
        print("T0BIS_MISS TAI CY   : {}".format(t0bis_tai_cyc))
    print("---------------------------")
    print("RF Frequency            : {}".format(dd.ddsGetRfFreq()))
    print("T0 delay PROG           : {}".format(dd.ddsGetT0Del()))
    print("T0 delay IN             : {}".format(dd.ddsGetLatch2(hexformat=True)))
    print("GUN delay PROG          : {}".format(dd.ddsGetInjDel()))
    print("GUN delay IN            : {}".format(dd.ddsGetLatchGunDel(hexformat=True)))
    print("Extraction delay PROG   : {}".format(dd.ddsGetExtDel()))
    print("Extraction delay IN     : {}".format(dd.ddsGetLatchExtDel(hexformat=True)))
    print("---------------------------")
    print("MB mode              : {}".format(dd.ddsTestMBEn()[0]))
    print("Bunch List Start     : {}".format(dd.ddsGetBlistStart()))
    print("Bunch List Stop      : {}".format(dd.ddsGetBlistStop()))
    print("Current Bunch        : {}".format(hex(dd.ddsGetBunchNum())))
    print("Auto Bunch List      : {}".format(dd.ddsTestAutoBlist()[0]))
    print("---------------------------")
    print("Rotinj               : {}".format(dd.ddsTestRotinj()[0]))
    print("Auto Rotinj          : {}".format(dd.ddsTestAutoRotinj()[0]))
    print("Rotinj Code          : {}".format(dd.ddsTestRotiCode()[0]))
    print("---------------------------")
    print("RF at PPS is         : {}".format(hex(dd.ddsGetRfPps()[0])))
    print("RF LOST              : {}".format(dd.ddsTestRfLost()[0]))
    print("---------------------------")
    print("RF_DBG_EN            : {}".format(dd.ddsTestRfDbgEn()[0]))
    print("RF_ADJ_EN            : {}".format(dd.ddsTestRfAdjEn()[0]))
    print("RF_ADJ_STOP          : {}".format(dd.ddsTestRfAdjStop()[0]))
    for cc in range(8):
        print("{}-".format(bus), end="")
    print()

# ------------------------------------------------------
def statout(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    print("=============================================")
    for cha in range(12):
        pct = dd.ddsGetPuCkMode(cha)
        if pct == 3:
            strpct = "Clock"
        elif pct == 2:
            strpct = "WidePulse"
        elif pct == 1:
            strpct = "GunPulse"
        else:
            strpct = "None "
        print("CH#{}, ENABLED={}, POLAR={}, MODE={}, SRCE={}, del_reg={}, wid_reg={}.".format(cha+1, 
                dd.ddsTestPuCkEn(cha)[0], dd.ddsTestPuCkPol(cha)[0], strpct, dd.ddsGetPuCkSrce(cha),
                dd.ddsGetPulDel(cha), dd.ddsGetPulWid(cha)))
    print("=============================================")

# ------------------------------------------------------
def statin(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    print("=============================================")
    for cha in range(4):
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        dd.ddsSetCoreBcc(0x8, BCC_CMD_DAC)
        # read
        dd.ddsSetCoreBcc(0xc, 0)
        dd.ddsSetCoreBcc(0x10, (7-2*cha))
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        time.sleep(0.01)
        vallu = dd.ddsGetCoreBcc(0x8)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
        print("INPUT#{}, POLAR={}, ENABLE = {}, IN_LEVEL = 0x{:0x}, STAMP_VALUE = {}.".format(cha, dd.ddsTestInPol(cha)[0], dd.ddsTestInEn(cha)[0], vallu, hex(dd.ddsGetTrigStamp(cha))))
    print("=============================================")

# ------------------------------------------------------
def enprog(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    ben = get_v(1, args)
    rfe = get_v(2, args)
    gie = get_v(3, args)

    dd = SpecWhist(ipp)

    if ben != 0:
        dd.ddsSetBunchEn()
    else:
        dd.ddsClearBunchEn()
    if rfe != 0:
        dd.ddsSetRfEn()
    else:
        dd.ddsClearRfEn()
    if gie != 0:
        dd.ddsSetGuniEn()
    else:
        dd.ddsClearGuniEn()

# ------------------------------------------------------
def sbprog(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    abl = get_v(1, args)
    mbu = get_v(2, args)

    dd = SpecWhist(ipp)

    if mbu != 0:
        dd.ddsSetMBEn()
    else:
        dd.ddsClearMBEn()
    if abl != 0:
        dd.ddsSetAutoBlist()
    else:
        dd.ddsClearAutoBlist()

# ------------------------------------------------------
def bl_fill(args):
# ------------------------------------------------------
    bus = get_v(0, args)

    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    sta = get_v(1, args)
    sto = get_v(2, args)
    zer = get_v(3, args)
    opto = get_v(4, args)

    if sto < sta:
        print("Stop_add < Start_add, exit")
        return False
    else:
        print("sto-sta = {}, sta = {}".format(sto-sta, sta))

    dd = SpecWhist(ipp)
    print("sta {} W start {}".format(hex(sta), dd.ddsSetBlistStart(sta)))
    print("sta {} R start {}".format(hex(sta), dd.ddsGetBlistStart()))
    print("sto {} W stop {}".format(hex(sto), dd.ddsSetBlistStop(sto)))
    
    if (opto != "-n"):
        for jj in range(0, sto-sta+1):
            blad = (sta+jj)*4
            dd.ddsSetBL(blad, (zer+jj)%992)
            if jj<20:
                print("blad {} : {}".format(blad, (zer+jj)%992))
    
# ------------------------------------------------------
def blread(args):
# ------------------------------------------------------
    bus = get_v(0, args)

    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    sta = get_v(1, args)
    sto = get_v(2, args)
    if sto < sta:
        print("Stop_add < Start_add, exit")
        return False
    else:
        print("sto-sta = {}, sta = {}".format(sto-sta, hex(sta)))

    dd = SpecWhist(ipp)

    for jj in range(0, sto-sta):
        blad = (sta+jj)*4
        llu =  dd.ddsGetBL(blad, False)
        print("blad {} : {} = {}".format(hex(blad), llu, hex(llu)))
    
# ------------------------------------------------------
def matprog(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    cha = get_v(1, args)
    and0 = get_v(2, args)
    and1 = get_v(3, args)
    or0 = get_v(4, args)
    or1 = get_v(5, args)

    dd = SpecWhist(ipp)

    matrikso = or1<<24 | or0<<16 | and1<<8 | and0
    dd.ddsSetMat(cha, matrikso)

# ------------------------------------------------------
def mbprog(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    argums = args.split(" ")

# 1st stop mb mode
    dd = SpecWhist(ipp)
    dd.ddsClearMBEn()

    mbli = []
# get values
    for vvv in range(1,len(argums)):
        valve = get_v(vvv, args)
        mbli.append(valve)
    mbli.sort()
    taj = len(mbli)
    print("Multi-bunch list of {} defined.".format(taj))
    for aa in range(taj):
        print(hex(mbli[aa]))
# check list consistency    
    delta = []
    evalu = True
    bb0 = 0
    if (max(mbli) - min(mbli)) < 352:
        print("straight good list")
    else:
        for bb in range(1, taj):
            tadel = mbli[bb] - mbli[bb-1]
            if tadel < 32:
                evalu = False
                break
            elif ((tadel > 350) and  (tadel < (992-352))):
                evalu = False
                break
            else:
                delta.append(tadel)
                if tadel >= (992-352):
                    bb0 = bb
    if evalu == False:
        print("bad delta : {} between {} and {}".format(tadel, mbli[bb], mbli[bb-1]))
        return False
# organize list for HW programming
    progli = []
    progli.append(mbli[bb0])
    for ab in range(1, taj):
        if ab == (taj-1):
            bit15 = 32768 # add bit<15>
        else:
            bit15 = 0 # add bit<15>
        ddel = mbli[(bb0+ab)%taj]//8 - mbli[(bb0+ab-1)%taj]//8
        print("ab={} > ddel={}".format(ab, hex(ddel)))
        if ddel > 0:
            progli.append((ddel-1)*8 + mbli[(bb0+ab)%taj]%8 + bit15)
        else:
            progli.append((ddel-1+(992/8))*8 + mbli[(bb0+ab)%taj]%8 + bit15)
    for aa in range(taj):
        print("{} ; {}".format(progli[aa], hex(progli[aa])))

# Now configure HW
    dd.ddsSetBlistStart(0x1000)
    dd.ddsSetBL(0x1000*4, progli[0])
    print("at {} : {}".format(hex(0x4000), hex(progli[0])))
    for aa in range(1, taj):
        blad = (0x1000+aa)*4
        dd.ddsSetBL(blad, progli[aa])
        print("at {} : {}".format(hex(blad), hex(progli[aa])))
#    print("aa {} - stop {}".format(aa, hex(blad)))
    dd.ddsSetBlistStop(0x1000+aa)
# now restart mb
    dd.ddsSetMBEn()

# ------------------------------------------------------
def dwprog(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dt0 = get_v(1, args)
    ddi = get_v(2, args)
    dde = get_v(3, args)
    wwi = get_v(4, args)
    wwe = get_v(5, args)

    dd = SpecWhist(ipp)

    dd.ddsSetT0Del(dt0)
    dd.ddsSetInjDel(ddi)
    dd.ddsSetExtDel(dde)
    dd.ddsSetInjWid(wwi)
    dd.ddsSetExtWid(wwe)
# reset bunch enable
    dd.ddsClearBunchEn()
    dd.ddsSetBunchEn()

# ------------------------------------------------------
def rd_histo(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_CMD_RD_HISTO)
    dd.ddsSetCoreBcc(0x0, 0x4000010)            

    time.sleep(0.01)
    for rr in range(9):
        vallu = dd.ddsGetCoreBcc(0x8 + 4*rr)
        print("@ {} : {}".format(hex(rr), hex(vallu)))
#discard
    dd.ddsDisCoreBcc()
#optional purge
    dd.ddsPurCoreBcc()
    
# ------------------------------------------------------
def seqen(args):
    """Set SEQ_EN bit in master BCC_CSR"""
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False
    vv2 = get_v(1, args)

    print("t0 enable passed = {}".format(vv2))
    if (vv2 == 0) or (vv2 == -1):
        ena = 0
    else:
        ena = 1

    print("t0 enable = {}".format(ena))
    
    dd = SpecWhist(ipp)

    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_SEQ_EN)
    dd.ddsSetCoreBcc(0xc, 1)  # write
    dd.ddsSetCoreBcc(0x10, ena)
    dd.ddsSetCoreBcc(0x0, 0x4000010)            

#discard
    dd.ddsDisCoreBcc()
#optional purge
    dd.ddsPurCoreBcc()
    
# ------------------------------------------------------
def bcl(args):
    """Set BCL_PIPE bit in master BCC_CSR"""
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    #test master
    msbit = dd.ddsTestSlave()[0]
    if msbit == 0:
        #pipe bunch clock registers
        #CoreBcc request for sending data
        dd.ddsSetCoreBcc(0x0, 0x1000000)
        #set master command 
        dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_BCL_PIPE)
        #send data
        dd.ddsSetCoreBcc(0x0, 0x4000010)
        #discard
        dd.ddsDisCoreBcc()
        #optional purge
        dd.ddsPurCoreBcc()
    else:
        print("     !!!! Requested module IS NOT MASTER !!!!")
    
# ------------------------------------------------------
def mux_t0(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    mux = get_v(1, args)
    print("T0 on {}".format(mux))
    dd = SpecWhist(ipp)

    dd.ddsSetT0Mux(mux)

# ------------------------------------------------------
def trigger(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    chx = get_v(1, args)

    dd = SpecWhist(ipp)
    rrr = True

    print("chx={}".format(chx))

    if chx == "-errrst":
        dd.ddsSetInjerRst()
        print("T00c!!")
    elif chx == "-ppsa":
        dd.ddsSetPpsAck()
        print("Tac!")
    elif chx == "-rrf":
        dd.ddsSetRfRst()
        print("Toc352!")
    elif chx == "-bli":
        dd.ddsSetBlistInc()
        print("Tic!")
    elif chx == "-blr":
        dd.ddsSetBlistRst()
        print("BRAZ!")
    elif chx == "-rfs":
        dd.ddsSetSyncRf()
        print("SYNCRF!")
    elif chx == "-rflrst":
        dd.ddsSetRflRaz()
        print("RFLRAZ!")
    elif chx == "-sk":
        dd.ddsSetSkip44()
        print("SKIP one RF/8 clock!")
    elif chx == "-ju":
        dd.ddsSetJump44()
        print("JUMP one RF/8 clock ahead!")
    else:
        rrr = False

    return rrr

# ------------------------------------------------------
def puprog(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    cha = get_v(1, args)-1
    if (cha < 0) or (cha > 11):
        print("bad channel")
        return False

    enab = get_v(2, args)
    pol = get_v(3, args)
    mod = get_v(4, args)
    sta = get_v(5, args)
    if sta == -1: sta = 0
    # delay is now set as RF periods but counted as RF/8 for pulses
    delm = get_v(6, args)
    if delm == -1:
        deli = 0
    else:
        deli = 8 * delm
    widi = get_v(7, args)
    if widi == -1: widi = 0

    dd = SpecWhist(ipp)

    if (enab == 0) or (enab == -1):
        dd.ddsClearPuCkEn(cha) # disable
    else:
        dd.ddsSetPuCkEn(cha)  # set enable
        if mod == 2:
            dd.ddsSetPuCkMode(cha, 0x2)  # set type rf/8 pulse
        elif mod == 1:
            dd.ddsSetPuCkMode(cha, 0x1)  # set type gun
        else:
            dd.ddsSetPuCkMode(cha, 0x0)  # unused default
        if pol == 1:
            dd.ddsSetPuCkPol(cha)
        else:
            dd.ddsClearPuCkPol(cha)
        dd.ddsSetPuCkSrce(cha, sta)
        dd.ddsSetPulDel(cha, deli)
        dd.ddsSetPulWid(cha, widi)
    return True

# ------------------------------------------------------
def otb(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    modo = get_v(1, args)
    polo = get_v(2, args)

    dd = SpecWhist(ipp)

    for cha in range(12):
        dd.ddsSetPuCkEn(cha)  # set enable
        if polo == 1:
            dd.ddsSetPuCkPol(cha)
        else:
            dd.ddsClearPuCkPol(cha)
        if (cha == 2) or (cha == 1):
            dd.ddsSetPuCkMode(cha, 0x1)  # set gun
        elif cha == 0:
            dd.ddsSetPuCkMode(cha, 0x2)  # set RF pulse
            dd.ddsSetPuCkSrce(cha, 1)    # inj
        else:
            if (modo == -1) or (modo == "-ck"):
                dd.ddsSetPuCkMode(cha, 0x3)  # set clock                
                dd.ddsSetPuCkSrce(cha, 1)    # SR_TC
                dd.ddsSetPulDel(cha, 0xf800) # 50%
            elif modo == "-pu":
                dd.ddsSetPuCkMode(cha, 0x2)  # set RF/8 pulse               
                dd.ddsSetPuCkSrce(cha, 0x2)    # extraction
                dd.ddsSetPulDel(cha, (cha-2)*8)
                dd.ddsSetPulWid(cha, cha-1)
                
# ------------------------------------------------------
def ckprog(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False
        
    cha = get_v(1, args)-1
    if (cha < 0) or (cha > 11):
        print("bad channel")
        return False

    ene = get_v(2, args)
    src = get_v(3, args)
    pol = get_v(4, args)
    pha = get_v(5, args)
    kof = get_v(6, args)

    dd = SpecWhist(ipp)

    if (ene == 0) or (ene == -1):
        dd.ddsClearPuCkEn(cha) # disable
    else:
        dd.ddsSetPuCkEn(cha)  # set enable + type clock
        dd.ddsSetPuCkMode(cha, 0x3)  # set clock mode
        if pol == 1:
            dd.ddsSetPuCkPol(cha)
        else:
            dd.ddsClearPuCkPol(cha)
        dd.ddsSetPuCkSrce(cha, src)
        if src != 3:
            if pha == -1: pha = 0
            dd.ddsSetPulDel(cha, pha)
        else:
            if kof == -1: kof = 0
            kopha = (kof<<10) + (pha<<21)
            dd.ddsSetPulDel(cha, kopha)
    return True

# ------------------------------------------------------
def srdiv(args):
# ------------------------------------------------------
    '''easy CLK_SR_DIV programming with intuitive parameters
    forces enable. To disable use ck command.
    '''
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False
    dd = SpecWhist(ipp)
    
    cha = get_v(1, args)-1
    if (cha < 0) or (cha > 11):
        print("bad channel")
        return False
    div = get_v(2, args)-1
    # min value
    if (div == -1) or (div == 0): div = 1
    phsr = get_v(3, args)
    if phsr < 0 : phsr = 0
    if phsr > div: phsr = 0
    pha = get_v(4, args)
    #reorganize phsr as k*62+p
    kpha = pha//62
    ppha = pha%62
    rpha = (kpha<<6) + ppha
    # enable, clock mode, clear polarity
    dd.ddsSetPuCkMode(cha, 0x3)
    dd.ddsSetPuCkEn(cha)
    dd.ddsClearPuCkPol(cha)
    # source = sr_div
    dd.ddsSetPuCkSrce(cha, 0x3)
    # program
    kopha = (div<<10) + (rpha<<3)
    dd.ddsSetPulDel(cha, kopha)
    dd.ddsSetSrDivPh(cha, phsr)
    return True
            
# ------------------------------------------------------
def ssrdiv(args):
# ------------------------------------------------------
    '''Once CLK_SR_DIV programmed consistently in 1 or 2 WHISTs, give both bus numbers to enable synchronize.
    Synchronization will take place at next rf_hot_ok from master (~next PPS, single source)
    '''
    busa = get_v(0, args)
    ippa = bus_lup(str(busa))
    if ippa == '0':
        return False
    busb = get_v(1, args)
    if busb == -1:
        print("Single WHIST")
        deuze = 0
    else:
        ippb = bus_lup(str(busb))
        if ippb == '0':
            return False
        else:
            deuze = 1
            print("SR_count synchronize WHIST modules {} and {}.".format(busa, busb))
    
    f_ssrc_en(ippa)

    if deuze == 1:
        f_ssrc_en(ippb)

    time.sleep(1.5)

    f_ssrc_ack(ippa)
    if deuze == 1:
        f_ssrc_ack(ippb)
    
# ------------------------------------------------------
# syncsrc individual calls
# ------------------------------------------------------
def f_ssrc_en(ippp):

    ddm = SpecWhist(ippp)
    paf = ddm.ddsClearSyncSrcEn()
    time.sleep(0.1)
    paf = ddm.ddsSetSyncSrcEn()

def f_ssrc_ack(ippp):
    
    ddm = SpecWhist(ippp)
    dom = ddm.ddsTestSyncSrcAck()
    pps = ddm.ddsGetBu44Lsb()
    print("dom = {} ; pps = {}".format(dom, pps))
#    paf = ddm.ddsClearSyncSrcEn()

# ------------------------------------------------------
def phprog(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    pll = get_v(1, args)
    adj = get_v(2, args)
    if (adj < 0) or (adj > 7):
        print("bad value")
        return False
    
    if pll == "-fpga":
        print("Writing CTRL {}".format(hex(dd.ddsSetFpgaPhAdj(adj))))
    elif pll == "-outck":
        print("Writing CTRL {}".format(hex(dd.ddsSetOutckPhAdj(adj))))
    else:
        print("!!!!!!!!!!!!!!!!!!!Bad option!!!!!!!!!!!!!!!!!!!!!!")

# ------------------------------------------------------
# a set of little functions to run pspps with M & S alternately
# ------------------------------------------------------
def wh_ppsack(buss):
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)
    ddm.ddsSetPpsAck()

def wh_read_pps(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)
    psp = int(ddm.ddsGetRfPps(hexformat=False)[0])
    return psp

def wh_read_pspps(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)
    psp = int(ddm.ddsGetPsPps(hexformat=False)[0])
    return psp

def wh_read_ppsrdy(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)

    rdy = ddm.ddsTestPpsRdy()
    return rdy[0]

def wh_seqen(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    dd.ddsSetCoreBcc(0x0, 0x1000000)
    dd.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_SEQ_EN)
    dd.ddsSetCoreBcc(0xc, 0)  # read
    dd.ddsSetCoreBcc(0x0, 0x4000010)            
    time.sleep(0.01)
    vallu = dd.ddsGetCoreBcc(0x8)

#discard
    dd.ddsDisCoreBcc()
#optional purge
    dd.ddsPurCoreBcc()
    return vallu

def wh_m_start(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)

    ddm.ddsSetCoreBcc(0x0, 0x1000000)
    ddm.ddsSetCoreBcc(0x8, BCC_MASTER_CMD_START)
    ddm.ddsSetCoreBcc(0x0, 0x4000010)            
    time.sleep(3)

def wh_s_start(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    ddm = SpecWhist(ipp)

    dds.ddsSetSyncRf()
    time.sleep(30)

def wh_s_stats(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dds = SpecWhist(ipp)

    lokok = dds.ddsTestSlvRfLok()[0]
    lokfail = dds.ddsTestSlvLokFail()[0]
    failcnt = int(dds.ddsGetLokFailCnt(hexformat=False))
    return [lokok, lokfail, failcnt]

#------------------------------------------------------
def tbench(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    chx = get_v(1, args)

    rrr = True

    print("chx={}".format(chx))

    if chx == "-b1":
        print("1 bunch, shift number")
        tbb1(bus)
    elif chx == "-bx":
        print("multi bunches, shift number")
        tbbx(bus)
    elif chx == "-ba":
        print("auto-bunchlist 992")
        tbba(bus)
    elif chx == "-st":
        print("compare pps and pspps stamps on module")
        tast(bus)
    else:
        rrr = False

    return rrr

# ------------------------------------------------------
def tast(buss):
# ------------------------------------------------------
    print("empty there")
    return False

# ------------------------------------------------------
def tbba(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

# fill bunch list RAM
    for jj in range(992):
      dd.ddsSetBL(jj*4, jj)
    dd.ddsSetBlistStart(0)
    dd.ddsSetBlistStop(992)

# mode set to : bunch_en rf_en b1 auto_blist
    dd.ddsSetRfEn()
    dd.ddsClearMBEn()
    dd.ddsSetAutoBlist()
# reset bunch list pointer
    dd.ddsSetBlistRst()
# start
    dd.ddsSetBunchEn()

    feuvert = True
# main loop
    while feuvert:
        try:
            time.sleep(2)
        except KeyboardInterrupt:
            feuvert = False
            return

    dd.ddsSetBlistRst()

# ------------------------------------------------------
def tbb1(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

# fill bunch list RAM
    for jj in range(992):
      dd.ddsSetBL(jj*4, jj)
    dd.ddsSetBlistStart(0)
    dd.ddsSetBlistStop(992)

# set type pulse on ch#3
    dd.ddsSetPuCkMode(3, 2)
# show gun output
#    dd.ddsSetCtrlMux(2)
# mode set to : bunch_en rf_en b1 not_auto_blist
    dd.ddsSetBunchEn()
    dd.ddsSetRfEn()
    dd.ddsClearMBEn()
    dd.ddsClearAutoBlist()
# set inj referred + default del & wid on ch#3
    dd.ddsSetPuCkSrce(3, 1)
    dd.ddsSetPulDel(3, 17)
    dd.ddsSetPulWid(3, 7)
    print("CH#{}, del_reg = {}.".format(3, dd.ddsGetPulDel(3)))
    print("CH#{}, wid_reg = {}.".format(3, dd.ddsGetPulWid(3)))
# reset bunch list pointer
    dd.ddsSetBlistRst()

    feuvert = True
# main loop
    while feuvert:
        for ggg in range(992):
            dd.ddsSetBlistInc()  # increment once/sec & loop
            try:
                time.sleep(2)
            except KeyboardInterrupt:
                feuvert = False
                return
        dd.ddsSetBlistRst()

# ------------------------------------------------------
def tbbx(buss):
# ------------------------------------------------------
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

# show gun on output 11
    dd.ddsSetPuCkMode(11, 0x1)  # set type gun
# mode set to : bunch_en rf_en not_auto_blist

    dd.ddsSetBunchEn()
    dd.ddsSetRfEn()
    dd.ddsClearAutoBlist()
# set bunch list pointer to multi bunch zone
    dd.ddsSetBlistStart(0x1000)
# get bunch list stop pointer
    bute = dd.ddsGetBlistStop()
    print("butrd = {}".format(bute))
    butex = int(dd.ddsGetBlistStop(hexformat=False))
    print("butex = {}".format(butex))

    feuvert = True
    dd.ddsSetMBEn()
# main loop
    ilo = 0
    while feuvert:
        dd.ddsSetBlistStart(0x1000+ilo*10)            
        try:
            time.sleep(0.5)
        except KeyboardInterrupt:
            return
        ilo += 1
        if ((0x1000+ilo*10) >= butex): ilo = 0
        
# ------------------------------------------------------
def ldbx(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False
# process file name : add full path
    fgot = get_v(1, args).strip("-")
    fname = "/segfs/linux/timing/whist/labo/bunchlists/" + fgot.lower()
    print(fname)
    alire = open(fname, "r")

    dd = SpecWhist(ipp)

# set start to default
    dd.ddsSetBlistStart(0x1000)
    nbpul = 0
    try:
        lu = csv.reader(alire,delimiter=" ")
        for row in lu:
            nbpul += 1
            blad = (int(row[0]) + 0x1000)*4
            if row[1].startswith("0X") or row[1].startswith("0x"):
                blv = int(row[1], base=16)
            else:
                blv = int(row[1])
            print("at {} : {}".format(hex(blad), hex(blv)))
# fill RAM
            dd.ddsSetBL(blad, blv)

    except Exception:
# dont forget to set stop.
        dd.ddsSetBlistStop(0x1000+nbpul-1)
        alire.close()
        print("nbpul={} ; write stop at {}".format(nbpul, 0x1000+nbpul))
                     
    finally:
# dont forget to set stop
        dd.ddsSetBlistStop(0x1000+nbpul-1)
        alire.close()

# ------------------------------------------------------
def rotinj(args):
# ------------------------------------------------------
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    eneb = get_v(1, args)
    oto = get_v(2, args)
    debu = get_v(3, args)
    
    dd = SpecWhist(ipp)

    if (eneb == 0) or (eneb == -1):
        dd.ddsClearRotinj()
        dd.ddsClearAutoRotinj()
    else:
        if (debu < 0) or (debu > 991):
            print("Bad bunch number {}".format(debu))
            return False
        if (oto == 0) or (oto == -1):
            dd.ddsClearAutoRotinj()
        else:
            dd.ddsSetAutoRotinj()
        delta = 352/4
        # organize lists for HW programming then program HW memory
        dd.ddsSetBlistStart(0x1000)
        blad = 0x4000
        # just write delta consecutive numbers starting from debu
        for bu in range(delta):
            jecri = (debu + bu)%992
            dd.ddsSetBL(blad, jecri)
            print("at {} : {}".format(hex(blad), jecri))
            blad += 4
        dd.ddsSetBlistStop((blad/4)-1)
        dd.ddsClearAutoBlist()
        dd.ddsClearMBEn()
        dd.ddsSetRotinj()

# ------------------------------------------------------
def ad95(args):
# ------------------------------------------------------
    '''R/W from/to AD9516 (WR = 0) or AD9510 (RF_PLL = 1)
    args : bus ; 1/0 (ad9510/16) ; -r/-w ; register_address ; value if W
    calls fad95 function
    '''
    
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    ad9 = get_v(1, args)
    rwr = get_v(2, args)
    ada = get_v(3, args)
    valw = get_v(4, args)

    if rwr == "-r":
        rwb = 0
    elif rwr == "-w":
        rwb = 1
        print("-----> Write {} at {}".format(hex(valw), hex(ada)))
    else:
        print("!!!!!!-----> -r / -w")
        return False

    if ad9 == 1:
        # ccs = 0x1 cs no longer used
        print("-----> RF PLL AD9510")
        cmd95x =  D3S_CMD_9510
    elif ad9 == 0:
        # ccs = 0x0
        print("-----> White Rabbit PLL AD9516")
        cmd95x =  D3S_CMD_9516
    else:
        print("!!!!!!-----> Bad PLL address {}".format(hex(ad9)))
        return False

    print("Lu : {}".format(fad95(bus, cmd95x, rwb, ada, valw)))
    
# ------------------------------------------------------
def fad95(buss, cmd95x, rwb, addb, valb):
# ------------------------------------------------------
    '''function to R/W into AD9516/10
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    if cmd95x ==  D3S_CMD_9510:
        upda = 0x5a
    elif cmd95x ==  D3S_CMD_9516:
        upda = 0x232
        
    dd = SpecWhist(ipp)

    #request
    dd.ddsSetCoreDds(0x0, 0x1000000)
    #set control command 0xa for ad9516, 0xb for ad9510
    dd.ddsSetCoreDds(0x8, cmd95x)
    #dummy
    dd.ddsSetCoreDds(0xc, 0)
    #set r=0/w=1
    dd.ddsSetCoreDds(0x10, rwb)
    #offset
    dd.ddsSetCoreDds(0x14, addb)
    #value to write
    if rwb == 1:
        dd.ddsSetCoreDds(0x18, valb)
    #send request  0x4000010 r or w
    dd.ddsSetCoreDds(0x0, 0x4000010)
    #Now update regs if w otherwise read value
    if rwb == 1:
        dd.ddsSetCoreDds(0x0, 0x1000000)
        dd.ddsSetCoreDds(0x8, cmd95x)
        dd.ddsSetCoreDds(0xc, 0)
        dd.ddsSetCoreDds(0x10, rwb)
        #update register offset
        dd.ddsSetCoreDds(0x14, upda)
        #write 1 to bit 0
        dd.ddsSetCoreDds(0x18, 1)
        #send request  0x4000010
        dd.ddsSetCoreDds(0x0, 0x4000010)
    elif rwb == 0:
        #read data
        time.sleep(0.01)
        vallu = dd.ddsGetCoreDds(0x10)
        #print("@ {} : {}".format(hex(addb), hex(vallu)))
        #discard
        dd.ddsDisCoreDds()
        #optional purge
        dd.ddsPurCoreDds()
        return vallu

# ------------------------------------------------------
def lad95(args):
# ------------------------------------------------------
    '''lists AD9516 / AD9510 registers
    '''
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)

    ad9 = get_v(1, args)

    feuvert = False

    if ad9 == 1:
        # ccs = 0x1 cs no longer used
        print("-----> RF PLL AD9510")
        ctrl95x = ctrl9510
        cmd95x =  D3S_CMD_9510
        feuvert = True
    elif ad9 == 0:
        # ccs = 0x0
        print("-----> White Rabbit PLL AD9516")
        ctrl95x = ctrl9516
        cmd95x =  D3S_CMD_9516
        feuvert = True
    else:
        print("!!!!!!-----> Bad PLL address {}".format(ad9))
        return False

    if feuvert:
        for adreg in ctrl95x:
            #request
            dd.ddsSetCoreDds(0x0, 0x1000000)
            #set control command 0xa for ad95xx
            dd.ddsSetCoreDds(0x8, cmd95x)
            #set dummy
            dd.ddsSetCoreDds(0xc, 0)
            #set r=0
            dd.ddsSetCoreDds(0x10, 0)
            #offset
            dd.ddsSetCoreDds(0x14, adreg)
            # CS should be obsolete : ad9510 directly addressed by cmd
            #dd.ddsSetCoreDds(0x18, ccs)
            #send request  0x4000010
            dd.ddsSetCoreDds(0x0, 0x4000010)
            #read data
            time.sleep(0.01)
            vallu = dd.ddsGetCoreDds(0x10)
            print("@ {} : {}".format(hex(adreg), hex(vallu)))
            #discard
            dd.ddsDisCoreDds()
            #optional purge
            dd.ddsPurCoreDds()        

# ------------------------------------------------------
def rfn(args):
# ------------------------------------------------------
    '''program RF/n into AD9510 OUTPUT#2
    calls fad95 function
    '''
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False
    
    enab = get_v(1, args)
    divi = get_v(2, args)
    if (enab == 0):
    # set disable : AD9510 output#2, register 0x3e
    #OUT2: [1..0] 2= safe power down = LVPECL OFF
    #      [3..2] 2=default=810mV output
        fad95(bus, D3S_CMD_9510, 1, 0x3e, 0x0a)
        return True
    else:
        if (divi > 32): divi = 32
        # set enable : AD9510 output#2, register 0x3e
        #OUT2: [1..0] 0=LVPECL ON
        #      [3..2] 2=default=810mV output
        fad95(bus, D3S_CMD_9510, 1, 0x3e, 0x08)
        #bypass or not
        if (divi == 1):
            # set bypass divider to AD9510 output#2, register 0x4d
            #OUT2: [3..0] 0=no phase offset
            #      [7..4] 8=bypass
            fad95(bus, D3S_CMD_9510, 1, 0x4d, 0x80)
        else:
            # unset bypass divider to AD9510 output#2, register 0x4d
            #OUT2: [3..0] 0=no phase offset
            #      [7..4] 0=No bypass, No Sync, No Force, Start = Low
            fad95(bus, D3S_CMD_9510, 1, 0x4d, 0x00)
            #now set division
            divh = (divi // 2) - 1 + (divi % 2)
            divl = (divi // 2) - 1
            #now program division in reg 0x4c
            #OUT2: [3..0] divl=nb of cycles high
            #      [7..4] divh=nb of cycles low
            divp = (divl << 4) + divh
            fad95(bus, D3S_CMD_9510, 1, 0x4c, divp)

# ------------------------------------------------------
def pll23(args):
# ------------------------------------------------------
    """scan PLL phase adjust on PLL-outputs 5-6, or set to value"""
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    outch = get_v(1, args)
    if (outch != 2) and (outch != 3):
        print("Bad channel for fine delay phase adjust {}".format(outch))
        return False

    raval = get_v(2, args)
    phcoarse = get_v(3, args)

    # unbypass fine delay for channel, PLL5 on ch3, PLL6 on ch2
    fad95(bus, D3S_CMD_9510, 1, (0x38 + (2-outch)*4), 0) 
    # set capacitor & current to default for channel
    fad95(bus, D3S_CMD_9510, 1, (0x39 + (2-outch)*4), PHADJUST) 

    if (raval != "-sc") and (raval != "-pi"):
        pfad = (raval << 1) & 0x7f
        # set fine delay for channel
        fad95(bus, D3S_CMD_9510, 1, (0x3a + (2-outch)*4), pfad)
        return True
    elif raval == "-pi":
        # set phase offset at resync if required
        if (phcoarse == 0) or (phcoarse == 1):
            fad95(bus, D3S_CMD_9510, 1, (0x55 + (2-outch)*2), phcoarse)
            fpllrfs(bus)
            return True
    else:
        # -sc option : scan delta_t
        print("scanning PLL fine phase delay adjust")
        # read PI coarse phase
        pif = (fad95(bus, D3S_CMD_9510, 0, (0x55 + (2-outch)*2), 0)) & 0x1
        print("Pi coarse phase for channel {} is {}.".format(outch, pif))
        irampbits = PHADJUST & 0x7
        iramp = 200*(irampbits+1)
        nocapsbits = (PHADJUST & 0x38) >> 3
        nocaps = 1
        # count number of 0s in nocapsbits
        for nc in range(3):
            print("nocapsbits={}".format(nocapsbits))
            bnc = nocapsbits & 0x1
            if bnc == 0:
                nocaps += 1
            nocapsbits = nocapsbits >> 1
        print("Delay parameters : iramp={} uamp ; nocaps={}".format(iramp, nocaps))
        derange = (200*(nocaps+3)*1.3286)/iramp
        print("Delay range = {}".format(derange))
        #        offsetns = 0.34 + (6*(nocaps-1)/iramp) + ((1600-iramp)/1000)
        offsetns = float(0.34 + (6.0*(nocaps-1)/iramp) + ((1600.0-iramp)/10000))
        print("Offset ns   = {}".format(offsetns))
        delayfs = derange*24/31 + offsetns
        print("Delay FS    = {}".format(delayfs))
        feuvert = True
        sck = 0
        while feuvert:
            pfad = (sck << 1) & 0x7f
            fad95(bus, D3S_CMD_9510, 1, (0x3a + (2-outch)*4), pfad)
            ajuste = offsetns + derange*sck/31
            print("delay adjust {} :   {} ns".format(sck, ajuste))
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                return
            sck += 1
            if sck == 0x19:
                sck = 0
            
# ------------------------------------------------------
def fpllrfs(buss):
# ------------------------------------------------------
    '''function trig RF_sync monostable
       function used in pll23
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    return dd.ddsSetSyncRf()

# ------------------------------------------------------
def phck(args):
# ------------------------------------------------------
    """shift  delay on channel clock  with time step
    if -srd mode add division factor and 50% option"""
    
    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    cha = get_v(1, args)-1
    sq = get_v(2, args)
    pas = get_v(3, args)
    div = get_v(4, args)-1
    if (div == -1) or (div == 0): div = 1

    dd = SpecWhist(ipp)

    dd.ddsSetPuCkEn(cha)  # set enable + type clock
    dd.ddsSetPuCkMode(cha, 0x3)  # set clock mode
    dd.ddsClearPuCkPol(cha) # no polarity
    if (sq == "-4"):
        dd.ddsSetPuCkSrce(cha, 5) # ck4 
    elif (sq == "-16"):
        dd.ddsSetPuCkSrce(cha, 4) # ck16   
    elif (sq == "-sr"):
        dd.ddsSetPuCkSrce(cha, 1) # tcsr
    elif (sq == "-srd"):
        dd.ddsSetPuCkSrce(cha, 3) # clk_sr_div
    elif (sq == "-bo"):
        dd.ddsSetPuCkSrce(cha, 2) # tcboo
    else:
        dd.ddsSetPuCkSrce(cha, 1) # tcsr default        
    
    feuvert = True
# main loop
    deli = 0
    delk = 0
    print("Parti !   = {}".format(deli))
    while feuvert:
        if (sq == "-4") or (sq == "-16"):
            dd.ddsSetPulDel(cha, deli + (delk % 8)*32)            
        elif (sq == "-srd"):
            pha = deli//8
            kpha = pha//62
            ppha = pha%62
            rpha = (kpha<<6) + ppha
            kopha = (div<<10) + (rpha<<3)
            dd.ddsSetPulDel(cha, kopha)
            dd.ddsSetSrDivPh(cha, delk)
        elif (sq == "-sr"):
            delo = deli & 0x3ff # basic delay
            dela = ((delo/8) + 62) % 124# toggle delay 1/2 period later
            delw = (dela << 10) | delo
            #print("delw={}".format(hex(delw)))
            dd.ddsSetPulDel(cha, delw)
        elif (sq == "-bo"):
            delo = deli & 0x1ff # basic delay
            dela = ((delo/8) + 22) % 44# toggle delay 1/2 period later
            delw = (dela << 10) | delo
            #print("delw={}".format(hex(delw)))
            dd.ddsSetPulDel(cha, delw)
        try:
            time.sleep(float(pas)/10)
        except KeyboardInterrupt:
            return
        if (sq == "-srd"):
            deli += 8
        else:
            deli += 1
        if (deli == 31) and ((sq == "-4") or (sq == "-16")):
            deli = 0
            delk += 1
        elif (sq == "-bo") and (deli == 352):
            deli = 0
        elif (sq == "-sr") and (deli == 992):
            deli = 0
        elif (sq == "-srd") and (deli == 992):
            deli = 0
            # coarse phase up to div
            if delk < div:
                delk += 1
            else:
                delk = 0
            
# ------------------------------------------------------
def ddt(args):
# ------------------------------------------------------
    """Test delta_t stability on outputs#2-3
    scan option : scan Delta_t 0->21
    run : set Delta_t to value then loop"""
    
    bus = get_v(0, args)
    print("bus={}".format(bus))
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    sort = get_v(1, args)
    # set channel#0 input threshold
    if sort == 3:
        dacLoader(bus, 1, 7, 0x80)
    elif sort == 2:
        dacLoader(bus, 1, 7, 0x20)
    else:
        print("Bad output value for test {}".format(sort))
        return False
    # read PI coarse phase
    pif = (fad95(bus, D3S_CMD_9510, 0, (0x55 + (2-sort)*2), 0)) & 0x1
    print("Pi coarse phase for channel {} is {}.".format(sort, pif))
    opti = get_v(2, args)
    rfdel = get_v(3, args)
    valu = get_v(4, args)

    # prepare sort output as tc_32
    ftc32(bus, sort-1, rfdel)
    # prepare input#0 : enable / polarity=0 positive
    fddtin(bus)
    print("config {} and input#0 OK".format(sort))

    if opti == "-r":
        pfad = (valu << 1) & 0x7f
        # set fine delay for channel : out#2=9510#6 ;  out#3=9510#5
        fad95(bus, D3S_CMD_9510, 1, (0x3a + (2-sort)*4), pfad)
        fddtingate(bus, 2)
        print("Test Stats after 2 seconds : {}".format(hex(fddtstat(bus))))
        return True
    elif opti == "-s":
        # one scan Delta_t values
        stats = fddtstat(bus)
        print("Stats before test : {}, {}, {}, {}".format(hex(stats[0]), hex(stats[1]), hex(stats[2]), hex(stats[3])))
        for sck in range(23):
            pfad = (sck << 1) & 0x7f
            fad95(bus, D3S_CMD_9510, 1, (0x3a + (2-sort)*4), pfad)
            fddtingate(bus, 1)
            stats = fddtstat(bus)
            print("Test Stats at {} : {}, {}, {}, {}".format(sck, hex(stats[0]), hex(stats[1]), hex(stats[2]), hex(stats[3])))
    elif opti == "-ls":
        # full scan to detect metastability
        stats = fddtstat(bus)
        # PI loop (0-1)
        for pip in range(2):
            # write coarse phase 0/PI then loop 22 delta_t
            fad95(bus, D3S_CMD_9510, 1, (0x55 + (2-sort)*2), pip)
            fpllrfs(bus)
            print("Coarse phase : {}*PI".format(pip))
            # RF fine delay loop (0-7)
            for prf in range(8):
                # write prf using ftc32 function
                ftc32(bus, sort-1, prf)
                print("RF phase : {}".format(prf))
                # delta_t loop (0-22)
                for sck in range(23):
                    pfad = (sck << 1) & 0x7f
                    fad95(bus, D3S_CMD_9510, 1, (0x3a + (2-sort)*4), pfad)
                    fddtingate(bus, 1)
                    stats = fddtstat(bus)
                    print("Test Stats at {} : {}, {}, {}, {}".format(sck, hex(stats[0]), hex(stats[1]), hex(stats[2]), hex(stats[3])))
    elif opti == "-l":
        # loop on scan Delta_t values
        feuvert = True
        while feuvert:
            dtsta = []
            print("-------> at {}".format(pendul.datetime.now()))
            for sck in range(23):
                pfad = (sck << 1) & 0x7f
                fad95(bus, D3S_CMD_9510, 1, (0x3a + (2-sort)*4), pfad)
                # gate 1s
                fddtingate(bus, 1)
                stats = fddtstat(bus)
                if (stats[0]+stats[1]+stats[3]) == 0x0:
                    dtsta.append(0)
                else:
                    dtsta.append(1)
            print("{}.".format(dtsta))
            try:
                time.sleep(600)
            except KeyboardInterrupt:
                return
    else:
        return False
    print("ddt end")

# ------------------------------------------------------
def ftc32(buss, sortie, rfdel):
# ------------------------------------------------------
    '''function to set tc_32 mode on output
       function used in ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    dd.ddsSetPuCkEn(sortie)  # set enable + type clock
    dd.ddsSetPuCkMode(sortie, 0x3)  # set clock mode
    dd.ddsClearPuCkPol(sortie) # positive pol
    dd.ddsSetPuCkSrce(sortie, 0) # 0=tc_32
    dd.ddsSetPulDel(sortie, rfdel)

# ------------------------------------------------------
def fddtin(buss):
# ------------------------------------------------------
    '''function to configure input#0 for test
       function used in ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    dd.ddsClearDdt  #clear...
    dd.ddsSetInEn(0)
    dd.ddsClearInPol(0)
    dd.ddsSetDdt    #... then set : process cleared
    return True

# ------------------------------------------------------
def fddtingate(buss, dur):
# ------------------------------------------------------
    '''function enable/disable input#0 for test
       function used in ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    time.sleep(dur/10)
    dd.ddsSetDdtGate   # open gate
    time.sleep(dur)
    dd.ddsClearDdtGate # close gate
    time.sleep(dur/10)
    return True

# ------------------------------------------------------
def fddtstat(buss):
# ------------------------------------------------------
    '''function read input#0 for test
       function used in ddt
    '''
    ipp = bus_lup(str(buss))
    if ipp == '0':
        return False

    dd = SpecWhist(ipp)
    stato = dd.ddsGetDdtHisto()
    metas_0 = stato & 0xFF
    metas_1 = (stato & 0xFF00) >> 8
    metas_2 = (stato & 0xFF0000) >> 16
    metas_3 = (stato & 0xFF000000) >> 24
    return [metas_0, metas_1, metas_2, metas_3]

# ------------------------------------------------------
def dld(args):
# ------------------------------------------------------
    """ad9510 Digital Lock Detect management :
    enable AD9510 function in register#D
    enable/reset latch of detected loss in VHDL"""
    
    bus = get_v(0, args)
    print("bus={}".format(bus))
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    e9510 = get_v(1, args)
    lr = get_v(2, args)

    if (e9510 != 0) or (e9510 == -1):
        # disable = 1 in 0xD, antibacklash assumed default=0
        fad95(bus, D3S_CMD_9510, 1, 0xD, 0x40)
    else:
        # enable = 0 in 0xD, antibacklash assumed default=0
        fad95(bus, D3S_CMD_9510, 1, 0xD, 0x0)

    # manage latch in vhdl
    dd = SpecWhist(ipp)
    if lr == "-l":
        # set latch
        dd.ddsSetDdsLkdEn()
        print("Enable DdsLkdEn bit")
        return True
    elif lr == "-r":
        # clear then set latch to reset vhdl logic for PLLOSS_BIT
        dd.ddsClearDdsLkdEn()
        time.sleep(0.1)
        dd.ddsSetDdsLkdEn()
        print("Resetting PLLOSS_BIT by clear/set DdsLkdEn bit")
        return True
    elif lr == "-nol":
        # clear latch enable
        dd.ddsClearDdsLkdEn()
        print("Clearing DdsLkdEn bit")
        return True
    else:
        print("DdsLkdEn bit = {}".format(dd.ddsTestDdsLkdEn()[0]))
        return False
    # read PI coarse phase
            
# ------------------------------------------------------
def tsts(args):
# ------------------------------------------------------
    """time stamping test :
       bus
       input index
       time gate length in seconds
       """

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False

    dex = get_v(1, args)
    if not 0 <= dex < 4:
        print("Bad index")
        return False

    gat = get_v(2, args)
    if gat <= 0 :
        print("No time gate.")
        return False

    dd = SpecWhist(ipp)

    #close in case, reset fifo, check, open gate and close
    print("Stamp_reg before clear = {}".format(dd.ddsGetStamp()))
    dd.ddsClearInEn(dex)
    dd.ddsResetInFifo(dex)
    vide = dd.ddsTestStampEpty(dex)[0]
    print("Fifo empty bit = {}".format(vide))
    if vide == 0:
        print("Stamp_reg after clear = {}".format(dd.ddsGetStamp()))
        return False
    dd.ddsSetInEn(dex)
    time.sleep(gat)
    dd.ddsClearInEn(dex)
    #read if any
    yakek = dd.ddsTestStampEpty(dex)[0]
    if yakek == 1:
        print("No stamp in gate.")
    else:
        ii = 0
        while (yakek != 1) and (ii < 10):
            #put next fifo value into read register (monostbale strobe)
            dd.ddsReadInFifo(dex)
            print("In({}) = {}".format(ii, dd.ddsGetTrigStamp(dex)))
            yakek = dd.ddsTestStampEpty(dex)[0]
            ii += 1

# ------------------------------------------------------
def csvsave(args):
# ------------------------------------------------------
    """write fine delays to csv filet :
       file name fd78csv
       args = bus, field, value
       """

    bus = get_v(0, args)
    ipp = bus_lup(str(bus))
    if ipp == '0':
        return False
    col = get_v(1, args)
    if col == "-fd1": col = 'fd1'
    elif col == "-fd2": col = 'fd2'
    elif col == "-fd3": col = 'fd3'
    elif col == "-md2": col = 'md2'
    elif col == "-md3": col = 'md3'
    else: col = 0
    if col not in fd78csvcols:
        print("Bad field")
        return False
    vfd = get_v(2, args)

    # ckeck fine_delay_file
    fdelbool = False
    try:
        with open('fd78csv', "r") as fd:
            fdelbool = True
            print("---------------> Fine_delay_file detected")
    except IOError:
        print("No current fine_delay_file. Creating it.")
        aecrire = open('fd78csv', "w")
        skr = csv.DictWriter(aecrire, ('BUS', 'fd1', 'fd2', 'fd3', 'md2', 'md3'),
                dialect=csv.excel, extrasaction='ignore', restval='',
                delimiter=',')
        skr.writeheader()
        aecrire.close()
        fdelbool = True

    # read and copy file, test if update or append
    hit = False
    lignes = []
    with open("fd78csv", "r") as fd:
        lu = csv.DictReader(fd,delimiter=",")
        titres = lu.fieldnames
        for row in lu:
            uneligne = row
            if (uneligne['BUS'] == str(bus)):
                # update field
                uneligne[col] = vfd
                hit = True # update
            # else hit=False : append
            lignes.append(uneligne)
    # now write file and append new bus line if any
    with open("fd78csv", "w") as fd:
        skr = csv.DictWriter(fd, fieldnames=titres)
        skr.writeheader()
        for raw in lignes:
            skr.writerow(raw)
        if hit == False:
            # add new line for bus with updated value and others=0
            nline = {'BUS':bus, 'fd1':0, 'fd2':0, 'fd3':0, 'md2':0, 'md3':0}
            nline[col] = vfd
            print("---------> Adding line {} to csv file.".format(nline))
            skr.writerow(nline)
        else:
            print("---------> csv file modified.")
