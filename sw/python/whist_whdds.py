'''
devised from spec_demo_user.py by Javier D. Garcia-Lasheras - CERN
'''

__author__ = "G. Goujon"
__copyright__ = "Copyright 2016, ESRF"
__license__ = ""
__version__ = "1.0"
__maintainer__ = "G. Goujon"
__email__ = "goujon@esrf.fr"
__status__ = "Development"


from whist_libc import *


class SpecWhist(Whist):
    '''Creates objets necessary to access resources on WHIST mezzanine plugged to SPEC board
       through an Etherbone connection.
    '''

    def __init__(self, ip=-1, iface='eth1', libc='/segfs/linux/timing/whist/labo/dep/lib/libwhist.so'):

        Whist.__init__(self, ip=ip, iface=iface,libc=libc)

        self.TopCrossBar = 0x100000

        self.ModulesMap = {
            'buclk': 0x10000,
            'blram': 0x18000,
            'wrnc': 0x20000,
            'ddsHMQIN': 0x24000,
            'ddsHMQOUT': 0x28000,
            'bccHMQIN': 0x24400,
            'bccHMQOUT': 0x28400,
            'evtHMQIN': 0x24800,
            'evtHMQOUT': 0x28800,
        }

        self.BuclkMap = {
            'CTRL': 0x00,
            'CSR0': 0x04,
            'CSR1': 0x08,
            'CSR2': 0x0c,
            'BLISTAD': 0x10,
            'BUN_DEL': 0x14,
            'BUN_WID': 0x18,
            'STAMP_CTRL': 0x1c,
            'RF_PPS_LO': 0x20,
            'RF_PPS_HI': 0x24,
            'RF_FREQ': 0x28,
            'T0_LOST_CNT': 0x2c,
            'T0_CNT': 0x30,
            'LATCH': 0x34,
            'LATCH2': 0x38,
            'T0_DEL': 0x3c,
            'HIST0': 0xa0,
            'HIST1': 0xa4,
            'HIST2': 0xa8,
            'DDT_HIST': 0xac,
            'STAMP_BASE': 0x400,
        }

        self.PulseOutMap = {
            'P_DEL_0': 0x40,
        }

        self.Csr0_Offsets = {
            'INJERRST': 0,
            '352RST': 1,
            'PPSACK': 2,
            'BLISTINC': 3,
            'BLISTRST': 4,
            'SYNCRF': 5,
            'MBLPRST': 6,
            'RFLRAZ': 7,
            'PDLKD': 8,
            '352LKD': 9,
            'RTD3S': 10,
            'RFOE': 11,
            'INJERR': 12,
            'T0LOST': 13,
            'SLAVE': 14,
            'QUIET': 15,
            'RFLOST': 16,
            'RFADJD': 17,
            'RFADJRUN': 18,
            'SYNCSRCEN': 23,
            'RFDBGEN': 24,
            'RFADJEN': 25,
            'RFADJSTOP': 26,
            'JUMP44': 30,
            'SKIP44': 31,
        }

        self.Csr1_Offsets = {
            'WROK': 0,
            'WRLOST': 1,
            'PPSRDY': 2,
            'SLOKRUN': 3,
            'SYNCSRCACK': 4,
            'BU44LSB': 8,
            'CSR1DBG': 16,
            'T0WD': 22,
            'FLASHED': 23,
            'PLLOSS': 24,
            'PLLSTATUS': 25,
            'SLOKAOK': 26,
            'SLOKNOK': 27,
            'SRAOK': 28,
            'SRNOK': 29,
            'BOAOK': 30,
            'BONOK': 31,
        }

        self.CtrlOffsets = {
            'BUN_EN': 0,
            'RF_EN': 1,
            'GUNIEN': 2,
            'T0WDEN': 3,
            'DDSLKDEN': 4,
            'ROTINJ': 5,
            'ADJOUT': 6,
            'ADJ352': 9,
            'T0WDTRIG' : 12,
            'Version': 16,
        }

        self.BlistAddOffsets = {
            'BLI_ART': 0,
            'MB_EN': 13,
            'AUTOBLIST': 14,
            'AUTOROTINJ': 15,
            'BLI_TOP': 16,
            'ROTICODE': 29,
        }

        self.StampOffsets = {
            'READ': 0,
            'RES': 4,
            'ENA': 8,
            'POL': 12,
            'EPTY': 16,
            'FULL': 20,
            'DDT': 30,
            'DDTGATE': 31,
        }

        self.PulOffsets = {
            'ENAB': 31,
            'MODE': 29,
            'POLA': 28,
            'SRCE': 24,
        }

        self.CtrlMasks = {
            'SPOLVAL': 0xF,
            'ADJ352': 0x00000e00,
            'ADJOUT': 0x000001c0,
            'ADJVAL': 0x7,
       }

        self.PulMasks = {
            'SRCE': 0xF000000,
            'MODE': 0x60000000,
            'WSRCE': 0xF,
            'WMODE': 0x3,
            'WIDVAL': 0xFFFFFF,
            'SRDIVPHVAL': 0x3FFFFF,
            'DELVAL': 0xFFFFFFFF,
       }

        self.RegMasks = {
            'CSR1DBG': 0x003F0000,
            'LSW': 0x0000FFFF,
            'LSWMSB': 0x0000FF00,
            'MSW': 0xFFFF0000,
            'LSTSIZE': 0x000007FF,
            'RAMMSK': 0x07FFF,
            'SMSBIT': 0x8000,
            'VERS': 0xFFFF0000,
            'BLI_VAL': 0x1FFF,
            'BLI_ART': 0x1FFF,
            'BLI_TOP': 0x1FFF0000,
            'BOO_VAL': 0xF,
            'SR_VAL': 0x1F,
            'INJBOO': 0xF,
            'INJSR': 0x1F00,
            'EXTBOO': 0xF0000,
            'EXTSR': 0x1F000000,
            'T0DEL': 0x3FFFFFFF,
            'T0MUX': 0xC0000000,
            'T0MUXVAL': 0x3,
            'T0WDTRIG_VAL': 0xF,
            'T0WDTRIG': 0xF000,
       }

        self.RegOffsets = {
            'INJBOO': 0,
            'INJSR': 8,
            'EXTBOO': 16,
            'EXTSR': 24,
            'T0MUX': 30,
        }

    # CTRL bits
    # BUNEN

    def ddsSetBunchEn(self):
        '''Set BUN_EN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['BUN_EN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestBunchEn(self):
        '''Return BUN_EN bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['BUN_EN']
        return self.whistTestBit(address, offset)

    def ddsClearBunchEn(self):
        '''Clear BUN_EN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['BUN_EN']
        self.whistClearBit(address, offset)

    # RF_EN
    def ddsSetRfEn(self):
        '''Set RF_EN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['RF_EN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestRfEn(self):
        '''Return RF_EN bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['RF_EN']
        return self.whistTestBit(address, offset)

    def ddsClearRfEn(self):
        '''Clear RF_EN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['RF_EN']
        self.whistClearBit(address, offset)

    # Gun Inj Enable
    def ddsSetGuniEn(self):
        '''Set GUNI_EN  bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['GUNIEN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestGuniEn(self):
        '''Return GUNI_EN bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['GUNIEN']
        return self.whistTestBit(address, offset)

    def ddsClearGuniEn(self):
        '''Clear GUNI_EN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['GUNIEN']
        self.whistClearBit(address, offset)

    # T0 Watch Dog Enable
    def ddsSetT0WdEn(self):
        '''Set GUNI_EN  bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['T0WDEN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestT0WdEn(self):
        '''Return GUNI_EN bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['T0WDEN']
        return self.whistTestBit(address, offset)

    def ddsClearT0WdEn(self):
        '''Clear GUNI_EN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['T0WDEN']
        self.whistClearBit(address, offset)
        
    # DDS Slave_PLL Lock Detect
    def ddsSetDdsLkdEn(self):
        '''Set Enable Slave Latched PLL status bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['DDSLKDEN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestDdsLkdEn(self):
        '''Return Enable Slave Latched PLL status bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['DDSLKDEN']
        return self.whistTestBit(address, offset)

    def ddsClearDdsLkdEn(self):
        '''Clear Enable Slave Latched PLL status bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['DDSLKDEN']
        self.whistClearBit(address, offset)

    # ROTINJ
    def ddsSetRotinj(self):
        '''Set RFSEL  bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['ROTINJ']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestRotinj(self):
        '''Return RFSEL bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['ROTINJ']
        return self.whistTestBit(address, offset)

    def ddsClearRotinj(self):
        '''Clear RFSEL bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        offset = self.CtrlOffsets['ROTINJ']
        self.whistClearBit(address, offset)

    # Version
    def ddsGetFPGA(self, hexformat=False):
        '''Return the hex value FPGA version.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['VERS']) >> 16
        return register

    # CSR#0 Control Bits
    # RFADJEN
    def ddsSetRfAdjEn(self):
        '''Set RFADJEN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFADJEN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestRfAdjEn(self):
        '''Return RFADJEN bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFADJEN']
        return self.whistTestBit(address, offset)

    def ddsClearRfAdjEn(self):
        '''Clear RFADJEN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFADJEN']
        self.whistClearBit(address, offset)

    # RFDBGEN
    def ddsSetRfDbgEn(self):
        '''Set RFDBGEN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFDBGEN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestRfDbgEn(self):
        '''Return RFDBGEN bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFDBGEN']
        return self.whistTestBit(address, offset)

    def ddsClearRfDbgEn(self):
        '''Clear RFDBGEN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFDBGEN']
        self.whistClearBit(address, offset)

    # RFADJSTOP
    def ddsSetRfAdjStop(self):
        '''Set RFADJSTOP bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFADJSTOP']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestRfAdjStop(self):
        '''Return RFADJSTOP bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFADJSTOP']
        return self.whistTestBit(address, offset)

    def ddsClearRfAdjStop(self):
        '''Clear RFADJSTOP bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFADJSTOP']
        self.whistClearBit(address, offset)

    # BLISTADD Control Bits
    # MB_EN
    def ddsSetMBEn(self):
        '''Set Multi Bunch Enable bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['MB_EN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestMBEn(self):
        '''Return Multi Bunch Enable bit status.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['MB_EN']
        return self.whistTestBit(address, offset)

    def ddsClearMBEn(self):
        '''Clear Multi Bunch Enable bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['MB_EN']
        self.whistClearBit(address, offset)

    # AUTOBLIST
    def ddsSetAutoBlist(self):
        '''Set AUTOBLIST  bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['AUTOBLIST']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestAutoBlist(self):
        '''Return AUTOBLIST bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['AUTOBLIST']
        return self.whistTestBit(address, offset)

    def ddsClearAutoBlist(self):
        '''Clear AUTOBLIST bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['AUTOBLIST']
        self.whistClearBit(address, offset)

    # AUTOROTINJ
    def ddsSetAutoRotinj(self):
        '''Set AUTOROTINJ  bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['AUTOROTINJ']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestAutoRotinj(self):
        '''Return AUTOROTINJ bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['AUTOROTINJ']
        return self.whistTestBit(address, offset)

    def ddsClearAutoRotinj(self):
        '''Clear AUTOROTINJ bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['AUTOROTINJ']
        self.whistClearBit(address, offset)

    # ROTICODE
    def ddsSetRotiCode(self):
        '''Set ROTICODE  bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['ROTICODE']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestRotiCode(self):
        '''Return ROTICODE bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['ROTICODE']
        return self.whistTestBit(address, offset)

    def ddsClearRotiCode(self):
        '''Clear ROTICODE bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        offset = self.BlistAddOffsets['ROTICODE']
        self.whistClearBit(address, offset)

    # Bunch List Pointers
    def ddsGetBlistStart(self, hexformat=True):
        '''Return the hex value bunch list start pointer.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & self.RegMasks['BLI_ART']
        if hexformat:
            register = hex(register)
        return register

    def ddsSetBlistStart(self, value):
        '''Set the hex value bunch list start pointer.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.RegMasks['BLI_ART']
        value = value & self.RegMasks['BLI_VAL']
        register = register | value
        self.whistWriteL(address, register)
        return hex(register)

    def ddsGetBlistStop(self, hexformat=True):
        '''Return the hex value bunch list stop pointer.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['BLI_TOP']) >> 16
        if hexformat:
            register = hex(register)
        return register

    def ddsSetBlistStop(self, value):
        '''Set the hex value bunch list stop pointer.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BLISTAD']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.RegMasks['BLI_TOP']
        value = (value & self.RegMasks['BLI_VAL']) << 16
        register = register | value
        self.whistWriteL(address, register)
        return hex(register)

    # CTRL RW bits
    # ADJ352 : <2:0> 
    def ddsGetFpgaPhAdj(self, hexformat=True):
        '''Return the hex value cnt_adj register.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.CtrlMasks['ADJ352']) >> self.CtrlOffsets['ADJ352']
        if hexformat:
            register = hex(register)
        return register

    def ddsSetFpgaPhAdj(self, value):
        '''Set CTRL cnt_adj.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] &  ~self.CtrlMasks['ADJ352']
        value = (value & self.CtrlMasks['ADJVAL']) << self.CtrlOffsets['ADJ352']
        register = register | value
        self.whistWriteL(address, register)
        return register

    def ddsGetOutckPhAdj(self, hexformat=True):
        '''Return the hex value cnt_adj register.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.CtrlMasks['ADJOUT']) >> self.CtrlOffsets['ADJOUT']
        if hexformat:
            register = hex(register)
        return register

    def ddsSetOutckPhAdj(self, value):
        '''Set CTRL cnt_adj.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] &  ~self.CtrlMasks['ADJOUT']
        value = (value & self.CtrlMasks['ADJVAL']) << self.CtrlOffsets['ADJOUT']
        register = register | value
        self.whistWriteL(address, register)
        return register

    def ddsSetT0WdTrig(self, value):
        '''Set T0 Watch Dog trigger register value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] &  ~self.RegMasks['T0WDTRIG']
        value = (value & self.RegMasks['T0WDTRIG_VAL']) << self.CtrlOffsets['T0WDTRIG']
        register = register | value
        self.whistWriteL(address, register)
        return register

    def ddsGetT0WdTrig(self, hexformat=True):
        '''Return T0 Watch Dog trigger register value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CTRL']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['T0WDTRIG']) >> self.CtrlOffsets['T0WDTRIG']
        if hexformat:
            register = hex(register)
        return register

    # STAMPING register
    # W only Monostable bits
    # read FIFO
    def ddsReadInFifo(self, valch):
        '''Read strobe for next fifo data into wishbone register.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['READ']+valch
        self.whistSetBit(address, offset)

    # reset FIFO
    def ddsResetInFifo(self, valch):
        '''Set reset fifo bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['RES']+valch
        self.whistSetBit(address, offset)

    # Test FIFO empty bits
    def ddsTestStampEpty(self, valch):
        '''Return the fifo empty BIT.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['EPTY']+valch
        return self.whistTestBit(address, offset)

    # Test FIFO full bits
    def ddsTestStampFull(self, valch):
        '''Return the fifo full BIT.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['FULL']+valch
        return self.whistTestBit(address, offset)

    # read full register content
    def ddsGetStamp(self, hexformat=True):
        '''Return the hex value stamping register value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
        else:
            register = reg[0]
        return register

    # inputs polarity
    def ddsTestInPol(self, valch):
        '''Return the input channel polarity BIT.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['POL']+valch
        return self.whistTestBit(address, offset)

    def ddsSetInPol(self, valch):
        '''Set input polarity BIT.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['POL']+valch
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsClearInPol(self, valch):
        '''Clear input polarity BIT.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['POL']+valch
        self.whistClearBit(address, offset)

    # inputs function enable
    def ddsTestInEn(self, valch):
        '''Return the input channel function enable BIT.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['ENA']+valch
        return self.whistTestBit(address, offset)

    def ddsSetInEn(self, valch):
        '''Set input channel function enable BIT.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['ENA']+valch
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsClearInEn(self, valch):
        '''Clear inpu channel function enable BIT.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['ENA']+valch
        self.whistClearBit(address, offset)

    # DDT test control BITs
    # DDT
    def ddsTestddt(self):
        '''Return DDT bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['DDT']
        return self.whistTestBit(address, offset)

    def ddsSetDdt(self):
        '''Set DDT bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['DDT']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsClearDdt(self):
        '''Clear DDT bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['DDT']
        self.whistClearBit(address, offset)

    # DDT_GATE
    def ddsTestDdtGate(self):
        '''Return DDT_GATE bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['DDTGATE']
        return self.whistTestBit(address, offset)

    def ddsSetDdtGate(self):
        '''Set DDT_GATE bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['DDTGATE']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsClearDdtGate(self):
        '''Clear DDT_GATE bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_CTRL']
        offset = self.StampOffsets['DDTGATE']
        self.whistClearBit(address, offset)

    # CSR W only Monostable bits
    # INJERR_RESET
    def ddsSetInjerRst(self):
        '''Set T0 bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['INJERRST']
        self.whistSetBit(address, offset)

    # 352RSTtr
    def ddsSetRfRst(self):
        '''Set 352RST bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['352RST']
        self.whistSetBit(address, offset)

    # PPSACK
    def ddsSetPpsAck(self):
        '''Set PPSACK bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['PPSACK']
        self.whistSetBit(address, offset)

    # Bunch List Increment
    def ddsSetBlistInc(self):
        '''Set BLISTINC bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['BLISTINC']
        self.whistSetBit(address, offset)

    # Bunch List Pointer Reset
    def ddsSetBlistRst(self):
        '''Set BLISTRST bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['BLISTRST']
        self.whistSetBit(address, offset)

    # SYNCRF
    def ddsSetSyncRf(self):
        '''Set SYNCRF bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['SYNCRF']
        self.whistSetBit(address, offset)

    # RFLRAZ
    def ddsSetRflRaz(self):
        '''Set RFLRAZ bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFLRAZ']
        self.whistSetBit(address, offset)

    # SKIP44
    def ddsSetSkip44(self):
        '''Set SKIP44 bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['SKIP44']
        self.whistSetBit(address, offset)

    # JUMP44
    def ddsSetJump44(self):
        '''Set JUMP44 bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['JUMP44']
        self.whistSetBit(address, offset)

    # CSR RW bits
    # FLASHED : read & clear

    def ddsTestFlashed(self):
        '''Return FLASHED bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['FLASHED']
        return self.whistTestBit(address, offset)

    def ddsClearFlashed(self):
        '''Clear FLASHED bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['FLASHED']
        self.whistClearBit(address, offset)

    # R only bits
    # RTD3S
    def ddsTestRtD3sRun(self):
        '''Return RtD3sRun bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RTD3S']
        return self.whistTestBit(address, offset)

    #RFoE
    def ddsTestRfoe(self):
        '''Return RFoE bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFOE']
        return self.whistTestBit(address, offset)

    # RTBCC
    # Slave bit
    def ddsTestSlave(self):
        '''Test SLAVE  bit in CSR0.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['SLAVE']
        return self.whistTestBit(address, offset)

    # BU44LSB
    def ddsGetBu44Lsb(self, hexformat=True):
        '''Return Bu44 LSByte from CSR#1.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['LSWMSB']) >> self.Csr1_Offsets['BU44LSB']
        if hexformat:
            register = hex(register)
        return register

    # CSR1DBG
    def ddsGetCsr1Dbg(self, hexformat=True):
        '''Return DBG field from CSR#1.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['CSR1DBG']) >> self.Csr1_Offsets['CSR1DBG']
        if hexformat:
            register = hex(register)
        return register

    # SYNCSRCACK
    def ddsTestSyncSrcAck(self):
        '''Return SYNCSRCACK bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['SYNCSRCACK']
        return self.whistTestBit(address, offset)

    # PPSRDY
    def ddsTestPpsRdy(self):
        '''Return PPSRDY bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['PPSRDY']
        return self.whistTestBit(address, offset)

    # 352LKD
    def ddsTest352Lkd(self):
        '''Return 352LKD bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['352LKD']
        return self.whistTestBit(address, offset)

    # PDLKD
    def ddsTestPdLkd(self):
        '''Return DDS Phase Detector Locked (PDLKD) bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['PDLKD']
        return self.whistTestBit(address, offset)

    # QUIET
    def ddsTestQuiet(self):
        '''Return DDS Phase Detector Locked (PDLKD) bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['QUIET']
        return self.whistTestBit(address, offset)

     # INJERR
    def ddsTestInjErr(self):
        '''Return INJERR bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['INJERR']
        return self.whistTestBit(address, offset)

    # WROK
    def ddsTestWrOK(self):
        '''Return WROK bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['WROK']
        return self.whistTestBit(address, offset)

    # WRLOST
    def ddsTestWrLost(self):
        '''Return WRLOST bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['WRLOST']
        return self.whistTestBit(address, offset)

    # T0 WATCH DOG
    def ddsTestT0Wd(self):
        '''Return T0WD wtch dog bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['T0WD']
        return self.whistTestBit(address, offset)

    # SLVRFLOK
    def ddsTestSlokRun(self):
        '''Return SLOKRUN bit value : slok process running.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['SLOKRUN']
        return self.whistTestBit(address, offset)

    def ddsTestSlokAok(self):
        '''Return SLOKAOK bit value : slok success.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['SLOKAOK']
        return self.whistTestBit(address, offset)

    def ddsTestSlokNok(self):
        '''Return SLOKNOK bit value : slok failed.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['SLOKNOK']
        return self.whistTestBit(address, offset)

    def ddsTestSRAok(self):
        '''Return SRAOK bit value : slok failed.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['SRAOK']
        return self.whistTestBit(address, offset)

    def ddsTestSRNok(self):
        '''Return SRAOK bit value : slok failed.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['SRNOK']
        return self.whistTestBit(address, offset)

    def ddsTestBoAok(self):
        '''Return SRAOK bit value : slok failed.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['BOAOK']
        return self.whistTestBit(address, offset)

    def ddsTestBoNok(self):
        '''Return SRAOK bit value : slok failed.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['BONOK']
        return self.whistTestBit(address, offset)

    # Slave PLL status
    def ddsTestPllStat(self):
        '''Return SLOKRUN bit value : slok process running.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['PLLSTATUS']
        return self.whistTestBit(address, offset)

    def ddsTestPllLoss(self):
        '''Return SLOKRUN bit value : slok process running.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        offset = self.Csr1_Offsets['PLLOSS']
        return self.whistTestBit(address, offset)

    # RFLOST
    def ddsTestRfLost(self):
        '''Return RFLOST bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFLOST']
        return self.whistTestBit(address, offset)

    # RFADJusteD
    def ddsTestRfAdjd(self):
        '''Return RFADJusteD bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFADJD']
        return self.whistTestBit(address, offset)

    # RFADJRUN
    def ddsTestRfAdjRun(self):
        '''Return RF_ADJUST_RUN bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['RFADJRUN']
        return self.whistTestBit(address, offset)

    # T0LOST
    def ddsTestT0Lost(self):
        '''Return T0LOST bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['T0LOST']
        return self.whistTestBit(address, offset)

    # CSR#2 Bunch number (Read only)
    def ddsGetBunchNum(self, hexformat=False):
        '''Get the current Bunch RegisterNumber value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR2']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & self.RegMasks['LSW']
        if hexformat:
            register = hex(register)
        return register

    # RF delta (Read only)
    def ddsGetRfDelta(self, hexformat=False):
        '''Get the RF delta value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR2']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['MSW']) >> 16
        if hexformat:
            register = hex(register)
        return register

    # Other registers
    # RF frequency
    def ddsGetRfFreq(self, hexformat=False):
        '''Get the RF Frequency value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['RF_FREQ']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsSetRfFreq(self, value):
        '''Set the RF Frequency'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['RF_FREQ']
        self.whistWriteL(address, value)
        return value

    # Delay
    # INJ
    def ddsGetInjDel(self, hexformat=True):
        '''Return the hex value injection delay as RF/8 periods.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BUN_DEL']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & self.RegMasks['LSW']
        if hexformat:
            register = hex(register)
        return register

    def ddsSetInjDel(self, value):
        '''Set the hex value injection delay as RF/8 periods.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BUN_DEL']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.RegMasks['LSW']
        value = value & self.RegMasks['LSW']
        register = register | value
        self.whistWriteL(address, register)
        return register

    # EXT
    def ddsGetExtDel(self, hexformat=True):
        '''Return the hex value extraction delay as super periods.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BUN_DEL']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['MSW']) >> 16
        if hexformat:
            register = hex(register)
        return register

    def ddsSetExtDel(self, value):
        '''Set the hex value extraction delay as super periods.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BUN_DEL']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.RegMasks['MSW']
        value = (value & self.RegMasks['LSW']) << 16
        register = register | value
        self.whistWriteL(address, register)
        return register

    # T0 delay
    def ddsGetT0Del(self, hexformat=True):
        '''Return the hex value of T0 delay register as RF/8 periods.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['T0_DEL']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & self.RegMasks['T0DEL']
        if hexformat:
            register = hex(register)
        return register

    def ddsSetT0Del(self, value):
        '''Return the hex value of T0 delay register as RF/8 periods.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['T0_DEL']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.RegMasks['T0DEL']
        value = value & self.RegMasks['T0DEL']
        register = register | value
        self.whistWriteL(address, register)
        return value

    def ddsGetT0Mux(self, hexformat=True):
        '''Return the hex value T0MUX in  register T0_DEL.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['T0_DEL']
        reg = self.whistReadL(address, hexformat=False)
        mask = self.RegMasks['T0MUX']
        register = (reg[0] & mask) >> self.RegOffsets['T0MUX']
        if hexformat:
            register = hex(register)
        return register

    def ddsSetT0Mux(self, value):
        '''Set T0MUX value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['T0_DEL']
        reg = self.whistReadL(address, hexformat=False)
        mask = ~self.RegMasks['T0MUX']
        register = reg[0] & mask
        value = (value & self.RegMasks['T0MUXVAL']) << self.RegOffsets['T0MUX']
        register = register | value
        self.whistWriteL(address, register)
        return register

    # Width
    def ddsGetInjWid(self, hexformat=True):
        '''Return the hex value injection width.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BUN_WID']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & self.RegMasks['LSW']
        if hexformat:
            register = hex(register)
        return register

    def ddsSetInjWid(self, value):
        '''Set the hex value injection width.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BUN_WID']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.RegMasks['LSW']
        value = value & self.RegMasks['LSW']
        register = register | value
        self.whistWriteL(address, register)
        return register

    def ddsGetExtWid(self, hexformat=True):
        '''Return the hex value extraction width.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BUN_WID']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['MSW']) >> 16
        if hexformat:
            register = hex(register)
        return register

    def ddsSetExtWid(self, value):
        '''Set the hex value extraction width.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['BUN_WID']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.RegMasks['MSW']
        value = (value & self.RegMasks['LSW']) << 16
        register = register | value
        self.whistWriteL(address, register)
        return register

    # RF_PPS double register (Read only)
    def ddsGetRfPps(self, hexformat=False):
        '''Return the hex double value RF counter at PPS.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['RF_PPS_LO']
        reglo = self.whistReadL(address, hexformat=False)
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['RF_PPS_HI']
        reghi = self.whistReadL(address, hexformat=False)
        register = (reghi[0] << 32) | reglo[0]
        if hexformat:
            register = hex(register)
        return [register,(reglo[1] & reghi[1])]
    
    # CSR1 debug Read Only registers
    def ddsGetInjBoo(self, hexformat=True):
        '''Return the hex value booster counter at injection.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] &  self.RegMasks['INJBOO'] >> self.RegOffsets['INJBOO']
        if hexformat:
            register = hex(register)
        return register

    def ddsGetInjSr(self, hexformat=True):
        '''Return the hex value SR counter at injection.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] &  self.RegMasks['INJSR']) >> self.RegOffsets['INJSR']
        if hexformat:
            register = hex(register)
        return register

    def ddsGetExtBoo(self, hexformat=True):
        '''Return the hex value booster counter at extraction.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] &  self.RegMasks['EXTBOO'] >> self.RegOffsets['EXTBOO']
        if hexformat:
            register = hex(register)
        return register

    def ddsGetExtSr(self, hexformat=True):
        '''Return the hex value SR counter at extraction.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR1']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] &  self.RegMasks['EXTSR']) >> self.RegOffsets['EXTSR']
        if hexformat:
            register = hex(register)
        return register

    # LATCH registers (Read only)
    def ddsGetLatchGunDel(self, hexformat=False):
        '''Return the hex value of DBG1.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['LATCH']
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & self.RegMasks['LSW']
        if hexformat:
            register = hex(register)
        return register

    def ddsGetLatchExtDel(self, hexformat=False):
        '''Return the hex value of DBG1.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['LATCH']
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.RegMasks['MSW']) >> 16
        if hexformat:
            register = hex(register)
        return register
        
    def ddsGetLatch2(self, hexformat=False):
        '''Return the hex value of DBG2.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['LATCH2']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsGetHist0(self, hexformat=False):
        '''Return the hex value of HIST0.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['HIST0']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsGetHist1(self, hexformat=False):
        '''Return the hex value of HIST1.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['HIST1']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsGetHist2(self, hexformat=False):
        '''Return the hex value of HIST2.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['HIST2']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsGetDdtHisto(self, hexformat=False):
        '''Return the hex value of DDT_HIST.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['DDT_HIST']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    # T0 counter (Read only)
    def ddsGetT0Cnt(self, hexformat=False):
        '''Return the hex value of T0 counter.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['T0_CNT']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    # T0_LOST counter (Read only)
    def ddsGetT0LostCnt(self, hexformat=True):
        '''Return the hex value of T0_LOST counter.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['T0_LOST_CNT']
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    # Bunch List RAM
    def ddsSetBL(self, valad, valda):
        '''Write Bunch List Ram 16-bit word : address, data'''
        address = self.TopCrossBar + self.ModulesMap['blram'] + (valad & self.RegMasks['RAMMSK'])
        self.whistWriteL(address, valda)
        return valda

    def ddsGetBL(self, valad, hexformat=True):
        '''Return bunch list value at address.'''
        address = self.TopCrossBar + self.ModulesMap['blram'] + (valad & self.RegMasks['RAMMSK'])
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    # other RW bits
    # SR_count clocks synchronization
    def ddsSetSyncSrcEn(self):
        '''Set SYNC_SRC_EN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['SYNCSRCEN']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestSyncSrcEn(self):
        '''Return SYNC_SRC_EN bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['SYNCSRCEN']
        return self.whistTestBit(address, offset)

    def ddsClearSyncSrcEn(self):
        '''Clear SYNC_SRC_EN bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['CSR0']
        offset = self.Csr0_Offsets['SYNCSRCEN']
        self.whistClearBit(address, offset)

    # Pulses / Clocks type
    # OUTPUT ENABLE in Width Register of channel#
    def ddsSetPuCkEn(self, channel):
        '''Set OUTPUT_ENABLE bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        offset = self.PulOffsets['ENAB']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestPuCkEn(self, channel):
        '''Return OUTPUT_ENABLE bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        offset = self.PulOffsets['ENAB']
        return self.whistTestBit(address, offset)

    def ddsClearPuCkEn(self, channel):
        '''Clear OUTPUT_ENABLE bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        offset = self.PulOffsets['ENAB']
        self.whistClearBit(address, offset)

    # POLARITY in Width Register of channel#
    def ddsSetPuCkPol(self, channel):
        '''Set POL bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        offset = self.PulOffsets['POLA']
        self.whistSetBit(address, offset)
        register = self.whistReadL(address, hexformat=False)
        return hex(register[0])

    def ddsTestPuCkPol(self, channel):
        '''Return POL bit value.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        offset = self.PulOffsets['POLA']
        return self.whistTestBit(address, offset)

    def ddsClearPuCkPol(self, channel):
        '''Clear POL bit.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        offset = self.PulOffsets['POLA']
        self.whistClearBit(address, offset)

    # Mode bitfield in Width register of channel#
    def ddsSetPuCkMode(self, channel, value):
        '''Set PULSE mode on output channel#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & ~self.PulMasks['MODE'])
        value = (value & self.PulMasks['WMODE']) << self.PulOffsets['MODE']
        register = register | value
        self.whistWriteL(address, register)
        return register

    def ddsGetPuCkMode(self, channel, hexformat=False):
        '''Return PU/CK bit field from output channel#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.PulMasks['MODE']) >> self.PulOffsets['MODE']
        if hexformat:
            register = hex(register)
        return register

    # Pulses / Clocks source
    # Srce bitfield in Width register of channel#
    def ddsGetPuCkSrce(self, channel, hexformat=False):
        '''Get Pulse / clock source from channel output#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & self.PulMasks['SRCE']) >> self.PulOffsets['SRCE']
        if hexformat:
            register = hex(register)
        return register

    def ddsSetPuCkSrce(self, channel, value):
        '''Set Pulse / clock source from channel output#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        reg = self.whistReadL(address, hexformat=False)
        register = (reg[0] & ~self.PulMasks['SRCE'])
        value = (value & self.PulMasks['WSRCE']) << self.PulOffsets['SRCE']
        register = register | value
        self.whistWriteL(address, register)
        return register

    # Pulses / clocks outputs
    # Delay register of channel#
    def ddsGetPulDel(self, channel, hexformat=True):
        '''Return the hex value delay register of channel#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 8*channel
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsSetPulDel(self, channel, value):
        '''Set the hex value delay of channel#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 8*channel
        value = value & self.PulMasks['DELVAL']
        self.whistWriteL(address, value)
        return value

    # Pulses / clocks outputs
    # Width register of channel#
    def ddsGetPulWid(self, channel, hexformat=True):
        '''Return the hex value width register of channel#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & self.PulMasks['WIDVAL']
        if hexformat:
            register = hex(register)
            return register
        else:
            return register

    def ddsSetPulWid(self, channel, value):
        '''Set the hex value width of channel#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.PulMasks['WIDVAL']
        value = value & self.PulMasks['WIDVAL']
        register = register | value
        self.whistWriteL(address, register)
        return register

    def ddsSetSrDivPh(self, channel, value):
        '''Set SRDIV coarse phase value in width register of channel#.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.PulseOutMap['P_DEL_0'] + 4 + 8*channel
        reg = self.whistReadL(address, hexformat=False)
        register = reg[0] & ~self.PulMasks['SRDIVPHVAL']
        value = value & self.PulMasks['SRDIVPHVAL']
        register = register | value
        self.whistWriteL(address, register)
        return register

    # Cores
    # dds
    def ddsSetCoreDds(self, reg, value):
        '''Set something in dds core'''
        address = self.TopCrossBar + self.ModulesMap['ddsHMQIN'] + reg
        self.whistWriteL(address, value)
        return value

    def ddsGetCoreDds(self, reg, hexformat=False):
        '''Get from dds core'''
        address = self.TopCrossBar + self.ModulesMap['ddsHMQOUT'] + reg
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsDisCoreDds(self):
        '''Discard dds core outgoing queue'''
        address = self.TopCrossBar + self.ModulesMap['ddsHMQOUT'] + 0
        self.whistWriteL(address, 0x8000000)
        return address

    def ddsPurCoreDds(self):
        '''Purge dds core outgoing queue'''
        address = self.TopCrossBar + self.ModulesMap['ddsHMQOUT'] + 0
        self.whistWriteL(address, 0x2000000)
        return address

    # bcc
    def ddsSetCoreBcc(self, reg, value):
        '''Set something in bcc core'''
        address = self.TopCrossBar + self.ModulesMap['bccHMQIN'] + reg
        self.whistWriteL(address, value)
        return value

    def ddsGetCoreBcc(self, reg, hexformat=False):
        '''Get from bcc core'''
        address = self.TopCrossBar + self.ModulesMap['bccHMQOUT'] + reg
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsDisCoreBcc(self):
        '''Discard bcc core outgoing queue'''
        address = self.TopCrossBar + self.ModulesMap['bccHMQOUT'] + 0
        self.whistWriteL(address, 0x8000000)
        return address

    def ddsPurCoreBcc(self):
        '''Purge bcc core outgoing queue'''
        address = self.TopCrossBar + self.ModulesMap['bccHMQOUT'] + 0
        self.whistWriteL(address, 0x2000000)
        return address

    # evt
    def ddsSetCoreEvt(self, reg, value):
        '''Set something in evt core'''
        address = self.TopCrossBar + self.ModulesMap['evtHMQIN'] + reg
        self.whistWriteL(address, value)
        return value

    def ddsGetCoreEvt(self, reg, hexformat=False):
        '''Get from evt core'''
        address = self.TopCrossBar + self.ModulesMap['evtHMQOUT'] + reg
        reg = self.whistReadL(address, hexformat=False)
        if hexformat:
            register = hex(reg[0])
            return register
        else:
            return reg[0]

    def ddsDisCoreEvt(self):
        '''Discard evt core outgoing queue'''
        address = self.TopCrossBar + self.ModulesMap['evtHMQOUT'] + 0
        self.whistWriteL(address, 0x8000000)
        return address

    def ddsPurCoreEvt(self):
        '''Purge evt core outgoing queue'''
        address = self.TopCrossBar + self.ModulesMap['evtHMQOUT'] + 0
        self.whistWriteL(address, 0x2000000)
        return address

    #LM32
    def ddsLm32Restart(self, index):
        '''Restart LM32 core'''
        address = self.TopCrossBar + self.ModulesMap['wrnc']
        self.whistWrncLm32Restart(address, index)
        return index

    def ddsLm32Loader(self, index, path):
        '''Load LM32 program'''
        address = self.TopCrossBar + self.ModulesMap['wrnc']
        self.whistWrncLm32Loader(address, index, path)
        return index

    # Read stamps (Read only)

    def ddsGetTrigStamp(self, channel, hexformat=False):
        '''Return the hex value of trigger input stamp.'''
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_BASE'] + 8*channel
        reglo = self.whistReadL(address, hexformat=False)
        #print("ad {} lo {}".format(hex(address), reglo))
        address = self.TopCrossBar + self.ModulesMap['buclk'] + self.BuclkMap['STAMP_BASE'] + 4 + 8*channel
        reghi = self.whistReadL(address, hexformat=False)
        #print("ad {} hi {}".format(hex(address), reghi))
        register = (reghi[0] << 32) | reglo[0]
        if hexformat:
            register = hex(register)
        return register


