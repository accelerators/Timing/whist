/*
 * This work is part of the White Rabbit Node Core project.
 *
 * Copyright (C) 2013-2015 CERN (www.cern.ch)
 * Author: Tomasz Wlostowski <tomasz.wlostowski@cern.ch>
 *
 * Released according to the GNU GPL, version 2 or any later version.
 */


/*.
 * WR Distributed DDS Realtime Firmware.
 *
 * ad9510_master_config.h: Master mode (RF IN->Phase detector) config registers for AD9510
 * on the FMC DDS v2 mezzanine.
 */

const struct ad95xx_reg ad9510_slave_config[] = {

{0x00, 0x90}, //[7] : 1=SDO tristate
              //[4] : 1=long instruction (mandatory)
//0x01,2,3 not used
{0x04, 0x00}, //Acounter=0
{0x05, 0x00}, //Bcounter_MSB=0
{0x06, 0x04}, //Bcounter=4
{0x07, 0x00}, //LOR function disabled
{0x08, 0x77}, //was 0... set to :
              //[6]PFD polarity=1 positive
              //[5..2] muxout on STATUS=Dh (loss of ref OR loss of DLD)
              //[1..0] charge pump normal operation=3
{0x09, 0x00}, //[6..4] charge pump current = 000 = default = 0.6mA
{0x0A, 0x04}, //slave : PLL powerdown unset, B not bypassed, prescaler=1=FD_div_2
{0x0B, 0x00}, //slave : R divider
{0x0C, 0x00}, //slave : R divider : 0=RF/8 from DAC
{0x0D, 0x40}, //[1..0] antibacklash to default
              //[5] digital lock detect window to default
              //[6] 1=lock detect disabled
{0x34, 0x00}, //OUT5 : enable delay block
{0x35, 0x1b}, //[2..0] : OUT5 : 800uA
              //[5..3] : OUT5 : 2 capacitors
{0x36, 0x00}, //OUT5 : programmable delay value from 0x0 to 0x18 (25 values)
//0x37 not used must read 4
{0x38, 0x00}, //OUT6 : enable delay block
{0x39, 0x1b}, //[2..0] : OUT6 : 800uA
              //[5..3] : OUT6 : 2 capacitors
{0x3A, 0x00}, //OUT6 : programmable delay value from 0x0 to 0x18 (25 values)
//0x3B not used must read 4
{0x3C, 0x08}, //OUT0: [1..0] 0= LVPECL ON -> RF_OUT
              //      [3..2] 2=default=810mV output
{0x3D, 0x08}, //OUT1: [1..0] 0= LVPECL ON -> CLK_PD
              //      [3..2] 2=default=810mV output
{0x3E, 0x08}, //OUT2: [1..0] 0= LVPECL ON -> CKRF_OUT
              //      [3..2] 2=default=810mV output
{0x3F, 0x0A}, //OUT3: [1..0] 2= safe power down = LVPECL OFF
              //      [3..2] 2=default=810mV output
{0x40, 0x02}, //OUT4: [0]    : 0=LVDS ON
              //      [2..1] : 1=default=3.5mA on 100 Ohms
              //      [3]    : 0=LVDS
              //      [4]    : not relevant for LVDS
{0x41, 0x02}, //OUT5: [0]    : 0=LVDS ON
              //      [2..1] : 1=default=3.5mA on 100 Ohms
              //      [3]    : 0=LVDS
              //      [4]    : not relevant for LVDS
{0x42, 0x02}, //OUT6: [0]    : 0=LVDS ON
              //      [2..1] : 1=default=3.5mA on 100 Ohms
              //      [3]    : 0=LVDS
              //      [4]    : not relevant for LVDS
{0x43, 0x02}, //OUT7: [0]    : 0=LVDS ON
              //      [2..1] : 1=default=3.5mA on 100 Ohms
              //      [3]    : 0=LVDS
              //      [4]    : not relevant for LVDS
//0x44 not used
{0x45, 0x02}, //slave :select CLK2 input, PLL prescaler ON, REFIN ON, CLK1=PD
//0x46 not used
//0x47 not used
{0x48, 0x00}, //OUT0 divider : RF_OUT : 0 (divider bypassed below)
              //[3..0] : clocks high : default 0
              //[7..4] : clocks low  : default 0
{0x49, 0x80}, //OUT0 : Bypass divider, phase offset=0
{0x4A, 0x33}, //OUT1 divider : CLK_PD = RF/8
              //[3..0] : clocks high : 3=4 cycles
              //[7..4] : clocks low  : 3=4 cycles
{0x4B, 0x00}, //OUT1 :  divider NOT bypassed, phase offset=0
{0x4C, 0x00}, //OUT2 divider : CKRF_OUT programmable
              //[3..0] : clocks high : default 0 / programmable
              //[7..4] : clocks low  : default 0 / programmable
{0x4D, 0x00}, //OUT2 :  divider NOT bypassed, phase offset=0
{0x4E, 0x00}, //OUT3 : WHIST : unused
{0x4F, 0x00}, //OUT3 : WHIST : unused
{0x50, 0x33}, //OUT4 divider : LVDS CLK2OUT_P/N = RF/8
              //[3..0] : clocks high : 3=4 cycles
              //[7..4] : clocks low  : 3=4 cycles
{0x51, 0x00}, //OUT4 : divider NOT bypassed, phase offset=0
{0x52, 0x00}, //OUT5 divider : LVDS DLV2_P/N : RF
              //[3..0] : clocks high : default 0
              //[7..4] : clocks low  : default 0
{0x53, 0x80}, //OUT5 : Bypass divider, phase offset=0
{0x54, 0x00}, //OUT6 divider : LVDS DLV1_P/N : RF
              //[3..0] : clocks high : default 0
              //[7..4] : clocks low  : default 0
{0x55, 0x80}, //OUT6 : Bypass divider, phase offset=0
{0x56, 0x00}, //OUT7 divider : LVDS RF_LV_P/N : RF
              //[3..0] : clocks high : default 0
              //[7..4] : clocks low  : default 0
{0x57, 0x80}, //OUT7 : Bypass divider, phase offset=0
{0x58, 0x00}, //disable SYNC detect for STATUS read
//0x59 not used
{0x5A, 0x01}  //self cleaning update registers command

};
