CC  := gcc
CXX := g++
AR  := ar

CXX11 = -std=c++0x
CXXFLAGS = -g -fPIC -D_REENTRANT $(CXX11)
CXXFLAGS += -Wall
LFLAGS := -lstdc++ -shared

OS = $(shell /csadmin/common/scripts/get_os)

# libwhist
LIBNAME := libwhist
SO_FILE := $(LIBNAME).so
A_FILE := $(LIBNAME).a
INCLUDES := $(wildcard *.h)

#SRC=$(wildcard *.cpp)
DIR_OBJS=obj/$(OS)
DIR_OUTS=bin/$(OS)
#OBJS=$(SRC:.cpp=.o)
OBJS = $(DIR_OBJS)/EbClient.o \
    $(DIR_OBJS)/EbPacket.o \
    $(DIR_OBJS)/EbRecord.o \
    $(DIR_OBJS)/FlashAccess.o \
    $(DIR_OBJS)/pywrapper.o \
    $(DIR_OBJS)/SoftcoreUtil.o



WRNC_INC  = ../rt_d3s/include
CXXFLAGS += -I. -I$(WRNC_INC)

#doxygen
DOXYGEN = doxygen
DOXYFILE = doxygen/doxyfile
export GIT_HEAD_REV := $(shell git rev-parse HEAD | cut -c 1-7)

.PHONY: clean all lib doc install

all:
	@echo "Usage: "
	@echo "    make lib           : generate lib"
	@echo "    make doc           : generate the library documentation"
	@echo "    make install       : install the library in the directory given in command line with the PREFIX variable"
	@echo "    make clean         : remove all generated files"

clean :
	rm -rf $(DIR_OBJS)
	rm -rf $(DIR_OUTS)
	rm -rf doxygen/doc

lib: $(DIR_OBJS) $(DIR_OUTS) $(A_FILE) $(SO_FILE)

$(DIR_OBJS):
	mkdir -p $(DIR_OBJS)

$(A_FILE): $(OBJS) 
	$(AR) rv $(DIR_OUTS)/$@ $^

$(SO_FILE): $(OBJS)
	$(CXX) $(LFLAGS) -o $(DIR_OUTS)/$@ $^

$(DIR_OUTS):
	mkdir -p $(DIR_OUTS)

$(DIR_OBJS)/%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

install:
	@if [ -z $(PREFIX) ]; then echo "you must define the destination directory with the PREFIX variable"; exit 1; fi
	@if [ ! -d $(PREFIX)/include ]; then mkdir -p $(PREFIX)/include; fi
	@if [ ! -d $(PREFIX)/lib ]; then mkdir -p $(PREFIX)/lib; fi
	install -m 0444 $(INCLUDES) $(PREFIX)/include
	install -m 0555 $(DIR_OUTS)/$(A_FILE) $(PREFIX)/lib/$(A_FILE)_$(GIT_HEAD_REV)
	install -m 0555 $(DIR_OUTS)/$(SO_FILE) $(PREFIX)/lib/$(SO_FILE)_$(GIT_HEAD_REV)

doc:
	$(DOXYGEN) $(DOXYFILE)
