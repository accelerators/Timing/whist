// ==============================================================================
/*!
 * @file   FlashAccess.h
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Class for interfacing an on-board memory FLASH used for the FPGA
 * configuration (storing bitstream)
 */
// ==============================================================================

#ifndef FLASHACCESS_H_
#define FLASHACCESS_H_

#include "EbClient.h"


#define ATTEMPS 2
#define FORMAT  0x14

#define CR    0x00
#define SR    0x04
#define GBBAR 0x08
#define MBBAR 0x0c
#define FAR   0x10

#define CS_HIGH 0x0
#define CS_LOW  0x8000000
#define START_SPI 0x4000000

#define SEND_1_BYTE 0x0
#define SEND_2_BYTE 0x1000000
#define SEND_3_BYTE 0x2000000

#define CMD_RELEASE_DPD  0xAB //release, exit deep power-down
#define CMD_DPD          0xB9 //enter deep power-down
#define CMD_WR_ENABLE    0x06 //Write Enable
#define CMD_WR_DISABLE   0x04 //Write Disable
#define CMD_RD_STATUS    0x05 //Read Status register
#define CMD_RD           0x03 //Read
#define CMD_RD_FAST      0x0B //Fast Read
#define CMD_PG_PROGRAM   0x02 //Page Program
#define CMD_BK_ERASE     0xC7 //Bulk Erase: Erase whole FLASH
#define CMD_SECTOR_ERASE 0xD8
#define DUMMY_BYTE       0x00

#define BULK_ERASE_TIME   23000 //in milliseconds
#define SECTOR_ERASE_TIME 600   //in milliseconds

//FLASH size: 4194304 bytes (16384 pages of 256 bytes | 64 sectors of 65536 bytes (256 pages))
#define FLASH_SIZE        0x3FFFFF
#define FLASH_PAGE_SIZE   256 //bytes
#define FLASH_SECTOR_SIZE 0x10000  //65536 bytes
#define FLASH_MAX_SECTOR  64

/*!
 * This class allows to communicate with a memory FLAH in order to program a bitstream
 * or reconfigure the FPGA from the FLASH content. This class assumes that a xil_multiboot
 * module is instantiated in the WR node design.
 *
 * See the following page:<br>
 *  \li http://www.ohwr.org/projects/conv-ttl-blo-gw/wiki/Xil_multiboot
 *
 * @todo Add retry and EB R/W operation failure detection
 * 
 * @ingroup Utils
 * @headerfile whistlib.h
 */

class FlashAccess
{
public:
/*!
 * @name Constructor
 * Only one constructor is defined for this class */
//@{
/*!
 * The object constructor.
 *
 * @param _dev Pointer to an EbClient object.
 * @param bitfile The fullpath to the bitstream.
 * @param flashBaseAddress The offset of the xil_multiboot module in the wishbone memory map.
 * @param goldenAddr The address in the FLASH memory of the golden bitstream (bitstream loaded
 * in case of reconfigurtion failure).
 * @param multiAddr The address in the FLASH memory of the multiboot bitstream (bitstream loaded
 * at reconfiguration).
 */
  FlashAccess(EbClient *_dev, string bitfile, uint32_t flashBaseAddress,
              uint32_t goldenAddr, uint32_t multiAddr);
//@}


/*!
 * @name Destructor
 * Only one destructor is defined for this class */
//@{
/*!
 * The object destructor
 */
  virtual ~FlashAccess();
//@}


/*! @name Methods */
//{@
/*!
 * Load the multiboot bitstream from the FLASH to the FPGA.
 *
 * @return True in case of success, false otherwise.
 */
  bool load_bitstream(void);
/*!
 * Program a new bitstream in the FLASH memory at the multiboot address.
 *
 * @param hasGolden Specify if a golden bitstream is present in the FLASH
 * @param load Indicate if the FPGA must be reconfigured from the FLASH after
 * bitstream programming operation.
 * @return True in case of success, false otherwise.
 */
  bool program_bitstream(bool hasGolden, bool load);
/*!
 * Read a bitstream from the FLASH memory at the multiboot address.
 *
 * @param bitstrem Pointer to the bitstream bytes read from FLASH
 * @param size Size of the bitstream to read, i.e. number of bytes to read
 * @return True in case of success, false otherwise.
 */
  bool read_bitstream(uint8_t *bitstream, size_t size);
//@}


private:
  //attributes
  EbClient *dev;
  string filename;
  uint8_t *bitstream;
  uint32_t baseAddress;
  uint32_t gbAddr;
  uint32_t mbAddr;
  //method
  bool load_bitfile(size_t *bitstream_size, size_t *page_loop_size, size_t *left_bytes);
  bool flash_release_from_deep_power_down(void);
  void flash_deep_power_down(void);
  void flash_write_enable(void);
  void flash_bulk_erase(void);
  void flash_sector_erase(uint8_t sector);
  bool flash_page_program(uint32_t addr);
  bool flash_read(uint32_t addr, uint8_t *bitstream, size_t size);
  bool flash_send_bytes(uint8_t *buf, size_t num);
  bool flash_recv_bytes(uint8_t *buf, size_t num);
  bool flash_read_status(uint32_t *status);
  bool flash_wait_for_spi_transfer(void);
  bool flash_wait_for_wip(uint32_t time_out);
  bool flash_wait_for_pp_wip(uint32_t time_out);
};

#endif /* FLASHACCESS_H_ */
