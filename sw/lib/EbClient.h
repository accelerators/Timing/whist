// ==============================================================================
/*!
 * @file   EbClient.h
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Class for communicating with a WR node through the Etherbone protocol
 */
// ==============================================================================

#ifndef EBCLIENT_H_
#define EBCLIENT_H_

#include <iostream>
#include <netinet/in.h>
#include <vector>
#include <sstream>
#include <mutex>
#include <fcntl.h>
#include "EbRecord.h"
#include "EbPacket.h"
#include "EbClient.h"

using namespace std;

/*!
 * @def EB_PORT
 * @brief Port of Etherbone device
 */
#define EB_PORT       0xebd0
/*!
 * @def EB_MAX_RCOUNT
 * @brief Maximum number of words which can be read in an EB record
 */
#define EB_MAX_RCOUNT 0xff
/*!
 * @def EB_MAX_WCOUNT
 * @brief Maximum number of words which can be written in an EB record
 */
#define EB_MAX_WCOUNT 0xff

/*!
 * This class allows to communicate with a node of a White Rabbit (WR) network through the
 * Etherbone protocol (over UDP transport layer).
 *
 * See the following page and document for a complete specification of Etherbone
 * implementation and protocol:<br>
 *  \li http://www.ohwr.org/projects/etherbone-core/wiki
 *  \li http://www.ohwr.org/projects/etherbone-core/documents
 *
 * @todo Add a constructor opening the socket.
 * @todo Extend the size of block (current limitation of 256 words) for R/W operation. It
 * can be achieved by creating several EB record.
 * 
 * @ingroup Etherbone
 * @headerfile whistlib.h
 */

//NL: define what a EbClientGuard is.
//NL: idea is to use the initialization is acquisition paradigm to properly ensure thread 
//NL: safety on the EbClient singleton and avoid deadlocks.
typedef std::lock_guard<std::recursive_mutex> EbClientGuard;

class EbClient
{
public:

/*!
 * @name Constructor
 * Only one constructor is defined for this class */
//@{
/*!
 * The object constructor.
 *
 * @param nodeip The IP address of the Etherbone device (WR node)
 * @param iface The interface name of the computer connected on the WR network
 */
  EbClient(const char *nodeip, const char* iface);
//@}

/*!
 * @name Destructor
 * Only one destructor is defined for this class */
//@{
/*!
 * The object destructor
 */
  virtual ~EbClient();
//@}


//Attributes
private:
  //NL: use recursive mutex to avoid deadlocks along the call stack
  std::recursive_mutex etherboneMutex;
  const char *nodeIpAddress;
  const char *ifaceName;
  int sockfd;
  bool ebSocketOpened;
  struct sockaddr_in nodeAddr;
  socklen_t nodeAddrLen;
  struct sockaddr_in clientAddr;
  uint8_t retry;
  uint32_t recvTimeout;
  uint32_t numberSendingError;
  uint32_t numberReceivingError;
  uint32_t numberReceivingSizeError;
  uint32_t numberTimeoutError;
  vector<uint32_t> wishboneCodeError;
  char * flushBuf;

/*! @name Methods */
//{@
public:
/*!
 * Return the underlying EbClient lock object.
 *
 * @return Return the underlying EbClient lock object.
 */
inline std::recursive_mutex& get_lock() { 
  return etherboneMutex;
};

/*!
 * Open a datagram (UDP) socket on the Etherbone device. The socket stays opened till ebClose is
 * called or the object is destroyed.
 *
 * @return True if the socket has been openend successfully, false otherwise.
 */
  bool ebOpen();
/*!
 * Open a socket on the Etherbone device. The socket stays opened till ebClose is
 * called or the object is destroyed.
 *
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the socket has been openend successfully, false otherwise.
 */
  bool ebOpen(char *status);
/*!
 * Check if the socket is opened.
 *
 * @return True if the socket is currently opened, false otherwise.
 */
  bool isOpened();
/*!
 * Close the socket.
 */
  void ebClose();
/*!
 * Read a register of the node.
 *
 * @param offset The address of the register to read on the wishbone bus.
 * @param data Pointer where the read value is returned.
 * @return True if the register has been read successfully, false otherwise.
 */
  bool ebRdReg(uint32_t offset, uint32_t *data);
/*!
 * Read a register of the node.
 *
 * @param offset The address of the register to read on the wishbone bus.
 * @param data Pointer where the read value is returned.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the register has been read successfully, false otherwise.
 */
  bool ebRdReg(uint32_t offset, uint32_t *data, char *status);
  
  /*!
 * Read a register of the node.
 *
 * @param offset The address of the register to read on the wishbone bus.
 * @param data Pointer where the read value is returned.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the register has been read successfully, false otherwise.
 */
  bool etherBoneReadRegister(uint32_t offset, uint32_t *data, char *status);
  
  /*!
 * Read a register of the node with automatic reset of the socket in case of error
 *
 * @param offset The address of the register to read on the wishbone bus.
 * @param data Pointer where the read value is returned.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the register has been read successfully, false otherwise.
 */
  bool etherBoneReadRegisterAutoReset(uint32_t offset, uint32_t *data, char *status);
  
/*!
 * Read a block of registers of the node.
 *
 * @param offset The address of the first register to read on the wishbone bus.
 * @param size The number of registers to read. The registers are read sequentially
 * starting at address offset.
 * @param data Pointer to the array where the read values are returned. The array must
 * have a length of at least size element.
 * @return True if the registers have been read successfully, false otherwise.
 */
  bool ebRdBlk(uint32_t offset, size_t size, uint32_t *data);
  /*!
 * Read a block of registers of the node.
 *
 * @param offset The address of the first register to read on the wishbone bus.
 * @param size The number of registers to read. The registers are read sequentially
 * starting at address offset.
 * @param data Pointer to the array where the read values are returned. The array must
 * have a length of at least size element.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the registers have been read successfully, false otherwise.
 */
  bool ebRdBlk(uint32_t offset, size_t size, uint32_t *data, char *status);
  /*!
 * Read a block of registers of the node.
 *
 * @param offset The address of the first register to read on the wishbone bus.
 * @param size The number of registers to read. The registers are read sequentially
 * starting at address offset.
 * @param data Pointer to the array where the read values are returned. The array must
 * have a length of at least size element.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the registers have been read successfully, false otherwise.
 */
  bool etherBoneReadBlock(uint32_t offset, size_t size, uint32_t *data, char *status);
  /*!
 * Read a block of registers of the node with automatic reset of the socket in case of error.
 *
 * @param offset The address of the first register to read on the wishbone bus.
 * @param size The number of registers to read. The registers are read sequentially
 * starting at address offset.
 * @param data Pointer to the array where the read values are returned. The array must
 * have a length of at least size element.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the registers have been read successfully, false otherwise.
 */
  bool etherBoneReadBlockAutoReset(uint32_t offset, size_t size, uint32_t *data, char *status);
/*!
 * Write a register of the node.
 *
 * @param offset The address of the register to write on the wishbone bus.
 * @param data The Value to write to the register.
 * @return True if the register has been written successfully, false otherwise.
 */
  bool ebWrReg(uint32_t offset, uint32_t data);
/*!
 * Write a register of the node.
 *
 * @param offset The address of the register to write on the wishbone bus.
 * @param data The Value to write to the register.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the register has been written successfully, false otherwise.
 */
  bool ebWrReg(uint32_t offset, uint32_t data, char *status);
  /*!
 * Write a register of the node.
 *
 * @param offset The address of the register to write on the wishbone bus.
 * @param data The Value to write to the register.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the register has been written successfully, false otherwise.
 */
  bool etherBoneWriteRegister(uint32_t offset, uint32_t data, char *status); 
/*!
 * Write a register of the node with automatic reset of the socket in case of error.
 *
 * @param offset The address of the register to write on the wishbone bus.
 * @param data The Value to write to the register.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the register has been written successfully, false otherwise.
 */
  bool etherBoneWriteRegisterAutoReset(uint32_t offset, uint32_t data, char *status);
/*!
 * Write a block of registers of the node.
 *
 * @param offset The address of the first register to write on the wishbone bus.
 * @param size The number of registers to write. The registers are written sequentially
 * starting at address offset.
 * @param data Pointer to the array of values to write. The array must have a length of
 * at least size element.
 * @return True if the registers have been read successfully, false otherwise.
 */
  bool ebWrBlk(uint32_t offset, size_t size, uint32_t *data);
/*!
 * Write a block of registers of the node.
 *
 * @param offset The address of the first register to write on the wishbone bus.
 * @param size The number of registers to write. The registers are written sequentially
 * starting at address offset.
 * @param data Pointer to the array of values to write. The array must have a length of
 * at least size element.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the registers have been read successfully, false otherwise.
 */
  bool ebWrBlk(uint32_t offset, size_t size, uint32_t *data, char *status);
  /*!
 * Write a block of registers of the node.
 *
 * @param offset The address of the first register to write on the wishbone bus.
 * @param size The number of registers to write. The registers are written sequentially
 * starting at address offset.
 * @param data Pointer to the array of values to write. The array must have a length of
 * at least size element.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the registers have been read successfully, false otherwise.
 */
  bool etherBoneWriteBlock(uint32_t offset, size_t size, uint32_t *data, char *status);
  /*!
 * Write a block of registers of the node with automatic reset of the socket in case of error.
 *
 * @param offset The address of the first register to write on the wishbone bus.
 * @param size The number of registers to write. The registers are written sequentially
 * starting at address offset.
 * @param data Pointer to the array of values to write. The array must have a length of
 * at least size element.
 * @param status Pointer to the destination array where the status (error) is to be copied.
 * @return True if the registers have been read successfully, false otherwise.
 */
  bool etherBoneWriteBlockAutoReset(uint32_t offset, size_t size, uint32_t *data, char *status);
  /**
   * 
   * @param packet duly filled
   * @return true if success
   */
  bool socketWriteRead( EbPacket &packet , uint32_t *data, char *status, size_t size=1 , int nbRetry=-1 );
/*!
 * Set the number of sending packet retry (default 0) in case of timeout
 *
 * @param nbRetry The number of retry
 */
  void setRetryNumber(uint8_t nbRetry);
/*!
 * Set the time out in milliseconds (default 200ms) for the receiving packet call
 *
 * @param timeout_ms The time out in milliseconds
 */
  void setRecvTimeout(uint32_t timeout_ms);
    
  /*!
   * close open socket
   */
  void resetSocket(void);
  /*!
   * Return in string some Diagnostic counter
   * and array of all wishboneCodeError received
   */
  string getEtherboneErrorSummary(void);
  /*!
   * Return in vector<uint32_t> errors counter
   * the order in the vector is as follows
   * numberSendingError
   * numberReceivingError
   * numberReceivingSizeError
   * numberTimeoutError
   * numberOfWishboneCodeError
   */
  vector<uint32_t> getEtherboneErrorCounter(void);
  
  /**
   * flush read buffer for socket
   */
  void flushSocket(void);
};

#endif /* EBCLIENT_H_ */
