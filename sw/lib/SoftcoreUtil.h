// ==============================================================================
/*!
 * @file   SoftcoreUtil.h
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Class for interfacing a SoftCore (CPU) of the WR Node Core
 * (WRNC - mockturtle)
 */
// ==============================================================================

#ifndef SOFTCOREUTIL_H_
#define SOFTCOREUTIL_H_

#include "mutex"
#include "EbClient.h"

/*!
 * @def WRNC_CSR
 * @brief Offset of the Control & Status Register of the WRNC
 */
#define WRNC_CSR  0xC000

/*!
 * This class allows to configure a White Rabbit (WR) node and communicate with
 * it through the Etherbone protocol. This class is useful only if a WRNC is
 * instantiated in the WR node design.
 *
 * See the following page and documents for a complete specification of the WRNC
 * implementation:<br>
 *  \li http://www.ohwr.org/projects/wr-node-core/wiki
 *  \li http://www.ohwr.org/projects/wr-node-core/documents
 *
 * 
 * @ingroup Utils
 * @headerfile whistlib.h
 */

class SoftcoreUtil
{
private:
public:
/*!
 * @name Constructor
 * Only one constructor is defined for this class */
//@{
/*!
 * The object constructor.
 *
 * @param _dev Pointer to an EbClient object.
 * @param wrncBaseAddress The offset of the WRNC core in the wishbone memory map.
 */
  SoftcoreUtil(EbClient *_dev, uint32_t wrncBaseAddress);
//@}


/*!
 * @name Destructor
 * Only one destructor is defined for this class */ //NL: a class can't have more tha, one dtor :-)
//@{
/*!
 * The object destructor
 */
  virtual ~SoftcoreUtil();
//@}


/*! @name Methods */
//{@
/*!
 * Load a LM32 firmware (.bin extension) in a CPU of the WRNC. The CPU is restarted
 * once the firmware has been loaded.
 *
 * @param cpu_index The index of the CPU to be loaded [0...7].
 * The WRNC can instantiate up to 8 cores.
 * @param firmware The full filepath of the firmware to load.
 * @return True if the firmware has been loaded successfully, false otherwise.
 */
  bool cpu_load(uint8_t cpu_index, const char *firmware);
/*!
 * Load a LM32 firmware (.bin extension) in a CPU of the WRNC. The CPU is restarted
 * once the firmware has been loaded.
 *
 * @param cpu_index The index of the CPU to be loaded [0...7].
 * The WRNC can instantiate up to 8 cores.
 * @param firmware The full filepath of the firmware to load.
 * @param status Pointer to the char array where the status of the operation is to be stored
 * @return True if the firmware has been loaded successfully, false otherwise.
 */
  bool cpu_load(uint8_t cpu_index, const char *firmware, char *status);
/*!
 * Restart a CPU of the WRNC.
 *
 * @param cpu_index The index of the CPU to be loaded [0...7].
 * The WRNC can instantiate up to 8 cores.
 * @return True if the CPU has been restarted successfully, false otherwise.
 */
  bool cpu_restart(uint8_t cpu_index);
/*!
 * Get number of CPU instantiated in the WRNC.
 *
 * @param nbCpu Pointer to the variable where the number of CPU is returned.
 * @return True if the number of CPU has been read successfully, false otherwise.
 */
  bool get_cpu_count(uint32_t *nbCpu);
/*!
 * Send a message to a CPU of the WRNC.
 *
 * @param slot Correspond to the index of the CPU [0...7]. The WRNC can instantiate up to 8 cores.
 * @param msg_payload Pointer to the array containing the message to send.
 * @param msg_size The size of the message to send.
 * @return True if the message has been sent successfully, false otherwise.
 */
  bool hmq_send_message(uint8_t slot, uint32_t *msg_payload, size_t msg_size);
  /*!
 * Send a message to a CPU of the WRNC.
 *
 * @param slot Correspond to the index of the CPU [0...7]. The WRNC can instantiate up to 8 cores.
 * @param msg_payload Pointer to the array containing the message to send.
 * @param msg_size The size of the message to send.
 * @param status Pointer to the char array where the status of the operation is to be stored
 * @return True if the message has been sent successfully, false otherwise.
 */
  bool hmq_send_message(uint8_t slot, uint32_t *msg_payload, size_t msg_size, char* status);
/*!
 * Receive a message from a CPU of the WRNC.
 *
 * @param slot Correspond to the index of the CPU [0...7]. The WRNC can instantiate up to 8 cores.
 * @param msg_payload Pointer to the array where the received message is to be copied.
 * @param msg_size The size of the message to receive. The length of the array pointed by msg_payload
 * array must have at least msg_size elements.
 * @param time_out_ms The time (in millisecond) to wait the CPU message until aborting. 
 * @return 0 if a message has been received. -1 if an error occured while communicating with
 * the node or -2 if the time out has been reached.
 */
  int8_t hmq_recv_message(uint8_t slot, uint32_t *msg_payload, size_t *msg_size, uint32_t time_out_ms);
  /*!
 * Receive a message from a CPU of the WRNC.
 *
 * @param slot Correspond to the index of the CPU [0...7]. The WRNC can instantiate up to 8 cores.
 * @param msg_payload Pointer to the array where the received message is to be copied.
 * @param msg_size The size of the message to receive. The length of the array pointed by msg_payload
 * array must have at least msg_size elements.
 * @param time_out_ms The time (in millisecond) to wait the CPU message until aborting.
 * @param status Pointer to the char array where the status of the operation is to be stored
 * @return 0 if a message has been received. -1 if an error occured while communicating with
 * the node or -2 if the time out has been reached.
 */
  int8_t hmq_recv_message(uint8_t slot, uint32_t *msg_payload, size_t *msg_size, uint32_t time_out_ms, char* status);
  
/*!
 * flushSoftcore Queue of CPU in the WRNC.
 *
 * @param slot Correspond to the index of the CPU [0...7]. The WRNC can instantiate up to 8 cores.
 * @return 0 if a message has been received. -1 if an error occured while communicating with
 * the node.
 */
  bool flushQueue(uint8_t slot);
  /*!
 * flushSoftcore Queue of CPU in the WRNC.
 *
 * @param slot Correspond to the index of the CPU [0...7]. The WRNC can instantiate up to 8 cores.
 * @param status Pointer to the char array where the status of the operation is to be stored
 * @return 0 if a message has been received. -1 if an error occured while communicating with
 * the node.
 */
  bool flushQueue(uint8_t slot, char* status);
//@}


private:
  //attributes
  EbClient *dev;
  uint32_t baseAddress;
  uint32_t hmqBase;
  //dummy buff for status
  char statusBuff[256] = "";
  //method
  bool wrnc_lm32_reset_set(uint8_t mask);
  bool wrnc_lm32_reset_clr(uint8_t mask);
  bool wrnc_lm32_enable_set(uint8_t mask);
  bool wrnc_lm32_enable_clr(uint8_t mask);
  int wrnc_lm32_firmware_load(int cpu_index, void *fw_buf, size_t count, loff_t off);
  int wrnc_lm32_load_application_file(int cpu_index, const char *path);
};

#endif /* SOFTCOREUTIL_H_ */
