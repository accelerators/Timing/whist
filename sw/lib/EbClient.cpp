// ==============================================================================
/*!
 * @file   EbClient.cpp
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Class for communicating with an Etherbone device
 */

#include <unistd.h>
#include <sys/socket.h>
#include <sys/poll.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <memory>
#include <cstdalign>
#include "EbClient.h"

//EB Packet Header size and EB Record Header size in number of 16bits words
#define EB_HDR_SIZE 0x2
#define EB_REC_SIZE 0x4

//NL: define what a EbClientBuffer is.
//NL: idea is to use the initialization is acquisition paradigm to properly ensure memory 
//NL: management in the EbClient implementation and avoid memory leaks.
typedef std::unique_ptr<uint16_t[]> EbClientBuffer;

EbClient::EbClient(const char *nodeip, const char *iface) : nodeIpAddress(nodeip), ifaceName(iface) 
{
    ebSocketOpened = false;
    retry = 0;
    recvTimeout = 200;
    numberSendingError = 0;
    numberReceivingError = 0;
    numberReceivingSizeError = 0;
    numberTimeoutError = 0;
    flushBuf = new char[256];
}

EbClient::~EbClient() 
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);
    
    if (ebSocketOpened) {
        ebClose();
    }
    delete [] flushBuf;
}

bool EbClient::ebOpen() 
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    char status[256];
    strcpy(status,"");
    return ebOpen(status);
}

bool EbClient::ebOpen(char *status) 
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0) {
        sprintf(status, "ERROR opening socket: %s\n", strerror(errno));
        //cerr << status;
        return false;
    }

    //set time out for receiving packets even if timeout will be managed by poll()
    struct timeval tv;
    tv.tv_sec = 10; //10s timeout
    tv.tv_usec = 0;
    setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof (struct timeval));

    //set the interface device to bind
    setsockopt(sockfd, SOL_SOCKET, SO_BINDTODEVICE, (char*) ifaceName, strlen(ifaceName));
    
    fcntl(sockfd, F_SETFL, O_NONBLOCK);

    // convert IP address decimal dot to network addr
    if (inet_aton(nodeIpAddress, &nodeAddr.sin_addr) == 0) {
        sprintf(status, "ERROR converting IP (%s): %s\n", nodeIpAddress, strerror(errno));
        //cerr << status;
        ebClose();
        return false;
    }
    nodeAddr.sin_family = AF_INET;
    nodeAddr.sin_port = htons(EB_PORT);
    nodeAddrLen = sizeof (nodeAddr);

    memset(&clientAddr, 0, sizeof (clientAddr));
    clientAddr.sin_family = AF_INET;
    clientAddr.sin_addr.s_addr = htonl(INADDR_ANY);

    //Each client has to take a port in the range [EBD1...FFFF]
    //So increment port until free port is found
    //TODO: find a more efficient way to do this
    uint32_t srcPort = EB_PORT;
    bool portFound = false;
    do {
        if (srcPort == 0x10000) {
            sprintf(status, "ERROR finding a source port: No available port!\n");
            //cerr << status;
            ebClose();
            //NL: there was a bug here before using EbClientGuard.
            return false;
        }

        clientAddr.sin_port = htons((uint16_t)++srcPort);
        int ans = bind(sockfd, (struct sockaddr *) &clientAddr, sizeof (clientAddr));
        if (ans < 0) {
            if (errno != EADDRINUSE) {
                sprintf(status, "ERROR binding address (%s:%u): %s\n", nodeIpAddress, srcPort, strerror(errno));
                //cerr << status;
                ebClose();
                //NL: there was a bug here before using EbClientGuard.
                return false;
            }
        } else {
            portFound = true;
        }
    } while (!portFound);

    //NL: moved ebSocketOpened here cause we are actully declared opened
    ebSocketOpened = true;

    return true;
}

bool EbClient::isOpened(void) 
{
    return ebSocketOpened;
}

void EbClient::ebClose(void) 
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);


    if (ebSocketOpened)
        close(sockfd);

    ebSocketOpened = false;
}    

bool EbClient::ebRdReg(uint32_t offset, uint32_t *data) 
{
    char status[256];
    strcpy(status,"");
    return etherBoneReadRegisterAutoReset(offset,data,status);
}

bool EbClient::ebRdReg(uint32_t offset, uint32_t *data, char *status) 
{
    return etherBoneReadRegisterAutoReset(offset,data,status);
}

bool EbClient::etherBoneReadRegisterAutoReset(uint32_t offset, uint32_t *data, char *status)
{

    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    bool returnCode = false;
    if(!(returnCode = etherBoneReadRegister(offset,data,status))){
        resetSocket();
    }
    return returnCode;
}

bool EbClient::etherBoneReadRegister(uint32_t offset, uint32_t *data, char *status)
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    //declare etherbone packet
    EbPacket packet;
    packet.setVersion(1);
    //initialize 2 records:
    //-> 1 for the register read
    //-> another one for the remote wishbone bus status
    EbRecord rec1;
    EbRecord rec2;
    //initialize read register record
    rec1.setBCA();
    rec1.setRFF();
    rec1.setRCount(1);
    rec1.setBaseRdAddr(0x8000);
    rec1.pushRdAddr(offset);
    //initialize remote wishbone bus status record
    rec2.setBCA();
    rec2.setRCA();
    rec2.setRFF();
    rec2.setCYC();
    rec2.setRCount(1);
    rec2.setBaseRdAddr(0x8001);
    rec2.pushRdAddr(0x4);

    //push EB Record in EB packet
    packet.pushEbRecord(rec1);
    packet.pushEbRecord(rec2);
    
    return socketWriteRead(packet,data,status);
}

bool EbClient::ebRdBlk(uint32_t offset, size_t size, uint32_t *data) 
{
    char status[256];
    strcpy(status,"");
    return etherBoneReadBlockAutoReset(offset,size,data,status);
}

bool EbClient::ebRdBlk(uint32_t offset, size_t size, uint32_t *data, char *status) 
{
    return etherBoneReadBlockAutoReset(offset,size,data,status);
}

bool EbClient::etherBoneReadBlockAutoReset(uint32_t offset, size_t size, uint32_t *data, char *status)
{
    const EbClientGuard guard(etherboneMutex);

    bool returnCode = false;
    if(!(returnCode = etherBoneReadBlock(offset,size,data,status))){
        resetSocket();
    }
    return returnCode;
}

bool EbClient::etherBoneReadBlock(uint32_t offset, size_t size, uint32_t *data, char *status)
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    //EB packet can manage up to EB_MAX_RCOUNT read values (see specification, rCount field)
    //check size param
    if (size > EB_MAX_RCOUNT) {
        sprintf(status, "ERROR maximum read values in EB packet: %u words\n", EB_MAX_RCOUNT);
        //cerr << status;
        return false;
    }

    //declare etherbone packet
    EbPacket packet;
    packet.setVersion(1);
    //initialize 2 records:
    //-> 1 for the register read
    //-> another one for the remote wishbone bus status
    EbRecord rec1;
    EbRecord rec2;
    //initialize read register record
    rec1.setBCA();
    rec1.setRFF();
    rec1.setRCount(size);
    rec1.setBaseRdAddr(0x8000);
    for (size_t s = 0; s < size; s++) {
        rec1.pushRdAddr(offset + s * 4);
    }
    //initialize remote wishbone bus status record
    rec2.setBCA();
    rec2.setRCA();
    rec2.setRFF();
    rec2.setCYC();
    rec2.setRCount(1);
    rec2.setBaseRdAddr(0x8000 + size);
    rec2.pushRdAddr(0x4);

    //push EB Record in EB packet
    packet.pushEbRecord(rec1);
    packet.pushEbRecord(rec2);
    
    return socketWriteRead( packet, data, status, size );
}

bool EbClient::ebWrReg(uint32_t offset, uint32_t data) {
    char status[256];
    strcpy(status,"");
    return etherBoneWriteRegisterAutoReset(offset, data, status);
}

bool EbClient::ebWrReg(uint32_t offset, uint32_t data, char *status) {
    return etherBoneWriteRegisterAutoReset(offset, data, status);
}

bool EbClient::etherBoneWriteRegisterAutoReset(uint32_t offset, uint32_t data, char *status){
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    bool returnCode = false;
    if(!(returnCode = etherBoneWriteRegister(offset,data,status))){
        resetSocket();
    }
    return returnCode;
}

bool EbClient::etherBoneWriteRegister(uint32_t offset, uint32_t data, char *status) 
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    //declare etherbone packet
    EbPacket packet;
    packet.setVersion(1);
    //initialize 1 record:
    //only one record which contains a write on remote wishbone bus and a read on
    //config space for wishbone bus status
    EbRecord rec1;
    //initialize read register record
    rec1.setBCA();
    rec1.setRCA();
    rec1.setRFF();
    rec1.setCYC();
    rec1.setRCount(1);
    rec1.setWCount(1);
    //BaseRetAddr
    rec1.setBaseRdAddr(0x8000);
    rec1.pushRdAddr(0x4);
    //BaseWriteAddr
    rec1.setBaseWrAddr(offset);
    rec1.pushWrValue(data);

    //push EB Record in EB packet
    packet.pushEbRecord(rec1);
    
    return socketWriteRead( packet, &data, status );
}

bool EbClient::ebWrBlk(uint32_t offset, size_t size, uint32_t *data) 
{
    char status[256];
    strcpy(status,"");
    return etherBoneWriteBlockAutoReset(offset, size, data, status);
}

bool EbClient::ebWrBlk(uint32_t offset, size_t size, uint32_t *data, char *status) 
{
    return etherBoneWriteBlockAutoReset(offset, size, data, status);
}

bool EbClient::etherBoneWriteBlockAutoReset(uint32_t offset, size_t size, uint32_t *data, char *status)
{
    const EbClientGuard guard(etherboneMutex);
    
    bool returnCode = false;
    if(!(returnCode = etherBoneWriteBlock(offset,size,data,status))){
        resetSocket();
    }
    return returnCode;
}

bool EbClient::etherBoneWriteBlock(uint32_t offset, size_t size, uint32_t *data, char *status)
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    //EB packet can manage up to EB_MAX_WCOUNT write values (see specification, wCount field)
    //check size param
    if (size > EB_MAX_WCOUNT) {
        sprintf(status, "ERROR maximum read values in EB packet: %u words\n", EB_MAX_WCOUNT);
        //cerr << status;
        return false;
    }

    //declare etherbone packet
    EbPacket packet;
    packet.setVersion(1);
    //initialize 1 record:
    //only one record which contains a write on remote wishbone bus and a read on
    //config space for wishbone bus status
    EbRecord rec1;
    //initialize read register record
    rec1.setBCA();
    rec1.setRCA();
    rec1.setRFF();
    rec1.setCYC();
    rec1.setRCount(1);
    rec1.setWCount(size);
    //BaseRetAddr
    rec1.setBaseRdAddr(0x8000);
    rec1.pushRdAddr(0x4);
    //BaseWriteAddr
    rec1.setBaseWrAddr(offset);
    for (size_t s = 0; s < size; s++) {
        rec1.pushWrValue(data[s]);
    }

    //push EB Record in EB packet
    packet.pushEbRecord(rec1);
    
    return socketWriteRead( packet, data, status, size );
}

bool EbClient::socketWriteRead( EbPacket &packet , uint32_t *data, char *status , size_t size, int nbRetry ){
    struct pollfd ufds;
    int rv;
    ufds.fd = sockfd;
    
    flushSocket();

    size_t packetSize = packet.getPacketSize();

    //NL: buf is 16bit words to use htons and ntohs functions
    EbClientBuffer buf(new uint16_t[packetSize]);
    //get data with conversion to network byte order
    packet.getPacketData(buf.get(), true); 

    size_t nbBytes;
    //Poll incoming buffer only
    ufds.events = POLLIN;
    
    ++nbRetry;
    //send packet
    //packetSize is number of 16bits words
    //so multiply by 2 to have number of bytes required by sendto()
    nbBytes = sendto(sockfd, buf.get(), packetSize * sizeof(uint16_t), 0, (struct sockaddr*) &nodeAddr, nodeAddrLen);
    if (nbBytes < 0) {
        numberSendingError++;
        sprintf(status, "ERROR sending packet to the node: %s\n", strerror(errno));
        //cerr << status;
        return false;
    }
    //Poll socket in order to know if we can receive data
    rv = poll(&ufds, 1, recvTimeout);

    if (rv < 0) {
        sprintf(status, "ERROR receiving packet from the node: %s\n", strerror(errno));
        //cerr << status;
        return false;
    } else if (rv == 0) {
        numberTimeoutError++;
        if(nbRetry < retry){
            //cout <<"retrying "<< "size=" << size << " nbRetry=" << nbRetry << endl;
            return socketWriteRead( packet, data, status, size, nbRetry );
        }
        sprintf(status, "ERROR Timeout while receiving packet from the node\n");
        //cerr << status;
        return false;
    }

    //used recv instead of recvfrom because the connection is finished, there
    //are no more exchanges, so we don't need node address and port given by recvfrom()
    nbBytes = recv(sockfd, buf.get(), packetSize * sizeof(uint16_t), 0); // receive normal data
    if (nbBytes < 0) {
        numberReceivingError++;
        sprintf(status, "ERROR receiving EB packet: %s\n", strerror(errno));
        //cerr << status;
        return false;
    }        
    //etherbone remote device sends always a packet with the same size as the request (see specification)
    else if (nbBytes != packetSize * sizeof(uint16_t)) {
        numberReceivingSizeError++;
        if(nbRetry < retry){
            return socketWriteRead( packet, data, status, size, nbRetry );
        }
        sprintf(status, "ERROR receiving EB packet: The size of the received packet should be %lu\n", packetSize * sizeof(uint16_t));
        //cerr << status;
        return false;
    }

    uint16_t reg_msb = 0;
    uint16_t reg_lsb = 0;
    //get remote wishbone status data
    //it is the last 32bits word of the packet
    reg_msb = ntohs(buf[packetSize - 2]);
    reg_lsb = ntohs(buf[packetSize - 1]);
    uint32_t wishboneError = (uint32_t) (reg_msb << 16 | reg_lsb);
    if (wishboneError != 0) {
        wishboneCodeError.push_back(wishboneError);
        if(nbRetry < retry){
            return socketWriteRead( packet, data, status, nbRetry );
        }
        sprintf(status, "ERROR in remote wishbone bus: %u\n", wishboneError);
        //cerr << status;
        return false;
    }
    
    //get read register data
    //EB_REC_SIZE takes into account the BaseRetAddr word
    for (size_t s = 0; s < size; s++) {
        reg_msb = ntohs(buf[EB_HDR_SIZE + EB_REC_SIZE + s * sizeof(uint16_t)]);
        reg_lsb = ntohs(buf[EB_HDR_SIZE + EB_REC_SIZE + s * sizeof(uint16_t) + 1]);
        data[s] = (uint32_t) (reg_msb << 16 | reg_lsb);
    }
    return true;
}

void EbClient::setRetryNumber(uint8_t nbRetry) 
{
    retry = nbRetry;
}

void EbClient::setRecvTimeout(uint32_t timeout_ms) 
{
    recvTimeout = timeout_ms;
}

void EbClient::resetSocket(void) 
{
    if (ebSocketOpened) {
        ebClose();
    }
    ebOpen();
}

string EbClient::getEtherboneErrorSummary(void) 
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    stringstream ss;
    ss << "Number of Sending Error = " << numberSendingError << endl;
    ss << "Number of Receiving Error = " << numberReceivingError << endl;
    ss << "Number of packets Receiving with wrong Size = " << numberReceivingSizeError << endl;
    ss << "Number of Timeout Error = " << numberTimeoutError << endl;
    uint32_t numberOfWishBoneError = wishboneCodeError.size();
    ss << "Number of WishBone Error Code Received = " << numberOfWishBoneError << endl;
    for(uint32_t i = 0 ; i < numberOfWishBoneError;i++){
        ss << "\t[" << i <<"]=" << wishboneCodeError[i];
    }
    
    return ss.str();
}

vector<uint32_t> EbClient::getEtherboneErrorCounter(void)
{
    //NL: critical section (use initialization is acquisition paradigm)
    const EbClientGuard guard(etherboneMutex);

    vector<uint32_t> rtn;
    rtn.push_back(numberSendingError);
    rtn.push_back(numberReceivingError);
    rtn.push_back(numberReceivingSizeError);
    rtn.push_back(numberTimeoutError);
    rtn.push_back(wishboneCodeError.size());
    
    return rtn;
}

void EbClient::flushSocket(void){
    int nread;
    while((nread = read(sockfd, flushBuf, sizeof(flushBuf)-1)) > 0) {
        flushBuf[nread]='\0';    // explicit null termination: updated based on comments
        cerr << "flushed data on socket :" << flushBuf << endl; // print the current receive buffer with a newline
        flushBuf[0]='\0';        // clear the buffer : I am 99% sure this is not needed now
    }
}
