// ==============================================================================
/*
 * file:   EbRecord.cpp
 *
 * author: antonin.broquet@esrf.fr
 * brief:  Class for Etherbone record
 */
// ==============================================================================

#include <unistd.h>
#include <string.h>

#include "EbRecord.h"

#define BCA 7
#define RCA 6
#define RFF 5

#define CYC 3
#define WCA 2
#define WFF 1

//EB Record Header size (uint16_t)
#define HDR_SIZE 0x2


EbRecord::EbRecord()
{
  flags=0;
  byteEnable=0xf;
  rCount=0;
  wCount=0;
}

EbRecord::~EbRecord()
{

}

void EbRecord::setRCA(void)
{
  flags |= (1<<RCA);
}

void EbRecord::setBCA(void)
{
  flags |= (1<<BCA);
}

void EbRecord::setRFF(void)
{
  flags |= (1<<RFF);
}

void EbRecord::setCYC(void)
{
  flags |= (1<<CYC);
}

void EbRecord::setWCA(void)
{
  flags |= (1<<WCA);
}

void EbRecord::setWFF(void)
{
  flags |= (1<<WFF);
}
/*
void EbRecord::setByteEnable(uint8_t en)
{
  byteEnable = en;
}
*/
void EbRecord::setRCount(uint8_t rcount)
{
  rCount=rcount;
}

void EbRecord::setWCount(uint8_t wcount)
{
  wCount=wcount;
}

//Be careful: if the pushRdAddr is called before this method, the first element will be erased
//the BaseRdAddr must be the first element of EB Record data (readAddr)
void EbRecord::setBaseRdAddr(uint32_t baseRetAddr)
{
  if(!readAddr.empty())
  {
    //content has already been pushed but BaseRetAddr must be the first element
    //So erase first element
    readAddr[0] = baseRetAddr;
  }
  else
  {
    readAddr.push_back(baseRetAddr);
  }
}

//Be careful: if the pushWrValue is called before this method, the first element will be erased
//the BaseWriteAddr must be the first element of EB Record data (writeVal)
void EbRecord::setBaseWrAddr(uint32_t baseWriteAddr)
{
  if(!writeVal.empty())
  {
    //content has already been pushed but BaseWriteAddr must be the first element
    //So erase first element
    writeVal[0] = baseWriteAddr;
  }
  else
  {
    writeVal.push_back(baseWriteAddr);
  }
}

void EbRecord::pushRdAddr(uint32_t addr)
{
  readAddr.push_back(addr);
}

void EbRecord::pushWrValue(uint32_t value)
{
  writeVal.push_back(value);
}

size_t EbRecord::getEbRecordSize(void)
{
  //readAddr.size() and writeVal.size() give the number of 32bits of the containers
  //so it is multiplied by 2 to convert it in number of 16bits
  return (HDR_SIZE + readAddr.size()*2 + writeVal.size()*2);
}

size_t EbRecord::getEbRecordData(uint16_t *buf)
{
  //fill EB Record in buf
  uint16_t ind = 0;
  //fill EB Record header
  buf[ind] = (flags << 8) | byteEnable;
  buf[++ind] = (wCount << 8) | rCount;
  //fill EB Record buf
  if(wCount > 0)
  {
    for(size_t i=0;i<writeVal.size();i++)
    {
      //writeVal contains 32bits words...
      //So split it in 2 16bits words, MSW first
      buf[++ind] = (writeVal[i] & 0xffff0000) >> 16;
      buf[++ind] = writeVal[i] & 0xffff;
    }
  }
  if(rCount > 0)
  {
    for(size_t i=0;i<readAddr.size();i++)
    {
      //readAddr contains 32bits words...
      //So split it in 2 16bits words, MSW first
      buf[++ind] = (readAddr[i] & 0xffff0000) >> 16;
      buf[++ind] = readAddr[i] & 0xffff;
    }
  }
  //readAddr.size() and writeVal.size() give the number of 32bits of the containers
  //so it is multiplied by 2 to convert it in number of 16bits
  return (HDR_SIZE + readAddr.size()*2 + writeVal.size()*2);
}
