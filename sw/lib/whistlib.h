// ==============================================================================
/*!
 * @file   whistlib.h
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Main include for the whist library
 * @todo Refactor classes in order to have consistency in coding style
 */
// ==============================================================================

#ifndef WHISTLIB_H
#define WHISTLIB_H

#include <iostream>
#include <vector>
#include <cstdlib>
#include <cstdint>
#include <cstring>

/*!
 * @mainpage Overview
 *
 * The WHIST project aims to refurbish the ESRF timing and synchronization system.
 * It is based on a White Rabbit network.
 * @image html wr_logo.jpg
 * @image latex wr_logo.jpg
 * See http://www.ohwr.org/projects/white-rabbit/wiki for more information about
 * White Rabbit (WR).
 * 
 * The WHIST node design includes the WRPTP core and the WRN core in order to
 * setup RFoWR feature. See the following pages for more information about these
 * cores:<br>
 *   \li WRPTP: http://www.ohwr.org/projects/wr-cores/wiki/Wrpc_core
 *   \li WRNC: http://www.ohwr.org/projects/wr-node-core/wiki
 *   \li RFoWR: http://www.ohwr.org/projects/wr-d3s/wiki/RF_distribution_demo
 *
 * The ESRF dedicated module is implemented in this design which provide
 * a sequencer delivering pulses and clocks.
 *
 * A WHIST node runs in standalone mode. It means that it can be configured and
 * interfaced only through the WR network. The Etherbone protocol allows accessing
 * the internal bus of the node trough the network.
 *
 * The WHIST library uses this Etherbone protocol in order to be able to interface
 * a WHIST node and configure/monitor it.
 *
 */


/*! 
 * @defgroup Etherbone Etherbone communication
 * @brief Group containing classes related to Etherbone
 * communication used to interface a White Rabbit node.
 */
//{@
#include "EbClient.h"
//@}
/*!
 * @defgroup Utils Cores specific interface
 * @brief Group containing classes used to interface a
 * specific module / core (WRNC, xil_multiboot, ...).
 * These classes use EbClient from Etherbone group.
 */
//{@
#include "SoftcoreUtil.h"
#include "FlashAccess.h"
//@}

#endif // WHISTLIB_H
