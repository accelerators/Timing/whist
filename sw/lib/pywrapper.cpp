// ==============================================================================
/*!
 * @deprecated
 * @file   pywrapper.cpp
 *
 * @author antonin.broquet@esrf.fr
 * @brief  File to export library as a C library for Python. This is done in this
 * way for backward compatibility with already developed Python module.
 *
 * @todo Implement a more efficient way to export the library to Python.
 *
 * @ingroup Utils
 */
// ==============================================================================

#include "whistlib.h"

//Following section provided for PYTHON
extern "C" {

EbClient *ebc;

bool WLopen(const char *ip, const char *iface)
{
  ebc = new EbClient(ip,iface);
  return ebc->ebOpen();
}

void eb_set_retry(uint8_t nb)
{
  ebc->setRetryNumber(nb);
}

void eb_set_timeout(uint32_t ms)
{
  ebc->setRecvTimeout(ms);
}

bool eb_read(uint32_t address, uint32_t *data)
{
  return ebc->ebRdReg(address,data);
}

bool eb_write(uint32_t address, uint32_t data)
{
  return ebc->ebWrReg(address,data);
}

bool eb_wrnc_lm32_loader(uint32_t address, int cpu_index, char *file)
{
  SoftcoreUtil scu(ebc,address);
  return scu.cpu_load(cpu_index,file);
}

bool eb_wrnc_lm32_restart(uint32_t address, int cpu_index)
{
  SoftcoreUtil scu(ebc,address);
  return scu.cpu_restart(cpu_index);
}

void WLclose()
{
  ebc->ebClose();
  delete ebc;
}

}
