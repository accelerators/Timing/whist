// ==============================================================================
/*!
 * @file   SoftcoreUtil.cpp
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Class for interfacing a SoftCore (CPU) of the WR Node Core
 * (WRNC - mockturtle)
 */
// ==============================================================================

#include <unistd.h>
#include "SoftcoreUtil.h"
#include "hw/wrn_cpu_csr.h"
#include "hw/mqueue.h"


//constructor

SoftcoreUtil::SoftcoreUtil(EbClient *_dev, uint32_t wrncBaseAddress) : baseAddress(wrncBaseAddress) {
    this->dev = _dev;
    hmqBase = baseAddress + BASE_HMQ;
}

//destructor

SoftcoreUtil::~SoftcoreUtil() {
}

bool SoftcoreUtil::cpu_load(uint8_t cpu_index, const char *firmware) {
    return cpu_load(cpu_index, statusBuff);
}

bool SoftcoreUtil::cpu_load(uint8_t cpu_index, const char *firmware, char *status) {
    int err;
    err = wrnc_lm32_load_application_file((int) cpu_index, firmware);
    if (err) {
        sprintf(status, "Cannot load application to CPU %d: check the communication or the firmware validity (full path, .bin extension)\n", cpu_index);
        cerr << status;
        return false;
    }
    return cpu_restart(cpu_index);
}

bool SoftcoreUtil::cpu_restart(uint8_t cpu_index) {
    //stop (pause) the cpu
    if (!wrnc_lm32_reset_set(1 << cpu_index)) {
        return false;
    }
    //disable the cpu
    if (!wrnc_lm32_enable_set(1 << cpu_index)) {
        return false;
    }

    //enable the cpu
    if (!wrnc_lm32_enable_clr(1 << cpu_index)) {
        return false;
    }

    //start (run) the cpu
    return wrnc_lm32_reset_clr(1 << cpu_index);
}

bool SoftcoreUtil::get_cpu_count(uint32_t *nbCpu) {
    if (!dev->ebRdReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_CORE_COUNT, nbCpu)) {
        return false;
    }
    return true;
}


bool SoftcoreUtil::hmq_send_message(uint8_t slot, uint32_t *msg_payload, size_t msg_size) {
    return hmq_send_message(slot, msg_payload, msg_size, statusBuff);
}

bool SoftcoreUtil::hmq_send_message(uint8_t slot, uint32_t *msg_payload, size_t msg_size, char* status) {
    //NL: is a gobal critical section required on all EbClient calls ?????
    //NL: in case it is...
    const EbClientGuard guard(dev->get_lock());

    size_t s;
    //claim
    if (!dev->ebWrReg(hmqBase + MQUEUE_BASE_IN(slot) + MQUEUE_SLOT_COMMAND, MQUEUE_CMD_CLAIM, status)) {
        return false;
    }
    //write data
    for (s = 0; s < msg_size; s++) {
        if (!dev->ebWrReg(hmqBase + MQUEUE_BASE_IN(slot) + MQUEUE_SLOT_DATA_START + s * 4, msg_payload[s], status)) {
            return false;
        }
    }
    // send message
    // why + 10??  -> NL: we have to answer the question!
    return dev->ebWrReg(hmqBase + MQUEUE_BASE_IN(slot) + MQUEUE_SLOT_COMMAND, MQUEUE_CMD_READY | (msg_size + 10), status);
}


int8_t SoftcoreUtil::hmq_recv_message(uint8_t slot, uint32_t *msg_payload, size_t *msg_size, uint32_t time_out_ms) {
    return hmq_recv_message( slot, msg_payload, msg_size, time_out_ms, statusBuff);
}

int8_t SoftcoreUtil::hmq_recv_message(uint8_t slot, uint32_t *msg_payload, size_t *msg_size, uint32_t time_out_ms, char* status) {
    //NL: is a gobal critical section required on all EbClient calls ?????
    //NL: in case it is...
    const EbClientGuard guard(dev->get_lock());

    //NL: WEIRD TMO IMPL !!!!!
    uint32_t tm;
    uint32_t reg;
    //wait that the slot becomes not empty
    for (tm = 0; tm < time_out_ms; tm++) {
        if (!dev->ebRdReg(hmqBase + MQUEUE_BASE_OUT(slot) + MQUEUE_SLOT_STATUS, &reg, status)) {
            return -1;
        }
        if ((reg & MQUEUE_SLOT_STATUS_EMPTY) == 0) {
            *msg_size = (reg & MQUEUE_SLOT_STATUS_MSG_SIZE_MASK) >> MQUEUE_SLOT_STATUS_MSG_SIZE_SHIFT;
            //read data
            if (!dev->ebRdBlk(hmqBase + MQUEUE_BASE_OUT(slot) + MQUEUE_SLOT_DATA_START, *msg_size, msg_payload, status)) {
                return -1;
            }
            //discard message
            if (!dev->ebWrReg(hmqBase + MQUEUE_BASE_OUT(slot) + MQUEUE_SLOT_COMMAND, MQUEUE_CMD_DISCARD, status)) {
                return -1;
            } else {
                //success
                return 0;
            }
        }
        usleep(1000); //1ms //NL: WEIRD TMO IMPL !!!!!
    }
    //time out reached
    return -2;
}

bool SoftcoreUtil::flushQueue(uint8_t slot, char* status) {
    //purge message
    //return true if success
    return dev->ebWrReg(hmqBase + MQUEUE_BASE_OUT(slot) + MQUEUE_SLOT_COMMAND, MQUEUE_CMD_PURGE, status);
}

/**
 * Set the reset bit of the CPUs according to the mask
 */
bool SoftcoreUtil::wrnc_lm32_reset_set(uint8_t mask) {
    //NL: is a gobal critical section required on all EbClient calls ?????
    //NL: in case it is...
    const EbClientGuard guard(dev->get_lock());

    uint32_t reg_val;
    if (!dev->ebRdReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_RESET, &reg_val)) {
        return false;
    }
    reg_val |= (mask & 0xFF);
    dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_RESET, reg_val);
    return true;
}

/**
 * Clear the reset bit of the CPUs according to the mask
 */
bool SoftcoreUtil::wrnc_lm32_reset_clr(uint8_t mask) {
    //NL: is a gobal critical section required on all EbClient calls ?????
    //NL: in case it is...
    const EbClientGuard guard(dev->get_lock());

    uint32_t reg_val;
    if (!dev->ebRdReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_RESET, &reg_val)) {
        return false;
    }
    reg_val &= (~mask & 0xFF);
    dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_RESET, reg_val);
    return true;
}

/**
 * Set the reset bit of the CPUs according to the mask
 * NOTE : for the CPU 1 means pause
 */
bool SoftcoreUtil::wrnc_lm32_enable_set(uint8_t mask) {
    //NL: is a gobal critical section required on all EbClient calls ?????
    //NL: in case it is...
    const EbClientGuard guard(dev->get_lock());

    uint32_t reg_val;
    if (!dev->ebRdReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_ENABLE, &reg_val)) {
        return false;
    }
    reg_val |= (mask & 0xFF);
    dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_ENABLE, reg_val);
    return true;
}

/**
 * Clear the reset bit of the CPUs according to the mask
 * NOTE : for the CPU 0 means run
 */
bool SoftcoreUtil::wrnc_lm32_enable_clr(uint8_t mask) {
    //NL: is a gobal critical section required on all EbClient calls ?????
    //NL: in case it is...
    const EbClientGuard guard(dev->get_lock());

    uint32_t reg_val;
    if (!dev->ebRdReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_ENABLE, &reg_val)) {
        return false;
    }
    reg_val &= (~mask & 0xFF);
    dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_ENABLE, reg_val);
    return true;
}

/**
 * It loads a given application into the CPU memory
 */
int SoftcoreUtil::wrnc_lm32_firmware_load(int cpu_index, void *fw_buf, size_t count, loff_t off) {
    //NL: is a gobal critical section required on all EbClient calls ?????
    //NL: in case it is...
    const EbClientGuard guard(dev->get_lock());

    uint32_t *fw = (uint32_t*) fw_buf, word, word_rb;
    uint32_t size, offset, i;
    uint32_t cpu_memsize;

    /* Select the CPU memory to write */
    if (!dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_CORE_SEL, cpu_index)) {
        return -1;
    }
    dev->ebRdReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_CORE_MEMSIZE, &cpu_memsize);

    if (off + count > cpu_memsize) {
        fprintf(stderr, "Cannot load firmware: size limit %d byte\n", cpu_memsize);
        return -1;
    }

    /* Calculate code size in 32bit word*/
    size = (count + 3) / 4;
    offset = off / 4;

    /* Reset the CPU before overwrite its memory */
    wrnc_lm32_reset_set((1 << cpu_index));

    /* Clean CPU memory */
    for (i = offset; i < cpu_memsize / 1024; ++i) {
        if (!dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_UADDR, i)) {
            return -1;
        }
        usleep(1); //NL: WTF-IT?
        dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_UDATA, 0);
        usleep(1); //NL: WTF-IT?
    }

    /* Load the firmware */
    for (i = 0; i < size; ++i) {
        //word = __cpu_to_be32(fw[i]);
        word = htobe32(fw[i]);
        if (!dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_UADDR, i + offset)) {
            return -1;
        }
        usleep(1); //NL: WTF-IT?
        dev->ebWrReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_UDATA, word);
        usleep(1); //NL: WTF-IT?
        dev->ebRdReg(baseAddress + WRNC_CSR + WRN_CPU_CSR_REG_UDATA, &word_rb);
        usleep(1); //NL: WTF-IT?
        if (word != word_rb) {
            fprintf(stderr, "failed to load firmware (byte %d | 0x%x != 0x%x)\n", i, word, word_rb);
            return -1;
        }
    }

    return 0;
}

/**
 * It loads a WRNC CPU firmware from a given file
 * @param[in] cpu_index CPU index
 * @param[in] path path to the firmware file
 * @return 0 on success, on error -1 and errno is set appropriately
 */
int SoftcoreUtil::wrnc_lm32_load_application_file(int cpu_index, const char *path) {
    int i, len;
    void *code;
    FILE *f;

    f = fopen(path, "rb");
    if (!f) {
        return -1;
    }

    fseek(f, 0, SEEK_END);
    len = ftell(f);
    rewind(f);
    if (!len) {
        return -1;
    }

    code = malloc(len);
    if (!code) {
        return -1;
    }

    /* Get the code from file */
    i = fread(code, 1, len, f);
    fclose(f);
    if (!i || i != len) {
        /*TODO: maybe optimize me with a loop */
        free(code);
        return -1;
    }

    if (wrnc_lm32_firmware_load(cpu_index, code, len, 0) != 0) {
        free(code);
        return -1;
    }
    free(code);

    return 0;
}
