// ==============================================================================
/*!
 * @file   EbPacket.h
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Class for Etherbone packet
 */
// ==============================================================================

#ifndef EBPACKET_H_
#define EBPACKET_H_

#include <iostream>
#include <vector>
#include <stdint.h>
#include "EbRecord.h"

using namespace std;

/*!
 * @enum ebsz_t
 * @brief association between EB header value and wishbone address/port sizes */
enum ebsz_t {
  EBSZ_8  = 0, /*!< 8 bits */
  EBSZ_16 = 2, /*!< 16 bits */
  EBSZ_32 = 4, /*!< 32 bits */
  EBSZ_64 = 8  /*!< 64 bits */
};


/*!
 * This class manages an Etherbone (EB) packet to send to an Etherbone endpoint.
 *
 * See the following page and document for a complete specification of Etherbone
 * implementation and protocol:<br>
 *  \li http://www.ohwr.org/projects/etherbone-core/wiki
 *  \li http://www.ohwr.org/projects/etherbone-core/documents
 *
 * @todo Implement Addr size and Port size setters once EbRecord supports
 * all data width (8/16/32/64-bit).
 * 
 * @ingroup Etherbone
 */

class EbPacket
{
public:
/*!
 * @name Constructor
 * Only one constructor is defined for this class */
//@{
/*!
 * The object constructor. It initializes Eb packet header to 0.
 */
  EbPacket();
//@}

/*!
 * @name Destructor
 * Only one destructor is defined for this class */
//@{
/*!
 * The object destructor
 */
  virtual ~EbPacket();
//@}


//Attributes
private:
  //EB Packet Header related
  uint8_t version;
  uint8_t flags;
  uint8_t addrSize;
  uint8_t portSize;
  //EB Record related
  vector<EbRecord> rec;

/*! @name Methods */
//{@
public:
/*!
 * Set the NR bit of the EB packet header.
 *
 * The No-Reads flag (NR) indicates that the entire message contains no read
 * operations.
 */
  void setNR(void);
/*!
 * Set the PF bit of the EB packet header.
 *
 * The Probe-Flag (PF) is used to negotiate compatible bus widths and version.
 */
  void setPF(void);
/*!
 * Set the EB version.
 *
 * @param vers The version number of Etherbone
 */
  void setVersion(uint8_t vers);


//  void setAddrSize(ebsz_t addrSz);
//  void setPortSize(ebsz_t portSz);


/*!
 * Push an EB record in this EB packet.
 *
 * @param ebRec The EbRecord to push
 */
  void pushEbRecord(EbRecord ebRec);
/*!
 * Get the size of this EB packet.
 *
 * @return The size of this EB packet in number of 16-bit words.
 */
  size_t getPacketSize(void);
/*!
 * Get the data of this EB packet.
 *
 * @param buf Pointer to the destination array where the EB packet data are to be copied.
 * @param h2ns Convert data to network order if true. No conversion otherwise. Default
 * value is true.
 * @return The size of this EB packet in number of 16-bit words.
 */
  size_t getPacketData(uint16_t *buf, bool h2ns=true);
//@}
};

#endif /* EBPACKET_H_ */
