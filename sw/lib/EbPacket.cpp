// ==============================================================================
/*
 * file:   EbPacket.cpp
 *
 * author: antonin.broquet@esrf.fr
 * brief:  Class for Etherbone packet
 */
// ==============================================================================

#include <arpa/inet.h>
#include <string.h>

#include "EbPacket.h"

#define PF 0
#define PR 1
#define NR 2

#define EB_MAGIC      0x4E6F;
//EB Packet Header in 16bits words
#define EB_HDR_SIZE   2

EbPacket::EbPacket()
{
  flags=0;
  version=0;
  addrSize=EBSZ_32;
  portSize=EBSZ_32;
}

EbPacket::~EbPacket()
{

}

void EbPacket::setNR(void)
{
  flags |= (1<<NR);
}

void EbPacket::setPF(void)
{
  flags |= (1<<PF);
}

void EbPacket::setVersion(uint8_t vers)
{
  version = vers;
}

//void EbPacket::setAddrSize(ebsz_t addrSz)
//{
//  addrSize = (uint8_t)addrSz;
//}
//
//void EbPacket::setPortSize(ebsz_t portSz)
//{
//  portSize = (uint8_t)portSz;
//}

void EbPacket::pushEbRecord(EbRecord ebRec)
{
  rec.push_back(ebRec);
}

size_t EbPacket::getPacketSize(void)
{
  uint16_t packetSize=EB_HDR_SIZE;
  for(size_t i=0;i<rec.size();i++)
  {
    packetSize += rec[i].getEbRecordSize();
  }
  return packetSize;
}

size_t EbPacket::getPacketData(uint16_t *buf, bool h2ns)
{
  //fill EB Packet in buf
  //fill EB Packet header
  buf[0] = EB_MAGIC;
  buf[1] = (((version << 4) | flags) << 8) | ((addrSize << 4) | portSize);
  size_t packetSize=EB_HDR_SIZE;
  size_t recordSize=0;
  uint16_t ebRecordBuf[2048];
  for(size_t i=0;i<rec.size();i++)
  {
    recordSize = rec[i].getEbRecordData(ebRecordBuf);
    //multiply by 2 to work in bytes as required by memcpy
    memcpy((uint16_t*)buf+packetSize,ebRecordBuf,(recordSize*2));
    packetSize += recordSize;
  }
  if(h2ns)
  {
    for(size_t i=0;i<packetSize;i++)
    {
      buf[i] = htons(buf[i]);
    }
  }
  return packetSize;
}
