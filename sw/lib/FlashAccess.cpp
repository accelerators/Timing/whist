// ==============================================================================
/*!
 * @file   FlashAccess.h
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Class for interfacing an on-board memory FLASH used for the FPGA
 * configuration (storing bitstream)
 */
// ==============================================================================

#include <unistd.h>
#include <string.h>
#include <sys/stat.h>
#include "FlashAccess.h"

FlashAccess::FlashAccess(EbClient *_dev, string bitfile, uint32_t flashBaseAddress,
                         uint32_t goldenAddr, uint32_t multiAddr) : filename(bitfile), baseAddress(flashBaseAddress), gbAddr(goldenAddr), mbAddr(multiAddr)
{
  this->dev = _dev;
  cout << "FLASH base address                    : " << hex << baseAddress << endl;
  cout << "Golden bitstream address (in FLASH)   : " << hex << gbAddr << endl;
  cout << "Multiboot bitstream address (in FLASH): " << hex << mbAddr << endl;
}

FlashAccess::~FlashAccess()
{
}

bool FlashAccess::load_bitstream(void)
{
  cout << "Loading bitstream... " << flush;
  if(flash_release_from_deep_power_down()==false)
  {
    cout << "Loading bitstream... failed" << endl;
    return false;
  }
  //SPI OPCODE (read x1) + Golden address offset
  if(!dev->ebWrReg(baseAddress+GBBAR,(0x0B<<24) | (0xffffff & gbAddr)))
  {
    cout << "Loading bitstream... failed" << endl;
    return false;
  }
  //SPI OPCODE (read x1) + Multiboot address offset
  if(!dev->ebWrReg(baseAddress+MBBAR,(0x0B<<24) | (0xffffff & mbAddr)))
  {
    cout << "Loading bitstream... failed" << endl;
    return false;
  }
  //Unlock bit for the IPROG command
  if(!dev->ebWrReg(baseAddress+CR,(uint32_t)0x10000))
  {
    cout << "Loading bitstream... failed" << endl;
    return false;
  }
  //Write IPROG bit command
  if(!dev->ebWrReg(baseAddress+CR,(uint32_t)0x20000))
  {
    cout << "Loading bitstream... failed" << endl;
    return false;
  }
  cout << "Loading bitstream... done" << endl;
  return true;
}

bool FlashAccess::program_bitstream(bool hasGolden, bool load)
{
  cout << "Programing bitstream sequence:" << endl;

  //Read bitstream
  size_t b_size;
  size_t nb_pages;
  size_t remaining_bytes;
  if(!load_bitfile(&b_size,&nb_pages,&remaining_bytes))
  {
    cout << "\rProgramming FLASH... failed" << endl;
    flash_deep_power_down();
    return false;
  }

  uint8_t nb_sectors = ((b_size/FLASH_SECTOR_SIZE) + (((b_size%FLASH_SECTOR_SIZE)>0) ? 1 : 0));

  //Release FLASH from deep power down
  if(flash_release_from_deep_power_down() == false)
  {
    free(bitstream);
    return false;
  }

  //!!WARNING: Only in case of NO golden bitstream!!
  //(indeed, in case of golden bitstream, erasing by sector is mandatory
  //in order to not erase golden bitstream)
  if(!hasGolden)
  {
    //Check that it is faster to erase by sector or whole FLASH
    //It depends of bitstream size:
    //M25P32 datasheet: Bulk Erase=23s | Sector Erase=0.6s
    //LX100T bitsream size ~= 3320228 => 3320228/65536 = 51 sectors => 51*0.6=30.6s
    //if required numbers of sectors multiplied by time of sector erase is greater than
    //bulk erase, erase with a bulk command
    if(nb_sectors*SECTOR_ERASE_TIME >= BULK_ERASE_TIME)
    {
      //erase with BULK command (whole FLASH)
      //Erase the FLASH. There is only one bitstream, so we can erase whole flash (Bulk Erase).
      //So it is faster to do a Bulk Erase than 51 Sector Erase
      cout << "Bulk Erasing... " << flush;
      flash_write_enable();
      flash_bulk_erase();
      //Wait maximum 81s (max Bulk Erase time from datasheet is 80s)
      if(flash_wait_for_wip(81000000)==false)
      {
        cout << "\rBulk Erasing... failed" << endl;
        flash_deep_power_down();
	free(bitstream);
        return false;
      }
      cout << "\rBulk Erasing... done" << endl;
    }
    else
    {
      //Do a Sector Erase to be faster than Bulk Erase
      //Set the first sector of the bitstream design
      size_t multiboot_start_sector =  mbAddr / FLASH_SECTOR_SIZE;
      size_t multiboot_end_sector =  multiboot_start_sector + nb_sectors;
      if(multiboot_end_sector > FLASH_MAX_SECTOR)
      {
        cout << "\rSector Erasing... failed" << endl;
        flash_deep_power_down();
	free(bitstream);
        return false;
      }
      size_t sector;
      cout << "Sector Erasing... " << flush;
      for(sector=multiboot_start_sector;sector<multiboot_end_sector;sector++)
      {
        flash_write_enable();
        flash_sector_erase(sector);
        //Wait maximum 4s (max Sector Erase time from datasheet is 3s)
        if(flash_wait_for_wip(3000000)==false)
        {
          cout << "\rSector Erasing... failed" << endl;
          flash_deep_power_down();
	  free(bitstream);
          return false;
        }

      }
      cout << "\rSector Erasing... done" << endl;
    }
  }
  else
  {
    //Design has a Golden bitstream (and the application bitstream, the multiboot image)
    //Do a Sector Erase to erase just the Multiboot design
    //Set the first sector of the Multiboot design
    size_t multiboot_start_sector =  mbAddr / FLASH_SECTOR_SIZE;
    size_t multiboot_end_sector =  multiboot_start_sector + nb_sectors;
    if(multiboot_end_sector > FLASH_MAX_SECTOR)
    {
      cout << "\rSector Erasing... failed" << endl;
      flash_deep_power_down();
      free(bitstream);
      return false;
    }
    size_t sector;
    cout << "Sector Erasing... " << flush;
    for(sector=multiboot_start_sector;sector<multiboot_end_sector;sector++)
    {
      flash_write_enable();
      flash_sector_erase(sector);
      //Wait maximum 4s (max Sector Erase time from datasheet is 3s)
      if(flash_wait_for_wip(3000000)==false)
      {
         cout << "\rSector Erasing... failed" << endl;
        flash_deep_power_down();
	free(bitstream);
        return false;
      }
    }
    cout << "\rSector Erasing... done" << endl;
  }

  //Write Bitstream
  cout << "Programming FLASH... " << flush;
  
  size_t p;
  uint32_t page_address=mbAddr;
  for(p=0;p<nb_pages;p++)
  {
    flash_write_enable();
    if(!flash_page_program(page_address))
    {
      cout << "\rProgramming FLASH... failed" << endl;
      flash_deep_power_down();
      free(bitstream);
      return false;
    }
    if(flash_send_bytes(bitstream+(FLASH_PAGE_SIZE*p),FLASH_PAGE_SIZE)==false)
    {
      cout << "\rProgramming FLASH... failed" << endl;
      flash_deep_power_down();
      free(bitstream);
      return false;
    }
    page_address += FLASH_PAGE_SIZE;
  }
  //remaining buffer bytes
  flash_write_enable();
  if(!flash_page_program(page_address))
  {
    cout << "\rProgramming FLASH... failed" << endl;
    flash_deep_power_down();
    free(bitstream);
    return false;
  }
  if(flash_send_bytes(bitstream+(FLASH_PAGE_SIZE*p),remaining_bytes)==false)
  {
    cout << "\rProgramming FLASH... failed" << endl;
    flash_deep_power_down();
    free(bitstream);
    return false;
  }
  cout << "\rProgramming FLASH... done" << endl;
  free(bitstream);
  //if reconfiguration is requested
  if(load==true)
  {
    return load_bitstream();
  }
  else
  {
    return true;
  }
}

bool FlashAccess::read_bitstream(uint8_t *bitstream, size_t size)
{
  cout << "Reading bitstream sequence:" << endl;

  //Release FLASH from deep power down
  if(flash_release_from_deep_power_down() == false)
  {
    return false;
  }

  //Read Bitstream from FLASH
  cout << "Reading FLASH... " << flush;

  flash_read(0x0, bitstream, size);

  cout << "\rReading FLASH... done" << endl;

  return true;
}

bool FlashAccess::load_bitfile(size_t *bitstream_size, size_t *page_loop_size, size_t *left_bytes)
{
  FILE *f;
  struct stat stbuf;
  size_t i;

  f = fopen(filename.c_str(), "r");
  if(!f)
  {
    fprintf(stderr, "%s: %s\n", filename.c_str(), strerror(errno));
    return false;
  }
  if (fstat(fileno(f), &stbuf) < 0) {
    fprintf(stderr, "%s: %s\n", filename.c_str(), strerror(errno));
    fclose(f);
    return false;
  }
  *bitstream_size = stbuf.st_size;
  *page_loop_size = stbuf.st_size / FLASH_PAGE_SIZE;
  *left_bytes = stbuf.st_size % FLASH_PAGE_SIZE;
  bitstream = (uint8_t*)malloc(sizeof(uint8_t)*stbuf.st_size);
  if(!bitstream)
  {
    fprintf(stderr, "loading %s: %s\n", filename.c_str(), strerror(errno));
    return false;
  }
  i = fread(bitstream,sizeof(uint8_t),stbuf.st_size,f);
  fclose(f);
  if(i != stbuf.st_size)
  {
    fprintf(stderr, "reading %s: unable to read all bytes\n", filename.c_str());
    free(bitstream);
    return false;
  }
  return true;
}

bool FlashAccess::flash_release_from_deep_power_down(void)
{
  //Release from Deep Power-Down mode
  cout << "Release FLASH from Deep Power-Down... " << endl;
  //send command
  if(dev->ebWrReg(baseAddress+FAR,
                  (uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|CMD_RELEASE_DPD) == false)
  {
    //writing to WNode failed, exit
    return false;
  }
  //set CS line HIGH
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_HIGH);
  cout << "\rRelease FLASH from Deep Power-Down... done" << endl;
  return true;
}

void FlashAccess::flash_deep_power_down(void)
{
  //Put in Deep Power-Down mode
  cout << "Put FLASH in Deep Power-Down... " << flush;
  //send command
  if(!dev->ebWrReg(baseAddress+FAR,
		   (uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|CMD_DPD))
  { 
    cout << "\rPut FLASH in Deep Power-Down... failed" << endl;
    return;
  }
  //set CS line HIGH
  if(!dev->ebWrReg(baseAddress+FAR,
		   (uint32_t)CS_HIGH))
  { 
    cout << "\rPut FLASH in Deep Power-Down... failed" << endl;
    return;
  }
  cout << "\rPut FLASH in Deep Power-Down... done" << endl;
}

void FlashAccess::flash_write_enable(void)
{
  //send Write Enable command
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|CMD_WR_ENABLE);
  //set CS line HIGH
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_HIGH);
}

void FlashAccess::flash_bulk_erase(void)
{
  //send Bulk Erase command
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|CMD_BK_ERASE);
  //set CS line HIGH
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_HIGH);
}

void FlashAccess::flash_sector_erase(uint8_t sector)
{
  //get the address of the sector
  uint32_t addr = sector * FLASH_SECTOR_SIZE;
  uint8_t addr0_lsb = addr & 0xff;
  uint8_t addr2_msb = (addr & 0xff0000) >> 16;
  uint8_t addr1     = (addr & 0xff00) >> 8;
  //send Sector Erase command
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_3_BYTE|
		(addr1<<16)|(addr2_msb<<8)|CMD_SECTOR_ERASE);
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|addr0_lsb);
  //set CS line HIGH
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_HIGH);
}

bool FlashAccess::flash_page_program(uint32_t addr)
{
  uint8_t addr0_lsb = addr & 0xff;
  uint8_t addr1     = (addr & 0x00ff00) >> 8;
  uint8_t addr2_msb = (addr & 0xff0000) >> 16;

  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_3_BYTE|(addr1<<16)|(addr2_msb<<8)|
		CMD_PG_PROGRAM);
  //flash_wait_for_spi_transfer();
  // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
  // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
  usleep(1);

  //send last address byte
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|addr0_lsb);

  //return flash_wait_for_spi_transfer();
  // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
  // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
  usleep(1);
  return true;
}

bool FlashAccess::flash_read(uint32_t addr, uint8_t *bitstream, size_t size)
{
  uint8_t addr0_lsb = addr & 0xff;
  uint8_t addr1     = (addr & 0x00ff00) >> 8;
  uint8_t addr2_msb = (addr & 0xff0000) >> 16;

  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_3_BYTE|(addr1<<16)|(addr2_msb<<8)|
		CMD_RD);
  //flash_wait_for_spi_transfer();
  // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
  // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
  usleep(1);

  //send last address byte
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|addr0_lsb);
  //return flash_wait_for_spi_transfer();
  // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
  // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
  usleep(1);

  flash_recv_bytes(bitstream, size);

  return true;
}

bool FlashAccess::flash_send_bytes(uint8_t *buf, size_t num)
{
  //this method has to be call after the page_program method
  size_t nb_3_bytes_to_send = num/3;
  //the aim is to send a maximum of 3 bytes. If num is not a multiple
  //of 3, we have to send 1 or 2 bytes after having sent nb_3_bytes_to_send*3
  //this 1 or 2 bytes are calculating with the modulo of 3.
  size_t send_n_byte = num % 3;
  //the FAR register of XIL_MULTIBOOT core sends max 3 bytes at a time
  uint8_t far_buf[3];
  size_t n,m;

  if(nb_3_bytes_to_send != 0)
  {
    for(n=0;n<nb_3_bytes_to_send;n++)
    {
      memcpy(far_buf,buf+(n*3),3);
      dev->ebWrReg(baseAddress+FAR,
		    (uint32_t)CS_LOW|START_SPI|SEND_3_BYTE|(far_buf[2]<<16)|
		    (far_buf[1]<<8)|far_buf[0]);
      // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
      // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
      usleep(1);
//      if(!flash_wait_for_spi_transfer())
//      {
//	return false;
//      }
    }
  }
  //remaining send_n_byte
  for(m=0;m<send_n_byte;m++)
  {
    memcpy(far_buf,buf+((n*3)+m),1);
    dev->ebWrReg(baseAddress+FAR,
		  (uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|*far_buf);
//    if(!flash_wait_for_spi_transfer())
//    {
//    	return false;
//    }
    // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
    // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
    usleep(1);
  }

  //set CS line HIGH
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_HIGH);

  //FLASH went in write cycle. Wait for WIP (time out 6ms) and returns its status
  return flash_wait_for_pp_wip(6000);

//  //FLASH went in write cycle. Wait for Page Program maximum time specified by
//  //datasheet (5ms)
//  usleep(5000);
//  return true;
}

bool FlashAccess::flash_recv_bytes(uint8_t *buf, size_t num)
{
  //this method has to be call after the page_program method
  size_t nb_3_bytes_to_recv = num/3;
  //the aim is to send a maximum of 3 bytes. If num is not a multiple
  //of 3, we have to send 1 or 2 bytes after having sent nb_3_bytes_to_send*3
  //this 1 or 2 bytes are calculating with the modulo of 3.
  size_t recv_n_byte = num % 3;
  uint32_t far = 0;
  size_t n;
  size_t i = 0;

  if(nb_3_bytes_to_recv != 0)
  {
    for(n=0;n<nb_3_bytes_to_recv;n++)
    {
      //send single SPI transfer
      dev->ebWrReg(baseAddress+FAR,(uint32_t)CS_LOW|START_SPI|SEND_3_BYTE|DUMMY_BYTE);
      // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
      // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
      usleep(1);
      //read FAR register
      dev->ebRdReg(baseAddress+FAR,&far);
      buf[i]   = far & 0x000000ff;
      buf[i+1] = (far & 0x0000ff00) >> 8;
      buf[i+2] = (far & 0x00ff0000) >> 16;
      i += 3;
    }
  }
  //remaining recv_n_byte
  for(n=0;n<recv_n_byte;n++)
  {
    //send single SPI transfer
    dev->ebWrReg(baseAddress+FAR,(uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|DUMMY_BYTE);
    // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
    // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
    usleep(1);
    //read FAR register
    dev->ebRdReg(baseAddress+FAR,&far);
    buf[i]   = far & 0x000000ff;
    i += 1;
  }

  //set CS line HIGH
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_HIGH);

  return true;
}

bool FlashAccess::flash_read_status(uint32_t *status)
{
  if(!dev->ebWrReg(baseAddress+FAR,
		   (uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|CMD_RD_STATUS))
  {
    return false;
  }
//  flash_wait_for_spi_transfer();
  // Reading HDL, SPI transfer must take 6 periods of multiboot clock (10MHz)
  // => SPI transfer ~ 600ns => wait 1us for SPI transfer completion
  usleep(1);

  //send single SPI transfer
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_LOW|START_SPI|SEND_1_BYTE|DUMMY_BYTE);
  //set CS line HIGH
  dev->ebWrReg(baseAddress+FAR,
		(uint32_t)CS_HIGH);
  //read FAR register
  dev->ebRdReg(baseAddress+FAR,status);
  return true;
}

bool FlashAccess::flash_wait_for_spi_transfer(void)
{
  uint32_t us_t;
  uint32_t reg;
  //Wait max 5000us=5ms for a SPI transfer. Otherwise, time out
  for(us_t=0;us_t<500;us_t++)
  {
    if(!dev->ebRdReg(baseAddress+FAR,&reg))
    {
      return false;
    }
    if(reg & 0x10000000) return true;
    usleep(10);
  }
  return false;
}

bool FlashAccess::flash_wait_for_wip(uint32_t time_out)
{
  uint32_t us_t;
  uint32_t reg;
  uint32_t limit=time_out/1000000; // wait 1s in time out loop
  //Wait until WIP goes to 0. Wait for time_out us.
  for(us_t=0;us_t<limit;us_t++)
  {
    //send Read Status command
    if(!flash_read_status(&reg))
    {
      return false;
    }
    //the read status of the FLASH is contained in the first byte
    //of the FAR register
    //check WIP (Write In Progress) bit. Bit 0 of status byte.
    //0 :erase ended, so exit the loop.
    if((reg & 0x1)==0)
    {
      return true;
    }
    usleep(1000000); // 1s
  }
  //time out reached, return false
  return false;
}

bool FlashAccess::flash_wait_for_pp_wip(uint32_t time_out)
{
  uint32_t us_t;
  uint32_t reg;
  uint32_t limit=time_out/1000; // wait 1ms in time out loop
  //Wait until WIP goes to 0. Wait for time_out us.
  for(us_t=0;us_t<limit;us_t++)
  {
    //send Read Status command
    if(!flash_read_status(&reg))
    {
      return false;
    }
    //the read status of the FLASH is contained in the first byte
    //of the FAR register
    //check WIP (Write In Progress) bit. Bit 0 of status byte.
    //0 :page program ended, so exit the loop.
    if((reg & 0x1)==0)
    {
      return true;
    }
    usleep(1000); // 1ms
  }
  //time out reached, return false
  return false;
}
