// ==============================================================================
/*!
 * @file   EbRecord.h
 *
 * @author antonin.broquet@esrf.fr
 * @brief  Class for Etherbone record
 */
// ==============================================================================

#ifndef EBRECORD_H_
#define EBRECORD_H_

#include <iostream>
#include <vector>
#include <stdint.h>

using namespace std;

/*!
 * This class is a class representing an Etherbone (EB) record in an Etherbone
 * packet. This class manages EB record for wishbone 32-bit data width only. <br>
 *
 * See the following page and document for a complete specification of Etherbone
 * implementation and protocol:<br>
 *  \li http://www.ohwr.org/projects/etherbone-core/wiki
 *  \li http://www.ohwr.org/projects/etherbone-core/documents
 *
 * @todo Add support to all data width: 8/16/32/64-bit.
 * 
 * @ingroup Etherbone
 * @headerfile EbPacket.h
 */

class EbRecord
{
public:

/*!
 * @name Constructor
 * Only one constructor is defined for this class */
//@{
/*!
 * The object constructor. It initializes record header to 0.
 */
  EbRecord();
//@}

/*!
 * @name Destructor
 * Only one destructor is defined for this class */
//@{
/*!
 * The object destructor
 */
  virtual ~EbRecord();
//@}


// Attributes
private:
  uint8_t flags;      /*!< Contains bits flags of the record header */
  uint8_t byteEnable; /*!< Specifies the data port width on the wishbone bus */
  uint8_t rCount;     /*!< Number of 32-bit words to read */
  uint8_t wCount;     /*!< Number of 32-bit words to write */
  vector<uint32_t> readAddr; /*!< Contains the BaseRetAddr word and all addresses of registers to read */
  vector<uint32_t> writeVal; /*!< Contains the BaseWriteAddr word and all values to write */

/*! @name Methods */
//{@
public:
/*!
 * Set the RCA bit of the record header.
 *
 * Set this bit if the addresses read in this record should be taken from the config address space.
 * Otherwise, addresses will be read from the wishbone bus.
 */
  void setRCA(void);
/*!
 * Set the BCA bit of the record header.
 *
 * Set this bit if the BaseRetAddr is in the config space.
 */
  void setBCA(void);
/*!
 * Set the RFF bit of the record header.
 *
 * Set this bit if the results of wishbone reads should be written back to a FIFO register located at
 * BaseRetAddr.
 * Otherwise, read values will be written sequentially starting at BaseRetAddr.
 */
  void setRFF(void);
/*!
 * Set the CYC bit of the record header.
 *
 * Set this bit if this record is the last of cycle.
 */
  void setCYC(void);
/*!
 * Set the WCA bit of the record header.
 *
 * Set this bit if the values written in this record should be written to the config address space.
 * Otherwise, values will be written to the wishbone bus.
 */
  void setWCA(void);
/*!
 * Set the WFF bit of the record header.
 *
 * Set this bit if the values written in this record should be written to a FIFO register.
 * Otherwise, values will be written sequentially starting at BaseWriteAddr.
 */
  void setWFF(void);


/*
 * Set the byte enable bits on the wishbone bus (8/16/32/64-bit width).
 *
 * For 64-bit data port width, all bits are used. For 8/16/32-bit widths, the rightmost
 * (least signicant) bits are used. The least signicant bit 15 matches the least signicant
 * byte of WriteVal.
 *
 * @param en The byte enable bits (0xf for 32-bit data width)
 */
//  void setByteEnable(uint8_t en);
/*!
 * Set the number of words to read in this record.
 *
 * @param rcount The number of words to read
 */
  void setRCount(uint8_t rcount);
/*!
 * Set the number of words to write in this record.
 *
 * @param wcount The number of words to write
 */
  void setWCount(uint8_t wcount);


/*!
 * Set the BaseRetAddr value for the read data section.
 *
 * This field is present only if rCount > 0. When it exists it indicates the address
 * on the origin Etherbone endpoint to which read results should be written.
 *
 * <b>Be careful:</b> The BaseRetAddr must be the first element of EB record data.
 * So if the pushRdAddr is called before this method, the first element of readAddr
 * container will be erased.
 *
 * @param baseRetAddr The value of the BaseRetAddr field.
 */
  void setBaseRdAddr(uint32_t baseRetAddr);
/*!
 * Set the BaseWriteAddr value for the write data section.
 *
 * This field is present only if wCount > 0. When it exists it indicates the address
 * on the origin Etherbone endpoint to which values should be written.
 *
 * <b>Be careful:</b> The BaseWriteAddr must be the first element of EB record data.
 * So if the pushWrValue is called before this method, the first element of writeVal
 * container will be erased.
 *
 * @param baseWriteAddr The value of the BaseWriteAddr field.
 */
  void setBaseWrAddr(uint32_t baseWriteAddr);


/*!
 * Push back a read address in the read data section.
 *
 * The addr is pushed back in the readAddr container.
 *
 * @param addr The address to read.
 */
  void pushRdAddr(uint32_t addr);
/*!
 * Push back a value to write in the write data section.
 *
 * The value is pushed back in the writeVal container.
 *
 * @param value The value to write.
 */
  void pushWrValue(uint32_t value);
/*!
 * Get the size of this EB record.
 *
 * @return The size of this EB record in number of 16-bit words.
 */
  size_t getEbRecordSize(void);
/*!
 * Get the data of this EB record.
 *
 * @param buf Pointer to the destination array where the EB record data are to be copied.
 * @return The size of this EB record in number of 16-bit words.
 */
  size_t getEbRecordData(uint16_t *buf);
//@}
};

#endif /* EBRECORD_H_ */
